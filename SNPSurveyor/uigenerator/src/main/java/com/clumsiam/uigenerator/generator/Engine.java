package com.clumsiam.uigenerator.generator;

import android.view.View;

/**
 * Created by Indocyber on 26/03/2018.
 */

public interface Engine {
    void onUIEvent(View view, String event, String tagUI, String values);
    void save();
    void submit();
    void next();
}
