package com.clumsiam.uigenerator.utils;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import java.util.HashMap;

/**
 * Created by Indocyber on 26/03/2018.
 */

public abstract class Calculate {
    public static long exec(String formula, HashMap<String,String> dataStore, JSONArray variable){
        ExpressionBuilder e = new ExpressionBuilder(formula);
        for (int i=0;i<variable.length();i++){
            try {
                String var=variable.getString(i);
                e.variable(var);
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
        }
        Expression expression=e.build();
        for (int i=0;i<variable.length();i++){
            try {
                String var=variable.getString(i);
                if(dataStore.containsKey(var)){
                    if(!dataStore.get(var).equals("")){
                        expression.setVariable(var,Formating.getCleanResultLong(dataStore.get(var)));
                    }else {
                        expression.setVariable(var,0);
                    }
                }else {
                    expression.setVariable(var,0);
                }
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
        }
        return (long) expression.evaluate();
    }
}

