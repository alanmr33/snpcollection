package com.clumsiam.uigenerator.generator;

/**
 * Created by Indocyber on 24/04/2018.
 */

public interface OnSetValue {
    public void OnSet(String data);
}

