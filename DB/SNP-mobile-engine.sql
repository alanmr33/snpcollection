/*
Navicat SQL Server Data Transfer

Source Server         : snp
Source Server Version : 130000
Source Host           : 127.0.0.1:1433
Source Database       : SNP
Source Schema         : dbo

Target Server Type    : SQL Server
Target Server Version : 130000
File Encoding         : 65001

Date: 2018-04-25 09:05:58
*/


-- ----------------------------
-- Table structure for AUTHORITY_LIST
-- ----------------------------
DROP TABLE [dbo].[AUTHORITY_LIST]
GO
CREATE TABLE [dbo].[AUTHORITY_LIST] (
[ID] int NOT NULL IDENTITY(1,1) ,
[AUTHORITY_ID] int NULL ,
[MENU] int NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[AUTHORITY_LIST]', RESEED, 1070)
GO

-- ----------------------------
-- Records of AUTHORITY_LIST
-- ----------------------------
SET IDENTITY_INSERT [dbo].[AUTHORITY_LIST] ON
GO
INSERT INTO [dbo].[AUTHORITY_LIST] ([ID], [AUTHORITY_ID], [MENU]) VALUES (N'1064', N'1', N'1')
GO
GO
INSERT INTO [dbo].[AUTHORITY_LIST] ([ID], [AUTHORITY_ID], [MENU]) VALUES (N'1065', N'1', N'6')
GO
GO
INSERT INTO [dbo].[AUTHORITY_LIST] ([ID], [AUTHORITY_ID], [MENU]) VALUES (N'1066', N'1', N'1002')
GO
GO
INSERT INTO [dbo].[AUTHORITY_LIST] ([ID], [AUTHORITY_ID], [MENU]) VALUES (N'1067', N'1', N'2')
GO
GO
INSERT INTO [dbo].[AUTHORITY_LIST] ([ID], [AUTHORITY_ID], [MENU]) VALUES (N'1068', N'1', N'3')
GO
GO
INSERT INTO [dbo].[AUTHORITY_LIST] ([ID], [AUTHORITY_ID], [MENU]) VALUES (N'1069', N'1', N'5')
GO
GO
INSERT INTO [dbo].[AUTHORITY_LIST] ([ID], [AUTHORITY_ID], [MENU]) VALUES (N'1070', N'1', N'4')
GO
GO
SET IDENTITY_INSERT [dbo].[AUTHORITY_LIST] OFF
GO

-- ----------------------------
-- Table structure for AUTHORITY_MST
-- ----------------------------
DROP TABLE [dbo].[AUTHORITY_MST]
GO
CREATE TABLE [dbo].[AUTHORITY_MST] (
[AUTHORITY_ID] int NOT NULL IDENTITY(1,1) ,
[AUTHORITY_NAME] varchar(70) NULL ,
[AKTIF_STATUS] int NULL ,
[CREATE_USER] varchar(10) NULL ,
[CREATE_DATE] datetime NULL ,
[MODIFY_USER] varchar(10) NULL ,
[MODIFY_DATE] datetime NULL ,
[MENU] int NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[AUTHORITY_MST]', RESEED, 4)
GO

-- ----------------------------
-- Records of AUTHORITY_MST
-- ----------------------------
SET IDENTITY_INSERT [dbo].[AUTHORITY_MST] ON
GO
INSERT INTO [dbo].[AUTHORITY_MST] ([AUTHORITY_ID], [AUTHORITY_NAME], [AKTIF_STATUS], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE], [MENU]) VALUES (N'1', N'ADMIN COLL', N'1', N'IGLO', N'2018-03-21 00:00:00.000', null, null, null)
GO
GO
INSERT INTO [dbo].[AUTHORITY_MST] ([AUTHORITY_ID], [AUTHORITY_NAME], [AKTIF_STATUS], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE], [MENU]) VALUES (N'4', N'COLLECTOR', N'1', N'IGLO', N'2018-03-27 17:25:20.000', null, null, null)
GO
GO
SET IDENTITY_INSERT [dbo].[AUTHORITY_MST] OFF
GO

-- ----------------------------
-- Table structure for CABANG_MST
-- ----------------------------
DROP TABLE [dbo].[CABANG_MST]
GO
CREATE TABLE [dbo].[CABANG_MST] (
[KODE_CABANG] varchar(10) NOT NULL ,
[KODE_REGION] varchar(10) NULL ,
[NAMA_CABANG] varchar(70) NULL ,
[ALAMAT] varchar(100) NULL ,
[AKTIF_STATUS] int NULL ,
[CREATE_USER] varchar(10) NULL ,
[CREATE_DATE] datetime NULL ,
[MODIFY_USER] varchar(10) NULL ,
[MODIFY_DATE] datetime NULL 
)


GO

-- ----------------------------
-- Records of CABANG_MST
-- ----------------------------
INSERT INTO [dbo].[CABANG_MST] ([KODE_CABANG], [KODE_REGION], [NAMA_CABANG], [ALAMAT], [AKTIF_STATUS], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE]) VALUES (N'CAB001', N'REG001', N'JAKARTA CENTRAL', N'DEKET MONAS', N'1', N'USE001', N'2018-03-20 00:00:00.000', N'USE001', N'2018-03-20 00:00:00.000')
GO
GO

-- ----------------------------
-- Table structure for COLLECTION_ACTIVITY
-- ----------------------------
DROP TABLE [dbo].[COLLECTION_ACTIVITY]
GO
CREATE TABLE [dbo].[COLLECTION_ACTIVITY] (
[ACTIVITY_ID] varchar(32) NOT NULL ,
[ACTIVITY_DATE] datetime NULL ,
[ACTIVITY_BODY] varchar(MAX) NULL ,
[ACTIVITY_ACTOR] varchar(10) NULL ,
[ORDER_ID] int NULL 
)


GO

-- ----------------------------
-- Records of COLLECTION_ACTIVITY
-- ----------------------------

-- ----------------------------
-- Table structure for COLLECTION_DETAIL
-- ----------------------------
DROP TABLE [dbo].[COLLECTION_DETAIL]
GO
CREATE TABLE [dbo].[COLLECTION_DETAIL] (
[ORDER_ID] int NOT NULL ,
[CUSTOMER_CODE] nvarchar(20) NOT NULL ,
[CONTRACT_NO] nvarchar(15) NOT NULL ,
[INSTALLMENT_NO] int NOT NULL ,
[LPK_NO] nvarchar(15) NULL ,
[CUSTOMER_NAME] nvarchar(100) NULL ,
[ADDRESS] nvarchar(1000) NULL ,
[VILLAGE] nvarchar(100) NULL ,
[DISTRICT] nvarchar(100) NULL ,
[RT] nvarchar(3) NULL ,
[RW] nvarchar(3) NULL ,
[CITY] nvarchar(100) NULL ,
[KODE_PROVINSI] varchar(5) NULL ,
[POST_CODE] nvarchar(10) NULL ,
[DUE_DATE] date NULL ,
[INSTALLMENT_AMT] int NULL ,
[DEPOSIT_AMT] int NULL ,
[PENALTY_AMT] int NULL ,
[COLLECTION_FEE] int NULL ,
[TOTAL_AMT] int NULL ,
[RECEIPT_NO] nvarchar(15) NULL ,
[MIN_PAYMENT] int NULL ,
[MAX_PAYMENT] int NULL ,
[REPAYMENT_DATE] date NULL ,
[NOTES] nvarchar(200) NULL ,
[PROMISE_COUNT] int NULL ,
[PROMISE_AVAILABLE] int NULL ,
[PROMISE_RANGE] int NULL ,
[PHONE_AREA] nvarchar(4) NULL ,
[PHONE_NUMBER] nvarchar(12) NULL ,
[MOBILE_PREFIX] nvarchar(4) NULL ,
[MOBILE_NO] nvarchar(12) NULL ,
[NAMA_BARANG] nvarchar(255) NULL ,
[PHOTO] varbinary(MAX) NULL 
)


GO

-- ----------------------------
-- Records of COLLECTION_DETAIL
-- ----------------------------
INSERT INTO [dbo].[COLLECTION_DETAIL] ([ORDER_ID], [CUSTOMER_CODE], [CONTRACT_NO], [INSTALLMENT_NO], [LPK_NO], [CUSTOMER_NAME], [ADDRESS], [VILLAGE], [DISTRICT], [RT], [RW], [CITY], [KODE_PROVINSI], [POST_CODE], [DUE_DATE], [INSTALLMENT_AMT], [DEPOSIT_AMT], [PENALTY_AMT], [COLLECTION_FEE], [TOTAL_AMT], [RECEIPT_NO], [MIN_PAYMENT], [MAX_PAYMENT], [REPAYMENT_DATE], [NOTES], [PROMISE_COUNT], [PROMISE_AVAILABLE], [PROMISE_RANGE], [PHONE_AREA], [PHONE_NUMBER], [MOBILE_PREFIX], [MOBILE_NO], [NAMA_BARANG], [PHOTO]) VALUES (N'1', N'001', N'1', N'91', N'1', N'alan', N'jl.kk', N'babakan', N'kertajati', N'1', N'2', N'majalengka', N'01', N'45457', N'2018-04-02', N'1700000', N'16000', N'18831', N'800', N'981983188', N'1', N'100000', N'100000', N'2018-04-03', N'-', N'1', N'1', N'1010', N'0194', N'13891893', N'0294', N'289849', N'kodok', null)
GO
GO

-- ----------------------------
-- Table structure for COLLECTION_HEADER
-- ----------------------------
DROP TABLE [dbo].[COLLECTION_HEADER]
GO
CREATE TABLE [dbo].[COLLECTION_HEADER] (
[ORDER_ID] int NOT NULL ,
[LPK_NO] nvarchar(15) NULL ,
[LPK_DATE] date NULL ,
[COLLECTOR_CODE] nvarchar(12) NULL ,
[STATUS] varchar(3) NULL ,
[REASSIGN] nvarchar(1) NULL ,
[REASSIGN_DATE] date NULL ,
[REASSIGN_BY] varchar(10) NULL ,
[PREVIOUS_COLLECTOR] varchar(12) NULL ,
[REASSIGN_NOTES] varchar(255) NULL 
)


GO

-- ----------------------------
-- Records of COLLECTION_HEADER
-- ----------------------------
INSERT INTO [dbo].[COLLECTION_HEADER] ([ORDER_ID], [LPK_NO], [LPK_DATE], [COLLECTOR_CODE], [STATUS], [REASSIGN], [REASSIGN_DATE], [REASSIGN_BY], [PREVIOUS_COLLECTOR], [REASSIGN_NOTES]) VALUES (N'1', N'1', N'2018-04-02', N'01', N'ASN', N'0', null, null, null, null)
GO
GO

-- ----------------------------
-- Table structure for COLLECTION_LOG
-- ----------------------------
DROP TABLE [dbo].[COLLECTION_LOG]
GO
CREATE TABLE [dbo].[COLLECTION_LOG] (
[ORDER_ID] int NOT NULL ,
[ACTIVITY_DATE] datetime NOT NULL ,
[STATUS] varchar(3) NULL ,
[CHANGED_BY] varchar(10) NULL 
)


GO

-- ----------------------------
-- Records of COLLECTION_LOG
-- ----------------------------

-- ----------------------------
-- Table structure for EVENTS
-- ----------------------------
DROP TABLE [dbo].[EVENTS]
GO
CREATE TABLE [dbo].[EVENTS] (
[event_id] nvarchar(8) NULL ,
[event_type] varchar(30) NULL ,
[event_component] varchar(100) NULL ,
[event_component_target] varchar(100) NULL ,
[event_data] varchar(MAX) NULL ,
[form_id] nvarchar(8) NULL 
)


GO

-- ----------------------------
-- Records of EVENTS
-- ----------------------------

-- ----------------------------
-- Table structure for FORM_FIELDS
-- ----------------------------
DROP TABLE [dbo].[FORM_FIELDS]
GO
CREATE TABLE [dbo].[FORM_FIELDS] (
[field_id] nvarchar(8) NOT NULL ,
[field_name] varchar(100) NULL ,
[field_type] varchar(50) NULL ,
[field_visibility] varchar(15) NULL ,
[field_label] varchar(100) NULL ,
[field_global_value] varchar(50) NULL ,
[field_weight] varchar(4) NULL ,
[field_store] varchar(100) NULL ,
[field_extra] varchar(MAX) NULL ,
[row_id] nvarchar(8) NULL 
)


GO

-- ----------------------------
-- Records of FORM_FIELDS
-- ----------------------------

-- ----------------------------
-- Table structure for FORM_ROWS
-- ----------------------------
DROP TABLE [dbo].[FORM_ROWS]
GO
CREATE TABLE [dbo].[FORM_ROWS] (
[row_id] nvarchar(8) NOT NULL ,
[row_order] int NULL ,
[row_visibility] varchar(15) NULL ,
[form_id] nvarchar(8) NULL 
)


GO

-- ----------------------------
-- Records of FORM_ROWS
-- ----------------------------

-- ----------------------------
-- Table structure for FORMS
-- ----------------------------
DROP TABLE [dbo].[FORMS]
GO
CREATE TABLE [dbo].[FORMS] (
[form_id] nvarchar(8) NOT NULL ,
[form_name] varchar(100) NULL ,
[form_label] varchar(255) NULL ,
[form_order] int NULL 
)


GO

-- ----------------------------
-- Records of FORMS
-- ----------------------------
INSERT INTO [dbo].[FORMS] ([form_id], [form_name], [form_label], [form_order]) VALUES (N'01', N'INIT', N'Inisiasi', N'1')
GO
GO
INSERT INTO [dbo].[FORMS] ([form_id], [form_name], [form_label], [form_order]) VALUES (N'02', N'PROMISE', N'Janji Bayar', N'2')
GO
GO
INSERT INTO [dbo].[FORMS] ([form_id], [form_name], [form_label], [form_order]) VALUES (N'03', N'PAYMENT', N'Payment', N'3')
GO
GO

-- ----------------------------
-- Table structure for HANDLING
-- ----------------------------
DROP TABLE [dbo].[HANDLING]
GO
CREATE TABLE [dbo].[HANDLING] (
[BATCH_HANDLING] int NOT NULL ,
[CONTRACT_NO] nvarchar(15) NOT NULL ,
[INSTALLMENT_NO] int NOT NULL ,
[ORDER_ID] int NOT NULL ,
[KODE_HANDLING] varchar(3) NULL ,
[KODE_KONSUMEN] nvarchar(20) NULL ,
[NO_LPK] nvarchar(15) NULL ,
[NOTES] nvarchar(1000) NULL ,
[TANGGAL_HANDLING] date NULL ,
[TANGGAL_JANJI] date NULL ,
[PROMISE_COUNT] int NULL ,
[PROMISE_AVAILABLE] int NULL ,
[PROMISE_RANGE] int NULL ,
[STATUS] nvarchar(3) NULL ,
[CREATE_USER] varchar(10) NULL ,
[CREATE_DATE] date NULL ,
[MODIFY_USER] varchar(10) NULL ,
[MODIFY_DATE] date NULL 
)


GO

-- ----------------------------
-- Records of HANDLING
-- ----------------------------

-- ----------------------------
-- Table structure for LOGIN_LOGS
-- ----------------------------
DROP TABLE [dbo].[LOGIN_LOGS]
GO
CREATE TABLE [dbo].[LOGIN_LOGS] (
[USER_ID] varchar(10) NOT NULL ,
[LOG_DATE] date NOT NULL ,
[LOGIN] time(7) NULL ,
[LOGOUT] time(7) NULL 
)


GO

-- ----------------------------
-- Records of LOGIN_LOGS
-- ----------------------------

-- ----------------------------
-- Table structure for MASTER_HANDLING
-- ----------------------------
DROP TABLE [dbo].[MASTER_HANDLING]
GO
CREATE TABLE [dbo].[MASTER_HANDLING] (
[BATCH_HANDLING_NO] int NOT NULL ,
[HANDLING_CODE] nvarchar(3) NULL ,
[PROMISE_FLAG] nvarchar(1) NULL ,
[DESKRIPSI_HANDLING] nvarchar(1000) NULL ,
[AKTIF_STATUS] nvarchar(1) NULL ,
[CREATE_USER] varchar(10) NULL ,
[CREATE_DATE] date NULL ,
[MODIFY_USER] varchar(10) NULL ,
[MODIFY_DATE] date NULL 
)


GO

-- ----------------------------
-- Records of MASTER_HANDLING
-- ----------------------------
INSERT INTO [dbo].[MASTER_HANDLING] ([BATCH_HANDLING_NO], [HANDLING_CODE], [PROMISE_FLAG], [DESKRIPSI_HANDLING], [AKTIF_STATUS], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE]) VALUES (N'1', N'1', N'0', N'P1-Fraud atau Penggelapan Uang angsuran
', N'Y', null, null, null, null)
GO
GO
INSERT INTO [dbo].[MASTER_HANDLING] ([BATCH_HANDLING_NO], [HANDLING_CODE], [PROMISE_FLAG], [DESKRIPSI_HANDLING], [AKTIF_STATUS], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE]) VALUES (N'2', N'2', N'0', N'P2-Force Major ( Bencana Alam, Meninggal dunia )', N'Y', null, null, null, null)
GO
GO
INSERT INTO [dbo].[MASTER_HANDLING] ([BATCH_HANDLING_NO], [HANDLING_CODE], [PROMISE_FLAG], [DESKRIPSI_HANDLING], [AKTIF_STATUS], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE]) VALUES (N'3', N'3', N'0', N'P3-Pindah Alamat', N'Y', null, null, null, null)
GO
GO
INSERT INTO [dbo].[MASTER_HANDLING] ([BATCH_HANDLING_NO], [HANDLING_CODE], [PROMISE_FLAG], [DESKRIPSI_HANDLING], [AKTIF_STATUS], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE]) VALUES (N'4', N'4', N'0', N'P4-Konsumen Kabur atau Hilang', N'Y', null, null, null, null)
GO
GO
INSERT INTO [dbo].[MASTER_HANDLING] ([BATCH_HANDLING_NO], [HANDLING_CODE], [PROMISE_FLAG], [DESKRIPSI_HANDLING], [AKTIF_STATUS], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE]) VALUES (N'5', N'5', N'0', N'P5-Gagal Janji Bayar', N'Y', null, null, null, null)
GO
GO
INSERT INTO [dbo].[MASTER_HANDLING] ([BATCH_HANDLING_NO], [HANDLING_CODE], [PROMISE_FLAG], [DESKRIPSI_HANDLING], [AKTIF_STATUS], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE]) VALUES (N'6', N'6', N'0', N'P6-Penjualan Fiktif', N'Y', null, null, null, null)
GO
GO
INSERT INTO [dbo].[MASTER_HANDLING] ([BATCH_HANDLING_NO], [HANDLING_CODE], [PROMISE_FLAG], [DESKRIPSI_HANDLING], [AKTIF_STATUS], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE]) VALUES (N'7', N'7', N'0', N'P7-Karakter kurang Baik', N'Y', null, null, null, null)
GO
GO
INSERT INTO [dbo].[MASTER_HANDLING] ([BATCH_HANDLING_NO], [HANDLING_CODE], [PROMISE_FLAG], [DESKRIPSI_HANDLING], [AKTIF_STATUS], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE]) VALUES (N'8', N'8', N'0', N'P8-Complain Penjualan Awal Kredit ( tenor, Angsuran, Produk)', N'Y', null, null, null, null)
GO
GO
INSERT INTO [dbo].[MASTER_HANDLING] ([BATCH_HANDLING_NO], [HANDLING_CODE], [PROMISE_FLAG], [DESKRIPSI_HANDLING], [AKTIF_STATUS], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE]) VALUES (N'9', N'9', N'0', N'P9-Complain service atau Barang rusak', N'Y', null, null, null, null)
GO
GO
INSERT INTO [dbo].[MASTER_HANDLING] ([BATCH_HANDLING_NO], [HANDLING_CODE], [PROMISE_FLAG], [DESKRIPSI_HANDLING], [AKTIF_STATUS], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE]) VALUES (N'10', N'10', N'0', N'P10-Barang di Gadai/Pindah tangan/ dijual dan tidak ada barang pengganti', N'Y', null, null, null, null)
GO
GO
INSERT INTO [dbo].[MASTER_HANDLING] ([BATCH_HANDLING_NO], [HANDLING_CODE], [PROMISE_FLAG], [DESKRIPSI_HANDLING], [AKTIF_STATUS], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE]) VALUES (N'11', N'11', N'0', N'P11-Konsumen sudah Bayar sebagian', N'Y', null, null, null, null)
GO
GO
INSERT INTO [dbo].[MASTER_HANDLING] ([BATCH_HANDLING_NO], [HANDLING_CODE], [PROMISE_FLAG], [DESKRIPSI_HANDLING], [AKTIF_STATUS], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE]) VALUES (N'12', N'12', N'0', N'P12-Masalah Input System', N'Y', null, null, null, null)
GO
GO
INSERT INTO [dbo].[MASTER_HANDLING] ([BATCH_HANDLING_NO], [HANDLING_CODE], [PROMISE_FLAG], [DESKRIPSI_HANDLING], [AKTIF_STATUS], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE]) VALUES (N'13', N'13', N'0', N'P13-Konsumen minta barang ditarik', N'Y', null, null, null, null)
GO
GO
INSERT INTO [dbo].[MASTER_HANDLING] ([BATCH_HANDLING_NO], [HANDLING_CODE], [PROMISE_FLAG], [DESKRIPSI_HANDLING], [AKTIF_STATUS], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE]) VALUES (N'14', N'14', N'0', N'P14-STNK belum diterima', N'Y', null, null, null, null)
GO
GO
INSERT INTO [dbo].[MASTER_HANDLING] ([BATCH_HANDLING_NO], [HANDLING_CODE], [PROMISE_FLAG], [DESKRIPSI_HANDLING], [AKTIF_STATUS], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE]) VALUES (N'15', N'15', N'0', N'P15-Usaha Bangkrut/PHK', N'Y', null, null, null, null)
GO
GO
INSERT INTO [dbo].[MASTER_HANDLING] ([BATCH_HANDLING_NO], [HANDLING_CODE], [PROMISE_FLAG], [DESKRIPSI_HANDLING], [AKTIF_STATUS], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE]) VALUES (N'16', N'0', N'1', N'Janji Bayar', N'Y', null, null, null, null)
GO
GO

-- ----------------------------
-- Table structure for MASTER_PROVINSI
-- ----------------------------
DROP TABLE [dbo].[MASTER_PROVINSI]
GO
CREATE TABLE [dbo].[MASTER_PROVINSI] (
[KODE_PROVINSI] varchar(5) NOT NULL ,
[NAMA_PROVINSI] varchar(50) NULL ,
[ACTIVE_STATUS] int NULL ,
[CREATE_USER] varchar(10) NULL ,
[CREATE_DATE] datetime NULL ,
[MODIFY_USER] varchar(10) NULL ,
[MODIFY_DATE] datetime NULL 
)


GO

-- ----------------------------
-- Records of MASTER_PROVINSI
-- ----------------------------
INSERT INTO [dbo].[MASTER_PROVINSI] ([KODE_PROVINSI], [NAMA_PROVINSI], [ACTIVE_STATUS], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE]) VALUES (N'01', N'DKI Jakarta', null, null, null, null, null)
GO
GO

-- ----------------------------
-- Table structure for MENU_MST
-- ----------------------------
DROP TABLE [dbo].[MENU_MST]
GO
CREATE TABLE [dbo].[MENU_MST] (
[MENU] int NOT NULL IDENTITY(1,1) ,
[MENU_NAME] varchar(70) NULL ,
[URL] varchar(70) NULL ,
[MENU_SORT] varchar(70) NULL ,
[DISPLAY_FLG] varchar(10) NULL ,
[CREATE_USER] varchar(10) NULL ,
[CREATE_DATE] datetime NULL ,
[MODIFY_USER] varchar(10) NULL ,
[MODIFY_DATE] datetime NULL ,
[AKTIF_STATUS] int NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[MENU_MST]', RESEED, 1003)
GO

-- ----------------------------
-- Records of MENU_MST
-- ----------------------------
SET IDENTITY_INSERT [dbo].[MENU_MST] ON
GO
INSERT INTO [dbo].[MENU_MST] ([MENU], [MENU_NAME], [URL], [MENU_SORT], [DISPLAY_FLG], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE], [AKTIF_STATUS]) VALUES (N'1', N'MASTER USER', N'masterUser', null, N'wd', N'IGLO', N'2018-02-21 00:00:00.000', null, null, N'1')
GO
GO
INSERT INTO [dbo].[MENU_MST] ([MENU], [MENU_NAME], [URL], [MENU_SORT], [DISPLAY_FLG], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE], [AKTIF_STATUS]) VALUES (N'2', N'MASTER REGIONAL', N'masterRegion', null, null, N'IGLO', N'2018-02-21 00:00:00.000', null, null, N'1')
GO
GO
INSERT INTO [dbo].[MENU_MST] ([MENU], [MENU_NAME], [URL], [MENU_SORT], [DISPLAY_FLG], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE], [AKTIF_STATUS]) VALUES (N'3', N'MASTER CABANG', N'masterCabang', null, null, N'IGLO', N'2018-02-21 00:00:00.000', null, null, N'1')
GO
GO
INSERT INTO [dbo].[MENU_MST] ([MENU], [MENU_NAME], [URL], [MENU_SORT], [DISPLAY_FLG], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE], [AKTIF_STATUS]) VALUES (N'4', N'MASTER MENU', N'masterMenu', null, null, N'IGLO', N'2018-02-21 00:00:00.000', null, null, N'1')
GO
GO
INSERT INTO [dbo].[MENU_MST] ([MENU], [MENU_NAME], [URL], [MENU_SORT], [DISPLAY_FLG], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE], [AKTIF_STATUS]) VALUES (N'5', N'MASTER AUTHORITY', N'masterAuthority', null, null, N'IGLO', N'2018-02-21 00:00:00.000', null, null, N'1')
GO
GO
INSERT INTO [dbo].[MENU_MST] ([MENU], [MENU_NAME], [URL], [MENU_SORT], [DISPLAY_FLG], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE], [AKTIF_STATUS]) VALUES (N'6', N'CHANGE PASSWORD', N'changePassword', null, null, N'IGLO', N'2018-02-21 00:00:00.000', null, null, N'1')
GO
GO
INSERT INTO [dbo].[MENU_MST] ([MENU], [MENU_NAME], [URL], [MENU_SORT], [DISPLAY_FLG], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE], [AKTIF_STATUS]) VALUES (N'1002', N'AKSES', N'akses', null, null, N'IGLO', N'2018-02-21 00:00:00.000', null, null, N'1')
GO
GO
SET IDENTITY_INSERT [dbo].[MENU_MST] OFF
GO

-- ----------------------------
-- Table structure for MOBILE_TRACKING
-- ----------------------------
DROP TABLE [dbo].[MOBILE_TRACKING]
GO
CREATE TABLE [dbo].[MOBILE_TRACKING] (
[USER_ID] varchar(10) NOT NULL ,
[TRACKING_DATE] datetime NOT NULL ,
[LOCATION] varchar(100) NULL ,
[ORDER_ID] varchar(20) NULL 
)


GO

-- ----------------------------
-- Records of MOBILE_TRACKING
-- ----------------------------

-- ----------------------------
-- Table structure for PARAM_MST
-- ----------------------------
DROP TABLE [dbo].[PARAM_MST]
GO
CREATE TABLE [dbo].[PARAM_MST] (
[PARAM_ID] varchar(3) NOT NULL ,
[CONDITION] varchar(50) NULL ,
[LEVEL_PARAM] varchar(100) NULL ,
[PARENT_ID] nvarchar(3) NULL 
)


GO

-- ----------------------------
-- Records of PARAM_MST
-- ----------------------------
INSERT INTO [dbo].[PARAM_MST] ([PARAM_ID], [CONDITION], [LEVEL_PARAM], [PARENT_ID]) VALUES (N'APP', N'AppVersion', N'V.1.0', null)
GO
GO
INSERT INTO [dbo].[PARAM_MST] ([PARAM_ID], [CONDITION], [LEVEL_PARAM], [PARENT_ID]) VALUES (N'ASN', N'SystemStatus', N'ASSIGN', null)
GO
GO
INSERT INTO [dbo].[PARAM_MST] ([PARAM_ID], [CONDITION], [LEVEL_PARAM], [PARENT_ID]) VALUES (N'BD0', N'BertemuDengan', N'Istri', null)
GO
GO
INSERT INTO [dbo].[PARAM_MST] ([PARAM_ID], [CONDITION], [LEVEL_PARAM], [PARENT_ID]) VALUES (N'BD1', N'BertemuDengan', N'Anak', null)
GO
GO
INSERT INTO [dbo].[PARAM_MST] ([PARAM_ID], [CONDITION], [LEVEL_PARAM], [PARENT_ID]) VALUES (N'BD2', N'BertemuDengan', N'Orang Tua', null)
GO
GO
INSERT INTO [dbo].[PARAM_MST] ([PARAM_ID], [CONDITION], [LEVEL_PARAM], [PARENT_ID]) VALUES (N'BD3', N'BertemuDengan', N'Anggota Keluarga Lainnya', null)
GO
GO
INSERT INTO [dbo].[PARAM_MST] ([PARAM_ID], [CONDITION], [LEVEL_PARAM], [PARENT_ID]) VALUES (N'BD4', N'BertemuDengan', N'Karyawan', null)
GO
GO
INSERT INTO [dbo].[PARAM_MST] ([PARAM_ID], [CONDITION], [LEVEL_PARAM], [PARENT_ID]) VALUES (N'BD5', N'BertemuDengan', N'Pembantu', null)
GO
GO
INSERT INTO [dbo].[PARAM_MST] ([PARAM_ID], [CONDITION], [LEVEL_PARAM], [PARENT_ID]) VALUES (N'BD6', N'BertemuDengan', N'Tidak Bertemu satupun', null)
GO
GO
INSERT INTO [dbo].[PARAM_MST] ([PARAM_ID], [CONDITION], [LEVEL_PARAM], [PARENT_ID]) VALUES (N'CLS', N'SystemStatus', N'CLOSE', null)
GO
GO
INSERT INTO [dbo].[PARAM_MST] ([PARAM_ID], [CONDITION], [LEVEL_PARAM], [PARENT_ID]) VALUES (N'DWN', N'SystemStatus', N'DOWNLOAD', null)
GO
GO
INSERT INTO [dbo].[PARAM_MST] ([PARAM_ID], [CONDITION], [LEVEL_PARAM], [PARENT_ID]) VALUES (N'FT0', N'FollowUpType', N'Site Visit', null)
GO
GO
INSERT INTO [dbo].[PARAM_MST] ([PARAM_ID], [CONDITION], [LEVEL_PARAM], [PARENT_ID]) VALUES (N'FT1', N'FollowUpType', N'Phone Call', null)
GO
GO
INSERT INTO [dbo].[PARAM_MST] ([PARAM_ID], [CONDITION], [LEVEL_PARAM], [PARENT_ID]) VALUES (N'LGC', N'LogicVersion', N'V.1.0.1', null)
GO
GO
INSERT INTO [dbo].[PARAM_MST] ([PARAM_ID], [CONDITION], [LEVEL_PARAM], [PARENT_ID]) VALUES (N'RSN', N'SystemStatus', N'REASSIGN', null)
GO
GO
INSERT INTO [dbo].[PARAM_MST] ([PARAM_ID], [CONDITION], [LEVEL_PARAM], [PARENT_ID]) VALUES (N'SA0', N'StatusAlamat', N'Sesuai', null)
GO
GO
INSERT INTO [dbo].[PARAM_MST] ([PARAM_ID], [CONDITION], [LEVEL_PARAM], [PARENT_ID]) VALUES (N'SA1', N'StatusAlamat', N'Tidak Sesuai', null)
GO
GO
INSERT INTO [dbo].[PARAM_MST] ([PARAM_ID], [CONDITION], [LEVEL_PARAM], [PARENT_ID]) VALUES (N'SB0', N'StatusBayar', N'Bayar', null)
GO
GO
INSERT INTO [dbo].[PARAM_MST] ([PARAM_ID], [CONDITION], [LEVEL_PARAM], [PARENT_ID]) VALUES (N'SB1', N'StatusBayar', N'Tidak Bayar', null)
GO
GO
INSERT INTO [dbo].[PARAM_MST] ([PARAM_ID], [CONDITION], [LEVEL_PARAM], [PARENT_ID]) VALUES (N'SBT', N'SystemStatus', N'SUBMIT', null)
GO
GO
INSERT INTO [dbo].[PARAM_MST] ([PARAM_ID], [CONDITION], [LEVEL_PARAM], [PARENT_ID]) VALUES (N'ST0', N'StatusTelp', N'Sesuai', null)
GO
GO
INSERT INTO [dbo].[PARAM_MST] ([PARAM_ID], [CONDITION], [LEVEL_PARAM], [PARENT_ID]) VALUES (N'ST1', N'StatusTelp', N'Tidak Sesuai', null)
GO
GO
INSERT INTO [dbo].[PARAM_MST] ([PARAM_ID], [CONDITION], [LEVEL_PARAM], [PARENT_ID]) VALUES (N'VR0', N'VisitResult', N'PTP', null)
GO
GO
INSERT INTO [dbo].[PARAM_MST] ([PARAM_ID], [CONDITION], [LEVEL_PARAM], [PARENT_ID]) VALUES (N'VR1', N'VisitResult', N'Pembayaran', null)
GO
GO

-- ----------------------------
-- Table structure for PAYMENT
-- ----------------------------
DROP TABLE [dbo].[PAYMENT]
GO
CREATE TABLE [dbo].[PAYMENT] (
[INSTALLMENT_NO] int NOT NULL ,
[ORDER_ID] int NOT NULL ,
[CUSTOMER_CODE] nvarchar(20) NULL ,
[CONTRACT_NO] nvarchar(15) NOT NULL ,
[CODE_COLLECTOR] varchar(12) NULL ,
[TOTAL_BAYAR] int NULL ,
[NO_KWITANSI] int NULL ,
[NO_LPK] nvarchar(15) NULL ,
[TANGGAL_TRANSAKSI] date NULL ,
[TANGGAL_BAYAR] date NULL ,
[STATUS] varchar(3) NULL ,
[CREATE_USER] varchar(10) NULL ,
[CREATED_DATE] date NULL ,
[MODIFY_USER] varchar(10) NULL ,
[MODIFY_DATE] date NULL 
)


GO

-- ----------------------------
-- Records of PAYMENT
-- ----------------------------

-- ----------------------------
-- Table structure for REASSIGN_COLLECTOR
-- ----------------------------
DROP TABLE [dbo].[REASSIGN_COLLECTOR]
GO
CREATE TABLE [dbo].[REASSIGN_COLLECTOR] (
[COLLECTOR_CODE] varchar(12) NOT NULL ,
[SEQ_NO] int NOT NULL ,
[REASSIGN_COLLECTOR_CODE] varchar(12) NULL ,
[PERIOD_FROM] date NULL ,
[PERIOD_TO] date NULL ,
[NOTES] varchar(255) NULL ,
[ORDER_ID] int NULL 
)


GO

-- ----------------------------
-- Records of REASSIGN_COLLECTOR
-- ----------------------------

-- ----------------------------
-- Table structure for REGION_MST
-- ----------------------------
DROP TABLE [dbo].[REGION_MST]
GO
CREATE TABLE [dbo].[REGION_MST] (
[KODE_REGION] varchar(10) NOT NULL ,
[NAMA_REGION] varchar(70) NULL ,
[NOTES] varchar(100) NULL ,
[AKTIF_STATUS] int NULL ,
[CREATE_USER] varchar(10) NULL ,
[CREATE_DATE] datetime NULL ,
[MODIFY_USER] varchar(10) NULL ,
[MODIFY_DATE] datetime NULL 
)


GO

-- ----------------------------
-- Records of REGION_MST
-- ----------------------------
INSERT INTO [dbo].[REGION_MST] ([KODE_REGION], [NAMA_REGION], [NOTES], [AKTIF_STATUS], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE]) VALUES (N'REG001', N'DKI JAKARTA', N'DAERAH JAKARTA', N'1', N'USE001', N'2018-03-20 00:00:00.000', N'USE001', N'2018-03-26 17:11:59.657')
GO
GO
INSERT INTO [dbo].[REGION_MST] ([KODE_REGION], [NAMA_REGION], [NOTES], [AKTIF_STATUS], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE]) VALUES (N'REG002', N'coba', N'Inimah coba aja', N'0', N'USE001', N'2018-03-26 17:12:25.270', N'USE001', N'2018-03-26 17:19:54.283')
GO
GO

-- ----------------------------
-- Table structure for TOKEN_MST
-- ----------------------------
DROP TABLE [dbo].[TOKEN_MST]
GO
CREATE TABLE [dbo].[TOKEN_MST] (
[TOKEN] varchar(32) NOT NULL ,
[TOKEN_DATE] datetime NULL ,
[USER_ID] varchar(10) NULL 
)


GO

-- ----------------------------
-- Records of TOKEN_MST
-- ----------------------------
INSERT INTO [dbo].[TOKEN_MST] ([TOKEN], [TOKEN_DATE], [USER_ID]) VALUES (N'15e56b9dfc6942fdeae3ee9a7ae47701', N'2018-04-02 09:44:35.423', N'USE002')
GO
GO
INSERT INTO [dbo].[TOKEN_MST] ([TOKEN], [TOKEN_DATE], [USER_ID]) VALUES (N'2773fca134ea266cd5b9a6b3fdf13241', N'2018-04-03 16:51:22.960', N'USE002')
GO
GO
INSERT INTO [dbo].[TOKEN_MST] ([TOKEN], [TOKEN_DATE], [USER_ID]) VALUES (N'29e01bd829a2c0c29d8b57c82578d26c', N'2018-04-09 15:26:17.630', N'USE002')
GO
GO
INSERT INTO [dbo].[TOKEN_MST] ([TOKEN], [TOKEN_DATE], [USER_ID]) VALUES (N'2c68175fa469b9879d79360d607664ec', N'2018-04-12 14:09:30.483', N'USE002')
GO
GO
INSERT INTO [dbo].[TOKEN_MST] ([TOKEN], [TOKEN_DATE], [USER_ID]) VALUES (N'2eeeda8b79e0c7086c19493027e41f68', N'2018-04-09 17:07:41.230', N'USE002')
GO
GO
INSERT INTO [dbo].[TOKEN_MST] ([TOKEN], [TOKEN_DATE], [USER_ID]) VALUES (N'35157960aaf9794d125d6d7205755ca0', N'2018-04-09 16:45:36.653', N'USE002')
GO
GO
INSERT INTO [dbo].[TOKEN_MST] ([TOKEN], [TOKEN_DATE], [USER_ID]) VALUES (N'3ee7f7fa924151f3c464fa72cab1aa06', N'2018-04-02 09:46:45.040', N'USE002')
GO
GO
INSERT INTO [dbo].[TOKEN_MST] ([TOKEN], [TOKEN_DATE], [USER_ID]) VALUES (N'4885467b7430ad6c20e25daafc74eca6', N'2018-04-09 17:06:35.130', N'USE002')
GO
GO
INSERT INTO [dbo].[TOKEN_MST] ([TOKEN], [TOKEN_DATE], [USER_ID]) VALUES (N'64dde158577c9a6ead541d715f303667', N'2018-04-12 14:07:15.490', N'USE002')
GO
GO
INSERT INTO [dbo].[TOKEN_MST] ([TOKEN], [TOKEN_DATE], [USER_ID]) VALUES (N'6709816ae5f914f31cde4e5c3f575ed4', N'2018-04-09 16:58:41.373', N'USE002')
GO
GO
INSERT INTO [dbo].[TOKEN_MST] ([TOKEN], [TOKEN_DATE], [USER_ID]) VALUES (N'6c28e311a0551cb2f565f28ab76f8775', N'2018-04-03 16:47:51.393', N'USE002')
GO
GO
INSERT INTO [dbo].[TOKEN_MST] ([TOKEN], [TOKEN_DATE], [USER_ID]) VALUES (N'6ffd61f02591bc9ca4631f7b3c8a6d39', N'2018-04-12 13:17:44.930', N'USE002')
GO
GO
INSERT INTO [dbo].[TOKEN_MST] ([TOKEN], [TOKEN_DATE], [USER_ID]) VALUES (N'72f4b28f38c70a082db8a2cd58773c4c', N'2018-04-04 13:46:42.930', N'USE002')
GO
GO
INSERT INTO [dbo].[TOKEN_MST] ([TOKEN], [TOKEN_DATE], [USER_ID]) VALUES (N'73ff25856f49023e44a4183457671ef0', N'2018-04-04 15:23:20.737', N'USE002')
GO
GO
INSERT INTO [dbo].[TOKEN_MST] ([TOKEN], [TOKEN_DATE], [USER_ID]) VALUES (N'7eb118bb5d66695a84e716800d785cc4', N'2018-04-12 10:56:24.700', N'USE002')
GO
GO
INSERT INTO [dbo].[TOKEN_MST] ([TOKEN], [TOKEN_DATE], [USER_ID]) VALUES (N'7f3c94b317169e8a50bdfa33bc1520e9', N'2018-04-12 10:56:06.543', N'USE002')
GO
GO
INSERT INTO [dbo].[TOKEN_MST] ([TOKEN], [TOKEN_DATE], [USER_ID]) VALUES (N'88892d0208a9618b27585104002a09fd', N'2018-04-04 13:41:50.290', N'USE002')
GO
GO
INSERT INTO [dbo].[TOKEN_MST] ([TOKEN], [TOKEN_DATE], [USER_ID]) VALUES (N'9a865df30557aea36d63d3759897f0f5', N'2018-04-12 15:45:47.290', N'USE002')
GO
GO
INSERT INTO [dbo].[TOKEN_MST] ([TOKEN], [TOKEN_DATE], [USER_ID]) VALUES (N'b5869ac4bec6d7ec54c4857514a357b5', N'2018-04-04 14:48:33.413', N'USE002')
GO
GO
INSERT INTO [dbo].[TOKEN_MST] ([TOKEN], [TOKEN_DATE], [USER_ID]) VALUES (N'b9c69fcc6fb8e78c37ecd016b5711541', N'2018-04-09 17:04:03.157', N'USE002')
GO
GO
INSERT INTO [dbo].[TOKEN_MST] ([TOKEN], [TOKEN_DATE], [USER_ID]) VALUES (N'd09db8a91ef94803c71c8bb4f20ae78c', N'2018-04-02 11:12:51.917', N'USE002')
GO
GO
INSERT INTO [dbo].[TOKEN_MST] ([TOKEN], [TOKEN_DATE], [USER_ID]) VALUES (N'd5843327055f3038c40defe5864fb84b', N'2018-04-24 14:26:29.760', N'USE002')
GO
GO
INSERT INTO [dbo].[TOKEN_MST] ([TOKEN], [TOKEN_DATE], [USER_ID]) VALUES (N'd6e910d358e8281a8e5982bfc2fab04e', N'2018-04-09 17:05:09.367', N'USE002')
GO
GO
INSERT INTO [dbo].[TOKEN_MST] ([TOKEN], [TOKEN_DATE], [USER_ID]) VALUES (N'e444b010dc7414a39239d3b2161a559b', N'2018-04-02 10:20:32.720', N'USE002')
GO
GO

-- ----------------------------
-- Table structure for UPDATE_KONSUMEN
-- ----------------------------
DROP TABLE [dbo].[UPDATE_KONSUMEN]
GO
CREATE TABLE [dbo].[UPDATE_KONSUMEN] (
[INSTALLMENT_NO] int NOT NULL ,
[ORDER_ID] int NOT NULL ,
[CUSTOMER_CODE] nvarchar(20) NOT NULL ,
[CONTRACT_NO] nvarchar(15) NULL ,
[NAMA_KONSUMEN] nvarchar(100) NULL ,
[ALAMAT] nvarchar(1000) NULL ,
[RT] nvarchar(3) NULL ,
[RW] nvarchar(3) NULL ,
[KELURAHAN] nvarchar(100) NULL ,
[KECAMATAN] nvarchar(100) NULL ,
[KODE_PROVINSI] varchar(5) NULL ,
[STATUS] varchar(3) NULL 
)


GO

-- ----------------------------
-- Records of UPDATE_KONSUMEN
-- ----------------------------

-- ----------------------------
-- Table structure for USER_PROFILE
-- ----------------------------
DROP TABLE [dbo].[USER_PROFILE]
GO
CREATE TABLE [dbo].[USER_PROFILE] (
[USER_ID] varchar(10) NOT NULL ,
[KODE_CABANG] varchar(10) NULL ,
[KODE_REGION] varchar(10) NULL ,
[KODE_KOLEKTOR] varchar(12) NULL ,
[KODE_KARYAWAN] varchar(10) NULL ,
[NAMA_KARYAWAN] varchar(100) NULL ,
[AUTHORITY_ID] int NULL ,
[IMEI_NUMBER] varchar(20) NULL ,
[PHONE_NUMBER] varchar(15) NULL ,
[TANGGAL_MULAI] datetime NULL ,
[TANGGAL_AKHIR] datetime NULL ,
[AKTIF_STATUS] int NULL ,
[PASSWORD] varchar(75) NULL ,
[CREATE_USER] varchar(10) NULL ,
[CREATE_DATE] datetime NULL ,
[MODIFY_USER] varchar(10) NULL ,
[MODIFY_DATE] datetime NULL ,
[KODE_REGIONAL] varchar(255) NULL 
)


GO

-- ----------------------------
-- Records of USER_PROFILE
-- ----------------------------
INSERT INTO [dbo].[USER_PROFILE] ([USER_ID], [KODE_CABANG], [KODE_REGION], [KODE_KOLEKTOR], [KODE_KARYAWAN], [NAMA_KARYAWAN], [AUTHORITY_ID], [IMEI_NUMBER], [PHONE_NUMBER], [TANGGAL_MULAI], [TANGGAL_AKHIR], [AKTIF_STATUS], [PASSWORD], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE], [KODE_REGIONAL]) VALUES (N'sccs', N'CAB001', N'REG001', null, N'jdfn', N'ihfjkerfhub', N'1', N'098hdfj83', N'098765456788', null, null, N'1', N'NfeqQp4J', N'USE001', N'2018-03-26 14:23:42.003', null, null, null)
GO
GO
INSERT INTO [dbo].[USER_PROFILE] ([USER_ID], [KODE_CABANG], [KODE_REGION], [KODE_KOLEKTOR], [KODE_KARYAWAN], [NAMA_KARYAWAN], [AUTHORITY_ID], [IMEI_NUMBER], [PHONE_NUMBER], [TANGGAL_MULAI], [TANGGAL_AKHIR], [AKTIF_STATUS], [PASSWORD], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE], [KODE_REGIONAL]) VALUES (N'sdcsdac', N'CAB001', N'REG001', null, N'karr', N'sdksdfksf', N'1', N'7y343hbdfh3', N'09876545678', null, null, N'1', N'lEUAcyGn', N'USE001', N'2018-03-26 14:22:49.623', null, null, null)
GO
GO
INSERT INTO [dbo].[USER_PROFILE] ([USER_ID], [KODE_CABANG], [KODE_REGION], [KODE_KOLEKTOR], [KODE_KARYAWAN], [NAMA_KARYAWAN], [AUTHORITY_ID], [IMEI_NUMBER], [PHONE_NUMBER], [TANGGAL_MULAI], [TANGGAL_AKHIR], [AKTIF_STATUS], [PASSWORD], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE], [KODE_REGIONAL]) VALUES (N'USE001', N'CAB001', N'REG001', null, null, N'INDOCYBER', N'1', N'098765456789', N'098765456787', N'2018-03-22 00:00:00.000', null, N'1', N'1ndocyber', N'ADMIN', N'2018-03-22 00:00:00.000', N'USE001', N'2018-03-26 15:04:07.540', null)
GO
GO
INSERT INTO [dbo].[USER_PROFILE] ([USER_ID], [KODE_CABANG], [KODE_REGION], [KODE_KOLEKTOR], [KODE_KARYAWAN], [NAMA_KARYAWAN], [AUTHORITY_ID], [IMEI_NUMBER], [PHONE_NUMBER], [TANGGAL_MULAI], [TANGGAL_AKHIR], [AKTIF_STATUS], [PASSWORD], [CREATE_USER], [CREATE_DATE], [MODIFY_USER], [MODIFY_DATE], [KODE_REGIONAL]) VALUES (N'USE002', N'CAB001', N'REG001', N'01', null, N'KARYAWAN1', N'4', N'353634092958320', N'0812456734837', null, null, N'1', N'1234', N'USE001', N'2018-03-26 13:24:45.840', N'USE002', N'2018-03-26 14:13:35.010', null)
GO
GO

-- ----------------------------
-- Indexes structure for table AUTHORITY_LIST
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table AUTHORITY_LIST
-- ----------------------------
ALTER TABLE [dbo].[AUTHORITY_LIST] ADD PRIMARY KEY ([ID])
GO

-- ----------------------------
-- Indexes structure for table AUTHORITY_MST
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table AUTHORITY_MST
-- ----------------------------
ALTER TABLE [dbo].[AUTHORITY_MST] ADD PRIMARY KEY ([AUTHORITY_ID])
GO

-- ----------------------------
-- Indexes structure for table CABANG_MST
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table CABANG_MST
-- ----------------------------
ALTER TABLE [dbo].[CABANG_MST] ADD PRIMARY KEY ([KODE_CABANG])
GO

-- ----------------------------
-- Indexes structure for table COLLECTION_ACTIVITY
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table COLLECTION_ACTIVITY
-- ----------------------------
ALTER TABLE [dbo].[COLLECTION_ACTIVITY] ADD PRIMARY KEY ([ACTIVITY_ID])
GO

-- ----------------------------
-- Indexes structure for table COLLECTION_DETAIL
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table COLLECTION_DETAIL
-- ----------------------------
ALTER TABLE [dbo].[COLLECTION_DETAIL] ADD PRIMARY KEY ([ORDER_ID], [CONTRACT_NO], [INSTALLMENT_NO])
GO

-- ----------------------------
-- Indexes structure for table COLLECTION_HEADER
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table COLLECTION_HEADER
-- ----------------------------
ALTER TABLE [dbo].[COLLECTION_HEADER] ADD PRIMARY KEY ([ORDER_ID])
GO

-- ----------------------------
-- Indexes structure for table COLLECTION_LOG
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table COLLECTION_LOG
-- ----------------------------
ALTER TABLE [dbo].[COLLECTION_LOG] ADD PRIMARY KEY ([ORDER_ID], [ACTIVITY_DATE])
GO

-- ----------------------------
-- Indexes structure for table FORM_FIELDS
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table FORM_FIELDS
-- ----------------------------
ALTER TABLE [dbo].[FORM_FIELDS] ADD PRIMARY KEY ([field_id])
GO

-- ----------------------------
-- Indexes structure for table FORM_ROWS
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table FORM_ROWS
-- ----------------------------
ALTER TABLE [dbo].[FORM_ROWS] ADD PRIMARY KEY ([row_id])
GO

-- ----------------------------
-- Indexes structure for table FORMS
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table FORMS
-- ----------------------------
ALTER TABLE [dbo].[FORMS] ADD PRIMARY KEY ([form_id])
GO

-- ----------------------------
-- Indexes structure for table HANDLING
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table HANDLING
-- ----------------------------
ALTER TABLE [dbo].[HANDLING] ADD PRIMARY KEY ([BATCH_HANDLING], [CONTRACT_NO], [INSTALLMENT_NO], [ORDER_ID])
GO

-- ----------------------------
-- Indexes structure for table LOGIN_LOGS
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table LOGIN_LOGS
-- ----------------------------
ALTER TABLE [dbo].[LOGIN_LOGS] ADD PRIMARY KEY ([USER_ID], [LOG_DATE])
GO

-- ----------------------------
-- Indexes structure for table MASTER_HANDLING
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table MASTER_HANDLING
-- ----------------------------
ALTER TABLE [dbo].[MASTER_HANDLING] ADD PRIMARY KEY ([BATCH_HANDLING_NO])
GO

-- ----------------------------
-- Indexes structure for table MASTER_PROVINSI
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table MASTER_PROVINSI
-- ----------------------------
ALTER TABLE [dbo].[MASTER_PROVINSI] ADD PRIMARY KEY ([KODE_PROVINSI])
GO

-- ----------------------------
-- Indexes structure for table MENU_MST
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table MENU_MST
-- ----------------------------
ALTER TABLE [dbo].[MENU_MST] ADD PRIMARY KEY ([MENU])
GO

-- ----------------------------
-- Indexes structure for table MOBILE_TRACKING
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table MOBILE_TRACKING
-- ----------------------------
ALTER TABLE [dbo].[MOBILE_TRACKING] ADD PRIMARY KEY ([USER_ID], [TRACKING_DATE])
GO

-- ----------------------------
-- Indexes structure for table PARAM_MST
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table PARAM_MST
-- ----------------------------
ALTER TABLE [dbo].[PARAM_MST] ADD PRIMARY KEY ([PARAM_ID])
GO

-- ----------------------------
-- Indexes structure for table PAYMENT
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table PAYMENT
-- ----------------------------
ALTER TABLE [dbo].[PAYMENT] ADD PRIMARY KEY ([INSTALLMENT_NO], [ORDER_ID], [CONTRACT_NO])
GO

-- ----------------------------
-- Indexes structure for table REASSIGN_COLLECTOR
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table REASSIGN_COLLECTOR
-- ----------------------------
ALTER TABLE [dbo].[REASSIGN_COLLECTOR] ADD PRIMARY KEY ([COLLECTOR_CODE], [SEQ_NO])
GO

-- ----------------------------
-- Indexes structure for table REGION_MST
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table REGION_MST
-- ----------------------------
ALTER TABLE [dbo].[REGION_MST] ADD PRIMARY KEY ([KODE_REGION])
GO

-- ----------------------------
-- Indexes structure for table TOKEN_MST
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table TOKEN_MST
-- ----------------------------
ALTER TABLE [dbo].[TOKEN_MST] ADD PRIMARY KEY ([TOKEN])
GO

-- ----------------------------
-- Indexes structure for table UPDATE_KONSUMEN
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table UPDATE_KONSUMEN
-- ----------------------------
ALTER TABLE [dbo].[UPDATE_KONSUMEN] ADD PRIMARY KEY ([INSTALLMENT_NO], [ORDER_ID], [CUSTOMER_CODE])
GO

-- ----------------------------
-- Indexes structure for table USER_PROFILE
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table USER_PROFILE
-- ----------------------------
ALTER TABLE [dbo].[USER_PROFILE] ADD PRIMARY KEY ([USER_ID])
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[AUTHORITY_LIST]
-- ----------------------------
ALTER TABLE [dbo].[AUTHORITY_LIST] ADD FOREIGN KEY ([AUTHORITY_ID]) REFERENCES [dbo].[AUTHORITY_MST] ([AUTHORITY_ID]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[AUTHORITY_LIST] ADD FOREIGN KEY ([MENU]) REFERENCES [dbo].[MENU_MST] ([MENU]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[CABANG_MST]
-- ----------------------------
ALTER TABLE [dbo].[CABANG_MST] ADD FOREIGN KEY ([KODE_REGION]) REFERENCES [dbo].[REGION_MST] ([KODE_REGION]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[COLLECTION_DETAIL]
-- ----------------------------
ALTER TABLE [dbo].[COLLECTION_DETAIL] ADD FOREIGN KEY ([ORDER_ID]) REFERENCES [dbo].[COLLECTION_HEADER] ([ORDER_ID]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[COLLECTION_DETAIL] ADD FOREIGN KEY ([KODE_PROVINSI]) REFERENCES [dbo].[MASTER_PROVINSI] ([KODE_PROVINSI]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[FORM_FIELDS]
-- ----------------------------
ALTER TABLE [dbo].[FORM_FIELDS] ADD FOREIGN KEY ([row_id]) REFERENCES [dbo].[FORM_ROWS] ([row_id]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[FORM_ROWS]
-- ----------------------------
ALTER TABLE [dbo].[FORM_ROWS] ADD FOREIGN KEY ([form_id]) REFERENCES [dbo].[FORMS] ([form_id]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[TOKEN_MST]
-- ----------------------------
ALTER TABLE [dbo].[TOKEN_MST] ADD FOREIGN KEY ([USER_ID]) REFERENCES [dbo].[USER_PROFILE] ([USER_ID]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[UPDATE_KONSUMEN]
-- ----------------------------
ALTER TABLE [dbo].[UPDATE_KONSUMEN] ADD FOREIGN KEY ([KODE_PROVINSI]) REFERENCES [dbo].[MASTER_PROVINSI] ([KODE_PROVINSI]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[USER_PROFILE]
-- ----------------------------
ALTER TABLE [dbo].[USER_PROFILE] ADD FOREIGN KEY ([KODE_CABANG]) REFERENCES [dbo].[CABANG_MST] ([KODE_CABANG]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[USER_PROFILE] ADD FOREIGN KEY ([KODE_REGION]) REFERENCES [dbo].[REGION_MST] ([KODE_REGION]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[USER_PROFILE] ADD FOREIGN KEY ([AUTHORITY_ID]) REFERENCES [dbo].[AUTHORITY_MST] ([AUTHORITY_ID]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
