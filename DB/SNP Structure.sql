/*
Navicat SQL Server Data Transfer

Source Server         : snp
Source Server Version : 130000
Source Host           : 127.0.0.1:1433
Source Database       : SNP
Source Schema         : dbo

Target Server Type    : SQL Server
Target Server Version : 130000
File Encoding         : 65001

Date: 2018-05-09 09:01:14
*/


-- ----------------------------
-- Table structure for AUTHORITY_LIST
-- ----------------------------
DROP TABLE [dbo].[AUTHORITY_LIST]
GO
CREATE TABLE [dbo].[AUTHORITY_LIST] (
[ID] int NOT NULL IDENTITY(1,1) ,
[AUTHORITY_ID] int NULL ,
[MENU] int NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[AUTHORITY_LIST]', RESEED, 1070)
GO

-- ----------------------------
-- Table structure for AUTHORITY_MST
-- ----------------------------
DROP TABLE [dbo].[AUTHORITY_MST]
GO
CREATE TABLE [dbo].[AUTHORITY_MST] (
[AUTHORITY_ID] int NOT NULL IDENTITY(1,1) ,
[AUTHORITY_NAME] varchar(70) NULL ,
[AKTIF_STATUS] int NULL ,
[CREATE_USER] varchar(10) NULL ,
[CREATE_DATE] datetime NULL ,
[MODIFY_USER] varchar(10) NULL ,
[MODIFY_DATE] datetime NULL ,
[MENU] int NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[AUTHORITY_MST]', RESEED, 4)
GO

-- ----------------------------
-- Table structure for CABANG_MST
-- ----------------------------
DROP TABLE [dbo].[CABANG_MST]
GO
CREATE TABLE [dbo].[CABANG_MST] (
[KODE_CABANG] varchar(10) NOT NULL ,
[KODE_REGION] varchar(10) NULL ,
[NAMA_CABANG] varchar(70) NULL ,
[ALAMAT] varchar(100) NULL ,
[AKTIF_STATUS] int NULL ,
[CREATE_USER] varchar(10) NULL ,
[CREATE_DATE] datetime NULL ,
[MODIFY_USER] varchar(10) NULL ,
[MODIFY_DATE] datetime NULL 
)


GO

-- ----------------------------
-- Table structure for COLLECTION_ACTIVITY
-- ----------------------------
DROP TABLE [dbo].[COLLECTION_ACTIVITY]
GO
CREATE TABLE [dbo].[COLLECTION_ACTIVITY] (
[ACTIVITY_ID] varchar(32) NOT NULL ,
[ACTIVITY_DATE] datetime NULL ,
[ACTIVITY_BODY] varchar(MAX) NULL ,
[ACTIVITY_ACTOR] varchar(10) NULL ,
[ORDER_ID] int NULL 
)


GO

-- ----------------------------
-- Table structure for COLLECTION_DETAIL
-- ----------------------------
DROP TABLE [dbo].[COLLECTION_DETAIL]
GO
CREATE TABLE [dbo].[COLLECTION_DETAIL] (
[ORDER_ID] int NOT NULL ,
[CUSTOMER_CODE] nvarchar(20) NOT NULL ,
[CONTRACT_NO] nvarchar(15) NOT NULL ,
[INSTALLMENT_NO] int NOT NULL ,
[LPK_NO] nvarchar(15) NULL ,
[CUSTOMER_NAME] nvarchar(100) NULL ,
[ADDRESS] nvarchar(1000) NULL ,
[VILLAGE] nvarchar(100) NULL ,
[DISTRICT] nvarchar(100) NULL ,
[RT] nvarchar(3) NULL ,
[RW] nvarchar(3) NULL ,
[CITY] nvarchar(100) NULL ,
[KODE_PROVINSI] varchar(5) NULL ,
[POST_CODE] nvarchar(10) NULL ,
[DUE_DATE] date NULL ,
[INSTALLMENT_AMT] int NULL ,
[DEPOSIT_AMT] int NULL ,
[PENALTY_AMT] int NULL ,
[COLLECTION_FEE] int NULL ,
[TOTAL_AMT] int NULL ,
[RECEIPT_NO] nvarchar(15) NULL ,
[MIN_PAYMENT] int NULL ,
[MAX_PAYMENT] int NULL ,
[REPAYMENT_DATE] date NULL ,
[NOTES] nvarchar(200) NULL ,
[PROMISE_COUNT] int NULL ,
[PROMISE_AVAILABLE] int NULL ,
[PROMISE_RANGE] int NULL ,
[PHONE_AREA] nvarchar(4) NULL ,
[PHONE_NUMBER] nvarchar(12) NULL ,
[MOBILE_PREFIX] nvarchar(4) NULL ,
[MOBILE_NO] nvarchar(12) NULL ,
[NAMA_BARANG] nvarchar(255) NULL ,
[PHOTO] varchar(100) NULL 
)


GO

-- ----------------------------
-- Table structure for COLLECTION_HEADER
-- ----------------------------
DROP TABLE [dbo].[COLLECTION_HEADER]
GO
CREATE TABLE [dbo].[COLLECTION_HEADER] (
[ORDER_ID] int NOT NULL ,
[LPK_NO] nvarchar(15) NULL ,
[LPK_DATE] date NULL ,
[COLLECTOR_CODE] nvarchar(12) NULL ,
[STATUS] varchar(3) NULL ,
[REASSIGN] nvarchar(1) NULL ,
[REASSIGN_DATE] date NULL ,
[REASSIGN_BY] varchar(10) NULL ,
[PREVIOUS_COLLECTOR] varchar(12) NULL ,
[REASSIGN_NOTES] varchar(255) NULL 
)


GO

-- ----------------------------
-- Table structure for COLLECTION_LOG
-- ----------------------------
DROP TABLE [dbo].[COLLECTION_LOG]
GO
CREATE TABLE [dbo].[COLLECTION_LOG] (
[ORDER_ID] int NOT NULL ,
[ACTIVITY_DATE] datetime NOT NULL ,
[STATUS] varchar(3) NULL ,
[CHANGED_BY] varchar(10) NULL 
)


GO

-- ----------------------------
-- Table structure for EVENTS
-- ----------------------------
DROP TABLE [dbo].[EVENTS]
GO
CREATE TABLE [dbo].[EVENTS] (
[event_id] nvarchar(8) NULL ,
[event_type] varchar(30) NULL ,
[event_component] varchar(100) NULL ,
[event_component_target] varchar(100) NULL ,
[event_data] varchar(MAX) NULL ,
[form_id] nvarchar(8) NULL 
)


GO

-- ----------------------------
-- Table structure for FORM_FIELDS
-- ----------------------------
DROP TABLE [dbo].[FORM_FIELDS]
GO
CREATE TABLE [dbo].[FORM_FIELDS] (
[field_id] nvarchar(8) NOT NULL ,
[field_name] varchar(100) NULL ,
[field_type] varchar(50) NULL ,
[field_visibility] varchar(15) NULL ,
[field_label] varchar(100) NULL ,
[field_global_value] varchar(50) NULL ,
[field_weight] varchar(4) NULL ,
[field_store] varchar(100) NULL ,
[field_extra] varchar(MAX) NULL ,
[row_id] nvarchar(8) NULL 
)


GO

-- ----------------------------
-- Table structure for FORM_ROWS
-- ----------------------------
DROP TABLE [dbo].[FORM_ROWS]
GO
CREATE TABLE [dbo].[FORM_ROWS] (
[row_id] nvarchar(8) NOT NULL ,
[row_order] int NULL ,
[row_visibility] varchar(15) NULL ,
[form_id] nvarchar(8) NULL 
)


GO

-- ----------------------------
-- Table structure for FORMS
-- ----------------------------
DROP TABLE [dbo].[FORMS]
GO
CREATE TABLE [dbo].[FORMS] (
[form_id] nvarchar(8) NOT NULL ,
[form_name] varchar(100) NULL ,
[form_label] varchar(255) NULL ,
[form_order] int NULL 
)


GO

-- ----------------------------
-- Table structure for HANDLING
-- ----------------------------
DROP TABLE [dbo].[HANDLING]
GO
CREATE TABLE [dbo].[HANDLING] (
[BATCH_HANDLING] int NOT NULL ,
[CONTRACT_NO] nvarchar(15) NOT NULL ,
[INSTALLMENT_NO] int NOT NULL ,
[ORDER_ID] int NOT NULL ,
[KODE_HANDLING] varchar(3) NULL ,
[KODE_KONSUMEN] nvarchar(20) NULL ,
[NO_LPK] nvarchar(15) NULL ,
[NOTES] nvarchar(1000) NULL ,
[TANGGAL_HANDLING] datetime NULL ,
[TANGGAL_JANJI] datetime NULL ,
[PROMISE_COUNT] int NULL ,
[PROMISE_AVAILABLE] int NULL ,
[PROMISE_RANGE] int NULL ,
[STATUS] nvarchar(3) NULL ,
[CREATE_USER] varchar(10) NULL ,
[CREATE_DATE] datetime NULL ,
[MODIFY_USER] varchar(10) NULL ,
[MODIFY_DATE] datetime NULL 
)


GO

-- ----------------------------
-- Table structure for LOGIN_LOGS
-- ----------------------------
DROP TABLE [dbo].[LOGIN_LOGS]
GO
CREATE TABLE [dbo].[LOGIN_LOGS] (
[USER_ID] varchar(10) NOT NULL ,
[LOG_DATE] date NOT NULL ,
[LOGIN] time(7) NULL ,
[LOGOUT] time(7) NULL 
)


GO

-- ----------------------------
-- Table structure for MASTER_HANDLING
-- ----------------------------
DROP TABLE [dbo].[MASTER_HANDLING]
GO
CREATE TABLE [dbo].[MASTER_HANDLING] (
[BATCH_HANDLING_NO] int NOT NULL ,
[HANDLING_CODE] nvarchar(3) NULL ,
[PROMISE_FLAG] nvarchar(1) NULL ,
[DESKRIPSI_HANDLING] nvarchar(1000) NULL ,
[AKTIF_STATUS] nvarchar(1) NULL ,
[CREATE_USER] varchar(10) NULL ,
[CREATE_DATE] date NULL ,
[MODIFY_USER] varchar(10) NULL ,
[MODIFY_DATE] date NULL 
)


GO

-- ----------------------------
-- Table structure for MASTER_PROVINSI
-- ----------------------------
DROP TABLE [dbo].[MASTER_PROVINSI]
GO
CREATE TABLE [dbo].[MASTER_PROVINSI] (
[KODE_PROVINSI] varchar(5) NOT NULL ,
[NAMA_PROVINSI] varchar(50) NULL ,
[ACTIVE_STATUS] int NULL ,
[CREATE_USER] varchar(10) NULL ,
[CREATE_DATE] datetime NULL ,
[MODIFY_USER] varchar(10) NULL ,
[MODIFY_DATE] datetime NULL 
)


GO

-- ----------------------------
-- Table structure for MENU_MST
-- ----------------------------
DROP TABLE [dbo].[MENU_MST]
GO
CREATE TABLE [dbo].[MENU_MST] (
[MENU] int NOT NULL IDENTITY(1,1) ,
[MENU_NAME] varchar(70) NULL ,
[URL] varchar(70) NULL ,
[MENU_SORT] varchar(70) NULL ,
[DISPLAY_FLG] varchar(10) NULL ,
[CREATE_USER] varchar(10) NULL ,
[CREATE_DATE] datetime NULL ,
[MODIFY_USER] varchar(10) NULL ,
[MODIFY_DATE] datetime NULL ,
[AKTIF_STATUS] int NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[MENU_MST]', RESEED, 1003)
GO

-- ----------------------------
-- Table structure for MOBILE_TRACKING
-- ----------------------------
DROP TABLE [dbo].[MOBILE_TRACKING]
GO
CREATE TABLE [dbo].[MOBILE_TRACKING] (
[USER_ID] varchar(10) NOT NULL ,
[TRACKING_DATE] datetime NOT NULL ,
[LOCATION] varchar(100) NULL ,
[ORDER_ID] varchar(20) NULL 
)


GO

-- ----------------------------
-- Table structure for PARAM_MST
-- ----------------------------
DROP TABLE [dbo].[PARAM_MST]
GO
CREATE TABLE [dbo].[PARAM_MST] (
[PARAM_ID] varchar(3) NOT NULL ,
[CONDITION] varchar(50) NULL ,
[LEVEL_PARAM] varchar(100) NULL ,
[PARENT_ID] nvarchar(3) NULL 
)


GO

-- ----------------------------
-- Table structure for PAYMENT
-- ----------------------------
DROP TABLE [dbo].[PAYMENT]
GO
CREATE TABLE [dbo].[PAYMENT] (
[INSTALLMENT_NO] int NOT NULL ,
[ORDER_ID] int NOT NULL ,
[CUSTOMER_CODE] nvarchar(20) NULL ,
[CONTRACT_NO] nvarchar(15) NOT NULL ,
[CODE_COLLECTOR] varchar(12) NULL ,
[TOTAL_BAYAR] int NULL ,
[NO_KWITANSI] int NULL ,
[NO_LPK] nvarchar(15) NULL ,
[TANGGAL_TRANSAKSI] datetime NULL ,
[TANGGAL_BAYAR] datetime NULL ,
[STATUS] varchar(3) NULL ,
[CREATE_USER] varchar(10) NULL ,
[CREATED_DATE] datetime NULL ,
[MODIFY_USER] varchar(10) NULL ,
[MODIFY_DATE] datetime NULL ,
[NOTES] varchar(1000) NULL 
)


GO

-- ----------------------------
-- Table structure for REASSIGN_COLLECTOR
-- ----------------------------
DROP TABLE [dbo].[REASSIGN_COLLECTOR]
GO
CREATE TABLE [dbo].[REASSIGN_COLLECTOR] (
[COLLECTOR_CODE] varchar(12) NOT NULL ,
[SEQ_NO] int NOT NULL ,
[REASSIGN_COLLECTOR_CODE] varchar(12) NULL ,
[PERIOD_FROM] date NULL ,
[PERIOD_TO] date NULL ,
[NOTES] varchar(255) NULL ,
[ORDER_ID] int NULL 
)


GO

-- ----------------------------
-- Table structure for REGION_MST
-- ----------------------------
DROP TABLE [dbo].[REGION_MST]
GO
CREATE TABLE [dbo].[REGION_MST] (
[KODE_REGION] varchar(10) NOT NULL ,
[NAMA_REGION] varchar(70) NULL ,
[NOTES] varchar(100) NULL ,
[AKTIF_STATUS] int NULL ,
[CREATE_USER] varchar(10) NULL ,
[CREATE_DATE] datetime NULL ,
[MODIFY_USER] varchar(10) NULL ,
[MODIFY_DATE] datetime NULL 
)


GO

-- ----------------------------
-- Table structure for TOKEN_MST
-- ----------------------------
DROP TABLE [dbo].[TOKEN_MST]
GO
CREATE TABLE [dbo].[TOKEN_MST] (
[TOKEN] varchar(32) NOT NULL ,
[TOKEN_DATE] datetime NULL ,
[USER_ID] varchar(10) NULL 
)


GO

-- ----------------------------
-- Table structure for UPDATE_KONSUMEN
-- ----------------------------
DROP TABLE [dbo].[UPDATE_KONSUMEN]
GO
CREATE TABLE [dbo].[UPDATE_KONSUMEN] (
[INSTALLMENT_NO] int NOT NULL ,
[ORDER_ID] int NOT NULL ,
[CUSTOMER_CODE] nvarchar(20) NOT NULL ,
[CONTRACT_NO] nvarchar(15) NULL ,
[NAMA_KONSUMEN] nvarchar(100) NULL ,
[ALAMAT] nvarchar(1000) NULL ,
[RT] nvarchar(3) NULL ,
[RW] nvarchar(3) NULL ,
[KELURAHAN] nvarchar(100) NULL ,
[KECAMATAN] nvarchar(100) NULL ,
[KODE_PROVINSI] varchar(5) NULL ,
[STATUS] varchar(3) NULL 
)


GO

-- ----------------------------
-- Table structure for USER_PROFILE
-- ----------------------------
DROP TABLE [dbo].[USER_PROFILE]
GO
CREATE TABLE [dbo].[USER_PROFILE] (
[USER_ID] varchar(10) NOT NULL ,
[KODE_CABANG] varchar(10) NULL ,
[KODE_REGION] varchar(10) NULL ,
[KODE_KOLEKTOR] varchar(12) NULL ,
[KODE_KARYAWAN] varchar(10) NULL ,
[NAMA_KARYAWAN] varchar(100) NULL ,
[AUTHORITY_ID] int NULL ,
[IMEI_NUMBER] varchar(20) NULL ,
[PHONE_NUMBER] varchar(15) NULL ,
[TANGGAL_MULAI] datetime NULL ,
[TANGGAL_AKHIR] datetime NULL ,
[AKTIF_STATUS] int NULL ,
[PASSWORD] varchar(75) NULL ,
[CREATE_USER] varchar(10) NULL ,
[CREATE_DATE] datetime NULL ,
[MODIFY_USER] varchar(10) NULL ,
[MODIFY_DATE] datetime NULL ,
[KODE_REGIONAL] varchar(255) NULL 
)


GO

-- ----------------------------
-- Indexes structure for table AUTHORITY_LIST
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table AUTHORITY_LIST
-- ----------------------------
ALTER TABLE [dbo].[AUTHORITY_LIST] ADD PRIMARY KEY ([ID])
GO

-- ----------------------------
-- Indexes structure for table AUTHORITY_MST
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table AUTHORITY_MST
-- ----------------------------
ALTER TABLE [dbo].[AUTHORITY_MST] ADD PRIMARY KEY ([AUTHORITY_ID])
GO

-- ----------------------------
-- Indexes structure for table CABANG_MST
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table CABANG_MST
-- ----------------------------
ALTER TABLE [dbo].[CABANG_MST] ADD PRIMARY KEY ([KODE_CABANG])
GO

-- ----------------------------
-- Indexes structure for table COLLECTION_ACTIVITY
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table COLLECTION_ACTIVITY
-- ----------------------------
ALTER TABLE [dbo].[COLLECTION_ACTIVITY] ADD PRIMARY KEY ([ACTIVITY_ID])
GO

-- ----------------------------
-- Indexes structure for table COLLECTION_DETAIL
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table COLLECTION_DETAIL
-- ----------------------------
ALTER TABLE [dbo].[COLLECTION_DETAIL] ADD PRIMARY KEY ([ORDER_ID], [CONTRACT_NO], [INSTALLMENT_NO])
GO

-- ----------------------------
-- Indexes structure for table COLLECTION_HEADER
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table COLLECTION_HEADER
-- ----------------------------
ALTER TABLE [dbo].[COLLECTION_HEADER] ADD PRIMARY KEY ([ORDER_ID])
GO

-- ----------------------------
-- Indexes structure for table COLLECTION_LOG
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table COLLECTION_LOG
-- ----------------------------
ALTER TABLE [dbo].[COLLECTION_LOG] ADD PRIMARY KEY ([ORDER_ID], [ACTIVITY_DATE])
GO

-- ----------------------------
-- Indexes structure for table FORM_FIELDS
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table FORM_FIELDS
-- ----------------------------
ALTER TABLE [dbo].[FORM_FIELDS] ADD PRIMARY KEY ([field_id])
GO

-- ----------------------------
-- Indexes structure for table FORM_ROWS
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table FORM_ROWS
-- ----------------------------
ALTER TABLE [dbo].[FORM_ROWS] ADD PRIMARY KEY ([row_id])
GO

-- ----------------------------
-- Indexes structure for table FORMS
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table FORMS
-- ----------------------------
ALTER TABLE [dbo].[FORMS] ADD PRIMARY KEY ([form_id])
GO

-- ----------------------------
-- Indexes structure for table HANDLING
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table HANDLING
-- ----------------------------
ALTER TABLE [dbo].[HANDLING] ADD PRIMARY KEY ([BATCH_HANDLING], [CONTRACT_NO], [INSTALLMENT_NO], [ORDER_ID])
GO

-- ----------------------------
-- Indexes structure for table LOGIN_LOGS
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table LOGIN_LOGS
-- ----------------------------
ALTER TABLE [dbo].[LOGIN_LOGS] ADD PRIMARY KEY ([USER_ID], [LOG_DATE])
GO

-- ----------------------------
-- Indexes structure for table MASTER_HANDLING
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table MASTER_HANDLING
-- ----------------------------
ALTER TABLE [dbo].[MASTER_HANDLING] ADD PRIMARY KEY ([BATCH_HANDLING_NO])
GO

-- ----------------------------
-- Indexes structure for table MASTER_PROVINSI
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table MASTER_PROVINSI
-- ----------------------------
ALTER TABLE [dbo].[MASTER_PROVINSI] ADD PRIMARY KEY ([KODE_PROVINSI])
GO

-- ----------------------------
-- Indexes structure for table MENU_MST
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table MENU_MST
-- ----------------------------
ALTER TABLE [dbo].[MENU_MST] ADD PRIMARY KEY ([MENU])
GO

-- ----------------------------
-- Indexes structure for table MOBILE_TRACKING
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table MOBILE_TRACKING
-- ----------------------------
ALTER TABLE [dbo].[MOBILE_TRACKING] ADD PRIMARY KEY ([USER_ID], [TRACKING_DATE])
GO

-- ----------------------------
-- Indexes structure for table PARAM_MST
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table PARAM_MST
-- ----------------------------
ALTER TABLE [dbo].[PARAM_MST] ADD PRIMARY KEY ([PARAM_ID])
GO

-- ----------------------------
-- Indexes structure for table PAYMENT
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table PAYMENT
-- ----------------------------
ALTER TABLE [dbo].[PAYMENT] ADD PRIMARY KEY ([INSTALLMENT_NO], [ORDER_ID], [CONTRACT_NO])
GO

-- ----------------------------
-- Indexes structure for table REASSIGN_COLLECTOR
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table REASSIGN_COLLECTOR
-- ----------------------------
ALTER TABLE [dbo].[REASSIGN_COLLECTOR] ADD PRIMARY KEY ([COLLECTOR_CODE], [SEQ_NO])
GO

-- ----------------------------
-- Indexes structure for table REGION_MST
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table REGION_MST
-- ----------------------------
ALTER TABLE [dbo].[REGION_MST] ADD PRIMARY KEY ([KODE_REGION])
GO

-- ----------------------------
-- Indexes structure for table TOKEN_MST
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table TOKEN_MST
-- ----------------------------
ALTER TABLE [dbo].[TOKEN_MST] ADD PRIMARY KEY ([TOKEN])
GO

-- ----------------------------
-- Indexes structure for table UPDATE_KONSUMEN
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table UPDATE_KONSUMEN
-- ----------------------------
ALTER TABLE [dbo].[UPDATE_KONSUMEN] ADD PRIMARY KEY ([INSTALLMENT_NO], [ORDER_ID], [CUSTOMER_CODE])
GO

-- ----------------------------
-- Indexes structure for table USER_PROFILE
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table USER_PROFILE
-- ----------------------------
ALTER TABLE [dbo].[USER_PROFILE] ADD PRIMARY KEY ([USER_ID])
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[AUTHORITY_LIST]
-- ----------------------------
ALTER TABLE [dbo].[AUTHORITY_LIST] ADD FOREIGN KEY ([AUTHORITY_ID]) REFERENCES [dbo].[AUTHORITY_MST] ([AUTHORITY_ID]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[AUTHORITY_LIST] ADD FOREIGN KEY ([MENU]) REFERENCES [dbo].[MENU_MST] ([MENU]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[CABANG_MST]
-- ----------------------------
ALTER TABLE [dbo].[CABANG_MST] ADD FOREIGN KEY ([KODE_REGION]) REFERENCES [dbo].[REGION_MST] ([KODE_REGION]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[COLLECTION_DETAIL]
-- ----------------------------
ALTER TABLE [dbo].[COLLECTION_DETAIL] ADD FOREIGN KEY ([ORDER_ID]) REFERENCES [dbo].[COLLECTION_HEADER] ([ORDER_ID]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[COLLECTION_DETAIL] ADD FOREIGN KEY ([KODE_PROVINSI]) REFERENCES [dbo].[MASTER_PROVINSI] ([KODE_PROVINSI]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[FORM_FIELDS]
-- ----------------------------
ALTER TABLE [dbo].[FORM_FIELDS] ADD FOREIGN KEY ([row_id]) REFERENCES [dbo].[FORM_ROWS] ([row_id]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[FORM_ROWS]
-- ----------------------------
ALTER TABLE [dbo].[FORM_ROWS] ADD FOREIGN KEY ([form_id]) REFERENCES [dbo].[FORMS] ([form_id]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[TOKEN_MST]
-- ----------------------------
ALTER TABLE [dbo].[TOKEN_MST] ADD FOREIGN KEY ([USER_ID]) REFERENCES [dbo].[USER_PROFILE] ([USER_ID]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[UPDATE_KONSUMEN]
-- ----------------------------
ALTER TABLE [dbo].[UPDATE_KONSUMEN] ADD FOREIGN KEY ([KODE_PROVINSI]) REFERENCES [dbo].[MASTER_PROVINSI] ([KODE_PROVINSI]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[USER_PROFILE]
-- ----------------------------
ALTER TABLE [dbo].[USER_PROFILE] ADD FOREIGN KEY ([KODE_CABANG]) REFERENCES [dbo].[CABANG_MST] ([KODE_CABANG]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[USER_PROFILE] ADD FOREIGN KEY ([KODE_REGION]) REFERENCES [dbo].[REGION_MST] ([KODE_REGION]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[USER_PROFILE] ADD FOREIGN KEY ([AUTHORITY_ID]) REFERENCES [dbo].[AUTHORITY_MST] ([AUTHORITY_ID]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
