package com.indocyber.lib;

import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Schema;

public class generator {
    public static void main(String[] args) {
        Schema schema=new Schema(1,"com.indocyber.ifgcol.db");
        schema.enableKeepSectionsByDefault();
        addTables(schema);
        try {
            new DaoGenerator().generateAll(schema,"../app/src/main/java");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private static void addTables(final Schema schema) {
        addTrackingTable(schema);
        addLoginAuditTable(schema);
        addOrderHeaderTable(schema);
        addNotifTable(schema);
        addParamsTable(schema);
        addFormTable(schema);
        addHandlingTable(schema);
        addProvinsi(schema);
    }
    private static Entity addProvinsi(final  Schema schema){
        Entity provinsi = schema.addEntity("provinsi");
        provinsi.addStringProperty("kode_provinsi");
        provinsi.addStringProperty("nama_provinsi");
        return provinsi;
    }
    private static Entity addHandlingTable(final  Schema schema){
        Entity handling = schema.addEntity("handling");
        handling.addLongProperty("handling").primaryKey();
        handling.addStringProperty("handling_code");
        handling.addStringProperty("deskripsi");
        return handling;
    }
    private static Entity addTrackingTable(final Schema schema) {
        Entity tracking = schema.addEntity("tracking");
        tracking.addLongProperty("tracking_id").primaryKey().autoincrement().notNull();
        tracking.addStringProperty("tracking_date");
        tracking.addStringProperty("position");
        tracking.addStringProperty("user_id");
        return tracking;
    }
    private static Entity addParamsTable(final Schema schema) {
        Entity params = schema.addEntity("params");
        params.addStringProperty("param_id").primaryKey().notNull();
        params.addStringProperty("param_condition");
        params.addStringProperty("param_description");
        params.addStringProperty("parent_id");
        params.addLongProperty("order");
        return params;
    }
    private static Entity addFormTable(Schema schema){
        Entity forms=schema.addEntity("forms");
        forms.addStringProperty("form_id").primaryKey().notNull();
        forms.addStringProperty("form_name");
        forms.addStringProperty("form_label");
        forms.addIntProperty("form_order");
        Entity rows=addRowTable(schema);
        forms.addToMany(rows,rows.getProperties().get(2));
        return forms;
    }
    private static Entity addRowTable(Schema schema){
        Entity rows=schema.addEntity("rows");
        rows.addStringProperty("row_id").primaryKey().notNull();
        rows.addIntProperty("row_order");
        rows.addStringProperty("row_visibility");
        rows.addStringProperty("form_id");
        Entity field=addFieldTable(schema);
        rows.addToMany(field,field.getProperties().get(9));
        return rows;
    }
    private static Entity addFieldTable(Schema schema){
        Entity rows=schema.addEntity("fields");
        rows.addStringProperty("field_id").primaryKey().notNull();
        rows.addStringProperty("field_name");
        rows.addStringProperty("field_type");
        rows.addStringProperty("field_visibility");
        rows.addStringProperty("field_label");
        rows.addStringProperty("field_global_value");
        rows.addStringProperty("field_weight");
        rows.addStringProperty("field_store");
        rows.addStringProperty("field_extra");
        rows.addStringProperty("row_id");
        Entity events=addEventTable(schema);
        rows.addToMany(events,events.getProperties().get(3));
        return rows;
    }
    private static Entity addEventTable(Schema schema){
        Entity events=schema.addEntity("events");
        events.addStringProperty("event_id").primaryKey().notNull();
        events.addStringProperty("event_type");
        events.addStringProperty("event_component");
        events.addStringProperty("event_component_target");
        events.addStringProperty("event_data");
        events.addStringProperty("form_id");
        return events;
    }
    private static Entity addNotifTable(final Schema schema) {
        Entity notif = schema.addEntity("notifications");
        notif.addLongProperty("notif_id").primaryKey().autoincrement().notNull();
        notif.addStringProperty("title");
        notif.addStringProperty("message");
        notif.addDateProperty("date");
        notif.addBooleanProperty("is_read");
        notif.addStringProperty("user_id");
        return notif;
    }
    private static Entity addLoginAuditTable(final Schema schema) {
        Entity log = schema.addEntity("login_logs");
        log.addLongProperty("log_id").primaryKey().autoincrement().notNull();
        log.addStringProperty("login_date");
        log.addStringProperty("login");
        log.addStringProperty("logout");
        log.addStringProperty("user_id");
        return log;
    }
    private static Entity addOrderHeaderTable(final Schema schema){
        Entity order = schema.addEntity("order_list");
        order.addStringProperty("order_id").primaryKey().notNull();
        order.addStringProperty("lpk_no");
        order.addDateProperty("lpk_date");
        order.addStringProperty("customer_code");
        order.addStringProperty("customer_name");
        order.addStringProperty("address");
        order.addStringProperty("village");
        order.addStringProperty("district");
        order.addStringProperty("city");
        order.addStringProperty("rt");
        order.addStringProperty("rw");
        order.addStringProperty("kode_provinsi");
        order.addStringProperty("post_code");
        order.addStringProperty("phone_area");
        order.addStringProperty("phone_number");
        order.addStringProperty("mobile_prefix");
        order.addStringProperty("mobile_no");
        order.addStringProperty("status");
        order.addBooleanProperty("enabled");
        order.addDateProperty("created");
        order.addStringProperty("user_id");
        order.addStringProperty("data");
        Entity order_detail=addOrderDetailTable(schema);
        order.addToMany(order_detail,order_detail.getProperties().get(1));
        Entity activity=addActivityTable(schema);
        order.addToMany(activity,activity.getProperties().get(1));
        return order;
    }
    private static Entity addOrderDetailTable(final Schema schema) {
        Entity order_detail = schema.addEntity("order_detail");
        order_detail.addLongProperty("detail_id").primaryKey();
        order_detail.addStringProperty("order_id");
        order_detail.addStringProperty("contract_no");
        order_detail.addStringProperty("installment_no");
        order_detail.addStringProperty("lpk_no");
        order_detail.addStringProperty("nama_barang");
        order_detail.addDateProperty("due_date");
        order_detail.addStringProperty("installment_amt");
        order_detail.addStringProperty("penalty_amt");
        order_detail.addStringProperty("collection_fee");
        order_detail.addStringProperty("total_amt");
        order_detail.addStringProperty("receipt_no");
        order_detail.addStringProperty("min_payment");
        order_detail.addStringProperty("max_payment");
        order_detail.addDateProperty("repayment_date");
        order_detail.addStringProperty("notes");
        order_detail.addIntProperty("promise_count");
        order_detail.addIntProperty("promise_available");
        order_detail.addStringProperty("promise_range");
        order_detail.addStringProperty("deposit_amt");
        order_detail.addStringProperty("data");
        order_detail.addStringProperty("status");
        order_detail.addBooleanProperty("enabled");
        order_detail.addStringProperty("user_id");
        order_detail.addStringProperty("activity_id");
        return order_detail;
    }
    private static Entity addActivityTable(final Schema schema) {
        Entity activity = schema.addEntity("activities");
        activity.addStringProperty("activity_id").primaryKey().notNull();
        activity.addStringProperty("order_id").notNull();
        activity.addStringProperty("customer_name");
        activity.addStringProperty("address");
        activity.addStringProperty("body");
        activity.addStringProperty("send_status");
        activity.addStringProperty("mobile_status");
        activity.addStringProperty("user_id");
        activity.addDateProperty("created_date");
        activity.addDateProperty("updated_date");
        return activity;
    }

}
