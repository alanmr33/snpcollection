package com.clumsiam.engine;

import java.util.HashMap;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

public abstract class PoseideonEngine {
    public static boolean loadScript(HashMap<String,String> data,
                                     String script){
        try {
            ScriptEngineManager manager = new ScriptEngineManager();
            ScriptEngine engine = manager.getEngineByName("JavaScript");
            String functionScript = "function runScript(data) { " +
                    "" +script+
                    " }";
            // evaluate script
            engine.eval(functionScript);
            Invocable inv = (Invocable) engine;
            return (boolean) inv.invokeFunction("runScript", data );
        } catch (Exception e) {
            e.printStackTrace();
            return  false;
        }
    }
}
