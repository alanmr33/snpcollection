package com.indocyber.ifgcol.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.clumsiam.uigenerator.utils.Formating;
import com.indocyber.ifgcol.activity.AppActivity;
import com.indocyber.ifgcol.adapter.holder.SelectToPayHolder;
import com.indocyber.ifgcol.db.order_detail;

import java.util.List;

/**
 * Created by Indocyber on 29/03/2018.
 */

public class ToPayAdapter extends RecyclerView.Adapter<SelectToPayHolder> {
    private AppActivity activity;
    private List<order_detail> order_details;
    private int layout;
    public ToPayAdapter(List<order_detail> order_details,AppActivity activity,int layout){
        this.activity=activity;
        this.order_details=order_details;
        this.layout=layout;
    }
    @Override
    public SelectToPayHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(this.layout, parent, false);
        return new SelectToPayHolder(v);
    }

    @Override
    public void onBindViewHolder(final SelectToPayHolder holder, int position) {
        final order_detail order_detail=order_details.get(position);
        holder.textViewContractNo.setText(order_detail.getContract_no());
        holder.textViewNamaBarang.setText(order_detail.getNama_barang().toUpperCase());
        holder.textViewInstallmentNo.setText(order_detail.getInstallment_no());
        holder.textViewInstallmentAMT.setText("Rp. "+ Formating.currency(Integer.parseInt(order_detail.getTotal_amt())));
        holder.textViewInstallmentAMT.setText("Rp. "+ Formating.currency(Integer.parseInt(order_detail.getInstallment_amt())));
        holder.txtViewPenalty.setText("Rp. "+ Formating.currency(Integer.parseInt(order_detail.getPenalty_amt())));
        holder.txtViewCF.setText("Rp. "+ Formating.currency(Integer.parseInt(order_detail.getCollection_fee())));
        holder.txtViewTotal.setText("Rp. "+ Formating.currency(Integer.parseInt(order_detail.getTotal_amt())));
    }

    @Override
    public int getItemCount() {
        return order_details.size();
    }
}
