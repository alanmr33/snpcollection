package com.indocyber.ifgcol.library;

import android.app.Activity;
import android.util.Log;

import java.io.File;
import java.io.IOException;

/**
 * Created by Indocyber on 07/05/2018.
 */
public abstract class FileCreator {
    public static final File createImages(Activity activity, String fileName, String orderID) {
        File storageDir = activity.getExternalFilesDir("images/" + orderID.replace("/", "_"));
        File file = null;
        try {
            if (!storageDir.exists()) {
                storageDir.mkdirs();
            }
            file = new File(storageDir.getAbsolutePath() + "/" + fileName + ".cam");
            if (!file.exists()) {
                file.createNewFile();
            }
        } catch (IOException e) {
            Log.e("CLUMSIA-FC",e.getMessage());
        }
        return file;
    }
}