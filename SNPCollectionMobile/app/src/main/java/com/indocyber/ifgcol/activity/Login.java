package com.indocyber.ifgcol.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.ifgcol.App;
import com.indocyber.ifgcol.UI.MD5;
import com.indocyber.ifgcol.db.login_logs;
import com.indocyber.ifgcol.db.login_logsDao;
import com.indocyber.ifgcol.R;
import com.indocyber.ifgcol.library.AppSettings;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.message.BasicHeader;

public class Login extends AppActivity {
    private EditText txtUserID,txtPassword;
    private Button btnLogin;
    private TextView txtError;
    private CheckBox chkShowPassword;
    private ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        txtUserID= (EditText) findViewById(R.id.editTextLoginUserID);
        txtPassword= (EditText) findViewById(R.id.editTextLoginPassword);
        txtError= (TextView) findViewById(R.id.textViewError);
        chkShowPassword= (CheckBox) findViewById(R.id.checkBoxShow);
        btnLogin= (Button) findViewById(R.id.buttonLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               String userid=txtUserID.getText().toString();
               String password=txtPassword.getText().toString();
               if(userid.equalsIgnoreCase("")){
                   txtError.setVisibility(View.VISIBLE);
                   txtError.setText("User Id harus diisi");
               }else if(password.equalsIgnoreCase("")){
                   txtError.setVisibility(View.VISIBLE);
                   txtError.setText("Password harus diisi");
               }else{
                   doLogin(userid,password);
               }
            }
        });
        chkShowPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    txtPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                }else{
                    txtPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    txtPassword.setSelection(txtPassword.length());
                }
            }
        });
        dialog=new ProgressDialog(this);
    }
    private void doLogin(String userid,String password){
        dialog.setMessage("Memeriksa data pengguna");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(false);
        dialog.show();
        Header[] headers=new Header[1];
        headers[0]=new BasicHeader("APPID","IFGCOL-IGLO");
        RequestParams params=new RequestParams();
        params.put("userid",userid);
        params.put("password", MD5.enc(password));
        params.put("device",getApp().getSession().getDevice());
        getAPIClient().post(this, AppSettings.LOGIN_URL,headers,params,"application/x-www-form-urlencoded",new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    dialog.dismiss();
                    if(response.getBoolean("status")){
                        getApp().getSession().setUserData(response.getJSONArray("data")
                                .getJSONObject(0));
                        getApp().getSession().setToken(response.getJSONArray("data")
                                .getJSONObject(0)
                                .getString("token"));
                        SimpleDateFormat dateFormat=new SimpleDateFormat("dd-MM-yyyy");
                        getApp().getSession().setLastLogin(dateFormat.format(Calendar.getInstance().getTime()));
                        getApp().getSession().setLogin(true);
                        getApp().getSession().setSynced(true);

                        login_logsDao login_logsDao=((App)getApplication()).getDbSession().getLogin_logsDao();
                        login_logs newLogs=new login_logs();
                        newLogs.setLog_id(Calendar.getInstance().getTimeInMillis());
                        SimpleDateFormat dateFormatA=new SimpleDateFormat("yyyy-MM-dd");
                        newLogs.setLogin_date(dateFormatA.format(Calendar.getInstance().getTime()));
                        SimpleDateFormat dateFormatB=new SimpleDateFormat("HH:mm:ss");
                        newLogs.setLogin(dateFormatB.format(Calendar.getInstance().getTime()));
                        try {
                            newLogs.setUser_id(getApp().getSession().getUserdata().getString("userID"));
                        } catch (JSONException e) {
                            FirebaseCrash.log(e.getMessage());
                        }
                        login_logsDao.insert(newLogs);
                        Intent intent=new Intent(Login.this,Splash.class);
                        startActivity(intent);
                        finish();
                    }else{
                        txtError.setVisibility(View.VISIBLE);
                        txtError.setText(response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] header, String body, Throwable throwable){
                dialog.dismiss();
                AlertDialog.Builder builder= new AlertDialog.Builder(Login.this);
                builder.setMessage("Terjadi Kesalahan : (Status Code "+statusCode+")");
                builder.setCancelable(false);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();
                Log.e("Error",AppSettings.SYNC_URL+" Response "+statusCode);
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject error){
                dialog.dismiss();
                AlertDialog.Builder builder= new AlertDialog.Builder(Login.this);
                builder.setMessage("Terjadi Kesalahan : (Status Code "+statusCode+")");
                builder.setCancelable(false);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();
                Log.e("Error",AppSettings.SYNC_URL+" Response "+statusCode);
            }
        });
    }
}
