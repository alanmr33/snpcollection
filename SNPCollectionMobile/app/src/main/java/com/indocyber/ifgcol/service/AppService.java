package com.indocyber.ifgcol.service;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.clumsiam.uigenerator.utils.Base64;
import com.clumsiam.uigenerator.utils.Resizer;
import com.clumsiam.uigenerator.utils.Session;
import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.ifgcol.App;
import com.indocyber.ifgcol.UI.MD5;
import com.indocyber.ifgcol.db.activities;
import com.indocyber.ifgcol.db.activitiesDao;
import com.indocyber.ifgcol.db.login_logs;
import com.indocyber.ifgcol.db.login_logsDao;
import com.indocyber.ifgcol.db.order_detailDao;
import com.indocyber.ifgcol.db.order_list;
import com.indocyber.ifgcol.db.order_detail;
import com.indocyber.ifgcol.db.order_listDao;
import com.indocyber.ifgcol.db.tracking;
import com.indocyber.ifgcol.db.trackingDao;
import com.indocyber.ifgcol.library.AppSettings;
import com.indocyber.ifgcol.library.Utils;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.greenrobot.greendao.query.Query;
import org.greenrobot.greendao.query.QueryBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class AppService extends Service {
    private AppServiceBinder mBinder=new AppServiceBinder();
    private AsyncHttpClient httpClient=new AsyncHttpClient();
    private trackingDao trackingDao;
    private activitiesDao activitiesDao;
    private login_logsDao login_logsDao;
    private com.indocyber.ifgcol.db.order_listDao order_listDao;
    private com.indocyber.ifgcol.db.order_detailDao order_detailDao;
    private Session session;
    public AppService() {
    }
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }
    public class AppServiceBinder extends Binder {
        public AppService getService(){
            return AppService.this;
        }
    }
    @Override
    public void onCreate() {
        super.onCreate();
        trackingDao=((App)getApplication()).getDbSession().getTrackingDao();
        order_listDao=((App)getApplication()).getDbSession().getOrder_listDao();
        order_detailDao=((App)getApplication()).getDbSession().getOrder_detailDao();
        activitiesDao=((App)getApplication()).getDbSession().getActivitiesDao();
        login_logsDao=((App)getApplication()).getDbSession().getLogin_logsDao();
        session=((App)getApplication()).getSession();
        sendtData(true);
        sendLog(false);
        getData(true);
        cleanData();
    }
    public void sendtData(final boolean timeout){
        final int delay=(timeout)? AppSettings.SUBMIT_TIME : 100;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (session.isLogin() && !session.isBlocked()) {
                    new SendTask().doInBackground("");
                }
                if(timeout){
                    sendtData(true);
                }
            }
        },delay);
    }
    public void sendTracking(){
        if (session.isLogin()  && !session.isBlocked()) {
            Log.i("TRACK","Send Tracking Data");
            RequestParams params = new RequestParams();

            trackingDao = ((App)getApplication()).getDbSession().getTrackingDao();
            final Query<tracking> query;
            try {
                query = trackingDao.queryBuilder()
                        .where(com.indocyber.ifgcol.db.trackingDao.Properties.User_id
                                .eq(session.getUserdata().getString("user_id")))
                        .build();
                final List<tracking> locsList = query.list();
                JSONArray locsArray = new JSONArray();

                if (locsList != null && locsList.size()>0) {
                    for (int i = 0; i < locsList.size(); i++) {
                        JSONObject locsObject = new JSONObject();
                        try {
                            locsObject.put("date", locsList.get(i).getTracking_date());
                            locsObject.put("location", locsList.get(i).getPosition());
                            locsArray.put(i, locsObject);
                        } catch (JSONException e) {
                            FirebaseCrash.log(e.getMessage());
                        }
                    }
                    params.put("tracking_data", locsArray.toString());
                    params.put("token", session.getToken());
                    httpClient.addHeader("APPID", "IFGCOL-IGLO");
                    httpClient.post(AppSettings.TRACK_URL, params, new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            Log.i("TRACK", "SUCCESS LOCATION TRACK");
                            for (int i = 0; i < locsList.size(); i++) {
                                trackingDao.delete(locsList.get(i));
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            try {
                                FirebaseCrash.log(statusCode+" FAILED UPLOAD TRACKING FOR USER ");
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.e("TRACK",error.getMessage());
                                FirebaseCrash.log(e.getMessage());
                            }
                        }
                    });
                }
            } catch (Exception e) {
                FirebaseCrash.log(e.getMessage());
            }
        }
    }
    public void sendLog(boolean timeout){
        int delay=(timeout)? AppSettings.LOG_TIME : 100;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(session.isLogin() && !session.isBlocked()){
                    RequestParams params = new RequestParams();

                    final Query<login_logs> query;
                    try {

                        query = login_logsDao.queryBuilder()
                                .where(com.indocyber.ifgcol.db.login_logsDao.Properties.User_id.eq(session.getUserdata().getString("user_id")))
                                .where(com.indocyber.ifgcol.db.login_logsDao.Properties.Login_date.lt(Utils.dateYesterday()))
                                .build();
                        final List<login_logs> logList = query.list();
                        JSONArray logArray = new JSONArray();

                        for (int i = 0; i < logList.size(); i++) {
                            JSONObject logObject = new JSONObject();
                            try {
                                logObject.put("date", logList.get(i).getLogin_date());
                                logObject.put("login", logList.get(i).getLogin());
                                logObject.put("logout", logList.get(i).getLogout());
                                logArray.put(i, logObject);
                            } catch (JSONException e) {
                                FirebaseCrash.log(e.getMessage());
                            }
                        }
                        params.put("logs", logArray.toString());
                        params.put("token", session.getToken());
                        httpClient.addHeader("APPID", "IFGCOL-IGLO");
                        httpClient.post(AppSettings.LOG_URL, params, new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                for (int i = 0; i < logList.size(); i++) {
                                    login_logsDao.delete(logList.get(i));
                                }
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                try {
                                    FirebaseCrash.log(statusCode+" LOGIN LOGS FOR USER "+session.getUserdata().getString("userID"));
                                } catch (Exception e) {
                                    FirebaseCrash.log(e.getMessage());
                                }
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();;
                        FirebaseCrash.log(e.getMessage());
                    }
                }
                sendLog(true);
            }
        },delay);
    }
    public void cleanData(){
        Query<activities> query = activitiesDao.queryBuilder()
                .where(com.indocyber.ifgcol.db.activitiesDao.Properties.Send_status.eq("MENGIRIM"))
                .build();
        List<activities> result = query.list();
        for (int i = 0; i < result.size(); i++) {
            result.get(i).setSend_status("ANTRIAN");
            result.get(i).setMobile_status("PENDING");
        }
        activitiesDao.updateInTx(result);
        Calendar calendar=Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH,-1);
        QueryBuilder<order_list> orderListQuery=order_listDao.queryBuilder()
                .where(com.indocyber.ifgcol.db.order_listDao.Properties.Created.lt(calendar.getTime()));
        List<order_list> order_lists=orderListQuery.list();
        for (int i=0;i<order_lists.size();i++){
            QueryBuilder<order_detail> orderDetailQuery=order_detailDao.queryBuilder()
                    .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Order_id.eq(order_lists.get(i).getOrder_id()));
            List<order_detail> order_details=orderDetailQuery.list();
            for (int a=0;a<order_details.size();a++){
                order_detailDao.delete(order_details.get(a));
            }
            QueryBuilder<activities> activitiesQuery=activitiesDao.queryBuilder()
                    .where(com.indocyber.ifgcol.db.activitiesDao.Properties.Order_id.eq(order_lists.get(i).getOrder_id()));
            List<activities> activities=activitiesQuery.list();
            for (int a=0;a<activities.size();a++){
                activitiesDao.delete(activities.get(a));
            }
            File f=getExternalFilesDir("images/" + order_lists.get(i).getOrder_id().replace("/", "_"));
            if(f.exists()){
                f.delete();
            }
            order_listDao.delete(order_lists.get(i));
        }
    }
    public void getData(boolean timeout){
        if(timeout){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(session.isLogin()){
                        doGet(true);
                    }else {
                        getData(true);
                    }
                }
            }, AppSettings.GET_TIME);
        }else{
            doGet(false);
        }
    }
    private void doGet(final boolean loop){
        RequestParams params = new RequestParams();
        params.put("token", ((App)getApplication()).getSession().getToken());
        httpClient.addHeader("APPID", "IFGCOL-IGLO");
        httpClient.get(AppSettings.ORDER_URL,params,new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    if(response.getBoolean("status")){
                        new UpdateDB().doInBackground(response.getJSONArray("data").toString());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if(loop){
                    getData(true);
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] header, String body, Throwable throwable){
                Log.e("Error",statusCode+" error");
                getData(true);
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject error){
                Log.e("Error",statusCode+" error");
                if(loop){
                    getData(true);
                }
            }
        });
    }
    public class SendTask extends AsyncTask<String,Long,Long>{

        @Override
        protected Long doInBackground(String... strings) {
            RequestParams params = new RequestParams();

            final Query<activities> query;
            try {
                query = activitiesDao.queryBuilder()
                        .where(com.indocyber.ifgcol.db.activitiesDao.Properties.Send_status.eq("ANTRIAN"))
                        .where(com.indocyber.ifgcol.db.activitiesDao.Properties.Mobile_status.eq("PENDING"))
                        .where(com.indocyber.ifgcol.db.activitiesDao.Properties.User_id
                                .eq(session.getUserdata().getString("user_id")))
                        .build();
                final List<activities> activitiesList = query.list();
                for (int i=0;i<activitiesList.size();i++){
                    final activities activities=activitiesList.get(i);
                    JSONObject body=new JSONObject(activities.getBody());
                    File f = new File(body.getJSONObject("input").getString("Photo-file"));
                    if ((f.length() / 1024) > 45) {
                        Resizer.compressImage(body.getJSONObject("input").getString("Photo-file"), 300);
                        f = new File(body.getJSONObject("input").getString("Photo-file"));
                    }
                    byte imageData[] = new byte[(int) f.length()];
                    try {
                        FileInputStream fis = new FileInputStream(f);
                        fis.read(imageData);
                    } catch (IOException e) {
                        FirebaseCrash.log(e.getMessage());
                    }
                    String base64 = Base64.encode(imageData);
                    body.put("image",base64);
                    params.put("order_id",activities.getOrder_id());
                    params.put("body", body);
                    params.put("token", ((App)getApplication()).getSession().getToken());
                    httpClient.addHeader("APPID", "IFGCOL-IGLO");
                    activities.setSend_status("MENGIRIM");
                    activitiesDao.update(activities);
                    Log.i("SEND",activities.getOrder_id()+" : "+activities.getActivity_id());
                    httpClient.post(AppSettings.SUBMIT_URL, params, new JsonHttpResponseHandler(){
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            try {
                                if(response.getBoolean("status")){
                                    QueryBuilder<order_detail> queryBuilder=order_detailDao.queryBuilder()
                                            .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Activity_id.eq(activities.getActivity_id()));
                                    List<order_detail> details=queryBuilder.list();
                                    for (int i=0;i<details.size();i++){
                                        details.get(i).setStatus("SENT");
                                        order_detailDao.update(details.get(i));
                                    }
                                    activities.setMobile_status("SENT");
                                    activities.setSend_status("TERKIRIM");
                                    activitiesDao.update(activities);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onFailure(int statusCode, Header[] header, String body, Throwable throwable){
                            Log.e("Error",statusCode+" error");
                            activities.setSend_status("ANTRIAN");
                            activitiesDao.update(activities);
                        }
                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject error){
                            Log.e("Error",statusCode+" error");
                            activities.setSend_status("ANTRIAN");
                            activitiesDao.update(activities);
                        }
                    });
                }
            } catch (Exception e) {
                FirebaseCrash.log(e.getMessage());
                e.printStackTrace();
            }
            return null;
        }
    }
    private class UpdateDB extends AsyncTask<String,Long,Long>{

        @Override
        protected Long doInBackground(String... strings) {
            try {
                for (int x = 0; x < strings.length; x++) {
                    JSONArray data = new JSONArray(strings[x]);
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject object = data.getJSONObject(i);
                        QueryBuilder<order_list> orderListQueryBuilder = order_listDao.queryBuilder()
                                .where(com.indocyber.ifgcol.db.order_listDao.Properties.Customer_code.eq(
                                        object.getString("customer_code")
                                ));
                        List<order_list> order_lists = orderListQueryBuilder.list();
                        if (order_lists.size() == 0) {
                            if (!object.getString("status").equalsIgnoreCase("RSN")) {
                                String orderID = MD5.enc(Calendar.getInstance().getTimeInMillis() + "ID");
                                order_list order_list = new order_list();
                                order_list.setOrder_id(orderID);
                                order_list.setLpk_no(object.getString("lpk_no"));
                                try {
                                    order_list.setLpk_date(dateFormat.parse(object.getString("lpk_date")));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                order_list.setCustomer_code(object.getString("customer_code"));
                                order_list.setCustomer_name(object.getString("customer_name"));
                                order_list.setAddress(object.getString("address"));
                                order_list.setVillage(object.getString("village"));
                                order_list.setDistrict(object.getString("district"));
                                order_list.setCity(object.getString("city"));
                                order_list.setRt(object.getString("rt"));
                                order_list.setRw(object.getString("rw"));
                                order_list.setKode_provinsi(object.getString("kode_provinsi"));
                                order_list.setPost_code(object.getString("post_code"));
                                order_list.setPhone_area(object.getString("phone_area"));
                                order_list.setPhone_number(object.getString("phone_number"));
                                order_list.setMobile_prefix(object.getString("mobile_prefix"));
                                order_list.setMobile_no(object.getString("mobile_no"));
                                order_list.setData(object.getString("data"));
                                order_list.setStatus("NEW");
                                order_list.setEnabled(true);
                                order_list.setCreated(Calendar.getInstance().getTime());
                                order_list.setUser_id(session.getUserdata().getString("user_id"));
                                order_listDao.insert(order_list);
                                JSONArray details = object.getJSONArray("details");
                                for (int a = 0; a < details.length(); a++) {
                                    JSONObject objectDetail = details.getJSONObject(a);
                                    QueryBuilder<order_detail> orderDetailQueryBuilder = order_detailDao.queryBuilder()
                                            .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Order_id.eq(
                                                    objectDetail.getString("order_id")
                                            ))
                                            .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Contract_no.eq(
                                                    objectDetail.getString("contract_no")
                                            ))
                                            .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Installment_no.eq(
                                                    objectDetail.getString("installment_no")
                                            ));
                                    List<order_detail> order_details = orderDetailQueryBuilder.list();
                                    if (order_details.size() == 0) {
                                        order_detail detail = new order_detail();
                                        detail.setDetail_id(Calendar.getInstance().getTimeInMillis());
                                        detail.setOrder_id(orderID);
                                        detail.setContract_no(objectDetail.getString("contract_no"));
                                        detail.setInstallment_no(objectDetail.getString("installment_no"));
                                        detail.setLpk_no(objectDetail.getString("lpk_no"));
                                        detail.setNama_barang(objectDetail.getString("nama_barang"));
                                        try {
                                            detail.setDue_date(dateFormat.parse(objectDetail.getString("due_date")));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        detail.setInstallment_amt(objectDetail.getString("installment_amt").replace(".00", ""));
                                        detail.setPenalty_amt(objectDetail.getString("penalty_amt").replace(".00", ""));
                                        detail.setCollection_fee(objectDetail.getString("collection_fee").replace(".00", ""));
                                        detail.setTotal_amt(objectDetail.getString("total_amt").replace(".00", ""));
                                        detail.setReceipt_no(objectDetail.getString("receipt_no"));
                                        detail.setMin_payment(objectDetail.getString("min_payment").replace(".00", ""));
                                        detail.setMax_payment(objectDetail.getString("max_payment").replace(".00", ""));
                                        try {
                                            detail.setRepayment_date(dateFormat.parse(objectDetail.getString("repayment_date")));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        detail.setNotes(objectDetail.getString("notes"));
                                        detail.setDeposit_amt(String.valueOf(objectDetail.getInt("deposit_amt")));
                                        detail.setPromise_count(objectDetail.getInt("promise_count"));
                                        detail.setPromise_available(objectDetail.getInt("promise_available"));
                                        detail.setPromise_range(objectDetail.getString("promise_range"));
                                        detail.setStatus("NEW");
                                        detail.setEnabled(true);
                                        order_detailDao.insert(detail);
                                    }
                                }
                            }
                        } else {
                            if (object.getString("status").equalsIgnoreCase("RSN")) {
                                QueryBuilder<order_detail> orderDetailQuery = order_detailDao.queryBuilder()
                                        .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Order_id.eq(order_lists.get(0).getOrder_id()));
                                List<order_detail> order_details = orderDetailQuery.list();
                                for (int a = 0; a < order_details.size(); a++) {
                                    order_detailDao.delete(order_details.get(a));
                                }
                                QueryBuilder<activities> activitiesQuery = activitiesDao.queryBuilder()
                                        .where(com.indocyber.ifgcol.db.activitiesDao.Properties.Order_id.eq(order_lists.get(0).getOrder_id()));
                                List<activities> activities = activitiesQuery.list();
                                for (int a = 0; a < activities.size(); a++) {
                                    activitiesDao.delete(activities.get(a));
                                }
                                File f = getExternalFilesDir("images/" + order_lists.get(0).getOrder_id().replace("/", "_"));
                                if (f.exists()) {
                                    f.delete();
                                }
                                order_listDao.delete(order_lists.get(0));
                            } else {
                                JSONArray details = object.getJSONArray("details");
                                for (int a = 0; a < details.length(); a++) {
                                    JSONObject objectDetail = details.getJSONObject(a);
                                    QueryBuilder<order_detail> orderDetailQueryBuilder = order_detailDao.queryBuilder()
                                            .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Order_id.eq(
                                                    objectDetail.getString("order_id")
                                            ))
                                            .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Contract_no.eq(
                                                    objectDetail.getString("contract_no")
                                            ))
                                            .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Installment_no.eq(
                                                    objectDetail.getString("installment_no")
                                            ));
                                    List<order_detail> order_details = orderDetailQueryBuilder.list();
                                    if (order_details.size() > 0) {
                                        if (objectDetail.getString("status").equals("CLS")) {
                                            order_detailDao.delete(order_details.get(0));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
                FirebaseCrash.log(e.getMessage());
            }
            return null;
        }
    }
}
