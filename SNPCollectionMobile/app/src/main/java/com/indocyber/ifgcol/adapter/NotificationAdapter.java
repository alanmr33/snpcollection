package com.indocyber.ifgcol.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indocyber.ifgcol.activity.AppActivity;
import com.indocyber.ifgcol.adapter.holder.NotificationHolder;
import com.indocyber.ifgcol.db.notifications;


import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Indocyber on 29/03/2018.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationHolder> {
    private List<notifications> notificationsList;
    private AppActivity activity;
    private int layout;
    public NotificationAdapter(List<notifications> notificationsList, AppActivity activity, int layout){
        this.notificationsList=notificationsList;
        this.activity=activity;
        this.layout=layout;
    }
    @Override
    public NotificationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(this.layout, parent, false);
        return new NotificationHolder(v);
    }

    @Override
    public void onBindViewHolder(NotificationHolder holder, int position) {
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd MMM yyyy HH:mm");
        notifications notifications=notificationsList.get(position);
        holder.textViewNotifDate.setText(simpleDateFormat.format(notifications.getDate()));
        holder.textViewNotifMessage.setText(notifications.getMessage());
        holder.textViewNotifTitle.setText(notifications.getTitle());
    }

    @Override
    public int getItemCount() {
        return notificationsList.size();
    }
}
