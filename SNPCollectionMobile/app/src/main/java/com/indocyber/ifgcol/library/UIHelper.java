package com.indocyber.ifgcol.library;

import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.clumsiam.uigenerator.generator.Engine;
import com.indocyber.ifgcol.App;
import com.indocyber.ifgcol.db.params;
import com.indocyber.ifgcol.db.paramsDao;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Indocyber on 12/04/2018.
 */

public abstract class UIHelper {
    public static void loadParams(final Activity activity,
                                   final Engine engine,
                                   final Spinner spinner,
                                   final String condition,
                                   String value){
        final int active=0;
        paramsDao paramsDao=((App)activity.getApplication())
                .getDbSession().getParamsDao();
        QueryBuilder<params> queryBuilder=paramsDao.queryBuilder()
                .where(com.indocyber.ifgcol.db.paramsDao.Properties.Param_condition.eq(condition));
        final List<params> paramsList=queryBuilder.list();
        ArrayList<CharSequence> listValue=new ArrayList<>();
        ((App)activity.getApplication()).tempDataStore.put(condition,paramsList.get(0).getParam_id());
        for (int i=0;i<paramsList.size();i++){
            listValue.add(paramsList.get(i).getParam_description());
            if(value!=null) {
                if (value.equals(paramsList.get(i).getParam_id())) ;
            }
        }
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter<>(activity.getBaseContext(), android.R.layout.simple_spinner_item,listValue);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setSelection(active);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((App)activity.getApplication()).tempDataStore.put(condition,paramsList.get(position).getParam_id());
                ((App)activity.getApplication()).tempDataStore.put(condition+"-Full",paramsList.get(position).getParam_description());
                engine.onUIEvent(spinner,"change",condition,paramsList.get(position).getParam_id());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
