package com.indocyber.ifgcol.service;


import android.app.NotificationManager;
import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.indocyber.ifgcol.App;
import com.indocyber.ifgcol.db.notifications;
import com.indocyber.ifgcol.R;
import com.indocyber.ifgcol.db.notificationsDao;

import java.util.Calendar;
import java.util.Map;
import java.util.Random;

public class FCMessagingService extends FirebaseMessagingService {
    private notificationsDao notificationsDao;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Random random=new Random();
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        try {
            notificationsDao = ((App) getApplication()).getDbSession().getNotificationsDao();
            String body = (remoteMessage.getNotification() == null) ? null : remoteMessage.getNotification().getBody();
            Map<String, String> data = remoteMessage.getData();
            if (body == null) {
                String title = data.get("title");
                body = data.get("body");
                if (title.equalsIgnoreCase("command")) {
                        if(body.equalsIgnoreCase("get_data")){
                            if(!((App)getApplicationContext()).mBound){
                                ((App)getApplicationContext()).bindService();
                            }
                            ((App)getApplicationContext()).mService.getData(false);
                        }else if(body.equalsIgnoreCase("send_data")){
                            if(!((App)getApplicationContext()).mBound){
                                ((App)getApplicationContext()).bindService();
                            }
                            ((App)getApplicationContext()).mService.sendtData(false);
                        }else if(body.equalsIgnoreCase("begin_eod")){
                            ((App) getApplication()).getSession().setBlocked(true);
                            final NotificationManager mNotifyManager =
                                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                            final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getBaseContext());
                            mBuilder.setContentTitle("Pemberitahuan")
                                    .setContentText("Akses aplikasi ditutup")
                                    .setGroupSummary(true)
                                    .setSound(alarmSound)
                                    .setSmallIcon(R.drawable.ic_notif);
                            mNotifyManager.notify(random.nextInt(1000), mBuilder.build());
                        }else if(body.equalsIgnoreCase("end_eod")){
                            ((App) getApplication()).getSession().setBlocked(false);
                            final NotificationManager mNotifyManager =
                                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                            final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getBaseContext());
                            mBuilder.setContentTitle("Pemberitahuan")
                                    .setContentText("Akses aplikasi dibuka")
                                    .setGroupSummary(true)
                                    .setSound(alarmSound)
                                    .setSmallIcon(R.drawable.ic_notif);
                            mNotifyManager.notify(random.nextInt(1000), mBuilder.build());
                        }
                } else {
                    final NotificationManager mNotifyManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getBaseContext());
                    mBuilder.setContentTitle(title)
                            .setContentText(body)
                            .setGroupSummary(true)
                            .setSound(alarmSound)
                            .setSmallIcon(R.drawable.ic_notif);
                    mNotifyManager.notify(random.nextInt(1000), mBuilder.build());
                    notifications notification = new notifications();
                    notification.setIs_read(false);
                    notification.setTitle(title);
                    notification.setMessage(body);
                    notification.setDate(Calendar.getInstance().getTime());
                    notification.setUser_id(((App) getApplication()).getSession().getUserdata().getString("user_id"));
                    notification.setNotif_id(Calendar.getInstance().getTimeInMillis());
                    notificationsDao.insert(notification);
                }
            } else {
                final NotificationManager mNotifyManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getBaseContext());
                mBuilder.setContentTitle("Pemberitahuan")
                        .setContentText(body)
                        .setGroupSummary(true)
                        .setSound(alarmSound)
                        .setSmallIcon(R.drawable.ic_notif);
                mNotifyManager.notify(random.nextInt(1000), mBuilder.build());
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
