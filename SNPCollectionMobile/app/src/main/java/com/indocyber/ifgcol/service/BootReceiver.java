package com.indocyber.ifgcol.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.indocyber.ifgcol.App;

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if(!((App)context.getApplicationContext()).mBound){
            ((App)context.getApplicationContext()).bindService();
        }
    }
}
