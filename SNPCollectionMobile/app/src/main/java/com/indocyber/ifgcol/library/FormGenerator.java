package com.indocyber.ifgcol.library;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.clumsiam.uigenerator.generator.Engine;
import com.clumsiam.uigenerator.generator.KangEventHandler;
import com.clumsiam.uigenerator.utils.Calculate;
import com.clumsiam.uigenerator.utils.Dialog;
import com.clumsiam.uigenerator.utils.Formating;
import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.ifgcol.App;
import com.indocyber.ifgcol.UI.RowsUI;
import com.indocyber.ifgcol.db.events;
import com.indocyber.ifgcol.R;
import com.indocyber.ifgcol.activity.AppActivity;
import com.indocyber.ifgcol.db.DaoSession;
import com.indocyber.ifgcol.db.eventsDao;
import com.indocyber.ifgcol.db.rows;
import com.indocyber.ifgcol.db.rowsDao;

import org.greenrobot.greendao.query.Query;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Indocyber on 24/04/2018.
 */

public class FormGenerator extends Fragment implements KangEventHandler {
    private String formID;
    private LinearLayout mainLayout;
    public HashMap<String,JSONObject> validationList;
    private List<events> eventsList;
    private DaoSession daoSession;
    public FormGenerator() {
    }
    public static FormGenerator newInstance(String formID) {
        FormGenerator fragment = new FormGenerator();
        Bundle args=new Bundle();
        args.putString("form_id",formID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        formID=getArguments().getString("form_id");
        validationList=new HashMap<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final AppActivity appActivity=((AppActivity)getActivity());
        daoSession=((App)appActivity.getApplication()).getDbSession();
        rowsDao rowsDao=daoSession.getRowsDao();
        eventsDao eventsDao=daoSession.getEventsDao();
        Query<rows> rowsQuery=rowsDao.queryBuilder()
                .where(com.indocyber.ifgcol.db.rowsDao.Properties.Form_id.eq(formID))
                .orderAsc(com.indocyber.ifgcol.db.rowsDao.Properties.Row_order)
                .build();
        Query<events> eventsQuery=eventsDao.queryBuilder()
                .where(com.indocyber.ifgcol.db.eventsDao.Properties.Form_id.eq(formID))
                .build();
        eventsList=eventsQuery.list();
        ScrollView scrollView=new ScrollView(appActivity);
        LinearLayout.LayoutParams scrollParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT,1);
        scrollView.setLayoutParams(scrollParams);
        mainLayout=new LinearLayout(appActivity);
        LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1);
        mainLayout.setLayoutParams(layoutParams);
        mainLayout.setOrientation(LinearLayout.VERTICAL);
        List<rows> rowsList=rowsQuery.list();
        for (int i=0;i<rowsList.size();i++){
            if(((AppActivity)getActivity()).readonly){
                View v= RowsUI.create(appActivity,mainLayout,rowsList.get(i),false,this,this);
                if(v!=null){
                    mainLayout.addView(v);
                }
            }else {
                View v=RowsUI.create(appActivity,mainLayout,rowsList.get(i),true,this,this);
                if(v!=null){
                    mainLayout.addView(v);
                }
            }
        }
        mainLayout.setPadding(0,0,0,30);
        scrollView.addView(mainLayout);
        return scrollView;
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onUIEvent(View view, String event, String tagUI, String values) {
        Log.i("EVENT",event+" => "+tagUI+" : "+values);
        for (int i=0;i<eventsList.size();i++) {
            events eventData = eventsList.get(i);
            if (event.equals(eventData.getEvent_type()) && tagUI.equals(eventData.getEvent_component())) {
                try {
                    JSONObject jsonObject = new JSONObject(eventData.getEvent_data());
                    switch (jsonObject.getString("type")) {
                        case "count":
                            EditText toCal = ((EditText) mainLayout.findViewWithTag(eventData.getEvent_component_target()));
                            toCal.setFilters(new InputFilter[]{new InputFilter.LengthFilter(23)});
                            toCal.setText(Formating.currency(Calculate.exec(jsonObject.getString("formula"),
                                    ((AppActivity) getActivity()).getApp().tempDataStore,
                                    jsonObject.getJSONArray("varList"))));
                            break;
                        case "set_val":
                            EditText toSet = ((EditText) mainLayout.findViewWithTag(eventData.getEvent_component_target()));
                            if(jsonObject.getString("value").contains("@")){
                                toSet.setText(((AppActivity) getActivity()).getApp().tempDataStore.get(jsonObject.getString("value")));
                            }else{
                                toSet.setText(jsonObject.getString("value"));
                            }
                            onUIEvent(toSet,"change",eventData.getEvent_component_target(),"");
                            break;
                        case "set_location":
                            EditText toSetLoc = ((EditText) mainLayout.findViewWithTag(eventData.getEvent_component_target()));
                            toSetLoc.setText(((App)getActivity().getApplicationContext()).location);
                            break;
                        case "enable":
                            View toEna = mainLayout.findViewWithTag(eventData.getEvent_component_target());
                            toEna.setEnabled(false);
                            toEna.setClickable(false);
                            break;
                        case "enable_when":
                            View toEnable = mainLayout.findViewWithTag(eventData.getEvent_component_target());
                            if (toEnable != null) {
                                if (((AppActivity) getActivity()).getApp().tempDataStore
                                        .containsKey(jsonObject.getString("key"))) {
                                    if (((AppActivity) getActivity()).getApp().tempDataStore
                                            .get(jsonObject.getString("key"))
                                            .equals(jsonObject.getString("value"))) {
                                        toEnable.setEnabled(true);
                                        toEnable.setClickable(true);
                                        if (toEnable instanceof Spinner) {
                                            LinearLayout layout = (LinearLayout) mainLayout.findViewWithTag(eventData.getEvent_component_target() + "-main");
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                                layout.setBackground(getActivity().getResources()
                                                        .getDrawable(R.drawable.ic_rounded_white_focus, null));
                                            } else {
                                                layout.setBackground(getActivity().getResources()
                                                        .getDrawable(R.drawable.ic_rounded_white_focus));
                                            }
                                        }
                                    } else {
                                        toEnable.setEnabled(false);
                                        toEnable.setClickable(false);
                                        if (toEnable instanceof Spinner) {
                                            LinearLayout layout = (LinearLayout) mainLayout.findViewWithTag(eventData.getEvent_component_target() + "-main");
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                                layout.setBackground(getActivity().getResources()
                                                        .getDrawable(R.drawable.ic_rounded_white, null));
                                            } else {
                                                layout.setBackground(getActivity().getResources()
                                                        .getDrawable(R.drawable.ic_rounded_white));
                                            }

                                        }
                                    }
                                }
                            }
                            if (mainLayout.findViewWithTag(eventData.getEvent_component_target()).getClass() == RadioGroup.class) {
                                ViewGroup toEnables = (ViewGroup) mainLayout.findViewWithTag(eventData.getEvent_component_target());
                                if (toEnables != null) {
                                    if (((AppActivity) getActivity()).getApp().tempDataStore
                                            .get(jsonObject.getString("key"))
                                            .equals(jsonObject.getString("value"))) {
                                        enableChilds(toEnables, true);
                                    } else {
                                        enableChilds(toEnables, false);
                                    }
                                }
                            }
                            break;
                        case "disable":
                            View toDis = mainLayout.findViewWithTag(eventData.getEvent_component_target());
                            toDis.setEnabled(false);
                            toDis.setClickable(false);
                            break;
                        case "disable_when":
                            View toDisable = mainLayout.findViewWithTag(eventData.getEvent_component_target());
                            if (toDisable != null) {
                                if (((AppActivity) getActivity()).getApp().tempDataStore
                                        .containsKey(jsonObject.getString("key"))) {
                                    if (((AppActivity) getActivity()).getApp().tempDataStore
                                            .get(jsonObject.getString("key"))
                                            .equals(jsonObject.getString("value"))) {
                                        toDisable.setEnabled(false);
                                        toDisable.setClickable(false);
                                    } else {
                                        toDisable.setEnabled(true);
                                        toDisable.setClickable(true);
                                    }
                                }
                            }
                            if (mainLayout.findViewWithTag(eventData.getEvent_component_target()).getClass() == RadioGroup.class) {
                                ViewGroup toDisables = (ViewGroup) mainLayout.findViewWithTag(eventData.getEvent_component_target());
                                if (toDisables != null) {
                                    if (((AppActivity) getActivity()).getApp().tempDataStore
                                            .get(jsonObject.getString("key"))
                                            .equals(jsonObject.getString("value"))) {
                                        enableChilds(toDisables, false);
                                    } else {
                                        enableChilds(toDisables, true);
                                    }
                                }
                            }
                            break;
                        case "invisible":
                            View toInvis = mainLayout.findViewWithTag(eventData.getEvent_component_target());
                            toInvis.setVisibility(View.GONE);
                            break;
                        case "invisible_when":
                            View toInvisWhen = mainLayout.findViewWithTag(eventData.getEvent_component_target());
                            if(((AppActivity) getActivity()).getApp().tempDataStore
                                    .containsKey(jsonObject.getString("key"))) {
                                if (((AppActivity) getActivity()).getApp().tempDataStore
                                        .containsKey(jsonObject.getString("key"))) {
                                    if (((AppActivity) getActivity()).getApp().tempDataStore
                                            .get(jsonObject.getString("key"))
                                            .equals(jsonObject.getString("value"))) {
                                        toInvisWhen.setVisibility(View.GONE);
                                    } else {
                                        toInvisWhen.setVisibility(View.VISIBLE);
                                    }
                                }
                            }
                            break;
                        case "visible":
                            View vis = mainLayout.findViewWithTag(eventData.getEvent_component_target());
                            vis.setVisibility(View.VISIBLE);
                            break;
                        case "visible_when":
                            View visWhen = mainLayout.findViewWithTag(eventData.getEvent_component_target());
                            if(((AppActivity) getActivity()).getApp().tempDataStore
                                    .containsKey(jsonObject.getString("key"))){
                                Log.i("MUST",jsonObject.getString("key")
                                        +" = "+
                                        jsonObject.getString("value"));
                                Log.i("CURRENT",jsonObject.getString("key")+" = "+
                                        ((AppActivity) getActivity()).getApp().tempDataStore
                                                .get(jsonObject.getString("key")));
                                if (((AppActivity) getActivity()).getApp().tempDataStore
                                        .get(jsonObject.getString("key"))
                                        .equals(jsonObject.getString("value"))) {
                                    if(visWhen.getClass().equals(Button.class)){
                                        visWhen.setVisibility(View.VISIBLE);
                                    }else{
                                        View visWhenMain = mainLayout.findViewWithTag(
                                                eventData.getEvent_component_target()+"-layout");
                                        if(visWhenMain!=null){
                                            visWhenMain.setVisibility(View.VISIBLE);
                                        }
                                    }
                                } else {
                                    if(visWhen.getClass().equals(Button.class)){
                                        visWhen.setVisibility(View.GONE);
                                    }else{
                                        View visWhenMain = mainLayout.findViewWithTag(
                                                eventData.getEvent_component_target()+"-layout");
                                        if(visWhenMain!=null){
                                            visWhenMain.setVisibility(View.GONE);
                                        }
                                    }
                                }
                            }else{
                                Log.i("NO VALUE",jsonObject.getString("key"));
                            }
                            break;
                        case "alert":
                            Dialog.alert(getActivity(), ((Engine)getActivity()), jsonObject.getString("message"), jsonObject.getString("tag"));
                            break;
                        case "confirm":
                            Dialog.confirm(getActivity(), ((Engine)getActivity()),
                                    jsonObject.getString("message"),
                                    jsonObject.getString("yes"),
                                    jsonObject.getString("no"),
                                    jsonObject.getString("tag"));
                            break;
                        case "submit":
                            submit();
                            break;
                        case "save":
                            ((Engine) getActivity()).save();
                            break;
                        case "next":
                            next();
                            break;
                        case "back":
                            getActivity().finish();
                            break;

                    }
                } catch (JSONException e) {
                    FirebaseCrash.log(e.getMessage());
                }
            }
        }
    }
    public void enableChilds(ViewGroup viewGroup,boolean enable){
        final int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = viewGroup.getChildAt(i);
            child.setEnabled(enable);
        }
    }
    public void submit(){
        boolean error=false;
        error=validate();
        if(!error){
            ((Engine) getActivity()).submit();
        }
    }
    public void next(){
        boolean error=false;
        error=validate();
        if(!error){
            ((Engine) getActivity()).next();
        }
    }
    private boolean validate(){
        Set<String> validKeys=validationList.keySet();
        for (String s : validKeys) {
            try {
                boolean required=(validationList.get(s).has("required"))? validationList.get(s).getBoolean("required") : false;
                String required_msg=(validationList.get(s).has("required_message"))? validationList.get(s).getString("required_message") : "";
                String regex=(validationList.get(s).has("regex"))? validationList.get(s).getString("regex") : "";
                String regex_msg=(validationList.get(s).has("regex_message"))? validationList.get(s).getString("regex_message") : "";
                String min=(validationList.get(s).has("min"))? validationList.get(s).getString("min") : "";
                String max=(validationList.get(s).has("min"))? validationList.get(s).getString("max") : "";
                String min_message=(validationList.get(s).has("min_message"))? validationList.get(s).getString("min_message") : "";
                String max_message=(validationList.get(s).has("max_message"))? validationList.get(s).getString("max_message") : "";
                if(required){
                    if(!((AppActivity)getActivity()).getApp().tempDataStore.containsKey(s)){
                        Toast.makeText(getActivity(),required_msg,Toast.LENGTH_SHORT).show();
                        return true;
                    }else {
                        if(((AppActivity)getActivity()).getApp().tempDataStore.get(s).equals("")){
                            Toast.makeText(getActivity(),required_msg,Toast.LENGTH_SHORT).show();
                           return true;
                        }
                    }
                }
                if(!regex.equals("")){
                    Pattern pattern = Pattern.compile(regex);
                    Matcher matcher = pattern.matcher(((AppActivity)getActivity()).getApp().tempDataStore.get(s));
                    if (!matcher.find()) {
                        Toast.makeText(getActivity(),regex_msg,Toast.LENGTH_SHORT).show();
                        return true;
                    }
                }
                if(!min.equals("")){
                    long minVal=(min.contains("@"))? Long.parseLong(((AppActivity)getActivity()).getApp().tempDataStore.get(min.replace("@",""))) : Long.parseLong(min);
                    long val=Long.parseLong(((AppActivity)getActivity()).getApp().tempDataStore.get(s));
                    if(val<minVal){
                        Toast.makeText(getActivity(),min_message.replace("[min]",String.valueOf(minVal)),Toast.LENGTH_SHORT).show();
                        return true;
                    }
                }
                if(!max.equals("")){
                    long maxVal=(max.contains("@"))? Long.parseLong(((AppActivity)getActivity()).getApp().tempDataStore.get(max.replace("@",""))) : Long.parseLong(max);
                    long val=Long.parseLong(((AppActivity)getActivity()).getApp().tempDataStore.get(s));
                    if(val>maxVal){
                        Toast.makeText(getActivity(),max_message.replace("[max]",String.valueOf(maxVal)),Toast.LENGTH_SHORT).show();
                        return true;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                FirebaseCrash.log(e.getMessage());
            }
        }
        return false;
    }
}
