package com.indocyber.ifgcol.activity;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.indocyber.ifgcol.adapter.NotificationAdapter;
import com.indocyber.ifgcol.R;
import com.indocyber.ifgcol.db.notifications;
import com.indocyber.ifgcol.db.notificationsDao;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.List;

public class NotificationList extends AppActivity {
    private notificationsDao notificationsDao;
    private NotificationAdapter adapter;
    private List<notifications> notificationsList;
    private RecyclerView recyclerView;
    private Button buttonCleanNotif;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_list);
        this.getSupportActionBar().setHomeButtonEnabled(true);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView= (RecyclerView) findViewById(R.id.notifList);
        buttonCleanNotif= (Button) findViewById(R.id.buttonCleanNotif);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        notificationsDao=getApp().getDbSession().getNotificationsDao();
        QueryBuilder<notifications> listQuery=notificationsDao.queryBuilder().orderDesc(com.indocyber.ifgcol.db.notificationsDao.Properties.Date);
        notificationsList=listQuery.list();
        for(int i=0;i<notificationsList.size();i++){
            notifications notif=notificationsList.get(i);
            notif.setIs_read(true);
        }
        notificationsDao.updateInTx(notificationsList);
        if(notificationsList.size()>0){
            buttonCleanNotif.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    notificationsDao.deleteInTx(notificationsList);
                    buttonCleanNotif.setVisibility(View.GONE);
                    generateList();
                }
            });
        }else{
            buttonCleanNotif.setVisibility(View.GONE);
        }
        generateList();
    }
    public void generateList(){
        adapter=new NotificationAdapter(notificationsList,this,R.layout.notification_item);
        recyclerView.setAdapter(adapter);
    }
}
