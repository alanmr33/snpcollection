package com.indocyber.ifgcol.UI;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clumsiam.uigenerator.generator.KangEventHandler;
import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.ifgcol.db.fields;
import com.indocyber.ifgcol.library.FileCreator;
import com.indocyber.ifgcol.library.FormGenerator;
import com.indocyber.ifgcol.R;
import com.indocyber.ifgcol.activity.AppActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

/**
 * Created by Indocyber on 31/10/2017.
 */

public abstract class PhotoPicker {
    public static final View create(final Activity activity, final View view, final fields props, final boolean enable, final KangEventHandler eventHandler, Fragment fragment) {
        final LinearLayout component=new LinearLayout(activity);
        LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1);
        component.setLayoutParams(layoutParams);
        component.setOrientation(LinearLayout.VERTICAL);
        final ImageView imageView=new ImageView(activity);
        imageView.setImageResource(R.drawable.image_placeholder);
        LinearLayout.LayoutParams imageLayout=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, 250
                ,0);
        imageLayout.gravity= Gravity.CENTER_HORIZONTAL;
        imageView.setLayoutParams(imageLayout);
        imageView.setTag(props.getField_name());
        if(enable) {
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    eventHandler.onUIEvent(v, "click", imageView.getTag().toString(), "");
                    ((AppActivity) activity).getApp().tempDataStore.put("active-photo-picker", imageView.getTag().toString());
                    int request = 1150;
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(((AppActivity) activity).getPackageManager()) != null) {
                        File photoFile = FileCreator.createImages(((AppActivity) activity),
                                imageView.getTag().toString(),
                                ((AppActivity) activity).getApp().tempDataStore.get("OrderID"));
                        Uri photoURI = FileProvider.getUriForFile(activity,
                                "com.indocyber.ifgcol.library.ImageProvider",
                                photoFile);
                        ((AppActivity) activity).getApp().tempDataStore.put(imageView.getTag().toString() + "-" + "file", photoFile.getAbsolutePath());
                        ((AppActivity) activity).getApp().tempDataStore.put(imageView.getTag().toString() + "-" + "refresh", "false");
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        activity.startActivityForResult(takePictureIntent, request);
                    }
                }
            });
        }
        try {
            JSONObject extras = new JSONObject(props.getField_extra());
            if(extras.has("validation")){
                ((FormGenerator)fragment).validationList.put(imageView.getTag().toString() + "-" + "file",extras.getJSONObject("validation"));
            }
            if(props.getField_label()!=null && !props.getField_label().equals("")) {
                try {
                    if(extras.has("validation")){
                        boolean required=(extras.getJSONObject("validation").has("required"))? extras.getJSONObject("validation").getBoolean("required") : false;
                        if(required){
                            LinearLayout labelLayout = new LinearLayout(activity);
                            labelLayout.setOrientation(LinearLayout.HORIZONTAL);
                            TextView lblRequired = new TextView(activity);
                            lblRequired.setText("*");
                            lblRequired.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                lblRequired.setTextColor(activity.getResources().getColor(R.color.colorPrimaryDark, null));
                            } else {
                                lblRequired.setTextColor(activity.getResources().getColor(R.color.colorPrimaryDark));
                            }
                            labelLayout.addView(LabelUI.create(activity, component, props));
                            labelLayout.addView(lblRequired);
                            component.addView(labelLayout);
                        }else{
                            component.addView(LabelUI.create(activity, component, props));
                        }
                    }else{
                        component.addView(LabelUI.create(activity, component, props));
                    }
                }catch (Exception e){
                    FirebaseCrash.log(e.getMessage());
                }
            }
        } catch (JSONException e) {
            FirebaseCrash.log(e.getMessage());
        }
        component.addView(imageView);
        final LinearLayout buttonGroup=new LinearLayout(activity);
        LinearLayout.LayoutParams layoutParamsbuttonGroup=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1);
        buttonGroup.setLayoutParams(layoutParamsbuttonGroup);
        buttonGroup.setOrientation(LinearLayout.HORIZONTAL);
        if(enable){
            LinearLayout.LayoutParams lblParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,1);
            lblParams.gravity=Gravity.CENTER_HORIZONTAL;
            TextView lbl=new TextView(activity);
            lbl.setText("Ambil Photo");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                lbl.setTextColor(activity.getResources().getColor(R.color.colorSecondaryDark,null));
            }else{
                lbl.setTextColor(activity.getResources().getColor(R.color.colorSecondaryDark));
            }
            lbl.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            lbl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    eventHandler.onUIEvent(v, "click", imageView.getTag().toString(), "");
                    ((AppActivity) activity).getApp().tempDataStore.put("active-photo-picker", imageView.getTag().toString());
                    int request = 1150;
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(((AppActivity) activity).getPackageManager()) != null) {
                        File photoFile = FileCreator.createImages(((AppActivity) activity),
                                imageView.getTag().toString(),
                                ((AppActivity) activity).getApp().tempDataStore.get("OrderID"));
                        Uri photoURI = FileProvider.getUriForFile(activity,
                                "com.indocyber.ifgcol.library.ImageProvider",
                                photoFile);
                        ((AppActivity) activity).getApp().tempDataStore.put(imageView.getTag().toString() + "-" + "file", photoFile.getAbsolutePath());
                        ((AppActivity) activity).getApp().tempDataStore.put(imageView.getTag().toString() + "-" + "refresh", "false");
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        activity.startActivityForResult(takePictureIntent, request);
                    }
                }
            });
            lbl.setLayoutParams(lblParams);
            TextView lblDelete=new TextView(activity);
            lblDelete.setText("Hapus Photo");
            lblDelete.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                lblDelete.setTextColor(activity.getResources().getColor(R.color.colorPrimaryDark,null));
            }else{
                lblDelete.setTextColor(activity.getResources().getColor(R.color.colorPrimaryDark));
            }
            lblDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder=new AlertDialog.Builder(activity);
                    builder.setMessage("Hapus Foto ?");
                    builder.setCancelable(false);
                    builder.setPositiveButton("YA", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            if(((AppActivity) activity).getApp().tempDataStore
                                    .containsKey(imageView.getTag().toString() + "-" + "file")){
                                File photo=new File(((AppActivity) activity).getApp().tempDataStore
                                        .get(imageView.getTag().toString() + "-" + "file"));
                                if(photo.exists()){
                                    photo.delete();
                                    ((AppActivity) activity).getApp().tempDataStore.remove(imageView.getTag().toString() + "-" + "file");
                                    buttonGroup.findViewWithTag("deleteButton").setVisibility(View.GONE);
                                    imageView.setImageResource(R.drawable.image_placeholder);
                                    imageView.setRotation(0);
                                }
                            }
                        }
                    });
                    builder.setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog=builder.create();
                    dialog.show();
                }
            });
            lblDelete.setTag("deleteButton");
            lblDelete.setLayoutParams(lblParams);
            buttonGroup.addView(lbl);
            buttonGroup.addView(lblDelete);
        }
        component.addView(buttonGroup);
        if(((AppActivity) activity).getApp().tempDataStore.containsKey(imageView.getTag().toString() + "-" + "file")){
            Log.i("Image File",((AppActivity) activity).getApp().tempDataStore.get(imageView.getTag().toString() + "-" + "file"));
            Bitmap bitmap = BitmapFactory.decodeFile(((AppActivity) activity).getApp().tempDataStore.get(imageView.getTag().toString() + "-" + "file"));
            imageView.setImageBitmap(bitmap);
//            imageView.setRotation(90);
        }
        if(enable){
            PhotoPicker.autoRefresh(imageView,buttonGroup,((AppActivity) activity),eventHandler);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(((AppActivity) activity).getApp().tempDataStore.containsKey(imageView.getTag().toString() + "-" + "file")){
                        buttonGroup.findViewWithTag("deleteButton").setVisibility(View.VISIBLE);
                    }else{
                        buttonGroup.findViewWithTag("deleteButton").setVisibility(View.GONE);
                    }
                    eventHandler.onUIEvent(component,"init",props.getField_name(),"");
                }
            },1000);
        }
        return component;
    }
    public static void autoRefresh(final ImageView imageView, final LinearLayout buttonGroup, final AppActivity appActivity, final KangEventHandler eventHandler){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(appActivity.getApp().tempDataStore.containsKey(imageView.getTag()+"-"+"refresh")) {
                    if (appActivity.getApp().tempDataStore.get(imageView.getTag() + "-" + "refresh").equals("true")) {
//                        appActivity.getApp().tempDataStore.put(imageView.getTag() + "-" + "refresh","false");
                        Bitmap bitmap = BitmapFactory.decodeFile(appActivity.getApp().tempDataStore.get(imageView.getTag().toString() + "-" + "file"));
                        imageView.setImageBitmap(bitmap);
//                        imageView.setRotation(90);
                        buttonGroup.findViewWithTag("deleteButton").setVisibility(View.VISIBLE);
                        eventHandler.onUIEvent(imageView, "change", imageView.getTag().toString(), appActivity.getApp().tempDataStore.get(imageView.getTag() + "-" + "file"));
                    }
                }
                autoRefresh(imageView,buttonGroup,appActivity,eventHandler);
            }
        },1000);
    }
}
