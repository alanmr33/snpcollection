package com.indocyber.ifgcol.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.clumsiam.uigenerator.generator.Engine;
import com.clumsiam.uigenerator.utils.Formating;
import com.indocyber.ifgcol.R;
import com.indocyber.ifgcol.adapter.SelectToPayAdapter;
import com.indocyber.ifgcol.db.order_detail;
import com.indocyber.ifgcol.db.order_detailDao;

import org.greenrobot.greendao.query.Query;

import java.util.ArrayList;
import java.util.List;

public class SelectToPay extends AppActivity implements Engine {
    private RecyclerView listOfBills;
    private TextView txtTotalAmt;
    private SelectToPayAdapter adapter;
    private com.indocyber.ifgcol.db.order_detailDao order_detailDao;
    private List<order_detail> order_details;
    private Button buttonCheckAll;
    private int totalPayment=0;
    private boolean isAll=false;
    private Button btnSave,btnNext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_to_pay);
        this.getSupportActionBar().setHomeButtonEnabled(true);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        buttonCheckAll= (Button) findViewById(R.id.buttonCheckAll);
        listOfBills= (RecyclerView) findViewById(R.id.listOfBill);
        txtTotalAmt= (TextView) findViewById(R.id.txtTotalAmt);
        listOfBills.setLayoutManager(new LinearLayoutManager(this));
        listOfBills.setItemAnimator(new DefaultItemAnimator());
        order_detailDao=getApp().getDbSession().getOrder_detailDao();
        if (!getApp().tempDataStore.containsKey("ActivityID")) {
            Query<order_detail> queryBuilder = order_detailDao.queryBuilder()
                    .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Order_id.eq(getApp().tempDataStore.get("OrderID")))
                    .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Status.eq("NEW"))
                    .build();
            order_details=queryBuilder.list();
        }else{
            Query<order_detail> queryBuilder = order_detailDao.queryBuilder()
                    .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Order_id.eq(getApp().tempDataStore.get("OrderID")))
                    .whereOr(com.indocyber.ifgcol.db.order_detailDao.Properties.Status.eq("NEW"),
                            com.indocyber.ifgcol.db.order_detailDao.Properties.Status.eq("DRAFT"))
                    .build();
            order_details=queryBuilder.list();
        }
        buttonCheckAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetCount();
                getApp().order_details=new ArrayList<>();
                changeButton(isAll);
                generateList(isAll);
            }
        });
        btnSave= (Button) findViewById(R.id.buttonSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });
        btnNext= (Button) findViewById(R.id.buttonNext);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next();
            }
        });
        generateList(false);
        Count();
    }
    public void changeButton(boolean isAll){
        if(isAll){
            this.isAll=false;
            buttonCheckAll.setText("Pilih Semua");
        }else{
            this.isAll=true;
            buttonCheckAll.setText("Batalkan Pilihan");
        }
    }
    protected void generateList(boolean isAll){
        adapter=new SelectToPayAdapter(order_details,this,R.layout.selecttopay_item,isAll);
        listOfBills.setAdapter(adapter);
    }
    private void resetCount(){
        totalPayment=0;
        txtTotalAmt.setText("Rp. 0");
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }
    public int getTotal(){
        return totalPayment;
    }
    public void setTotalPayment(int totalPayment){
        this.totalPayment=totalPayment;
        txtTotalAmt.setText("Rp. "+ Formating.currency(totalPayment));
    }

    @Override
    public void onUIEvent(View view, String event, String tagUI, String values) {

    }

    @Override
    public void save() {
        saveForm();
        Intent main=new Intent(this,Main.class);
        main.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(main);
        finish();
    }

    @Override
    public void submit() {

    }

    @Override
    public void next() {
        if(getApp().order_details.size()>0){
            long totalToPay=0,minToPay=0,maxToPay=0;
            for (int i=0;i<getApp().order_details.size();i++){
                totalToPay+=Long.parseLong(getApp().order_details.get(i).getTotal_amt());
                minToPay+=Long.parseLong(getApp().order_details.get(i).getMin_payment());
                maxToPay+=Long.parseLong(getApp().order_details.get(i).getMax_payment());
            }
            getApp().tempDataStore.put("TotalItem",String.valueOf(getApp().order_details.size()));
            getApp().tempDataStore.put("PaymentAmt",String.valueOf(totalToPay));
            if(getApp().order_details.size()>1){
                getApp().tempDataStore.put("minPaymentAmt",String.valueOf(minToPay));
                getApp().tempDataStore.put("maxPaymentAmt",String.valueOf(maxToPay));
            }else{
                getApp().tempDataStore.put("minPaymentAmt",getApp().order_details.get(0).getMin_payment());
                getApp().tempDataStore.put("maxPaymentAmt",getApp().order_details.get(0).getMax_payment());
            }
            Intent next=new Intent(this,PaymentList.class);
            next.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(next);
        }else{
            Toast.makeText(this,"Silahkan Pilih Salah Satu !",Toast.LENGTH_SHORT).show();
        }
    }
    public void Count(){
        int total=0;
        for (int i=0;i<getApp().order_details.size();i++){
            total+=Integer.parseInt(getApp().order_details.get(i).getTotal_amt());
        }
        setTotalPayment(total);
    }
}
