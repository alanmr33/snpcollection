package com.indocyber.ifgcol.UI;

import com.google.firebase.crash.FirebaseCrash;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Indocyber on 09/11/2017.
 */

public abstract class MD5 {
    public static final String enc(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            FirebaseCrash.log(e.getMessage());
        }
        return "";
    }
}
