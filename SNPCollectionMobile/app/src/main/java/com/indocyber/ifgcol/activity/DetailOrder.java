package com.indocyber.ifgcol.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.clumsiam.uigenerator.generator.Engine;
import com.indocyber.ifgcol.App;
import com.indocyber.ifgcol.R;
import com.indocyber.ifgcol.db.order_detail;
import com.indocyber.ifgcol.db.order_detailDao;

import org.greenrobot.greendao.query.Query;
import org.greenrobot.greendao.query.QueryBuilder;

import java.util.ArrayList;
import java.util.List;

public class DetailOrder extends AppActivity implements Engine {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_order);
        this.getSupportActionBar().setHomeButtonEnabled(true);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        loadForm(R.id.layoutForm,"01");
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onUIEvent(View view, String event, String tagUI, String values) {

    }

    @Override
    public void save() {
        saveForm();
        Intent main=new Intent(this,Main.class);
        startActivity(main);
        finish();
    }

    @Override
    public void submit() {

    }

    @Override
    public void next() {
        if(((App)getApplication()).tempDataStore.containsKey("VisitResult")){
            if(!((App)getApplication()).tempDataStore.get("VisitResult").equals("VR0")) {
                if(((App)getApplication()).tempDataStore.get("MODE").equals("PENDING")
                        ||
                        ((App)getApplication()).tempDataStore.get("MODE").equals("SENT")){
                    Intent next = new Intent(this, PaymentList.class);
                    next.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(next);
                }else{
                    Intent next = new Intent(this, SelectToPay.class);
                    next.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(next);
                }
            }else{
                Intent next = new Intent(this, SelectToPromise.class);
                next.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(next);
            }
        }

    }
}
