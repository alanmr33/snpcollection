package com.indocyber.ifgcol.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.clumsiam.uigenerator.generator.Engine;
import com.indocyber.ifgcol.adapter.ToPayAdapter;
import com.indocyber.ifgcol.R;
import com.indocyber.ifgcol.db.order_detail;
import com.indocyber.ifgcol.db.order_detailDao;

import org.greenrobot.greendao.query.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PaymentList extends AppActivity implements Engine {
    private Button btnSave,btnNext;
    private RecyclerView listPayed;
    private order_detailDao order_detailDao;
    private ToPayAdapter adapter;
    private TextView txtNama,txtAlamat,txtNoHP;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_list);
        this.getSupportActionBar().setHomeButtonEnabled(true);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        txtNama= (TextView) findViewById(R.id.txtNamaPemesan);
        txtAlamat= (TextView) findViewById(R.id.txtAlamat);
        txtNoHP= (TextView) findViewById(R.id.txtTelepon);
        listPayed= (RecyclerView) findViewById(R.id.listPayed);
        btnSave= (Button) findViewById(R.id.buttonSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(readonly){
                    finish();
                }else{
                    save();
                }
            }
        });
        if(readonly){
            btnSave.setText("KEMBALI");
        }
        btnNext= (Button) findViewById(R.id.buttonNext);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next();
            }
        });
        listPayed.setLayoutManager(new LinearLayoutManager(this));
        listPayed.setItemAnimator(new DefaultItemAnimator());
        if(!getApp().tempDataStore.get("MODE").equals("PENDING") && !getApp().tempDataStore.get("MODE").equals("SENT")) {
            adapter = new ToPayAdapter(getApp().order_details, this, R.layout.topay_item);
        }else {
            order_detailDao=getApp().getDbSession().getOrder_detailDao();
            Query<order_detail> queryBuilder = order_detailDao.queryBuilder()
                    .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Activity_id.eq(getApp().tempDataStore.get("ActivityID")))
                    .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Status.eq(getApp().tempDataStore.get("MODE")))
                    .build();
            adapter = new ToPayAdapter(queryBuilder.list(), this, R.layout.topay_item);
        }
        listPayed.setAdapter(adapter);
        try {
            txtAlamat.setText(getApp().tempDataStore.get("CustomerAddress"));
            txtNoHP.setText(getApp().tempDataStore.get("CustomerPhone"));
            txtNama.setText(getApp().tempDataStore.get("CustomerName"));
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onUIEvent(View view, String event, String tagUI, String values) {

    }

    @Override
    public void save() {
        saveForm();
        Intent main=new Intent(this,Main.class);
        main.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(main);
        finish();
    }

    @Override
    public void submit() {

    }

    @Override
    public void next() {
        Intent next=new Intent(this,PaymentSubmit.class);
        next.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(next);
    }
}
