package com.indocyber.ifgcol.UI;

import android.app.Activity;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.clumsiam.uigenerator.generator.KangEventHandler;
import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.ifgcol.App;
import com.indocyber.ifgcol.db.DaoSession;
import com.indocyber.ifgcol.db.fields;
import com.indocyber.ifgcol.db.params;
import com.indocyber.ifgcol.db.paramsDao;
import com.indocyber.ifgcol.library.FormGenerator;
import com.indocyber.ifgcol.activity.AppActivity;

import org.greenrobot.greendao.query.Query;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Indocyber on 01/11/2017.
 */

public abstract class CheckBoxUI {
    public static final View create(final Activity activity, final View view, final fields props, boolean enable, final KangEventHandler eventHandler, Fragment fragment) {
        final LinearLayout component=new LinearLayout(activity);
        component.setTag(props.getField_name());
        LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1);
        component.setLayoutParams(layoutParams);
        component.setOrientation(LinearLayout.VERTICAL);
        DaoSession daoSession=((App)((AppActivity) activity).getApplication()).getDbSession();
        paramsDao paramsDao=daoSession.getParamsDao();
        Query<params> paramsQuery=paramsDao.queryBuilder()
                .where(com.indocyber.ifgcol.db.paramsDao.Properties.Param_condition.eq(props.getField_global_value()))
                .build();
        final List<params> paramsList=paramsQuery.list();
        for (int i=0;i<paramsList.size();i++){
            params chkParams=paramsList.get(i);
            final CheckBox checkBox=new CheckBox(activity);
            LinearLayout.LayoutParams chkLayout=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1);
            checkBox.setText(chkParams.getParam_description());
            checkBox.setLayoutParams(chkLayout);
            checkBox.setTag(component.getTag().toString()+"-"+chkParams.getParam_id());
            if(((AppActivity) activity).getApp().tempDataStore.containsKey(checkBox.getTag().toString())){
                if(((AppActivity) activity).getApp().tempDataStore.get(checkBox.getTag().toString()).equals("1")){
                    checkBox.setChecked(true);
                }
            }
            if(!enable){
                checkBox.setEnabled(false);
            }
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        ((AppActivity) activity).getApp().tempDataStore.put(checkBox.getTag().toString(),String.valueOf(1));
                        ((AppActivity) activity).getApp().tempDataStore.put(component.getTag().toString(),String.valueOf(1));
                        eventHandler.onUIEvent(component,"change",checkBox.getTag().toString(),"1");
                    }else{
                        ((AppActivity) activity).getApp().tempDataStore.put(checkBox.getTag().toString(),String.valueOf(0));
                        ((AppActivity) activity).getApp().tempDataStore.put(component.getTag().toString(),String.valueOf(1));

                        eventHandler.onUIEvent(component,"change",checkBox.getTag().toString(),"0");
                    }
                }
            });
            try {
                JSONObject extras = new JSONObject(props.getField_extra());
                if(extras.has("validation")){
                    ((FormGenerator)fragment).validationList.put(checkBox.getTag().toString(),extras.getJSONObject("validation"));
                }
            } catch (JSONException e) {
                FirebaseCrash.log(e.getMessage());
            }
            component.addView(checkBox);
        }
        if(enable) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    eventHandler.onUIEvent(component, "init", props.getField_name(), "");
                }
            }, 1000);
        }
        return component;
    }
}