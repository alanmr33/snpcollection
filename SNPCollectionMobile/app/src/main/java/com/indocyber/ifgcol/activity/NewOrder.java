package com.indocyber.ifgcol.activity;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.ifgcol.db.order_detail;
import com.indocyber.ifgcol.db.order_list;
import com.indocyber.ifgcol.R;
import com.indocyber.ifgcol.adapter.NewAdapter;
import com.indocyber.ifgcol.db.order_listDao;
import com.indocyber.ifgcol.library.RefreshInf;

import org.greenrobot.greendao.query.Query;
import org.greenrobot.greendao.query.QueryBuilder;

import java.util.List;

public class NewOrder extends AppActivity implements RefreshInf {
    private com.indocyber.ifgcol.db.order_listDao order_listDao;
    private com.indocyber.ifgcol.db.order_detailDao order_detailDao;
    private NewAdapter adapter;
    private List<order_list> listsOrder;
    private RecyclerView recyclerView;
    private EditText editTextSearch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_order);
        this.getSupportActionBar().setHomeButtonEnabled(true);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView= (RecyclerView) findViewById(R.id.newOrderList);
        editTextSearch=(EditText) findViewById(R.id.editTextSearch);
        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(final Editable s) {
                NewOrder.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(!s.toString().equals("")){
                            searchOrder(s.toString());
                        }else{
                            getAll();
                        }
                    }
                });
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        order_listDao=getApp().getDbSession().getOrder_listDao();
        order_detailDao=getApp().getDbSession().getOrder_detailDao();
         getAll();
    }
    public void searchOrder(String text){
        try {
            QueryBuilder<order_list> listQuery = order_listDao.queryBuilder()
                    .where(com.indocyber.ifgcol.db.order_listDao.Properties.User_id.eq(getApp().getSession().getUserdata().getString("user_id")))
                    .whereOr(com.indocyber.ifgcol.db.order_listDao.Properties.Customer_name.like("%" + text + "%"),
                            com.indocyber.ifgcol.db.order_listDao.Properties.Address.like("%" + text + "%"));
            listsOrder = listQuery.list();
            generateList();
        }catch (Exception e){
            e.printStackTrace();
            FirebaseCrash.log(e.getMessage());
        }
    }
    public void getAll(){
        try {
            QueryBuilder<order_list> listQuery=order_listDao.queryBuilder()
                    .where(com.indocyber.ifgcol.db.order_listDao.Properties.User_id.eq(getApp().getSession().getUserdata().getString("user_id")));
            listsOrder=listQuery.list();
            generateList();
        }catch (Exception e){
            e.printStackTrace();
            FirebaseCrash.log(e.getMessage());
        }
    }
    @Override
    protected void onResume(){
        super.onResume();
        searchOrder(editTextSearch.getText().toString());
    }
    public void generateList(){
        adapter=new NewAdapter(listsOrder,this,R.layout.new_order_item,order_detailDao);
        recyclerView.setAdapter(adapter);
    }
}
