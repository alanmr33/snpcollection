package com.indocyber.ifgcol.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.ifgcol.R;
import com.indocyber.ifgcol.UI.MD5;
import com.indocyber.ifgcol.library.AppSettings;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;

import cz.msebera.android.httpclient.Header;

public class Setting extends AppActivity implements View.OnClickListener{
    private EditText txtUserID,txtFullname,txtOPassword,txtNPassword,txtCPassword;
    private Button btnSave;
    private ProgressDialog loading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        this.getSupportActionBar().setHomeButtonEnabled(true);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        /*
        init all component
         */
        txtUserID= (EditText) findViewById(R.id.editTextUserID);
        txtFullname= (EditText) findViewById(R.id.editTextFullname);
        txtOPassword= (EditText) findViewById(R.id.editTextOPassword);
        txtCPassword= (EditText) findViewById(R.id.editTextCPassword);
        txtNPassword= (EditText) findViewById(R.id.editTextNPassword);
        btnSave= (Button) findViewById(R.id.buttonSaveSetting);
        /*
        set all on click listener
         */
        btnSave.setOnClickListener(this);

        try {
            txtUserID.setText(getApp().getSession().getUserdata().getString("user_id"));
            txtFullname.setText(getApp().getSession().getUserdata().getString("nama_karyawan"));
            txtUserID.setEnabled(false);
        } catch (JSONException e) {
            Log.e(getClass().getSimpleName(),e.getMessage());
        }
    }
    /*
    all on click event is here
     */
    @Override
    public void onClick(View v) {
        /*
        choose action by event
         */
        switch (v.getId()){
            case R.id.buttonSaveSetting :
                String username=txtUserID.getText().toString();
                final String fullname=txtFullname.getText().toString();
                String opassword=txtOPassword.getText().toString();
                String cpassword=txtCPassword.getText().toString();
                String npassword=txtNPassword.getText().toString();
                if(fullname.equals("")){
                    Toast.makeText(this,"Fullname is required",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(opassword.equals("")){
                    Toast.makeText(this,"Kata sandi lama harus diisi",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(!npassword.equals(cpassword)){
                    Toast.makeText(this,"Kata sandi baru tidak cocok",Toast.LENGTH_SHORT).show();
                    return;
                }
                /*
                show loading
                 */
                loading=new ProgressDialog(this);
                loading.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                loading.setCancelable(false);
                loading.setMessage("Menyimpan Perubahan ...");
                loading.show();
                /*
                set Params
                 */
                RequestParams params=new RequestParams();
                JSONObject data=new JSONObject();
                params.put("token", getApp().getSession().getToken());
                try {
                    data.put("opassword", MD5.enc(opassword));
                    data.put("npassword",MD5.enc(npassword));
                    data.put("username",username);
                    data.put("fullname",fullname);
                } catch (JSONException e) {
                    FirebaseCrash.log(e.getMessage());
                }
                params.put("data", data.toString());
                getAPIClient().addHeader("APPID", "IFGCOL-IGLO");
                getAPIClient().post(AppSettings.SETTING_URL, params, new AsyncHttpResponseHandler() {
                    /*
                     handle success save change
                      */
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        loading.hide();
                        try {
                            String data=new String(responseBody, StandardCharsets.UTF_8);
                            JSONObject response=new JSONObject(data);
                            if(response.getBoolean("status")){
                                JSONObject userData=getApp().getSession().getUserdata();
                                userData.put("isFirstLogin","0");
                                getApp().getSession().setUserData(userData);
                                getApp().getSession().setUserData(getApp().getSession()
                                        .getUserdata().put("Fullname",fullname));
                                if(!txtNPassword.getText().toString().equals("") && txtNPassword.getText()==null) {
                                    getApp().getSession().setLastPassword(txtNPassword.getText().toString());
                                }
                                Intent m=new Intent(Setting.this,Main.class);
                                startActivity(m);
                                finish();
                            }else{
                                Toast.makeText(Setting.this,response.getString("message"),Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            Log.e(getClass().getSimpleName(),e.getMessage());
                        }
                    }
                    /*
                    handle error save
                     */
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        loading.hide();
                        AlertDialog.Builder builder = new AlertDialog.Builder(Setting.this);
                        builder.setMessage("Terjadi Kesalahan : "+statusCode);
                        builder.setCancelable(false);
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        builder.show();
                    }
                });
                break;
        }
    }

}
