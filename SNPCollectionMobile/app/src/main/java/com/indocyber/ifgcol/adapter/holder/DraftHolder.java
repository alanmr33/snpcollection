package com.indocyber.ifgcol.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indocyber.ifgcol.R;

/**
 * Created by Indocyber on 08/05/2018.
 */

public class DraftHolder extends RecyclerView.ViewHolder {
    public TextView textViewNamaPemesan,textViewNoHP,textViewAlamat,textViewTagihan,textViewLPKNO,textStatus;
    public Button buttonDiscard;
    public LinearLayout newOrderItem,layoutPengiriman;
    public DraftHolder(View itemView) {
        super(itemView);
        textViewLPKNO= (TextView) itemView.findViewById(R.id.textViewLPKNO);
        textViewNamaPemesan= (TextView) itemView.findViewById(R.id.textViewNamaPemesan);
        textViewNoHP= (TextView) itemView.findViewById(R.id.textViewNoHP);
        textViewAlamat= (TextView) itemView.findViewById(R.id.textViewAlamat);
        textViewTagihan= (TextView) itemView.findViewById(R.id.textViewCount);
        textStatus= (TextView) itemView.findViewById(R.id.textStatus);
        newOrderItem= (LinearLayout) itemView.findViewById(R.id.newOrderItem);
        layoutPengiriman= (LinearLayout) itemView.findViewById(R.id.layoutStatus);
        buttonDiscard= (Button) itemView.findViewById(R.id.buttonDiscard);
    }
}
