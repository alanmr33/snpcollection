package com.indocyber.ifgcol.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.clumsiam.uigenerator.generator.Engine;
import com.indocyber.ifgcol.R;
import com.indocyber.ifgcol.adapter.ToPayAdapter;
import com.indocyber.ifgcol.db.order_detail;

import org.greenrobot.greendao.query.Query;

import java.util.ArrayList;
import java.util.List;

public class SelectToPromise extends AppActivity implements Engine {
    private Button btnSave,btnNext;
    private RecyclerView listPromise;
    private ToPayAdapter adapter;
    private TextView txtNama,txtAlamat,txtNoHP;
    private EditText editPaymentDate;
    private com.indocyber.ifgcol.db.order_detailDao order_detailDao;
    private List<order_detail> order_details;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selet_to_promise);
        this.getSupportActionBar().setHomeButtonEnabled(true);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        txtNama= (TextView) findViewById(R.id.txtNamaPemesan);
        txtAlamat= (TextView) findViewById(R.id.txtAlamat);
        txtNoHP= (TextView) findViewById(R.id.txtTelepon);
        listPromise= (RecyclerView) findViewById(R.id.listPayed);
        btnSave= (Button) findViewById(R.id.buttonSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(readonly){
                    finish();
                }else{
                    save();
                }
            }
        });
        if(readonly){
            btnSave.setText("Kembali");
        }
        btnNext= (Button) findViewById(R.id.buttonNext);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next();
            }
        });
        listPromise.setLayoutManager(new LinearLayoutManager(this));
        listPromise.setItemAnimator(new DefaultItemAnimator());
        order_detailDao=getApp().getDbSession().getOrder_detailDao();
        if (getApp().tempDataStore.get("MODE").equals("DRAFT") || getApp().tempDataStore.get("MODE").equals("NEW")) {
            Query<order_detail> queryBuilder = order_detailDao.queryBuilder()
                    .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Order_id.eq(getApp().tempDataStore.get("OrderID")))
                    .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Status.eq(getApp().tempDataStore.get("MODE")))
                    .build();
            order_details=queryBuilder.list();
        }else{
            Query<order_detail> queryBuilder = order_detailDao.queryBuilder()
                    .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Activity_id.eq(getApp().tempDataStore.get("ActivityID")))
                    .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Status.eq(getApp().tempDataStore.get("MODE")))
                    .build();
            order_details=queryBuilder.list();
        }
        adapter=new ToPayAdapter(order_details,this,R.layout.topay_item);
        listPromise.setAdapter(adapter);
        try {
            txtAlamat.setText(getApp().tempDataStore.get("CustomerAddress"));
            txtNoHP.setText(getApp().tempDataStore.get("CustomerPhone"));
            txtNama.setText(getApp().tempDataStore.get("CustomerName"));
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    @Override
    public void onUIEvent(View view, String event, String tagUI, String values) {

    }

    @Override
    public void save() {
        for(int i=0;i<order_details.size();i++){
            getApp().order_details.add(order_details.get(i));
        }
        saveForm();
        Intent main=new Intent(this,Main.class);
        main.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(main);
        finish();
    }

    @Override
    public void submit() {

    }

    @Override
    public void next() {
        getApp().order_details=new ArrayList<>();
        for (int i=0;i<order_details.size();i++){
            getApp().order_details.add(order_details.get(i));
        }
        long totalToPay=0,minToPay=0,maxToPay=0;
        for (int i=0;i<getApp().order_details.size();i++){
            totalToPay+=Long.parseLong(getApp().order_details.get(i).getTotal_amt());
            minToPay+=Long.parseLong(getApp().order_details.get(i).getMin_payment());
            maxToPay+=Long.parseLong(getApp().order_details.get(i).getMax_payment());
        }
        if(!getApp().tempDataStore.containsKey("TotalItem"))    {
            getApp().tempDataStore.put("TotalItem",String.valueOf(getApp().order_details.size()));
            getApp().tempDataStore.put("PaymentAmt",String.valueOf(totalToPay));
        }else if(!getApp().tempDataStore.get("TotalItem").equals(String.valueOf(getApp().order_details.size()))){
            getApp().tempDataStore.put("TotalItem",String.valueOf(getApp().order_details.size()));
            getApp().tempDataStore.put("PaymentAmt",String.valueOf(totalToPay));
        }
        getApp().tempDataStore.put("minPaymentAmt",String.valueOf(minToPay));
        getApp().tempDataStore.put("maxPaymentAmt",String.valueOf(maxToPay));
        getApp().tempDataStore.put("Handling",getApp().getParamValue("BHandling"));
        Intent next=new Intent(this,PTPList.class);
        next.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(next);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
