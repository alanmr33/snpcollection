package com.indocyber.ifgcol.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.clumsiam.uigenerator.generator.Engine;
import com.indocyber.ifgcol.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class PTPList extends AppActivity implements Engine {
    private TextView txtNama,txtAlamat,txtNoHP;
    int max=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_submit);
        this.getSupportActionBar().setHomeButtonEnabled(true);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        txtNama= (TextView) findViewById(R.id.txtNamaPemesan);
        txtAlamat= (TextView) findViewById(R.id.txtAlamat);
        txtNoHP= (TextView) findViewById(R.id.txtTelepon);
        try {
            txtAlamat.setText(getApp().tempDataStore.get("CustomerAddress"));
            txtNoHP.setText(getApp().tempDataStore.get("CustomerPhone"));
            txtNama.setText(getApp().tempDataStore.get("CustomerName"));
        }catch (Exception e){
            e.printStackTrace();
        }
        loadForm(R.id.fragmentContainer,"02");
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onUIEvent(View view, String event, String tagUI, String values) {

    }

    @Override
    public void save() {
        saveForm();
        Intent main=new Intent(this,Main.class);
        main.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(main);
        finish();
    }

    @Override
    public void submit() {
        String statusP=(getApp().tempDataStore.containsKey("PaymentStatus"))? getApp().tempDataStore.get("PaymentStatus") : "";
        if(statusP.equals("SP1")) {
            if (isAvailable()) {
                if (checkDate()) {
                    submitForm();
                } else {
                    Toast.makeText(this, "Tanggal Janji Maks. H+" + max, Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Janji bayar tidak tersedia", Toast.LENGTH_SHORT).show();
            }
        }else{
            submitForm();
        }
    }

    @Override
    public void next() {

    }
    public boolean isAvailable(){
        for (int i=0;i<getApp().order_details.size();i++){
            if(getApp().order_details.get(i).getPromise_available()-1<=0){
                return false;
            }
        }
        return true;
    }
    public boolean checkDate(){
        SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date datePromise=df.parse(getApp().tempDataStore.get("PromiseDate"));
            Date now= Calendar.getInstance().getTime();
            long diff = datePromise.getTime() - now.getTime();
            long diffDay = diff / (24 * 60 * 60 * 1000);
            for (int i=0;i<getApp().order_details.size();i++){
                if(diffDay>Integer.parseInt(getApp().order_details.get(i).getPromise_range())){
                    max=Integer.parseInt(getApp().order_details.get(i).getPromise_range());
                    return false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
}
