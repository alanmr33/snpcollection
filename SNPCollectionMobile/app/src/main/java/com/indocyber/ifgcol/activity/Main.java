package com.indocyber.ifgcol.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.clumsiam.uigenerator.utils.DateUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.messaging.FirebaseMessaging;
import com.indocyber.ifgcol.App;
import com.indocyber.ifgcol.db.activitiesDao;
import com.indocyber.ifgcol.db.login_logs;
import com.indocyber.ifgcol.db.login_logsDao;
import com.indocyber.ifgcol.db.notifications;
import com.indocyber.ifgcol.db.order_detailDao;
import com.indocyber.ifgcol.db.order_listDao;
import com.indocyber.ifgcol.db.tracking;
import com.indocyber.ifgcol.db.trackingDao;
import com.indocyber.ifgcol.R;
import com.indocyber.ifgcol.db.notificationsDao;
import com.indocyber.ifgcol.db.order_detail;

import org.greenrobot.greendao.query.Query;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Main extends AppActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener,View.OnClickListener {
    private RelativeLayout menuNew,menuSent,menuSaved,menuPending, menuAccount, menuLogout,menuNotif;
    private boolean reload=false;
    private TextView txtNotifNew,txtNotifDraft,txtNotifPending,txtNotifSent,txtNotifAlert,txtDate,textViewWelcome;
    private ImageView imgNotifNew,imgNotifDraft,imgNotifPending,imgNotifSent,imgNotifAlert;
    private order_detailDao orderDetailDao;
    private order_listDao orderListDao;
    private notificationsDao notificationsDao;
    private LinearLayout mainMenu;
    //location code is here
    private Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    public double latitude;
    public double longitude;
    private static final int UPDATE_INTERVAL = 3*60*1000;
    private static final int FATEST_INTERVAL = 2*60*1000;
    private static final int DISPLACEMENT = 200;
    private long lastDate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        menuNew= (RelativeLayout) findViewById(R.id.menuNew);
        menuNew.setOnClickListener(this);
        menuSent= (RelativeLayout) findViewById(R.id.menuSent);
        menuSent.setOnClickListener(this);
        menuSaved= (RelativeLayout) findViewById(R.id.menuDraft);
        menuSaved.setOnClickListener(this);
        menuPending= (RelativeLayout) findViewById(R.id.menuPending);
        menuPending.setOnClickListener(this);
        menuAccount= (RelativeLayout) findViewById(R.id.menuAccount);
        menuAccount.setOnClickListener(this);
        menuLogout= (RelativeLayout) findViewById(R.id.menuLogout);
        menuLogout.setOnClickListener(this);
        menuNotif= (RelativeLayout) findViewById(R.id.menuNotif);
        menuNotif.setOnClickListener(this);
        txtNotifNew = (TextView)findViewById(R.id.text_notif_bubbleNew);
        imgNotifNew = (ImageView) findViewById(R.id.img_notif_bubbleNew);
        txtNotifDraft = (TextView)findViewById(R.id.text_notif_bubbleDraft);
        imgNotifDraft = (ImageView) findViewById(R.id.img_notif_bubbleDraft);
        txtNotifPending = (TextView)findViewById(R.id.text_notif_bubblePending);
        imgNotifPending = (ImageView) findViewById(R.id.img_notif_bubblePending);
        txtNotifSent = (TextView)findViewById(R.id.text_notif_bubbleSent);
        imgNotifSent = (ImageView) findViewById(R.id.img_notif_bubbleSent);
        txtNotifAlert = (TextView)findViewById(R.id.text_notif_bubbleNotif);
        imgNotifAlert = (ImageView) findViewById(R.id.img_notif_bubbleNotif);
        txtDate= (TextView) findViewById(R.id.textViewDate);
        textViewWelcome= (TextView) findViewById(R.id.textViewWelcome);
        txtDate.setText(DateUtils.dateNow());
        orderDetailDao=getApp().getDbSession().getOrder_detailDao();
        orderListDao=getApp().getDbSession().getOrder_listDao();
        notificationsDao=getApp().getDbSession().getNotificationsDao();
        try {
            textViewWelcome.setText("Anda login sebagai "+getApp().getSession().getUserdata().getString("nama_karyawan"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        reloadCount();
        try {
            FirebaseMessaging
                    .getInstance()
                    .subscribeToTopic(getApp().getSession()
                            .getUserdata().getString("user_id"));
            FirebaseMessaging
                    .getInstance()
                    .subscribeToTopic(getApp().getSession()
                            .getUserdata().getString("kode_cabang"));
            FirebaseMessaging
                    .getInstance()
                    .subscribeToTopic(getApp().getSession()
                            .getUserdata().getString("kode_region"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (checkPlayServices()) {
            checkLocationOn();
            buildGoogleApiClient();
            createLocationRequest();
        }
        if(!((App)getApplicationContext()).mBound){
            ((App)getApplicationContext()).bindService();
        }
        lastDate=Calendar.getInstance().getTimeInMillis();
    }
    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
        if(((App)getApplicationContext()).mService!=null && ((App)getApplicationContext()).mBound){
            ((App)getApplicationContext()).mService.getData(false);
            ((App)getApplicationContext()).mService.sendtData(false);
        }
    }
    @Override
    protected void onStop() {
        super.onStop();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mGoogleApiClient!=null){
            if(mGoogleApiClient.isConnected()) {
                stopLocationUpdates();
                mGoogleApiClient.disconnect();
            }
        }
    }
    @Override
    public void onResume(){
        super.onResume();
        if(checkPlayServices()){
            if(!mGoogleApiClient.isConnected() && mGoogleApiClient != null){
                mGoogleApiClient.connect();
                if(mGoogleApiClient.isConnected()){
                    startLocationUpdates();
                }
            }
        }
        if(getApp().getSession().isBlocked()){
            AlertDialog.Builder builder= new AlertDialog.Builder(this);
            builder.setMessage("App tidak bisa diakses untuk sementara waktu. Silahkan kembali beberapa saat lagi !");
            builder.setCancelable(false);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    finish();
                }
            });
            builder.show();
        }else{
            if(((App)getApplicationContext()).mService!=null && ((App)getApplicationContext()).mBound){
                ((App)getApplicationContext()).mService.sendTracking();
            }
        }
        reloadCount();
    }
    public void reloadCount(){
        int timeout=3000;
        if(!reload){
            timeout=100;
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                newOrderListCount();
                draftOrderListCount();
                pendingOrderListCount();
                sentOrderListCount();
                notifOrderListCount();
                reloadCount();
            }
        },timeout);
    }
    private void newOrderListCount(){
        Query<order_detail> listQuery=orderDetailDao.queryBuilder()
                .where(order_detailDao.Properties.Enabled.eq(true))
                .where(order_detailDao.Properties.Status.eq("NEW"))
                .build();
        List<order_detail> order_lists=listQuery.list();
        int size=order_lists.size();
        if (size==0){
            txtNotifNew.setVisibility(View.INVISIBLE);
            imgNotifNew.setVisibility(View.INVISIBLE);
        }
        else if (size>99) {
            txtNotifNew.setText("99+");
            txtNotifNew.setVisibility(View.VISIBLE);
            imgNotifNew.setVisibility(View.VISIBLE);
        }
        else {
            txtNotifNew.setText(String.valueOf(size));
            txtNotifNew.setVisibility(View.VISIBLE);
            imgNotifNew.setVisibility(View.VISIBLE);
        }
    }
    private void draftOrderListCount(){
        Query<order_detail> listQuery=orderDetailDao.queryBuilder()
                .where(order_detailDao.Properties.Enabled.eq(true))
                .where(order_detailDao.Properties.Status.eq("DRAFT"))
                .build();
        List<order_detail> order_lists=listQuery.list();
        int size=order_lists.size();
        if (size==0){
            txtNotifDraft.setVisibility(View.INVISIBLE);
            imgNotifDraft.setVisibility(View.INVISIBLE);
        }
        else if (size>99) {
            txtNotifDraft.setText("99+");
            txtNotifDraft.setVisibility(View.VISIBLE);
            imgNotifDraft.setVisibility(View.VISIBLE);
        }
        else {
            txtNotifDraft.setText(String.valueOf(size));
            txtNotifDraft.setVisibility(View.VISIBLE);
            imgNotifDraft.setVisibility(View.VISIBLE);
        }
    }
    private void pendingOrderListCount(){
        Query<order_detail> listQuery=orderDetailDao.queryBuilder()
                .where(order_detailDao.Properties.Enabled.eq(true))
                .where(order_detailDao.Properties.Status.eq("PENDING"))
                .build();
        List<order_detail> order_lists=listQuery.list();
        int size=order_lists.size();
        if (size==0){
            txtNotifPending.setVisibility(View.INVISIBLE);
            imgNotifPending.setVisibility(View.INVISIBLE);
        }
        else if (size>99) {
            txtNotifPending.setText("99+");
            txtNotifPending.setVisibility(View.VISIBLE);
            imgNotifPending.setVisibility(View.VISIBLE);
        }
        else {
            txtNotifPending.setText(String.valueOf(size));
            txtNotifPending.setVisibility(View.VISIBLE);
            imgNotifPending.setVisibility(View.VISIBLE);
        }
    }
    private void sentOrderListCount(){
        Query<order_detail> listQuery=orderDetailDao.queryBuilder()
                .where(order_detailDao.Properties.Enabled.eq(true))
                .where(order_detailDao.Properties.Status.eq("SENT"))
                .build();
        List<order_detail> order_lists=listQuery.list();
        int size=order_lists.size();
        if (size==0){
            txtNotifSent.setVisibility(View.INVISIBLE);
            imgNotifSent.setVisibility(View.INVISIBLE);
        }
        else if (size>99) {
            txtNotifSent.setText("99+");
            txtNotifSent.setVisibility(View.VISIBLE);
            imgNotifSent.setVisibility(View.VISIBLE);
        }
        else {
            txtNotifSent.setText(String.valueOf(size));
            txtNotifSent.setVisibility(View.VISIBLE);
            imgNotifSent.setVisibility(View.VISIBLE);
        }
    }
    private void notifOrderListCount(){
        Query<notifications> listQuery=notificationsDao.queryBuilder()
                .where(com.indocyber.ifgcol.db.notificationsDao.Properties.Is_read.eq(false))
                .build();
        List<notifications> order_lists=listQuery.list();
        int size=order_lists.size();
        if (size==0){
            txtNotifAlert.setVisibility(View.INVISIBLE);
            imgNotifAlert.setVisibility(View.INVISIBLE);
        }
        else if (size>99) {
            txtNotifAlert.setText("99+");
            txtNotifAlert.setVisibility(View.VISIBLE);
            imgNotifAlert.setVisibility(View.VISIBLE);
        }
        else {
            txtNotifAlert.setText(String.valueOf(size));
            txtNotifAlert.setVisibility(View.VISIBLE);
            imgNotifAlert.setVisibility(View.VISIBLE);
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.menuNew :
                Intent newI=new Intent(Main.this,NewOrder.class);
                startActivity(newI);
                break;
            case R.id.menuNotif :
                Intent newN=new Intent(Main.this,NotificationList.class);
                newN.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(newN);
                break;
            case R.id.menuDraft :
                Intent newD=new Intent(Main.this,SavedOrder.class);
                newD.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(newD);
                break;
            case R.id.menuPending :
                Intent newP=new Intent(Main.this,PendingOrder.class);
                newP.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(newP);
                break;
            case R.id.menuSent :
                Intent newS=new Intent(Main.this,SentOrder.class);
                newS.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(newS);
                break;
            case R.id.menuAccount :
                Intent newA=new Intent(Main.this,Setting.class);
                startActivity(newA);
                break;
            case R.id.menuLogout :
                try {
                    login_logsDao login_logsDao=((App)getApplication()).getDbSession().getLogin_logsDao();
                    SimpleDateFormat dateFormatA=new SimpleDateFormat("yyyy-MM-dd");
                    SimpleDateFormat dateFormatB=new SimpleDateFormat("HH:mm:ss");
                    Query<login_logs> login_logsQuery=login_logsDao.queryBuilder()
                            .where(com.indocyber.ifgcol.db.login_logsDao.Properties.Login_date.eq(dateFormatA.format(Calendar.getInstance().getTime())))
                            .where(com.indocyber.ifgcol.db.login_logsDao.Properties.Logout.isNull())
                            .where(com.indocyber.ifgcol.db.login_logsDao.Properties.User_id.eq(getApp().getSession().getUserdata().getString("userID")))
                            .orderDesc(com.indocyber.ifgcol.db.login_logsDao.Properties.Login_date)
                            .build();
                    List<login_logs> login_logsList=login_logsQuery.list();
                    if(login_logsList.size()>0){
                        login_logsList.get(0).setLogout(dateFormatB.format(Calendar.getInstance().getTime()));
                        login_logsDao.insertOrReplace(login_logsList.get(0));
                    }
                } catch (JSONException e) {
                    FirebaseCrash.log(e.getMessage());
                }
                getApp().getSession().setLogin(false);
                Intent newL=new Intent(Main.this,Login.class);
                startActivity(newL);
                finish();
                break;
        }
    }
    /**
     * Create Api Client
     * */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
    }
    public void checkLocationOn(){
        LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}
        if(!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setCancelable(false);
            dialog.setMessage("Hidupkan lokasi anda untuk mengakses survey app !");
            dialog.setPositiveButton("Hidupkan", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                    //get gps
                }
            });
            dialog.setNegativeButton("Keluar", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    paramDialogInterface.dismiss();
                    finish();
                }
            });
            dialog.show();
        }
    }
    /**
     check location Service
     **/
    private boolean checkPlayServices() {
        int resultCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GoogleApiAvailability.getInstance().isUserResolvableError(resultCode)) {
                int locationRequestCode = 1000;
                Log.e("Error Loc",GoogleApiAvailability.getInstance().getErrorString(resultCode));
                GoogleApiAvailability.getInstance().getErrorDialog(this, locationRequestCode, resultCode).show();
            } else {
                Toast.makeText(getApplicationContext(), "This device is not supported.", Toast.LENGTH_LONG).show();
                finish();
            }
            Log.i("Location Service","Location Service Is Turned Off");
            AlertDialog.Builder msg=new AlertDialog.Builder(this);
            msg.setCancelable(false);
            msg.setMessage("Please , turn on location to user survey app !");
            msg.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent settings = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(settings);
                }
            });
            msg.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            AlertDialog dialog=msg.create();
            dialog.show();
            return false;
        }
        return true;
    }

    private void updateLocationData() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            latitude = mLastLocation.getLatitude();
            longitude = mLastLocation.getLongitude();
            getApp().location=latitude+","+longitude;
            if(mLastLocation.getSpeed()<40) {
                long nowL=Calendar.getInstance().getTimeInMillis();
                if((nowL-lastDate)>=getApp().scheduler.get("tracking_time")) {
                    lastDate=nowL;
                    ((App) getApplication()).getSession().setLocation(latitude + "," + longitude);
                    getApp().location = latitude + "," + longitude;
                    Log.i("LOC", "location " + latitude + "," + longitude);
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date now = Calendar.getInstance().getTime();
                    trackingDao trackingDao = ((App) getApplication()).getDbSession().getTrackingDao();
                    try {
                        tracking tracking = new tracking();
                        tracking.setPosition(latitude + "," + longitude);
                        tracking.setTracking_date(dateFormat.format(now));
                        tracking.setTracking_id(Calendar.getInstance().getTimeInMillis());
                        tracking.setUser_id(((App) getApplication())
                                .getSession()
                                .getUserdata().getString("user_id"));
                        trackingDao.insertOrReplace(tracking);
                        getApp().mService.sendTracking();

                    } catch (Exception e) {
                        e.printStackTrace();
                        FirebaseCrash.log(e.getMessage());
                    }
                }
            }
        }else{
            Log.i("Location Service","Received location nulled");
            if(mGoogleApiClient.isConnected()){
                startLocationUpdates();
            }else{
                mGoogleApiClient.connect();
                startLocationUpdates();
            }
        }
    }
    protected void createLocationRequest() {
        Log.i("Location Service","Create Location Request");
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
    }
    //receive location
    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions();
            return;
        }
        Log.i("Location Service","Starting Update Location");
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }
    protected void stopLocationUpdates() {
        Log.i("Location Service","Stoping Update Location");
        if(mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }
    //Requesting permission location
    private void requestPermissions(){
        Boolean locationCoarse=ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_COARSE_LOCATION);
        Boolean locationFine=ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        if(locationCoarse || locationFine){
            ActivityCompat.requestPermissions(this,new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION}, 110);
        }
    }
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
        updateLocationData();
    }

    @Override
    public void onConnectionSuspended(int i) {
        // connect on susspend
        mGoogleApiClient.connect();
        startLocationUpdates();
        updateLocationData();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("Location Request", "Connection failed: Error Code = "+ connectionResult.getErrorCode());
        Log.e("Location Request", "Connection failed: Error Message = "+ connectionResult.getErrorMessage());
    }

    @Override
    public void onLocationChanged(Location location) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions();
            return;
        }
        if(mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            updateLocationData();
        }
    }
    @Override
    public void onBackPressed() {
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
    }
}
