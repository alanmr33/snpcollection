package com.indocyber.ifgcol.library;

/**
 * Created by Indocyber on 26/03/2018.
 */

public abstract class AppSettings {
    public static final boolean DEBUG=true;
    public static final String QA_SERVER="http://snp.clumsia.com/mobile/mobile/";
//    public static final String QA_SERVER="http://192.168.42.127:8081/mobile/";
    public static final String PROD_SERVER="https://local/";
    public static final String SERVER=(DEBUG)? QA_SERVER : PROD_SERVER ;
    public static final String TRACK_URL=SERVER+"tracking";
    public static final String ORDER_URL=SERVER+"order";
    public static final String SUBMIT_URL=SERVER+"order";
    public static final String LOG_URL=SERVER+"login_logs";
    public static final String SYNC_URL=SERVER+"sync";
    public static final String LOGIN_URL=SERVER+"login";
    public static final String SETTING_URL=SERVER+"save_settings";
    public static final int SPLASH=3000;
    public static final int PERMISSION_CODE=1000;
    public static final int TRACK_TIME=2*60*1000;
    public static final int LOG_TIME=10*60*1000;
    public static final int SUBMIT_TIME=60*1000;
    public static final int GET_TIME=60*60*1000;
}
