package com.indocyber.ifgcol.UI;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.View;
import android.widget.TextView;

import com.indocyber.ifgcol.db.fields;


/**
 * Created by Indocyber on 31/10/2017.
 */

public abstract class LabelUI {
    public static final View create(Activity activity, View view, final fields props) {
        final TextView component=new TextView(activity);
        component.setTypeface(Typeface.DEFAULT_BOLD);
        if(!props.getField_label().equalsIgnoreCase("BLANK")) {
            component.setText(props.getField_label()+" ");
        }else{
            component.setText("");
        }
        return component;
    }
}