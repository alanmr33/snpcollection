package com.indocyber.ifgcol.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.StatFs;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.clumsiam.uigenerator.utils.DateUtils;
import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.ifgcol.App;
import com.indocyber.ifgcol.db.login_logs;
import com.indocyber.ifgcol.R;
import com.indocyber.ifgcol.db.login_logsDao;
import com.indocyber.ifgcol.library.AppSettings;

import org.greenrobot.greendao.query.Query;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class Splash extends AppActivity {
    private boolean permissionRequest=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        check();
    }
    @Override
    protected void onResume(){
        super.onResume();
        check();
    }
    private void check(){
        SimpleDateFormat dateFormat=new SimpleDateFormat("dd-MM-yyyy");
        if(!dateFormat.format(Calendar.getInstance().getTime()).equals(getApp().getSession().getLastLogin())){
            if(getApp().getSession().isLogin()){
                try {
                    login_logsDao login_logsDao=((App)getApplication()).getDbSession().getLogin_logsDao();
                    Query<login_logs> login_logsQuery=login_logsDao.queryBuilder()
                            .where(com.indocyber.ifgcol.db.login_logsDao.Properties.Login_date.eq(DateUtils.dateYesterday()))
                            .where(com.indocyber.ifgcol.db.login_logsDao.Properties.Logout.isNull())
                            .where(com.indocyber.ifgcol.db.login_logsDao.Properties.User_id
                                    .eq(getApp().getSession().getUserdata().getString("userID")))
                            .build();
                    List<login_logs> login_logsList=login_logsQuery.list();
                    if(login_logsList.size()>0){
                        login_logsList.get(0).setLogout("00:00:00");
                        login_logsDao.insertOrReplace(login_logsList.get(0));
                    }
                } catch (JSONException e) {
                    FirebaseCrash.log(e.getMessage());
                }
            }
            getApp().getSession().setLogin(false);
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(Build.VERSION.SDK_INT<19){
                    AlertDialog.Builder builder=new AlertDialog.Builder(Splash.this);
                    builder.setMessage("Perangkat tidak didukung . Min. Kitkat");
                    builder.setCancelable(false);
                    AlertDialog dialog=builder.create();
                    dialog.show();
                }else {
                    if(checkPermission()){
                        loginCheck();
                    }else {
                        if (Build.VERSION.SDK_INT >= 22) {
                            requestPermissions();
                        } else {
                            permissionRequest = false;
                            loginCheck();
                        }
                    }
                }
            }
        }, AppSettings.SPLASH);
    }
    public void loadDevice(){
        TelephonyManager mngr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        try {
            String deviceID = (!mngr.getDeviceId().equals("") || mngr.getDeviceId() != null) ? mngr.getDeviceId() : "3210140110950022";
            getApp().getSession().setDevice(deviceID);
        }catch (Exception e){
            String deviceID = "3210140110950022";
            getApp().getSession().setDevice(deviceID);
        }
    }
    private void loginCheck(){
        loadDevice();
        if(getApp().getSession().isSynced()) {
            try {
                JSONObject scheduler=getApp().getParamObject("Scheduler");
                getApp().scheduler.put("tracking_time",scheduler.getLong("tracking_time"));
                getApp().scheduler.put("log_time",scheduler.getLong("log_time"));
                getApp().scheduler.put("submit_time",scheduler.getLong("submit_time"));
                getApp().scheduler.put("get_time",scheduler.getLong("get_time"));
            }catch (Exception e){
                getApp().scheduler.put("tracking_time", (long) AppSettings.TRACK_TIME);
                getApp().scheduler.put("log_time", (long) AppSettings.LOG_TIME);
                getApp().scheduler.put("submit_time", (long) AppSettings.SUBMIT_TIME);
                getApp().scheduler.put("get_time", (long) AppSettings.GET_TIME);
            }
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
                StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
                long bytesAvailable = 0;
                bytesAvailable = stat.getFreeBytes();
                long megAvailable = bytesAvailable / 1048576;
                if(megAvailable<=100) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                    dialog.setMessage("Penyimpanan tidak mencukupi untuk menjalankan App (< 100 MB)");
                    dialog.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    });
                    AlertDialog dialogMemory=dialog.create();
                    dialogMemory.show();
                }else {
                    if (getApp().getSession().isLogin()) {
                        Intent main = new Intent(Splash.this, Main.class);
                        startActivity(main);
                        finish();
                    } else {
                        Intent login = new Intent(Splash.this, Login.class);
                        startActivity(login);
                        finish();
                    }
                }
            } else{
                if (getApp().getSession().isLogin()) {
                    Intent main = new Intent(Splash.this, Main.class);
                    startActivity(main);
                    finish();
                } else {
                    Intent login = new Intent(Splash.this, Login.class);
                    startActivity(login);
                    finish();
                }
            }
        }else {
            Intent ifg = new Intent(Splash.this, IFGSettings.class);
            startActivity(ifg);
        }
    }
    /*
    check permission in realtime
     */
    private Boolean checkPermission(){
        int phoneState= ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        int storagePermission= ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int storagePermissionW= ActivityCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int cameraP= ActivityCompat.checkSelfPermission(this,Manifest.permission.CAMERA);
        int locationCoarse=ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION);
        int locationFine=ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION);
        if(phoneState== PackageManager.PERMISSION_GRANTED && storagePermission==PackageManager.PERMISSION_GRANTED && locationCoarse==PackageManager.PERMISSION_GRANTED
                && locationFine==PackageManager.PERMISSION_GRANTED && storagePermissionW==PackageManager.PERMISSION_GRANTED
                &&cameraP==PackageManager.PERMISSION_GRANTED){
            return true;
        }else{
            return false;
        }
    }
    //Requesting permission
    private void requestPermissions() {
        if (permissionRequest) {
            permissionRequest=false;
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_PHONE_STATE}, AppSettings.PERMISSION_CODE);
        }
    }
    /*
    on get permission from intent
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
        if(requestCode==AppSettings.PERMISSION_CODE){
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED
                    && grantResults[3] == PackageManager.PERMISSION_GRANTED && grantResults[4] == PackageManager.PERMISSION_GRANTED){
               loginCheck();
            }else{
                Toast.makeText(this,"Please Grant Our Permission !",Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }
}
