package com.indocyber.ifgcol.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indocyber.ifgcol.R;

/**
 * Created by Indocyber on 28/03/2018.
 */

public class NewHolder extends RecyclerView.ViewHolder {
    public TextView textViewNamaPemesan,textViewNoHP,textViewAlamat,textViewTagihan,textStatus,textViewLPKNO;
    public LinearLayout newOrderItem,layoutPengiriman;
    public NewHolder(View itemView) {
        super(itemView);
        textViewLPKNO= (TextView) itemView.findViewById(R.id.textViewLPKNO);
        textViewNamaPemesan= (TextView) itemView.findViewById(R.id.textViewNamaPemesan);
        textViewNoHP= (TextView) itemView.findViewById(R.id.textViewNoHP);
        textViewAlamat= (TextView) itemView.findViewById(R.id.textViewAlamat);
        textViewTagihan= (TextView) itemView.findViewById(R.id.textViewCount);
        newOrderItem= (LinearLayout) itemView.findViewById(R.id.newOrderItem);
        layoutPengiriman= (LinearLayout) itemView.findViewById(R.id.layoutStatus);
        textStatus= (TextView) itemView.findViewById(R.id.textStatus);
    }
}
