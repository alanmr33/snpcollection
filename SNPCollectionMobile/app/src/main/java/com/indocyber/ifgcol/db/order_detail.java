package com.indocyber.ifgcol.db;

import org.greenrobot.greendao.annotation.*;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END

/**
 * Entity mapped to table "ORDER_DETAIL".
 */
@Entity
public class order_detail {

    @Id
    private Long detail_id;
    private String order_id;
    private String contract_no;
    private String installment_no;
    private String lpk_no;
    private String nama_barang;
    private java.util.Date due_date;
    private String installment_amt;
    private String penalty_amt;
    private String collection_fee;
    private String total_amt;
    private String receipt_no;
    private String min_payment;
    private String max_payment;
    private java.util.Date repayment_date;
    private String notes;
    private Integer promise_count;
    private Integer promise_available;
    private String promise_range;
    private String deposit_amt;
    private String data;
    private String status;
    private Boolean enabled;
    private String user_id;
    private String activity_id;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    @Generated
    public order_detail() {
    }

    public order_detail(Long detail_id) {
        this.detail_id = detail_id;
    }

    @Generated
    public order_detail(Long detail_id, String order_id, String contract_no, String installment_no, String lpk_no, String nama_barang, java.util.Date due_date, String installment_amt, String penalty_amt, String collection_fee, String total_amt, String receipt_no, String min_payment, String max_payment, java.util.Date repayment_date, String notes, Integer promise_count, Integer promise_available, String promise_range, String deposit_amt, String data, String status, Boolean enabled, String user_id, String activity_id) {
        this.detail_id = detail_id;
        this.order_id = order_id;
        this.contract_no = contract_no;
        this.installment_no = installment_no;
        this.lpk_no = lpk_no;
        this.nama_barang = nama_barang;
        this.due_date = due_date;
        this.installment_amt = installment_amt;
        this.penalty_amt = penalty_amt;
        this.collection_fee = collection_fee;
        this.total_amt = total_amt;
        this.receipt_no = receipt_no;
        this.min_payment = min_payment;
        this.max_payment = max_payment;
        this.repayment_date = repayment_date;
        this.notes = notes;
        this.promise_count = promise_count;
        this.promise_available = promise_available;
        this.promise_range = promise_range;
        this.deposit_amt = deposit_amt;
        this.data = data;
        this.status = status;
        this.enabled = enabled;
        this.user_id = user_id;
        this.activity_id = activity_id;
    }

    public Long getDetail_id() {
        return detail_id;
    }

    public void setDetail_id(Long detail_id) {
        this.detail_id = detail_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getContract_no() {
        return contract_no;
    }

    public void setContract_no(String contract_no) {
        this.contract_no = contract_no;
    }

    public String getInstallment_no() {
        return installment_no;
    }

    public void setInstallment_no(String installment_no) {
        this.installment_no = installment_no;
    }

    public String getLpk_no() {
        return lpk_no;
    }

    public void setLpk_no(String lpk_no) {
        this.lpk_no = lpk_no;
    }

    public String getNama_barang() {
        return nama_barang;
    }

    public void setNama_barang(String nama_barang) {
        this.nama_barang = nama_barang;
    }

    public java.util.Date getDue_date() {
        return due_date;
    }

    public void setDue_date(java.util.Date due_date) {
        this.due_date = due_date;
    }

    public String getInstallment_amt() {
        return installment_amt;
    }

    public void setInstallment_amt(String installment_amt) {
        this.installment_amt = installment_amt;
    }

    public String getPenalty_amt() {
        return penalty_amt;
    }

    public void setPenalty_amt(String penalty_amt) {
        this.penalty_amt = penalty_amt;
    }

    public String getCollection_fee() {
        return collection_fee;
    }

    public void setCollection_fee(String collection_fee) {
        this.collection_fee = collection_fee;
    }

    public String getTotal_amt() {
        return total_amt;
    }

    public void setTotal_amt(String total_amt) {
        this.total_amt = total_amt;
    }

    public String getReceipt_no() {
        return receipt_no;
    }

    public void setReceipt_no(String receipt_no) {
        this.receipt_no = receipt_no;
    }

    public String getMin_payment() {
        return min_payment;
    }

    public void setMin_payment(String min_payment) {
        this.min_payment = min_payment;
    }

    public String getMax_payment() {
        return max_payment;
    }

    public void setMax_payment(String max_payment) {
        this.max_payment = max_payment;
    }

    public java.util.Date getRepayment_date() {
        return repayment_date;
    }

    public void setRepayment_date(java.util.Date repayment_date) {
        this.repayment_date = repayment_date;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Integer getPromise_count() {
        return promise_count;
    }

    public void setPromise_count(Integer promise_count) {
        this.promise_count = promise_count;
    }

    public Integer getPromise_available() {
        return promise_available;
    }

    public void setPromise_available(Integer promise_available) {
        this.promise_available = promise_available;
    }

    public String getPromise_range() {
        return promise_range;
    }

    public void setPromise_range(String promise_range) {
        this.promise_range = promise_range;
    }

    public String getDeposit_amt() {
        return deposit_amt;
    }

    public void setDeposit_amt(String deposit_amt) {
        this.deposit_amt = deposit_amt;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getActivity_id() {
        return activity_id;
    }

    public void setActivity_id(String activity_id) {
        this.activity_id = activity_id;
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
