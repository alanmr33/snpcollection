package com.indocyber.ifgcol.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.clumsiam.uigenerator.utils.Formating;
import com.indocyber.ifgcol.activity.SelectToPay;
import com.indocyber.ifgcol.activity.AppActivity;
import com.indocyber.ifgcol.adapter.holder.SelectToPayHolder;
import com.indocyber.ifgcol.db.order_detail;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Indocyber on 28/03/2018.
 */

public class SelectToPayAdapter extends RecyclerView.Adapter<SelectToPayHolder> {
    private AppActivity activity;
    private List<order_detail> order_details;
    private int layout;
    private boolean isAll;
    public SelectToPayAdapter(List<order_detail> order_details,AppActivity activity,int layout,boolean isAll){
        this.activity=activity;
        this.order_details=order_details;
        this.layout=layout;
        this.isAll=isAll;
    }
    @Override
    public SelectToPayHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(this.layout, parent, false);
        return new SelectToPayHolder(v);
    }

    @Override
    public void onBindViewHolder(final SelectToPayHolder holder, int position) {
        final order_detail order_detail=order_details.get(position);
        holder.textViewContractNo.setText(order_detail.getContract_no());
        holder.textViewNamaBarang.setText(order_detail.getNama_barang().toUpperCase());
        holder.textViewInstallmentNo.setText(order_detail.getInstallment_no());
        holder.textViewInstallmentAMT.setText("Rp. "+ Formating.currency(Integer.parseInt(order_detail.getInstallment_amt())));
        holder.txtViewPenalty.setText("Rp. "+ Formating.currency(Integer.parseInt(order_detail.getPenalty_amt())));
        holder.txtViewCF.setText("Rp. "+ Formating.currency(Integer.parseInt(order_detail.getCollection_fee())));
        holder.txtViewTotal.setText("Rp. "+ Formating.currency(Integer.parseInt(order_detail.getTotal_amt())));
        holder.selectToPayItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.checkBoxPay.isChecked()){
                    holder.checkBoxPay.setChecked(false);
                }else{
                    holder.checkBoxPay.setChecked(true);
                }
            }
        });
        holder.checkBoxPay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    if(activity.getApp().order_details.size()<order_details.size()) {
                        boolean exist=false;
                        for (int i=0;i<activity.getApp().order_details.size();i++){
                            if(activity.getApp().order_details.get(i).equals(order_detail)){
                                exist=true;
                            }
                        }
                        if(!exist){
                            activity.getApp().order_details.add(order_detail);
                        }
                        ((SelectToPay) activity).Count();
                        if (activity.getApp().order_details.size() == order_details.size()) {
                            ((SelectToPay) activity).changeButton(false);
                        }
                    }
                }else {
                    activity.getApp().order_details.remove(order_detail);
                    ((SelectToPay) activity).Count();
                    ((SelectToPay)activity).changeButton(true);

                }
            }
        });
        if(isAll){
            holder.checkBoxPay.setChecked(true);
        }else{
            holder.checkBoxPay.setChecked(false);
            for (int i=0;i<activity.getApp().order_details.size();i++){
                if(activity.getApp().order_details.get(i).equals(order_detail)){
                    holder.checkBoxPay.setChecked(true);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return order_details.size();
    }
}
