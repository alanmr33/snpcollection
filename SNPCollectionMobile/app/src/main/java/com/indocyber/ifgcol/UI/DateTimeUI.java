package com.indocyber.ifgcol.UI;

import android.app.Activity;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clumsiam.uigenerator.generator.KangEventHandler;
import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.ifgcol.R;
import com.indocyber.ifgcol.db.fields;

import org.json.JSONObject;

/**
 * Created by Indocyber on 07/05/2018.
 */

public abstract class DateTimeUI {
    public static final View create(Activity activity, View view, final fields props, boolean enable, KangEventHandler eventHandler, Fragment fragment) {
        LinearLayout component=new LinearLayout(activity);
        if(props.getField_visibility().equalsIgnoreCase("visible")){
            component.setVisibility(View.VISIBLE);
        }else{
            component.setVisibility(View.GONE);
        }
        component.setTag(props.getField_name()+"-layout");
        float weight=1;
        if(!props.getField_weight().replace(",",".").equals("")){
            weight=Float.parseFloat(props.getField_weight().replace(",","."));
        }
        LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,weight);
        layoutParams.setMargins(3,2,3,2);
        component.setLayoutParams(layoutParams);
        component.setOrientation(LinearLayout.VERTICAL);
        /*
        label text
         */
        if(props.getField_label()!=null && !props.getField_label().equals("")) {

            JSONObject extras;
            try {
                extras = new JSONObject(props.getField_extra());
                if(extras.has("validation")){
                    boolean required=(extras.getJSONObject("validation").has("required"))? extras.getJSONObject("validation").getBoolean("required") : false;
                    if(required){
                        LinearLayout labelLayout = new LinearLayout(activity);
                        labelLayout.setOrientation(LinearLayout.HORIZONTAL);
                        TextView lblRequired = new TextView(activity);
                        lblRequired.setText("*");
                        lblRequired.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            lblRequired.setTextColor(activity.getResources().getColor(R.color.colorPrimaryDark, null));
                        } else {
                            lblRequired.setTextColor(activity.getResources().getColor(R.color.colorPrimaryDark));
                        }
                        labelLayout.addView(LabelUI.create(activity, component, props));
                        labelLayout.addView(lblRequired);
                        component.addView(labelLayout);
                    }else{
                        component.addView(LabelUI.create(activity, component, props));
                    }
                }else{
                    component.addView(LabelUI.create(activity, component, props));
                }
            }catch (Exception e){
                FirebaseCrash.log(e.getMessage());
            }
        }
        component.addView(DatePickerUI.create(activity,component,props,enable,eventHandler,fragment));
        return component;
    }
}
