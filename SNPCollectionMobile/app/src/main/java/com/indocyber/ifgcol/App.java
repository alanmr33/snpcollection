package com.indocyber.ifgcol;

import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.StrictMode;
import android.support.multidex.MultiDex;

import com.clumsiam.uigenerator.utils.Session;
import com.indocyber.ifgcol.db.params;
import com.indocyber.ifgcol.db.DaoMaster;
import com.indocyber.ifgcol.db.DaoSession;
import com.indocyber.ifgcol.db.order_detail;
import com.indocyber.ifgcol.db.paramsDao;
import com.indocyber.ifgcol.service.AppService;
import com.yan.netmanager.NetStatus;
import com.yan.netmanager.NetStatusManager;

import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.query.Query;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Indocyber on 19/02/2018.
 */

public class App extends Application {
    public HashMap<String,String> tempDataStore=new HashMap<>();
    public HashMap<String,Long> scheduler=new HashMap<>();
    public ArrayList<order_detail> order_details=new ArrayList<>();
    private DaoSession daoSession;
    private Session session;
    public AppService mService;
    public boolean mBound = false;
    public String location="";
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            AppService.AppServiceBinder binder = (AppService.AppServiceBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, this.getExternalFilesDir("db")
                .getPath() + "/snp_col.db");
        Database db = helper.getEncryptedWritableDb("1ndocyber");
        daoSession = new DaoMaster(db).newSession();
        session=new Session("snp_col",this);
        if(!mBound){
            bindService();
        }
    }
    public DaoSession getDbSession() {
        return daoSession;
    }
    public Session getSession(){
        return session;
    }
    public  void bindService(){
        Intent intent = new Intent(getApplicationContext(), AppService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        mBound=true;
    }
    public void clearTempDataStore(){
        tempDataStore=new HashMap<>();
    }
    public void clearChoice(){
        order_details=new ArrayList<>();
    }
    public JSONObject getParamObject(String name){
        try {
            paramsDao paramsDao=daoSession.getParamsDao();
            Query<params> paramsQuery=paramsDao.queryBuilder()
                    .where(com.indocyber.ifgcol.db.paramsDao.Properties.Param_condition.eq(name))
                    .build();
            List<params> paramsList=paramsQuery.list();
            return new JSONObject(paramsList.get(0).getParam_description());
        } catch (Exception e) {
            return new JSONObject();
        }
    }
    public List<params> getParams(String name){
        paramsDao paramsDao=daoSession.getParamsDao();
        Query<params> paramsQuery=paramsDao.queryBuilder()
                .where(com.indocyber.ifgcol.db.paramsDao.Properties.Param_condition.eq(name))
                .build();
        return paramsQuery.list();
    }
    public JSONArray getParamArray(String name){
        try {
            paramsDao paramsDao=daoSession.getParamsDao();
            Query<params> paramsQuery=paramsDao.queryBuilder()
                    .where(com.indocyber.ifgcol.db.paramsDao.Properties.Param_condition.eq(name))
                    .build();
            List<params> paramsList=paramsQuery.list();
            return new JSONArray(paramsList.get(0).getParam_description());
        } catch (Exception e) {
            return new JSONArray();
        }
    }
    public String getParamValue(String name){
        try{
            paramsDao paramsDao=daoSession.getParamsDao();
            Query<params> paramsQuery=paramsDao.queryBuilder()
                    .where(com.indocyber.ifgcol.db.paramsDao.Properties.Param_condition.eq(name))
                    .build();
            List<params> paramsList=paramsQuery.list();
            return paramsList.get(0).getParam_description();
        }catch (Exception e){
            return new String("");
        }
    }
}
