package com.indocyber.ifgcol.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.clumsiam.uigenerator.utils.Base64;
import com.clumsiam.uigenerator.utils.Resizer;
import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.ifgcol.App;
import com.indocyber.ifgcol.activity.DetailOrder;
import com.indocyber.ifgcol.activity.Main;
import com.indocyber.ifgcol.activity.PendingOrder;
import com.indocyber.ifgcol.adapter.holder.DraftHolder;
import com.indocyber.ifgcol.adapter.holder.NewHolder;
import com.indocyber.ifgcol.db.activities;
import com.indocyber.ifgcol.db.activitiesDao;
import com.indocyber.ifgcol.db.order_detailDao;
import com.indocyber.ifgcol.db.order_list;
import com.indocyber.ifgcol.activity.AppActivity;
import com.indocyber.ifgcol.db.order_detail;
import com.indocyber.ifgcol.library.AppSettings;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.greenrobot.greendao.query.Query;
import org.greenrobot.greendao.query.QueryBuilder;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Indocyber on 05/04/2018.
 */

public class PendingAdapter extends RecyclerView.Adapter<DraftHolder> {
    private List<activities> activitiesList;
    private AppActivity activity;
    private com.indocyber.ifgcol.db.order_detailDao order_detailDao;
    private activitiesDao activitiesDao;
    private int layout;
    public PendingAdapter(List<activities> activitiesList, AppActivity activity, int layout,order_detailDao order_detailDao,activitiesDao activitiesDao){
        this.activitiesList=activitiesList;
        this.activity=activity;
        this.layout=layout;
        this.order_detailDao=order_detailDao;
        this.activitiesDao=activitiesDao;
    }
    @Override
    public DraftHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(this.layout, parent, false);
        return new DraftHolder(v);
    }

    @Override
    public void onBindViewHolder(DraftHolder holder, int position) {
        try {
            String telepon = "";
            final activities item = activitiesList.get(position);
            JSONObject data=new JSONObject(item.getBody());
            final JSONObject input=data.getJSONObject("input");
            final JSONArray details=new JSONArray(data.getString("items"));
            if (input.has("Phone")) {
                telepon = input.getString("Phone");
            } else {
                telepon="-";
            }
            holder.textViewLPKNO.setText(input.getString("LpkNo"));
            holder.textViewNamaPemesan.setText(input.getString("CustomerName"));
            holder.textViewAlamat.setText(input.getString("CustomerAddress"));
            holder.textViewNoHP.setText(telepon);
            holder.layoutPengiriman.setVisibility(View.VISIBLE);
            holder.textStatus.setText(item.getSend_status());
            Query<order_detail> order_detailQuery=order_detailDao.queryBuilder()
                    .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Activity_id.eq(item.getActivity_id()))
                    .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Status.eq("PENDING"))
                    .build();
            List<order_detail> order_details=order_detailQuery.list();
            holder.textViewTagihan.setText(order_details.size() + " Tagihan");
            holder.newOrderItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent detail = new Intent(activity, DetailOrder.class);
                        activity.getApp().clearTempDataStore();
                        activity.getApp().clearChoice();
                        Iterator<String> keyIterator = input.keys();
                        while (keyIterator.hasNext()) {
                            String key = keyIterator.next();
                            activity.getApp().tempDataStore.put(key, input.getString(key));
                        }
                        activity.getApp().tempDataStore.put("MODE","PENDING");
                        detail.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        activity.startActivity(detail);
                    }catch (Exception e){
                        FirebaseCrash.log(e.getMessage());
                        e.printStackTrace();
                    }
                }
            });
            if(!item.getSend_status().equals("MENGIRIM")){
                holder.buttonDiscard.setVisibility(View.VISIBLE);
                holder.buttonDiscard.setText("KIRIM ULANG");
                holder.buttonDiscard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            final ProgressDialog loading = new ProgressDialog(activity);
                            loading.setCancelable(false);
                            loading.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                            loading.setMessage("Mengirim data ke server");
                            loading.show();
                            RequestParams params = new RequestParams();
                            JSONObject body = new JSONObject(item.getBody());
                            File f = new File(body.getJSONObject("input").getString("Photo-file"));
                            if ((f.length() / 1024) > 45) {
                                Resizer.compressImage(body.getJSONObject("input").getString("Photo-file"), 300);
                                f = new File(body.getJSONObject("input").getString("Photo-file"));
                            }
                            byte imageData[] = new byte[(int) f.length()];
                            try {
                                FileInputStream fis = new FileInputStream(f);
                                fis.read(imageData);
                            } catch (IOException e) {
                                FirebaseCrash.log(e.getMessage());
                            }
                            String base64 = Base64.encode(imageData);
                            body.put("image", base64);
                            params.put("order_id", item.getOrder_id());
                            params.put("body", body);
                            params.put("token", ((App) activity.getApplication()).getSession().getToken());
                            ((AppActivity) activity).httpClient.addHeader("APPID", "IFGCOL-IGLO");
                            ((AppActivity) activity).httpClient.post(AppSettings.SUBMIT_URL, params, new JsonHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                    loading.dismiss();
                                    try {
                                        if (response.getBoolean("status")) {
                                            QueryBuilder<order_detail> queryBuilder = order_detailDao.queryBuilder()
                                                    .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Activity_id.eq(item.getActivity_id()));
                                            List<order_detail> details = queryBuilder.list();
                                            for (int i = 0; i < details.size(); i++) {
                                                details.get(i).setStatus("SENT");
                                                order_detailDao.update(details.get(i));
                                            }
                                            item.setMobile_status("SENT");
                                            item.setSend_status("TERKIRIM");
                                            activitiesDao.update(item);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    ((PendingOrder)activity).getAll();
                                    Toast.makeText(activity,"Data terkirim.",Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onFailure(int statusCode, Header[] header, String body, Throwable throwable) {
                                    loading.dismiss();
                                    Log.e("Error", statusCode + " error");
                                    item.setSend_status("ANTRIAN");
                                    activitiesDao.update(item);
                                    ((PendingOrder)activity).getAll();
                                    Toast.makeText(activity,"Pengiriman gagal. Silahkan coba lagi nanti !",Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject error) {
                                    loading.dismiss();
                                    Log.e("Error", statusCode + " error");
                                    item.setSend_status("ANTRIAN");
                                    activitiesDao.update(item);
                                    ((PendingOrder)activity).getAll();
                                    Toast.makeText(activity,"Pengiriman gagal. Silahkan coba lagi nanti !",Toast.LENGTH_SHORT).show();
                                }
                            });
                        }catch (Exception e){
                            FirebaseCrash.log(e.getMessage());
                            e.printStackTrace();
                        }
                    }
                });
            }
        }catch (Exception e){
            FirebaseCrash.log(e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return activitiesList.size();
    }
}
