package com.indocyber.ifgcol.db;

import org.greenrobot.greendao.annotation.*;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END

/**
 * Entity mapped to table "EVENTS".
 */
@Entity
public class events {

    @Id
    @NotNull
    private String event_id;
    private String event_type;
    private String event_component;
    private String event_component_target;
    private String event_data;
    private String form_id;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    @Generated
    public events() {
    }

    public events(String event_id) {
        this.event_id = event_id;
    }

    @Generated
    public events(String event_id, String event_type, String event_component, String event_component_target, String event_data, String form_id) {
        this.event_id = event_id;
        this.event_type = event_type;
        this.event_component = event_component;
        this.event_component_target = event_component_target;
        this.event_data = event_data;
        this.form_id = form_id;
    }

    @NotNull
    public String getEvent_id() {
        return event_id;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setEvent_id(@NotNull String event_id) {
        this.event_id = event_id;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getEvent_component() {
        return event_component;
    }

    public void setEvent_component(String event_component) {
        this.event_component = event_component;
    }

    public String getEvent_component_target() {
        return event_component_target;
    }

    public void setEvent_component_target(String event_component_target) {
        this.event_component_target = event_component_target;
    }

    public String getEvent_data() {
        return event_data;
    }

    public void setEvent_data(String event_data) {
        this.event_data = event_data;
    }

    public String getForm_id() {
        return form_id;
    }

    public void setForm_id(String form_id) {
        this.form_id = form_id;
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
