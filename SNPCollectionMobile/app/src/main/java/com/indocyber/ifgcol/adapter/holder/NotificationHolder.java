package com.indocyber.ifgcol.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.indocyber.ifgcol.R;

/**
 * Created by Indocyber on 29/03/2018.
 */

public class NotificationHolder extends RecyclerView.ViewHolder {
    public TextView textViewNotifTitle, textViewNotifMessage, textViewNotifDate;
    public NotificationHolder(View itemView) {
        super(itemView);
        textViewNotifDate= (TextView) itemView.findViewById(R.id.textViewNotifDate);
        textViewNotifMessage= (TextView) itemView.findViewById(R.id.textViewNotifMessage);
        textViewNotifTitle= (TextView) itemView.findViewById(R.id.textViewNotifTitle);
    }
}
