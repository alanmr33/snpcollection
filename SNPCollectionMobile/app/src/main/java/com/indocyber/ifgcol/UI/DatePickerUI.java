package com.indocyber.ifgcol.UI;

import android.app.Activity;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.clumsiam.uigenerator.generator.KangEventHandler;
import com.clumsiam.uigenerator.generator.OnSetValue;
import com.clumsiam.uigenerator.ui.DatePicker;
import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.ifgcol.R;
import com.indocyber.ifgcol.activity.AppActivity;
import com.indocyber.ifgcol.db.fields;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by Indocyber on 08/05/2018.
 */

public abstract class DatePickerUI {
    public static final View create(final Activity activity, final View view, final fields props, final boolean enable, final KangEventHandler eventHandler, Fragment fragment) {
        final LinearLayout component=new LinearLayout(activity);
        LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1);
        component.setLayoutParams(layoutParams);
        component.setOrientation(LinearLayout.HORIZONTAL);

        final EditText componentEdit=new EditText(activity);
        componentEdit.setTag(props.getField_name());
        componentEdit.setEnabled(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            componentEdit.setBackground(activity.getResources().getDrawable(R.drawable.input_bg,null));
        }else{
            componentEdit.setBackground(activity.getResources().getDrawable(R.drawable.input_bg));
        }
        componentEdit.setSingleLine(true);
        LinearLayout.LayoutParams editParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1);
        componentEdit.setPadding(3,8,3,8);
        componentEdit.setLayoutParams(editParams);
        if(((AppActivity) activity).getApp().tempDataStore.containsKey(props.getField_store())){
            componentEdit.setText(((AppActivity) activity).getApp().tempDataStore.get(props.getField_store()));
        }

        ImageView imageView=new ImageView(activity);
        imageView.setImageResource(R.drawable.ic_date);
        LinearLayout.LayoutParams imageLayout=new LinearLayout.LayoutParams(48, 48
                ,0);
        imageLayout.gravity= Gravity.CENTER_HORIZONTAL;
        imageView.setLayoutParams(imageLayout);
        if(enable){
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DatePicker datePicker=new DatePicker();
                    datePicker.setOnSetvalue(new OnSetValue() {
                        @Override
                        public void OnSet(String data) {
                            SimpleDateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                            SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            try {
                                data=format.format(df.parse(data+":00"));
                            } catch (Exception e) {
                                e.printStackTrace();
                                FirebaseCrash.log(e.getMessage());
                            }
                            componentEdit.setText(data);
                            ((AppActivity) activity).getApp().tempDataStore.put(props.getField_store(),data);
                            eventHandler.onUIEvent(componentEdit,"change",componentEdit.getTag().toString(),data);
                        }
                    });
                    datePicker.show(activity.getFragmentManager(),null);
                }
            });
        }
        component.addView(componentEdit);
        component.addView(imageView);
        if(enable){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    eventHandler.onUIEvent(component,"init",props.getField_name(),"");
                }
            },1000);
        }
        return component;
    }
}
