package com.indocyber.ifgcol.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.ifgcol.activity.AppActivity;
import com.indocyber.ifgcol.activity.DetailOrder;
import com.indocyber.ifgcol.adapter.holder.NewHolder;
import com.indocyber.ifgcol.db.order_detail;
import com.indocyber.ifgcol.db.order_detailDao;
import com.indocyber.ifgcol.db.order_list;

import org.greenrobot.greendao.query.Query;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Indocyber on 28/03/2018.
 */

public class NewAdapter extends RecyclerView.Adapter<NewHolder> {
    private List<order_list> order_lists=new ArrayList<>();
    private AppActivity activity;
    private com.indocyber.ifgcol.db.order_detailDao order_detailDao;
    private int layout;
    public NewAdapter(List<order_list> order_lists, AppActivity activity, int layout,order_detailDao order_detailDao){
        this.activity=activity;
        this.layout=layout;
        this.order_detailDao=order_detailDao;
        for (int i=0;i<order_lists.size();i++){
            Query<order_detail> order_detailQuery=order_detailDao.queryBuilder()
                    .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Order_id.eq(order_lists.get(i).getOrder_id()))
                    .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Status.eq("NEW"))
                    .build();
            List<order_detail> order_details=order_detailQuery.list();
            if(order_details.size()>0){
                this.order_lists.add(order_lists.get(i));
            }
        }
    }
    @Override
    public NewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(this.layout, parent, false);
        return new NewHolder(v);
    }

    @Override
    public void onBindViewHolder(NewHolder holder, int position) {
        String telepon="";
        final order_list item=order_lists.get(position);
        if(item.getPhone_area()!=null){
               telepon+=item.getPhone_area()+item.getPhone_number();
        }else{
            if(!telepon.equals("")){
              telepon+=" , ";
            }
            telepon+=item.getMobile_prefix()+item.getMobile_no();
        }
        holder.textViewLPKNO.setText(item.getLpk_no());
        holder.textViewNamaPemesan.setText(item.getCustomer_name().toUpperCase());
        holder.textViewAlamat.setText(item.getAddress().toUpperCase()+" RT/RW "
                +item.getRt()+"/"+item.getRw()+" ,"+item.getVillage()+" , "+item.getDistrict()+" , "+item.getCity());
        holder.textViewNoHP.setText(telepon);
        Query<order_detail> order_detailQuery=order_detailDao.queryBuilder()
                .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Order_id.eq(item.getOrder_id()))
                .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Status.eq("NEW"))
                .build();
        List<order_detail> order_details=order_detailQuery.list();
        holder.textViewTagihan.setText(order_details.size()+" Tagihan");
        final String finalTelepon = telepon;
        holder.newOrderItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent detail=new Intent(activity, DetailOrder.class);
                    detail.putExtra("readonly",false);
                    activity.getApp().clearTempDataStore();
                    activity.getApp().clearChoice();
                    activity.getApp().tempDataStore.put("MODE","NEW");
                    activity.getApp().tempDataStore.put("Phone", finalTelepon);
                    activity.getApp().tempDataStore.put("LpkNo",item.getLpk_no());
                    activity.getApp().tempDataStore.put("OrderID",item.getOrder_id());
                    activity.getApp().tempDataStore.put("CustomerCode",item.getCustomer_code());
                    activity.getApp().tempDataStore.put("CustomerName",item.getCustomer_name().toUpperCase());
                    activity.getApp().tempDataStore.put("CustomerAddress",item.getAddress().toUpperCase()+" RT/RW "
                    +item.getRt()+"/"+item.getRw()+" ,"+item.getVillage()+" , "+item.getDistrict()+" , "+item.getCity());
                    activity.getApp().tempDataStore.put("CustomerPhone",item.getMobile_prefix()+item.getMobile_no());
                    activity.getApp().tempDataStore.put("CustomerTelphoneHome",item.getPhone_area()+item.getPhone_number());
                    activity.getApp().tempDataStore.put("OldCustomerName",item.getCustomer_name().toUpperCase());
                    String address=item.getAddress().toUpperCase()+" RT/RW "
                            +item.getRt()+"/"+item.getRw()+" ,"+item.getVillage()+" , "+item.getDistrict()+" , "+item.getCity();
                    activity.getApp().tempDataStore.put("OldCustomerAddress",address.replace("null","-"));
                    activity.getApp().tempDataStore.put("OldCustomerPhone",item.getMobile_prefix()+item.getMobile_no());
                    activity.getApp().tempDataStore.put("OldCustomerTelphoneHome",item.getPhone_area()+item.getPhone_number());
                    JSONObject lastDataInput=new JSONObject(item.getData());
                    Iterator<String> keyIterator=lastDataInput.keys();
                    while (keyIterator.hasNext()){
                        String key=keyIterator.next();
                        activity.getApp().tempDataStore.put(key, lastDataInput.getString(key));
                    }
                    detail.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    activity.startActivity(detail);
                } catch (Exception e) {
                    FirebaseCrash.log(e.getMessage());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return order_lists.size();
    }
}
