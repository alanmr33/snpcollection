package com.indocyber.ifgcol.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indocyber.ifgcol.R;

/**
 * Created by Indocyber on 28/03/2018.
 */

public class SelectToPayHolder extends RecyclerView.ViewHolder {
    public CheckBox checkBoxPay;
    public TextView textViewContractNo,textViewNamaBarang,textViewInstallmentNo, textViewInstallmentAMT,txtViewPenalty,txtViewCF,txtViewTotal;
    public LinearLayout selectToPayItem;
    public SelectToPayHolder(View itemView) {
        super(itemView);
        checkBoxPay= (CheckBox) itemView.findViewById(R.id.checkBoxPay);
        textViewContractNo= (TextView) itemView.findViewById(R.id.textViewContractNo);
        textViewNamaBarang= (TextView) itemView.findViewById(R.id.textViewNamaBarang);
        textViewInstallmentNo= (TextView) itemView.findViewById(R.id.textViewInstallmentNo);
        textViewInstallmentAMT= (TextView) itemView.findViewById(R.id.textViewInstallmentAMT);
        txtViewPenalty= (TextView) itemView.findViewById(R.id.textViewPenalty);
        txtViewCF= (TextView) itemView.findViewById(R.id.textViewCollectionFee);
        txtViewTotal= (TextView) itemView.findViewById(R.id.textViewTotal);
        selectToPayItem= (LinearLayout) itemView.findViewById(R.id.selectPayItem);
    }
}
