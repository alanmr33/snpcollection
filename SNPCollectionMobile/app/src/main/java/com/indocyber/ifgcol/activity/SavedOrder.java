package com.indocyber.ifgcol.activity;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.ifgcol.db.activities;
import com.indocyber.ifgcol.db.activitiesDao;
import com.indocyber.ifgcol.db.order_list;
import com.indocyber.ifgcol.R;
import com.indocyber.ifgcol.adapter.DraftAdapter;
import com.indocyber.ifgcol.library.RefreshInf;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.List;

public class SavedOrder extends AppActivity implements RefreshInf {
    private activitiesDao activitiesDao;
    private DraftAdapter adapter;
    private List<activities> activitiesList;
    private RecyclerView recyclerView;
    private EditText editTextSearch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_order);
        this.getSupportActionBar().setHomeButtonEnabled(true);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView= (RecyclerView) findViewById(R.id.savedOrderList);
        editTextSearch=(EditText) findViewById(R.id.editTextSearch);
        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(final Editable s) {
                SavedOrder.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(!s.toString().equals("")){
                            searchOrder(s.toString());
                        }else{
                            getAll();
                            generateList();
                        }
                    }
                });
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        activitiesDao=getApp().getDbSession().getActivitiesDao();
        getAll();
    }
    public void getAll(){
        try {
            QueryBuilder<activities> listQuery = activitiesDao.queryBuilder()
                    .where(com.indocyber.ifgcol.db.activitiesDao.Properties.User_id.eq(getApp().getSession().getUserdata().getString("user_id")))
                    .where(com.indocyber.ifgcol.db.activitiesDao.Properties.Mobile_status.eq("DRAFT"));
            activitiesList = listQuery.list();
            generateList();
        }catch (Exception e){
            e.printStackTrace();
            FirebaseCrash.log(e.getMessage());
        }
    }
    public void searchOrder(String text){
        try {
            QueryBuilder<activities> listQuery = activitiesDao.queryBuilder()
                    .whereOr(com.indocyber.ifgcol.db.activitiesDao.Properties.Customer_name.like("%" + text + "%"),
                            com.indocyber.ifgcol.db.activitiesDao.Properties.Address.like("%" + text + "%"))
                    .where(com.indocyber.ifgcol.db.activitiesDao.Properties.User_id.eq(getApp().getSession().getUserdata().getString("user_id")))
                    .where(com.indocyber.ifgcol.db.activitiesDao.Properties.Mobile_status.eq("DRAFT"));
            activitiesList = listQuery.list();
            generateList();
        }catch (Exception e){
            e.printStackTrace();
            FirebaseCrash.log(e.getMessage());
        }
    }
    @Override
    protected void onResume(){
        super.onResume();
        searchOrder(editTextSearch.getText().toString());
    }
    public void generateList(){
        adapter=new DraftAdapter(activitiesList,this,R.layout.new_order_item,getApp().getDbSession().getOrder_detailDao());
        recyclerView.setAdapter(adapter);
    }
}
