package com.indocyber.ifgcol.activity;


import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import com.clumsiam.uigenerator.generator.Engine;
import com.indocyber.ifgcol.R;

public class PaymentSubmit extends AppActivity implements Engine {
    private TextView txtNama,txtAlamat,txtNoHP;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_submit);
        this.getSupportActionBar().setHomeButtonEnabled(true);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        txtNama= (TextView) findViewById(R.id.txtNamaPemesan);
        txtAlamat= (TextView) findViewById(R.id.txtAlamat);
        txtNoHP= (TextView) findViewById(R.id.txtTelepon);
        try {
            txtAlamat.setText(getApp().tempDataStore.get("CustomerAddress"));
            txtNoHP.setText(getApp().tempDataStore.get("CustomerPhone"));
            txtNama.setText(getApp().tempDataStore.get("CustomerName"));
        }catch (Exception e){
            e.printStackTrace();
        }
        loadForm(R.id.fragmentContainer,"03");
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onUIEvent(View view, String event, String tagUI, String values) {

    }

    @Override
    public void save() {
        saveForm();
        Intent main=new Intent(this,Main.class);
        main.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(main);
        finish();
    }

    @Override
    public void submit() {
        submitForm();
    }

    @Override
    public void next() {

    }
}
