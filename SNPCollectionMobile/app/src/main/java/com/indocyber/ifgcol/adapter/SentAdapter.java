package com.indocyber.ifgcol.adapter;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.ifgcol.activity.DetailOrder;
import com.indocyber.ifgcol.adapter.holder.DraftHolder;
import com.indocyber.ifgcol.adapter.holder.NewHolder;
import com.indocyber.ifgcol.db.activities;
import com.indocyber.ifgcol.db.activitiesDao;
import com.indocyber.ifgcol.db.order_detailDao;
import com.indocyber.ifgcol.db.order_list;
import com.indocyber.ifgcol.activity.AppActivity;
import com.indocyber.ifgcol.db.order_detail;

import org.greenrobot.greendao.query.Query;
import org.greenrobot.greendao.query.QueryBuilder;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Indocyber on 05/04/2018.
 */

public class SentAdapter extends RecyclerView.Adapter<DraftHolder> {
    private List<activities> activitiesList;
    private AppActivity activity;
    private com.indocyber.ifgcol.db.order_detailDao order_detailDao;
    private int layout;
    public SentAdapter(List<activities> activitiesList, AppActivity activity, int layout, com.indocyber.ifgcol.db.order_detailDao order_detailDao){
        this.activitiesList=activitiesList;
        this.activity=activity;
        this.layout=layout;
        this.order_detailDao=order_detailDao;
    }
    @Override
    public DraftHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(this.layout, parent, false);
        return new DraftHolder(v);
    }

    @Override
    public void onBindViewHolder(DraftHolder holder, int position) {
        try {
            String telepon = "";
            final activities item = activitiesList.get(position);
            JSONObject data=new JSONObject(item.getBody());
            final JSONObject input=data.getJSONObject("input");
            final JSONArray details=new JSONArray(data.getString("items"));
            if (input.has("Phone")) {
                telepon = input.getString("Phone");
            } else {
                telepon="-";
            }
            holder.textViewLPKNO.setText(input.getString("LpkNo"));
            holder.textViewNamaPemesan.setText(input.getString("CustomerName"));
            holder.textViewAlamat.setText(input.getString("CustomerAddress"));
            holder.textViewNoHP.setText(telepon);
            holder.layoutPengiriman.setVisibility(View.GONE);
            Query<order_detail> order_detailQuery=order_detailDao.queryBuilder()
                    .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Activity_id.eq(item.getActivity_id()))
                    .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Status.eq("SENT"))
                    .build();
            List<order_detail> order_details=order_detailQuery.list();
            holder.textViewTagihan.setText(order_details.size() + " Tagihan");
            holder.newOrderItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent detail = new Intent(activity, DetailOrder.class);
                        activity.getApp().clearTempDataStore();
                        activity.getApp().clearChoice();
                        Iterator<String> keyIterator = input.keys();
                        while (keyIterator.hasNext()) {
                            String key = keyIterator.next();
                            activity.getApp().tempDataStore.put(key, input.getString(key));
                        }
                        activity.getApp().tempDataStore.put("MODE","SENT");
                        detail.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        activity.startActivity(detail);
                    }catch (Exception e){
                        FirebaseCrash.log(e.getMessage());
                        e.printStackTrace();
                    }
                }
            });
            holder.buttonDiscard.setVisibility(View.VISIBLE);
            holder.buttonDiscard.setText("Hapus");
            holder.buttonDiscard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder=new AlertDialog.Builder(activity);
                    builder.setMessage("Apakah anda yakin ?");
                    builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            activitiesDao activitiesDao=activity.getApp().getDbSession().getActivitiesDao();
                            order_detailDao order_detailDao=activity.getApp().getDbSession().getOrder_detailDao();
                            QueryBuilder<order_detail> queryBuilder=order_detailDao.queryBuilder()
                                    .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Activity_id
                                            .eq(item.getActivity_id()));
                            List<order_detail> order_details=queryBuilder.list();
                            for (int i=0;i<order_details.size();i++){
                                order_detailDao.delete(order_details.get(i));
                            }
                            activitiesDao.delete(item);
                            activity.getAll();
                        }
                    });
                    builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog=builder.create();
                    dialog.show();
                }
            });
        }catch (Exception e){
            FirebaseCrash.log(e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return activitiesList.size();
    }
}
