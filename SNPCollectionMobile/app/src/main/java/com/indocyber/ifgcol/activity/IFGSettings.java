package com.indocyber.ifgcol.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.indocyber.ifgcol.db.events;
import com.indocyber.ifgcol.db.fields;
import com.indocyber.ifgcol.db.forms;
import com.indocyber.ifgcol.db.handling;
import com.indocyber.ifgcol.db.params;
import com.indocyber.ifgcol.db.provinsi;
import com.indocyber.ifgcol.db.rows;
import com.indocyber.ifgcol.R;
import com.indocyber.ifgcol.db.fieldsDao;
import com.indocyber.ifgcol.db.handlingDao;
import com.indocyber.ifgcol.library.AppSettings;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.greenrobot.greendao.query.QueryBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.message.BasicHeader;

import com.indocyber.ifgcol.db.eventsDao;

public class IFGSettings extends AppActivity implements View.OnClickListener {
    private com.indocyber.ifgcol.db.paramsDao paramsDao;
    private com.indocyber.ifgcol.db.provinsiDao provinsiDao;
    private handlingDao handlingDao;
    private com.indocyber.ifgcol.db.formsDao formsDao;
    private com.indocyber.ifgcol.db.rowsDao rowsDao;
    private fieldsDao fieldsDao;
    private eventsDao eventsDao;
    public TextView txtDevID, txtAppID,txtLogicID;
    public Button buttonSave, buttonTest;
    private ProgressDialog progressDialog;
    int total=0,progress=0;
    private String[] tableList=new String[]{"param","provinsi","handling","forms","form_rows","form_fields","events"};
    private int current;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ifgsettings);
        buttonSave = (Button) findViewById(R.id.buttonSaveConfiguration);
        buttonTest = (Button) findViewById(R.id.buttonConnectionTest);
        txtDevID = (TextView) findViewById(R.id.textViewDevID);
        txtAppID = (TextView) findViewById(R.id.textViewAppID);
        txtLogicID = (TextView) findViewById(R.id.textViewLogicID);
        buttonSave.setOnClickListener(this);
        buttonTest.setOnClickListener(this);
        TelephonyManager mngr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String mode=(AppSettings.SERVER.contains("qa"))? "-DEVQA" : "";
            txtAppID.setText("V."+pInfo.versionName+mode);
            txtLogicID.setText(getApp().getSession().getVersion());
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            txtDevID.setText(getApp().getSession().getDevice()




            );

        } catch (Exception e) {
            Log.e(getClass().getSimpleName(),e.getMessage());
        }
        this.getSupportActionBar().setHomeButtonEnabled(true);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        paramsDao=getApp().getDbSession().getParamsDao();
        provinsiDao=getApp().getDbSession().getProvinsiDao();
        handlingDao=getApp().getDbSession().getHandlingDao();
        formsDao=getApp().getDbSession().getFormsDao();
        rowsDao=getApp().getDbSession().getRowsDao();
        fieldsDao=getApp().getDbSession().getFieldsDao();
        eventsDao=getApp().getDbSession().getEventsDao();
        progressDialog=new ProgressDialog(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonConnectionTest :
                current=0;
                total=0;
                progress=0;
               syncTask(1);
            break;
        }
    }
    private void syncTask(final int page){
        if (current < tableList.length) {
            Header[] headers = new Header[1];
            headers[0] = new BasicHeader("APPID", "IFGCOL-IGLO");
            RequestParams params = new RequestParams();
            params.put("page", page);
            params.put("table", tableList[current]);
            progressDialog.setMessage("Mengunduh data "+(current+1)+" dari "+tableList.length);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.show();
            getAPIClient().get(this, AppSettings.SYNC_URL, headers, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        if (response.has("data") && response.getBoolean("status")) {
                            if (response.getJSONArray("data").length() > 0) {
                                total+=response.getJSONArray("data").length();
                                int active=current;
                                JSONArray jsonArray=response.getJSONArray("data");
                                if (active == 0) {
                                    params[] params = new params[jsonArray.length()];
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject object = null;
                                        object = jsonArray.getJSONObject(i);
                                        params[i] = new params();
                                        params[i].setParam_id(object.getString("param_id"));
                                        params[i].setParam_condition(object.getString("condition"));
                                        params[i].setParam_description(object.getString("level"));
                                        params[i].setParent_id(object.getString("parent_id"));
                                        progress++;
                                    }
                                    paramsDao.insertOrReplaceInTx(params);
                                }
                                if (active == 1) {
                                    provinsi[] provinsis = new provinsi[jsonArray.length()];
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject object = null;
                                        object = jsonArray.getJSONObject(i);
                                        provinsis[i] = new provinsi();
                                        provinsis[i].setKode_provinsi(object.getString("kode_provinsi"));
                                        provinsis[i].setNama_provinsi(object.getString("nama_provinsi"));
                                        progress++;
                                    }
                                    provinsiDao.insertOrReplaceInTx(provinsis);
                                }
                                if (active == 2) {
                                    handling[] handlings = new handling[jsonArray.length()];
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject object = null;
                                        object = jsonArray.getJSONObject(i);
                                        handlings[i] = new handling();
                                        handlings[i].setHandling(object.getLong("handling_no"));
                                        handlings[i].setHandling_code(object.getString("handling_code"));
                                        handlings[i].setDeskripsi(object.getString("deskripsi"));
                                        progress++;
                                    }
                                    handlingDao.insertOrReplaceInTx(handlings);
                                }
                                if (active == 3) {
                                    forms[] forms = new forms[jsonArray.length()];
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject object = null;
                                        object = jsonArray.getJSONObject(i);
                                        forms[i] = new forms();
                                        forms[i].setForm_id(object.getString("form_id"));
                                        forms[i].setForm_name(object.getString("form_name"));
                                        forms[i].setForm_label(object.getString("form_label"));
                                        forms[i].setForm_order(object.getInt("form_order"));
                                        progress++;
                                    }
                                    formsDao.insertOrReplaceInTx(forms);
                                }
                                if (active == 4) {
                                    rows[] rows = new rows[jsonArray.length()];
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject object = null;
                                        object = jsonArray.getJSONObject(i);
                                        rows[i] = new rows();
                                        rows[i].setForm_id(object.getString("form_id"));
                                        rows[i].setRow_id(object.getString("row_id"));
                                        rows[i].setRow_visibility(object.getString("row_visibility"));
                                        rows[i].setRow_order(object.getInt("row_order"));
                                        progress++;
                                    }
                                    rowsDao.insertOrReplaceInTx(rows);
                                }
                                if (active == 5) {
                                    fields[] fields = new fields[jsonArray.length()];
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject object = null;
                                        object = jsonArray.getJSONObject(i);
                                        fields[i] = new fields();
                                        fields[i].setField_id(object.getString("field_id"));
                                        fields[i].setRow_id(object.getString("row_id"));
                                        fields[i].setField_extra(object.getString("field_extra"));
                                        fields[i].setField_global_value(object.getString("field_global_value"));
                                        fields[i].setField_label(object.getString("field_label"));
                                        fields[i].setField_name(object.getString("field_name"));
                                        fields[i].setField_store(object.getString("field_store"));
                                        fields[i].setField_type(object.getString("field_type"));
                                        fields[i].setField_visibility(object.getString("field_visibility"));
                                        fields[i].setField_weight(object.getString("field_weight"));
                                        progress++;
                                    }
                                    fieldsDao.insertOrReplaceInTx(fields);
                                }
                                if (active == 6) {
                                    events[] events = new events[jsonArray.length()];
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject object = null;
                                        object = jsonArray.getJSONObject(i);
                                        events[i] = new events();
                                        events[i].setForm_id(object.getString("form_id"));
                                        events[i].setEvent_component(object.getString("event_component"));
                                        events[i].setEvent_component_target(object.getString("event_component_target"));
                                        events[i].setEvent_data(object.getString("event_data"));
                                        events[i].setEvent_id(object.getString("event_id"));
                                        events[i].setEvent_type(object.getString("event_type"));
                                        progress++;
                                    }
                                    eventsDao.insertOrReplaceInTx(events);
                                }
                                syncTask(page + 1);
                            } else {
                                if (current < tableList.length) {
                                    current++;
                                    syncTask(1);
                                } else {
                                    finishDialog();
                                }
                            }
                        } else {
                            if (current < tableList.length) {
                                current++;
                                syncTask(1);
                            } else {
                                finishDialog();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                        AlertDialog.Builder builder = new AlertDialog.Builder(IFGSettings.this);
                        builder.setMessage("Terjadi Kesalahan : "+e.getMessage());
                        builder.setCancelable(false);
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        builder.show();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] header, String body, Throwable throwable) {
                    progressDialog.dismiss();
                    String message=(statusCode==0)? "Koneksi Jaringan Bermasalah." : "Terjadi Kesalahan : (Status Code " + statusCode;
                    AlertDialog.Builder builder = new AlertDialog.Builder(IFGSettings.this);
                    builder.setMessage(message);
                    builder.setCancelable(false);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                    Log.e("Error", AppSettings.SYNC_URL + " Response " + statusCode);
                    throwable.printStackTrace();
                }
                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                    progressDialog.dismiss();
                    AlertDialog.Builder builder = new AlertDialog.Builder(IFGSettings.this);
                    builder.setMessage("Format Tidak Sesuai");
                    builder.setCancelable(false);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                    Log.e("Error", AppSettings.SYNC_URL + " Response " + response);
                    throwable.printStackTrace();
                }
            });
        }else{
            finishDialog();
        }
    }
    private void finishDialog(){
       new Handler().postDelayed(new Runnable() {
           @Override
           public void run() {
               QueryBuilder<params> paramsQuery=paramsDao.queryBuilder()
                       .where(com.indocyber.ifgcol.db.paramsDao.Properties.Param_condition.eq("LogicVersion"));
               List<params> paramsList=paramsQuery.list();
               if(progress>=total) {
                   progressDialog.dismiss();
                   getApp().getSession().setSynced(true);
                   getApp().getSession().setVersion(paramsList.get(0).getParam_description());
                   txtLogicID.setText(paramsList.get(0).getParam_description());
                   AlertDialog.Builder builder = new AlertDialog.Builder(IFGSettings.this);
                   builder.setMessage("Versi logic terbaru sudah terinstall");
                   builder.setCancelable(false);
                   builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                       @Override
                       public void onClick(DialogInterface dialog, int which) {
                           dialog.dismiss();
                       }
                   });
                   builder.show();
               }else{
                   finishDialog();
               }
           }
       },1000);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
