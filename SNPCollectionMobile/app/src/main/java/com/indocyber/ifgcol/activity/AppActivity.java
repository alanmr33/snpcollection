package com.indocyber.ifgcol.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.clumsiam.uigenerator.utils.Base64;
import com.clumsiam.uigenerator.utils.Resizer;
import com.google.firebase.crash.FirebaseCrash;
import com.google.gson.Gson;
import com.indocyber.ifgcol.App;
import com.indocyber.ifgcol.UI.MD5;
import com.indocyber.ifgcol.db.activities;
import com.indocyber.ifgcol.db.activitiesDao;
import com.indocyber.ifgcol.db.order_detail;
import com.indocyber.ifgcol.db.order_detailDao;
import com.indocyber.ifgcol.library.AppSettings;
import com.indocyber.ifgcol.library.FormGenerator;
import com.indocyber.ifgcol.library.RefreshInf;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yan.netmanager.NetStatus;
import com.yan.netmanager.NetStatusManager;

import org.greenrobot.greendao.query.QueryBuilder;
import org.json.JSONObject;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Indocyber on 26/03/2018.
 */

public class AppActivity extends AppCompatActivity implements RefreshInf{
    private App app;
    public boolean readonly=false;
    public AsyncHttpClient httpClient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(getClass().getSimpleName(), "====onCreate===");
        app=((App)getApplication());
        httpClient=new AsyncHttpClient();
        if(getApp().tempDataStore.containsKey("MODE")){
            if(getApp().tempDataStore.get("MODE").equals("PENDING")
                    ||
                    getApp().tempDataStore.get("MODE").equals("SENT")){
                readonly=true;
            }
        }
        NetStatusManager.getInstance().config("http://www.google.com", "http://www.google.com");
        NetStatusManager.getInstance().setCheckType(true);
        NetStatusManager.getInstance().configTimeOut(300000, 300000);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(getClass().getSimpleName(), "====onStart===");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(getClass().getSimpleName(), "====onPause===");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(getClass().getSimpleName(), "====onResume===");
        NetStatusManager.getInstance().refreshStatus();
    }
    public App getApp(){
        return app;
    }
    public AsyncHttpClient getAPIClient(){
        return httpClient;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        String refreshKey=getApp().tempDataStore.get("active-photo-picker")+"-refresh";
        if (requestCode == 1150 && resultCode == RESULT_OK) {
            Resizer.compressImage(getApp().tempDataStore.get(getApp().tempDataStore.get("active-photo-picker")+"-file"),600);
            getApp().tempDataStore.put(refreshKey,"true");
        }else{
            getApp().tempDataStore.put(refreshKey,"false");
            File check=new File(getApp().tempDataStore.get(getApp().tempDataStore.get("active-photo-picker")+"-file"));
            if(check.exists()){
                check.delete();
                getApp().tempDataStore.remove(getApp().tempDataStore.get("active-photo-picker")+"-file");
            }
        }
    }
    public void saveForm(){
        try {
            order_detailDao order_detailDao=getApp().getDbSession().getOrder_detailDao();
            activitiesDao activitiesDao = getApp().getDbSession().getActivitiesDao();
            activities activities = new activities();
            JSONObject object = new JSONObject();
            JSONObject input = new JSONObject();
            if (!getApp().tempDataStore.containsKey("ActivityID")) {
                getApp().tempDataStore.put("ActivityID", MD5.enc(getApp().tempDataStore.get("OrderID") +
                        Calendar.getInstance().getTimeInMillis()));
                activities.setActivity_id(getApp().tempDataStore.get("ActivityID"));
                activities.setCreated_date(Calendar.getInstance().getTime());
            } else {
                activities.setActivity_id(getApp().tempDataStore.get("ActivityID"));
                activities.setUpdated_date(Calendar.getInstance().getTime());
            }
            Set<String> validKeys = getApp().tempDataStore.keySet();
            for (String s : validKeys) {
                input.put(s, getApp().tempDataStore.get(s));
            }
            object.put("input", input);
            object.put("items",new Gson().toJson(getApp().order_details));
            activities.setBody(object.toString());
            activities.setSend_status("NONE");
            activities.setMobile_status("DRAFT");
            activities.setOrder_id(getApp().tempDataStore.get("OrderID"));
            activities.setCustomer_name(getApp().tempDataStore.get("CustomerName"));
            activities.setAddress(getApp().tempDataStore.get("CustomerAddress"));
            activities.setUser_id(getApp().getSession().getUserdata().getString("user_id"));
            QueryBuilder<order_detail> queryBuilder=order_detailDao.queryBuilder()
                    .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Activity_id.eq(activities.getActivity_id()));
            List<order_detail> currents=queryBuilder.list();
            for (int i=0;i<currents.size();i++){
                currents.get(i).setStatus("NEW");
                currents.get(i).setActivity_id(null);
                order_detailDao.update(currents.get(i));
            }
            for (int i=0;i<getApp().order_details.size();i++){
                getApp().order_details.get(i).setStatus("DRAFT");
                getApp().order_details.get(i).setActivity_id(activities.getActivity_id());
                order_detailDao.update(getApp().order_details.get(i));
            }
            activitiesDao.insertOrReplace(activities);
        }catch (Exception e){
            FirebaseCrash.log(e.getMessage());
            e.printStackTrace();
        }
    }
    public void submitForm(){
        try {
            final order_detailDao order_detailDao=getApp().getDbSession().getOrder_detailDao();
            final activitiesDao activitiesDao = getApp().getDbSession().getActivitiesDao();
            final ProgressDialog loading=new ProgressDialog(this);
            loading.setCancelable(false);
            loading.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            loading.setMessage("Mengirim data ke server");

            final activities activities = new activities();
            JSONObject object = new JSONObject();
            JSONObject input = new JSONObject();
            if (!getApp().tempDataStore.containsKey("ActivityID")) {
                getApp().tempDataStore.put("ActivityID", MD5.enc(getApp().tempDataStore.get("OrderID") +
                        Calendar.getInstance().getTimeInMillis()));
                activities.setActivity_id(getApp().tempDataStore.get("ActivityID"));
                activities.setCreated_date(Calendar.getInstance().getTime());
            } else {
                activities.setActivity_id(getApp().tempDataStore.get("ActivityID"));
                activities.setUpdated_date(Calendar.getInstance().getTime());
            }
            SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            getApp().tempDataStore.put("submitDate",format.format(Calendar.getInstance().getTime()));
            getApp().tempDataStore.put("submitLocation",getApp().location);
            if( getApp().tempDataStore.containsKey("PromiseDate")) {
                getApp().tempDataStore.put("PromiseDate", getApp().tempDataStore.get("PromiseDate"));
            }
            Set<String> validKeys = getApp().tempDataStore.keySet();
            for (String s : validKeys) {
                input.put(s, getApp().tempDataStore.get(s));
            }
            object.put("input", input);
            object.put("items",new Gson().toJson(getApp().order_details));
            activities.setBody(object.toString());
            activities.setSend_status("MENGIRIM");
            activities.setMobile_status("PENDING");
            activities.setUser_id(getApp().getSession().getUserdata().getString("user_id"));
            activities.setOrder_id(getApp().tempDataStore.get("OrderID"));
            activities.setCustomer_name(getApp().tempDataStore.get("CustomerName"));
            activities.setAddress(getApp().tempDataStore.get("CustomerAddress"));
            activitiesDao.insertOrReplace(activities);
            QueryBuilder<order_detail> queryBuilder=order_detailDao.queryBuilder()
                    .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Activity_id.eq(activities.getActivity_id()));
            List<order_detail> currents=queryBuilder.list();
            for (int i=0;i<currents.size();i++){
                currents.get(i).setStatus("NEW");
                currents.get(i).setActivity_id(null);
                order_detailDao.update(currents.get(i));
            }
            for (int i=0;i<getApp().order_details.size();i++){
                getApp().order_details.get(i).setStatus("PENDING");
                getApp().order_details.get(i).setActivity_id(activities.getActivity_id());
                order_detailDao.update(getApp().order_details.get(i));
            }
            if(isConnected()){
                loading.show();
                RequestParams params=new RequestParams();
                JSONObject body=new JSONObject(activities.getBody());
                File f = new File(body.getJSONObject("input").getString("Photo-file"));
                if ((f.length() / 1024) > 45) {
                    Resizer.compressImage(body.getJSONObject("input").getString("Photo-file"), 300);
                    f = new File(body.getJSONObject("input").getString("Photo-file"));
                }
                byte imageData[] = new byte[(int) f.length()];
                try {
                    FileInputStream fis = new FileInputStream(f);
                    fis.read(imageData);
                } catch (IOException e) {
                    FirebaseCrash.log(e.getMessage());
                }
                String base64 = Base64.encode(imageData);
                body.put("image",base64);
                params.put("order_id",activities.getOrder_id());
                params.put("body", body);
                params.put("token", ((App)getApplication()).getSession().getToken());
                httpClient.addHeader("APPID", "IFGCOL-IGLO");
                httpClient.post(AppSettings.SUBMIT_URL, params, new JsonHttpResponseHandler(){
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        loading.dismiss();
                        try {
                            if(response.getBoolean("status")){
                                QueryBuilder<order_detail> queryBuilder=order_detailDao.queryBuilder()
                                        .where(com.indocyber.ifgcol.db.order_detailDao.Properties.Activity_id.eq(activities.getActivity_id()));
                                List<order_detail> details=queryBuilder.list();
                                for (int i=0;i<details.size();i++){
                                    details.get(i).setStatus("SENT");
                                    order_detailDao.update(details.get(i));
                                }
                                activities.setMobile_status("SENT");
                                activities.setSend_status("TERKIRIM");
                                activitiesDao.update(activities);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Intent main=new Intent(getBaseContext(),Main.class);
                        main.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(main);
                        finish();
                    }
                    @Override
                    public void onFailure(int statusCode, Header[] header, String body, Throwable throwable){
                        loading.dismiss();
                        Log.e("Error",statusCode+" error");
                        activities.setSend_status("ANTRIAN");
                        activitiesDao.update(activities);
                        unstableDialog();
                    }
                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject error){
                        loading.dismiss();
                        Log.e("Error",statusCode+" error");
                        activities.setSend_status("ANTRIAN");
                        activitiesDao.update(activities);
                        unstableDialog();
                    }
                });
            }else{
                unstableDialog();
            }
        }catch (Exception e){
            FirebaseCrash.log(e.getMessage());
            e.printStackTrace();
        }
    }
    public void loadForm(int layout,String form_id){
        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
        transaction.replace(layout,FormGenerator.newInstance(form_id));
        transaction.commit();
    }

    @Override
    public void getAll() {

    }
    public void unstableDialog(){
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setMessage("Koneksi tidak stabil ! Data akan dikirim saat koneksi bagus.");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent main=new Intent(getBaseContext(),Main.class);
                startActivity(main);
                finish();
            }
        });
        AlertDialog dialog=builder.create();
        dialog.show();
    }
    public boolean isConnected(){
        if(!NetStatusManager.getInstance().getNetStatus().equals(NetStatus.UNKNOW)){
            return true;
        }else{
            return false;
        }
    }
}
