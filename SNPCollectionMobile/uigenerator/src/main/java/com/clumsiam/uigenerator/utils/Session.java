package com.clumsiam.uigenerator.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.Preference;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Indocyber on 19/03/2018.
 */

public class Session {
    private Context ctx;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private final String IS_LOGIN="is_login";
    private final String USERDATA="user";
    private final String HASHCODE="hash";
    private final String IMEI="imei";
    private final String CONFIGURATION="config";
    private final String TOKEN="token";
    private final String IS_CONNECTED="is_connected";
    private final String IS_CONFIGURED="is_configured";
    private final String IS_SYNCED="is_synced";
    private final String DEVICE_PIN="pin";
    private final String LAST_LOG="long_time";
    private final String LOCATION="location";
    private final String LAST_LOGIN="last_login";
    private final String SERVICEJOBS="jobs";
    private final String LOGIC_VERSION="logic_version";
    private final String LAST_PASSWORD="last_password";
    private final String BLOCKED="blocked";
    public Session(String sessionName,Context ctx){
        this.ctx=ctx;
        this.preferences=this.ctx.getSharedPreferences(sessionName,Context.MODE_PRIVATE);
        this.editor=this.preferences.edit();
    }
    public void setLastLog(Long dateLog){
        editor.putLong(LAST_LOG,dateLog);
        editor.commit();
    }
    public void setLogin(Boolean login){
        editor.putBoolean(IS_LOGIN,login);
        editor.commit();
    }
    public void setBlocked(Boolean blocked){
        editor.putBoolean(BLOCKED,blocked);
        editor.commit();
    }
    public void setConnected(Boolean connected){
        editor.putBoolean(IS_CONNECTED,connected);
        editor.commit();
    }
    public void setSynced(Boolean synced){
        editor.putBoolean(IS_SYNCED,synced);
        editor.commit();
    }
    public void setConfigured(Boolean configured){
        editor.putBoolean(IS_CONFIGURED,configured);
        editor.commit();
    }
    public void setToken(String token){
        editor.putString(TOKEN,token);
        editor.commit();
    }
    public void setLocation(String location){
        editor.putString(LOCATION,location);
        editor.commit();
    }
    public void setVersion(String version){
        editor.putString(LOGIC_VERSION,version);
        editor.commit();
    }
    public void setPIN(String token){
        editor.putString(DEVICE_PIN,token);
        editor.commit();
    }
    public void setLastLogin(String lastLogin){
        editor.putString(LAST_LOGIN,lastLogin);
        editor.commit();
    }
    public void setDevice(String device){
        editor.putString(IMEI,device);
        editor.commit();
    }
    public void setHash(String hash){
        editor.putString(HASHCODE,hash);
        editor.commit();
    }
    public void setLastPassword(String password){
        editor.putString(LAST_PASSWORD,password);
        editor.commit();
    }
    public void setUserData(JSONObject userData){
        editor.putString(USERDATA,userData.toString());
        editor.commit();
    }
    public void setJobs(JSONObject jobsList){
        editor.putString(SERVICEJOBS,jobsList.toString());
        editor.commit();
    }
    public void setConfiguration(JSONObject configuration){
        editor.putString(CONFIGURATION,configuration.toString());
        editor.commit();
    }
    public Boolean isLogin(){
        return this.preferences.getBoolean(IS_LOGIN,false);
    }
    public Boolean isBlocked(){
        return this.preferences.getBoolean(BLOCKED,false);
    }
    public Boolean isConnected(){
        return this.preferences.getBoolean(IS_CONNECTED,false);
    }
    public Boolean isSynced(){
        return this.preferences.getBoolean(IS_SYNCED,false);
    }
    public Boolean isConfigured(){
        return this.preferences.getBoolean(IS_CONFIGURED,false);
    }
    public JSONObject getUserdata() throws JSONException {
        String json=this.preferences.getString(USERDATA,"{}");
        return new JSONObject(json);
    }
    public JSONObject getJobs() throws JSONException {
        String json=this.preferences.getString(SERVICEJOBS,"{}");
        return new JSONObject(json);
    }
    public JSONObject getConfig() throws JSONException {
        String json=this.preferences.getString(CONFIGURATION,"{}");
        return new JSONObject(json);
    }
    public String getVersion(){
        String url=this.preferences.getString(LOGIC_VERSION,"-");
        return url;
    }
    public String getHash(){
        String hash=this.preferences.getString(HASHCODE,"");
        return hash;
    }
    public String getDevice(){
        String imei=this.preferences.getString(IMEI,"");
        return imei;
    }
    public String getToken(){
        String token=this.preferences.getString(TOKEN,"");
        return token;
    }
    public String getPIN(){
        String pin=this.preferences.getString(DEVICE_PIN,"");
        return pin;
    }
    public String getLocation(){
        String location=this.preferences.getString(LOCATION,"");
        return location;
    }
    public String getLastLogin(){
        String last=this.preferences.getString(LAST_LOGIN,"");
        return last;
    }
    public String getLastPassword(){
        String last=this.preferences.getString(LAST_PASSWORD,"");
        return last;
    }
    public Long getLast_Log(){
        Long last_log=this.preferences.getLong(LAST_LOG,0);
        return last_log;
    }
}
