package com.clumsiam.uigenerator.generator;

import android.view.View;

/**
 * Created by Indocyber on 24/04/2018.
 */

public interface KangEventHandler {
    void onUIEvent(View view, String event, String tagUI, String values);
}