package com.clumsiam.uigenerator.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

/**
 * Created by Indocyber on 26/03/2018.
 */

public class Formating {

    private static DecimalFormat decimalFormat;

    private static void init() {
        // set the currency format
        decimalFormat = new DecimalFormat("#,###;-#,###");

        // swap comma and dot in rupiah currency
        DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance();
        dfs.setDecimalSeparator(',');
        dfs.setGroupingSeparator('.');

        // set it to the formatter
        decimalFormat.setDecimalFormatSymbols(dfs);
    }

    public static String currency(long amount) {
        if (decimalFormat == null) {
            init();
        }
        return decimalFormat.format(amount);
    }

    public static long getCleanResultLong(String originalString) {
        originalString = cleanString(originalString);
        return (originalString.length() > 0) ? Long.parseLong(originalString) : 0;
    }

    public static String cleanString(String originalString) {
        if (originalString.contains(",")) {
            originalString = originalString.replaceAll(",", "");
        }
        if (originalString.contains(".")) {
            originalString = originalString.replaceAll("\\.", "");
        }
        if (originalString.contains("(")) {
            originalString = originalString.replaceAll("\\(", "");
        }
        if (originalString.contains(")")) {
            originalString = originalString.replaceAll("\\)", "");
        }
        return originalString;
    }

}

