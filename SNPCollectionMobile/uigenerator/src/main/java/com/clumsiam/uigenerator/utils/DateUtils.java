package com.clumsiam.uigenerator.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Indocyber on 26/03/2018.
 */

public abstract class DateUtils {
    public static String dateNow(){
        SimpleDateFormat dateFormat=new SimpleDateFormat("dd MMM yyyy");
        Date date= Calendar.getInstance().getTime();
        return dateFormat.format(date);
    }
    public static String dateNowInternatiol(){
        SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
        Date date= Calendar.getInstance().getTime();
        return dateFormat.format(date);
    }
    public static String dateYesterday(){
        SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
        Calendar cl=Calendar.getInstance();
        cl.add(Calendar.DAY_OF_MONTH,-1);
        Date date=cl.getTime();
        return dateFormat.format(date);
    }
}
