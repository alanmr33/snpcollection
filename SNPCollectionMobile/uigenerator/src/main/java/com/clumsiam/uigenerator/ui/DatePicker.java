package com.clumsiam.uigenerator.ui;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.clumsiam.uigenerator.R;
import com.clumsiam.uigenerator.generator.OnSetValue;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Indocyber on 24/04/2018.
 */

public class DatePicker extends DialogFragment {
    private  String date;
    private OnSetValue setValue;
    @Override
    public android.app.Dialog onCreateDialog(Bundle savedInstanceState) {
        final android.app.Dialog dialog=new android.app.Dialog(getActivity());
        dialog.setContentView(R.layout.datetimepicker);
        final com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker datePicker=
                (com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker) dialog.findViewById(R.id.datePicker);
        datePicker.setCurved(true);
        datePicker.setCyclic(true);
        datePicker.setVisibleItemCount(7);
        datePicker.setTodayText("Hari Ini");
        datePicker.setMustBeOnFuture(true);
        datePicker.setDefaultDate(Calendar.getInstance().getTime());
        datePicker.setMinDate(Calendar.getInstance().getTime());
        Button btnOK= (Button) dialog.findViewById(R.id.buttonDPOK);
        Button btnCancel= (Button) dialog.findViewById(R.id.buttonDPCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpleDateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm");
                Date d=datePicker.getDate();
                date=df.format(d);
                setValue.OnSet(date);
                dialog.dismiss();
            }
        });
        return dialog;
    }
    public void setOnSetvalue(OnSetValue setValue){
        this.setValue=setValue;
    }
    public String getDate(){
        return date;
    }
}

