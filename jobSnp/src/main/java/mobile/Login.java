package mobile;

import mobile.dao.LoginDao;
import mobile.dto.Response;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Indocyber on 27/03/2018.
 */
@RestController
public class Login {
    @Autowired
    private LoginDao loginDao;
    @RequestMapping(value = "/login",
            method = RequestMethod.POST,
            headers = {"APPID=IFGCOL-IGLO"})
    @ResponseBody
    public Response doLogin(@RequestParam("userid") String userid,
                            @RequestParam("password") String password,
                            @RequestParam("device") String device){
      Response response;
      response=loginDao.doLogin(userid,password,device);
      return response;
    }
    @RequestMapping(value = "/login_logs",
            method = RequestMethod.POST,
            headers = {"APPID=IFGCOL-IGLO"})
    @ResponseBody
    public Response saveLogs(@RequestParam("token") String token,
                            @RequestParam("logs") String login_logs){
        Response response;
        response=loginDao.saveLog(token,login_logs);
        return response;
    }
    @RequestMapping(value = "/save_settings",
            method = RequestMethod.POST,
            headers = {"APPID=IFGCOL-IGLO"})
    @ResponseBody
    public Response saveSettings(@RequestParam("token") String token,
                             @RequestParam("data") String data){
        Response response;
        response=loginDao.saveSetting(token,data);
        return response;
    }
}
