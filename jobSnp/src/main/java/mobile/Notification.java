package mobile;

import org.json.JSONObject;
import org.symphonyoss.symphony.jcurl.JCurl;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.security.cert.CertificateParsingException;

/**
 * Created by Indocyber on 04/04/2018.
 */
public abstract class Notification {
    public static boolean sendNotif(String target, String title, String body){
        JSONObject object=new JSONObject();
        JSONObject message=new JSONObject();
        message.put("topic",target);
        message.put("title",title);
        message.put("body",body);
        object.put("data",message);
        object.put("to","/topics/"+target);
        JCurl jcurl = JCurl.builder()
                .method(JCurl.HttpMethod.POST)
                .header("Authorization","key=AAAAhC9ZqCQ:APA91bEO4ymF2IDFlY-bhoi90O0L-qpaxjI56T90iXFo5mpoIv3MTYdyjNxZJc58kIBXCaMA7iH1YTSTlCPzeE4CFetfD_FEt3k9fqDnl_NcrauJIovARWHN94zVx2ES1KDJ35qkeiNw")
                .header("Content-Type","application/json")
                .data(object.toString())
                .build();
        try {
            HttpURLConnection connection=jcurl.connect("https://fcm.googleapis.com/fcm/send");
            JCurl.Response response = jcurl.processResponse(connection);
            System.out.println("===NOTIF===");
            System.out.println(response.getOutput());
            return true;
        } catch (IOException | CertificateParsingException e) {
            e.printStackTrace();
        }
        return false;
    }
    public static boolean downloadTask(String type, String target){
        JSONObject object=new JSONObject();
        JSONObject message=new JSONObject();
        message.put("title","COMMAND");
        message.put("body","GET_DATA");
        message.put("topic",target);
        object.put("data",message);
        object.put("to","/topics/"+target);
        JCurl jcurl = JCurl.builder()
                .method(JCurl.HttpMethod.POST)
                .header("Authorization","key=AAAAhC9ZqCQ:APA91bEO4ymF2IDFlY-bhoi90O0L-qpaxjI56T90iXFo5mpoIv3MTYdyjNxZJc58kIBXCaMA7iH1YTSTlCPzeE4CFetfD_FEt3k9fqDnl_NcrauJIovARWHN94zVx2ES1KDJ35qkeiNw")
                .header("Content-Type","application/json")
                .data(object.toString())
                .build();
        try {
            HttpURLConnection connection=jcurl.connect("https://fcm.googleapis.com/fcm/send");
            JCurl.Response response = jcurl.processResponse(connection);
            System.out.println("===NOTIF===");
            System.out.println(response.getOutput());
            return true;
        } catch (IOException | CertificateParsingException e) {
           e.printStackTrace();
        }
        return  false;
    }

}
