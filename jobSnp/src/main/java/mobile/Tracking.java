package mobile;

import mobile.dao.TrackingDao;
import mobile.dto.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Indocyber on 02/04/2018.
 */
@RestController
public class Tracking {
    @Autowired
    TrackingDao trackingDao;
    @RequestMapping(value = "/tracking",
            method = RequestMethod.POST,
            headers = {"APPID=IFGCOL-IGLO"})
    @ResponseBody
    public Response getOrder(@RequestParam("token") String token,
                             @RequestParam("tracking_data") String tracking_data){
        return trackingDao.saveTracking(token,tracking_data);
    }
}
