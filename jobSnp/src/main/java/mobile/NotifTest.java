package mobile;

import mobile.dto.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Indocyber on 04/04/2018.
 */
@RestController
public class NotifTest {
    @RequestMapping(value = "/notif",
            method = RequestMethod.POST,
            headers = {"APPID=IFGCOL-IGLO"})
    @ResponseBody
    public Response send(@RequestParam("target") String target,
                         @RequestParam("title") String title,
                         @RequestParam("body") String body){
        Response response=new Response();
        boolean sent=Notification.sendNotif(target,title,body);
        if(sent){
            response.setMessage("Message Sent");
        }else{
            response.setMessage("Failed to Send Message");
        }
        response.setStatus(sent);
        return response;
    }
}

