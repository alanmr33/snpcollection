package mobile.dao;

import mobile.dto.Handling;
import mobile.dto.Provinsi;
import mobile.dto.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Indocyber on 04/04/2018.
 */
@Repository("HandlingDao")
public class HandlingDaoImpl implements HandlingDao {
    private JdbcTemplate db;
    @Autowired
    public void setDataSource(DataSource dataSource){
        this.db=new JdbcTemplate(dataSource);
    }
    @Override
    public Response getHandlings() {
        Response response=new Response();
        response.setStatus(true);
        response.setMessage("Data Exist");
        List<Object> handlingList=db.query("SELECT\n" +
                "dbo.MASTER_HANDLING.BATCH_HANDLING_NO,\n" +
                "dbo.MASTER_HANDLING.HANDLING_CODE,\n" +
                "dbo.MASTER_HANDLING.DESKRIPSI_HANDLING\n" +
                "FROM\n" +
                "dbo.MASTER_HANDLING\n" +
                "WHERE\n" +
                "dbo.MASTER_HANDLING.PROMISE_FLAG = '0'\n",new RowMapper<Object>() {
            @Override
            public Handling mapRow(ResultSet resultSet, int i) throws SQLException {
                Handling handling=new Handling();
                handling.setHandling_no(resultSet.getInt("BATCH_HANDLING_NO"));
                handling.setHandling_code(resultSet.getString("HANDLING_CODE"));
                handling.setDeskripsi(resultSet.getString("DESKRIPSI_HANDLING"));
                return handling;
            }
        });
        response.setData(handlingList);
        return response;
    }
}
