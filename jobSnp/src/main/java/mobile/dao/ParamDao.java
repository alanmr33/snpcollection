package mobile.dao;

import mobile.dto.Response;

/**
 * Created by Indocyber on 03/04/2018.
 */
public interface ParamDao {
    public Response getParam(int start,int end);
}
