package mobile.dao;

import mobile.dto.OrderDetail;
import mobile.dto.OrderHeader;
import mobile.dto.Response;
import mobile.dto.UserMobile;
import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import utils.Base64;

import javax.sql.DataSource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Indocyber on 02/04/2018.
 */
@Repository("OrderDao")
public class OrderDaoImpl implements OrderDao {
    @Autowired
    private LoginDao loginDao;
    private JdbcTemplate db;
    @Autowired
    public void setDataSource(DataSource dataSource){
        this.db=new JdbcTemplate(dataSource);
    }
    @Override
    public Response getOrder(String token) {
        final UserMobile userMobile=loginDao.getUser(token);
        Response response=new Response();
        response.setStatus(true);
        response.setMessage("Query Success");
        List<Object> orderList=db.query("SELECT\n" +
                "dbo.COLLECTION_HEADER.ORDER_ID,\n" +
                "dbo.COLLECTION_HEADER.LPK_NO,\n" +
                "dbo.COLLECTION_HEADER.LPK_DATE,\n" +
                "dbo.COLLECTION_HEADER.COLLECTOR_CODE,\n" +
                "dbo.COLLECTION_HEADER.PREVIOUS_COLLECTOR,\n" +
                "dbo.COLLECTION_DETAIL.STATUS,\n" +
                "dbo.COLLECTION_DETAIL.CUSTOMER_CODE,\n" +
                "dbo.COLLECTION_DETAIL.CUSTOMER_NAME,\n" +
                "dbo.COLLECTION_DETAIL.ADDRESS,\n" +
                "dbo.COLLECTION_DETAIL.VILLAGE,\n" +
                "dbo.COLLECTION_DETAIL.DISTRICT,\n" +
                "dbo.COLLECTION_DETAIL.RT,\n" +
                "dbo.COLLECTION_DETAIL.RW,\n" +
                "dbo.COLLECTION_DETAIL.CITY,\n" +
                "dbo.COLLECTION_DETAIL.KODE_PROVINSI,\n" +
                "dbo.COLLECTION_DETAIL.POST_CODE,\n" +
                "dbo.COLLECTION_DETAIL.PROMISE_RANGE,\n" +
                "dbo.COLLECTION_DETAIL.PHONE_AREA,\n" +
                "dbo.COLLECTION_DETAIL.PHONE_NUMBER,\n" +
                "dbo.COLLECTION_DETAIL.MOBILE_PREFIX,\n" +
                "dbo.COLLECTION_DETAIL.MOBILE_NO\n" +
                "\n" +
                "FROM\n" +
                "dbo.COLLECTION_HEADER\n" +
                "INNER JOIN dbo.COLLECTION_DETAIL ON dbo.COLLECTION_DETAIL.ORDER_ID = dbo.COLLECTION_HEADER.ORDER_ID " +
                "WHERE \n" +
                "dbo.COLLECTION_DETAIL.STATUS='ASN' \n" +
                "AND\n" +
                "(" +
                "dbo.COLLECTION_HEADER.COLLECTOR_CODE='"+userMobile.getKode_kolektor()+"' \n"+
                "OR \n"+
                "dbo.COLLECTION_HEADER.PREVIOUS_COLLECTOR='"+userMobile.getKode_kolektor()+"' \n" +
                ") " +
                "GROUP BY \n" +
                "dbo.COLLECTION_HEADER.ORDER_ID,\n" +
                "dbo.COLLECTION_HEADER.LPK_NO,\n" +
                "dbo.COLLECTION_HEADER.LPK_DATE,\n" +
                "dbo.COLLECTION_HEADER.COLLECTOR_CODE,\n" +
                "dbo.COLLECTION_HEADER.PREVIOUS_COLLECTOR,\n" +
                "dbo.COLLECTION_DETAIL.STATUS,\n" +
                "dbo.COLLECTION_DETAIL.CUSTOMER_CODE,\n" +
                "dbo.COLLECTION_DETAIL.CUSTOMER_NAME,\n" +
                "dbo.COLLECTION_DETAIL.ADDRESS,\n" +
                "dbo.COLLECTION_DETAIL.VILLAGE,\n" +
                "dbo.COLLECTION_DETAIL.DISTRICT,\n" +
                "dbo.COLLECTION_DETAIL.RT,\n" +
                "dbo.COLLECTION_DETAIL.RW,\n" +
                "dbo.COLLECTION_DETAIL.CITY,\n" +
                "dbo.COLLECTION_DETAIL.KODE_PROVINSI,\n" +
                "dbo.COLLECTION_DETAIL.POST_CODE,\n" +
                "dbo.COLLECTION_DETAIL.PROMISE_RANGE,\n" +
                "dbo.COLLECTION_DETAIL.PHONE_AREA,\n" +
                "dbo.COLLECTION_DETAIL.PHONE_NUMBER,\n" +
                "dbo.COLLECTION_DETAIL.MOBILE_PREFIX,\n" +
                "dbo.COLLECTION_DETAIL.MOBILE_NO\n", new RowMapper<Object>() {
            @Override
            public Object mapRow(ResultSet rs, int i) throws SQLException {
                OrderHeader header=new OrderHeader();
                header.setOrder_id(String.valueOf(rs.getInt("ORDER_ID")));
                header.setLpk_no(rs.getString("LPK_NO"));
                header.setLpk_date(rs.getString("LPK_DATE"));
                header.setCustomer_code(rs.getString("CUSTOMER_CODE"));
                header.setCustomer_name(rs.getString("CUSTOMER_NAME"));
                header.setAddress(rs.getString("ADDRESS"));
                header.setVillage(rs.getString("VILLAGE"));
                header.setDistrict(rs.getString("DISTRICT"));
                header.setCity(rs.getString("CITY"));
                header.setRt(rs.getString("RT"));
                header.setRw(rs.getString("RW"));
                header.setKode_provinsi(rs.getString("KODE_PROVINSI"));
                header.setPost_code(rs.getString("POST_CODE"));
                header.setPhone_area(rs.getString("PHONE_AREA"));
                header.setPhone_number(rs.getString("PHONE_NUMBER"));
                header.setMobile_prefix(rs.getString("MOBILE_PREFIX"));
                header.setMobile_no(rs.getString("MOBILE_NO"));
                header.setUser_id(rs.getString("COLLECTOR_CODE"));
                header.setData("{}");
                if(!userMobile.getKode_kolektor().equals(rs.getString("COLLECTOR_CODE"))){
                    header.setStatus("RSN");
                }else if(rs.getString("STATUS").equals("ASN")){
                    header.setStatus("DWN");
                }else{
                    header.setStatus(rs.getString("STATUS"));
                }
                List<OrderDetail> detailList=db.query("SELECT\n" +
                        "dbo.COLLECTION_DETAIL.ORDER_ID,\n" +
                        "dbo.COLLECTION_DETAIL.CONTRACT_NO,\n" +
                        "dbo.COLLECTION_DETAIL.INSTALLMENT_NO,\n" +
                        "dbo.COLLECTION_DETAIL.LPK_NO,\n" +
                        "dbo.COLLECTION_DETAIL.INSTALLMENT_AMT,\n" +
                        "dbo.COLLECTION_DETAIL.DEPOSIT_AMT,\n" +
                        "dbo.COLLECTION_DETAIL.PENALTY_AMT,\n" +
                        "dbo.COLLECTION_DETAIL.COLLECTION_FEE,\n" +
                        "dbo.COLLECTION_DETAIL.TOTAL_AMT,\n" +
                        "dbo.COLLECTION_DETAIL.RECEIPT_NO,\n" +
                        "dbo.COLLECTION_DETAIL.MIN_PAYMENT,\n" +
                        "dbo.COLLECTION_DETAIL.MAX_PAYMENT,\n" +
                        "dbo.COLLECTION_DETAIL.REPAYMENT_DATE,\n" +
                        "dbo.COLLECTION_DETAIL.NOTES,\n" +
                        "dbo.COLLECTION_DETAIL.PROMISE_COUNT,\n" +
                        "dbo.COLLECTION_DETAIL.PROMISE_AVAILABLE,\n" +
                        "dbo.COLLECTION_DETAIL.PROMISE_RANGE,\n" +
                        "dbo.COLLECTION_DETAIL.NAMA_BARANG,\n" +
                        "dbo.COLLECTION_DETAIL.DUE_DATE,\n" +
                        "dbo.COLLECTION_DETAIL.STATUS\n" +
                        "FROM\n" +
                        "dbo.COLLECTION_DETAIL\n" +
                        "WHERE \n" +
                        "dbo.COLLECTION_DETAIL.CUSTOMER_CODE='"+header.getCustomer_code()+"' AND " +
                        "dbo.COLLECTION_DETAIL.STATUS='ASN'", new RowMapper<OrderDetail>() {
                    @Override
                    public OrderDetail mapRow(ResultSet resultSet, int i) throws SQLException {
                        OrderDetail detail=new OrderDetail();
                        detail.setOrder_id(String.valueOf(resultSet.getInt("ORDER_ID")));
                        detail.setContract_no(resultSet.getString("CONTRACT_NO"));
                        detail.setInstallment_no(resultSet.getString("INSTALLMENT_NO"));
                        detail.setLpk_no(resultSet.getString("LPK_NO"));
                        detail.setInstallment_amt(resultSet.getString("INSTALLMENT_AMT"));
                        detail.setDeposit_amt(resultSet.getInt("DEPOSIT_AMT"));
                        detail.setPenalty_amt(resultSet.getString("PENALTY_AMT"));
                        detail.setCollection_fee(resultSet.getString("COLLECTION_FEE"));
                        detail.setTotal_amt(resultSet.getString("TOTAL_AMT"));
                        detail.setReceipt_no(resultSet.getString("RECEIPT_NO"));
                        detail.setMin_payment(resultSet.getString("MIN_PAYMENT"));
                        detail.setMax_payment(resultSet.getString("MAX_PAYMENT"));
                        detail.setRepayment_date(resultSet.getString("REPAYMENT_DATE"));
                        detail.setNotes(resultSet.getString("NOTES"));
                        detail.setPromise_count(resultSet.getInt("PROMISE_COUNT"));
                        detail.setPromise_available(resultSet.getInt("PROMISE_AVAILABLE"));
                        detail.setPromise_range(resultSet.getString("PROMISE_RANGE"));
                        detail.setNama_barang(resultSet.getString("NAMA_BARANG"));
                        detail.setStatus(resultSet.getString("STATUS"));
                        detail.setDue_date(resultSet.getString("DUE_DATE"));
                        return detail;
                    }
                });
                header.setDetails(detailList);
                return header;
            }
        });
        response.setData(orderList);
        return response;
    }

    @Override
    public Response saveOrder(String token, String orderID, String body) {
        UserMobile userMobile=loginDao.getUser(token);
        JSONObject object=new JSONObject(body);
        JSONObject input=object.getJSONObject("input");
        JSONArray items=new JSONArray(object.getString("items"));
        String oldName=input.getString("OldCustomerName");
        String Name=input.getString("CustomerName");
        String oldAddress=input.getString("OldCustomerAddress");
        String Address=input.getString("CustomerAddress");
        String oldPhone=input.getString("OldCustomerPhone");
        String Phone=input.getString("CustomerPhone");
        String Code=input.getString("CustomerCode");
        String image=object.getString("image");
        byte[] data = Base64.decode(image);
        File imageFile = new File("D:/Photos/"+orderID+"_"+input.getString("ActivityID")+".jpg");
        if (!imageFile.exists()) {
            imageFile.getParentFile().mkdirs();
        }
        try {
            OutputStream stream = new FileOutputStream(imageFile.getPath());
            stream.write(data);
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String activityID=saveActivity(body,userMobile.getUser_id());
        boolean check=db.query("SELECT * FROM MOBILE_TRACKING " +
                "WHERE ACTIVITY_ID='"+activityID+"' " +
                "AND USER_ID='"+userMobile.getUser_id()+"' " +
                "AND TRACKING_DATE='"+input.getString("submitDate")+"'", new ResultSetExtractor<Boolean>() {
            @Override
            public Boolean extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                if(resultSet.next()){
                    return true;
                }
                return false;
            }
        });
        if(!check){
            db.update("INSERT INTO MOBILE_TRACKING (" +
                    "TRACKING_DATE,LOCATION,USER_ID,ACTIVITY_ID)" +
                    "VALUES" +
                    "(" +
                    "?," +
                    "?," +
                    "?," +
                    "?)",input.getString("submitDate"),input.getString("submitLocation"),userMobile.getUser_id(),activityID);
        }else{
            db.update("UPDATE MOBILE_TRACKING SET " +
                    "LOCATION ='"+input.getString("submitLocation")+"' " +
                    "WHERE ACTIVITY_ID='"+activityID+"' " +
                    "AND USER_ID='"+userMobile.getUser_id()+"' " +
                    "AND TRACKING_DATE='"+input.getString("submitDate")+"'");
        }
        String status="CMP";
        int amt=(input.has("PaymentAmt"))? Integer.valueOf(input.getString("PaymentAmt")) : 0;
        for (int i=0;i<items.length();i++){
            JSONObject item=items.getJSONObject(i);
            orderID=db.query("SELECT TOP 1 ORDER_ID FROM COLLECTION_DETAIL " +
                    "WHERE " +
                    "CONTRACT_NO='"+item.getString("contract_no")+"' "+
                    "ORDER BY ORDER_ID DESC", new ResultSetExtractor<String>() {
                @Override
                public String extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                    if(resultSet.next()){
                        return resultSet.getString("ORDER_ID");
                    }
                    return "";
                }
            });
            if(input.getString("VisitResult").equals("VR0")){
                /*
                PTP TIDAK BAYAR
                 */
                boolean exist=db.query("SELECT * FROM HANDLING " +
                        "WHERE ORDER_ID='"+orderID+"' " +
                        "AND INSTALLMENT_NO='"+item.getString("installment_no")+"' " +
                        "AND CONTRACT_NO='"+item.getString("contract_no")+"'", new ResultSetExtractor<Boolean>() {
                    @Override
                    public Boolean extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                        if(resultSet.next()){
                            return true;
                        }
                        return false;
                    }
                });
                String statusP=(input.has("PaymentStatus"))? input.getString("PaymentStatus") : "SP1";
                String handling=(input.has("Handling"))? input.getString("Handling") : "1";
                String pasal=(input.has("PaymentStatusPasal"))? input.getString("PaymentStatusPasal") : "0";
                String from=(input.has("ReceiveFrom"))? input.getString("ReceiveFrom") : "";
                String promise_date=(input.has("PromiseDate"))? input.getString("PromiseDate") : "";
                int deposit=0;
                if(statusP.equals("SP1")){
                    item.put("promise_count",item.getInt("promise_count")+1);
                    item.put("promise_available",item.getInt("promise_available")-1);
                    pasal="0";
                    int topay=(item.has("total_amt"))? Integer.valueOf(item.getString("total_amt").replace(".00","")) : 0;
                    if(amt>0){
                        if(amt>topay){
                            amt-=topay;
                            deposit=topay;
                        }else{
                            deposit=amt;
                            amt=0;
                        }
                    }
                }else{
                    from="";
                    deposit=0;
                    promise_date="";
                }
                if(!exist){
                    db.update("INSERT INTO HANDLING (" +
                                    "BATCH_HANDLING," +
                                    "CONTRACT_NO," +
                                    "INSTALLMENT_NO," +
                                    "ORDER_ID," +
                                    "KODE_KONSUMEN," +
                                    "KODE_HANDLING," +
                                    "NO_LPK," +
                                    "NOTES," +
                                    "TANGGAL_HANDLING," +
                                    "TANGGAL_JANJI," +
                                    "PROMISE_COUNT," +
                                    "PROMISE_AVAILABLE," +
                                    "PROMISE_RANGE," +
                                    "STATUS," +
                                    "CREATE_USER," +
                                    "CREATE_DATE," +
                                    "MODIFY_USER," +
                                    "MODIFY_DATE," +
                                    "DEPOSIT," +
                                    "DEPOSIT_FROM)" +
                                    "VALUES" +
                                    "(" +
                                    "'"+handling+"'," +
                                    "'"+item.getString("contract_no")+"'," +
                                    "'"+ item.getString("installment_no")+"'," +
                                    "'"+orderID+"'," +
                                    "'"+ input.getString("CustomerCode")+"'," +
                                    "'"+pasal+"'," +
                                    "'"+item.getString("lpk_no")+"'," +
                                    "'"+input.getString("Cronology")+"'," +
                                    "'"+input.getString("submitDate")+"'," +
                                    "'"+ promise_date+"'," +
                                    "'"+item.getInt("promise_count")+"'," +
                                    "'"+item.getInt("promise_available")+"'," +
                                    "'"+item.getString("promise_range")+"'," +
                                    "'"+status+"'," +
                                    "'"+userMobile.getUser_id()+"'," +
                                    "GETDATE()," +
                                    "'"+userMobile.getUser_id()+"'," +
                                    "GETDATE()," +
                                    "'"+deposit+"'," +
                                    "'"+from+"')");
                }else{
                    db.update("UPDATE HANDLING SET " +
                            "KODE_KONSUMEN=?," +
                            "NO_LPK=?," +
                            "NOTES=?," +
                            "TANGGAL_HANDLING=?," +
                            "TANGGAL_JANJI=?," +
                            "PROMISE_COUNT=?," +
                            "PROMISE_AVAILABLE=?," +
                            "PROMISE_RANGE=?," +
                            "STATUS=?," +
                            "MODIFY_USER=?," +
                            "MODIFY_DATE=GETDATE(), " +
                            "DEPOSIT=?, " +
                            "DEPOSIT_FROM=? " +
                            "WHERE " +
                            "BATCH_HANDLING=? AND " +
                            "CONTRACT_NO=? AND " +
                            "INSTALLMENT_NO=? AND " +
                            "ORDER_ID=?",
                            input.getString("CustomerCode"),
                            item.getString("lpk_no"),
                            input.getString("Cronology"),
                            input.getString("submitDate"),
                            promise_date,
                            item.getInt("promise_count"),
                            item.getInt("promise_available"),
                            item.getString("promise_range"),
                            status,
                            userMobile.getUser_id(),
                            deposit,
                            from,
                            handling,
                            item.getString("contract_no"),
                            item.getString("installment_no"),
                            orderID
                    );
                }
            }else{
                /*
                PEMBAYARAN
                 */
                boolean exist=db.query("SELECT * FROM PAYMENT " +
                        "WHERE ORDER_ID='"+orderID+"' " +
                        "AND INSTALLMENT_NO='"+item.getString("installment_no")+"' " +
                        "AND CONTRACT_NO='"+item.getString("contract_no")+"'", new ResultSetExtractor<Boolean>() {
                    @Override
                    public Boolean extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                        if(resultSet.next()){
                            return true;
                        }
                        return false;
                    }
                });
                String from=(input.has("ReceiveFrom"))? input.getString("ReceiveFrom") : "";
                if(!exist){
                    db.update("INSERT INTO PAYMENT (" +
                                    "INSTALLMENT_NO," +
                                    "ORDER_ID," +
                                    "CONTRACT_NO," +
                                    "CUSTOMER_CODE," +
                                    "CODE_COLLECTOR," +
                                    "TERIMA_DARI," +
                                    "TOTAL_BAYAR," +
                                    "NO_KWITANSI," +
                                    "NO_LPK," +
                                    "TANGGAL_TRANSAKSI," +
                                    "TANGGAL_BAYAR," +
                                    "STATUS," +
                                    "CREATE_USER," +
                                    "CREATED_DATE," +
                                    "MODIFY_USER," +
                                    "MODIFY_DATE," +
                                    "NOTES" +
                                    ")VALUES(?,?,?,?,?,?,?,?,?,GETDATE(),?,?,?,GETDATE(),?,GETDATE(),?)",
                            item.getString("installment_no"),
                            orderID,
                            item.getString("contract_no"),
                            input.getString("CustomerCode"),
                            userMobile.getKode_kolektor(),
                            from,
                            input.getString("PaymentAmt").replace(".",""),
                            item.getString("receipt_no"),
                            item.getString("lpk_no"),
                            input.getString("submitDate"),
                            status,
                            userMobile.getUser_id(),
                            userMobile.getUser_id(),
                            input.getString("Cronology")
                    );
                }else{
                    db.update("UPDATE PAYMENT SET " +
                            "CUSTOMER_CODE=?," +
                            "CODE_COLLECTOR=?," +
                            "TERIMA_DARI=?," +
                            "TOTAL_BAYAR=?," +
                            "NO_KWITANSI=?," +
                            "NO_LPK=?," +
                            "TANGGAL_TRANSAKSI=GETDATE()," +
                            "TANGGAL_BAYAR=?," +
                            "STATUS=?," +
                            "MODIFY_USER=?," +
                            "MODIFY_DATE=GETDATE()," +
                            "NOTES=? " +
                            "WHERE " +
                            "INSTALLMENT_NO=? AND " +
                            "ORDER_ID=? AND " +
                            "CONTRACT_NO=?",
                            input.getString("CustomerCode"),
                            userMobile.getKode_kolektor(),
                            from,
                            input.getString("PaymentAmt").replace(".",""),
                            item.getString("receipt_no"),
                            item.getString("lpk_no"),
                            input.getString("submitDate"),
                            status,
                            userMobile.getUser_id(),
                            input.getString("Cronology"),
                            item.getString("installment_no"),
                            orderID,
                            item.getString("contract_no"));
                }
            }
            db.update("UPDATE COLLECTION_DETAIL SET " +
                            "STATUS='CMP', " +
                            "ACTIVITY_ID='"+activityID+"', " +
                            "PHOTO=? " +
                            "WHERE ORDER_ID=? AND " +
                            "CONTRACT_NO=? AND " +
                            "INSTALLMENT_NO=?",
                    imageFile.getAbsolutePath(),
                    orderID,
                    item.getString("contract_no"),
                    item.getString("installment_no")
            );
            if(!oldName.equals(Name) || !oldAddress.equals(Address)){
                boolean exist=db.query("SELECT * FROM UPDATE_KONSUMEN " +
                        "WHERE ORDER_ID='"+orderID+"' " +
                        "AND INSTALLMENT_NO='"+item.getString("installment_no")+"' " +
                        "AND CONTRACT_NO='"+item.getString("contract_no")+"'", new ResultSetExtractor<Boolean>() {
                    @Override
                    public Boolean extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                        if(resultSet.next()){
                            return true;
                        }
                        return false;
                    }
                });
                if(!exist){
                    db.update("INSERT INTO UPDATE_KONSUMEN " +
                            "(ORDER_ID," +
                            "INSTALLMENT_NO," +
                            "CONTRACT_NO," +
                            "CUSTOMER_CODE," +
                            "NAMA_KONSUMEN," +
                            "ALAMAT," +
                            "PHONE," +
                            "STATUS)" +
                            "VALUES" +
                            "('"+orderID+"'," +
                            "'"+item.getString("installment_no")+"'," +
                            "'"+item.getString("contract_no")+"'," +
                            "'"+Code+"'," +
                            "'"+Name+"'," +
                            "'"+Address+"'," +
                            "'"+Phone+"'," +
                            "'"+status+"')");
                }else{
                    db.update("UPDATE UPDATE_KONSUMEN SET " +
                            "NAMA_KONSUMEN='"+Name+"', " +
                            "PHONE='"+Phone+"', " +
                            "ALAMAT='"+Address+"' " +
                            "WHERE ORDER_ID='"+orderID+"' " +
                            "AND CUSTOMER_CODE='"+Code+"' " +
                            "AND CONTRACT_NO='"+item.getString("contract_no")+"'");
                }
            }
        }
        db.update("{call SP_CHECK_TASK("+orderID+")}");
        Response response=new Response();
        response.setStatus(true);
        response.setMessage("OK");
        return response;
    }

    @Override
    public Response createLog(String orderID, String status, String userID) {
        Response response=new Response();
        db.update("INSERT  INTO COLLECTION_LOG" +
                "(ORDER_ID," +
                "ACTIVITY_DATE," +
                "STATUS," +
                "CHANGED_BY)" +
                "VALUES" +
                "(?,GETDATE(),?,?)",orderID,status,userID);
        response.setStatus(true);
        response.setMessage("Log created");
        return response;
    }

    @Override
    public String saveActivity(String body, String userID) {
        SimpleDateFormat dateFormat=new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        String activityID= DigestUtils.md5Hex("ACT"+dateFormat.format(Calendar.getInstance().getTime()));
        db.update("INSERT INTO COLLECTION_ACTIVITY" +
                "(ACTIVITY_ID," +
                "ACTIVITY_DATE," +
                "ACTIVITY_BODY," +
                "ACTIVITY_ACTOR)" +
                "VALUES " +
                "(?," +
                "GETDATE()," +
                "?," +
                "?" +
                ")",activityID,body,userID);
        return activityID;
    }
}
