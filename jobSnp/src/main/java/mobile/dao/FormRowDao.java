package mobile.dao;


import mobile.dto.Response;

/**
 * Created by Indocyber on 24/04/2018.
 */
public interface FormRowDao {
    public Response getFormRows();
}
