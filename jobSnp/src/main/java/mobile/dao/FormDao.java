package mobile.dao;

import mobile.dto.Response;

/**
 * Created by Indocyber on 24/04/2018.
 */
public interface FormDao {
    public Response getForms();
}
