package mobile.dao;

import mobile.dto.FormEvents;
import mobile.dto.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Indocyber on 25/04/2018.
 */
@Repository("EventDao")
public class EventDaoImpl implements EventDao {
    private JdbcTemplate db;
    @Autowired
    public void setDataSource(DataSource dataSource){
        this.db=new JdbcTemplate(dataSource);
    }
    @Override
    public Response getEvents() {
        Response response=new Response();
        response.setStatus(true);
        response.setMessage("Data Exist");
        List<Object> formList=db.query("SELECT\n" +
                "dbo.EVENTS.event_id,\n" +
                "dbo.EVENTS.event_type,\n" +
                "dbo.EVENTS.event_component,\n" +
                "dbo.EVENTS.event_component_target,\n" +
                "dbo.EVENTS.event_data,\n" +
                "dbo.EVENTS.form_id\n" +
                "FROM\n" +
                "dbo.EVENTS\n", new RowMapper<Object>() {
            @Override
            public FormEvents mapRow(ResultSet resultSet, int i) throws SQLException {
                FormEvents events=new FormEvents();
                events.setForm_id(resultSet.getString("form_id"));
                events.setEvent_id(resultSet.getString("event_id"));
                events.setEvent_type(resultSet.getString("event_type"));
                events.setEvent_component(resultSet.getString("event_component"));
                events.setEvent_component_target(resultSet.getString("event_component_target"));
                events.setEvent_data(resultSet.getString("event_data"));
                return events;
            }
        });
        response.setData(formList);
        return response;
    }
}
