package mobile.dao;

import mobile.dto.Response;

/**
 * Created by Indocyber on 02/04/2018.
 */
public interface OrderDao {
    public Response getOrder(String token);
    public Response saveOrder(String token,String orderID, String body);
    public Response createLog(String orderID, String status,String userID);
    public String saveActivity(String body,String userID);
}
