package mobile.dao;

import mobile.dto.Param;
import mobile.dto.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Indocyber on 03/04/2018.
 */
@Repository("ParamDao")
public class ParamDaoImpl implements ParamDao {
    private JdbcTemplate db;
    @Autowired
    public void setDataSource(DataSource dataSource){
        this.db=new JdbcTemplate(dataSource);
    }
    @Override
    public Response getParam(int start, int end) {
        Response response=new Response();
        response.setStatus(true);
        response.setMessage("Object Received");
        List<Object> data=db.query("SELECT * FROM (SELECT\n" +
                "ROW_NUMBER() OVER(ORDER BY dbo.PARAM_MST.PARAM_ID ASC) as row,\n" +
                "dbo.PARAM_MST.PARAM_ID,\n" +
                "dbo.PARAM_MST.CONDITION,\n" +
                "dbo.PARAM_MST.LEVEL_PARAM,\n" +
                "dbo.PARAM_MST.PARENT_ID\n" +
                "FROM\n" +
                "dbo.PARAM_MST) AS a\n" +
                "WHERE\n" +
                "row>="+start+" AND row<="+end, new RowMapper<Object>() {
            @Override
            public Param mapRow(ResultSet resultSet, int i) throws SQLException {
                Param param=new Param();
                param.setParam_id(resultSet.getString("PARAM_ID"));
                param.setParent_id(resultSet.getString("PARENT_ID"));
                param.setCondition(resultSet.getString("CONDITION"));
                param.setLevel(resultSet.getString("LEVEL_PARAM"));
                return param;
            }
        });
        response.setData(data);
        return response;
    }
}
