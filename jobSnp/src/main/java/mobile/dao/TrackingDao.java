package mobile.dao;

import mobile.dto.Response;

/**
 * Created by Indocyber on 02/04/2018.
 */
public interface TrackingDao {
    public Response saveTracking(String token,String trackingData);
}
