package mobile.dao;

import mobile.dto.Response;
import mobile.dto.UserMobile;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import utils.TokenUtils;

import javax.sql.DataSource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Indocyber on 28/03/2018.
 */
@Repository("LoginDao")
public class LoginDaoImpl implements LoginDao {
    private JdbcTemplate db;
    
    @Autowired
    public void setDataSource(DataSource dataSource){
        this.db=new JdbcTemplate(dataSource);
    }
    
    @Override
    public Response doLogin(String userid, String password, String device) {
        Response response = new Response();
        UserMobile userMobile = db.query("SELECT\n" +
                "dbo.USER_PROFILE.USER_ID,\n" +
                "dbo.USER_PROFILE.KODE_KOLEKTOR,\n" +
                "dbo.USER_PROFILE.KODE_KARYAWAN,\n" +
                "dbo.USER_PROFILE.KODE_CABANG,\n" +
                "dbo.USER_PROFILE.KODE_REGION,\n" +
                "dbo.USER_PROFILE.NAMA_KARYAWAN,\n" +
                "dbo.USER_PROFILE.IMEI_NUMBER,\n" +
                "dbo.USER_PROFILE.PHONE_NUMBER\n" +
                "\n" +
                "FROM\n" +
                "dbo.USER_PROFILE\n" +
                "INNER JOIN dbo.AUTHORITY_MST ON dbo.USER_PROFILE.AUTHORITY_ID = dbo.AUTHORITY_MST.AUTHORITY_ID\n" +
                "WHERE\n" +
                "dbo.USER_PROFILE.USER_ID = '"+userid+"' AND\n" +
                "dbo.USER_PROFILE.PASSWORD = '"+password+"' AND\n" +
                "dbo.AUTHORITY_MST.AUTHORITY_ID = '4' AND \n"+
                "dbo.USER_PROFILE.AKTIF_STATUS = '1'\n", new ResultSetExtractor<UserMobile>() {
            @Override
            public UserMobile extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                if(resultSet.next()){
                    UserMobile loginUser=new UserMobile();
                    loginUser.setUser_id(resultSet.getString("USER_ID"));
                    loginUser.setKode_cabang(resultSet.getString("KODE_CABANG"));
                    loginUser.setKode_region(resultSet.getString("KODE_REGION"));
                    loginUser.setKode_kolektor(resultSet.getString("KODE_KOLEKTOR"));
                    loginUser.setKode_karyawan(resultSet.getString("KODE_KARYAWAN"));
                    loginUser.setNama_karyawan(resultSet.getString("NAMA_KARYAWAN"));
                    loginUser.setImei(resultSet.getString("IMEI_NUMBER"));
                    loginUser.setPhone_number(resultSet.getString("PHONE_NUMBER"));
                    return loginUser;
                }
                return null;
            }
        });
        if(userMobile!=null){
            if(device.equals(userMobile.getImei())) {
                String token = TokenUtils.generateToken();
                db.update("INSERT INTO  TOKEN_MST" +
                        "(TOKEN,TOKEN_DATE,USER_ID) " +
                        "VALUES" +
                        "('"+token+"',GETDATE(),'"+userMobile.getUser_id()+"')");
                response.setStatus(true);
                response.setMessage("Login Sukses");
                List<Object> users=new ArrayList<>();
                userMobile.setToken(token);
                users.add(userMobile);
                response.setData(users);
            }else{
                response.setStatus(false);
                response.setMessage("Device Tidak Sesuai");
            }
        }else{
            response.setStatus(false);
            response.setMessage("User ID dan Password Tidak Sesuai");
        }
        return response;
    }

    @Override
    public Response saveLog(String token, String data) {
        JSONArray logs=new JSONArray(data);
        UserMobile userMobile=getUser(token);
        for (int i=0;i<logs.length();i++){
            JSONObject object=logs.getJSONObject(i);
            db.update("INSERT INTO LOGIN_LOGS " +
                    "(USER_ID," +
                    "LOG_DATE," +
                    "LOGIN," +
                    "LOGOUT)" +
                    "VALUES" +
                    "('"+userMobile.getUser_id()+"'," +
                    "'"+object.getString("date")+"'," +
                    "'"+object.getString("login")+"'," +
                    "'"+object.getString("logout")+"')");
        }
        Response response=new Response();
        response.setStatus(true);
        response.setMessage("Log Saved");
        return response;
    }

    @Override
    public Response saveSetting(String token, String data) {
        Response response=new Response();
        UserMobile userMobile=getUser(token);
        JSONObject object=new JSONObject(data);
        String opassword=object.getString("opassword");
        String npassword=object.getString("npassword");
        boolean exist=db.query("SELECT * FROM USER_PROFILE " +
                "WHERE USER_ID='"+userMobile.getUser_id()+"' " +
                "AND PASSWORD='"+opassword+"'", new ResultSetExtractor<Boolean>() {
            @Override
            public Boolean extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                if(resultSet.next()){
                    return true;
                }
                return false;
            }
        });
        if(exist){
            db.update("UPDATE USER_PROFILE SET PASSWORD='" + npassword + "' " +
                    "WHERE USER_ID='" + userMobile.getUser_id() + "' " +
                    "AND PASSWORD='" + opassword + "'");
            response.setStatus(true);
            response.setMessage("Data berhasil disimpan");
        }else{
            response.setStatus(false);
            response.setMessage("Password lama tidak sesuai");
        }
        return response;
    }

    @Override
    public UserMobile getUser(String token) {
        UserMobile userMobile=db.query("SELECT\n" +
                "dbo.TOKEN_MST.TOKEN,\n" +
                "dbo.TOKEN_MST.TOKEN_DATE,\n" +
                "dbo.TOKEN_MST.USER_ID,\n" +
                "dbo.USER_PROFILE.USER_ID,\n" +
                "dbo.USER_PROFILE.KODE_CABANG,\n" +
                "dbo.USER_PROFILE.KODE_REGION,\n" +
                "dbo.USER_PROFILE.KODE_KOLEKTOR,\n" +
                "dbo.USER_PROFILE.KODE_KARYAWAN,\n" +
                "dbo.USER_PROFILE.NAMA_KARYAWAN,\n" +
                "dbo.USER_PROFILE.AUTHORITY_ID,\n" +
                "dbo.USER_PROFILE.IMEI_NUMBER,\n" +
                "dbo.USER_PROFILE.PHONE_NUMBER,\n" +
                "dbo.USER_PROFILE.TANGGAL_MULAI,\n" +
                "dbo.USER_PROFILE.TANGGAL_AKHIR,\n" +
                "dbo.USER_PROFILE.AKTIF_STATUS,\n" +
                "dbo.USER_PROFILE.PASSWORD,\n" +
                "dbo.USER_PROFILE.CREATE_USER,\n" +
                "dbo.USER_PROFILE.CREATE_DATE,\n" +
                "dbo.USER_PROFILE.MODIFY_USER,\n" +
                "dbo.USER_PROFILE.MODIFY_DATE,\n" +
                "dbo.USER_PROFILE.KODE_REGIONAL\n" +
                "FROM\n" +
                "dbo.TOKEN_MST\n" +
                "INNER JOIN dbo.USER_PROFILE ON dbo.TOKEN_MST.USER_ID = dbo.USER_PROFILE.USER_ID\n" +
                "WHERE\n" +
                "dbo.TOKEN_MST.TOKEN='"+token+"'", new ResultSetExtractor<UserMobile>() {
            
        	@Override
            public UserMobile extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                if(resultSet.next()){
                    UserMobile loginUser=new UserMobile();
                    loginUser.setUser_id(resultSet.getString("USER_ID"));
                    loginUser.setKode_cabang(resultSet.getString("KODE_CABANG"));
                    loginUser.setKode_region(resultSet.getString("KODE_REGION"));
                    loginUser.setKode_kolektor(resultSet.getString("KODE_KOLEKTOR"));
                    loginUser.setKode_karyawan(resultSet.getString("KODE_KARYAWAN"));
                    loginUser.setNama_karyawan(resultSet.getString("NAMA_KARYAWAN"));
                    loginUser.setImei(resultSet.getString("IMEI_NUMBER"));
                    loginUser.setPhone_number(resultSet.getString("PHONE_NUMBER"));
                    return loginUser;
                }
                return new UserMobile();
            }
        });
        return userMobile;
    }
}
