package mobile.dao;

import mobile.dto.Response;
import mobile.dto.UserMobile;

/**
 * Created by Indocyber on 28/03/2018.
 */
public interface LoginDao {
    public Response doLogin(String userid,String password,String device);
    public Response saveLog(String token, String data);
    public Response saveSetting(String token, String data);
    public UserMobile getUser(String token);
}
