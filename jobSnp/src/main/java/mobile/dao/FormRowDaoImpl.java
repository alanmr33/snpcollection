package mobile.dao;

import mobile.dto.FormRows;
import mobile.dto.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Indocyber on 24/04/2018.
 */
@Repository("FormRowDao")
public class FormRowDaoImpl implements FormRowDao {
    private JdbcTemplate db;
    @Autowired
    public void setDataSource(DataSource dataSource){
        this.db=new JdbcTemplate(dataSource);
    }
    @Override
    public Response getFormRows() {
        Response response=new Response();
        response.setStatus(true);
        response.setMessage("Data Exist");
        List<Object> listRows=db.query("SELECT\n" +
                "dbo.FORM_ROWS.row_id,\n" +
                "dbo.FORM_ROWS.row_order,\n" +
                "dbo.FORM_ROWS.row_visibility,\n" +
                "dbo.FORM_ROWS.form_id\n" +
                "FROM\n" +
                "dbo.FORM_ROWS\n", new RowMapper<Object>() {
            @Override
            public FormRows mapRow(ResultSet resultSet, int i) throws SQLException {
                FormRows formRows=new FormRows();
                formRows.setForm_id(resultSet.getString("form_id"));
                formRows.setRow_id(resultSet.getString("row_id"));
                formRows.setRow_order(resultSet.getInt("row_order"));
                formRows.setRow_visibility(resultSet.getString("row_visibility"));
                return formRows;
            }
        });
        response.setData(listRows);
        return response;
    }
}
