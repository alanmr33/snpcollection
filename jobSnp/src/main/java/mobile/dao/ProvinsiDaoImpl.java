package mobile.dao;
import mobile.dto.Provinsi;
import mobile.dto.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Indocyber on 04/04/2018.
 */
@Repository("ProvinsiDao")
public class ProvinsiDaoImpl implements ProvinsiDao {
    private JdbcTemplate db;
    @Autowired
    public void setDataSource(DataSource dataSource){
        this.db=new JdbcTemplate(dataSource);
    }

    @Override
    public Response getProvinsi() {
        Response response=new Response();
        response.setStatus(true);
        response.setMessage("Data Exist");
        List<Object> provinsiList=db.query("SELECT\n" +
                "dbo.MASTER_PROVINSI.KODE_PROVINSI,\n" +
                "dbo.MASTER_PROVINSI.NAMA_PROVINSI\n" +
                "FROM\n" +
                "dbo.MASTER_PROVINSI\n",new RowMapper<Object>() {
            @Override
            public Provinsi mapRow(ResultSet resultSet, int i) throws SQLException {
                Provinsi provinsi=new Provinsi();
                provinsi.setKode_provinsi(resultSet.getString("KODE_PROVINSI"));
                provinsi.setNama_provinsi(resultSet.getString("NAMA_PROVINSI"));
                return provinsi;
            }
        });
        response.setData(provinsiList);
        return response;
    }
}
