package mobile.dao;

import mobile.dto.Response;

/**
 * Created by Indocyber on 04/04/2018.
 */
public interface ProvinsiDao {
    public Response getProvinsi();
}
