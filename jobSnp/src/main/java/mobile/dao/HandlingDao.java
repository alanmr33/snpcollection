package mobile.dao;

import mobile.dto.Response;
import org.springframework.stereotype.Repository;

/**
 * Created by Indocyber on 04/04/2018.
 */
public interface HandlingDao {
    public Response getHandlings();
}
