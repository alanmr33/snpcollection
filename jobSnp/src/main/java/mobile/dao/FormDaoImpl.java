package mobile.dao;

import mobile.dto.Forms;
import mobile.dto.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Indocyber on 24/04/2018.
 */
@Repository("FormDao")
public class FormDaoImpl implements FormDao {
    private JdbcTemplate db;
    @Autowired
    public void setDataSource(DataSource dataSource){
        this.db=new JdbcTemplate(dataSource);
    }
    @Override
    public Response getForms() {
        Response response=new Response();
        response.setStatus(true);
        response.setMessage("Data Exist");
        List<Object> formList=db.query("SELECT\n" +
                "dbo.FORMS.form_id,\n" +
                "dbo.FORMS.form_name,\n" +
                "dbo.FORMS.form_label,\n" +
                "dbo.FORMS.form_order\n" +
                "FROM\n" +
                "dbo.FORMS\n", new RowMapper<Object>() {
            @Override
            public Forms mapRow(ResultSet resultSet, int i) throws SQLException {
                Forms forms =new Forms();
                forms.setForm_id(resultSet.getString("form_id"));
                forms.setForm_label(resultSet.getString("form_label"));
                forms.setForm_name(resultSet.getString("form_name"));
                forms.setForm_order(resultSet.getInt("form_order"));
                return forms;
            }
        });
        response.setData(formList);
        return response;
    }
}
