package mobile.dao;

import mobile.dto.Response;

/**
 * Created by Indocyber on 25/04/2018.
 */
public interface EventDao {
    public Response getEvents();
}
