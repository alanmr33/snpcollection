package mobile.dao;

import mobile.dto.Response;
import mobile.dto.UserMobile;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;

/**
 * Created by Indocyber on 02/04/2018.
 */
@Repository("TrackingDao")
public class TrackingDaoImpl implements TrackingDao {
    @Autowired
    private LoginDao loginDao;
    private JdbcTemplate db;
    @Autowired
    public void setDataSource(DataSource dataSource){
        this.db=new JdbcTemplate(dataSource);
    }
    @Override
    public Response saveTracking(String token, String trackingData) {
        JSONArray data= new JSONArray(trackingData);
        UserMobile userMobile=loginDao.getUser(token);
        for (int i=0;i<data.length();i++){
            JSONObject track=data.getJSONObject(i);
            db.update("INSERT INTO MOBILE_TRACKING (" +
                    "TRACKING_DATE,LOCATION,USER_ID)" +
                    "VALUES" +
                    "('"+track.getString("date")+"'," +
                    "'"+track.getString("location")+"'," +
                    "'"+userMobile.getUser_id()+"')");
        }
        Response response=new Response();
        response.setStatus(true);
        response.setMessage("Tracking Saved");
        return response;
    }
}
