package mobile.dao;

import mobile.dto.FormFields;
import mobile.dto.Param;
import mobile.dto.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Indocyber on 24/04/2018.
 */
@Repository("FormFieldDao")
public class FormFieldDaoImpl implements FormFieldDao{
    private JdbcTemplate db;
    @Autowired
    public void setDataSource(DataSource dataSource){
        this.db=new JdbcTemplate(dataSource);
    }
    @Override
    public Response getFormFields(int start, int end) {
        Response response=new Response();
        response.setStatus(true);
        response.setMessage("Object Received");
        List<Object> data=db.query("SELECT * FROM (SELECT\n" +
                "ROW_NUMBER() OVER(ORDER BY dbo.FORM_FIELDS.field_id ASC) as row," +
                "dbo.FORM_FIELDS.field_id,\n" +
                "dbo.FORM_FIELDS.field_name,\n" +
                "dbo.FORM_FIELDS.field_type,\n" +
                "dbo.FORM_FIELDS.field_visibility,\n" +
                "dbo.FORM_FIELDS.field_label,\n" +
                "dbo.FORM_FIELDS.field_global_value,\n" +
                "dbo.FORM_FIELDS.field_weight,\n" +
                "dbo.FORM_FIELDS.field_store,\n" +
                "dbo.FORM_FIELDS.field_extra,\n" +
                "dbo.FORM_FIELDS.row_id\n" +
                "FROM\n" +
                "dbo.FORM_FIELDS\n) AS a\n" +
                "WHERE\n" +
                "row>="+start+" AND row<="+end, new RowMapper<Object>() {
            @Override
            public FormFields mapRow(ResultSet resultSet, int i) throws SQLException {
                FormFields formFields=new FormFields();
                formFields.setField_id(resultSet.getString("field_id"));
                formFields.setField_name(resultSet.getString("field_name"));
                formFields.setField_type(resultSet.getString("field_type"));
                formFields.setField_label(resultSet.getString("field_label"));
                formFields.setField_visibility(resultSet.getString("field_visibility"));
                formFields.setField_global_value(resultSet.getString("field_global_value"));
                formFields.setField_weight(resultSet.getString("field_weight"));
                formFields.setField_store(resultSet.getString("field_store"));
                formFields.setField_extra(resultSet.getString("field_extra"));
                formFields.setRow_id(resultSet.getString("row_id"));
                return formFields;
            }
        });
        response.setData(data);
        return response;
    }
}
