package mobile.dto;

/**
 * Created by Indocyber on 02/04/2018.
 */
public class OrderDetail {
    private String order_id;
    private String contract_no;
    private String installment_no;
    private String lpk_no;
    private String nama_barang;
    private String due_date;
    private String installment_amt;
    private String penalty_amt;
    private String collection_fee;
    private String total_amt;
    private String receipt_no;
    private String min_payment;
    private String max_payment;
    private String repayment_date;
    private String notes;
    private int deposit_amt;
    private int promise_count;
    private int  promise_available;
    private String promise_range;
    private String status;

    public int getDeposit_amt() {
        return deposit_amt;
    }

    public void setDeposit_amt(int deposit_amt) {
        this.deposit_amt = deposit_amt;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getContract_no() {
        return contract_no;
    }

    public void setContract_no(String contract_no) {
        this.contract_no = contract_no;
    }

    public String getInstallment_no() {
        return installment_no;
    }

    public void setInstallment_no(String installment_no) {
        this.installment_no = installment_no;
    }

    public String getLpk_no() {
        return lpk_no;
    }

    public void setLpk_no(String lpk_no) {
        this.lpk_no = lpk_no;
    }

    public String getNama_barang() {
        return nama_barang;
    }

    public void setNama_barang(String nama_barang) {
        this.nama_barang = nama_barang;
    }

    public String getDue_date() {
        return due_date;
    }

    public void setDue_date(String due_date) {
        this.due_date = due_date;
    }

    public String getInstallment_amt() {
        return installment_amt;
    }

    public void setInstallment_amt(String installment_amt) {
        this.installment_amt = installment_amt;
    }

    public String getPenalty_amt() {
        return penalty_amt;
    }

    public void setPenalty_amt(String penalty_amt) {
        this.penalty_amt = penalty_amt;
    }

    public String getCollection_fee() {
        return collection_fee;
    }

    public void setCollection_fee(String collection_fee) {
        this.collection_fee = collection_fee;
    }

    public String getTotal_amt() {
        return total_amt;
    }

    public void setTotal_amt(String total_amt) {
        this.total_amt = total_amt;
    }

    public String getReceipt_no() {
        return receipt_no;
    }

    public void setReceipt_no(String receipt_no) {
        this.receipt_no = receipt_no;
    }

    public String getMin_payment() {
        return min_payment;
    }

    public void setMin_payment(String min_payment) {
        this.min_payment = min_payment;
    }

    public String getMax_payment() {
        return max_payment;
    }

    public void setMax_payment(String max_payment) {
        this.max_payment = max_payment;
    }

    public String getRepayment_date() {
        return repayment_date;
    }

    public void setRepayment_date(String repayment_date) {
        this.repayment_date = repayment_date;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public int getPromise_count() {
        return promise_count;
    }

    public void setPromise_count(int promise_count) {
        this.promise_count = promise_count;
    }

    public int getPromise_available() {
        return promise_available;
    }

    public void setPromise_available(int promise_available) {
        this.promise_available = promise_available;
    }

    public String getPromise_range() {
        return promise_range;
    }

    public void setPromise_range(String promise_range) {
        this.promise_range = promise_range;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
