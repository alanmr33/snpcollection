package mobile.dto;

/**
 * Created by Indocyber on 24/04/2018.
 */
public class FormRows {
    private String row_id;
    private int row_order;
    private String row_visibility;
    private String form_id;

    public String getRow_id() {
        return row_id;
    }

    public void setRow_id(String row_id) {
        this.row_id = row_id;
    }

    public int getRow_order() {
        return row_order;
    }

    public void setRow_order(int row_order) {
        this.row_order = row_order;
    }

    public String getRow_visibility() {
        return row_visibility;
    }

    public void setRow_visibility(String row_visibility) {
        this.row_visibility = row_visibility;
    }

    public String getForm_id() {
        return form_id;
    }

    public void setForm_id(String form_id) {
        this.form_id = form_id;
    }
}
