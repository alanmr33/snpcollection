package mobile.dto;

/**
 * Created by Indocyber on 27/03/2018.
 */
public class UserMobile {
    private String user_id;
    private String kode_kolektor;
    private String kode_karyawan;
    private String nama_karyawan;
    private String phone_number;
    private String kode_cabang;
    private String kode_region;
    private String token;
    private String imei;

    public String getKode_cabang() {
        return kode_cabang;
    }

    public void setKode_cabang(String kode_cabang) {
        this.kode_cabang = kode_cabang;
    }

    public String getKode_region() {
        return kode_region;
    }

    public void setKode_region(String kode_region) {
        this.kode_region = kode_region;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getKode_kolektor() {
        return kode_kolektor;
    }

    public void setKode_kolektor(String kode_kolektor) {
        this.kode_kolektor = kode_kolektor;
    }

    public String getKode_karyawan() {
        return kode_karyawan;
    }

    public void setKode_karyawan(String kode_karyawan) {
        this.kode_karyawan = kode_karyawan;
    }

    public String getNama_karyawan() {
        return nama_karyawan;
    }

    public void setNama_karyawan(String nama_karyawan) {
        this.nama_karyawan = nama_karyawan;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
