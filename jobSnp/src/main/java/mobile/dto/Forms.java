package mobile.dto;

/**
 * Created by Indocyber on 24/04/2018.
 */
public class Forms {
    private String form_id;
    private String form_name;
    private String form_label;
    private int form_order;

    public String getForm_id() {
        return form_id;
    }

    public void setForm_id(String form_id) {
        this.form_id = form_id;
    }

    public String getForm_name() {
        return form_name;
    }

    public void setForm_name(String form_name) {
        this.form_name = form_name;
    }

    public String getForm_label() {
        return form_label;
    }

    public void setForm_label(String form_label) {
        this.form_label = form_label;
    }

    public int getForm_order() {
        return form_order;
    }

    public void setForm_order(int form_order) {
        this.form_order = form_order;
    }
}
