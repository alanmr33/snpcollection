package mobile.dto;

/**
 * Created by Indocyber on 04/04/2018.
 */
public class Provinsi {
    private String kode_provinsi;
    private String nama_provinsi;

    public String getKode_provinsi() {
        return kode_provinsi;
    }

    public void setKode_provinsi(String kode_provinsi) {
        this.kode_provinsi = kode_provinsi;
    }

    public String getNama_provinsi() {
        return nama_provinsi;
    }

    public void setNama_provinsi(String nama_provinsi) {
        this.nama_provinsi = nama_provinsi;
    }
}
