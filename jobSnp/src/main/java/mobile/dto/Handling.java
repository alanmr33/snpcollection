package mobile.dto;

/**
 * Created by Indocyber on 04/04/2018.
 */
public class Handling {
    private int handling_no;
    private String handling_code;
    private String deskripsi;

    public int getHandling_no() {
        return handling_no;
    }

    public void setHandling_no(int handling_no) {
        this.handling_no = handling_no;
    }

    public String getHandling_code() {
        return handling_code;
    }

    public void setHandling_code(String handling_code) {
        this.handling_code = handling_code;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
}
