package mobile.dto;


import java.util.List;

/**
 * Created by Indocyber on 02/04/2018.
 */
public class OrderHeader {
    /*
    STATUS LIST : ASN , RSN , DWN , SBT ,CLS
     */
    private String order_id;
    private String lpk_no;
    private String lpk_date;
    private String customer_code;
    private String customer_name;
    private String address;
    private String village;
    private String district;
    private String city;
    private String rt;
    private String rw;
    private String kode_provinsi;
    private String post_code;
    private String phone_area;
    private String phone_number;
    private String mobile_prefix;
    private String mobile_no;
    private String status;
    private String user_id;
    private String data;
    private List<OrderDetail> details;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCustomer_code() {
        return customer_code;
    }

    public void setCustomer_code(String customer_code) {
        this.customer_code = customer_code;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getLpk_no() {
        return lpk_no;
    }

    public void setLpk_no(String lpk_no) {
        this.lpk_no = lpk_no;
    }

    public String getLpk_date() {
        return lpk_date;
    }

    public void setLpk_date(String lpk_date) {
        this.lpk_date = lpk_date;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getRt() {
        return rt;
    }

    public void setRt(String rt) {
        this.rt = rt;
    }

    public String getRw() {
        return rw;
    }

    public void setRw(String rw) {
        this.rw = rw;
    }

    public String getKode_provinsi() {
        return kode_provinsi;
    }

    public void setKode_provinsi(String kode_provinsi) {
        this.kode_provinsi = kode_provinsi;
    }

    public String getPost_code() {
        return post_code;
    }

    public void setPost_code(String post_code) {
        this.post_code = post_code;
    }

    public String getPhone_area() {
        return phone_area;
    }

    public void setPhone_area(String phone_area) {
        this.phone_area = phone_area;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getMobile_prefix() {
        return mobile_prefix;
    }

    public void setMobile_prefix(String mobile_prefix) {
        this.mobile_prefix = mobile_prefix;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public List<OrderDetail> getDetails() {
        return details;
    }

    public void setDetails(List<OrderDetail> details) {
        this.details = details;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

}