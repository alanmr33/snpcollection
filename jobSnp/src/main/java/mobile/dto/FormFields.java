package mobile.dto;

/**
 * Created by Indocyber on 24/04/2018.
 */
public class FormFields {
    private String field_id;
    private String field_name;
    private String field_type;
    private String field_visibility;
    private String field_label;
    private String field_global_value;
    private String field_weight;
    private String field_store;
    private String field_extra;
    private String row_id;

    public String getField_id() {
        return field_id;
    }

    public void setField_id(String field_id) {
        this.field_id = field_id;
    }

    public String getField_name() {
        return field_name;
    }

    public void setField_name(String field_name) {
        this.field_name = field_name;
    }

    public String getField_type() {
        return field_type;
    }

    public void setField_type(String field_type) {
        this.field_type = field_type;
    }

    public String getField_visibility() {
        return field_visibility;
    }

    public void setField_visibility(String field_visibility) {
        this.field_visibility = field_visibility;
    }

    public String getField_label() {
        return field_label;
    }

    public void setField_label(String field_label) {
        this.field_label = field_label;
    }

    public String getField_global_value() {
        return field_global_value;
    }

    public void setField_global_value(String field_global_value) {
        this.field_global_value = field_global_value;
    }

    public String getField_weight() {
        return field_weight;
    }

    public void setField_weight(String field_weight) {
        this.field_weight = field_weight;
    }

    public String getField_store() {
        return field_store;
    }

    public void setField_store(String field_store) {
        this.field_store = field_store;
    }

    public String getField_extra() {
        return field_extra;
    }

    public void setField_extra(String field_extra) {
        this.field_extra = field_extra;
    }

    public String getRow_id() {
        return row_id;
    }

    public void setRow_id(String row_id) {
        this.row_id = row_id;
    }
}
