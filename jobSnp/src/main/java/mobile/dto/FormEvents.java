package mobile.dto;

/**
 * Created by Indocyber on 25/04/2018.
 */
public class FormEvents {
    private  String event_id;
    private  String event_type;
    private  String event_component;
    private  String event_component_target;
    private  String event_data;
    private  String form_id;

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getEvent_component() {
        return event_component;
    }

    public void setEvent_component(String event_component) {
        this.event_component = event_component;
    }

    public String getEvent_component_target() {
        return event_component_target;
    }

    public void setEvent_component_target(String event_component_target) {
        this.event_component_target = event_component_target;
    }

    public String getEvent_data() {
        return event_data;
    }

    public void setEvent_data(String event_data) {
        this.event_data = event_data;
    }

    public String getForm_id() {
        return form_id;
    }

    public void setForm_id(String form_id) {
        this.form_id = form_id;
    }
}
