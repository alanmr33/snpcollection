package mobile.dto;


import java.util.List;

/**
 * Created by Indocyber on 27/03/2018.
 */
public class Response {
    private boolean status;
    private String message;
    private List<Object> data;

    public List<Object> getData() {
        return data;
    }

    public void setData(List<Object> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {

        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

}
