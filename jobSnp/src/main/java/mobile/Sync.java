package mobile;

import mobile.dao.*;
import mobile.dto.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Indocyber on 03/04/2018.
 */
@RestController
public class Sync {
    @Autowired
    private EventDao eventDao;
    @Autowired
    private FormFieldDao fieldDao;
    @Autowired
    private FormDao formDao;
    @Autowired
    private FormRowDao formRowDao;
    @Autowired
    private ParamDao paramDao;
    @Autowired
    private ProvinsiDao provinsiDao;
    @Autowired
    private HandlingDao handlingDao;
    @RequestMapping(value = "/sync",
            method = RequestMethod.GET,
            headers = {"APPID=IFGCOL-IGLO"})
    @ResponseBody
    public Response getOrder(@RequestParam("table")String tableName,
                             @RequestParam("page")int page){
        if(tableName.equals("param") || tableName.equals("form_fields")) {
            int start = 0;
            start = (page > 0) ? (page - 1) * 5000 : 0;
            int end = page * 5000;
            if(tableName.equals("param")){
                return paramDao.getParam(start, end);
            }else{
                return fieldDao.getFormFields(start,end);
            }
        }
        if(tableName.equals("provinsi") && page==1){
            return  provinsiDao.getProvinsi();
        }
        if(tableName.equals("handling") && page==1){
            return  handlingDao.getHandlings();
        }
        if(tableName.equals("forms") && page==1){
            return  formDao.getForms();
        }
        if(tableName.equals("form_rows") && page==1){
            return  formRowDao.getFormRows();
        }
        if(tableName.equals("events") && page==1){
            return  eventDao.getEvents();
        }
        Response response=new Response();
        response.setStatus(false);
        response.setMessage("No Table Found");
        return response;
    }
}

