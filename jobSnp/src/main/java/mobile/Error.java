package mobile;

import mobile.dto.Response;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Indocyber on 27/03/2018.
 */
@RestController
@ControllerAdvice
public class Error {
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public Response error400(){
        Response response = new Response();
        response.setMessage("Not Enough Access");
        response.setStatus(false);
        return response;
    }
    @ExceptionHandler(ClassNotFoundException.class)
    public Response error404(){
        Response response = new Response();
        response.setMessage("Error 404");
        response.setStatus(false);
        return response;
    }
}
