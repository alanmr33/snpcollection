package mobile;

import mobile.dao.OrderDao;
import mobile.dto.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Indocyber on 02/04/2018.
 */
@RestController
public class Order {
    @Autowired
    OrderDao orderDao;
    @RequestMapping(value = "/order",
            method = RequestMethod.GET,
            headers = {"APPID=IFGCOL-IGLO"})
    @ResponseBody
    public Response getOrder(@RequestParam("token") String token){
        return orderDao.getOrder(token);
    }
    @RequestMapping(value = "/order",
            method = RequestMethod.POST,
            headers = {"APPID=IFGCOL-IGLO"})
    @ResponseBody
    public Response saveOrder(@RequestParam("token") String token,
                              @RequestParam("order_id") String order_id,
                              @RequestParam("body") String body){
        return orderDao.saveOrder(token,order_id,body);
    }
}
