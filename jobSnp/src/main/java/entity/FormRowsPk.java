package entity;

import java.io.Serializable;

public class FormRowsPk implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String rowId;

	public String getRowId() {
		return rowId;
	}

	public void setRowId(String rowId) {
		this.rowId = rowId;
	}

	

}
