package entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.common.util.StringHelper;

@Entity
@Table(name="PAYMENT")
@IdClass(PaymentPK.class)
public class Payment {
	private int installmentNo;
	private int orderId;
	private String customerCode;
	private String contractNo;
	private String codeCollector;
	private int totalBayar;
	private int noKwitansi;
	private String noLpk;
	private Date tanggalTransaksi;
	private Date tanggalBayar;
	private String status;
	private String createUser;
	private Date createDate;
	private String modifyUser;
	private Date modifyDate;
	
	@Id
	@Column(name="INSTALLMENT_NO")
	public int getInstallmentNo() {
		return installmentNo;
	}
	public void setInstallmentNo(int installmentNo) {
		this.installmentNo = installmentNo;
	}
	
	@Column(name="ORDER_ID")
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	
	@Column(name="CUSTOMER_CODE")
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	
	@Column(name="CONTRACT_NO")
	public String getContractNo() {
		return contractNo;
	}
	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}
	
	@Column(name="CODE_COLLECTOR")
	public String getCodeCollector() {
		return codeCollector;
	}
	public void setCodeCollector(String codeCollector) {
		this.codeCollector = codeCollector;
	}
	
	@Column(name="TOTAL_BAYAR")
	public int getTotalBayar() {
		return totalBayar;
	}
	public void setTotalBayar(int totalBayar) {
		this.totalBayar = totalBayar;
	}
	
	@Column(name="NO_KWITANSI")
	public int getNoKwitansi() {
		return noKwitansi;
	}
	public void setNoKwitansi(int noKwitansi) {
		this.noKwitansi = noKwitansi;
	}
	
	@Column(name="NO_LPK")
	public String getNoLpk() {
		return noLpk;
	}
	public void setNoLpk(String noLpk) {
		this.noLpk = noLpk;
	}
	
	@Column(name="TANGGAL_TRANSAKSI")
	public Date getTanggalTransaksi() {
		return tanggalTransaksi;
	}
	public void setTanggalTransaksi(Date tanggalTransaksi) {
		this.tanggalTransaksi = tanggalTransaksi;
	}
	
	@Column(name="TANGGAL_BAYAR")
	public Date getTanggalBayar() {
		return tanggalBayar;
	}
	public void setTanggalBayar(Date tanggalBayar) {
		this.tanggalBayar = tanggalBayar;
	}
	
	@Column(name="STATUS")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="CREATE_USER")
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	
	@Column(name="CREATED_DATE")
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	@Column(name="MODIFY_USER")
	public String getModifyUser() {
		return modifyUser;
	}
	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}
	
	@Column(name="MODIFY_DATE")
	public Date getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
		
}
