package entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;


@Entity
@Table(name="PARAM_MST")
@IdClass(ParamMstPK.class)
public class ParamMst {
	private String paramId;
	private String condition;
	private String levelParam;
	private String parentId;
	
	@Id
	@Column(name="PARAM_ID")
	public String getParamId() {
		return paramId;
	}
	public void setParamId(String paramId) {
		this.paramId = paramId;
	}
	
	@Column(name="CONDITION")
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	
	@Column(name="LEVEL_PARAM")
	public String getLevelParam() {
		return levelParam;
	}
	public void setLevelParam(String levelParam) {
		this.levelParam = levelParam;
	}
	
	@Column(name="PARENT_ID")
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	
	
	
	
	
}
