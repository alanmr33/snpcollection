package entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name="REGION_MST")
@IdClass(MasterRegionalPK.class)
public class MasterRegional {
	private String kode_region;
	private String nama_region;
	private String notes;
	private int aktif_status;
	private String create_user;
	private Date create_date;
	private String modify_user;
	private Date modify_date;
	
	@Id
	@Column(name="KODE_REGION")
	public String getKode_region() {
		return kode_region;
	}
	public void setKode_region(String kode_region) {
		this.kode_region = kode_region;
	}

	@Column(name="NAMA_REGION")
	public String getNama_region() {
		return nama_region;
	}
	public void setNama_region(String nama_region) {
		this.nama_region = nama_region;
	}
	
	@Column(name="NOTES")
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	@Column(name="AKTIF_STATUS")
	public int getAktif_status() {
		return aktif_status;
	}
	public void setAktif_status(int aktif_status) {
		this.aktif_status = aktif_status;
	}
	
	@Column(name="CREATE_USER")
	public String getCreate_user() {
		return create_user;
	}
	public void setCreate_user(String create_user) {
		this.create_user = create_user;
	}
	
	@Column(name="CREATE_DATE")
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
	
	@Column(name="MODIFY_USER")
	public String getModify_user() {
		return modify_user;
	}
	public void setModify_user(String modify_user) {
		this.modify_user = modify_user;
	}
	
	@Column(name="MODIFY_DATE")
	public Date getModify_date() {
		return modify_date;
	}
	public void setModify_date(Date modify_date) {
		this.modify_date = modify_date;
	}

	
}
