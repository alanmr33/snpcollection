package entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name="USER_PROFILE")
@IdClass(UserProfilePK.class)
public class UserProfile {
	private String userId;
	private String kodeCabang;
	private String kodeRegion;
	private int aktifStatus;
	private String password;
	private String authorityId;
	private String kodeKolektor;
	private String kodeKaryawan;
	private String namaKaryawan;
	private String imeiNumber;
	private String phoneNumber;
	private Date tanggalMulai;
	private Date tanggalAkhir;
	private String createUser;
	private Date createDate;
	private String modifyUser;
	private Date modifyDate;
	
	@Id
	@Column(name="USER_ID")
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	@Column(name="KODE_CABANG")
	public String getKodeCabang() {
		return kodeCabang;
	}
	public void setKodeCabang(String kodeCabang) {
		this.kodeCabang = kodeCabang;
	}
	
	@Column(name="KODE_REGIONAL")
	public String getKodeRegion() {
		return kodeRegion;
	}
	public void setKodeRegion(String kodeRegion) {
		this.kodeRegion = kodeRegion;
	}
	
	@Column(name="AKTIF_STATUS")
	public int getAktifStatus() {
		return aktifStatus;
	}
	public void setAktifStatus(int aktifStatus) {
		this.aktifStatus = aktifStatus;
	}
	
	@Column(name="PASSWORD")
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Column(name="AUTHORITY_ID")
	public String getAuthorityId() {
		return authorityId;
	}
	public void setAuthorityId(String authorityId) {
		this.authorityId = authorityId;
	}
	
	@Column(name="KODE_KOLEKTOR")
	public String getKodeKolektor() {
		return kodeKolektor;
	}
	public void setKodeKolektor(String kodeKolektor) {
		this.kodeKolektor = kodeKolektor;
	}
	
	@Column(name="KODE_KARYAWAN")
	public String getKodeKaryawan() {
		return kodeKaryawan;
	}
	public void setKodeKaryawan(String kodeKaryawan) {
		this.kodeKaryawan = kodeKaryawan;
	}
	
	@Column(name="NAMA_KARYAWAN")
	public String getNamaKaryawan() {
		return namaKaryawan;
	}
	public void setNamaKaryawan(String namaKaryawan) {
		this.namaKaryawan = namaKaryawan;
	}
	
	@Column(name="IMEI_NUMBER")
	public String getImeiNumber() {
		return imeiNumber;
	}
	public void setImeiNumber(String imeiNumber) {
		this.imeiNumber = imeiNumber;
	}
	
	@Column(name="PHONE_NUMBER")
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	@Column(name="TANGGAL_MULAI")
	public Date getTanggalMulai() {
		return tanggalMulai;
	}
	public void setTanggalMulai(Date tanggalMulai) {
		this.tanggalMulai = tanggalMulai;
	}
	
	@Column(name="TANGGAL_AKHIR")
	public Date getTanggalAkhir() {
		return tanggalAkhir;
	}
	public void setTanggalAkhir(Date tanggalAkhir) {
		this.tanggalAkhir = tanggalAkhir;
	}
	
	@Column(name="CREATE_USER")
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	
	@Column(name="CREATE_DATE")
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	@Column(name="MODIFY_USER")
	public String getModifyUser() {
		return modifyUser;
	}
	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}
	
	@Column(name="MODIFY_DATE")
	public Date getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	
	
	
	
	
	
}
