package entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name="MASTER_PROVINSI")
@IdClass(MasterProvinsiPK.class)
public class MasterProvinsi {
	private String kodeProvinsi;
	private String namaProvinsi;
	private int aktifStatus;
	private String createBy;
	private Date createDate;
	private String modifyBy;
	private Date modifyDate;
	
	@Id
	@Column(name="KODE_PROVINSI")
	public String getKodeProvinsi() {
		return kodeProvinsi;
	}
	public void setKodeProvinsi(String kodeProvinsi) {
		this.kodeProvinsi = kodeProvinsi;
	}
	
	@Column(name="NAMA_PROVINSI")
	public String getNamaProvinsi() {
		return namaProvinsi;
	}
	public void setNamaProvinsi(String namaProvinsi) {
		this.namaProvinsi = namaProvinsi;
	}
	
	@Column(name="ACTIVE_STATUS")
	public int getAktifStatus() {
		return aktifStatus;
	}
	public void setAktifStatus(int aktifStatus) {
		this.aktifStatus = aktifStatus;
	}
	
	@Column(name="CREATE_USER")
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	
	@Column(name="CREATE_DATE")
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	@Column(name="MODIFY_USER")
	public String getModifyBy() {
		return modifyBy;
	}
	public void setModifyBy(String modifyBy) {
		this.modifyBy = modifyBy;
	}
	
	@Column(name="MODIFY_DATE")
	public Date getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	
	
	

}
