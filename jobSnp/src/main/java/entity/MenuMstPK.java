package entity;

import java.io.Serializable;

public class MenuMstPK implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private int menu;

	public int getMenu() {
		return menu;
	}

	public void setMenu(int menu) {
		this.menu = menu;
	}

	

}
