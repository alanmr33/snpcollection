package entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name="EVENTS")
@IdClass(EventsPk.class)
public class Events {
	private String eventId;
	private String eventType;
	private String eventComponent;
	private String eventComponentTarget;
	private String eventData;
	private String formId;
	
	@Id
	@Column(name="event_id")
	public String getEventId() {
		return eventId;
	}
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="event_type")
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	
	@Column(name="event_component")
	public String getEventComponent() {
		return eventComponent;
	}
	public void setEventComponent(String eventComponent) {
		this.eventComponent = eventComponent;
	}
	
	@Column(name="event_component_target")
	public String getEventComponentTarget() {
		return eventComponentTarget;
	}
	public void setEventComponentTarget(String eventComponentTarget) {
		this.eventComponentTarget = eventComponentTarget;
	}
	
	@Column(name="event_data")
	public String getEventData() {
		return eventData;
	}
	public void setEventData(String eventData) {
		this.eventData = eventData;
	}
	
	@Column(name="form_id")
	public String getFormId() {
		return formId;
	}
	public void setFormId(String formId) {
		this.formId = formId;
	}
			
}
