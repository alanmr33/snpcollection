package entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.common.util.StringHelper;

@Entity
@Table(name="HANDLING")
@IdClass(HandlingPK.class)
public class Handling {
	private int batchHandling;
	private String countracNo;
	private int installmentNo;
	private int orderId;
	private String kodeHandling;
	private String kodeKonsumen;
	private String noLpk;
	private String notes;
	private Date tanggalHandling;
	private Date tanggalJanji;
	private int promiseCount;
	private int promiseAvailable;
	private int promiseRange;
	private String status;
	private String createUser;
	private Date createDate;
	private String modifyUser;
	private Date modifyDate;
	
	@Id
	@Column(name="BATCH_HANDLING")
	public int getBatchHandling() {
		return batchHandling;
	}
	public void setBatchHandling(int batchHandling) {
		this.batchHandling = batchHandling;
	}
	
	@Column(name="CONTRACT_NO")
	public String getCountracNo() {
		return countracNo;
	}
	public void setCountracNo(String countracNo) {
		this.countracNo = countracNo;
	}
	
	@Column(name="INSTALLMENT_NO")
	public int getInstallmentNo() {
		return installmentNo;
	}
	public void setInstallmentNo(int installmentNo) {
		this.installmentNo = installmentNo;
	}
	
	@Column(name="ORDER_ID")
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	
	@Column(name="KODE_HANDLING")
	public String getKodeHandling() {
		return kodeHandling;
	}
	public void setKodeHandling(String kodeHandling) {
		this.kodeHandling = kodeHandling;
	}
	
	@Column(name="KODE_KONSUMEN")
	public String getKodeKonsumen() {
		return kodeKonsumen;
	}
	public void setKodeKonsumen(String kodeKonsumen) {
		this.kodeKonsumen = kodeKonsumen;
	}
	
	@Column(name="NO_LPK")
	public String getNoLpk() {
		return noLpk;
	}
	public void setNoLpk(String noLpk) {
		this.noLpk = noLpk;
	}
	
	@Column(name="NOTES")
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	@Column(name="TANGGAL_HANDLING")
	public Date getTanggalHandling() {
		return tanggalHandling;
	}
	public void setTanggalHandling(Date tanggalHandling) {
		this.tanggalHandling = tanggalHandling;
	}
	
	@Column(name="TANGGAL_JANJI")
	public Date getTanggalJanji() {
		return tanggalJanji;
	}
	public void setTanggalJanji(Date tanggalJanji) {
		this.tanggalJanji = tanggalJanji;
	}
	
	@Column(name="PROMISE_COUNT")
	public int getPromiseCount() {
		return promiseCount;
	}
	public void setPromiseCount(int promiseCount) {
		this.promiseCount = promiseCount;
	}
	
	@Column(name="PROMISE_AVAILABLE")
	public int getPromiseAvailable() {
		return promiseAvailable;
	}
	public void setPromiseAvailable(int promiseAvailable) {
		this.promiseAvailable = promiseAvailable;
	}
	
	@Column(name="PROMISE_RANGE")
	public int getPromiseRange() {
		return promiseRange;
	}
	public void setPromiseRange(int promiseRange) {
		this.promiseRange = promiseRange;
	}
	
	@Column(name="STATUS")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	@Column(name="CREATE_USER")
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	
	@Column(name="CREATE_DATE")
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	@Column(name="MODIFY_USER")
	public String getModifyUser() {
		return modifyUser;
	}
	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}
	
	@Column(name="MODIFY_DATE")
	public Date getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	
}
