package entity;

public class UserMenuAccordion {
	private String title;
	private String url;

	public UserMenuAccordion() {
	}

	public UserMenuAccordion(String title) {
		this.title = title;
	}
	
	public UserMenuAccordion(String title, String url){
		this.title = title;
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}


	
	
}
