package entity;

import java.io.Serializable;

public class FormFieldsPk implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String fieldId;

	public String getFieldId() {
		return fieldId;
	}

	public void setFieldId(String fieldId) {
		this.fieldId = fieldId;
	}

}
