package entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name="FORM_FIELDS")
@IdClass(FormFieldsPk.class)
public class FormFields {
	private String fieldId;
	private String fieldName;
	private String fieldType;
	private String fieldVisibility;
	private String fieldLabel;
	private String fieldGlobalValue;
	private String fieldWeight;
	private String fieldStore;
	private String fieldExtra;
	private String rowId;
	
	@Id
	@Column(name="field_id")
	public String getFieldId() {
		return fieldId;
	}
	public void setFieldId(String fieldId) {
		this.fieldId = fieldId;
	}
	
	@Column(name="field_name")
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	
	@Column(name="field_type")
	public String getFieldType() {
		return fieldType;
	}
	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}
	
	@Column(name="field_visibility")
	public String getFieldVisibility() {
		return fieldVisibility;
	}
	public void setFieldVisibility(String fieldVisibility) {
		this.fieldVisibility = fieldVisibility;
	}
	
	@Column(name="field_label")
	public String getFieldLabel() {
		return fieldLabel;
	}
	public void setFieldLabel(String fieldLabel) {
		this.fieldLabel = fieldLabel;
	}
	
	@Column(name="field_global_value")
	public String getFieldGlobalValue() {
		return fieldGlobalValue;
	}
	public void setFieldGlobalValue(String fieldGlobalValue) {
		this.fieldGlobalValue = fieldGlobalValue;
	}
	
	@Column(name="field_weight")
	public String getFieldWeight() {
		return fieldWeight;
	}
	public void setFieldWeight(String fieldWeight) {
		this.fieldWeight = fieldWeight;
	}
	
	@Column(name="field_store")
	public String getFieldStore() {
		return fieldStore;
	}
	public void setFieldStore(String fieldStore) {
		this.fieldStore = fieldStore;
	}
	
	@Column(name="field_extra")
	public String getFieldExtra() {
		return fieldExtra;
	}
	public void setFieldExtra(String fieldExtra) {
		this.fieldExtra = fieldExtra;
	}
	
	@Column(name="row_id")
	public String getRowId() {
		return rowId;
	}
	public void setRowId(String rowId) {
		this.rowId = rowId;
	}
	
			
}
