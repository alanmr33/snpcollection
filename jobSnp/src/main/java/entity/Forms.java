package entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name="FORMS")
@IdClass(FormsPk.class)
public class Forms {
	private String formId;
	private String formName;
	private String formLabel;
	private String formOrder;
	
	
	@Id
	@Column(name="form_id")
	public String getFormId() {
		return formId;
	}
	public void setFormId(String formId) {
		this.formId = formId;
	}
	
	@Column(name="form_name")
	public String getFormName() {
		return formName;
	}
	public void setFormName(String formName) {
		this.formName = formName;
	}
	
	@Column(name="form_label")
	public String getFormLabel() {
		return formLabel;
	}
	public void setFormLabel(String formLabel) {
		this.formLabel = formLabel;
	}
	
	@Column(name="form_order")
	public String getFormOrder() {
		return formOrder;
	}
	public void setFormOrder(String formOrder) {
		this.formOrder = formOrder;
	}		
}
