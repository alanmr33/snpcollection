package entity;

import java.io.Serializable;

public class AuthorityMstPK implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private int authorityId;

	public int getAuthorityId() {
		return authorityId;
	}

	public void setAuthorityId(int authorityId) {
		this.authorityId = authorityId;
	}

}
