package entity;

import java.io.Serializable;

public class MasterRegionalPK implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private int kode_region;

	public int getKode_region() {
		return kode_region;
	}

	public void setKode_region(int kode_region) {
		this.kode_region = kode_region;
	}


}
