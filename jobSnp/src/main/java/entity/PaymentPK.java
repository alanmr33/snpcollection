package entity;

import java.io.Serializable;

public class PaymentPK implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private int installmentNo;

	public int getInstallmentNo() {
		return installmentNo;
	}

	public void setInstallmentNo(int installmentNo) {
		this.installmentNo = installmentNo;
	}	
}
