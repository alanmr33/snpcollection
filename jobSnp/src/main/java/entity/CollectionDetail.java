package entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name="COLLECTION_DETAIL")
@IdClass(CollectionHeaderPK.class)
public class CollectionDetail {
	private int orderId;
	private String customerCodde;
	private String contractNo;
	private int installmentNo;
	private String lpkNo;
	private String customerName;
	private String address;
	private String village;
	private String district;
	private String rt;
	private String rw;
	private String city;
	private String kodeProvinsi;
	private String postCode;
	private Date dueDate;
	private int installmentAmt;
	private int depositAmt;
	private int penaltyAmt;
	private int collectionFee;
	private int totalAmt;
	private String receiptNo;
	private int minPayment;
	private int maxPayment;
	private Date repaymentDate;
	private String notes;
	private int promiseCount;
	private int promiseAvailable;
	private int promiseRange;
	private String phoneArea;
	private String phoneNumber;
	private String mobilePrefix;
	private String mobileNo;
	private String namaBarang;
	private String photo;
	
	@Id
	@Column(name="ORDER_ID")
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	
	@Column(name="CUSTOMER_CODE")
	public String getCustomerCodde() {
		return customerCodde;
	}
	public void setCustomerCodde(String customerCodde) {
		this.customerCodde = customerCodde;
	}
	
	@Column(name="CONTRACT_NO")
	public String getContractNo() {
		return contractNo;
	}
	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}
	
	@Column(name="INSTALLMENT_NO")
	public int getInstallmentNo() {
		return installmentNo;
	}
	public void setInstallmentNo(int installmentNo) {
		this.installmentNo = installmentNo;
	}
	
	@Column(name="LPK_NO")
	public String getLpkNo() {
		return lpkNo;
	}
	public void setLpkNo(String lpkNo) {
		this.lpkNo = lpkNo;
	}
	
	@Column(name="CUSTOMER_NAME")
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	@Column(name="ADDRESS")
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	@Column(name="VILLAGE")
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	
	@Column(name="DISTRICT")
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	
	@Column(name="RT")
	public String getRt() {
		return rt;
	}
	public void setRt(String rt) {
		this.rt = rt;
	}
	
	@Column(name="RW")
	public String getRw() {
		return rw;
	}
	public void setRw(String rw) {
		this.rw = rw;
	}
	
	@Column(name="CITY")
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	@Column(name="KODE_PROVINSI")
	public String getKodeProvinsi() {
		return kodeProvinsi;
	}
	public void setKodeProvinsi(String kodeProvinsi) {
		this.kodeProvinsi = kodeProvinsi;
	}
	
	@Column(name="POST_CODE")
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	
	@Column(name="DUE_DATE")
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	
	@Column(name="INSTALLMENT_AMT")
	public int getInstallmentAmt() {
		return installmentAmt;
	}
	public void setInstallmentAmt(int installmentAmt) {
		this.installmentAmt = installmentAmt;
	}
	
	@Column(name="DEPOSIT_AMT")
	public int getDepositAmt() {
		return depositAmt;
	}
	public void setDepositAmt(int depositAmt) {
		this.depositAmt = depositAmt;
	}
	
	@Column(name="PENALTY_AMT")
	public int getPenaltyAmt() {
		return penaltyAmt;
	}
	public void setPenaltyAmt(int penaltyAmt) {
		this.penaltyAmt = penaltyAmt;
	}
	
	@Column(name="COLLECTION_FEE")
	public int getCollectionFee() {
		return collectionFee;
	}
	public void setCollectionFee(int collectionFee) {
		this.collectionFee = collectionFee;
	}
	
	@Column(name="TOTAL_AMT")
	public int getTotalAmt() {
		return totalAmt;
	}
	public void setTotalAmt(int totalAmt) {
		this.totalAmt = totalAmt;
	}
	
	@Column(name="RECEIPT_NO")
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	
	@Column(name="MIN_PAYMENT")
	public int getMinPayment() {
		return minPayment;
	}
	public void setMinPayment(int minPayment) {
		this.minPayment = minPayment;
	}
	
	@Column(name="MAX_PAYMENT")
	public int getMaxPayment() {
		return maxPayment;
	}
	public void setMaxPayment(int maxPayment) {
		this.maxPayment = maxPayment;
	}
	
	@Column(name="REPAYMENT_DATE")
	public Date getRepaymentDate() {
		return repaymentDate;
	}
	public void setRepaymentDate(Date repaymentDate) {
		this.repaymentDate = repaymentDate;
	}
	
	@Column(name="NOTES")
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	@Column(name="PROMISE_COUNT")
	public int getPromiseCount() {
		return promiseCount;
	}
	public void setPromiseCount(int promiseCount) {
		this.promiseCount = promiseCount;
	}
	
	@Column(name="PROMISE_AVAILABLE")
	public int getPromiseAvailable() {
		return promiseAvailable;
	}
	public void setPromiseAvailable(int promiseAvailable) {
		this.promiseAvailable = promiseAvailable;
	}
	
	@Column(name="PROMISE_RANGE")
	public int getPromiseRange() {
		return promiseRange;
	}
	public void setPromiseRange(int promiseRange) {
		this.promiseRange = promiseRange;
	}
	
	@Column(name="PHONE_AREA")
	public String getPhoneArea() {
		return phoneArea;
	}
	public void setPhoneArea(String phoneArea) {
		this.phoneArea = phoneArea;
	}
	
	@Column(name="PHONE_NUMBER")
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	@Column(name="MOBILE_PREFIX")
	public String getMobilePrefix() {
		return mobilePrefix;
	}
	public void setMobilePrefix(String mobilePrefix) {
		this.mobilePrefix = mobilePrefix;
	}
	
	@Column(name="MOBILE_NO")
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	
	@Column(name="NAMA_BARANG")
	public String getNamaBarang() {
		return namaBarang;
	}
	public void setNamaBarang(String namaBarang) {
		this.namaBarang = namaBarang;
	}
	
	@Column(name="PHOTO")
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	
	
		
}
