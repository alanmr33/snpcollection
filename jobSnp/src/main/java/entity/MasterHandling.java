package entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MASTER_HANDLING")
public class MasterHandling {
	
	private int batchHandlingNo;
	private String handlingCode;
	private String promiseFlag;
	private String deskripsi;
	private String aktifStatus;
	private String createUser;
	private Date createDate;
	private String modifyUser;
	private Date modifyDate;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="BATCH_HANDLING_NO")
	public int getBatchHandlingNo() {
		return batchHandlingNo;
	}
	public void setBatchHandlingNo(int batchHandlingNo) {
		this.batchHandlingNo = batchHandlingNo;
	}
	
	@Column(name="HANDLING_CODE")
	public String getHandlingCode() {
		return handlingCode;
	}
	public void setHandlingCode(String handlingCode) {
		this.handlingCode = handlingCode;
	}
	
	@Column(name="PROMISE_FLAG")
	public String getPromiseFlag() {
		return promiseFlag;
	}
	public void setPromiseFlag(String promiseFlag) {
		this.promiseFlag = promiseFlag;
	}
	
	@Column(name="DESKRIPSI_HANDLING")
	public String getDeskripsi() {
		return deskripsi;
	}
	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}
	
	@Column(name="AKTIF_STATUS")
	public String getAktifStatus() {
		return aktifStatus;
	}
	public void setAktifStatus(String aktifStatus) {
		this.aktifStatus = aktifStatus;
	}
	
	@Column(name="CREATE_USER")
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	
	@Column(name="CREATE_DATE")
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	@Column(name="MODIFY_USER")
	public String getModifyUser() {
		return modifyUser;
	}
	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}
	
	@Column(name="MODIFY_DATE")
	public Date getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	
}
