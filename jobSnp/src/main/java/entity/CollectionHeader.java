package entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name="COLLECTION_HEADER")
@IdClass(CollectionHeaderPK.class)
public class CollectionHeader {
	private int orderId;
	private String lpkNo;
	private Date lpkDate;
	private String collectorCode;
	private String status;
	private String reassign;
	private Date reassignDate;
	private String reassignBy;
	private String previousCollector;
	private String reassignNotes;
	
	@Id
	@Column(name="USER_ID")
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	
	@Column(name="LPK_NO")
	public String getLpkNo() {
		return lpkNo;
	}
	public void setLpkNo(String lpkNo) {
		this.lpkNo = lpkNo;
	}
	
	@Column(name="LPK_DATE")
	public Date getLpkDate() {
		return lpkDate;
	}
	public void setLpkDate(Date lpkDate) {
		this.lpkDate = lpkDate;
	}
	
	@Column(name="COLLECTOR_CODE")
	public String getCollectorCode() {
		return collectorCode;
	}
	public void setCollectorCode(String collectorCode) {
		this.collectorCode = collectorCode;
	}
	
	@Column(name="STATUS")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="REASSIGN")
	public String getReassign() {
		return reassign;
	}
	public void setReassign(String reassign) {
		this.reassign = reassign;
	}
	
	@Column(name="REASSIGN_DATE")
	public Date getReassignDate() {
		return reassignDate;
	}
	public void setReassignDate(Date reassignDate) {
		this.reassignDate = reassignDate;
	}
	
	@Column(name="REASSIGN_BY")
	public String getReassignBy() {
		return reassignBy;
	}
	public void setReassignBy(String reassignBy) {
		this.reassignBy = reassignBy;
	}
	
	@Column(name="PREVIOUS_COLLECTOR")
	public String getPreviousCollector() {
		return previousCollector;
	}
	public void setPreviousCollector(String previousCollector) {
		this.previousCollector = previousCollector;
	}
	
	@Column(name="REASSIGN_NOTES")
	public String getReassignNotes() {
		return reassignNotes;
	}
	public void setReassignNotes(String reassignNotes) {
		this.reassignNotes = reassignNotes;
	}
		
	
}
