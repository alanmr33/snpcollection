package entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name="FORM_ROWS")
@IdClass(FormRowsPk.class)
public class FormRows {
	private String rowId;
	private String rowOrder;
	private String rowVisibility;
	private String formId;
	
	
	@Id
	@Column(name="row_id")
	public String getRowId() {
		return rowId;
	}
	public void setRowId(String rowId) {
		this.rowId = rowId;
	}
	
	@Column(name="row_order")
	public String getRowOrder() {
		return rowOrder;
	}
	public void setRowOrder(String rowOrder) {
		this.rowOrder = rowOrder;
	}
	
	@Column(name="row_visibility")
	public String getRowVisibility() {
		return rowVisibility;
	}
	public void setRowVisibility(String rowVisibility) {
		this.rowVisibility = rowVisibility;
	}
	
	@Column(name="form_id")
	public String getFormId() {
		return formId;
	}
	public void setFormId(String formId) {
		this.formId = formId;
	}
	
	
	
				
}
