package utils;

import org.apache.commons.codec.digest.DigestUtils;
import sun.security.provider.MD5;

import java.sql.Date;
import java.util.Calendar;
import java.util.UUID;

/**
 * Created by Indocyber on 27/03/2018.
 */
public abstract class TokenUtils {
    public static String generateToken(){
        UUID uuid = UUID.randomUUID();
        String tokenTemp=uuid.toString()+ Calendar.getInstance().getTime().getTime();
        return DigestUtils.md5Hex(tokenTemp);
    }
}
