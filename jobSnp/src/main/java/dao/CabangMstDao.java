package dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import entity.CabangMst;
import entity.CabangMstPK;


public interface CabangMstDao extends JpaRepository<CabangMst, CabangMstPK>{
	@Query(value="SELECT " 
			+ "CASE WHEN (cm.KODE_CABANG IS NULL OR cm.KODE_CABANG = '') THEN '--' ELSE cast(cm.KODE_CABANG as varchar) END AS KODE_CABANG, "
			+ "CASE WHEN (cm.NAMA_CABANG IS NULL OR cm.NAMA_CABANG = '') THEN '--' ELSE cast(cm.NAMA_CABANG as varchar) END AS NAMA_CABANG, "
			+ "CASE WHEN (cm.KODE_REGION IS NULL OR cm.KODE_REGION = '') THEN '--' ELSE cast(cm.KODE_REGION as varchar) END AS KODE_REGION, "
			+ "CASE WHEN (rm.NAMA_REGION IS NULL OR rm.NAMA_REGION = '') THEN '--' ELSE cast(rm.NAMA_REGION as varchar) END AS RegionalName, "
			+ "CASE WHEN (cm.ALAMAT IS NULL OR cm.ALAMAT = '') THEN '--' ELSE cast(cm.ALAMAT as varchar) END AS ALAMAT, "
			+ "CASE WHEN cm.AKTIF_STATUS  = 1 THEN 'ENABLED' ELSE 'DISABLED' END AS statusAktif "
			+ "FROM CABANG_MST as cm "
			+ "INNER JOIN REGION_MST rm on rm.KODE_REGION = cm.KODE_REGION "
			+ "WHERE KODE_CABANG LIKE %:keySearch% or NAMA_CABANG LIKE %:keySearch% or NAMA_REGION LIKE %:keySearch% OR ALAMAT LIKE %:keySearch% ", nativeQuery=true)
	public List<Object[]> findCabangAllBySearch(@Param("keySearch")String keySearch);
	
	@Query(value="SELECT " 
			+ "CASE WHEN (cm.KODE_CABANG IS NULL OR cm.KODE_CABANG = '') THEN '--' ELSE cast(cm.KODE_CABANG as varchar) END AS KODE_CABANG, "
			+ "CASE WHEN (cm.NAMA_CABANG IS NULL OR cm.NAMA_CABANG = '') THEN '--' ELSE cast(cm.NAMA_CABANG as varchar) END AS NAMA_CABANG, "
			+ "CASE WHEN (cm.KODE_REGION IS NULL OR cm.KODE_REGION = '') THEN '--' ELSE cast(cm.KODE_REGION as varchar) END AS KODE_REGION, "
			+ "CASE WHEN (rm.NAMA_REGION IS NULL OR rm.NAMA_REGION = '') THEN '--' ELSE cast(rm.NAMA_REGION as varchar) END AS RegionalName, "
			+ "CASE WHEN (cm.ALAMAT IS NULL OR cm.ALAMAT = '') THEN '--' ELSE cast(cm.ALAMAT as varchar) END AS ALAMAT, "
			+ "CASE WHEN cm.AKTIF_STATUS  = 1 THEN 'ENABLED' ELSE 'DISABLED' END AS statusAktif "
			+ "FROM CABANG_MST as cm "
			+ "INNER JOIN REGION_MST rm on rm.KODE_REGION = cm.KODE_REGION "
			+ "WHERE cm.AKTIF_STATUS != 0 and KODE_CABANG LIKE %:keySearch% or NAMA_CABANG LIKE %:keySearch% or NAMA_REGION LIKE %:keySearch% OR ALAMAT LIKE %:keySearch% ", nativeQuery=true)
	public List<Object[]> findCabangAllActiveBySearch(@Param("keySearch")String keySearch);
	
	@Query(value="SELECT " 
			+ "CASE WHEN (cm.KODE_CABANG IS NULL OR cm.KODE_CABANG = '') THEN '--' ELSE cast(cm.KODE_CABANG as varchar) END AS KODE_CABANG, "
			+ "CASE WHEN (cm.NAMA_CABANG IS NULL OR cm.NAMA_CABANG = '') THEN '--' ELSE cast(cm.NAMA_CABANG as varchar) END AS NAMA_CABANG, "
			+ "CASE WHEN (cm.KODE_REGION IS NULL OR cm.KODE_REGION = '') THEN '--' ELSE cast(cm.KODE_REGION as varchar) END AS KODE_REGION, "
			+ "CASE WHEN (rm.NAMA_REGION IS NULL OR rm.NAMA_REGION = '') THEN '--' ELSE cast(rm.NAMA_REGION as varchar) END AS RegionalName, "
			+ "CASE WHEN (cm.ALAMAT IS NULL OR cm.ALAMAT = '') THEN '--' ELSE cast(cm.ALAMAT as varchar) END AS ALAMAT, "
			+ "CASE WHEN cm.AKTIF_STATUS  = 1 THEN 'ENABLED' ELSE 'DISABLED' END AS statusAktif "
			+ "FROM CABANG_MST as cm "
			+ "INNER JOIN REGION_MST rm on rm.KODE_REGION = cm.KODE_REGION "
			+ "WHERE cm.KODE_REGION = :regionalId and cm.AKTIF_STATUS != 0 and (KODE_CABANG LIKE %:keySearch% or NAMA_CABANG LIKE %:keySearch% or NAMA_REGION LIKE %:keySearch% OR ALAMAT LIKE %:keySearch%)", nativeQuery=true)
	public List<Object[]> findCabangAllActiveByRegionalSearch(@Param("regionalId")String regionalId,@Param("keySearch")String keySearch);
	
	@Modifying
	@Transactional
	@Query(value="insert "
			+ "into CABANG_MST "
			+ "(KODE_CABANG, KODE_REGION, NAMA_CABANG, ALAMAT, AKTIF_STATUS, CREATE_USER, CREATE_DATE) "
			+ "VALUES(:cabangCode, :regionalCode, :cabangName, :alamat, :aktifStatus, :createUser, getDate())", nativeQuery=true)
	public void saveCabang(@Param("cabangCode") String cabangCode,
										@Param("regionalCode") String regionalCode, 
										@Param("cabangName") String cabangName, 
										@Param("alamat") String alamat, 
										@Param("aktifStatus") int aktifStatus, 
										@Param("createUser") String createUser);
	
	@Modifying
	@Transactional
	@Query(value="update "
			+ "CABANG_MST "
			+ "set "
			+ "KODE_REGION =:regionCode, "
			+ "NAMA_CABANG =:cabangName, "
			+ "ALAMAT =:alamat, "
			+ "AKTIF_STATUS =:aktifStatus, "
			+ "MODIFY_USER =:modifyUser, "
			+ "MODIFY_DATE = getDate() "
			+ "where KODE_CABANG =:cabangCode", nativeQuery=true)
	public void updateCabang(@Param("regionCode") String regionCode,
										@Param("cabangName") String cabangName,
										@Param("alamat") String alamat, 
										@Param("aktifStatus") int aktifStatus,
										@Param("modifyUser") String modifyUser,
										@Param("cabangCode") String cabangCode);
	
	@Modifying
	@Transactional
	@Query(value="update "
			+ "CABANG_MST "
			+ "set "
			+ "AKTIF_STATUS = 0, "
			+ "MODIFY_USER =:modifyUser, "
			+ "MODIFY_DATE = getDate() "
			+ "where KODE_CABANG =:cabangCode", nativeQuery=true)
	public void deleteCabang(@Param("modifyUser") String modifyUser,
										@Param("cabangCode") String cabangCode);

	@Query("SELECT "
			+ "cm "
			+ "FROM CabangMst cm "
		    + "WHERE cm.kodeCabang = :kodeCabang")
	public CabangMst findOneCabang(@Param("kodeCabang")String kodeCabang);
}
	

