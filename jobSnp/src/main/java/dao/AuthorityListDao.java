package dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import entity.AuthorityList;

public interface AuthorityListDao extends JpaRepository<AuthorityList, Serializable>{
	@Modifying
	@Transactional
	@Query(value="SELECT "
			+ "al.AUTHORITY_ID, " 
			+ "al.MENU, "
			+ "Case when (am.AUTHORITY_NAME is null or am.AUTHORITY_NAME = '') then '(blank)' ELSE cast(am.AUTHORITY_NAME as varchar) end as AUTHORITY_NAME, " 
			+ "Case when (me.MENU_NAME is null or me.MENU_NAME = '') then '(blank)' ELSE cast(me.MENU_NAME as varchar) end as MENU_NAME, "
			+ "Case when (me.URL is null or me.URL = '') then '(blank)' ELSE cast(me.URL as varchar) end as URL "
			+ "FROM AUTHORITY_LIST al "
			+ "INNER JOIN AUTHORITY_MST am on am.AUTHORITY_ID = al.AUTHORITY_ID "
			+ "INNER JOIN MENU_MST me on me.MENU = al.MENU AND me.AKTIF_STATUS = 1 "
			+ "WHERE al.AUTHORITY_ID = :authorityId ", nativeQuery=true)
	public List<Object[]> findAuthorityListByAuthorityId(@Param("authorityId")String authorityId);
	
	@Query(value="SELECT "
			+ "AM.AUTHORITY_ID, "
			+ "AM.AUTHORITY_NAME "
			+ "FROM AUTHORITY_MST AM "
			+ "WHERE AM.AUTHORITY_NAME LIKE %:keySearch%", nativeQuery=true)
	public List<Object[]> findAllAuthorityBySearch(@Param("keySearch")String keySearch);
	
	@Modifying
	@Transactional
	@Query(value= "DELETE FROM AUTHORITY_LIST WHERE AUTHORITY_ID = :AUTHORITY_ID", nativeQuery=true)
	public void delAuthorityList(@Param("AUTHORITY_ID")String AUTHORITY_ID);
	
	@Modifying
	@Transactional
	@Query(value= "INSERT INTO AUTHORITY_LIST (AUTHORITY_ID,MENU) VALUES (:AUTHORITY_ID,:MENU)", nativeQuery=true)
	public void saveAuthority(@Param("AUTHORITY_ID")String AUTHORITY_ID,@Param("MENU")String MENU);
	
}
