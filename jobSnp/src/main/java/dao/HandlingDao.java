package dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import entity.Handling;
import entity.HandlingPK;


public interface HandlingDao extends JpaRepository<Handling, HandlingPK>{
	@Modifying
	@Transactional
	@Query(value="INSERT "
			+ "INTO HANDLING "
			+ "(BATCH_HANDLING, CONTRACT_NO, INSTALLMENT_NO, ORDER_ID, KODE_HANDLING,KODE_KONSUMEN,NO_LPK,NOTES,TANGGAL_HANDLING,TANGGAL_JANJI,PROMISE_COUNT,PROMISE_AVAILABLE,PROMISE_RANGE,STATUS,CREATE_USER,CREATE_DATE) "
			+ "VALUES(:BATCH_HANDLING, :CONTRACT_NO, :INSTALLMENT_NO, :ORDER_ID, :KODE_HANDLING, :KODE_KONSUMEN, :NO_LPK, :NOTES, :TANGGAL_HANDLING, :TANGGAL_JANJI, :PROMISE_COUNT, :PROMISE_AVAILABLE, :PROMISE_RANGE, :STATUS, :CREATE_USER, getDate())", nativeQuery=true)
	public void insertHandling(
										@Param("BATCH_HANDLING") int BATCH_HANDLING,
										@Param("CONTRACT_NO") String CONTRACT_NO, 
										@Param("INSTALLMENT_NO") int INSTALLMENT_NO, 
										@Param("ORDER_ID") int ORDER_ID,
										@Param("KODE_HANDLING") String KODE_HANDLING,
										@Param("KODE_KONSUMEN") String KODE_KONSUMEN, 
										@Param("NO_LPK") String NO_LPK, 
										@Param("NOTES") String NOTES,
										@Param("TANGGAL_HANDLING") String TANGGAL_HANDLING,
										@Param("TANGGAL_JANJI") String TANGGAL_JANJI, 
										@Param("PROMISE_COUNT") int PROMISE_COUNT, 
										@Param("PROMISE_AVAILABLE") int PROMISE_AVAILABLE,
										@Param("PROMISE_RANGE") int PROMISE_RANGE,
										@Param("STATUS") String STATUS, 
										@Param("CREATE_USER") String CREATE_USER);
	
	@Modifying
	@Transactional
	@Query(value="INSERT "
			+ "INTO HANDLING "
			+ "(BATCH_HANDLING, CONTRACT_NO, INSTALLMENT_NO, ORDER_ID, KODE_HANDLING,KODE_KONSUMEN,NO_LPK,NOTES,TANGGAL_HANDLING,TANGGAL_JANJI,PROMISE_COUNT,PROMISE_AVAILABLE,PROMISE_RANGE,STATUS,CREATE_USER,CREATE_DATE,DEPOSIT,DEPOSIT_FROM) "
			+ "VALUES(:BATCH_HANDLING, :CONTRACT_NO, :INSTALLMENT_NO, :ORDER_ID, :kodeHandling, :KODE_KONSUMEN, :NO_LPK, :NOTES, :TANGGAL_HANDLING, :TANGGAL_JANJI, :PROMISE_COUNT, :PROMISE_AVAILABLE, :PROMISE_RANGE, :STATUS, :CREATE_USER, getDate(), :deposit, :depositFrom)", nativeQuery=true)
	public void insertHandlingDeposit(
										@Param("BATCH_HANDLING") int BATCH_HANDLING,
										@Param("CONTRACT_NO") String CONTRACT_NO, 
										@Param("INSTALLMENT_NO") int INSTALLMENT_NO, 
										@Param("ORDER_ID") int ORDER_ID,
										@Param("kodeHandling") String kodeHandling,
										@Param("KODE_KONSUMEN") String KODE_KONSUMEN, 
										@Param("NO_LPK") String NO_LPK, 
										@Param("NOTES") String NOTES,
										@Param("TANGGAL_HANDLING") String TANGGAL_HANDLING,
										@Param("TANGGAL_JANJI") String TANGGAL_JANJI, 
										@Param("PROMISE_COUNT") int PROMISE_COUNT, 
										@Param("PROMISE_AVAILABLE") int PROMISE_AVAILABLE,
										@Param("PROMISE_RANGE") int PROMISE_RANGE,
										@Param("STATUS") String STATUS, 
										@Param("CREATE_USER") String CREATE_USER,
										@Param("deposit") int deposit,
										@Param("depositFrom") String depositFrom);
	
	@Modifying
	@Transactional
	@Query(value="INSERT "
			+ "INTO HANDLING "
			+ "(BATCH_HANDLING, CONTRACT_NO, INSTALLMENT_NO, ORDER_ID,KODE_HANDLING, KODE_KONSUMEN,NO_LPK,NOTES,TANGGAL_HANDLING,PROMISE_COUNT,PROMISE_AVAILABLE,PROMISE_RANGE,STATUS,CREATE_USER,CREATE_DATE,DEPOSIT,DEPOSIT_FROM) "
			+ "VALUES(:BATCH_HANDLING, :CONTRACT_NO, :INSTALLMENT_NO, :ORDER_ID, :kodeHandling, :KODE_KONSUMEN, :NO_LPK, :NOTES, :TANGGAL_HANDLING, :PROMISE_COUNT, :PROMISE_AVAILABLE, :PROMISE_RANGE, :STATUS, :CREATE_USER, getDate(), :deposit, :depositFrom)", nativeQuery=true)
	public void insertHandDepoNoTglJanji(
										@Param("BATCH_HANDLING") int BATCH_HANDLING,
										@Param("CONTRACT_NO") String CONTRACT_NO, 
										@Param("INSTALLMENT_NO") int INSTALLMENT_NO, 
										@Param("ORDER_ID") int ORDER_ID,
										@Param("kodeHandling") String kodeHandling,
										@Param("KODE_KONSUMEN") String KODE_KONSUMEN, 
										@Param("NO_LPK") String NO_LPK, 
										@Param("NOTES") String NOTES,
										@Param("TANGGAL_HANDLING") String TANGGAL_HANDLING,
										@Param("PROMISE_COUNT") int PROMISE_COUNT, 
										@Param("PROMISE_AVAILABLE") int PROMISE_AVAILABLE,
										@Param("PROMISE_RANGE") int PROMISE_RANGE,
										@Param("STATUS") String STATUS, 
										@Param("CREATE_USER") String CREATE_USER,
										@Param("deposit") int deposit,
										@Param("depositFrom") String depositFrom);
	
	@Modifying
	@Transactional
	@Query(value="UPDATE "
			+ "HANDLING "
			+ "SET "
			+ "KODE_HANDLING = :KODE_HANDLING, "
			+ "KODE_KONSUMEN = :KODE_KONSUMEN, "
			+ "NO_LPK = :NO_LPK, "
			+ "NOTES = :NOTES, "
			+ "TANGGAL_HANDLING = :TANGGAL_HANDLING, "
			+ "TANGGAL_JANJI = :TANGGAL_JANJI, "
			+ "PROMISE_COUNT = :PROMISE_COUNT, "
			+ "PROMISE_AVAILABLE = :PROMISE_AVAILABLE, "
			+ "PROMISE_RANGE = :PROMISE_RANGE, "
			+ "STATUS = :STATUS, "
			+ "MODIFY_USER = :MODIFY_USER, "
			+ "MODIFY_DATE = getDate() "
			+ "WHERE BATCH_HANDLING = :BATCH_HANDLING AND INSTALLMENT_NO = :INSTALLMENT_NO AND ORDER_ID = :ORDER_ID AND CONTRACT_NO = :CONTRACT_NO ", nativeQuery=true)
	public void updateHandling(@Param("BATCH_HANDLING") int BATCH_HANDLING,
										@Param("CONTRACT_NO") String CONTRACT_NO, 
										@Param("INSTALLMENT_NO") int INSTALLMENT_NO, 
										@Param("ORDER_ID") int ORDER_ID,
										@Param("KODE_HANDLING") String KODE_HANDLING,
										@Param("KODE_KONSUMEN") String KODE_KONSUMEN, 
										@Param("NO_LPK") String NO_LPK, 
										@Param("NOTES") String NOTES,
										@Param("TANGGAL_HANDLING") String TANGGAL_HANDLING,
										@Param("TANGGAL_JANJI") String TANGGAL_JANJI, 
										@Param("PROMISE_COUNT") int PROMISE_COUNT, 
										@Param("PROMISE_AVAILABLE") int PROMISE_AVAILABLE,
										@Param("PROMISE_RANGE") int PROMISE_RANGE,
										@Param("STATUS") String STATUS, 
										@Param("MODIFY_USER") String MODIFY_USER);
	
	

}
