package dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import entity.MasterProvinsi;
import entity.MasterProvinsiPK;

public interface MasterProvinsiDao extends JpaRepository<MasterProvinsi, MasterProvinsiPK>{
	@Modifying
	@Transactional
	@Query(value="SELECT " 
					+ "case when (MP.KODE_PROVINSI is null or MP.KODE_PROVINSI='') then '--' "
						+ "else cast(MP.KODE_PROVINSI as varchar(10)) end as kodeProvinsi, "
					+ "case when (MP.NAMA_PROVINSI is null or MP.NAMA_PROVINSI='') then '--' "
						+ "else cast(MP.NAMA_PROVINSI as varchar(70)) end as namaProvinsi, "
					+ "cast(MP.ACTIVE_STATUS as int) as aktif, "
					+ "case when (MP.CREATE_USER is null or MP.CREATE_USER='') then '--' "
						+ "else cast(MP.CREATE_USER as varchar(10)) end as createuser, "
					+ "case when (MP.CREATE_DATE is null or MP.CREATE_DATE='') then '--' "
						+ "else cast(MP.CREATE_DATE as varchar) end as createDate, "
					+ "case when (MP.MODIFY_USER is null or MP.MODIFY_USER='') then '--' "
						+ "else cast(MP.MODIFY_USER as varchar(10)) end as modifyUser, "
					+ "case when (MP.MODIFY_DATE is null or MP.MODIFY_DATE='') then '--' "
						+ "else cast(MP.MODIFY_DATE as varchar) end as modifyDate "
					+ "FROM MASTER_PROVINSI MP " , nativeQuery=true)
	public List<Object[]> findAllProvinsi();
	
	@Query("SELECT "
			+ "mp "
			+ "FROM MasterProvinsi mp "
		    + "WHERE mp.kodeProvinsi =:kodeProvinsi")
	public MasterProvinsi findOneProvinsi(@Param("kodeProvinsi") String kodeProvinsi);
	
	@Query(value="SELECT " 
					+ "case when (MP.KODE_PROVINSI is null or MP.KODE_PROVINSI='') then '--' "
						+ "else cast(MP.KODE_PROVINSI as varchar(10)) end as kodeProvinsi, "
					+ "case when (MP.NAMA_PROVINSI is null or MP.NAMA_PROVINSI='') then '--' "
						+ "else cast(MP.NAMA_PROVINSI as varchar(70)) end as namaProvinsi, "
					+ "cast(MP.ACTIVE_STATUS as int) as aktif, "
					+ "case when (MP.CREATE_USER is null or MP.CREATE_USER='') then '--' "
						+ "else cast(MP.CREATE_USER as varchar(10)) end as createuser, "
					+ "case when (MP.CREATE_DATE is null or MP.CREATE_DATE='') then '--' "
						+ "else cast(MP.CREATE_DATE as varchar) end as createDate, "
					+ "case when (MP.MODIFY_USER is null or MP.MODIFY_USER='') then '--' "
						+ "else cast(MP.MODIFY_USER as varchar(10)) end as modifyUser, "
					+ "case when (MP.MODIFY_DATE is null or MP.MODIFY_DATE='') then '--' "
						+ "else cast(MP.MODIFY_DATE as varchar) end as modifyDate "
					+ "FROM MASTER_PROVINSI MP "
			+ "where (MP.KODE_PROVINSI LIKE %:search% or "
			+ "MP.NAMA_PROVINSI LIKE %:search%) and "
			+ "MP.ACTIVE_STATUS !=0 " , nativeQuery=true)
	public List<Object[]> searchAllProvinsi(@Param("search") String search);

	@Modifying
	@Transactional
	@Query(value="insert "
			+ "into MASTER_PROVINSI "
			+ "(KODE_PROVINSI, NAMA_PROVINSI, ACTIVE_STATUS, CREATE_USER, CREATE_DATE) "
			+ "VALUES(:provinsiCode, :provinsiName, :aktifStatus, :createUser, getDate())", nativeQuery=true)
	public void InsertProvinsi(@Param("provinsiCode") String provinsiCode,
										@Param("provinsiName") String provinsiName,  
										@Param("aktifStatus") int aktifStatus, 
										@Param("createUser") String createUser);
	
	@Modifying
	@Transactional
	@Query(value="update "
			+ "MASTER_PROVINSI "
			+ "set "
			+ "NAMA_PROVINSI =:provinsiName, "
			+ "MODIFY_USER =:modifyUser, "
			+ "ACTIVE_STATUS =:aktifStatus, "
			+ "MODIFY_DATE = getDate() "
			+ "where KODE_PROVINSI =:provinsiCode", nativeQuery=true)
	public void UpdateProvinsi(@Param("provinsiName") String provinsiName,
										@Param("modifyUser") String modifyUser,
										@Param("aktifStatus") int aktifStatus,
										@Param("provinsiCode") String provinsiCode);
	@Modifying
	@Transactional
	@Query(value="update "
			+ "MASTER_PROVINSI "
			+ "set "
			+ "ACTIVE_STATUS = 0, "
			+ "MODIFY_USER =:modifyUser, "
			+ "MODIFY_DATE = getDate() "
			+ "where KODE_PROVINSI =:provinsiCode", nativeQuery=true)
	public void DeleteProvinsi(@Param("modifyUser") String modifyUser,
										@Param("provinsiCode") String provinsiCode);
	
	
}
