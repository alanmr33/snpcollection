package dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import entity.AuthorityMst;
import entity.CabangMst;

public interface AuthorityMstDao extends JpaRepository<AuthorityMst, Serializable>{
	@Modifying
	@Transactional
	@Query(value="SELECT "
			+ "am.authority_Id, "
			+ "Case when (am.authority_Name is null or am.authority_Name = '') then '(blank)' ELSE cast(am.authority_Name as varchar) end as authorityName, "
			+ "Case when (am.aktif_status is null or am.aktif_status = '') then 0 ELSE cast(am.aktif_status as int) end as aktifStatus "
			+ "FROM authority_mst am "
			+ "WHERE am.authorityId = :authorityId ", nativeQuery=true)
	public List<Object[]> findAuthorityByAuthorityId(@Param("authorityId")int authorityId);
	
	@Query(value="SELECT "
			+ "am.authority_Id, "
			+ "Case when (am.authority_Name is null or am.authority_Name = '') then '(blank)' ELSE cast(am.authority_Name as varchar) end as authorityName, "
			+ "Case when (am.aktif_status is null or am.aktif_status = '') then 0 ELSE cast(am.aktif_status as int) end as aktifStatus "
			+ "FROM authority_mst am "
			+ "WHERE am.authority_Id like %:search% or "
			+ "am.authority_Name like %:search% ", nativeQuery=true)
	public List<Object[]> searchAuthority(@Param("search")String search);
	
	@Query(value="SELECT "
			+ "am.authority_Id, "
			+ "Case when (am.authority_Name is null or am.authority_Name = '') then '(blank)' ELSE cast(am.authority_Name as varchar) end as authorityName, "
			+ "Case when (am.aktif_status is null or am.aktif_status = '') then 0 ELSE cast(am.aktif_status as int) end as aktifStatus "
			+ "FROM authority_mst am "
			+ "WHERE am.authority_Id like %:search% or "
			+ "am.authority_Name like %:search% and am.aktif_status != 0", nativeQuery=true)
	public List<Object[]> searchAllAuthorityActive(@Param("search")String search);
	
	@Modifying
	@Transactional
	@Query(value="INSERT INTO authority_mst (AUTHORITY_NAME, AKTIF_STATUS, CREATE_USER, CREATE_DATE) "
			+ "VALUES  "
			+ "(:authorityName, "
			+ ":aktifStatus, "
			+ ":createUser, "
			+ "getdate()) ", nativeQuery=true)
	public void saveAuthority(@Param("authorityName")String authorityName,
			@Param("aktifStatus")int aktifStatus,
			@Param("createUser")String createUser); 
	
	@Modifying
	@Transactional
	@Query(value="UPDATE authority_mst "
			+ "set "
			+ "AUTHORITY_NAME =:authorityName, "
			+ "AKTIF_STATUS =:aktifStatus, "
			+ "MODIFY_USER =:modifyUser, "
			+ "MODIFY_DATE =getDate() "
			+ "where AUTHORITY_ID =:authorityID", nativeQuery=true)
	public void updateAuthority(@Param("authorityName")String authorityName,
			@Param("aktifStatus")int aktifStatus,
			@Param("modifyUser")String modifyUser,
			@Param("authorityID")int authorityID); 
	
	@Modifying
	@Transactional
	@Query(value="UPDATE authority_mst "
			+ "set "
			+ "AKTIF_STATUS = 0 , "
			+ "MODIFY_USER = :modifyUser, "
			+ "MODIFY_DATE = getDate() "
			+ "where AUTHORITY_ID = :authorityID ", nativeQuery=true)
	public void deleteAuthority(@Param("modifyUser")String modifyUser,
			@Param("authorityID")int authorityID); 
	
	@Query("SELECT "
			+ "am "
			+ "FROM AuthorityMst am "
		    + "WHERE am.authorityId = :authorityId")
	public AuthorityMst findOneAuthority(@Param("authorityId")int authorityId);
}
