package dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import entity.CollectionHeader;
import entity.CollectionHeaderPK;


public interface CollectionHeaderDao extends JpaRepository<CollectionHeader, CollectionHeaderPK>{
	@Modifying
	@Transactional
	@Query(value="INSERT "
			+ "INTO COLLECTION_HEADER "
			+ "(ORDER_ID, LPK_NO, LPK_DATE, COLLECTOR_CODE, STATUS) "
			+ "VALUES(:orderId, :lpkNo, :lpkDate, :collectorCode, :status)", nativeQuery=true)
	public void insertCollectionHeaderInUploadManual(@Param("orderId") String orderId,
										@Param("lpkNo") String lpkNo, 
										@Param("lpkDate") String lpkDate, 
										@Param("collectorCode") String collectorCode, 
										@Param("status") String status);
	
	@Modifying
	@Transactional
	@Query(value="UPDATE COLLECTION_HEADER "
			+ "SET STATUS = 'CMP' "
			+ "WHERE ORDER_ID = :orderId ", nativeQuery=true)
	public void updateCollectionHeaderStatusToCmp(@Param("orderId") int orderId);
	
	@Modifying
	@Transactional
	@Query(value="EXEC SP_CHECK_TASK :orderId ", nativeQuery=true)
	public void execSP_CHECK_TASK(@Param("orderId") int orderId);
}
