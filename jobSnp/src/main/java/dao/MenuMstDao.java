package dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import entity.MenuMst;
import entity.MenuMstPK;

public interface MenuMstDao extends JpaRepository<MenuMst, MenuMstPK>{
	@Query(value="SELECT  "
			+ "MENU, "
			+ "CASE WHEN (mm.MENU_NAME IS NULL OR mm.MENU_NAME = '') THEN '--' ELSE cast(mm.MENU_NAME as varchar) END AS MENU_NAME, "
			+ "CASE WHEN (mm.URL IS NULL OR mm.URL = '') THEN '--' ELSE cast(mm.URL as varchar) END AS URL, "
			+ "CASE WHEN (mm.MENU_SORT IS NULL OR mm.MENU_SORT = '') THEN '0' ELSE cast(mm.MENU_SORT as varchar) END AS MENU_SORT, "
			+ "CASE WHEN (mm.DISPLAY_FLG IS NULL OR mm.DISPLAY_FLG = '') THEN '--' ELSE cast(mm.DISPLAY_FLG as varchar) END AS DISPLAY_FLG, "
			+ "CASE WHEN (mm.CREATE_USER IS NULL OR mm.CREATE_USER = '') THEN '--' ELSE cast(mm.CREATE_USER as varchar) END AS CREATE_USER, "
			+ "CASE WHEN (mm.CREATE_DATE IS NULL OR mm.CREATE_DATE = '') THEN '--' ELSE cast(mm.CREATE_DATE as varchar) END AS CREATE_DATE, "
			+ "CASE WHEN (mm.MODIFY_USER IS NULL OR mm.MODIFY_USER = '') THEN '--' ELSE cast(mm.MODIFY_USER as varchar) END AS MODIFY_USER, "
			+ "CASE WHEN (mm.MODIFY_DATE IS NULL OR mm.MODIFY_DATE = '') THEN '--' ELSE cast(mm.MODIFY_DATE as varchar) END AS MODIFY_DATE, "
			+ "CASE WHEN mm.AKTIF_STATUS  = 1 THEN 'ENABLED' ELSE 'DISABLED' END AS aktifStatus "
			+ "FROM MENU_MST mm "
			+ "WHERE mm.MENU_NAME LIKE %:keySearch% ", nativeQuery=true)
	public List<Object[]> findMenuAllBySearch(@Param("keySearch")String keySearch);
	
	@Modifying
	@Transactional
	@Query(value="update "
			+ "MENU_MST "
			+ "set "
			+ "AKTIF_STATUS = 0, "
			+ "MODIFY_USER =:modifyUser, "
			+ "MODIFY_DATE = getDate() "
			+ "where MENU =:menu", nativeQuery=true)
	public void DeleteMenu(@Param("modifyUser") String modifyUser,
								@Param("menu") int menu);
	
	@Modifying
	@Transactional
	@Query(value="update "
			+ "MENU_MST "
			+ "set "
			+ "AKTIF_STATUS = 1, "
			+ "MODIFY_USER =:modifyUser, "
			+ "MODIFY_DATE = getDate() "
			+ "where MENU =:menu", nativeQuery=true)
	public void aktifkanMenu(@Param("modifyUser") String modifyUser,
								@Param("menu") int menu);
	
}
