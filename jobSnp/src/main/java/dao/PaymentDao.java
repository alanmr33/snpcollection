package dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import entity.CollectionHeader;
import entity.CollectionHeaderPK;
import entity.Payment;
import entity.PaymentPK;


public interface PaymentDao extends JpaRepository<Payment, PaymentPK>{
	@Modifying
	@Transactional
	@Query(value="INSERT "
			+ "INTO PAYMENT "
			+ "(INSTALLMENT_NO, ORDER_ID, CUSTOMER_CODE, CONTRACT_NO, CODE_COLLECTOR,TOTAL_BAYAR,NO_KWITANSI,NO_LPK,TANGGAL_TRANSAKSI,TANGGAL_BAYAR,STATUS,CREATE_USER,CREATED_DATE) "
			+ "VALUES(:INSTALLMENT_NO, :ORDER_ID, :CUSTOMER_CODE, :CONTRACT_NO, :CODE_COLLECTOR,:TOTAL_BAYAR,:NO_KWITANSI, :NO_LPK, :TANGGAL_TRANSAKSI,:TANGGAL_BAYAR,:STATUS,:CREATE_USER,getDate())", nativeQuery=true)
	public void insertPayment(
										@Param("INSTALLMENT_NO") int INSTALLMENT_NO,
										@Param("ORDER_ID") int ORDER_ID, 
										@Param("CUSTOMER_CODE") String CUSTOMER_CODE, 
										@Param("CONTRACT_NO") String CONTRACT_NO,
										@Param("CODE_COLLECTOR") String CODE_COLLECTOR,
										@Param("TOTAL_BAYAR") int TOTAL_BAYAR, 
										@Param("NO_KWITANSI") String NO_KWITANSI, 
										@Param("NO_LPK") String NO_LPK,
										@Param("TANGGAL_TRANSAKSI") String TANGGAL_TRANSAKSI,
										@Param("TANGGAL_BAYAR") String TANGGAL_BAYAR, 
										@Param("STATUS") String STATUS, 
										@Param("CREATE_USER") String CREATE_USER);
	
	@Modifying
	@Transactional
	@Query(value="UPDATE "
			+ "PAYMENT "
			+ "SET "
			+ "CUSTOMER_CODE = :CUSTOMER_CODE,"
			+ "CODE_COLLECTOR = :CODE_COLLECTOR,"
			+ "TOTAL_BAYAR = :TOTAL_BAYAR,"
			+ "NO_KWITANSI = :NO_KWITANSI,"
			+ "NO_LPK = :NO_LPK,"
			+ "TANGGAL_TRANSAKSI = :TANGGAL_TRANSAKSI,"
			+ "TANGGAL_BAYAR = :TANGGAL_BAYAR,"
			+ "STATUS = :STATUS,"
			+ "MODIFY_USER = :MODIFY_USER,"
			+ "MODIFY_DATE = getDate() "
			+ "WHERE INSTALLMENT_NO = :INSTALLMENT_NO AND ORDER_ID = :ORDER_ID AND CONTRACT_NO = :CONTRACT_NO ", nativeQuery=true)
	public void updatePayment(@Param("INSTALLMENT_NO") int INSTALLMENT_NO, 
										@Param("ORDER_ID") int ORDER_ID,
										@Param("CONTRACT_NO") String CONTRACT_NO, 
										@Param("CUSTOMER_CODE") String KODE_HANDLING,
										@Param("CODE_COLLECTOR") String KODE_KONSUMEN, 
										@Param("TOTAL_BAYAR") String NO_LPK, 
										@Param("NO_KWITANSI") String NOTES,
										@Param("NO_LPK") String TANGGAL_HANDLING,
										@Param("TANGGAL_TRANSAKSI") String TANGGAL_JANJI, 
										@Param("TANGGAL_BAYAR") int PROMISE_COUNT, 
										@Param("STATUS") int PROMISE_AVAILABLE,
										@Param("MODIFY_USER") String MODIFY_USER);
}
