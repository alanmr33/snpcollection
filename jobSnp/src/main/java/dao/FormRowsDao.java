package dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import antlr.debug.Event;
import entity.CabangMst;
import entity.CabangMstPK;
import entity.Events;
import entity.EventsPk;
import entity.FormRows;
import entity.FormRowsPk;
import entity.Forms;
import entity.FormsPk;


public interface FormRowsDao extends JpaRepository<FormRows, FormRowsPk>{
	
	@Query(value="SELECT "
				+ "CASE WHEN (fr.row_id IS NULL OR fr.row_id = '') THEN '--' ELSE cast(fr.row_id as varchar) END AS row_id, "
				+ "CASE WHEN (fr.row_order IS NULL OR fr.row_order = '') THEN '--' ELSE cast(fr.row_order as varchar) END AS row_order, "
				+ "CASE WHEN (fr.row_visibility IS NULL OR fr.row_visibility = '') THEN '--' ELSE cast(fr.row_visibility as varchar) END AS row_visibility, "
				+ "CASE WHEN (fr.form_id IS NULL OR fr.form_id = '') THEN '--' ELSE cast(fr.form_id as varchar) END AS form_id "
				+ "FROM "
				+ "FORM_ROWS fr "
				+ "WHERE "
				+ "fr.row_id LIKE %:keySearch% OR "
				+ "fr.row_order LIKE %:keySearch% OR "
				+ "fr.row_visibility LIKE %:keySearch% OR "
				+ "fr.form_id LIKE %:keySearch% ", nativeQuery=true)
	public List<Object[]> findFormRowsWithSearch(@Param("keySearch")String keySearch);
	
	@Query(value="SELECT "
				+ "CASE WHEN (fr.row_id IS NULL OR fr.row_id = '') THEN '--' ELSE cast(fr.row_id as varchar) END AS row_id, "
				+ "CASE WHEN (fr.row_order IS NULL OR fr.row_order = '') THEN '--' ELSE cast(fr.row_order as varchar) END AS row_order, "
				+ "CASE WHEN (fr.row_visibility IS NULL OR fr.row_visibility = '') THEN '--' ELSE cast(fr.row_visibility as varchar) END AS row_visibility, "
				+ "CASE WHEN (fr.form_id IS NULL OR fr.form_id = '') THEN '--' ELSE cast(fr.form_id as varchar) END AS form_id "
				+ "FROM "
				+ "FORM_ROWS fr "
				+ "WHERE fr.row_id = :keySearch", nativeQuery=true)
	public List<Object[]> findFormRowsById(@Param("keySearch")String keySearch);
	
	
	
	@Modifying
	@Transactional
	@Query(value="INSERT "
			+ "INTO FORM_ROWS "
			+ "(row_id, row_order, row_visibility, form_id) "
			+ "VALUES(:rowId, :rowOrder, :rowVisibility, :formId)", nativeQuery=true)
	public void saveFormRows(@Param("rowId") String rowId,
						  @Param("rowOrder") String rowOrder, 
						  @Param("rowVisibility") String rowVisibility,
						  @Param("formId") String formId
						  );
	
	@Modifying
	@Transactional
	@Query(value="UPDATE "
			+ "FORM_ROWS "
			+ "SET "
			+ "row_order =:rowOrder, "
			+ "row_visibility =:rowVisibility, "
			+ "form_id =:formId, "
			+ "where row_id =:rowId", nativeQuery=true)
	public void updateFormRows(
			  @Param("rowOrder") String rowOrder, 
			  @Param("rowVisibility") String rowVisibility,
			  @Param("formId") String formId,
			  @Param("rowId") String rowId
			  );
	
	@Modifying
	@Transactional
	@Query(value="UPDATE "
			+ "FORM_ROWS "
			+ "SET "
			+ "row_order =:rowOrder "
			+ "where row_id =:rowId", nativeQuery=true)
	public void upDownFormRows(
			  @Param("rowOrder") int rowOrder,
			  @Param("rowId") String rowId
			  );
	
	
	
	@Modifying
	@Transactional
	@Query(value="DELETE FORM_ROWS WHERE row_id = :rowId", nativeQuery=true)
	public void deleteFormRows(@Param("rowId") String rowId);
	
}
	

