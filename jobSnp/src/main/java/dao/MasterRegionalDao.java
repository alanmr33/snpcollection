package dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import entity.MasterRegional;
import entity.MasterRegionalPK;

public interface MasterRegionalDao extends JpaRepository<MasterRegional, MasterRegionalPK>{
	@Modifying
	@Transactional
	@Query(value="SELECT " 
					+ "case when (RM.KODE_REGION is null or RM.KODE_REGION='') then '--' "
						+ "else cast(RM.KODE_REGION as varchar(10)) end as kodeRegion, "
					+ "case when (RM.NAMA_REGION is null or RM.NAMA_REGION='') then '--' "
						+ "else cast(RM.NAMA_REGION as varchar(70)) end as namaRegion, "
					+ "case when (RM.NOTES is null or RM.NOTES='') then '--' "
						+ "else cast(RM.NOTES as varchar(100)) end as notes, "
					+ "cast(RM.AKTIF_STATUS as int) as aktif, "
					+ "case when (RM.CREATE_USER is null or RM.CREATE_USER='') then '--' "
						+ "else cast(RM.CREATE_USER as varchar(10)) end as createuser, "
					+ "case when (RM.CREATE_DATE is null or RM.CREATE_DATE='') then '--' "
						+ "else cast(RM.CREATE_DATE as varchar) end as createDate, "
					+ "case when (RM.MODIFY_USER is null or RM.MODIFY_USER='') then '--' "
						+ "else cast(RM.MODIFY_USER as varchar(10)) end as modifyUser, "
					+ "case when (RM.MODIFY_DATE is null or RM.MODIFY_DATE='') then '--' "
						+ "else cast(RM.MODIFY_DATE as varchar) end as modifyDate "
					+ "FROM REGION_MST RM " , nativeQuery=true)
	public List<Object[]> findAllRegional();
	
	@Query(value="SELECT " 
			+ "case when (RM.KODE_REGION is null or RM.KODE_REGION='') then '--' "
				+ "else cast(RM.KODE_REGION as varchar(10)) end as kodeRegion, "
			+ "case when (RM.NAMA_REGION is null or RM.NAMA_REGION='') then '--' "
				+ "else cast(RM.NAMA_REGION as varchar(70)) end as namaRegion, "
			+ "case when (RM.NOTES is null or RM.NOTES='') then '--' "
				+ "else cast(RM.NOTES as varchar(100)) end as notes, "
			+ "cast(RM.AKTIF_STATUS as int) as aktif, "
			+ "case when (RM.CREATE_USER is null or RM.CREATE_USER='') then '--' "
				+ "else cast(RM.CREATE_USER as varchar(10)) end as createuser, "
			+ "case when (RM.CREATE_DATE is null or RM.CREATE_DATE='') then '--' "
				+ "else cast(RM.CREATE_DATE as varchar) end as createDate, "
			+ "case when (RM.MODIFY_USER is null or RM.MODIFY_USER='') then '--' "
				+ "else cast(RM.MODIFY_USER as varchar(10)) end as modifyUser, "
			+ "case when (RM.MODIFY_DATE is null or RM.MODIFY_DATE='') then '--' "
				+ "else cast(RM.MODIFY_DATE as varchar) end as modifyDate "
			+ "FROM REGION_MST RM "
			+ "where (RM.KODE_REGION LIKE %:search% or "
			+ "RM.NAMA_REGION LIKE %:search%) and "
			+ "RM.AKTIF_STATUS !=0 " , nativeQuery=true)
	public List<Object[]> searchAllRegional(@Param("search") String search);
	
	@Query(value="SELECT " 
			+ "case when (RM.KODE_REGION is null or RM.KODE_REGION='') then '--' "
				+ "else cast(RM.KODE_REGION as varchar(10)) end as kodeRegion, "
			+ "case when (RM.NAMA_REGION is null or RM.NAMA_REGION='') then '--' "
				+ "else cast(RM.NAMA_REGION as varchar(70)) end as namaRegion, "
			+ "case when (RM.NOTES is null or RM.NOTES='') then '--' "
				+ "else cast(RM.NOTES as varchar(100)) end as notes, "
			+ "cast(RM.AKTIF_STATUS as int) as aktif, "
			+ "case when (RM.CREATE_USER is null or RM.CREATE_USER='') then '--' "
				+ "else cast(RM.CREATE_USER as varchar(10)) end as createuser, "
			+ "case when (RM.CREATE_DATE is null or RM.CREATE_DATE='') then '--' "
				+ "else cast(RM.CREATE_DATE as varchar) end as createDate, "
			+ "case when (RM.MODIFY_USER is null or RM.MODIFY_USER='') then '--' "
				+ "else cast(RM.MODIFY_USER as varchar(10)) end as modifyUser, "
			+ "case when (RM.MODIFY_DATE is null or RM.MODIFY_DATE='') then '--' "
				+ "else cast(RM.MODIFY_DATE as varchar) end as modifyDate "
			+ "FROM REGION_MST RM "
			+ "where (RM.KODE_REGION LIKE %:search% or "
			+ "RM.NAMA_REGION LIKE %:search%)" , nativeQuery=true)
	public List<Object[]> searchAllRegional2(@Param("search") String search);
	
	@Query(value="SELECT " 
			+ "case when (RM.KODE_REGION is null or RM.KODE_REGION='') then '--' "
				+ "else cast(RM.KODE_REGION as varchar(10)) end as kodeRegion, "
			+ "case when (RM.NAMA_REGION is null or RM.NAMA_REGION='') then '--' "
				+ "else cast(RM.NAMA_REGION as varchar(70)) end as namaRegion, "
			+ "case when (RM.NOTES is null or RM.NOTES='') then '--' "
				+ "else cast(RM.NOTES as varchar(100)) end as notes, "
			+ "cast(RM.AKTIF_STATUS as int) as aktif, "
			+ "case when (RM.CREATE_USER is null or RM.CREATE_USER='') then '--' "
				+ "else cast(RM.CREATE_USER as varchar(10)) end as createuser, "
			+ "case when (RM.CREATE_DATE is null or RM.CREATE_DATE='') then '--' "
				+ "else cast(RM.CREATE_DATE as varchar) end as createDate, "
			+ "case when (RM.MODIFY_USER is null or RM.MODIFY_USER='') then '--' "
				+ "else cast(RM.MODIFY_USER as varchar(10)) end as modifyUser, "
			+ "case when (RM.MODIFY_DATE is null or RM.MODIFY_DATE='') then '--' "
				+ "else cast(RM.MODIFY_DATE as varchar) end as modifyDate "
			+ "FROM REGION_MST RM "
			+ "where RM.KODE_REGION = :regionalId " , nativeQuery=true)
	public List<Object[]> searchRegionalByRegionalId(@Param("regionalId") String regionalId);

	@Modifying
	@Transactional
	@Query(value="insert "
			+ "into REGION_MST "
			+ "(KODE_REGION, NAMA_REGION, NOTES, AKTIF_STATUS, CREATE_USER, CREATE_DATE) "
			+ "VALUES(:regionalCode, :regionalName, :notes, :aktifStatus, :createUser, getDate())", nativeQuery=true)
	public void InsertRegional(@Param("regionalCode") String regionalCode,
										@Param("regionalName") String regionalName, 
										@Param("notes") String notes, 
										@Param("aktifStatus") int aktifStatus, 
										@Param("createUser") String createUser);
	
	@Modifying
	@Transactional
	@Query(value="update "
			+ "REGION_MST "
			+ "set "
			+ "NAMA_REGION =:regionName, "
			+ "NOTES =:notes, "
			+ "AKTIF_STATUS = :aktifStatus, "
			+ "MODIFY_USER =:modifyUser, "
			+ "MODIFY_DATE = getDate() "
			+ "where KODE_REGION =:regionCode", nativeQuery=true)
	public void UpdateRegional(@Param("regionName") String regionName,
										@Param("notes") String notes, 
										@Param("aktifStatus") String aktifStatus, 
										@Param("modifyUser") String modifyUser,
										@Param("regionCode") String regionCode);
	@Modifying
	@Transactional
	@Query(value="update "
			+ "REGION_MST "
			+ "set "
			+ "AKTIF_STATUS = 0, "
			+ "MODIFY_USER =:modifyUser, "
			+ "MODIFY_DATE = getDate() "
			+ "where KODE_REGION =:regionCode", nativeQuery=true)
	public void DeleteRegional(@Param("modifyUser") String modifyUser,
										@Param("regionCode") String regionCode);
	
}
