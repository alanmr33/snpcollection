package dao;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import entity.AuthorityMst;

public interface ManualAssignDao  extends JpaRepository<AuthorityMst, Serializable>{
	@Query(value="select "
			+ "cd.INSTALLMENT_NO, "
			+ "case when (cd.CONTRACT_NO is null or cd.CONTRACT_NO='') then '--' "
				+ "else cast(cd.CONTRACT_NO as varchar) end as contractNo, "
			+ "cd.ORDER_ID, "
			+ "case when (cd.LPK_NO is null or cd.LPK_NO='') then '--' "
				+ "else cast(cd.LPK_NO as varchar) end as lpkNo, "
			+ "case when (cd.CUSTOMER_NAME is null or cd.CUSTOMER_NAME='') then '--' "
				+ "else cast(cd.CUSTOMER_NAME as varchar) end as customerName, "
			+ "case when (cd.ADDRESS is null or cd.ADDRESS='') then '--' "
				+ "else cast(cd.ADDRESS as varchar) end as address, "
			+ "case when (cd.PHONE_NUMBER is null or cd.PHONE_NUMBER='') then '--' "
				+ "else cast(cd.PHONE_NUMBER as varchar) end as phoneNumber, "
			+ "case when (cd.NOTES is null or cd.NOTES='') then '--' "
				+ "else cast(cd.NOTES as varchar) end as notes, "
			+ "case when (cd.DUE_DATE is null or cd.DUE_DATE='') then '--' "
				+ "else cast(cd.DUE_DATE as varchar) end as dueDate "
			+ "from COLLECTION_DETAIL cd ", nativeQuery=true)
	public List<Object[]> findManualAssign();
	
	@Query(value="select "
			+ "cd.INSTALLMENT_NO, "
			+ "case when (cd.CONTRACT_NO is null or cd.CONTRACT_NO='') then '--' "
				+ "else cast(cd.CONTRACT_NO as varchar) end as contractNo, "
			+ "cd.ORDER_ID, "
			+ "case when (cd.LPK_NO is null or cd.LPK_NO='') then '--' "
				+ "else cast(cd.LPK_NO as varchar) end as lpkNo, "
			+ "case when (cd.CUSTOMER_NAME is null or cd.CUSTOMER_NAME='') then '--' "
				+ "else cast(cd.CUSTOMER_NAME as varchar) end as customerName, "
			+ "case when (cd.ADDRESS is null or cd.ADDRESS='') then '--' "
				+ "else cast(cd.ADDRESS as varchar) end as address, "
			+ "case when (cd.PHONE_NUMBER is null or cd.PHONE_NUMBER='') then '--' "
				+ "else cast(cd.PHONE_NUMBER as varchar) end as phoneNumber, "
			+ "case when (cd.NOTES is null or cd.NOTES='') then '--' "
				+ "else cast(cd.NOTES as varchar) end as notes, "
			+ "case when (cd.DUE_DATE is null or cd.DUE_DATE='') then '--' "
				+ "else cast(cd.DUE_DATE as varchar) end as dueDate "
			+ "from COLLECTION_DETAIL cd "
			+ "where cd.LPK_NO like %:search% "
			+ "or cd.CUSTOMER_NAME like %:search% ", nativeQuery=true)
	public List<Object[]> findAllManualAssign(@Param("search")String search);
	
	@Modifying
	@Transactional
	@Query(value="insert "
			+ "into REASSIGN_COLLECTOR "
			+ "(SEQ_NO,COLLECTOR_CODE, REASSIGN_COLLECTOR_CODE, PERIOD_FROM, PERIOD_TO, NOTES, ORDER_ID)"
			+ "values"
			+ "(:seqNo, "
			+ ":colCode, "
			+ ":newColCode, "
			+ ":periodFrom, "
			+ ":periodTo, "
			+ ":notes, "
			+ ":orderId)", nativeQuery=true)
	public void insertAssign(@Param("seqNo")int seqNo,
									@Param("colCode")String colCode,
									@Param("newColCode")String newColCode,
									@Param("periodFrom")Date periodFrom,
									@Param("periodTo")Date periodTo,
									@Param("notes")String notes,
									@Param("orderId")int orderId);
	
	@Query(value="SELECT " 
			+ "case when (MAX(rc.SEQ_NO) is null OR MAX(rc.SEQ_NO) = '') then 0 " 
			+ "else cast(MAX(rc.SEQ_NO) as int) end as maxi "
			+ "FROM REASSIGN_COLLECTOR rc WHERE rc.COLLECTOR_CODE = :kodeKolektor", nativeQuery=true)
	public int maxSeqnoBByUserId(@Param("kodeKolektor")String kodeKolektor);
	
	@Modifying
	@Transactional
	@Query(value="update "
			+ "COLLECTION_HEADER "
			+ "set "
			+ "COLLECTOR_CODE=:newCol, "
			+ "REASSIGN=1, "
			+ "STATUS = 'RSN', "
			+ "REASSIGN_DATE=getdate(), "
			+ "REASSIGN_BY=:reassignBy, "
			+ "PREVIOUS_COLLECTOR=:colPrev, "
			+ "REASSIGN_NOTES=:notes "
			+ "where ORDER_ID=:orderId ", nativeQuery=true)
	public void updateAssign(@Param("newCol")String newCol,
									@Param("reassignBy")String reassignBy,
									@Param("colPrev")String colPrev,
									@Param("notes")String notes,
									@Param("orderId")int orderId);
	
	
	
	@Query(value="SELECT distinct "
			+ "cd.INSTALLMENT_NO, "
			+ "case when (cd.CONTRACT_NO is null or cd.CONTRACT_NO='') then '--' "
				+ "else cast(cd.CONTRACT_NO as varchar) end as contractNO, "
			+ "cd.ORDER_ID, "
			+ "case when (cd.LPK_NO is null or cd.LPK_NO='') then '--' "
				+ "else cast(cd.LPK_NO as varchar) end as lpk, "
			+ "case when (cd.CUSTOMER_NAME is null or cd.CUSTOMER_NAME='') then '--' "
				+ "else cast(cd.CUSTOMER_NAME as varchar) end as CUSTOMERNAME, "
			+ "case when (cd.ADDRESS is null or cd.ADDRESS='') then '--' "
				+ "else cast(cd.ADDRESS as varchar) end as ADDRESS, "
			+ "case when (cd.PHONE_NUMBER is null or cd.PHONE_NUMBER='') then '--' "
				+ "else cast(cd.PHONE_NUMBER as varchar) end as PHONENUMBER, "
			+ "case when (cd.NOTES is null or cd.NOTES='') then '--' "
				+ "ELSE CAST(cd.NOTES as varchar) end as notes, "
			+ "case when (cd.DUE_DATE is null or cd.DUE_DATE='') then '--' "
				+ "else cast(cd.DUE_DATE as varchar) end as duedate, "
			+ "case when (up.KODE_KOLEKTOR is null or up.KODE_KOLEKTOR='') then '--' "
				+ "ELSE CAST(up.KODE_KOLEKTOR as varchar) end as kodeKolek, "
			+ "case when (up.NAMA_KARYAWAN is null or up.NAMA_KARYAWAN='') then '--' "
				+ "ELSE CAST(up.NAMA_KARYAWAN as varchar) end as namaKaryawan "
			+ "from COLLECTION_HEADER ch "
			+ "left join COLLECTION_DETAIL cd  on cd.ORDER_ID=ch.ORDER_ID "
			+ "left join USER_PROFILE up on ch.COLLECTOR_CODE=up.KODE_KOLEKTOR "
			+ "left join AKSES_REGION_CABANG arc on arc.USER_ID=up.USER_ID and arc.KODE_REGION IS NULL "
			+ "left join AKSES_REGION_CABANG arc2 on arc2.USER_ID=up.USER_ID and arc2.KODE_CABANG IS NULL "
			+ "where arc2.KODE_REGION=:regionalCode and arc.KODE_CABANG=:cabangCode and "
			+ "(cd.LPK_NO like %:search% or cd.CUSTOMER_NAME like %:search%)", nativeQuery=true)
	public List<Object[]> findAssignRegionalCabang(@Param("regionalCode") String regionalCode,
									@Param("cabangCode")String cabangCode,
									@Param("search")String search);
	
	@Query(value="select distinct "
			+ "cd.INSTALLMENT_NO, "
			+ "case when (cd.CONTRACT_NO is null or cd.CONTRACT_NO='') then '--' "
				+ "else cast(cd.CONTRACT_NO as varchar) end as contractNO, "
			+ "cd.ORDER_ID, "
			+ "case when (cd.LPK_NO is null or cd.LPK_NO='') then '--' "
				+ "else cast(cd.LPK_NO as varchar) end as lpk, "
			+ "case when (cd.CUSTOMER_NAME is null or cd.CUSTOMER_NAME='') then '--' "
				+ "else cast(cd.CUSTOMER_NAME as varchar) end as CUSTOMERNAME, "
			+ "case when (cd.ADDRESS is null or cd.ADDRESS='') then '--' "
				+ "else cast(cd.ADDRESS as varchar) end as ADDRESS, "
			+ "case when (cd.PHONE_NUMBER is null or cd.PHONE_NUMBER='') then '--' "
				+ "else cast(cd.PHONE_NUMBER as varchar) end as PHONENUMBER, "
			+ "case when (cd.NOTES is null or cd.NOTES='') then '--' "
				+ "ELSE CAST(cd.NOTES as varchar) end as notes, "
			+ "case when (cd.DUE_DATE is null or cd.DUE_DATE='') then '--' "
				+ "else cast(cd.DUE_DATE as varchar) end as duedate, "
			+ "case when (up.KODE_KOLEKTOR is null or up.KODE_KOLEKTOR='') then '--' "
				+ "ELSE CAST(up.KODE_KOLEKTOR as varchar) end as kodeKolek, "
			+ "case when (up.NAMA_KARYAWAN is null or up.NAMA_KARYAWAN='') then '--' "
				+ "ELSE CAST(up.NAMA_KARYAWAN as varchar) end as namaKaryawan "
			+ "from COLLECTION_HEADER ch "
			+ "left join COLLECTION_DETAIL cd  on cd.ORDER_ID=ch.ORDER_ID "
			+ "left join USER_PROFILE up on ch.COLLECTOR_CODE=up.KODE_KOLEKTOR "
			+ "left join AKSES_REGION_CABANG arc on arc.USER_ID=up.USER_ID "
			+ "where arc.KODE_CABANG=:cabangCode and "
			+ "(cd.LPK_NO like %:search% or cd.CUSTOMER_NAME like %:search%)", nativeQuery=true)
	public List<Object[]> findAssignCabang(@Param("cabangCode")String cabangCode,
									@Param("search")String search);
	
	@Query(value="select distinct "
			+ "cd.INSTALLMENT_NO, "
			+ "case when (cd.CONTRACT_NO is null or cd.CONTRACT_NO='') then '--' "
				+ "else cast(cd.CONTRACT_NO as varchar) end as contractNO, "
			+ "cd.ORDER_ID, "
			+ "case when (cd.LPK_NO is null or cd.LPK_NO='') then '--' "
				+ "else cast(cd.LPK_NO as varchar) end as lpk, "
			+ "case when (cd.CUSTOMER_NAME is null or cd.CUSTOMER_NAME='') then '--' "
				+ "else cast(cd.CUSTOMER_NAME as varchar) end as CUSTOMERNAME, "
			+ "case when (cd.ADDRESS is null or cd.ADDRESS='') then '--' "
				+ "else cast(cd.ADDRESS as varchar) end as ADDRESS, "
			+ "case when (cd.PHONE_NUMBER is null or cd.PHONE_NUMBER='') then '--' "
				+ "else cast(cd.PHONE_NUMBER as varchar) end as PHONENUMBER, "
			+ "case when (cd.NOTES is null or cd.NOTES='') then '--' "
				+ "ELSE CAST(cd.NOTES as varchar) end as notes, "
			+ "case when (cd.DUE_DATE is null or cd.DUE_DATE='') then '--' "
				+ "else cast(cd.DUE_DATE as varchar) end as duedate "
			+ "from COLLECTION_HEADER ch "
			+ "left join COLLECTION_DETAIL cd on cd.ORDER_ID=ch.ORDER_ID "
			+ "left join USER_PROFILE up on ch.COLLECTOR_CODE=up.KODE_KOLEKTOR "
			+ "left join AKSES_REGION_CABANG arc on arc.USER_ID=up.USER_ID "
			+ "where (arc.KODE_REGION=:regionalCode or arc.KODE_CABANG=:cabangCode) "
			+ "and up.KODE_KOLEKTOR=:kodeKol and (cd.LPK_NO like %:search% "
			+ "or cd.CUSTOMER_NAME like %:search%)", nativeQuery=true)
	public List<Object[]> findAssignRegionalCabangSurveyor(@Param("regionalCode") String regionalCode,
									@Param("cabangCode")String cabangCode,
									@Param("kodeKol")String kodeKol,
									@Param("search")String search);
	
	@Query(value="select distinct "
			+ "cd.INSTALLMENT_NO, "
			+ "case when (cd.CONTRACT_NO is null or cd.CONTRACT_NO='') then '--' "
				+ "else cast(cd.CONTRACT_NO as varchar) end as contractNO, "
			+ "cd.ORDER_ID, "
			+ "case when (cd.LPK_NO is null or cd.LPK_NO='') then '--' "
				+ "else cast(cd.LPK_NO as varchar) end as lpk, "
			+ "case when (cd.CUSTOMER_NAME is null or cd.CUSTOMER_NAME='') then '--' "
				+ "else cast(cd.CUSTOMER_NAME as varchar) end as CUSTOMERNAME, "
			+ "case when (cd.ADDRESS is null or cd.ADDRESS='') then '--' "
				+ "else cast(cd.ADDRESS as varchar) end as ADDRESS, "
			+ "case when (cd.PHONE_NUMBER is null or cd.PHONE_NUMBER='') then '--' "
				+ "else cast(cd.PHONE_NUMBER as varchar) end as PHONENUMBER, "
			+ "case when (cd.NOTES is null or cd.NOTES='') then '--' "
				+ "ELSE CAST(cd.NOTES as varchar) end as notes, "
			+ "case when (cd.DUE_DATE is null or cd.DUE_DATE='') then '--' "
				+ "else cast(cd.DUE_DATE as varchar) end as duedate, "
			+ "case when (up.KODE_KOLEKTOR is null or up.KODE_KOLEKTOR='') then '--' "
				+ "ELSE CAST(up.KODE_KOLEKTOR as varchar) end as kodeKolek, "
			+ "case when (up.NAMA_KARYAWAN is null or up.NAMA_KARYAWAN='') then '--' "
				+ "ELSE CAST(up.NAMA_KARYAWAN as varchar) end as namaKaryawan "
			+ "from COLLECTION_HEADER ch "
			+ "left join COLLECTION_DETAIL cd on cd.ORDER_ID=ch.ORDER_ID "
			+ "left join USER_PROFILE up on ch.COLLECTOR_CODE=up.KODE_KOLEKTOR "
			+ "left join AKSES_REGION_CABANG arc on arc.USER_ID=up.USER_ID and arc.KODE_REGION IS NULL "
			+ "where arc.KODE_CABANG=:cabangCode and ch.COLLECTOR_CODE = :kodeKol and (cd.LPK_NO like %:search% "
			+ "or cd.CUSTOMER_NAME like %:search%)", nativeQuery=true)
	public List<Object[]> findAssignCabangSurveyor(@Param("cabangCode")String cabangCode,
									@Param("kodeKol")String kodeKol,
									@Param("search")String search);
}