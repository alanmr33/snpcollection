package dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import entity.CabangMst;
import entity.CabangMstPK;
import entity.Forms;
import entity.FormsPk;


public interface FormsDao extends JpaRepository<Forms, FormsPk>{
	
	
	@Query(value="SELECT " 
				+ "CASE WHEN (f.form_id IS NULL OR f.form_id = '') THEN '--' ELSE cast(f.form_id as varchar) END AS form_id, "
				+ "CASE WHEN (f.form_name IS NULL OR f.form_name = '') THEN '--' ELSE cast(f.form_name as varchar) END AS form_name, "
				+ "CASE WHEN (f.form_label IS NULL OR f.form_label = '') THEN '--' ELSE cast(f.form_label as varchar) END AS form_label, "
				+ "CASE WHEN (f.form_order IS NULL OR f.form_order = '') THEN '--' ELSE cast(f.form_order as varchar) END AS form_order "
				+ "FROM FORMS f "
				+ "WHERE "
				+ "f.form_id LIKE %:keySearch% OR "
				+ "f.form_name LIKE %:keySearch% OR "
				+ "f.form_label LIKE %:keySearch% OR "
				+ "f.form_order LIKE %:keySearch% ", nativeQuery=true)
	public List<Object[]> findFormsWithSearch(@Param("keySearch")String keySearch);
	
	@Query(value="SELECT " 
			+ "CASE WHEN (f.form_id IS NULL OR f.form_id = '') THEN '--' ELSE cast(f.form_id as varchar) END AS form_id, "
			+ "CASE WHEN (f.form_name IS NULL OR f.form_name = '') THEN '--' ELSE cast(f.form_name as varchar) END AS form_name, "
			+ "CASE WHEN (f.form_label IS NULL OR f.form_label = '') THEN '--' ELSE cast(f.form_label as varchar) END AS form_label, "
			+ "CASE WHEN (f.form_order IS NULL OR f.form_order = '') THEN '--' ELSE cast(f.form_order as varchar) END AS form_order "
			+ "FROM FORMS f "
			+ "WHERE f.form_id = :keySearch", nativeQuery=true)
public List<Object[]> findFormsById(@Param("keySearch")String keySearch);
	
	
	
	@Modifying
	@Transactional
	@Query(value="INSERT "
			+ "INTO FORMS "
			+ "(form_id, form_name, form_label, form_order) "
			+ "VALUES(:formId, :formName, :formLabel, :formOrder)", nativeQuery=true)
	public void saveForms(@Param("formId") String formId,
						  @Param("formName") String formName, 
						  @Param("formLabel") String formLabel,
						  @Param("formOrder") String formOrder
						  );
	
	@Modifying
	@Transactional
	@Query(value="UPDATE "
			+ "FORMS "
			+ "SET "
			+ "form_name =:formName, "
			+ "form_label =:formLabel, "
			+ "form_order =:formOrder "
			+ "where form_id =:formId", nativeQuery=true)
	public void updateForms(@Param("formName") String formName,
							 @Param("formLabel") String formLabel,
							 @Param("formOrder") String formOrder, 
							 @Param("formId") String formId
							 );
	
	@Modifying
	@Transactional
	@Query(value="UPDATE "
			+ "FORMS "
			+ "SET "
			+ "form_order =:formOrder "
			+ "where form_id =:formId", nativeQuery=true)
	public void upDownOrder(@Param("formOrder") int formOrder, 
							 @Param("formId") String formId
							 );
	
	@Modifying
	@Transactional
	@Query(value="DELETE FORMS WHERE form_id = :formId", nativeQuery=true)
	public void deleteForms(@Param("formId") String formId);
	
}
	

