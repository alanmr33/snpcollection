package dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import entity.CabangMst;
import entity.CabangMstPK;


public interface AksesRegionCabangDao extends JpaRepository<CabangMst, CabangMstPK>{
	@Query(value="SELECT " 
				+ "CASE WHEN (arc.USER_ID IS NULL OR arc.USER_ID = '') THEN '--' ELSE cast(arc.USER_ID as varchar) END AS USER_ID, "
				+ "CASE WHEN (arc.KODE_REGION IS NULL OR arc.KODE_REGION = '') THEN '--' ELSE cast(arc.KODE_REGION as varchar) END AS KODE_REGION, "
				+ "CASE WHEN (rm.NAMA_REGION IS NULL OR rm.NAMA_REGION = '') THEN '--' ELSE cast(rm.NAMA_REGION as varchar) END AS NAMA_REGION "
				+ "FROM AKSES_REGION_CABANG arc "
				+ "INNER JOIN REGION_MST rm on rm.KODE_REGION = arc.KODE_REGION AND rm.AKTIF_STATUS = 1 "
				+ "WHERE arc.KODE_CABANG is null AND arc.USER_ID = :userID AND (arc.KODE_REGION LIKE %:keySearch% OR rm.NAMA_REGION LIKE %:keySearch%)", nativeQuery=true)
	public List<Object[]> findAksesRegionByUserId(@Param("userID")String userID,@Param("keySearch")String keySearch);
	
	@Query(value="SELECT "
				+ "CASE WHEN (arc.USER_ID IS NULL OR arc.USER_ID = '') THEN '--' ELSE cast(arc.USER_ID as varchar) END AS USER_ID, "
				+ "CASE WHEN (arc.KODE_CABANG IS NULL OR arc.KODE_CABANG = '') THEN '--' ELSE cast(arc.KODE_CABANG as varchar) END AS KODE_CABANG, "
				+ "CASE WHEN (cm.NAMA_CABANG IS NULL OR cm.NAMA_CABANG = '') THEN '--' ELSE cast(cm.NAMA_CABANG as varchar) END AS NAMA_CABANG "
				+ "FROM AKSES_REGION_CABANG arc "
				+ "INNER JOIN CABANG_MST cm on cm.KODE_CABANG = arc.KODE_CABANG AND cm.AKTIF_STATUS = 1 "
				+ "WHERE arc.KODE_REGION is null AND arc.USER_ID = :userID AND (arc.KODE_REGION LIKE %:keySearch% OR cm.NAMA_CABANG LIKE %:keySearch%)", nativeQuery=true)
	public List<Object[]> findAksesCabangByUserId(@Param("userID")String userID,@Param("keySearch")String keySearch);
	
	@Query(value="SELECT "
			+ "CASE WHEN (arc.USER_ID IS NULL OR arc.USER_ID = '') THEN '--' ELSE cast(arc.USER_ID as varchar) END AS USER_ID, "
			+ "CASE WHEN (arc.KODE_CABANG IS NULL OR arc.KODE_CABANG = '') THEN '--' ELSE cast(arc.KODE_CABANG as varchar) END AS KODE_CABANG, "
			+ "CASE WHEN (cm.NAMA_CABANG IS NULL OR cm.NAMA_CABANG = '') THEN '--' ELSE cast(cm.NAMA_CABANG as varchar) END AS NAMA_CABANG "
			+ "FROM AKSES_REGION_CABANG arc "
			+ "INNER JOIN CABANG_MST cm on cm.KODE_CABANG = arc.KODE_CABANG AND cm.AKTIF_STATUS = 1"
			+ "WHERE arc.KODE_REGION is null AND arc.USER_ID = :userID AND cm.KODE_REGION = :kodeRegion AND (arc.KODE_REGION LIKE %:keySearch% OR cm.NAMA_CABANG LIKE %:keySearch%)", nativeQuery=true)
	public List<Object[]> findAksesCabangByUserIdAndRegion(@Param("userID")String userID,@Param("kodeRegion")String kodeRegion,@Param("keySearch")String keySearch);
	
	@Modifying
	@Transactional
	@Query(value="INSERT "
			+ "INTO AKSES_REGION_CABANG (USER_ID, KODE_REGION) VALUES (:userID, :kodeRegion)", nativeQuery=true)
	public void insertRegion(@Param("userID") String userID,
						   @Param("kodeRegion") String kodeRegion);
	
	@Modifying
	@Transactional
	@Query(value="INSERT "
			+ "INTO AKSES_REGION_CABANG (USER_ID, KODE_CABANG) VALUES (:userID, :kodeCabang)", nativeQuery=true)
	public void insertCabang(@Param("userID") String userID,
						   @Param("kodeCabang") String kodeCabang);

	@Modifying
	@Transactional
	@Query(value="DELETE FROM AKSES_REGION_CABANG WHERE USER_ID = :userID AND KODE_CABANG is null", nativeQuery=true)
	public void deleteRegionByUserId(@Param("userID") String userID);
	
	@Modifying
	@Transactional
	@Query(value="DELETE FROM AKSES_REGION_CABANG WHERE USER_ID = :userID AND KODE_REGION is null", nativeQuery=true)
	public void deleteCabangByUserId(@Param("userID") String userID);
}
	

