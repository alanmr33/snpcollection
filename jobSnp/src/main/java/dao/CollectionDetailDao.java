package dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import entity.CollectionDetail;
import entity.CollectionDetailPK;


public interface CollectionDetailDao extends JpaRepository<CollectionDetail, CollectionDetailPK>{
	
	@Modifying
	@Transactional
	@Query(value="INSERT "
			+ "INTO COLLECTION_DETAIL "
			+ "(ORDER_ID, CUSTOMER_CODE, CONTRACT_NO, INSTALLMENT_NO, LPK_NO, CUSTOMER_NAME, ADDRESS, VILLAGE, DISTRICT, RT, RW, CITY, KODE_PROVINSI, POST_CODE, DUE_DATE, INSTALLMENT_AMT, DEPOSIT_AMT, PENALTY_AMT, COLLECTION_FEE, TOTAL_AMT, RECEIPT_NO, MIN_PAYMENT, MAX_PAYMENT, REPAYMENT_DATE, NOTES, PROMISE_COUNT, PROMISE_AVAILABLE, PROMISE_RANGE, PHONE_AREA, PHONE_NUMBER, MOBILE_PREFIX, MOBILE_NO, NAMA_BARANG, STATUS) "
			+ "VALUES(:ORDER_ID, :CUSTOMER_CODE, :CONTRACT_NO, :INSTALLMENT_NO, :LPK_NO, :CUSTOMER_NAME, :ADDRESS, :VILLAGE, :DISTRICT, :RT, :RW, :CITY, :KODE_PROVINSI, :POST_CODE, :DUE_DATE, :INSTALLMENT_AMT, :DEPOSIT_AMT, :PENALTY_AMT, :COLLECTION_FEE, :TOTAL_AMT, :RECEIPT_NO, :MIN_PAYMENT, :MAX_PAYMENT, :REPAYMENT_DATE, :NOTES, :PROMISE_COUNT, :PROMISE_AVAILABLE, :PROMISE_RANGE, :PHONE_AREA, :PHONE_NUMBER, :MOBILE_PREFIX, :MOBILE_NO, :NAMA_BARANG, 'ASN')", nativeQuery=true)
	public void insertCollectionDetailInUploadManual
										(@Param("ORDER_ID") int ORDER_ID,
										@Param("CUSTOMER_CODE") String CUSTOMER_CODE, 
										@Param("CONTRACT_NO") String CONTRACT_NO, 
										@Param("INSTALLMENT_NO") int INSTALLMENT_NO, 
										@Param("LPK_NO") String LPK_NO,
										@Param("CUSTOMER_NAME") String CUSTOMER_NAME,
										@Param("ADDRESS") String ADDRESS,
										@Param("VILLAGE") String VILLAGE,
										@Param("DISTRICT") String DISTRICT,
										@Param("RT") String RT,
										@Param("RW") String RW,
										@Param("CITY") String CITY,
										@Param("KODE_PROVINSI") String KODE_PROVINSI,
										@Param("POST_CODE") String POST_CODE,
										@Param("DUE_DATE") String DUE_DATE,
										@Param("INSTALLMENT_AMT") int INSTALLMENT_AMT,
										@Param("DEPOSIT_AMT") int DEPOSIT_AMT,
										@Param("PENALTY_AMT") int PENALTY_AMT,
										@Param("COLLECTION_FEE") int COLLECTION_FEE,
										@Param("TOTAL_AMT") int TOTAL_AMT,
										@Param("RECEIPT_NO") String RECEIPT_NO,
										@Param("MIN_PAYMENT") int MIN_PAYMENT,
										@Param("MAX_PAYMENT") int MAX_PAYMENT,
										@Param("REPAYMENT_DATE") String REPAYMENT_DATE,
										@Param("NOTES") String NOTES,
										@Param("PROMISE_COUNT") int PROMISE_COUNT,
										@Param("PROMISE_AVAILABLE") int PROMISE_AVAILABLE,
										@Param("PROMISE_RANGE") int PROMISE_RANGE,
										@Param("PHONE_AREA") String PHONE_AREA,
										@Param("PHONE_NUMBER") String PHONE_NUMBER,
										@Param("MOBILE_PREFIX") String MOBILE_PREFIX,
										@Param("MOBILE_NO") String MOBILE_NO,
										@Param("NAMA_BARANG") String NAMA_BARANG
										);
	
	@Modifying
	@Transactional
	@Query(value="update "
			+ "COLLECTION_HEADER "
			+ "set "
			+ "STATUS = :status "
			+ "where ORDER_ID =:ORDER_ID", nativeQuery=true)
	public void updateCollectionHeadetToCMP(@Param("status") String status,@Param("ORDER_ID") int ORDER_ID);
	
	@Modifying
	@Transactional
	@Query(value="update "
			+ "COLLECTION_DETAIL "
			+ "set "
			+ "STATUS = 'CMP' "
			+ "where ORDER_ID = :orderId AND CONTRACT_NO = :contractNo AND INSTALLMENT_NO = :instalmentNo", nativeQuery=true)
	public void updateStatusCollectionDetailToCMP(@Param("orderId") int orderId, @Param("contractNo") String contractNo,@Param("instalmentNo") int instalmentNo);
	
	@Modifying
	@Transactional
	@Query(value="update "
			+ "COLLECTION_DETAIL "
			+ "set "
			+ "STATUS = 'CLS', "
			+ "NOTES = :notes "
			+ "where ORDER_ID = :orderId AND CONTRACT_NO = :contractNo AND INSTALLMENT_NO = :instalmentNo", nativeQuery=true)
	public void updateStatusCollectionDetailToCLS(@Param("orderId") int orderId, @Param("contractNo") String contractNo,@Param("instalmentNo") int instalmentNo, @Param("notes") String notes);
	
	
	@Query(value="SELECT COUNT(cd.ORDER_ID) FROM COLLECTION_DETAIL cd WHERE cd.ORDER_ID = :orderId AND cd.STATUS = 'ASN' ", nativeQuery=true)
	public int cekStatusCollectionDetailByOrderId(@Param("orderId") int orderId);
	
	@Modifying
	@Transactional
	@Query(value="update "
			+ "COLLECTION_DETAIL "
			+ "set "
			+ "PROMISE_COUNT = :promiseCount, "
			+ "PROMISE_AVAILABLE = :promiseAvailable "
			+ "where ORDER_ID =:orderId AND CONTRACT_NO = :contractNo", nativeQuery=true)
	public void updatePromiseCountPromisAvailable(@Param("promiseCount") int promiseCount,@Param("promiseAvailable") int promiseAvailable,@Param("orderId") int orderId,@Param("contractNo") String contractNo);
	
	@Modifying
	@Transactional
	@Query(value="update "
			+ "COLLECTION_DETAIL "
			+ "set "
			+ "NOTES = :remark "
			+ "where ORDER_ID =:ORDER_ID", nativeQuery=true)
	public void updateCdToCloseInTaskAdjustment(@Param("remark") String remark,@Param("ORDER_ID") int ORDER_ID);
	
	
	@Query(value="SELECT "
			+ "CASE WHEN (cd.ORDER_ID IS NULL OR cd.ORDER_ID = 0) THEN '--' ELSE cast(cd.ORDER_ID as varchar)END AS ORDER_ID, " 
			+ "CASE WHEN (cd.CUSTOMER_NAME IS NULL OR cd.CUSTOMER_NAME = '') THEN '--' ELSE cast(cd.CUSTOMER_NAME as varchar) END AS CUSTOMER_NAME, " 
			+ "CASE WHEN (cd.ADDRESS IS NULL OR cd.ADDRESS = '') THEN '--' ELSE cast(cd.ADDRESS as varchar) END AS ADDRESS, "
			+ "CASE WHEN (up.NAMA_KARYAWAN IS NULL OR up.NAMA_KARYAWAN = '') THEN cast(ch.COLLECTOR_CODE as varchar) ELSE cast(up.NAMA_KARYAWAN as varchar) END AS NAMA_KARYAWAN, " 
			+ "cast(cd.INSTALLMENT_AMT + cd.PENALTY_AMT + cd.COLLECTION_FEE as varchar) as total, "
			+ "CASE WHEN (cd.NOTES IS NULL OR cd.NOTES = '') THEN '--' ELSE cast(cd.NOTES as varchar) END AS NOTES, "
			+ "CASE WHEN (cd.CONTRACT_NO IS NULL OR cd.CONTRACT_NO = '') THEN '--' ELSE cast(cd.CONTRACT_NO as varchar) END AS CONTRACT_NO, "
			+ "CASE WHEN (cd.INSTALLMENT_NO IS NULL OR cd.INSTALLMENT_NO = '') THEN '--' ELSE cast(cd.INSTALLMENT_NO as varchar) END AS INSTALLMENT_NO "
			+ "FROM COLLECTION_DETAIL cd " 
			+ "INNER JOIN COLLECTION_HEADER ch on ch.ORDER_ID = cd.ORDER_ID " 
			+ "LEFT JOIN USER_PROFILE up on up.KODE_KOLEKTOR = ch.COLLECTOR_CODE " 
			+ "LEFT JOIN HANDLING ha on ha.ORDER_ID = cd.ORDER_ID "
			+ "LEFT JOIN AKSES_REGION_CABANG arc on arc.USER_ID = up.USER_ID and arc.KODE_CABANG is null "
			+ "LEFT JOIN AKSES_REGION_CABANG arc2 on arc2.USER_ID = up.USER_ID and arc2.KODE_REGION is null "
			+ "WHERE ch.STATUS = 'ASN' AND arc.KODE_REGION = :kodeRegional AND (cd.ORDER_ID LIKE %:keySearch% OR cd.CUSTOMER_NAME LIKE %:keySearch% OR up.NAMA_KARYAWAN LIKE %:keySearch%) ", nativeQuery=true)
			public List<Object[]> selectForTaskAdjustmentByKodeRegional(@Param("kodeRegional") String kodeRegional, @Param("keySearch") String keySearch);
			
	@Query(value="SELECT "
			+ "CASE WHEN (cd.ORDER_ID IS NULL OR cd.ORDER_ID = 0) THEN '--' ELSE cast(cd.ORDER_ID as varchar)END AS ORDER_ID, " 
			+ "CASE WHEN (cd.CUSTOMER_NAME IS NULL OR cd.CUSTOMER_NAME = '') THEN '--' ELSE cast(cd.CUSTOMER_NAME as varchar) END AS CUSTOMER_NAME, " 
			+ "CASE WHEN (cd.ADDRESS IS NULL OR cd.ADDRESS = '') THEN '--' ELSE cast(cd.ADDRESS as varchar) END AS ADDRESS, "
			+ "CASE WHEN (up.NAMA_KARYAWAN IS NULL OR up.NAMA_KARYAWAN = '') THEN cast(ch.COLLECTOR_CODE as varchar) ELSE cast(up.NAMA_KARYAWAN as varchar) END AS NAMA_KARYAWAN, " 
			+ "cast(cd.INSTALLMENT_AMT + cd.PENALTY_AMT + cd.COLLECTION_FEE as varchar) as total, "
			+ "CASE WHEN (cd.NOTES IS NULL OR cd.NOTES = '') THEN '--' ELSE cast(cd.NOTES as varchar) END AS NOTES, "
			+ "CASE WHEN (cd.CONTRACT_NO IS NULL OR cd.CONTRACT_NO = '') THEN '--' ELSE cast(cd.CONTRACT_NO as varchar) END AS CONTRACT_NO, "
			+ "CASE WHEN (cd.INSTALLMENT_NO IS NULL OR cd.INSTALLMENT_NO = '') THEN '--' ELSE cast(cd.INSTALLMENT_NO as varchar) END AS INSTALLMENT_NO "
			+ "FROM COLLECTION_DETAIL cd " 
			+ "INNER JOIN COLLECTION_HEADER ch on ch.ORDER_ID = cd.ORDER_ID " 
			+ "LEFT JOIN USER_PROFILE up on up.KODE_KOLEKTOR = ch.COLLECTOR_CODE " 
			+ "LEFT JOIN HANDLING ha on ha.ORDER_ID = cd.ORDER_ID "
			+ "LEFT JOIN AKSES_REGION_CABANG arc on arc.USER_ID = up.USER_ID and arc.KODE_CABANG is null "
			+ "LEFT JOIN AKSES_REGION_CABANG arc2 on arc2.USER_ID = up.USER_ID and arc2.KODE_REGION is null "
			+ "WHERE ch.STATUS = 'ASN' AND cd.STATUS = 'ASN' AND arc2.KODE_CABANG = :kodeCabang AND (cd.ORDER_ID LIKE %:keySearch% OR cd.CUSTOMER_NAME LIKE %:keySearch% OR up.NAMA_KARYAWAN LIKE %:keySearch%) ", nativeQuery=true)
			public List<Object[]> selectForTaskAdjustmentByKodeCabang(@Param("kodeCabang") String kodeCabang, @Param("keySearch") String keySearch);
			
	@Query(value="SELECT "
			+ "CASE WHEN (cd.ORDER_ID IS NULL OR cd.ORDER_ID = 0) THEN '--' ELSE cast(cd.ORDER_ID as varchar)END AS ORDER_ID, " 
			+ "CASE WHEN (cd.CUSTOMER_NAME IS NULL OR cd.CUSTOMER_NAME = '') THEN '--' ELSE cast(cd.CUSTOMER_NAME as varchar) END AS CUSTOMER_NAME, " 
			+ "CASE WHEN (cd.ADDRESS IS NULL OR cd.ADDRESS = '') THEN '--' ELSE cast(cd.ADDRESS as varchar) END AS ADDRESS, "
			+ "CASE WHEN (up.NAMA_KARYAWAN IS NULL OR up.NAMA_KARYAWAN = '') THEN cast(ch.COLLECTOR_CODE as varchar) ELSE cast(up.NAMA_KARYAWAN as varchar) END AS NAMA_KARYAWAN, " 
			+ "cast(cd.INSTALLMENT_AMT + cd.PENALTY_AMT + cd.COLLECTION_FEE as varchar) as total, "
			+ "CASE WHEN (cd.NOTES IS NULL OR cd.NOTES = '') THEN '--' ELSE cast(cd.NOTES as varchar) END AS NOTES, "
			+ "CASE WHEN (cd.CONTRACT_NO IS NULL OR cd.CONTRACT_NO = '') THEN '--' ELSE cast(cd.CONTRACT_NO as varchar) END AS CONTRACT_NO, "
			+ "CASE WHEN (cd.INSTALLMENT_NO IS NULL OR cd.INSTALLMENT_NO = '') THEN '--' ELSE cast(cd.INSTALLMENT_NO as varchar) END AS INSTALLMENT_NO "
			+ "FROM COLLECTION_DETAIL cd " 
			+ "INNER JOIN COLLECTION_HEADER ch on ch.ORDER_ID = cd.ORDER_ID " 
			+ "LEFT JOIN USER_PROFILE up on up.KODE_KOLEKTOR = ch.COLLECTOR_CODE " 
			+ "LEFT JOIN HANDLING ha on ha.ORDER_ID = cd.ORDER_ID "
			+ "LEFT JOIN AKSES_REGION_CABANG arc on arc.USER_ID = up.USER_ID and arc.KODE_CABANG is null "
			+ "LEFT JOIN AKSES_REGION_CABANG arc2 on arc2.USER_ID = up.USER_ID and arc2.KODE_REGION is null "
			+ "WHERE ch.STATUS = 'ASN' AND cd.STATUS = 'ASN' AND arc2.KODE_CABANG = :kodeCabang AND ch.COLLECTOR_CODE = :kodeKol AND (cd.ORDER_ID LIKE %:keySearch% OR cd.CUSTOMER_NAME LIKE %:keySearch% OR up.NAMA_KARYAWAN LIKE %:keySearch%) ", nativeQuery=true)
			public List<Object[]> selectForTaskAdjustmentByKodeCabangKodeKol(@Param("kodeCabang") String kodeCabang,@Param("kodeKol") String kodeKol, @Param("keySearch") String keySearch);
	
			@Query(value="SELECT "
					+ "CASE WHEN (cd.ORDER_ID IS NULL OR cd.ORDER_ID = 0) THEN '--' ELSE cast(cd.ORDER_ID as varchar)END AS ORDER_ID, " 
					+ "CASE WHEN (cd.CUSTOMER_NAME IS NULL OR cd.CUSTOMER_NAME = '') THEN '--' ELSE cast(cd.CUSTOMER_NAME as varchar) END AS CUSTOMER_NAME, " 
					+ "CASE WHEN (cd.ADDRESS IS NULL OR cd.ADDRESS = '') THEN '--' ELSE cast(cd.ADDRESS as varchar) END AS ADDRESS, "
					+ "CASE WHEN (up.NAMA_KARYAWAN IS NULL OR up.NAMA_KARYAWAN = '') THEN cast(ch.COLLECTOR_CODE as varchar) ELSE cast(up.NAMA_KARYAWAN as varchar) END AS NAMA_KARYAWAN, " 
					+ "cast(cd.INSTALLMENT_AMT + cd.PENALTY_AMT + cd.COLLECTION_FEE as varchar) as total, "
					+ "CASE WHEN (cd.NOTES IS NULL OR cd.NOTES = '') THEN '--' ELSE cast(cd.NOTES as varchar) END AS NOTES, "
					+ "CASE WHEN (cd.CONTRACT_NO IS NULL OR cd.CONTRACT_NO = '') THEN '--' ELSE cast(cd.CONTRACT_NO as varchar) END AS CONTRACT_NO, "
					+ "CASE WHEN (cd.INSTALLMENT_NO IS NULL OR cd.INSTALLMENT_NO = '') THEN '--' ELSE cast(cd.INSTALLMENT_NO as varchar) END AS INSTALLMENT_NO "
					+ "FROM COLLECTION_DETAIL cd " 
					+ "INNER JOIN COLLECTION_HEADER ch on ch.ORDER_ID = cd.ORDER_ID " 
					+ "LEFT JOIN USER_PROFILE up on up.KODE_KOLEKTOR = ch.COLLECTOR_CODE " 
					+ "LEFT JOIN HANDLING ha on ha.ORDER_ID = cd.ORDER_ID "
					+ "LEFT JOIN AKSES_REGION_CABANG arc on arc.USER_ID = up.USER_ID and arc.KODE_CABANG is null "
					+ "LEFT JOIN AKSES_REGION_CABANG arc2 on arc2.USER_ID = up.USER_ID and arc2.KODE_REGION is null "
					+ "WHERE ch.STATUS = 'ASN' AND cd.STATUS = 'ASN' AND arc.KODE_REGION = :kodeRegional AND ch.COLLECTOR_CODE = :kodeKol AND (cd.ORDER_ID LIKE %:keySearch% OR cd.CUSTOMER_NAME LIKE %:keySearch% OR up.NAMA_KARYAWAN LIKE %:keySearch%) ", nativeQuery=true)
					public List<Object[]> selectForTaskAdjustmentByKodeRegionalKodeKol(@Param("kodeRegional") String kodeRegional,@Param("kodeKol") String kodeKol, @Param("keySearch") String keySearch);
			
		@Query(value="SELECT "
				+ "CASE WHEN (cd.ORDER_ID IS NULL OR cd.ORDER_ID = 0) THEN '--' ELSE cast(cd.ORDER_ID as varchar)END AS ORDER_ID, " 
				+ "CASE WHEN (cd.CUSTOMER_NAME IS NULL OR cd.CUSTOMER_NAME = '') THEN '--' ELSE cast(cd.CUSTOMER_NAME as varchar) END AS CUSTOMER_NAME, " 
				+ "CASE WHEN (cd.ADDRESS IS NULL OR cd.ADDRESS = '') THEN '--' ELSE cast(cd.ADDRESS as varchar) END AS ADDRESS, "
				+ "CASE WHEN (up.NAMA_KARYAWAN IS NULL OR up.NAMA_KARYAWAN = '') THEN cast(ch.COLLECTOR_CODE as varchar) ELSE cast(up.NAMA_KARYAWAN as varchar) END AS NAMA_KARYAWAN, " 
				+ "cast(cd.INSTALLMENT_AMT + cd.PENALTY_AMT + cd.COLLECTION_FEE as varchar) as total, "
				+ "CASE WHEN (cd.NOTES IS NULL OR cd.NOTES = '') THEN '--' ELSE cast(cd.NOTES as varchar) END AS NOTES, "
				+ "CASE WHEN (cd.CONTRACT_NO IS NULL OR cd.CONTRACT_NO = '') THEN '--' ELSE cast(cd.CONTRACT_NO as varchar) END AS CONTRACT_NO, "
				+ "CASE WHEN (cd.INSTALLMENT_NO IS NULL OR cd.INSTALLMENT_NO = '') THEN '--' ELSE cast(cd.INSTALLMENT_NO as varchar) END AS INSTALLMENT_NO "
				+ "FROM COLLECTION_DETAIL cd " 
				+ "INNER JOIN COLLECTION_HEADER ch on ch.ORDER_ID = cd.ORDER_ID " 
				+ "LEFT JOIN USER_PROFILE up on up.KODE_KOLEKTOR = ch.COLLECTOR_CODE " 
				+ "LEFT JOIN HANDLING ha on ha.ORDER_ID = cd.ORDER_ID "
				+ "LEFT JOIN AKSES_REGION_CABANG arc on arc.USER_ID = up.USER_ID and arc.KODE_CABANG is null "
				+ "LEFT JOIN AKSES_REGION_CABANG arc2 on arc2.USER_ID = up.USER_ID and arc2.KODE_REGION is null "
				+ "WHERE ch.STATUS = 'ASN' AND cd.STATUS = 'ASN' AND arc.KODE_REGION = :kodeRegional AND arc2.KODE_CABANG = :kodeCabang AND (cd.ORDER_ID LIKE %:keySearch% OR cd.CUSTOMER_NAME LIKE %:keySearch% OR up.NAMA_KARYAWAN LIKE %:keySearch%) ", nativeQuery=true)
				public List<Object[]> selectForTaskAdjustmentByKodeRegionalKodeCabang(@Param("kodeRegional") String kodeRegional,@Param("kodeCabang") String kodeCabang, @Param("keySearch") String keySearch);	
					
		@Query(value="SELECT "
			+ "CASE WHEN (ch.COLLECTOR_CODE IS NULL OR ch.COLLECTOR_CODE = '') THEN '--' ELSE cast(ch.COLLECTOR_CODE as varchar)END AS COLLECTOR_CODE, "
			+ "CASE WHEN (up.NAMA_KARYAWAN IS NULL OR up.NAMA_KARYAWAN = '') THEN '--' ELSE cast(up.NAMA_KARYAWAN as varchar)END AS NAMA_KARYAWAN, "
			+ "CASE WHEN (cm.NAMA_CABANG IS NULL OR cm.NAMA_CABANG = '') THEN '--' ELSE cast(cm.NAMA_CABANG as varchar)END AS NAMA_CABANG, "
			+ "COUNT(cd.CONTRACT_NO) as jumlahKunjungan, "
			+ "CASE WHEN (up.USER_ID IS NULL OR up.USER_ID = '') THEN '--' ELSE cast(up.USER_ID as varchar) END AS USER_ID "
			+ "FROM COLLECTION_DETAIL cd "
			+ "INNER JOIN COLLECTION_HEADER ch on ch.ORDER_ID = cd.ORDER_ID "
			+ "INNER JOIN USER_PROFILE up on up.KODE_KOLEKTOR = ch.COLLECTOR_CODE "
			+ "INNER JOIN AKSES_REGION_CABANG arb on arb.USER_ID = up.USER_ID and arb.KODE_REGION is null "
			+ "INNER JOIN CABANG_MST cm on cm.KODE_CABANG = arb.KODE_CABANG "
			+ "WHERE cd.STATUS = 'CMP' AND cm.KODE_CABANG = :kodeCabang AND ch.LPK_DATE >= :dateAwal AND ch.LPK_DATE <= :dateAkhir "
			+ "AND (ch.COLLECTOR_CODE LIKE %:keySearch% OR up.NAMA_KARYAWAN LIKE %:keySearch% OR cm.NAMA_CABANG LIKE %:keySearch%) "
			+ "GROUP BY up.USER_ID, ch.COLLECTOR_CODE, up.NAMA_KARYAWAN, cm.NAMA_CABANG ", nativeQuery=true)
	public List<Object[]> selectReportKunjunganKodeCabang(@Param("dateAwal") Date dateAwal, @Param("dateAkhir") Date dateAkhir, @Param("kodeCabang") String kodeCabang, @Param("keySearch") String keySearch);
	
	@Query(value="SELECT "
			+ "CASE WHEN (ch.COLLECTOR_CODE IS NULL OR ch.COLLECTOR_CODE = '') THEN '--' ELSE cast(ch.COLLECTOR_CODE as varchar)END AS COLLECTOR_CODE, "
			+ "CASE WHEN (up.NAMA_KARYAWAN IS NULL OR up.NAMA_KARYAWAN = '') THEN '--' ELSE cast(up.NAMA_KARYAWAN as varchar)END AS NAMA_KARYAWAN, "
			+ "CASE WHEN (cm.NAMA_CABANG IS NULL OR cm.NAMA_CABANG = '') THEN '--' ELSE cast(cm.NAMA_CABANG as varchar)END AS NAMA_CABANG, "
			+ "COUNT(cd.CONTRACT_NO) as jumlahKunjungan, "
			+ "CASE WHEN (up.USER_ID IS NULL OR up.USER_ID = '') THEN '--' ELSE cast(up.USER_ID as varchar) END AS USER_ID "
			+ "FROM COLLECTION_DETAIL cd "
			+ "INNER JOIN COLLECTION_HEADER ch on ch.ORDER_ID = cd.ORDER_ID "
			+ "INNER JOIN USER_PROFILE up on up.KODE_KOLEKTOR = ch.COLLECTOR_CODE "
			+ "INNER JOIN AKSES_REGION_CABANG arb on arb.USER_ID = up.USER_ID and arb.KODE_REGION is null "
			+ "INNER JOIN CABANG_MST cm on cm.KODE_CABANG = arb.KODE_CABANG "
			+ "WHERE cm.KODE_CABANG = :kodeCabang AND ch.LPK_DATE >= :dateAwal AND ch.LPK_DATE <= :dateAkhir "
			+ "AND (ch.COLLECTOR_CODE LIKE %:keySearch% OR up.NAMA_KARYAWAN LIKE %:keySearch% OR cm.NAMA_CABANG LIKE %:keySearch%) "
			+ "GROUP BY up.USER_ID, ch.COLLECTOR_CODE, up.NAMA_KARYAWAN, cm.NAMA_CABANG ", nativeQuery=true)
	public List<Object[]> selectRekonKolektorKodeCabang(@Param("dateAwal") Date dateAwal, @Param("dateAkhir") Date dateAkhir, @Param("kodeCabang") String kodeCabang, @Param("keySearch") String keySearch);
	
	
	@Query(value="SELECT "
			+ "CASE WHEN (ch.COLLECTOR_CODE IS NULL OR ch.COLLECTOR_CODE = '') THEN '--' ELSE cast(ch.COLLECTOR_CODE as varchar)END AS COLLECTOR_CODE, "
			+ "CASE WHEN (up.NAMA_KARYAWAN IS NULL OR up.NAMA_KARYAWAN = '') THEN '--' ELSE cast(up.NAMA_KARYAWAN as varchar)END AS NAMA_KARYAWAN, "
			+ "CASE WHEN (cm.NAMA_CABANG IS NULL OR cm.NAMA_CABANG = '') THEN '--' ELSE cast(cm.NAMA_CABANG as varchar)END AS NAMA_CABANG, "
			+ "COUNT(cd.CONTRACT_NO) as jumlahKunjungan, "
			+ "CASE WHEN (up.USER_ID IS NULL OR up.USER_ID = '') THEN '--' ELSE cast(up.USER_ID as varchar) END AS USER_ID "
			+ "FROM COLLECTION_DETAIL cd "
			+ "INNER JOIN COLLECTION_HEADER ch on ch.ORDER_ID = cd.ORDER_ID "
			+ "INNER JOIN USER_PROFILE up on up.KODE_KOLEKTOR = ch.COLLECTOR_CODE "
			+ "INNER JOIN AKSES_REGION_CABANG arb on arb.USER_ID = up.USER_ID and arb.KODE_REGION is null "
			+ "INNER JOIN CABANG_MST cm on cm.KODE_CABANG = arb.KODE_CABANG "
			+ "WHERE cd.STATUS = 'CMP' AND ch.COLLECTOR_CODE = :kodeKolektor AND cm.KODE_CABANG = :kodeCabang AND ch.LPK_DATE >= :dateAwal AND ch.LPK_DATE <= :dateAkhir "
			+ "AND (ch.COLLECTOR_CODE LIKE %:keySearch% OR up.NAMA_KARYAWAN LIKE %:keySearch% OR cm.NAMA_CABANG LIKE %:keySearch%) "
			+ "GROUP BY up.USER_ID, ch.COLLECTOR_CODE, up.NAMA_KARYAWAN, cm.NAMA_CABANG ", nativeQuery=true)
	public List<Object[]> selectReportKunjunganKodeCabangKodeKolektor(@Param("dateAwal") Date dateAwal, @Param("dateAkhir") Date dateAkhir, @Param("kodeCabang") String kodeCabang, @Param("kodeKolektor") String kodeKolektor, @Param("keySearch") String keySearch);
	
	@Query(value="SELECT "
			+ "CASE WHEN (ch.COLLECTOR_CODE IS NULL OR ch.COLLECTOR_CODE = '') THEN '--' ELSE cast(ch.COLLECTOR_CODE as varchar)END AS COLLECTOR_CODE, "
			+ "CASE WHEN (up.NAMA_KARYAWAN IS NULL OR up.NAMA_KARYAWAN = '') THEN '--' ELSE cast(up.NAMA_KARYAWAN as varchar)END AS NAMA_KARYAWAN, "
			+ "CASE WHEN (cm.NAMA_CABANG IS NULL OR cm.NAMA_CABANG = '') THEN '--' ELSE cast(cm.NAMA_CABANG as varchar)END AS NAMA_CABANG, "
			+ "COUNT(cd.CONTRACT_NO) as jumlahKunjungan, "
			+ "CASE WHEN (up.USER_ID IS NULL OR up.USER_ID = '') THEN '--' ELSE cast(up.USER_ID as varchar) END AS USER_ID "
			+ "FROM COLLECTION_DETAIL cd "
			+ "INNER JOIN COLLECTION_HEADER ch on ch.ORDER_ID = cd.ORDER_ID "
			+ "INNER JOIN USER_PROFILE up on up.KODE_KOLEKTOR = ch.COLLECTOR_CODE "
			+ "INNER JOIN AKSES_REGION_CABANG arb on arb.USER_ID = up.USER_ID and arb.KODE_REGION is null "
			+ "INNER JOIN CABANG_MST cm on cm.KODE_CABANG = arb.KODE_CABANG "
			+ "WHERE ch.COLLECTOR_CODE = :kodeKolektor AND cm.KODE_CABANG = :kodeCabang AND ch.LPK_DATE >= :dateAwal AND ch.LPK_DATE <= :dateAkhir "
			+ "AND (ch.COLLECTOR_CODE LIKE %:keySearch% OR up.NAMA_KARYAWAN LIKE %:keySearch% OR cm.NAMA_CABANG LIKE %:keySearch%) "
			+ "GROUP BY up.USER_ID, ch.COLLECTOR_CODE, up.NAMA_KARYAWAN, cm.NAMA_CABANG ", nativeQuery=true)
	public List<Object[]> selectRekonKolektorKodeCabangKodeKolektor(@Param("dateAwal") Date dateAwal, @Param("dateAkhir") Date dateAkhir, @Param("kodeCabang") String kodeCabang, @Param("kodeKolektor") String kodeKolektor, @Param("keySearch") String keySearch);
	
	@Query(value="SELECT "
			+ "CASE WHEN (ch.COLLECTOR_CODE IS NULL OR ch.COLLECTOR_CODE = '') THEN '--' ELSE cast(ch.COLLECTOR_CODE as varchar)END AS COLLECTOR_CODE, "
			+ "CASE WHEN (up.NAMA_KARYAWAN IS NULL OR up.NAMA_KARYAWAN = '') THEN '--' ELSE cast(up.NAMA_KARYAWAN as varchar)END AS NAMA_KARYAWAN, "
			+ "CASE WHEN (cm.NAMA_CABANG IS NULL OR cm.NAMA_CABANG = '') THEN '--' ELSE cast(cm.NAMA_CABANG as varchar)END AS NAMA_CABANG, "
			+ "COUNT(cd.CONTRACT_NO) as jumlahKunjungan, "
			+ "CASE WHEN (up.USER_ID IS NULL OR up.USER_ID = '') THEN '--' ELSE cast(up.USER_ID as varchar) END AS USER_ID "
			+ "FROM COLLECTION_DETAIL cd "
			+ "INNER JOIN COLLECTION_HEADER ch on ch.ORDER_ID = cd.ORDER_ID "
			+ "INNER JOIN USER_PROFILE up on up.KODE_KOLEKTOR = ch.COLLECTOR_CODE "
			+ "INNER JOIN AKSES_REGION_CABANG arb on arb.USER_ID = up.USER_ID and arb.KODE_REGION is null "
			+ "INNER JOIN CABANG_MST cm on cm.KODE_CABANG = arb.KODE_CABANG "
			+ "WHERE ch.COLLECTOR_CODE = :kodeKolektor AND cm.KODE_REGION = :kodeRegion AND ch.LPK_DATE >= :dateAwal AND ch.LPK_DATE <= :dateAkhir "
			+ "AND (ch.COLLECTOR_CODE LIKE %:keySearch% OR up.NAMA_KARYAWAN LIKE %:keySearch% OR cm.NAMA_CABANG LIKE %:keySearch%) "
			+ "GROUP BY up.USER_ID, ch.COLLECTOR_CODE, up.NAMA_KARYAWAN, cm.NAMA_CABANG ", nativeQuery=true)
	public List<Object[]> selectRekonKolektorKodeRegionalKodeKolektor(@Param("dateAwal") Date dateAwal, @Param("dateAkhir") Date dateAkhir, @Param("kodeRegion") String kodeRegion, @Param("kodeKolektor") String kodeKolektor, @Param("keySearch") String keySearch);
	
	
	@Query(value="SELECT "
			+ "CASE WHEN (ch.ORDER_ID IS NULL OR ch.ORDER_ID = '') THEN '--' ELSE cast(ch.ORDER_ID as varchar)END AS ORDER_ID, "
			+ "CASE WHEN (ch.COLLECTOR_CODE IS NULL OR ch.COLLECTOR_CODE = '') THEN '--' ELSE cast(ch.COLLECTOR_CODE as varchar)END AS COLLECTOR_CODE, "
			+ "CASE WHEN (up.NAMA_KARYAWAN IS NULL OR up.NAMA_KARYAWAN = '') THEN '--' ELSE cast(up.NAMA_KARYAWAN as varchar)END AS NAMA_KARYAWAN, "
			+ "CASE WHEN (cm.NAMA_CABANG IS NULL OR cm.NAMA_CABANG = '') THEN '--' ELSE cast(cm.NAMA_CABANG as varchar)END AS NAMA_CABANG, "
			+ "COUNT(cd.CONTRACT_NO) as jumlahKunjungan, "
			+ "CASE WHEN (up.USER_ID IS NULL OR up.USER_ID = '') THEN '--' ELSE cast(up.USER_ID as varchar) END AS USER_ID "
			+ "FROM COLLECTION_DETAIL cd "
			+ "INNER JOIN COLLECTION_HEADER ch on ch.ORDER_ID = cd.ORDER_ID "
			+ "INNER JOIN USER_PROFILE up on up.KODE_KOLEKTOR = ch.COLLECTOR_CODE "
			+ "INNER JOIN AKSES_REGION_CABANG arb on arb.USER_ID = up.USER_ID and arb.KODE_REGION is null "
			+ "INNER JOIN CABANG_MST cm on cm.KODE_CABANG = arb.KODE_CABANG "
			+ "WHERE cd.STATUS = 'CMP' AND ch.COLLECTOR_CODE = :kodeKolektor AND cm.KODE_REGION = :kodeRegional AND ch.LPK_DATE >= :dateAwal AND ch.LPK_DATE <= :dateAkhir "
			+ "AND (ch.COLLECTOR_CODE LIKE %:keySearch% OR up.NAMA_KARYAWAN LIKE %:keySearch% OR cm.NAMA_CABANG LIKE %:keySearch%) "
			+ "GROUP BY up.USER_ID, ch.ORDER_ID, ch.COLLECTOR_CODE, up.NAMA_KARYAWAN, cm.NAMA_CABANG ", nativeQuery=true)
	public List<Object[]> selectReportKunjunganKodeRegionalKodeKolektor(@Param("dateAwal") Date dateAwal, @Param("dateAkhir") Date dateAkhir, @Param("kodeRegional") String kodeRegional, @Param("kodeKolektor") String kodeKolektor, @Param("keySearch") String keySearch);
	
	@Query(value="SELECT "
			+ "CASE WHEN (ch.COLLECTOR_CODE IS NULL OR ch.COLLECTOR_CODE = '') THEN '--' ELSE cast(ch.COLLECTOR_CODE as varchar)END AS COLLECTOR_CODE, "
			+ "CASE WHEN (up.NAMA_KARYAWAN IS NULL OR up.NAMA_KARYAWAN = '') THEN '--' ELSE cast(up.NAMA_KARYAWAN as varchar)END AS NAMA_KARYAWAN, "
			+ "CASE WHEN (cm.NAMA_CABANG IS NULL OR cm.NAMA_CABANG = '') THEN '--' ELSE cast(cm.NAMA_CABANG as varchar)END AS NAMA_CABANG, "
			+ "COUNT(cd.CONTRACT_NO) as jumlahKunjungan, "
			+ "CASE WHEN (up.USER_ID IS NULL OR up.USER_ID = '') THEN '--' ELSE cast(up.USER_ID as varchar) END AS USER_ID "
			+ "FROM COLLECTION_DETAIL cd "
			+ "INNER JOIN COLLECTION_HEADER ch on ch.ORDER_ID = cd.ORDER_ID "
			+ "INNER JOIN USER_PROFILE up on up.KODE_KOLEKTOR = ch.COLLECTOR_CODE "
			+ "INNER JOIN AKSES_REGION_CABANG arb on arb.USER_ID = up.USER_ID and arb.KODE_REGION is null "
			+ "INNER JOIN CABANG_MST cm on cm.KODE_CABANG = arb.KODE_CABANG "
			+ "WHERE cd.STATUS = 'CMP' AND cm.KODE_CABANG = :kodeCabang AND cm.KODE_REGION = :kodeRegional AND ch.LPK_DATE >= :dateAwal AND ch.LPK_DATE <= :dateAkhir "
			+ "AND (ch.COLLECTOR_CODE LIKE %:keySearch% OR up.NAMA_KARYAWAN LIKE %:keySearch% OR cm.NAMA_CABANG LIKE %:keySearch%) "
			+ "GROUP BY up.USER_ID, ch.COLLECTOR_CODE, up.NAMA_KARYAWAN, cm.NAMA_CABANG ", nativeQuery=true)
	public List<Object[]> selectReportKunjunganKodeRegionalKodeCabang(@Param("dateAwal") Date dateAwal, @Param("dateAkhir") Date dateAkhir, @Param("kodeRegional") String kodeRegional, @Param("kodeCabang") String kodeCabang, @Param("keySearch") String keySearch);
	
	@Query(value="SELECT "
			+ "CASE WHEN (ch.COLLECTOR_CODE IS NULL OR ch.COLLECTOR_CODE = '') THEN '--' ELSE cast(ch.COLLECTOR_CODE as varchar)END AS COLLECTOR_CODE, "
			+ "CASE WHEN (up.NAMA_KARYAWAN IS NULL OR up.NAMA_KARYAWAN = '') THEN '--' ELSE cast(up.NAMA_KARYAWAN as varchar)END AS NAMA_KARYAWAN, "
			+ "CASE WHEN (cm.NAMA_CABANG IS NULL OR cm.NAMA_CABANG = '') THEN '--' ELSE cast(cm.NAMA_CABANG as varchar)END AS NAMA_CABANG, "
			+ "COUNT(cd.CONTRACT_NO) as jumlahKunjungan, "
			+ "CASE WHEN (up.USER_ID IS NULL OR up.USER_ID = '') THEN '--' ELSE cast(up.USER_ID as varchar) END AS USER_ID "
			+ "FROM COLLECTION_DETAIL cd "
			+ "INNER JOIN COLLECTION_HEADER ch on ch.ORDER_ID = cd.ORDER_ID "
			+ "INNER JOIN USER_PROFILE up on up.KODE_KOLEKTOR = ch.COLLECTOR_CODE "
			+ "INNER JOIN AKSES_REGION_CABANG arb on arb.USER_ID = up.USER_ID and arb.KODE_REGION is null "
			+ "INNER JOIN CABANG_MST cm on cm.KODE_CABANG = arb.KODE_CABANG "
			+ "WHERE cm.KODE_CABANG = :kodeCabang AND cm.KODE_REGION = :kodeRegional AND ch.LPK_DATE >= :dateAwal AND ch.LPK_DATE <= :dateAkhir "
			+ "AND (ch.COLLECTOR_CODE LIKE %:keySearch% OR up.NAMA_KARYAWAN LIKE %:keySearch% OR cm.NAMA_CABANG LIKE %:keySearch%) "
			+ "GROUP BY up.USER_ID, ch.COLLECTOR_CODE, up.NAMA_KARYAWAN, cm.NAMA_CABANG ", nativeQuery=true)
	public List<Object[]> selectRekonKolektorKodeRegionalKodeCabang(@Param("dateAwal") Date dateAwal, @Param("dateAkhir") Date dateAkhir, @Param("kodeRegional") String kodeRegional, @Param("kodeCabang") String kodeCabang, @Param("keySearch") String keySearch);
	
	
	
	@Query(value="SELECT " 
			+ "CASE WHEN (cd.CUSTOMER_NAME IS NULL OR cd.CUSTOMER_NAME = '') THEN '--' ELSE cast(cd.CUSTOMER_NAME as varchar) END AS CUSTOMER_NAME, "
			+ "CASE WHEN (pa.TANGGAL_BAYAR IS NULL OR pa.TANGGAL_BAYAR = '') THEN '--' ELSE cast(pa.TANGGAL_BAYAR as varchar)END AS TANGGAL_BAYAR, "
			+ "CASE WHEN (ha.TANGGAL_JANJI IS NULL OR ha.TANGGAL_JANJI = '') THEN '--' ELSE cast(ha.TANGGAL_JANJI as varchar)END AS TANGGAL_JANJI, "
			+ "CASE WHEN (ha.NOTES IS NULL OR ha.NOTES = '') THEN '--' ELSE cast(ha.NOTES as varchar)END AS ALASAN, "
			+ "CASE WHEN (cd.INSTALLMENT_AMT IS NULL OR cd.INSTALLMENT_AMT = '0') THEN '0' ELSE cast(cd.INSTALLMENT_AMT as varchar)END AS total, "
			+ "CASE WHEN (ch.LPK_DATE IS NULL OR ch.LPK_DATE = '') THEN '--' ELSE cast(ch.LPK_DATE as varchar)END AS LPK_DATE, "
			+ "CASE WHEN (ha.ORDER_ID is null) THEN 'BAYAR' ELSE 'TIDAK BAYAR' END AS STATUS, "
			+ "CASE WHEN (cd.CONTRACT_NO IS NULL OR cd.CONTRACT_NO = '') THEN '--' ELSE cast(cd.CONTRACT_NO as varchar) END AS CONTRACT_NO "
			+ "FROM COLLECTION_DETAIL cd "
			+ "LEFT JOIN PAYMENT pa on pa.ORDER_ID = cd.ORDER_ID and pa.INSTALLMENT_NO = cd.INSTALLMENT_NO and pa.CONTRACT_NO = cd.CONTRACT_NO "
			+ "LEFT JOIN HANDLING ha on ha.ORDER_ID = cd.ORDER_ID and ha.INSTALLMENT_NO = cd.INSTALLMENT_NO and ha.CONTRACT_NO = cd.CONTRACT_NO "
			+ "INNER JOIN COLLECTION_HEADER ch on ch.ORDER_ID = cd.ORDER_ID "
			+ "INNER JOIN USER_PROFILE up on up.KODE_KOLEKTOR = ch.COLLECTOR_CODE "
			+ "WHERE cd.STATUS = 'CMP' AND ch.LPK_DATE >= :dateAwal AND ch.LPK_DATE <= :dateAkhir AND up.USER_ID = :userId AND "
			+ "(cd.CUSTOMER_NAME LIKE %:keySearch% OR pa.TANGGAL_BAYAR LIKE %:keySearch% OR ha.TANGGAL_JANJI LIKE %:keySearch%)", nativeQuery=true)
			public List<Object[]> selectDetailReportKunjungan(@Param("dateAwal") Date dateAwal, @Param("dateAkhir") Date dateAkhir, @Param("userId") String userId, @Param("keySearch") String keySearch);
	
	@Query(value="SELECT "
			+ "a.CONTRACT_NO, "
			+ "a.CUSTOMER_NAME, " 
			+ "a.NO_KWITANSI, "
			+ "sum(a.ang) as angsuran, " 
			+ "sum(a.jumlahKuitansi) as jumKui, " 
			+ "sum(a.kuitansiTertagih) as kuiTertagih, " 
			+ "sum(a.pemba) as pembayaran  "
			+ "FROM( "
					+ "SELECT "
					+ "CASE WHEN (cd.CONTRACT_NO IS NULL OR cd.CONTRACT_NO = '') THEN '--' ELSE cast(cd.CONTRACT_NO as varchar) END AS CONTRACT_NO, "
					+ "CASE WHEN (cd.CUSTOMER_NAME IS NULL OR cd.CUSTOMER_NAME = '') THEN '--' ELSE cast(cd.CUSTOMER_NAME as varchar)END AS CUSTOMER_NAME, " 
					+ "CASE WHEN (pa.NO_KWITANSI IS NULL OR pa.NO_KWITANSI = '') THEN '--' ELSE cast(pa.NO_KWITANSI as varchar)END AS NO_KWITANSI, " 
					+ "cast(cd.INSTALLMENT_AMT as int) as ang, "
					+ "count(cd.RECEIPT_NO) as jumlahKuitansi, "
					+ "count(pa.ORDER_ID) as kuitansiTertagih, "
					+ "sum(CASE WHEN (pa.TOTAL_BAYAR IS NULL OR pa.TOTAL_BAYAR = '') THEN 0 ELSE cast(pa.TOTAL_BAYAR as int) END) as pemba "  
					+ "FROM COLLECTION_DETAIL cd "
					+ "LEFT JOIN PAYMENT pa on pa.ORDER_ID = cd.ORDER_ID and pa.INSTALLMENT_NO = cd.INSTALLMENT_NO and pa.CONTRACT_NO = cd.CONTRACT_NO "
					+ "LEFT JOIN HANDLING ha on ha.ORDER_ID = cd.ORDER_ID and ha.INSTALLMENT_NO = cd.INSTALLMENT_NO and ha.CONTRACT_NO = cd.CONTRACT_NO "
					+ "INNER JOIN COLLECTION_HEADER ch on ch.ORDER_ID = cd.ORDER_ID "
					+ "INNER JOIN USER_PROFILE up on up.KODE_KOLEKTOR = ch.COLLECTOR_CODE " 
					+ "WHERE ch.LPK_DATE >= :dateAwal AND ch.LPK_DATE <= :dateAkhir AND up.USER_ID = :userId AND "
					+ "(cd.CONTRACT_NO LIKE %:keySearch% OR cd.CUSTOMER_NAME LIKE %:keySearch% OR cd.RECEIPT_NO LIKE %:keySearch%) "
					+ "GROUP BY cd.CONTRACT_NO,cd.CUSTOMER_NAME,pa.NO_KWITANSI,cd.INSTALLMENT_AMT "
			+ ") as a "
			+ "GROUP BY a.CONTRACT_NO, "
			+ "a.CUSTOMER_NAME, "
			+ "a.NO_KWITANSI  ", nativeQuery=true)
	public List<Object[]> selectDetailRekonKolektor(@Param("dateAwal") Date dateAwal, @Param("dateAkhir") Date dateAkhir, @Param("userId") String userId, @Param("keySearch") String keySearch);
	
	@Query(value="SELECT " 
			+ "CASE WHEN (cd.INSTALLMENT_NO IS NULL OR cd.INSTALLMENT_NO = 0) THEN '0' ELSE cast(cd.INSTALLMENT_NO as varchar)END AS INSTALLMENT_NO, " 
			+ "CASE WHEN (cd.ORDER_ID IS NULL OR cd.ORDER_ID = 0) THEN '0' ELSE cast(cd.ORDER_ID as varchar) END AS ORDER_ID, "
			+ "CASE WHEN (cd.CUSTOMER_CODE IS NULL OR cd.CUSTOMER_CODE = '') THEN '--' ELSE cast(cd.CUSTOMER_CODE as varchar) END AS CUSTOMER_CODE, " 
			+ "CASE WHEN (cd.CONTRACT_NO IS NULL OR cd.CONTRACT_NO = '') THEN '--' ELSE cast(cd.CONTRACT_NO as varchar) END AS CONTRACT_NO, "
			+ "CASE WHEN (ch.COLLECTOR_CODE IS NULL OR ch.COLLECTOR_CODE = '') THEN '--' ELSE cast(ch.COLLECTOR_CODE as varchar) END AS COLLECTOR_CODE, "
			+ "CASE WHEN (cd.TOTAL_AMT IS NULL OR cd.TOTAL_AMT = 0) THEN '0' ELSE cast(cd.TOTAL_AMT as varchar) END AS TOTAL_AMT, "
			+ "CASE WHEN (cd.RECEIPT_NO IS NULL OR cd.RECEIPT_NO = '') THEN '--' ELSE cast(cd.RECEIPT_NO as varchar) END AS RECEIPT_NO, "
			+ "CASE WHEN (cd.LPK_NO IS NULL OR cd.LPK_NO = '') THEN '--' ELSE cast(cd.LPK_NO as varchar) END AS LPK_NO "
			+ "FROM COLLECTION_DETAIL cd "
			+ "INNER JOIN COLLECTION_HEADER ch on ch.ORDER_ID = cd.ORDER_ID " 
			+ "WHERE cd.ORDER_ID = :orderId AND cd.CONTRACT_NO = :contractNo AND cd.INSTALLMENT_NO = :instalmentNo", nativeQuery=true)
	public List<Object[]> selectCollectionDetailForInputPayment(@Param("orderId") int orderId, @Param("contractNo") String contractNo,@Param("instalmentNo") int instalmentNo);
	
	@Query(value="SELECT " 
			+ "CASE WHEN (cd.CONTRACT_NO IS NULL OR cd.CONTRACT_NO = '') THEN '--' ELSE cast(cd.CONTRACT_NO as varchar) END AS CONTRACT_NO, "
			+ "CASE WHEN (cd.INSTALLMENT_NO IS NULL OR cd.INSTALLMENT_NO = 0) THEN '--' ELSE cast(cd.INSTALLMENT_NO as varchar) END AS INSTALLMENT_NO, "
			+ "CASE WHEN (cd.ORDER_ID IS NULL OR cd.ORDER_ID = '') THEN '--' ELSE cast(cd.ORDER_ID as varchar) END AS ORDER_ID, "
			+ "CASE WHEN (cd.CUSTOMER_CODE IS NULL OR cd.CUSTOMER_CODE = '') THEN '--' ELSE cast(cd.CUSTOMER_CODE as varchar) END AS CUSTOMER_CODE, "
			+ "CASE WHEN (cd.LPK_NO IS NULL OR cd.LPK_NO = '') THEN '--' ELSE cast(cd.LPK_NO as varchar) END AS LPK_NO, "
			+ "CASE WHEN (cd.NOTES IS NULL OR cd.NOTES = '') THEN '' ELSE cast(cd.NOTES as varchar) END AS NOTES, "
			+ "CASE WHEN (cd.PROMISE_COUNT IS NULL OR cd.PROMISE_COUNT = '') THEN '0' ELSE cast(cd.PROMISE_COUNT as varchar) END AS PROMISE_COUNT, "
			+ "CASE WHEN (cd.PROMISE_AVAILABLE IS NULL OR cd.PROMISE_AVAILABLE = '') THEN '0' ELSE cast(cd.PROMISE_AVAILABLE as varchar) END AS PROMISE_AVAILABLE, "
			+ "CASE WHEN (cd.PROMISE_RANGE IS NULL OR cd.PROMISE_RANGE = '') THEN '0' ELSE cast(cd.PROMISE_RANGE as varchar) END AS PROMISE_RANGE "
			+ "FROM COLLECTION_DETAIL cd "
			+ "INNER JOIN COLLECTION_HEADER ch on ch.ORDER_ID = cd.ORDER_ID " 
			+ "WHERE cd.ORDER_ID = :orderId AND cd.CONTRACT_NO = :contractNo AND cd.INSTALLMENT_NO = :instalmentNo", nativeQuery=true)
	public List<Object[]> selectCollectionDetailForInputHandling(@Param("orderId") int orderId, @Param("contractNo") String contractNo,@Param("instalmentNo") int instalmentNo);

	@Query(value="SELECT " 
			+ "CASE WHEN (cd.CONTRACT_NO IS NULL OR cd.CONTRACT_NO = '') THEN '--' ELSE cast(cd.CONTRACT_NO as varchar) END AS CONTRACT_NO, "
			+ "CASE WHEN (cd.INSTALLMENT_NO IS NULL OR cd.INSTALLMENT_NO = 0) THEN '--' ELSE cast(cd.INSTALLMENT_NO as varchar) END AS INSTALLMENT_NO, "
			+ "CASE WHEN (cd.ORDER_ID IS NULL OR cd.ORDER_ID = '') THEN '--' ELSE cast(cd.ORDER_ID as varchar) END AS ORDER_ID, "
			+ "CASE WHEN (cd.CUSTOMER_CODE IS NULL OR cd.CUSTOMER_CODE = '') THEN '--' ELSE cast(cd.CUSTOMER_CODE as varchar) END AS CUSTOMER_CODE, "
			+ "CASE WHEN (cd.LPK_NO IS NULL OR cd.LPK_NO = '') THEN '--' ELSE cast(cd.LPK_NO as varchar) END AS LPK_NO, "
			+ "CASE WHEN (cd.NOTES IS NULL OR cd.NOTES = '') THEN '' ELSE cast(cd.NOTES as varchar) END AS NOTES, "
			+ "CASE WHEN (cd.PROMISE_COUNT IS NULL OR cd.PROMISE_COUNT = '') THEN '0' ELSE cast(cd.PROMISE_COUNT as varchar) END AS PROMISE_COUNT, "
			+ "CASE WHEN (cd.PROMISE_AVAILABLE IS NULL OR cd.PROMISE_AVAILABLE = '') THEN '0' ELSE cast(cd.PROMISE_AVAILABLE as varchar) END AS PROMISE_AVAILABLE, "
			+ "CASE WHEN (cd.PROMISE_RANGE IS NULL OR cd.PROMISE_RANGE = '') THEN '0' ELSE cast(cd.PROMISE_RANGE as varchar) END AS PROMISE_RANGE, "
			+ "CASE WHEN (cd.TOTAL_AMT IS NULL OR cd.TOTAL_AMT = 0) THEN '0' ELSE cast(cd.TOTAL_AMT as varchar) END AS TOTAL_AMT "
			+ "FROM COLLECTION_DETAIL cd "
			+ "INNER JOIN COLLECTION_HEADER ch on ch.ORDER_ID = cd.ORDER_ID " 
			+ "WHERE cd.ORDER_ID = :orderId AND cd.CONTRACT_NO = :contractNo AND cd.INSTALLMENT_NO = :instalmentNo", nativeQuery=true)
	public List<Object[]> selectCollectionDetailForInputHandling2(@Param("orderId") int orderId, @Param("contractNo") String contractNo,@Param("instalmentNo") int instalmentNo);
	
	@Query(value="SELECT " 
			+ "rm.NAMA_REGION, "
			+ "up.NAMA_KARYAWAN, "
			+ "COUNT(cd.ORDER_ID) AS bucketCollector, "
			+ "CASE WHEN (ch.PREVIOUS_COLLECTOR IS NULL OR ch.PREVIOUS_COLLECTOR = '') THEN '--' ELSE cast(ch.PREVIOUS_COLLECTOR as varchar) END AS PREVIOUS_COLLECTOR "
			+ "ch.ORDER_ID "
			+ "FROM COLLECTION_HEADER ch "
			+ "INNER JOIN COLLECTION_DETAIL cd on cd.ORDER_ID = ch.ORDER_ID "
			+ "INNER JOIN USER_PROFILE up on up.KODE_KOLEKTOR = ch.COLLECTOR_CODE "
			+ "INNER JOIN AKSES_REGION_CABANG arc on arc.USER_ID = up.USER_ID AND arc.KODE_CABANG IS NULL "
			+ "INNER JOIN AKSES_REGION_CABANG arc2 on arc2.USER_ID = up.USER_ID AND arc2.KODE_REGION IS NULL "
			+ "INNER JOIN REGION_MST rm on rm.KODE_REGION = arc.KODE_REGION "
			+ "INNER JOIN CABANG_MST cm on cm.KODE_CABANG = arc2.KODE_CABANG "
			+ "WHERE rm.NAMA_REGION LIKE %:keySearch% OR up.NAMA_KARYAWAN LIKE %keySearch% "
			+ "GROUP BY "
			+ "rm.NAMA_REGION, "
			+ "up.NAMA_KARYAWAN, "
			+ "ch.PREVIOUS_COLLECTOR,"
			+ "ch.ORDER_ID", nativeQuery=true)
	public List<Object[]> selectResultPenugasanUlang(@Param("keySearch") String keySearch);
	
	@Query(value="SELECT " 
			+ "rm.NAMA_REGION, "
			+ "up.NAMA_KARYAWAN, "
			+ "COUNT(cd.ORDER_ID) AS bucketCollector, "
			+ "CASE WHEN (up2.NAMA_KARYAWAN IS NULL OR up2.NAMA_KARYAWAN = '') THEN '--' ELSE cast(up2.NAMA_KARYAWAN as varchar) END AS namaPervious, "
			+ "ch.ORDER_ID "
			+ "FROM COLLECTION_HEADER ch "
			+ "INNER JOIN COLLECTION_DETAIL cd on cd.ORDER_ID = ch.ORDER_ID "
			+ "INNER JOIN USER_PROFILE up on up.KODE_KOLEKTOR = ch.COLLECTOR_CODE "
			+ "INNER JOIN AKSES_REGION_CABANG arc on arc.USER_ID = up.USER_ID AND arc.KODE_CABANG IS NULL "
			+ "INNER JOIN AKSES_REGION_CABANG arc2 on arc2.USER_ID = up.USER_ID AND arc2.KODE_REGION IS NULL "
			+ "INNER JOIN REGION_MST rm on rm.KODE_REGION = arc.KODE_REGION "
			+ "INNER JOIN CABANG_MST cm on cm.KODE_CABANG = arc2.KODE_CABANG "
			+ "LEFT JOIN USER_PROFILE up2 on up2.KODE_KOLEKTOR = ch.PREVIOUS_COLLECTOR "
			+ "WHERE "
			+ "ch.STATUS != 'CMP' AND rm.KODE_REGION = :kodeRegion AND cm.KODE_CABANG = :kodeCabang "
			+ "AND (rm.NAMA_REGION LIKE %:keySearch% OR up.NAMA_KARYAWAN LIKE %:keySearch%) "
			+ "GROUP BY "
			+ "rm.NAMA_REGION, "
			+ "up.NAMA_KARYAWAN, "
			+ "ch.PREVIOUS_COLLECTOR,"
			+ "ch.ORDER_ID,"
			+ "up2.NAMA_KARYAWAN", nativeQuery=true)
	public List<Object[]> selectResultPenugasanUlangKodeRegKodeCab(@Param("kodeRegion") String kodeRegion,@Param("kodeCabang") String kodeCabang,@Param("keySearch") String keySearch);
	
	@Query(value="SELECT " 
			+ "rm.NAMA_REGION, "
			+ "up.NAMA_KARYAWAN, "
			+ "COUNT(cd.ORDER_ID) AS bucketCollector, "
			+ "CASE WHEN (up2.NAMA_KARYAWAN IS NULL OR up2.NAMA_KARYAWAN = '') THEN '--' ELSE cast(up2.NAMA_KARYAWAN as varchar) END AS namaPervious, "
			+ "ch.ORDER_ID "
			+ "FROM COLLECTION_HEADER ch "
			+ "INNER JOIN COLLECTION_DETAIL cd on cd.ORDER_ID = ch.ORDER_ID "
			+ "INNER JOIN USER_PROFILE up on up.KODE_KOLEKTOR = ch.COLLECTOR_CODE "
			+ "INNER JOIN AKSES_REGION_CABANG arc on arc.USER_ID = up.USER_ID AND arc.KODE_CABANG IS NULL "
			+ "INNER JOIN AKSES_REGION_CABANG arc2 on arc2.USER_ID = up.USER_ID AND arc2.KODE_REGION IS NULL "
			+ "INNER JOIN REGION_MST rm on rm.KODE_REGION = arc.KODE_REGION "
			+ "INNER JOIN CABANG_MST cm on cm.KODE_CABANG = arc2.KODE_CABANG "
			+ "LEFT JOIN USER_PROFILE up2 on up2.KODE_KOLEKTOR = ch.PREVIOUS_COLLECTOR "
			+ "WHERE "
			+ "ch.STATUS != 'CMP' AND cm.KODE_CABANG = :kodeCabang "
			+ "AND (rm.NAMA_REGION LIKE %:keySearch% OR up.NAMA_KARYAWAN LIKE %:keySearch%) "
			+ "GROUP BY "
			+ "rm.NAMA_REGION, "
			+ "up.NAMA_KARYAWAN, "
			+ "ch.PREVIOUS_COLLECTOR,"
			+ "ch.ORDER_ID,"
			+ "up2.NAMA_KARYAWAN", nativeQuery=true)
	public List<Object[]> selectResultPenugasanUlangKodeCab(@Param("kodeCabang") String kodeCabang,@Param("keySearch") String keySearch);
	
	@Query(value="SELECT COUNT(ORDER_ID) AS bucketPayment  FROM PAYMENT WHERE ORDER_ID = :orderId", nativeQuery=true)
	public int bucketPayment(@Param("orderId") int orderId);
	
	@Query(value="SELECT COUNT(cd.ORDER_ID) FROM COLLECTION_DETAIL cd "
			+ "INNER JOIN COLLECTION_HEADER ch on ch.ORDER_ID = CD.ORDER_ID "
			+ "WHERE cd.STATUS != 'CMP' AND ch.ORDER_ID =  :orderId", nativeQuery=true)
	public int jumOrderNotCmp(@Param("orderId") int orderId);
	
	@Query(value="SELECT COUNT(ORDER_ID) AS bucketHandling  FROM HANDLING WHERE ORDER_ID = :orderId", nativeQuery=true)
	public int bucketHandling(@Param("orderId") int orderId);
	@Query(value="SELECT " 
			+ "rm.NAMA_REGION, "
			+ "up.NAMA_KARYAWAN, "
			+ "COUNT(cd.ORDER_ID) AS bucketCollector, "
			+ "CASE WHEN (up2.NAMA_KARYAWAN IS NULL OR up2.NAMA_KARYAWAN = '') THEN '--' ELSE cast(up2.NAMA_KARYAWAN as varchar) END AS namaPervious, "
			+ "ch.ORDER_ID "
			+ "FROM COLLECTION_HEADER ch "
			+ "INNER JOIN COLLECTION_DETAIL cd on cd.ORDER_ID = ch.ORDER_ID "
			+ "INNER JOIN USER_PROFILE up on up.KODE_KOLEKTOR = ch.COLLECTOR_CODE "
			+ "INNER JOIN AKSES_REGION_CABANG arc on arc.USER_ID = up.USER_ID AND arc.KODE_CABANG IS NULL "
			+ "INNER JOIN AKSES_REGION_CABANG arc2 on arc2.USER_ID = up.USER_ID AND arc2.KODE_REGION IS NULL "
			+ "INNER JOIN REGION_MST rm on rm.KODE_REGION = arc.KODE_REGION "
			+ "INNER JOIN CABANG_MST cm on cm.KODE_CABANG = arc2.KODE_CABANG "
			+ "LEFT JOIN USER_PROFILE up2 on up2.KODE_KOLEKTOR = ch.PREVIOUS_COLLECTOR "
			+ "WHERE "
			+ "rm.KODE_REGION = :kodeRegional "
			+ "AND (rm.NAMA_REGION LIKE %:keySearch% OR up.NAMA_KARYAWAN LIKE %:keySearch%) "
			+ "GROUP BY "
			+ "rm.NAMA_REGION, "
			+ "up.NAMA_KARYAWAN, "
			+ "ch.PREVIOUS_COLLECTOR,"
			+ "ch.ORDER_ID,"
			+ "up2.NAMA_KARYAWAN", nativeQuery=true)
	public List<Object[]> selectResultPenugasanUlangKodeRegional(@Param("kodeRegional") String kodeRegional,@Param("keySearch") String keySearch);
	
	
	@Query(value="SELECT " 
			+ "rm.NAMA_REGION, "
			+ "up.NAMA_KARYAWAN, "
			+ "COUNT(cd.ORDER_ID) AS bucketCollector, "
			+ "CASE WHEN (up2.NAMA_KARYAWAN IS NULL OR up2.NAMA_KARYAWAN = '') THEN '--' ELSE cast(up2.NAMA_KARYAWAN as varchar) END AS namaPervious, "
			+ "ch.ORDER_ID "
			+ "FROM COLLECTION_HEADER ch "
			+ "INNER JOIN COLLECTION_DETAIL cd on cd.ORDER_ID = ch.ORDER_ID "
			+ "INNER JOIN USER_PROFILE up on up.KODE_KOLEKTOR = ch.COLLECTOR_CODE "
			+ "INNER JOIN AKSES_REGION_CABANG arc on arc.USER_ID = up.USER_ID AND arc.KODE_CABANG IS NULL "
			+ "INNER JOIN AKSES_REGION_CABANG arc2 on arc2.USER_ID = up.USER_ID AND arc2.KODE_REGION IS NULL "
			+ "INNER JOIN REGION_MST rm on rm.KODE_REGION = arc.KODE_REGION "
			+ "INNER JOIN CABANG_MST cm on cm.KODE_CABANG = arc2.KODE_CABANG "
			+ "LEFT JOIN USER_PROFILE up2 on up2.KODE_KOLEKTOR = ch.PREVIOUS_COLLECTOR "
			+ "WHERE "
			+ "ch.STATUS != 'CMP' AND cm.KODE_CABANG = :kodeCab AND ch.COLLECTOR_CODE = :kodeKol "
			+ "AND (rm.NAMA_REGION LIKE %:keySearch% OR up.NAMA_KARYAWAN LIKE %:keySearch%) "
			+ "GROUP BY "
			+ "rm.NAMA_REGION, "
			+ "up.NAMA_KARYAWAN, "
			+ "ch.PREVIOUS_COLLECTOR,"
			+ "ch.ORDER_ID,"
			+ "up2.NAMA_KARYAWAN", nativeQuery=true)
	public List<Object[]> selectResultPenugasanUlangKodeCabKodeKol(@Param("kodeCab") String kodeCab,@Param("kodeKol") String kodeKol,@Param("keySearch") String keySearch);
	
	@Query(value="SELECT " 
			+ "rm.NAMA_REGION, "
			+ "up.NAMA_KARYAWAN, "
			+ "COUNT(cd.ORDER_ID) AS bucketCollector, "
			+ "CASE WHEN (up2.NAMA_KARYAWAN IS NULL OR up2.NAMA_KARYAWAN = '') THEN '--' ELSE cast(up2.NAMA_KARYAWAN as varchar) END AS namaPervious, "
			+ "ch.ORDER_ID "
			+ "FROM COLLECTION_HEADER ch "
			+ "INNER JOIN COLLECTION_DETAIL cd on cd.ORDER_ID = ch.ORDER_ID "
			+ "INNER JOIN USER_PROFILE up on up.KODE_KOLEKTOR = ch.COLLECTOR_CODE "
			+ "INNER JOIN AKSES_REGION_CABANG arc on arc.USER_ID = up.USER_ID AND arc.KODE_CABANG IS NULL "
			+ "INNER JOIN AKSES_REGION_CABANG arc2 on arc2.USER_ID = up.USER_ID AND arc2.KODE_REGION IS NULL "
			+ "INNER JOIN REGION_MST rm on rm.KODE_REGION = arc.KODE_REGION "
			+ "INNER JOIN CABANG_MST cm on cm.KODE_CABANG = arc2.KODE_CABANG "
			+ "LEFT JOIN USER_PROFILE up2 on up2.KODE_KOLEKTOR = ch.PREVIOUS_COLLECTOR"
			+ "WHERE "
			+ "ch.STATUS != 'CMP' AND rm.KODE_REGION = :kodeReg AND ch.COLLECTOR_CODE = :kodeKol "
			+ "AND (rm.NAMA_REGION LIKE %:keySearch% OR up.NAMA_KARYAWAN LIKE %:keySearch%) "
			+ "GROUP BY "
			+ "rm.NAMA_REGION, "
			+ "up.NAMA_KARYAWAN, "
			+ "ch.PREVIOUS_COLLECTOR,"
			+ "ch.ORDER_ID,"
			+ "up2.NAMA_KARYAWAN ", nativeQuery=true)
	public List<Object[]> selectResultPenugasanUlangKodeRegKodeKol(@Param("kodeReg") String kodeReg,@Param("kodeKol") String kodeKol,@Param("keySearch") String keySearch);
	
	
	@Query(value="SELECT "
			+ "CASE WHEN (cd.ORDER_ID IS NULL OR cd.ORDER_ID = '') THEN '--' ELSE cast(cd.ORDER_ID as varchar) END AS ORDER_ID, "
			+ "CASE WHEN (cd.CONTRACT_NO IS NULL OR cd.CONTRACT_NO = '') THEN '--' ELSE cast(cd.CONTRACT_NO as varchar) END AS CONTRACT_NO, "
			+ "CASE WHEN (cd.INSTALLMENT_NO IS NULL OR cd.INSTALLMENT_NO = '') THEN '--' ELSE cast(cd.INSTALLMENT_NO as varchar) END AS INSTALLMENT_NO, "
			+ "CASE WHEN (cd.CUSTOMER_NAME IS NULL OR cd.CUSTOMER_NAME = '') THEN '--' ELSE cast(cd.CUSTOMER_NAME as varchar) END AS CUSTOMER_NAME, "
			+ "CASE WHEN (cd.ADDRESS IS NULL OR cd.ADDRESS = '') THEN '--' ELSE cast(cd.ADDRESS as varchar) END AS ADDRESS, "
			+ "CASE WHEN (ch.LPK_DATE IS NULL OR ch.LPK_DATE = '') THEN '--' ELSE cast(ch.LPK_DATE as varchar) END AS LPK_DATE, "
			+ "CASE WHEN (ch.COLLECTOR_CODE IS NULL OR ch.COLLECTOR_CODE = '') THEN '--' ELSE cast(ch.COLLECTOR_CODE as varchar) END AS COLLECTOR_CODE "
			+ "FROM COLLECTION_DETAIL cd "
			+ "INNER JOIN COLLECTION_HEADER ch ON ch.ORDER_ID = cd.ORDER_ID "
			+ "WHERE "
			+ "cd.ORDER_ID = :orderId AND "
			+ "(cd.ORDER_ID LIKE %:search% OR "
			+ "cd.CONTRACT_NO LIKE %:search% OR "
			+ "cd.INSTALLMENT_NO LIKE %:search% OR "
			+ "cd.CUSTOMER_NAME LIKE %:search% OR "
			+ "cd.ADDRESS LIKE %:search%)", nativeQuery=true)
	public List<Object[]> selectResultPenugasanUlangDetail(@Param("orderId") int orderID,@Param("search") String search);
	
	@Query(value="SELECT "			
			+ "TOP 1 "
			+ "ch.ORDER_ID, "
			+ "CASE WHEN (rc.PERIOD_TO IS NULL OR rc.PERIOD_TO = '') THEN 'NULL' ELSE cast(rc.PERIOD_TO as varchar) END AS PERIOD_TO " 
			+ "FROM COLLECTION_HEADER ch "
			+ "INNER JOIN REASSIGN_COLLECTOR rc on rc.ORDER_ID = ch.ORDER_ID "
			+ "WHERE "
			+ "ch.REASSIGN = '1' AND ch.ORDER_ID = :orderId "
			+ "ORDER BY "
			+ "rc.PERIOD_TO desc ", nativeQuery=true)
	public List<Object[]> selectCollectionHeaderReassignDate(@Param("orderId") int orderID);
	
	@Query(value="SELECT "
			+ "TOP 1 "
			+ "CASE WHEN (rc.COLLECTOR_CODE IS NULL OR rc.COLLECTOR_CODE = '') THEN '--' ELSE cast(rc.COLLECTOR_CODE as varchar) END AS COLLECTOR_CODE, "
			+ "CASE WHEN (rc.REASSIGN_COLLECTOR_CODE IS NULL OR rc.REASSIGN_COLLECTOR_CODE = '') THEN '--' ELSE cast(rc.REASSIGN_COLLECTOR_CODE as varchar) END AS REASSIGN_COLLECTOR_CODE, "
			+ "CASE WHEN (rc.PERIOD_FROM IS NULL OR rc.PERIOD_FROM = '') THEN '--' ELSE cast(rc.PERIOD_FROM as varchar) END AS PERIOD_FROM, "
			+ "CASE WHEN (rc.PERIOD_TO IS NULL OR rc.PERIOD_TO = '') THEN '--' ELSE cast(rc.PERIOD_TO as varchar) END AS PERIOD_TO "
			+ "FROM REASSIGN_COLLECTOR rc " 
			+ "where rc.REASSIGN_COLLECTOR_CODE = :kodeKOlektor "
			+ "ORDER BY rc.PERIOD_TO desc ", nativeQuery=true)
	public List<Object[]> selectReassignCollectorByCollectorCode(@Param("kodeKOlektor") String kodeKOlektor);
}
