package dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import dto.MasterHandlingDto;
import entity.MasterHandling;

public interface MasterHandlingDao extends JpaRepository<MasterHandling, Serializable>{

	@Query(value="SELECT " 
			+ "case when (MH.BATCH_HANDLING_NO is null or MH.BATCH_HANDLING_NO='') then '--' "
				+ "else cast(MH.BATCH_HANDLING_NO as varchar(10)) end as BATCH, "
			+ "case when (MH.HANDLING_CODE is null or MH.HANDLING_CODE='') then '--' "
				+ "else cast(MH.HANDLING_CODE as varchar(70)) end as CODEHAND, "
			+ "case when (MH.PROMISE_FLAG is null or MH.PROMISE_FLAG='') then '--' "
				+ "else cast(MH.PROMISE_FLAG as VARCHAR(1)) end as PROMISE, "
			+ "case when (MH.DESKRIPSI_HANDLING is null or MH.DESKRIPSI_HANDLING='') then '--' "
				+ "else cast(MH.DESKRIPSI_HANDLING as varchar(250)) end as DESKRIPSI, "
			+ "case when(MH.AKTIF_STATUS is null or MH.AKTIF_STATUS='') then '--' "
				+ "else cast(MH.AKTIF_STATUS as VARCHAR(1)) end as AKTIF "
			+ "FROM MASTER_HANDLING MH "
			+ "where MH.BATCH_HANDLING_NO = :batchHand AND (MH.BATCH_HANDLING_NO like %:search% or "
			+ "MH.HANDLING_CODE like %:search%) " , nativeQuery=true)
	public List<Object[]> findAllHandling(@Param("search")String search,@Param("batchHand")int batchHand);
	
	@Query(value="SELECT " 
			+ "case when (MH.BATCH_HANDLING_NO is null or MH.BATCH_HANDLING_NO='') then '--' "
				+ "else cast(MH.BATCH_HANDLING_NO as varchar(10)) end as BATCH, "
			+ "case when (MH.HANDLING_CODE is null or MH.HANDLING_CODE='') then '--' "
				+ "else cast(MH.HANDLING_CODE as varchar(70)) end as CODEHAND, "
			+ "case when (MH.PROMISE_FLAG is null or MH.PROMISE_FLAG='') then '--' "
				+ "else cast(MH.PROMISE_FLAG as VARCHAR(1)) end as PROMISE, "
			+ "case when (MH.DESKRIPSI_HANDLING is null or MH.DESKRIPSI_HANDLING='') then '--' "
				+ "else cast(MH.DESKRIPSI_HANDLING as varchar(250)) end as DESKRIPSI, "
			+ "case when(MH.AKTIF_STATUS is null or MH.AKTIF_STATUS='') then '--' "
				+ "else cast(MH.AKTIF_STATUS as VARCHAR(1)) end as AKTIF "
			+ "FROM MASTER_HANDLING MH "
			+ "where MH.BATCH_HANDLING_NO = :batchHandling " , nativeQuery=true)
	public List<Object[]> findAllHandlingByBatchHandling(@Param("batchHandling")int batchHandling);
	
	@Query("SELECT " 
			+ "MH "
			+ "FROM MasterHandling MH "
			+ "where MH.batchHandlingNo=:handNo" )
	public MasterHandling findOneHandling(@Param("handNo")int handNo);
	
	@Modifying
	@Transactional
	@Query(value="insert "
			+ "into MASTER_HANDLING "
			+ "(BATCH_HANDLING_NO,HANDLING_CODE, PROMISE_FLAG, "
			+ "DESKRIPSI_HANDLING, AKTIF_STATUS, CREATE_USER, CREATE_DATE) "
			+ "VALUES"
			+ "(:batchHandling, :handCode, :promise, :deskripsi, :aktif, "
			+ ":createUser, getdate())", nativeQuery=true)
	public void insertHandling(@Param("batchHandling") int batchHandling,@Param("handCode") String handCode,  
										@Param("promise") String promise, 
										@Param("deskripsi") String deskripsi, 
										@Param("aktif") String aktif, 
										@Param("createUser") String createUser);
	
	@Modifying
	@Transactional
	@Query(value="update "
			+ "MASTER_HANDLING "
			+ "set "
			+ "HANDLING_CODE =:handCode, "
			+ "PROMISE_FLAG =:promise, "
			+ "DESKRIPSI_HANDLING =:deskripsi, "
			+ "AKTIF_STATUS =:aktif, "
			+ "MODIFY_USER =:modifyUser, "
			+ "MODIFY_DATE = GETDATE() "
			+ "where BATCH_HANDLING_NO =:handNo", nativeQuery=true)
	public void updateHandling(@Param("handCode") String handCode,
										@Param("promise") String promise,  
										@Param("deskripsi") String deskripsi, 
										@Param("aktif") String aktif, 
										@Param("modifyUser") String modifyUser, 
										@Param("handNo") int handNo);
	
	@Modifying
	@Transactional
	@Query(value="update "
			+ "MASTER_HANDLING "
			+ "set "
			+ "AKTIF_STATUS = '0', "
			+ "MODIFY_USER =:modifyUser, "
			+ "MODIFY_DATE = getDate()  "
			+ "where BATCH_HANDLING_NO =:handNo AND HANDLING_CODE = :handCode ", nativeQuery=true)
	public void deleteHandling(@Param("modifyUser") String modifyUser, 
										@Param("handNo") int handNo,
										@Param("handCode") String handCode);
	
	
	@Query(value="SELECT MAX(BATCH_HANDLING_NO) FROM MASTER_HANDLING", nativeQuery=true)
	public int maxBatchHandling();
}