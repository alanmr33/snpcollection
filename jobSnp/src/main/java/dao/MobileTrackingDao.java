package dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import entity.MobileTracking;
import entity.MobileTrackingPK;

public interface MobileTrackingDao extends JpaRepository<MobileTracking, MobileTrackingPK>{
	@Query(value="SELECT "
			+ "CASE WHEN (mt.USER_ID IS NULL OR mt.USER_ID = '') THEN 'mt.USER_ID' ELSE cast(mt.USER_ID as varchar) END AS USER_ID, " 
			+ "CASE WHEN (up.NAMA_KARYAWAN IS NULL OR up.NAMA_KARYAWAN = '') THEN 'mt.USER_ID' ELSE cast(up.NAMA_KARYAWAN as varchar) END AS NAMA_KARYAWAN, " 
			+ "CASE WHEN (rm.NAMA_REGION IS NULL OR rm.NAMA_REGION = '') THEN '--' ELSE cast(rm.NAMA_REGION as varchar) END AS NAMA_REGION, "
			+ "CASE WHEN (cm.NAMA_CABANG IS NULL OR cm.NAMA_CABANG = '') THEN '--' ELSE cast(cm.NAMA_CABANG as varchar) END AS NAMA_CABAMG "
			+ "FROM MOBILE_TRACKING mt "
			+ "INNER JOIN USER_PROFILE up on up.USER_ID = mt.USER_ID "
			+ "LEFT JOIN AKSES_REGION_CABANG arb on arb.USER_ID = mt.USER_ID and arb.KODE_REGION is null "
			+ "LEFT JOIN CABANG_MST cm on cm.KODE_CABANG = arb.KODE_CABANG "
			+ "LEFT JOIN REGION_MST rm on rm.KODE_REGION = cm.KODE_REGION "
			+ "WHERE cm.KODE_CABANG = :kodeCabang AND mt.TRACKING_DATE >= :dateAwal and mt.TRACKING_DATE <= :dateAkhir AND "
			+ "(mt.USER_ID LIKE %:keySearch% OR up.NAMA_KARYAWAN LIKE %:keySearch% OR rm.NAMA_REGION LIKE %:keySearch% OR cm.NAMA_CABANG LIKE %:keySearch%) "
			+ "GROUP by mt.USER_ID, up.NAMA_KARYAWAN, rm.NAMA_REGION, cm.NAMA_CABANG", nativeQuery=true)
	public List<Object[]> findMobileTrackingAllByCabang(@Param("kodeCabang")String kodeCabang,
			@Param("dateAwal")Date dateAwal,
			@Param("dateAkhir")Date dateAkhir,
			@Param("keySearch")String keySearch);
	
	
	
	@Query(value="SELECT "
			+ "CASE WHEN (mt.USER_ID IS NULL OR mt.USER_ID = '') THEN 'mt.USER_ID' ELSE cast(mt.USER_ID as varchar) END AS USER_ID, " 
			+ "CASE WHEN (up.NAMA_KARYAWAN IS NULL OR up.NAMA_KARYAWAN = '') THEN 'mt.USER_ID' ELSE cast(up.NAMA_KARYAWAN as varchar) END AS NAMA_KARYAWAN, " 
			+ "CASE WHEN (rm.NAMA_REGION IS NULL OR rm.NAMA_REGION = '') THEN '--' ELSE cast(rm.NAMA_REGION as varchar) END AS NAMA_REGION, "
			+ "CASE WHEN (cm.NAMA_CABANG IS NULL OR cm.NAMA_CABANG = '') THEN '--' ELSE cast(cm.NAMA_CABANG as varchar) END AS NAMA_CABAMG "
			+ "FROM MOBILE_TRACKING mt "
			+ "INNER JOIN USER_PROFILE up on up.USER_ID = mt.USER_ID "
			+ "LEFT JOIN AKSES_REGION_CABANG arb on arb.USER_ID = mt.USER_ID and arb.KODE_REGION is null "
			+ "LEFT JOIN CABANG_MST cm on cm.KODE_CABANG = arb.KODE_CABANG "
			+ "LEFT JOIN REGION_MST rm on rm.KODE_REGION = cm.KODE_REGION "
			+ "WHERE cm.KODE_CABANG = :kodeCabang AND mt.USER_ID = :userId AND mt.TRACKING_DATE >= :dateAwal and mt.TRACKING_DATE <= :dateAkhir AND "
			+ "(mt.USER_ID LIKE %:keySearch% OR up.NAMA_KARYAWAN LIKE %:keySearch% OR rm.NAMA_REGION LIKE %:keySearch% OR cm.NAMA_CABANG LIKE %:keySearch%)"
			+ "GROUP by mt.USER_ID, up.NAMA_KARYAWAN, rm.NAMA_REGION, cm.NAMA_CABANG",nativeQuery=true)
	public List<Object[]> findMobileTrackingByKodeCabangUserId(@Param("kodeCabang")String kodeCabang,@Param("userId")String userId,
			@Param("dateAwal")Date dateAwal,
			@Param("dateAkhir")Date dateAkhir,
			@Param("keySearch")String keySearch);
	
	@Query(value="SELECT "
			+ "CASE WHEN (mt.USER_ID IS NULL OR mt.USER_ID = '') THEN 'mt.USER_ID' ELSE cast(mt.USER_ID as varchar) END AS USER_ID, " 
			+ "CASE WHEN (up.NAMA_KARYAWAN IS NULL OR up.NAMA_KARYAWAN = '') THEN 'mt.USER_ID' ELSE cast(up.NAMA_KARYAWAN as varchar) END AS NAMA_KARYAWAN, " 
			+ "CASE WHEN (rm.NAMA_REGION IS NULL OR rm.NAMA_REGION = '') THEN '--' ELSE cast(rm.NAMA_REGION as varchar) END AS NAMA_REGION, "
			+ "CASE WHEN (cm.NAMA_CABANG IS NULL OR cm.NAMA_CABANG = '') THEN '--' ELSE cast(cm.NAMA_CABANG as varchar) END AS NAMA_CABAMG "
			+ "FROM MOBILE_TRACKING mt "
			+ "INNER JOIN USER_PROFILE up on up.USER_ID = mt.USER_ID "
			+ "LEFT JOIN AKSES_REGION_CABANG arb on arb.USER_ID = mt.USER_ID and arb.KODE_REGION is null "
			+ "LEFT JOIN CABANG_MST cm on cm.KODE_CABANG = arb.KODE_CABANG "
			+ "LEFT JOIN REGION_MST rm on rm.KODE_REGION = cm.KODE_REGION "
			+ "WHERE rm.KODE_REGION = :kodeRegion AND mt.USER_ID = :userId AND mt.TRACKING_DATE >= :dateAwal and mt.TRACKING_DATE <= :dateAkhir AND "
			+ "(mt.USER_ID LIKE %:keySearch% OR up.NAMA_KARYAWAN LIKE %:keySearch% OR rm.NAMA_REGION LIKE %:keySearch% OR cm.NAMA_CABANG LIKE %:keySearch%)"
			+ "GROUP by mt.USER_ID, up.NAMA_KARYAWAN, rm.NAMA_REGION, cm.NAMA_CABANG",nativeQuery=true)
	public List<Object[]> findMobileTrackingByKodeRegionUserId(@Param("kodeRegion")String kodeRegion,@Param("userId")String userId,
			@Param("dateAwal")Date dateAwal,
			@Param("dateAkhir")Date dateAkhir,
			@Param("keySearch")String keySearch);
	
	@Query(value="SELECT "
			+ "CASE WHEN (mt.USER_ID IS NULL OR mt.USER_ID = '') THEN 'mt.USER_ID' ELSE cast(mt.USER_ID as varchar) END AS USER_ID, " 
			+ "CASE WHEN (up.NAMA_KARYAWAN IS NULL OR up.NAMA_KARYAWAN = '') THEN 'mt.USER_ID' ELSE cast(up.NAMA_KARYAWAN as varchar) END AS NAMA_KARYAWAN, " 
			+ "CASE WHEN (rm.NAMA_REGION IS NULL OR rm.NAMA_REGION = '') THEN '--' ELSE cast(rm.NAMA_REGION as varchar) END AS NAMA_REGION, "
			+ "CASE WHEN (cm.NAMA_CABANG IS NULL OR cm.NAMA_CABANG = '') THEN '--' ELSE cast(cm.NAMA_CABANG as varchar) END AS NAMA_CABAMG "
			+ "FROM MOBILE_TRACKING mt "
			+ "INNER JOIN USER_PROFILE up on up.USER_ID = mt.USER_ID "
			+ "LEFT JOIN AKSES_REGION_CABANG arb on arb.USER_ID = mt.USER_ID and arb.KODE_REGION is null "
			+ "LEFT JOIN CABANG_MST cm on cm.KODE_CABANG = arb.KODE_CABANG "
			+ "LEFT JOIN REGION_MST rm on rm.KODE_REGION = cm.KODE_REGION "
			+ "WHERE rm.KODE_REGION = :kodeRegion AND cm.KODE_CABANG = :kodeCabang AND mt.TRACKING_DATE >= :dateAwal and mt.TRACKING_DATE <= :dateAkhir AND "
			+ "(mt.USER_ID LIKE %:keySearch% OR up.NAMA_KARYAWAN LIKE %:keySearch% OR rm.NAMA_REGION LIKE %:keySearch% OR cm.NAMA_CABANG LIKE %:keySearch%)"
			+ "GROUP by mt.USER_ID, up.NAMA_KARYAWAN, rm.NAMA_REGION, cm.NAMA_CABANG",nativeQuery=true)
	public List<Object[]> findMobileTrackingByKodeRegionKodeCang(@Param("kodeRegion")String kodeRegion,@Param("kodeCabang")String kodeCabang,
			@Param("dateAwal")Date dateAwal,
			@Param("dateAkhir")Date dateAkhir,
			@Param("keySearch")String keySearch);
	
	@Query(value="SELECT "
			+ "CASE WHEN (mt.USER_ID IS NULL OR mt.USER_ID = '') THEN '--' ELSE cast(mt.USER_ID as varchar) END AS USER_ID, "
			+ "CASE WHEN (up.NAMA_KARYAWAN IS NULL OR up.NAMA_KARYAWAN = '') THEN '--' ELSE cast(up.NAMA_KARYAWAN as varchar) END AS NAMA_KARYAWAN, "
			+ "CASE WHEN (rm.NAMA_REGION IS NULL OR rm.NAMA_REGION = '') THEN '--' ELSE cast(rm.NAMA_REGION as varchar) END AS NAMA_REGION, "
			+ "CASE WHEN (cm.NAMA_CABANG IS NULL OR cm.NAMA_CABANG = '') THEN '--' ELSE cast(cm.NAMA_CABANG as varchar) END AS NAMA_CABAMG, "
			+ "CASE WHEN (mt.TRACKING_DATE IS NULL OR mt.TRACKING_DATE = '') THEN '--' ELSE cast(mt.TRACKING_DATE as varchar) END AS TRACKING_DATE, "
			+ "CASE WHEN (mt.LOCATION IS NULL OR mt.LOCATION = '') THEN '--' ELSE cast(mt.LOCATION as varchar) END AS LOCATION, "
			+ "CASE WHEN (mt.ORDER_ID IS NULL OR mt.ORDER_ID = '') THEN '--' ELSE cast(mt.ORDER_ID as varchar) END AS ORDER_ID "
			+ "FROM MOBILE_TRACKING mt "
			+ "INNER JOIN USER_PROFILE up on up.USER_ID = mt.USER_ID "
			+ "LEFT JOIN AKSES_REGION_CABANG arb on arb.USER_ID = mt.USER_ID and arb.KODE_REGION is null "
			+ "LEFT JOIN CABANG_MST cm on cm.KODE_CABANG = arb.KODE_CABANG "
			+ "LEFT JOIN REGION_MST rm on rm.KODE_REGION = cm.KODE_REGION "
			+ "WHERE mt.ORDER_ID is null and mt.USER_ID = :userId AND mt.TRACKING_DATE >= :dateAwal and mt.TRACKING_DATE <= :dateAkhir ", nativeQuery=true)
	public List<Object[]> findMobileTrackingByUserIdNotOrderId(@Param("userId")String userId,
			@Param("dateAwal")Date dateAwal,
			@Param("dateAkhir")Date dateAkhir);
	
	@Query(value="SELECT "
			+ "CASE WHEN (mt.USER_ID IS NULL OR mt.USER_ID = '') THEN '--' ELSE cast(mt.USER_ID as varchar) END AS USER_ID, "
			+ "CASE WHEN (up.NAMA_KARYAWAN IS NULL OR up.NAMA_KARYAWAN = '') THEN '--' ELSE cast(up.NAMA_KARYAWAN as varchar) END AS NAMA_KARYAWAN, "
			+ "CASE WHEN (rm.NAMA_REGION IS NULL OR rm.NAMA_REGION = '') THEN '--' ELSE cast(rm.NAMA_REGION as varchar) END AS NAMA_REGION, "
			+ "CASE WHEN (cm.NAMA_CABANG IS NULL OR cm.NAMA_CABANG = '') THEN '--' ELSE cast(cm.NAMA_CABANG as varchar) END AS NAMA_CABAMG, "
			+ "CASE WHEN (mt.TRACKING_DATE IS NULL OR mt.TRACKING_DATE = '') THEN '--' ELSE cast(mt.TRACKING_DATE as varchar) END AS TRACKING_DATE, "
			+ "CASE WHEN (mt.LOCATION IS NULL OR mt.LOCATION = '') THEN '--' ELSE cast(mt.LOCATION as varchar) END AS LOCATION, "
			+ "CASE WHEN (mt.ACTIVITY_ID IS NULL OR mt.ACTIVITY_ID = '') THEN '--' ELSE cast(mt.ACTIVITY_ID as varchar) END AS ACTIVITY_ID "
			+ "FROM MOBILE_TRACKING mt "
			+ "INNER JOIN USER_PROFILE up on up.USER_ID = mt.USER_ID "
			+ "LEFT JOIN AKSES_REGION_CABANG arb on arb.USER_ID = mt.USER_ID and arb.KODE_REGION is null "
			+ "LEFT JOIN CABANG_MST cm on cm.KODE_CABANG = arb.KODE_CABANG "
			+ "LEFT JOIN REGION_MST rm on rm.KODE_REGION = cm.KODE_REGION "
			+ "WHERE mt.USER_ID = :userId AND mt.TRACKING_DATE >= :dateAwal and mt.TRACKING_DATE <= :dateAkhir ", nativeQuery=true)
	public List<Object[]> findMobileTrackingByUserIdWithOrderId(@Param("userId")String userId,
			@Param("dateAwal")Date dateAwal,
			@Param("dateAkhir")Date dateAkhir);
}
