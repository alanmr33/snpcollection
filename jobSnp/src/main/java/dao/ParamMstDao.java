package dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import entity.CabangMst;
import entity.ParamMst;
import entity.ParamMstPK;


public interface ParamMstDao extends JpaRepository<ParamMst, ParamMstPK>{
	
	@Modifying
	@Transactional
	@Query(value="INSERT "
			+ "INTO PARAM_MST "
			+ "(PARAM_ID, CONDITION, LEVEL_PARAM, PARENT_ID) "
			+ "VALUES(:paramId, :condition, :levelParam, :parentId)", nativeQuery=true)
	public void saveParam(@Param("paramId") String paramId, 
						  @Param("condition") String condition, 
						  @Param("levelParam") String levelParam, 
						  @Param("parentId") String parentId);
	
	@Modifying
	@Transactional
	@Query(value="UPDATE "
			+ "PARAM_MST "
			+ "SET "
			+ "CONDITION =:condition, "
			+ "LEVEL_PARAM =:levelParam, "
			+ "PARENT_ID =:parentId "
			+ "where PARAM_ID =:paramId", nativeQuery=true)
	public void updateParam(@Param("condition") String condition,
							@Param("levelParam") String levelParam,
							@Param("parentId") String parentId,
							@Param("paramId") String paramId);
	
	@Modifying
	@Transactional
	@Query(value="DELETE PARAM_MST WHERE PARAM_ID = :paramId ", nativeQuery=true)
	public void deleteParam(@Param("paramId") String paramId);
	
	@Query(value="SELECT "
			+ "CASE WHEN (pm.PARAM_ID IS NULL OR pm.PARAM_ID = '') THEN '--' ELSE cast(pm.PARAM_ID as varchar) END AS PARAM_ID, "
			+ "CASE WHEN (pm.CONDITION IS NULL OR pm.CONDITION = '') THEN '--' ELSE cast(pm.CONDITION as varchar) END AS CONDITION, "
			+ "CASE WHEN (pm.LEVEL_PARAM IS NULL OR pm.LEVEL_PARAM = '') THEN '--' ELSE cast(pm.LEVEL_PARAM as varchar) END AS LEVEL_PARAM, "
			+ "CASE WHEN (pm.PARENT_ID IS NULL OR pm.PARENT_ID = '') THEN '0' ELSE cast(pm.PARENT_ID as varchar) END PARENT_ID "
			+ "FROM PARAM_MST pm "
		    + "WHERE pm.PARAM_ID LIKE %:keySearch% OR pm.CONDITION LIKE %:keySearch% OR pm.LEVEL_PARAM LIKE %:keySearch% "
		    + "OR pm.PARENT_ID LIKE %:keySearch% ", nativeQuery=true)
	public List<Object[]> findParamWithSearch(@Param("keySearch")String keySearch);
	
	@Query(value="SELECT "
			+ "CASE WHEN (pm.PARAM_ID IS NULL OR pm.PARAM_ID = '') THEN '--' ELSE cast(pm.PARAM_ID as varchar) END AS PARAM_ID, "
			+ "CASE WHEN (pm.CONDITION IS NULL OR pm.CONDITION = '') THEN '--' ELSE cast(pm.CONDITION as varchar) END AS CONDITION, "
			+ "CASE WHEN (pm.LEVEL_PARAM IS NULL OR pm.LEVEL_PARAM = '') THEN '--' ELSE cast(pm.LEVEL_PARAM as varchar) END AS LEVEL_PARAM, "
			+ "CASE WHEN (pm.PARENT_ID IS NULL OR pm.PARENT_ID = '') THEN '--' ELSE cast(pm.PARENT_ID as varchar) END PARENT_ID "
			+ "FROM PARAM_MST pm "
		    + "WHERE pm.PARAM_ID = :paramId", nativeQuery=true)
	public List<Object[]> findOneParam(@Param("paramId")String paramId);

}
