package dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import antlr.debug.Event;
import entity.CabangMst;
import entity.CabangMstPK;
import entity.Events;
import entity.EventsPk;
import entity.Forms;
import entity.FormsPk;


public interface EventsDao extends JpaRepository<Events, EventsPk>{
	
	@Query(value="SELECT " 
				+ "CASE WHEN (e.event_id IS NULL OR e.event_id = '') THEN '--' ELSE cast(e.event_id as varchar) END AS event_id, "
				+ "CASE WHEN (e.event_type IS NULL OR e.event_type = '') THEN '--' ELSE cast(e.event_type as varchar) END AS event_type, "
				+ "CASE WHEN (e.event_component IS NULL OR e.event_component = '') THEN '--' ELSE cast(e.event_component as varchar) END AS event_component, "
				+ "CASE WHEN (e.event_component_target IS NULL OR e.event_component_target = '') THEN '--' ELSE cast(e.event_component_target as varchar) END AS event_component_target, "
				+ "CASE WHEN (e.event_data IS NULL OR e.event_data = '') THEN '--' ELSE cast(e.event_data as varchar) END AS event_data, "
				+ "CASE WHEN (e.form_id IS NULL OR e.form_id = '') THEN '--' ELSE cast(e.form_id as varchar) END AS form_id "
				+ "FROM EVENTS e "
				+ "WHERE "
				+ "e.event_id LIKE %:keySearch% OR "
				+ "e.event_type LIKE %:keySearch% OR "
				+ "e.event_component LIKE %:keySearch% OR "
				+ "e.event_component_target LIKE %:keySearch% OR "
				+ "e.event_data LIKE %:keySearch% OR "
				+ "e.form_id LIKE %:keySearch% ", nativeQuery=true)
	public List<Object[]> findEventsWithSearch(@Param("keySearch")String keySearch);
	
	@Query(value="SELECT " 
			+ "CASE WHEN (e.event_id IS NULL OR e.event_id = '') THEN '--' ELSE cast(e.event_id as varchar) END AS event_id, "
			+ "CASE WHEN (e.event_type IS NULL OR e.event_type = '') THEN '--' ELSE cast(e.event_type as varchar) END AS event_type, "
			+ "CASE WHEN (e.event_component IS NULL OR e.event_component = '') THEN '--' ELSE cast(e.event_component as varchar) END AS event_component, "
			+ "CASE WHEN (e.event_component_target IS NULL OR e.event_component_target = '') THEN '--' ELSE cast(e.event_component_target as varchar) END AS event_component_target, "
			+ "CASE WHEN (e.event_data IS NULL OR e.event_data = '') THEN '--' ELSE cast(e.event_data as varchar) END AS event_data, "
			+ "CASE WHEN (e.form_id IS NULL OR e.form_id = '') THEN '--' ELSE cast(e.form_id as varchar) END AS form_id, "
			+ "FROM EVENTS e "
			+ "WHERE e.event_id = :keySearch", nativeQuery=true)
public List<Object[]> findEventsById(@Param("keySearch")String keySearch);
	
	
	
	@Modifying
	@Transactional
	@Query(value="INSERT "
			+ "INTO EVENTS "
			+ "(event_id, event_type, event_component, event_component_target, event_data, form_id) "
			+ "VALUES(:eventId, :eventType, :eventComponent, :eventComponenTarget, :eventData, :formId)", nativeQuery=true)
	public void saveEvents(@Param("eventId") String eventId,
						  @Param("eventType") String eventType, 
						  @Param("eventComponent") String eventComponent,
						  @Param("eventComponenTarget") String eventComponenTarget,
						  @Param("eventData") String eventData,
						  @Param("formId") String formId
						  );
	
	@Modifying
	@Transactional
	@Query(value="UPDATE "
			+ "EVENTS "
			+ "SET "
			+ "event_type =:eventType, "
			+ "event_component =:eventComponent, "
			+ "event_component_target =:eventComponenTarget, "
			+ "event_data =:eventData, "
			+ "form_id =:formId "
			+ "where event_id =:eventId", nativeQuery=true)
	public void updateEvents(
			  @Param("eventType") String eventType, 
			  @Param("eventComponent") String eventComponent,
			  @Param("eventComponenTarget") String eventComponenTarget,
			  @Param("eventData") String eventData,
			  @Param("formId") String formId,
			  @Param("eventId") String eventId
							 );
	
	@Modifying
	@Transactional
	@Query(value="DELETE EVENTS WHERE event_id = :eventId", nativeQuery=true)
	public void deleteEvents(@Param("eventId") String eventId);
	
}
	

