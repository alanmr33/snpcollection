package dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import antlr.debug.Event;
import entity.CabangMst;
import entity.CabangMstPK;
import entity.Events;
import entity.EventsPk;
import entity.FormFields;
import entity.FormFieldsPk;
import entity.Forms;
import entity.FormsPk;


public interface FormFieldsDao extends JpaRepository<FormFields, FormFieldsPk>{
	
	@Query(value="SELECT "
				+ "CASE WHEN (ff.field_id IS NULL OR ff.field_id = '') THEN '--' ELSE cast(ff.field_id as varchar) END AS field_id, "
				+ "CASE WHEN (ff.field_name IS NULL OR ff.field_name = '') THEN '--' ELSE cast(ff.field_name as varchar) END AS field_name, "
				+ "CASE WHEN (ff.field_type IS NULL OR ff.field_type = '') THEN '--' ELSE cast(ff.field_type as varchar) END AS field_type, "
				+ "CASE WHEN (ff.field_visibility IS NULL OR ff.field_visibility = '') THEN '--' ELSE cast(ff.field_visibility as varchar) END AS field_visibility, "
				+ "CASE WHEN (ff.field_label IS NULL OR ff.field_label = '') THEN '--' ELSE cast(ff.field_label as varchar) END AS field_label, "
				+ "CASE WHEN (ff.field_global_value IS NULL OR ff.field_global_value = '') THEN '--' ELSE cast(ff.field_global_value as varchar) END AS field_global_value, "
				+ "CASE WHEN (ff.field_weight IS NULL OR ff.field_weight = '') THEN '--' ELSE cast(ff.field_weight as varchar) END AS field_weight, "
				+ "CASE WHEN (ff.field_store IS NULL OR ff.field_store = '') THEN '--' ELSE cast(ff.field_store as varchar) END AS field_store, "
				+ "CASE WHEN (ff.field_extra IS NULL OR ff.field_extra = '') THEN '--' ELSE cast(ff.field_extra as varchar) END AS field_extra, "
				+ "CASE WHEN (ff.row_id IS NULL OR ff.row_id = '') THEN '--' ELSE cast(ff.row_id as varchar) END AS row_id "
				+ "FROM "
				+ "FORM_FIELDS ff "
				+ "WHERE "
				+ "ff.field_id LIKE %:keySearch% OR "
				+ "ff.field_name LIKE %:keySearch% OR "
				+ "ff.field_type LIKE %:keySearch% OR "
				+ "ff.field_visibility LIKE %:keySearch% OR "
				+ "ff.field_label LIKE %:keySearch% OR "
				+ "ff.field_global_value LIKE %:keySearch% OR "
				+ "ff.field_weight LIKE %:keySearch% OR "
				+ "ff.field_store LIKE %:keySearch% OR "
				+ "ff.field_extra LIKE %:keySearch% OR "
				+ "ff.row_id LIKE %:keySearch% ", nativeQuery=true)
	public List<Object[]> findFormFieldsWithSearch(@Param("keySearch")String keySearch);
	
	@Query(value="SELECT "
			+ "CASE WHEN (ff.field_id IS NULL OR ff.field_id = '') THEN '--' ELSE cast(ff.field_id as varchar) END AS field_id, "
			+ "CASE WHEN (ff.field_name IS NULL OR ff.field_name = '') THEN '--' ELSE cast(ff.field_name as varchar) END AS field_name, "
			+ "CASE WHEN (ff.field_type IS NULL OR ff.field_type = '') THEN '--' ELSE cast(ff.field_type as varchar) END AS field_type, "
			+ "CASE WHEN (ff.field_visibility IS NULL OR ff.field_visibility = '') THEN '--' ELSE cast(ff.field_visibility as varchar) END AS field_visibility, "
			+ "CASE WHEN (ff.field_label IS NULL OR ff.field_label = '') THEN '--' ELSE cast(ff.field_label as varchar) END AS field_label, "
			+ "CASE WHEN (ff.field_global_value IS NULL OR ff.field_global_value = '') THEN '--' ELSE cast(ff.field_global_value as varchar) END AS field_global_value, "
			+ "CASE WHEN (ff.field_weight IS NULL OR ff.field_weight = '') THEN '--' ELSE cast(ff.field_weight as varchar) END AS field_weight, "
			+ "CASE WHEN (ff.field_store IS NULL OR ff.field_store = '') THEN '--' ELSE cast(ff.field_store as varchar) END AS field_store, "
			+ "CASE WHEN (ff.field_extra IS NULL OR ff.field_extra = '') THEN '--' ELSE cast(ff.field_extra as varchar) END AS field_extra, "
			+ "CASE WHEN (ff.row_id IS NULL OR ff.row_id = '') THEN '--' ELSE cast(ff.row_id as varchar) END AS row_id "
			+ "FROM "
			+ "FORM_FIELDS ff "
			+ "WHERE "
			+ "ff.row_id = :keySearch ", nativeQuery=true)
public List<Object[]> findFormFieldsByRowId(@Param("keySearch")String keySearch);
	
	@Query(value="SELECT "
			+ "CASE WHEN (ff.field_id IS NULL OR ff.field_id = '') THEN '--' ELSE cast(ff.field_id as varchar) END AS field_id, "
			+ "CASE WHEN (ff.field_name IS NULL OR ff.field_name = '') THEN '--' ELSE cast(ff.field_name as varchar) END AS field_name, "
			+ "CASE WHEN (ff.field_type IS NULL OR ff.field_type = '') THEN '--' ELSE cast(ff.field_type as varchar) END AS field_type, "
			+ "CASE WHEN (ff.field_visibility IS NULL OR ff.field_visibility = '') THEN '--' ELSE cast(ff.field_visibility as varchar) END AS field_visibility, "
			+ "CASE WHEN (ff.field_label IS NULL OR ff.field_label = '') THEN '--' ELSE cast(ff.field_label as varchar) END AS field_label, "
			+ "CASE WHEN (ff.field_global_value IS NULL OR ff.field_global_value = '') THEN '--' ELSE cast(ff.field_global_value as varchar) END AS field_global_value, "
			+ "CASE WHEN (ff.field_weight IS NULL OR ff.field_weight = '') THEN '--' ELSE cast(ff.field_weight as varchar) END AS field_weight, "
			+ "CASE WHEN (ff.field_store IS NULL OR ff.field_store = '') THEN '--' ELSE cast(ff.field_store as varchar) END AS field_store, "
			+ "CASE WHEN (ff.field_extra IS NULL OR ff.field_extra = '') THEN '--' ELSE cast(ff.field_extra as varchar) END AS field_extra, "
			+ "CASE WHEN (ff.row_id IS NULL OR ff.row_id = '') THEN '--' ELSE cast(ff.row_id as varchar) END AS row_id "
			+ "FROM "
			+ "FORM_FIELDS ff "
			+ "WHERE "
			+ "ff.field_id = :keySearch", nativeQuery=true)
public List<Object[]> findFormFieldsById(@Param("keySearch")String keySearch);
	
	
	
	@Modifying
	@Transactional
	@Query(value="INSERT "
			+ "INTO FORM_FIELDS "
			+ "(field_id, field_name, field_type, field_visibility, field_label, field_global_value, field_weight, field_store, field_extra, row_id) "
			+ "VALUES(:field_id, :field_name, :field_type, :field_visibility, :field_label, :field_global_value, :field_weight, :field_store, :field_extra, :row_id)", nativeQuery=true)
	public void saveFormFields(@Param("field_id") String field_id,
						  @Param("field_name") String field_name, 
						  @Param("field_type") String field_type,
						  @Param("field_visibility") String field_visibility,
						  @Param("field_label") String field_label,
						  @Param("field_global_value") String field_global_value,
						  @Param("field_weight") String field_weight,
						  @Param("field_store") String field_store,
						  @Param("field_extra") String field_extra,
						  @Param("row_id") String row_id
						  );
	
	@Modifying
	@Transactional
	@Query(value="UPDATE "
			+ "FORM_FIELDS "
			+ "SET "
			+ "field_name =:field_name, "
			+ "field_type =:field_type, "
			+ "field_visibility =:field_visibility, "
			+ "field_label =:field_label, "
			+ "field_global_value =:field_global_value, "
			+ "field_weight =:field_weight, "
			+ "field_store =:field_store, "
			+ "field_extra =:field_extra, "
			+ "row_id =:row_id "
			+ "where field_id =:field_id", nativeQuery=true)
	public void updateFormFields(
			  @Param("field_name") String field_name, 
			  @Param("field_type") String field_type,
			  @Param("field_visibility") String field_visibility,
			  @Param("field_label") String field_label,
			  @Param("field_global_value") String field_global_value,
			  @Param("field_weight") String field_weight,
			  @Param("field_store") String field_store,
			  @Param("field_extra") String field_extra,
			  @Param("row_id") String row_id,
			  @Param("field_id") String field_id
			  );
	
	@Modifying
	@Transactional
	@Query(value="DELETE FORM_FIELDS WHERE field_id = :field_id", nativeQuery=true)
	public void deleteFormFields(@Param("field_id") String field_id);
	
}
	

