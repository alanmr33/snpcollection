package dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import entity.UserProfile;
import entity.UserProfilePK;


public interface UserProfileDao extends JpaRepository<UserProfile, UserProfilePK>{
	
	
	@Query(value="SELECT " 
					+ "up.USER_ID, "
					+ "up.AKTIF_STATUS, "
					+ "up.AUTHORITY_ID,"
					+ "up.PASSWORD "
					+ "FROM USER_PROFILE up " 
					+ "WHERE up.USER_ID = :userId ", nativeQuery=true)
	public List<Object[]> findUserByUserId(@Param("userId")String userId);
	
	@Query(value="SELECT " 
			+ "up.USER_ID, "
			+ "up.AKTIF_STATUS, "
			+ "up.AUTHORITY_ID,"
			+ "up.PASSWORD, "
			+ "CASE WHEN (up.NAMA_KARYAWAN IS NULL OR up.NAMA_KARYAWAN = '') THEN '--' ELSE cast(up.NAMA_KARYAWAN as varchar) END AS namaKaryawan, "
			+ "CASE WHEN (am.AUTHORITY_NAME IS NULL OR am.AUTHORITY_NAME = '') THEN '--' ELSE cast(am.AUTHORITY_NAME as varchar) END AS AUTHORITY_NAME "
			+ "FROM USER_PROFILE up "
			+ "LEFT JOIN AUTHORITY_MST am on am.AUTHORITY_ID = up.AUTHORITY_ID " 
			+ "WHERE up.AKTIF_STATUS = 1 AND up.USER_ID = :userId AND up.PASSWORD = :password ", nativeQuery=true)
	public List<Object[]> findUserByUserIdAndPass(@Param("userId")String userId,@Param("password")String password);
	

	
	@Query(value="SELECT "
			+ "CASE WHEN (up.USER_ID IS NULL OR up.USER_ID = '') THEN '--' ELSE cast(up.USER_ID as varchar) END AS USER_ID, " 
			+ "CASE WHEN (up.NAMA_KARYAWAN IS NULL OR up.NAMA_KARYAWAN = '') THEN '--' ELSE cast(up.NAMA_KARYAWAN as varchar) END AS NAMA_KARYAWAN, " 
			+ "CASE WHEN up.AKTIF_STATUS  = 1 THEN 'ENABLED' ELSE 'DISABLED' END AS statusAktif, "
			+ "CASE WHEN (am.AUTHORITY_ID IS NULL OR am.AUTHORITY_ID = '') THEN '--' ELSE cast(am.AUTHORITY_ID as varchar) END AS AUTHORITY_ID, "
			+ "CASE WHEN (am.AUTHORITY_NAME IS NULL OR am.AUTHORITY_NAME = '') THEN '--' ELSE cast(am.AUTHORITY_NAME as varchar) END AS AUTHORITY_NAME, "
			+ "CASE WHEN (up.IMEI_NUMBER IS NULL OR up.IMEI_NUMBER = '') THEN '--' ELSE cast(up.IMEI_NUMBER as varchar) END AS IMEI_NUMBER, "
			+ "CASE WHEN (up.PHONE_NUMBER IS NULL OR up.PHONE_NUMBER = '') THEN '--' ELSE cast(up.PHONE_NUMBER as varchar) END AS PHONE_NUMBER, "
			+ "CASE WHEN (up.KODE_KOLEKTOR IS NULL OR up.KODE_KOLEKTOR = '') THEN '--' ELSE cast(up.KODE_KOLEKTOR as varchar) END AS KODE_KOLEKTOR, "
			+ "CASE WHEN (up.KODE_KARYAWAN IS NULL OR up.KODE_KARYAWAN = '') THEN '--' ELSE cast(up.KODE_KARYAWAN as varchar) END AS KODE_KARYAWAN "
			+ "FROM USER_PROFILE up "
			+ "INNER JOIN AUTHORITY_MST am on am.AUTHORITY_ID = up.AUTHORITY_ID " 
			+ "LEFT JOIN AKSES_REGION_CABANG arc on arc.USER_ID = UP.USER_ID "
			+ "LEFT JOIN CABANG_MST cm on cm.KODE_CABANG = arc.KODE_CABANG "
			+ "LEFT JOIN REGION_MST rm on rm.KODE_REGION = arc.KODE_REGION "
			+ "WHERE cm.NAMA_CABANG LIKE %:keySearch% OR rm.NAMA_REGION LIKE %:keySearch% OR up.USER_ID LIKE %:keySearch% or up.NAMA_KARYAWAN LIKE %:keySearch% " 
			+ "GROUP BY up.USER_ID, up.KODE_KARYAWAN, up.NAMA_KARYAWAN, up.AKTIF_STATUS, am.AUTHORITY_ID, am.AUTHORITY_NAME, up.IMEI_NUMBER, up.PHONE_NUMBER, up.KODE_KOLEKTOR", nativeQuery=true)
	public List<Object[]> findUserAllBySearch(@Param("keySearch")String keySearch);
	
	@Query(value="SELECT "
			+ "CASE WHEN (up.USER_ID IS NULL OR up.USER_ID = '') THEN '--' ELSE cast(up.USER_ID as varchar) END AS USER_ID, " 
			+ "CASE WHEN (up.NAMA_KARYAWAN IS NULL OR up.NAMA_KARYAWAN = '') THEN '--' ELSE cast(up.NAMA_KARYAWAN as varchar) END AS NAMA_KARYAWAN, " 
			+ "CASE WHEN up.AKTIF_STATUS  = 1 THEN 'ENABLED' ELSE 'DISABLED' END AS statusAktif, "
			+ "CASE WHEN (am.AUTHORITY_ID IS NULL OR am.AUTHORITY_ID = '') THEN '--' ELSE cast(am.AUTHORITY_ID as varchar) END AS AUTHORITY_ID, "
			+ "CASE WHEN (am.AUTHORITY_NAME IS NULL OR am.AUTHORITY_NAME = '') THEN '--' ELSE cast(am.AUTHORITY_NAME as varchar) END AS AUTHORITY_NAME, "
			+ "CASE WHEN (up.IMEI_NUMBER IS NULL OR up.IMEI_NUMBER = '') THEN '--' ELSE cast(up.IMEI_NUMBER as varchar) END AS IMEI_NUMBER, "
			+ "CASE WHEN (up.PHONE_NUMBER IS NULL OR up.PHONE_NUMBER = '') THEN '--' ELSE cast(up.PHONE_NUMBER as varchar) END AS PHONE_NUMBER, "
			+ "CASE WHEN (up.KODE_KOLEKTOR IS NULL OR up.KODE_KOLEKTOR = '') THEN '--' ELSE cast(up.KODE_KOLEKTOR as varchar) END AS KODE_KOLEKTOR "
			+ "FROM USER_PROFILE up "
			+ "INNER JOIN AUTHORITY_MST am on am.AUTHORITY_ID = up.AUTHORITY_ID " 
			+ "INNER JOIN AKSES_REGION_CABANG arc on arc.USER_ID = up.USER_ID and arc.KODE_REGION IS NULL "
			+ "LEFT JOIN CABANG_MST cm on cm.KODE_CABANG = arc.KODE_CABANG "
			+ "WHERE up.AKTIF_STATUS = 1 AND cm.KODE_CABANG = :kodeCabang and (up.USER_ID LIKE %:keySearch% or up.NAMA_KARYAWAN LIKE %:keySearch% OR cm.NAMA_CABANG LIKE %:keySearch%) " 
			+ "GROUP BY up.USER_ID, up.NAMA_KARYAWAN, up.AKTIF_STATUS, am.AUTHORITY_ID, am.AUTHORITY_NAME, up.IMEI_NUMBER, up.PHONE_NUMBER, up.KODE_KOLEKTOR",nativeQuery=true)
	public List<Object[]> findUserAllByKodeCabangAndSearch(@Param("kodeCabang")String kodeCabang,@Param("keySearch")String keySearch);
	
	@Query(value="SELECT " 
			+ "CASE WHEN (up.USER_ID IS NULL OR up.USER_ID = '') THEN '--' ELSE cast(up.USER_ID as varchar) END AS USER_ID, "
			+ "CASE WHEN (up.NAMA_KARYAWAN IS NULL OR up.NAMA_KARYAWAN = '') THEN '--' ELSE cast(up.NAMA_KARYAWAN as varchar) END AS NAMA_KARYAWAN, " 
			+ "CASE WHEN up.AKTIF_STATUS  = 1 THEN 'ENABLED' ELSE 'DISABLED' END AS statusAktif, "
			+ "CASE WHEN (am.AUTHORITY_ID IS NULL OR am.AUTHORITY_ID = '') THEN '--' ELSE cast(am.AUTHORITY_ID as varchar) END AS AUTHORITY_ID, "
			+ "CASE WHEN (am.AUTHORITY_NAME IS NULL OR am.AUTHORITY_NAME = '') THEN '--' ELSE cast(am.AUTHORITY_NAME as varchar) END AS AUTHORITY_NAME, " 
			+ "CASE WHEN (up.IMEI_NUMBER IS NULL OR up.IMEI_NUMBER = '') THEN '--' ELSE cast(up.IMEI_NUMBER as varchar) END AS IMEI_NUMBER, "
			+ "CASE WHEN (up.PHONE_NUMBER IS NULL OR up.PHONE_NUMBER = '') THEN '--' ELSE cast(up.PHONE_NUMBER as varchar) END AS PHONE_NUMBER, "
			+ "CASE WHEN (up.KODE_KOLEKTOR IS NULL OR up.KODE_KOLEKTOR = '') THEN '--' ELSE cast(up.KODE_KOLEKTOR as varchar) END AS KODE_KOLEKTOR " 
			+ "FROM USER_PROFILE up "
			+ "INNER JOIN AUTHORITY_MST am on am.AUTHORITY_ID = up.AUTHORITY_ID "
			+ "INNER JOIN AKSES_REGION_CABANG arc on arc.USER_ID = up.USER_ID and arc.KODE_REGION is null "
			+ "INNER JOIN AKSES_REGION_CABANG arc2 on arc2.USER_ID = up.USER_ID and arc2.KODE_CABANG is null "
			+ "LEFT JOIN CABANG_MST cm on cm.KODE_CABANG = arc.KODE_CABANG "
			+ "LEFT JOIN REGION_MST rm on rm.KODE_REGION = arc2.KODE_REGION " 
			+ "WHERE up.AKTIF_STATUS = 1 AND rm.KODE_REGION = :kodeRegional AND cm.KODE_CABANG = :kodeCabang and (up.USER_ID LIKE %:keySearch% or up.NAMA_KARYAWAN LIKE %:keySearch% or rm.NAMA_REGION LIKE %:keySearch% OR cm.NAMA_CABANG LIKE %:keySearch%) " 
			+ "GROUP BY up.USER_ID, up.NAMA_KARYAWAN, up.AKTIF_STATUS, am.AUTHORITY_ID, am.AUTHORITY_NAME, up.IMEI_NUMBER, up.PHONE_NUMBER, up.KODE_KOLEKTOR ",nativeQuery=true)
	public List<Object[]> findUserAllByKodeRegionalKodeCabangAndSearch(@Param("kodeRegional")String kodeRegional,@Param("kodeCabang")String kodeCabang,@Param("keySearch")String keySearch);
	
	@Modifying
	@Transactional
	@Query(value="INSERT INTO USER_PROFILE (USER_ID, KODE_KARYAWAN, NAMA_KARYAWAN, AUTHORITY_ID, IMEI_NUMBER, PHONE_NUMBER, AKTIF_STATUS,CREATE_USER,CREATE_DATE, PASSWORD, KODE_KOLEKTOR) "
			+ "VALUES "
			+ "(:USER_ID, " 
			+ ":KODE_KARYAWAN, " 
			+ ":NAMA_KARYAWAN, " 
			+ ":AUTHORITY_ID, " 
			+ ":IMEI_NUMBER, " 
			+ ":PHONE_NUMBER, "
			+ ":AKTIF_STATUS, " 
			+ ":CREATE_USER, "
			+ "getdate(),"
			+ ":PASSWORD,"
			+ ":KODE_KOLEKTOR)  ", nativeQuery=true)
	public void saveUserProfile(
			@Param("USER_ID")String USER_ID,
			@Param("KODE_KARYAWAN")String KODE_KARYAWAN,
			@Param("NAMA_KARYAWAN")String NAMA_KARYAWAN,
			@Param("AUTHORITY_ID")String AUTHORITY_ID,
			@Param("IMEI_NUMBER")String IMEI_NUMBER,
			@Param("PHONE_NUMBER")String PHONE_NUMBER,
			@Param("AKTIF_STATUS")String AKTIF_STATUS,
			@Param("CREATE_USER")String CREATE_USER,
			@Param("PASSWORD")String PASSWORD,
			@Param("KODE_KOLEKTOR")String KODE_KOLEKTOR);
	
	@Modifying
	@Transactional
	@Query(value="UPDATE USER_PROFILE "
			+ "SET "
			+ "KODE_KARYAWAN = :KODE_KARYAWAN, " 
			+ "NAMA_KARYAWAN = :NAMA_KARYAWAN, " 
			+ "AUTHORITY_ID = :AUTHORITY_ID, " 
			+ "IMEI_NUMBER = :IMEI_NUMBER, " 
			+ "PHONE_NUMBER = :PHONE_NUMBER, " 
			+ "AKTIF_STATUS = :AKTIF_STATUS, "
			+ "MODIFY_USER = :MODIFY_USER, "
			+ "MODIFY_DATE = getdate(), "
			+ "KODE_KOLEKTOR = :KODE_KOLEKTOR "
			+ "WHERE "
			+ "USER_ID = :USER_ID ", nativeQuery=true)
	public void updateUserProfile(
			@Param("KODE_KARYAWAN")String KODE_KARYAWAN,
			@Param("NAMA_KARYAWAN")String NAMA_KARYAWAN,
			@Param("AUTHORITY_ID")String AUTHORITY_ID,
			@Param("IMEI_NUMBER")String IMEI_NUMBER,
			@Param("PHONE_NUMBER")String PHONE_NUMBER,
			@Param("AKTIF_STATUS")String AKTIF_STATUS,
			@Param("MODIFY_USER")String MODIFY_USER,
			@Param("USER_ID")String USER_ID,
			@Param("KODE_KOLEKTOR")String KODE_KOLEKTOR);
	
	@Modifying
	@Transactional
	@Query(value="UPDATE USER_PROFILE "
			+ "SET "
			+ "AKTIF_STATUS = :AKTIF_STATUS, "
			+ "MODIFY_USER = :MODIFY_USER, "
			+ "MODIFY_DATE = getdate() "
			+ "WHERE "
			+ "USER_ID = :USER_ID ", nativeQuery=true)
	public void disableUser(
			@Param("AKTIF_STATUS")String AKTIF_STATUS,
			@Param("MODIFY_USER")String MODIFY_USER,
			@Param("USER_ID")String USER_ID);
	
	@Modifying
	@Transactional
	@Query(value="UPDATE USER_PROFILE "
			+ "SET "
			+ "PASSWORD = :PASSWORD, "
			+ "MODIFY_USER = :MODIFY_USER, "
			+ "MODIFY_DATE = getdate() "
			+ "WHERE "
			+ "USER_ID = :USER_ID ", nativeQuery=true)
	public void changePasswordUserProfile(
			@Param("PASSWORD")String PASSWORD,
			@Param("MODIFY_USER")String MODIFY_USER,
			@Param("USER_ID")String USER_ID);
	
	@Modifying
	@Transactional
	@Query(value="UPDATE USER_PROFILE "
			+ "SET "
			+ "PASSWORD = :PASSWORD, "
			+ "MODIFY_USER = :MODIFY_USER, "
			+ "MODIFY_DATE = getdate() "
			+ "WHERE "
			+ "USER_ID = :USER_ID ", nativeQuery=true)
	public void resetPassword(
			@Param("PASSWORD")String PASSWORD,
			@Param("MODIFY_USER")String MODIFY_USER,
			@Param("USER_ID")String USER_ID);
	
	@Query(value="SELECT "
			+ "CASE WHEN (up.USER_ID IS NULL OR up.USER_ID = '') THEN '--' ELSE cast(up.USER_ID as varchar) END AS USER_ID, "
			+ "CASE WHEN (up.NAMA_KARYAWAN IS NULL OR up.NAMA_KARYAWAN = '') THEN '--' ELSE cast(up.NAMA_KARYAWAN as varchar) END AS NAMA_KARYAWAN "
			+ "FROM USER_PROFILE up "
			+ "INNER JOIN AUTHORITY_MST am on am.AUTHORITY_ID = up.AUTHORITY_ID "
			+ "INNER JOIN AKSES_REGION_CABANG arc on arc.USER_ID = UP.USER_ID "
			+ "WHERE (arc.KODE_CABANG =:kodeCabang OR arc.KODE_REGION=:kodeRegional) AND up.AKTIF_STATUS=1 "  
			+ "GROUP BY up.USER_ID, up.NAMA_KARYAWAN", nativeQuery=true)
	public List<Object[]> findUserByBranchAndRegional(@Param("kodeCabang")String kodeCabang, @Param("kodeRegional")String kodeRegional);
	
}
	

