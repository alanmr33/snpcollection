package pageservice;

import java.util.List;

public interface SidebarPageConfig {
	
	public List<SidebarPage> getPages();
	public List<SidebarPage> getPages1();
	public List<SidebarPage> getPages2();
	public List<SidebarPage> getPages3();
	public List<SidebarPage> getPages4();
	public List<SidebarPage> getPages5();
	
	public SidebarPage getPage(String name);
	public SidebarPage getPage1(String name);
	public SidebarPage getPage2(String name);
	public SidebarPage getPage3(String name);
	public SidebarPage getPage4(String name);
	public SidebarPage getPage5(String name);
}
