package pageservice;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;

import dto.UserProfileDto;
import entity.UserMenuAccordion;

public class SidebarPageConfigAjaxBasedImpl implements SidebarPageConfig{
	
	
	private UserProfileDto dsfmstUser2Dto = new UserProfileDto();
	
	HashMap<String,SidebarPage> pageMap = new LinkedHashMap<String,SidebarPage>();
	HashMap<String, SidebarPage> pageMap1 = new LinkedHashMap<String,SidebarPage>();
	HashMap<String, SidebarPage> pageMap2 = new LinkedHashMap<String,SidebarPage>();
	HashMap<String, SidebarPage> pageMap3 = new LinkedHashMap<String,SidebarPage>();
	HashMap<String, SidebarPage> pageMap4 = new LinkedHashMap<String,SidebarPage>();
	HashMap<String, SidebarPage> pageMap5 = new LinkedHashMap<String,SidebarPage>();
	
	public SidebarPageConfigAjaxBasedImpl(){		
		
		Session sess = Sessions.getCurrent();
		
		if(sess.hasAttribute("user")){
			
			dsfmstUser2Dto = (UserProfileDto) Sessions.getCurrent().getAttribute("user");
		}
		else{
			sess.removeAttribute("user");
		}
		
		List<UserMenuAccordion> userMenuAccordions  = dsfmstUser2Dto.getLisAccordions();
		int i = 1;
		for(UserMenuAccordion a : userMenuAccordions){
		
			if(a.getTitle().equalsIgnoreCase("MASTER USER") || a.getTitle().equalsIgnoreCase("AKSES MENU")|| a.getTitle().equalsIgnoreCase("UBAH KATA SANDI"))
			{
				pageMap.put("fn" + i, new SidebarPage(a.getTitle(), a.getTitle(),"", a.getUrl()));
				i++;
			}else if (a.getTitle().equalsIgnoreCase("MASTER REGIONAL")||a.getTitle().equalsIgnoreCase("MASTER CABANG") ||a.getTitle().equalsIgnoreCase("MASTER MENU") ||a.getTitle().equalsIgnoreCase("MASTER AUTHORITY") ||a.getTitle().equalsIgnoreCase("MASTER PROVINSI") || a.getTitle().equalsIgnoreCase("MASTER HANDLING") ) {
				pageMap1.put("fn" + i, new SidebarPage(a.getTitle(), a.getTitle(),"", a.getUrl()));
				i++;
			}else if (a.getTitle().equalsIgnoreCase("UNGGAH PENANGANAN")||a.getTitle().equalsIgnoreCase("PENUGASAN ULANG") ||a.getTitle().equalsIgnoreCase("KOREKSI") ||a.getTitle().equalsIgnoreCase("PEMBERITAHUAN")) {
				pageMap2.put("fn" + i, new SidebarPage(a.getTitle(), a.getTitle(),"", a.getUrl()));
				i++;
			}else if (a.getTitle().equalsIgnoreCase("MOBILE TRACKING")) {
				pageMap3.put("fn" + i, new SidebarPage(a.getTitle(), a.getTitle(),"", a.getUrl()));
				i++;
			}else if (a.getTitle().equalsIgnoreCase("KUNJUNGAN KOLEKTOR") ||a.getTitle().equalsIgnoreCase("REKON KOLEKTOR") ||a.getTitle().equalsIgnoreCase("KOLLEKTOR FULL DATA")) {
				pageMap4.put("fn4" + i, new SidebarPage(a.getTitle(), a.getTitle(),"", a.getUrl()));
				i++;
			}else if (a.getTitle().equalsIgnoreCase("MASTER PARAMETER") || a.getTitle().equalsIgnoreCase("MASTER FORMS") || a.getTitle().equalsIgnoreCase("MASTER EVENTS") || a.getTitle().equalsIgnoreCase("FORM ROWS")) {
				pageMap5.put("fn5" + i, new SidebarPage(a.getTitle(), a.getTitle(),"", a.getUrl()));
				i++;
			}
		}
	}
	
	
	public List<SidebarPage> getPages(){
		return new ArrayList<SidebarPage>(pageMap.values());
	}
	public SidebarPage getPage(String name){
		return pageMap.get(name);
	}
	
	
	public List<SidebarPage> getPages1(){
		return new ArrayList<SidebarPage>(pageMap1.values());
	}
	public SidebarPage getPage1(String name){
		return pageMap1.get(name);
	}


	public List<SidebarPage> getPages2(){
		return new ArrayList<SidebarPage>(pageMap2.values());
	}
	public SidebarPage getPage2(String name){
		return pageMap2.get(name);
	}
	
	public List<SidebarPage> getPages3(){
		return new ArrayList<SidebarPage>(pageMap3.values());
	}
	public SidebarPage getPage3(String name){
		return pageMap3.get(name);
	}
	
	public List<SidebarPage> getPages4(){
		return new ArrayList<SidebarPage>(pageMap4.values());
	}
	public SidebarPage getPage4(String name){
		return pageMap4.get(name);
	}


	public List<SidebarPage> getPages5(){
		return new ArrayList<SidebarPage>(pageMap5.values());
	}
	public SidebarPage getPage5(String name){
		return pageMap5.get(name);
	}
	
	
}
