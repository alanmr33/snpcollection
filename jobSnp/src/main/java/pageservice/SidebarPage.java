package pageservice;


import java.io.Serializable;

public class SidebarPage implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2160320275789026296L;
	String name;
	String label;
	String iconUri;
	String uri;

	public SidebarPage(String name, String label, String iconUri, String uri) {
		this.name = name;
		this.label = label;
		this.iconUri = iconUri;
		this.uri = uri;
	}

	public String getName() {
		return name;
	}

	public String getLabel() {
		return label;
	}

	public String getIconUri() {
		return iconUri;
	}

	public String getUri() {
		return uri;
	}
}
