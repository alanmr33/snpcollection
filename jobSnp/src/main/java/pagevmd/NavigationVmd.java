package pagevmd;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Filedownload;

import com.mysql.jdbc.Statement;

import dto.AuthorityListDto;
import dto.AuthorityMstDto;
import dto.MenuMstDto;
import dto.UserProfileDto;
import pageservice.SidebarPage;
import service.AuthorityListSvc;
import service.AuthorityMstSvc;
import service.MenuMstSvc;


@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class NavigationVmd {
	
	@WireVariable
	private AuthorityListSvc authorityListSvc;
	

	private String includeSrc;
	
	@Init
	public void load(){
		UserProfileDto dsfMstUserDtoSession = (UserProfileDto) Sessions.getCurrent().getAttribute("user");
		List<AuthorityListDto> list = authorityListSvc.findAuthorityListByAuthorityId(dsfMstUserDtoSession.getAuthorityId());
		if (list.size() != 0) {
			includeSrc = "/master/Home.zul";
		}
	}
	@GlobalCommand("onNavigate")
	@NotifyChange("includeSrc")
	public void onNavigate(@BindingParam("page") SidebarPage page) {
		String locationUri = page.getUri();
		String name = page.getName();
		includeSrc = locationUri;
	}

	public String getIncludeSrc() {
		return includeSrc;
	}
		
}
