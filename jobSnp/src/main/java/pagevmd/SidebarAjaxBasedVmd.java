package pagevmd;

import java.util.List;

import pageservice.SidebarPage;
import pageservice.SidebarPageConfig;
import pageservice.SidebarPageConfigAjaxBasedImpl;

public class SidebarAjaxBasedVmd {


private SidebarPageConfig pageConfig = new SidebarPageConfigAjaxBasedImpl();
	
	public List<SidebarPage> getSidebarPages() {
		return pageConfig.getPages();
	}
	
	public List<SidebarPage> getSidebarPages1(){
		return pageConfig.getPages1();
	}
	
	public List<SidebarPage> getSidebarPages2(){
		return pageConfig.getPages2();
	}
	
	public List<SidebarPage> getSidebarPages3(){
		return pageConfig.getPages3();
	}
	
	public List<SidebarPage> getSidebarPages4(){
		return pageConfig.getPages4();
	}
	
	public List<SidebarPage> getSidebarPages5(){
		return pageConfig.getPages5();
	}
	
	
}
