package service;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.query.Param;

import dto.AuthorityMstDto;
import entity.AuthorityMst;


public interface AuthorityMstSvc {
	public List<AuthorityMstDto> findAuthorityByAuthorityId(int authorityId);
	public List<AuthorityMstDto> searchAuthority(String search);
	public List<AuthorityMstDto> searchAllAuthorityActive(String search);
	public void saveAuthority(String authorityName, int aktifStatus, String createUser);
	public void updateAuthority(String authorityName,int aktifStatus,String modifyUser,int authorityID); 
	public void deleteAuthority(String modifyUser,int authorityID); 
	public AuthorityMstDto findOneAuthority(int authorityId);
}
