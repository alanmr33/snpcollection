package service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import dto.MenuMstDto;
import dto.UserProfileDto;
import entity.MenuMstPK;

public interface MenuMstSvc {
	public List<MenuMstDto> findMenuAllBySearch(String keySearch);
	public void DeleteMenu(String modifyUser,int menu);
	public void aktifkanMenu(String modifyUser,int menu);
}
