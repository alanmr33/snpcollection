package service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import dto.AuthorityMstDto;
import dto.CabangMstDto;
import dto.FormsDto;
import dto.UserProfileDto;
import entity.CabangMst;

public interface FormsSvc {
	public List<FormsDto> findFormsWithSearch(String keySearch);
	public FormsDto findFormsById(String keySearch);
	public void deleteForms(String formId);
	public void updateForms(String formName, String formLabel, String formOrder, String formId);
	public void upDownOrder(int formOrder, String formId);
	public void saveForms(String formId, String formName, String formLabel, String formOrder);
}
