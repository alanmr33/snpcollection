package service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import dto.MasterProvinsiDto;

public interface MasterProvinsiSvc {
	public List<MasterProvinsiDto> findAllProvinsi();
	public List<MasterProvinsiDto> searchAllProvinsi( String search);
	public void InsertProvinsi(String provinsiCode, String provinsiName, int aktifStatus, String createUser);
	public void UpdateProvinsi(String provinsiName, String modifyUser, int aktifStatus, String provinsiCode);
	public void DeleteProvinsi(String modifyUser, String provinsiCode);
	public MasterProvinsiDto findOneProvinsi(String provinsiCode);
}
