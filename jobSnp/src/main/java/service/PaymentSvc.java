package service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import dto.AuthorityMstDto;
import dto.CabangMstDto;
import dto.UserProfileDto;
import entity.CabangMst;

public interface PaymentSvc {
	public void insertPayment(int INSTALLMENT_NO,int ORDER_ID,String CUSTOMER_CODE,String CONTRACT_NO,String CODE_COLLECTOR,int TOTAL_BAYAR,String NO_KWITANSI,String NO_LPK,String TANGGAL_TRANSAKSI,String TANGGAL_BAYAR,String STATUS,String CREATE_USER);
	
}
