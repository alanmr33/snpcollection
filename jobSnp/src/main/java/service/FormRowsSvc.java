package service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import dto.AuthorityMstDto;
import dto.CabangMstDto;
import dto.EventsDto;
import dto.FormRowsDto;
import dto.FormsDto;
import dto.UserProfileDto;
import entity.CabangMst;

public interface FormRowsSvc {
	public List<FormRowsDto> findFormRowsWithSearch(String keySearch);
	public FormRowsDto findFormRowsById(String keySearch);
	public void deleteFormRows(String row_id);
	public void updateFormRows(String row_order,String row_visibility,String form_id, String row_id);
	public void upDownFormRows(int row_order, String row_id);
	public void saveFormRows(String row_id,String row_order,String row_visibility,String form_id);
}
