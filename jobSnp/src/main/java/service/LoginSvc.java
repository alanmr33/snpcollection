package service;

import java.security.NoSuchAlgorithmException;
import java.util.Map;

public interface LoginSvc {
	public Map<String , Object> findRoleMenu(String userId, String password) throws NoSuchAlgorithmException;
}
