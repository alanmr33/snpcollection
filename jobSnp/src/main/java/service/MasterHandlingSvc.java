package service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import dto.MasterHandlingDto;

public interface MasterHandlingSvc {

	public List<MasterHandlingDto> findAllHandling(String search,int batchHand);
	public List<MasterHandlingDto> findAllHandlingByBatchHandling(int batchHandling);
	public MasterHandlingDto findOneHandling(int handNo);
	public void insertHandling(int batchHandling,String handCode, String promise, String deskripsi, String aktif, String createUser);
	public void updateHandling(String handCode, String promise, String deskripsi, String aktif, String modifyUser, int handNo);
	public void deleteHandling(String modifyUser, int handNo, String handCode);
	public int maxBatchHandling();
}
