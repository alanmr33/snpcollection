package service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import dto.AksesRegionCabangDto;
import dto.AuthorityMstDto;
import dto.CabangMstDto;
import dto.UserProfileDto;
import entity.CabangMst;

public interface AksesRegionCabangSvc {
	public List<AksesRegionCabangDto> findRegionByUserId(String userId, String keySearch);
	public List<AksesRegionCabangDto> fintCabangByUserId(String userId, String keySearch);
	public List<AksesRegionCabangDto> fintCabangByUserIdAndKodeRegion(String userId, String kodeRegion, String keySearch);
	public void insertRegion(String userID,String kodeRegion);
	public void insertCabang(String userID,String kodeCabang);
	public void deleteRegion(String userID);
	public void deleteCabang(String userID);
}
