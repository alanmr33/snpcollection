package service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import dto.AuthorityMstDto;
import dto.UserProfileDto;

public interface UserProfileSvc {
	public List<UserProfileDto> findUserByUserId(String userId);
	public List<UserProfileDto> findUserByUserIdAndPass(String userId, String pass);
	public List<UserProfileDto> findUserAllBySearch(String keySearch);
	public List<UserProfileDto> findUserAllByKodeCabangAndSearch(String kodeCabang,String keySearch);
	public List<UserProfileDto> findUserAllByKodeRegionalKodeCabangAndSearch(String kodeRegional,String kodeCabang,String keySearch);
	public void saveUserProfile(String USER_ID,
								String KODE_KARYAWAN,
								String NAMA_KARYAWAN,
								String AUTHORITY_ID,
								String IMEI_NUMBER,
								String PHONE_NUMBER,
								String AKTIF_STATUS,
								String CREATE_USER,
								String PASSWORD,
								String KODE_KOLEKTOR);
	
	public void updateUserProfile(String KODE_KARYAWAN,
								String NAMA_KARYAWAN,
								String AUTHORITY_ID,
								String IMEI_NUMBER,
								String PHONE_NUMBER,
								String AKTIF_STATUS,
								String MODIFY_USER,
								String USER_ID,
								String KODE_KOLEKTOR);
	
	public void disableUserProfile(String AKTIF_STATUS,
								String MODIFY_USER,
								String USER_ID);
	
	public void changePassworUserProfile(String PASSWORD,
			String MODIFY_USER,
			String USER_ID);
	
	public void resetPassword(String PASSWORD,
			String MODIFY_USER,
			String USER_ID);
	
	public List<UserProfileDto> findUserByBranchAndRegional(String kodeCabang, String kodeRegional);
}
