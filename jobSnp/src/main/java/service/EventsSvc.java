package service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import dto.AuthorityMstDto;
import dto.CabangMstDto;
import dto.EventsDto;
import dto.FormsDto;
import dto.UserProfileDto;
import entity.CabangMst;

public interface EventsSvc {
	public List<EventsDto> findEventsWithSearch(String keySearch);
	public EventsDto findEventsById(String keySearch);
	public void deleteEvents(String eventsId);
	public void updateEvents(String eventType,String eventComponent,String eventComponenTarget,String eventData,String formId,String eventId);
	public void saveEvents(String eventId,String eventType,String eventComponent,String eventComponenTarget,String eventData,String formId);
}
