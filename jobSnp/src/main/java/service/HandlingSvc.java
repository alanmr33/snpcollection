package service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import dto.AuthorityMstDto;
import dto.CabangMstDto;
import dto.UserProfileDto;
import entity.CabangMst;

public interface HandlingSvc {
	public void insertHandling(int BATCH_HANDLING,String CONTRACT_NO,int INSTALLMENT_NO,int ORDER_ID, String KODE_HANDLING,String KODE_KONSUMEN,String NO_LPK,String NOTES,String TANGGAL_HANDLING,String TANGGAL_JANJI,int PROMISE_COUNT,int PROMISE_AVAILABLE,int PROMISE_RANGE,String STATUS,String CREATE_USER);
	public void insertHandDepo(int BATCH_HANDLING,String CONTRACT_NO,int INSTALLMENT_NO,int ORDER_ID, String KODE_HANDLING,String KODE_KONSUMEN,String NO_LPK,String NOTES,String TANGGAL_HANDLING,String TANGGAL_JANJI,int PROMISE_COUNT,int PROMISE_AVAILABLE,int PROMISE_RANGE,String STATUS,String CREATE_USER,int deposit, String depositFrom);
	public void insertHandDepoNoTglJanji(int BATCH_HANDLING,String CONTRACT_NO,int INSTALLMENT_NO,int ORDER_ID, String KODE_HANDLING,String KODE_KONSUMEN,String NO_LPK,String NOTES,String TANGGAL_HANDLING,int PROMISE_COUNT,int PROMISE_AVAILABLE,int PROMISE_RANGE,String STATUS,String CREATE_USER,int deposit, String depositFrom);
	
	
}
