package service;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.query.Param;

import dto.ManualAssignDto;

public interface ManualAssignSvc {
	public List<ManualAssignDto> findManualAssign();
	public List<ManualAssignDto> findAllManualAssign(String search);
	public void insertAssign(int seqNo, String colCode, String newColCode, Date periodFrom, Date periodTo, String notes, int orderId);
	public int maxSeqnoBByUserId(String kodeKolektor);
	public void updateAssign(String newCol, String reassignBy,String colPrev,String notes,int orderId);
	public List<ManualAssignDto> findAssignRegionalCabang(String regionalCode,String cabangCode,String search);
	public List<ManualAssignDto> findAssignCabang(String cabangCode,String search);
	public List<ManualAssignDto> findAssignRegionalCabangSurveyor(String regionalCode,String cabangCode,String kodeKol,String search);
	public List<ManualAssignDto> findAssignCabangSurveyor(String cabangCode,String kodeKol,String search);
}
