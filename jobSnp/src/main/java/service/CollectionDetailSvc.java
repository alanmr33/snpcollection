package service;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.query.Param;

import dto.CollectionDetailDto;
import dto.CollectionHeaderDto;
import dto.ManualAssignDto;
import dto.MobileTrackingDto;
import dto.ReassignCollector;

public interface CollectionDetailSvc {
	
	public List<CollectionDetailDto> selectForTaskAdjustmentByKodeRegional(String koderRegional, String keySearch);
	public List<CollectionDetailDto> selectForTaskAdjustmentByKodeCabang(String koderCabang, String keySearch);
	public List<CollectionDetailDto> selectForTaskAdjustmentByKodeCabangKodeKol(String koderCabang,String kodeKol, String keySearch);
	public List<CollectionDetailDto> selectForTaskAdjustmentByKodeRegionalKodeKol(String kodeRegional,String kodeKol, String keySearch);
	public List<CollectionDetailDto> selectForTaskAdjustmentByKodeRegionalKodeCabang(String koderRegional, String kodeCabang,String keySearch);
	public void updateCollectionHeadetToCMP(String status,int orderId);
	public void updateStatusCollectionDetailToCMP(int orderId,String contractNo,int instalmentNo);
	public void updateStatusCollectionDetailToCLS(int orderId,String contractNo,int instalmentNo, String notes);
	public void updateCdToCloseInTaskAdjustment(String remark,int orderId);
	public int cekStatusCollectionDetailByOrderId(int orderId);
	public void updatePromiseCountPromisAvailable(int promiseCount,int promiseAvailable,int orderId,String contractNo);
	public CollectionDetailDto selectCollectionDetailForInputPayment(int orderId,String contractNo,int instalmentNo);
	public CollectionDetailDto selectCollectionDetailForInputHandling(int orderId,String contractNo,int instalmentNo);
	public CollectionDetailDto selectCollectionDetailForInputHandling2(int orderId,String contractNo,int instalmentNo);
	
	
	
	public List<CollectionDetailDto> selectReportKunjunganKodeCabang(Date dateAwal, Date dateAkhir, String kodeCabang, String keySearch);
	public List<CollectionDetailDto> selectReportKunjunganKodeCabangKodeKolektor(Date dateAwal, Date dateAkhir, String kodeCabang, String kodeKolektor, String keySearch);
	public List<CollectionDetailDto> selectReportKunjunganKodeRegionalKodeKolektor(Date dateAwal, Date dateAkhir, String kodeRegional, String kodeKolektor, String keySearch);
	public List<CollectionDetailDto> selectReportKunjunganKodeRegionalKodeCabang(Date dateAwal, Date dateAkhir, String kodeRegional, String kodeCabang, String keySearch);
	public List<CollectionDetailDto> selectReportKunjunganDetailView(Date dateAwal, Date dateAkhir, String userId, String keySearch);
	
	
	
	public List<CollectionDetailDto> selectRekonKolektorKodeRegionalKodeCabang(Date dateAwal, Date dateAkhir, String kodeRegional, String kodeCabang, String keySearch);
	public List<CollectionDetailDto> selectRekonKolektorKodeCabang(Date dateAwal, Date dateAkhir, String kodeCabang, String keySearch);
	public List<CollectionDetailDto> selectRekonKolektorKodeCabangKodeKolektor(Date dateAwal, Date dateAkhir, String kodeCabang, String kodeKolektor, String keySearch);
	public List<CollectionDetailDto> selectRekonKolektorKodeRegionalKodeKolektor(Date dateAwal, Date dateAkhir, String kodeRegional, String kodeKolektor, String keySearch);
	
	public List<CollectionDetailDto> selectRekonKolektorDetail(Date dateAwal, Date dateAkhir, String userId, String keySearch);
	public void insertCollectionDetailInUploadManual(
			int orderId,
			String customerCode,
			String contractNo,
			int instalmentNo,
			String lpkNo,
			String customerName,
			String address,
			String village,
			String district,
			String rt,
			String rw,
			String city,
			String kodeProvinsi,
			String postCode,
			String dueDate,
			int installmentAmt,
			int depositAmt,
			int penaltyAmt,
			int collectionFee,
			int totalAmt,
			String receiptNo,
			int minPayment,
			int maxPayment,
			String repaymentDate,
			String notes,
			int promiseCount,
			int promiseAvailable,
			int promiseRange,
			String phoneArea,
			String phoneNumber,
			String mobilePrefix,
			String mobileNo,
			String namaBarang);
	
	public List<ManualAssignDto> selectResultPenugasanUlang(String keySearch);
	public List<ManualAssignDto> selectResultPenugasanUlangKodeRegKodeCab(String kodeRegion,String kodeCabang,String keySearch);
	public List<ManualAssignDto> selectResultPenugasanUlangKodeCab(String kodeCabang,String keySearch);
	public List<ManualAssignDto> selectResultPenugasanUlangKodeCabKodeKol(String kodeCab,String kodeKol,String keySearch);
	public List<ManualAssignDto> selectResultPenugasanUlangKodeRegKodeKol(String kodeReg,String kodeKol,String keySearch);
	public List<ManualAssignDto> selectResultPenugasanUlangKodeRegional(String kodeRegional,String keySearch);
	public List<ManualAssignDto> selectResultPenugasanUlangDetail(int orderId,String search);
	public List<ManualAssignDto> selectCollectionHeaderReassignDate(int orderId);
	public int bucketPayment(int orderId);
	public int jumOrderNotCmp(int orderId);
	public int bucketHandling(int orderId);
	
	//==== query table rassigng collector
	public ReassignCollector selectReassignCollectorByCollectorCode(String kodeKolektor);
	//==========
}
