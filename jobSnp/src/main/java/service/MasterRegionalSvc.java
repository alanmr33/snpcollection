package service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import dto.MasterRegionalDto;

public interface MasterRegionalSvc {
	public List<MasterRegionalDto> findAllRegional();
	public void DeleteRegional(String modifyUser, String regionCode);
	public void UpdateRegional(String regionName, String notes, String aktifStatus,String modifyUser, String regionCode);
	public void InsertRegional(String regionalCode, String regionalName, String notes, int aktifStatus, String createUser);
	public List<MasterRegionalDto> searchAllRegional(String search);
	public List<MasterRegionalDto> searchAllRegional2(String search);
	public List<MasterRegionalDto> searchRegionalByRegionalId(String regionalId);
}