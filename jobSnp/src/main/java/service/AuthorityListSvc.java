package service;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.query.Param;

import dto.AuthorityListDto;
import dto.AuthorityMstDto;


public interface AuthorityListSvc {
	public List<AuthorityListDto> findAuthorityListByAuthorityId(String authorityId);
	public List<AuthorityListDto> findAllAuthorityBySearch(String keySearch);
	public void saveAuthorityList(String authorityId, String menu);
	public void delAuthorityList(String authorityId);
}
