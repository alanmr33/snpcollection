package service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import dto.AuthorityMstDto;
import dto.CabangMstDto;
import dto.UserProfileDto;
import entity.CabangMst;

public interface CabangMstSvc {
	public List<CabangMstDto> findCabangAllBySearch(String keySearch);
	public List<CabangMstDto> findCabangAllActiveBySearch(String keySearch);
	public List<CabangMstDto> findCabangAllActiveByRegionalSearch(String regionalId,String keySearch);
	public void deleteCabang(String modifyUser,String cabangCode);
	public void updateCabang(String regionCode, String cabangName, String alamat, int aktifStatus, String modifyUser, String cabangCode);
	public void saveCabang(String cabangCode, String regionalCode, String cabangName, String alamat, int aktifStatus, String createUser);
	public CabangMstDto findOneCabang(String kodeCabang);
}
