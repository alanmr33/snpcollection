package service.impl;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Hex;
import org.apache.poi.hssf.record.DSFRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import service.AuthorityMstSvc;
import service.CabangMstSvc;
import service.EventsSvc;
import service.FormRowsSvc;
import service.FormsSvc;
import service.MenuMstSvc;
import service.LoginSvc;
import service.UserProfileSvc;
import dao.AuthorityMstDao;
import dao.CabangMstDao;
import dao.EventsDao;
import dao.FormRowsDao;
import dao.FormsDao;
import dao.MenuMstDao;
import dao.UserProfileDao;
import dto.AuthorityMstDto;
import dto.CabangMstDto;
import dto.EventsDto;
import dto.FormRowsDto;
import dto.FormsDto;
import dto.MenuMstDto;
import dto.UserProfileDto;
import dto.MenuUrlDto;
import entity.CabangMst;
import entity.UserMenuAccordion;
import entity.UserProfile;

@Service("formRowsSvc")
@Transactional
public class FormRowsSvcImpl implements FormRowsSvc {
	
	@Autowired
	private FormRowsDao forRowsDao;

	@Override
	public List<FormRowsDto> findFormRowsWithSearch(String keySearch) {
		List<FormRowsDto> listFormRowsDto = new ArrayList<FormRowsDto>();
		List<Object[]> listObject = new ArrayList<Object[]>();
		listObject = forRowsDao.findFormRowsWithSearch(keySearch);
		if (listObject.size() != 0) {
			for (Object[] obj : listObject) {
				FormRowsDto fr = new FormRowsDto();
				fr.setRowId(obj[0].toString());
				fr.setRowOrder(obj[1].toString());
				fr.setRowVisibility(obj[2].toString());
				fr.setFormId(obj[3].toString());
				fr.setItem("sdsd");
				listFormRowsDto.add(fr);
			}
		}
		return listFormRowsDto;
	}

	@Override
	public FormRowsDto findFormRowsById(String keySearch) {
		FormRowsDto fr = new FormRowsDto();
		List<Object[]> listObject = new ArrayList<Object[]>();
		listObject = forRowsDao.findFormRowsById(keySearch);
		if (listObject.size() != 0) {
			for (Object[] obj : listObject) {
				fr.setRowId(obj[0].toString());
				fr.setRowOrder(obj[1].toString());
				fr.setRowVisibility(obj[2].toString());
				fr.setFormId(obj[3].toString());
			}
		}
		return fr;
	}

	@Override
	public void deleteFormRows(String row_id) {
		forRowsDao.deleteFormRows(row_id);
	}

	@Override
	public void updateFormRows(String row_order, String row_visibility,
			String form_id, String row_id) {
		forRowsDao.updateFormRows(row_order, row_visibility, form_id, row_id);
	}

	@Override
	public void saveFormRows(String row_id, String row_order,
			String row_visibility, String form_id) {
		forRowsDao.saveFormRows(row_id, row_order, row_visibility, form_id);		
	}

	@Override
	public void upDownFormRows(int row_order, String row_id) {
		forRowsDao.upDownFormRows(row_order, row_id);
	}
		
}
