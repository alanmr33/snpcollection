package service.impl;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Hex;
import org.apache.poi.hssf.record.DSFRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import service.AuthorityMstSvc;
import service.CabangMstSvc;
import service.MenuMstSvc;
import service.LoginSvc;
import service.UserProfileSvc;
import dao.AuthorityMstDao;
import dao.CabangMstDao;
import dao.MenuMstDao;
import dao.UserProfileDao;
import dto.AuthorityMstDto;
import dto.CabangMstDto;
import dto.MenuMstDto;
import dto.UserProfileDto;
import dto.MenuUrlDto;
import entity.CabangMst;
import entity.UserMenuAccordion;
import entity.UserProfile;

@Service("cabangMstSvc")
@Transactional
public class CabangMstSvcImpl implements CabangMstSvc {
	
	@Autowired
	private CabangMstDao cabangMstDao;
	
	@Override
	public List<CabangMstDto> findCabangAllBySearch(String keySearch) {
		List<Object[]> listUserObj = cabangMstDao.findCabangAllBySearch(keySearch);
		List<CabangMstDto> listCabangMstDto = new ArrayList<CabangMstDto>();
		if (listUserObj.size() != 0) {
			for (Object[] obj : listUserObj) {
				CabangMstDto cabangMstDto = new CabangMstDto();
				cabangMstDto.setKodeCabang(obj[0].toString());
				cabangMstDto.setNamaCabang(obj[1].toString());
				cabangMstDto.setKodeRegional(obj[2].toString());
				cabangMstDto.setNamaRegion(obj[3].toString());
				cabangMstDto.setAlamat(obj[4].toString());
				cabangMstDto.setKeteranganAktif(obj[5].toString());
				listCabangMstDto.add(cabangMstDto);
			}
		}
		return listCabangMstDto;
	}

	@Override
	public void deleteCabang(String modifyUser, String cabangCode) {
		cabangMstDao.deleteCabang(modifyUser, cabangCode);
		
	}

	@Override
	public void updateCabang(String regionCode, String cabangName,
			String alamat, int aktifStatus, String modifyUser, String cabangCode) {
		cabangMstDao.updateCabang(regionCode, cabangName, alamat, aktifStatus, modifyUser, cabangCode);
	}

	@Override
	public void saveCabang(String cabangCode, String regionalCode,
			String cabangName, String alamat, int aktifStatus, String createUser) {
		cabangMstDao.saveCabang(cabangCode, regionalCode, cabangName, alamat, aktifStatus, createUser);
	}

	@Override
	public CabangMstDto findOneCabang(String kodeCabang) {
		CabangMst cabangMst = cabangMstDao.findOneCabang(kodeCabang);
		CabangMstDto cabangMstDto = new CabangMstDto();
		if(cabangMst!=null){
			cabangMstDto.setKodeCabang(cabangMst.getKodeCabang());
		}
		return cabangMstDto;
	}

	@Override
	public List<CabangMstDto> findCabangAllActiveBySearch(String keySearch) {
		List<Object[]> listUserObj = cabangMstDao.findCabangAllActiveBySearch(keySearch);
		List<CabangMstDto> listCabangMstDto = new ArrayList<CabangMstDto>();
		if (listUserObj.size() != 0) {
			for (Object[] obj : listUserObj) {
				CabangMstDto cabangMstDto = new CabangMstDto();
				cabangMstDto.setKodeCabang(obj[0].toString());
				cabangMstDto.setNamaCabang(obj[1].toString());
				cabangMstDto.setKodeRegional(obj[2].toString());
				cabangMstDto.setNamaRegion(obj[3].toString());
				cabangMstDto.setAlamat(obj[4].toString());
				cabangMstDto.setKeteranganAktif(obj[5].toString());
				listCabangMstDto.add(cabangMstDto);
			}
		}
		return listCabangMstDto;
	}

	@Override
	public List<CabangMstDto> findCabangAllActiveByRegionalSearch(String regionalId,String keySearch) {
		List<Object[]> listUserObj = cabangMstDao.findCabangAllActiveByRegionalSearch(regionalId, keySearch);
		List<CabangMstDto> listCabangMstDto = new ArrayList<CabangMstDto>();
		if (listUserObj.size() != 0) {
			for (Object[] obj : listUserObj) {
				CabangMstDto cabangMstDto = new CabangMstDto();
				cabangMstDto.setKodeCabang(obj[0].toString());
				cabangMstDto.setNamaCabang(obj[1].toString());
				cabangMstDto.setKodeRegional(obj[2].toString());
				cabangMstDto.setNamaRegion(obj[3].toString());
				cabangMstDto.setAlamat(obj[4].toString());
				cabangMstDto.setKeteranganAktif(obj[5].toString());
				cabangMstDto.setItemList(true);
				listCabangMstDto.add(cabangMstDto);
			}
		}
		return listCabangMstDto;
	}

	

}
