package service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.cache.spi.access.RegionAccessStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import dao.AksesRegionCabangDao;
import dao.UserProfileDao;
import dto.AksesRegionCabangDto;
import dto.AuthorityMstDto;
import dto.UserProfileDto;
import service.AksesRegionCabangSvc;
import service.UserProfileSvc;

@Service("userProfileSvc")
@Transactional
public class userProfileSvcImpl implements UserProfileSvc {
	
	@Autowired
	private UserProfileDao userProfileDao;
	
	@Autowired
	private AksesRegionCabangSvc aksesRegionCabangSvc;

	@Override
	public List<UserProfileDto> findUserByUserId(String userId) {
		List<Object[]> listUserObj = userProfileDao.findUserByUserId(userId);
		List<UserProfileDto> listUserProfile = new ArrayList<UserProfileDto>();
		if (listUserObj.size() != 0) {
			for (Object[] obj : listUserObj) {
				UserProfileDto userProfileDto = new UserProfileDto();
				userProfileDto.setUserId(obj[0].toString());
				userProfileDto.setAktifStatus(Integer.valueOf(obj[1].toString()));
				userProfileDto.setAuthorityId(obj[2].toString());
				userProfileDto.setPassword(obj[3].toString());
				listUserProfile.add(userProfileDto);
			}
		}
		return listUserProfile;
	}

	@Override
	public List<UserProfileDto> findUserAllBySearch(String keySearch) {
		List<Object[]> listUserObj = userProfileDao.findUserAllBySearch(keySearch);
		List<UserProfileDto> listUserProfile = new ArrayList<UserProfileDto>();
		List<AksesRegionCabangDto> listTempRegional = new ArrayList<AksesRegionCabangDto>();
		List<AksesRegionCabangDto> listTempCabang = new ArrayList<AksesRegionCabangDto>();
		if (listUserObj.size() != 0) {
			for (Object[] obj : listUserObj) {
				UserProfileDto userProfileDto = new UserProfileDto();
				userProfileDto.setUserId(obj[0].toString());
				userProfileDto.setNamaKaryawan(obj[1].toString());
				userProfileDto.setKeteranganStatus(obj[2].toString());
				userProfileDto.setAuthorityId(obj[3].toString());
				userProfileDto.setAuthorityName(obj[4].toString());
				userProfileDto.setImeiNumber(obj[5].toString());
				userProfileDto.setPhoneNumber(obj[6].toString());
				userProfileDto.setKodeKolektor(obj[7].toString());
				userProfileDto.setKodeKaryawan(obj[8].toString());
				if (aksesRegionCabangSvc.findRegionByUserId(userProfileDto.getUserId(), "") != null) {
					listTempRegional = aksesRegionCabangSvc.findRegionByUserId(userProfileDto.getUserId(), "");
					String strTempRegional = "";
					if (listTempRegional.size() != 0) {
						for (int i = 0; i < listTempRegional.size(); i++) {
							if (i == 0) {
								strTempRegional = strTempRegional+listTempRegional.get(i).getNamaRegional();
							}else {
								strTempRegional = strTempRegional+", "+listTempRegional.get(i).getNamaRegional();
							}
						}
					}
					userProfileDto.setNamaRegional(strTempRegional);
				}
				if (aksesRegionCabangSvc.fintCabangByUserId(userProfileDto.getUserId(), "") != null) {
					listTempCabang = aksesRegionCabangSvc.fintCabangByUserId(userProfileDto.getUserId(), "");
					String strTempCabang = "";
					if (listTempCabang.size() != 0) {
						for (int i = 0; i < listTempCabang.size(); i++) {
							if (i == 0) {
								strTempCabang = strTempCabang+listTempCabang.get(i).getNamaCabang();
							}else {
								strTempCabang = strTempCabang+", "+listTempCabang.get(i).getNamaCabang();
							}
						}
					}
					userProfileDto.setNamaCabang(strTempCabang);
				}
				listUserProfile.add(userProfileDto);
			}
		}
		return listUserProfile;
	}

	@Override
	public void saveUserProfile(String USER_ID, String KODE_KARYAWAN, String NAMA_KARYAWAN,
			String AUTHORITY_ID, String IMEI_NUMBER, String PHONE_NUMBER,
			String AKTIF_STATUS, String CREATE_USER, String PASSWORD, String KODE_KOLEKTOR) {
		userProfileDao.saveUserProfile(USER_ID, KODE_KARYAWAN, NAMA_KARYAWAN, AUTHORITY_ID, IMEI_NUMBER, PHONE_NUMBER, AKTIF_STATUS, CREATE_USER, PASSWORD, KODE_KOLEKTOR);
		
	}

	@Override
	public void updateUserProfile(String KODE_KARYAWAN, String NAMA_KARYAWAN, String AUTHORITY_ID,
			String IMEI_NUMBER, String PHONE_NUMBER, String AKTIF_STATUS,
			String MODIFY_USER, String USER_ID, String KODE_KOLEKTOR) {
		userProfileDao.updateUserProfile(KODE_KARYAWAN, NAMA_KARYAWAN, AUTHORITY_ID, IMEI_NUMBER, PHONE_NUMBER, AKTIF_STATUS, MODIFY_USER, USER_ID, KODE_KOLEKTOR);
		
	}

	@Override
	public void disableUserProfile(String AKTIF_STATUS, String MODIFY_USER,
			String USER_ID) {
		userProfileDao.disableUser(AKTIF_STATUS, MODIFY_USER, USER_ID);
		
	}

	@Override
	public void resetPassword(String PASSWORD, String MODIFY_USER,
			String USER_ID) {
		userProfileDao.resetPassword(PASSWORD, MODIFY_USER, USER_ID);
		
	}

	@Override
	public void changePassworUserProfile(String PASSWORD, String MODIFY_USER,
			String USER_ID) {
		userProfileDao.changePasswordUserProfile(PASSWORD, MODIFY_USER, USER_ID);
		
	}

	@Override
	public List<UserProfileDto> findUserAllByKodeCabangAndSearch(
			String kodeCabang, String keySearch) {
		List<Object[]> listUserObj = userProfileDao.findUserAllByKodeCabangAndSearch(kodeCabang, keySearch);
		List<UserProfileDto> listUserProfile = new ArrayList<UserProfileDto>();
		if (listUserObj.size() != 0) {
			for (Object[] obj : listUserObj) {
				UserProfileDto userProfileDto = new UserProfileDto();
				userProfileDto.setUserId(obj[0].toString());
				userProfileDto.setNamaKaryawan(obj[1].toString());
				userProfileDto.setKeteranganStatus(obj[2].toString());
				userProfileDto.setAuthorityId(obj[3].toString());
				userProfileDto.setAuthorityName(obj[4].toString());
				userProfileDto.setImeiNumber(obj[5].toString());
				userProfileDto.setPhoneNumber(obj[6].toString());
				userProfileDto.setKodeKolektor(obj[7].toString());
				userProfileDto.setItemList(false);
				List<AksesRegionCabangDto> listTempRegional = new ArrayList<AksesRegionCabangDto>();
				List<AksesRegionCabangDto> listTempCabang = new ArrayList<AksesRegionCabangDto>();
				listTempRegional = aksesRegionCabangSvc.findRegionByUserId(userProfileDto.getUserId(), "");
				String strTempRegional = "";
				if (listTempRegional.size() != 0) {
					for (int i = 0; i < listTempRegional.size(); i++) {
						if (i == 0) {
							strTempRegional = strTempRegional+listTempRegional.get(i).getNamaRegional();
						}else {
							strTempRegional = strTempRegional+", "+listTempRegional.get(i).getNamaRegional();
						}
					}
				}
				listTempCabang = aksesRegionCabangSvc.fintCabangByUserId(userProfileDto.getUserId(), "");
				String strTempCabang = "";
				if (listTempCabang.size() != 0) {
					for (int i = 0; i < listTempCabang.size(); i++) {
						if (i == 0) {
							strTempCabang = strTempCabang+listTempCabang.get(i).getNamaCabang();
						}else {
							strTempCabang = strTempCabang+", "+listTempCabang.get(i).getNamaCabang();
						}
					}
				}
				userProfileDto.setNamaRegional(strTempRegional);
				userProfileDto.setNamaCabang(strTempCabang);
				listUserProfile.add(userProfileDto);
			}
		}
		return listUserProfile;
	}
	
	@Override
	public List<UserProfileDto> findUserByBranchAndRegional(String kodeCabang,
			String kodeRegional) {
		List<Object[]> listUserObj = userProfileDao.findUserByBranchAndRegional(kodeCabang, kodeRegional);
		List<UserProfileDto> listUserProfile = new ArrayList<UserProfileDto>();
		
		if (listUserObj.size() != 0) {
			for (Object[] obj : listUserObj) {
				UserProfileDto userProfileDto = new UserProfileDto();
				userProfileDto.setUserId(obj[0].toString());
				userProfileDto.setNamaKaryawan(obj[1].toString());
				listUserProfile.add(userProfileDto);
			}
		}
		return listUserProfile;

	}

	@Override
	public List<UserProfileDto> findUserByUserIdAndPass(String userId,
			String pass) {
		List<Object[]> listUserObj = userProfileDao.findUserByUserIdAndPass(userId, pass);
		List<UserProfileDto> listUserProfile = new ArrayList<UserProfileDto>();
		if (listUserObj.size() != 0) {
			for (Object[] obj : listUserObj) {
				UserProfileDto userProfileDto = new UserProfileDto();
				userProfileDto.setUserId(obj[0].toString());
				userProfileDto.setAktifStatus(Integer.valueOf(obj[1].toString()));
				userProfileDto.setAuthorityId(obj[2].toString());
				userProfileDto.setPassword(obj[3].toString());
				userProfileDto.setNamaKaryawan(obj[4].toString());
				userProfileDto.setAuthorityName(obj[5].toString());
				listUserProfile.add(userProfileDto);
			}
		}
		return listUserProfile;
	}

	@Override
	public List<UserProfileDto> findUserAllByKodeRegionalKodeCabangAndSearch(
			String kodeRegional,String kodeCabang, String keySearch) {
		List<Object[]> listUserObj = userProfileDao.findUserAllByKodeRegionalKodeCabangAndSearch(kodeRegional, kodeCabang, keySearch);
		List<UserProfileDto> listUserProfile = new ArrayList<UserProfileDto>();
		if (listUserObj.size() != 0) {
			for (Object[] obj : listUserObj) {
				UserProfileDto userProfileDto = new UserProfileDto();
				userProfileDto.setUserId(obj[0].toString());
				userProfileDto.setNamaKaryawan(obj[1].toString());
				userProfileDto.setKeteranganStatus(obj[2].toString());
				userProfileDto.setAuthorityId(obj[3].toString());
				userProfileDto.setAuthorityName(obj[4].toString());
				userProfileDto.setImeiNumber(obj[5].toString());
				userProfileDto.setPhoneNumber(obj[6].toString());
				userProfileDto.setKodeKolektor(obj[7].toString());
				userProfileDto.setItemList(false);
				List<AksesRegionCabangDto> listTempRegional = new ArrayList<AksesRegionCabangDto>();
				List<AksesRegionCabangDto> listTempCabang = new ArrayList<AksesRegionCabangDto>();
				listTempRegional = aksesRegionCabangSvc.findRegionByUserId(userProfileDto.getUserId(), "");
				String strTempRegional = "";
				if (listTempRegional.size() != 0) {
					for (int i = 0; i < listTempRegional.size(); i++) {
						if (i == 0) {
							strTempRegional = strTempRegional+listTempRegional.get(i).getNamaRegional();
						}else {
							strTempRegional = strTempRegional+", "+listTempRegional.get(i).getNamaRegional();
						}
					}
				}
				listTempCabang = aksesRegionCabangSvc.fintCabangByUserId(userProfileDto.getUserId(), "");
				String strTempCabang = "";
				if (listTempCabang.size() != 0) {
					for (int i = 0; i < listTempCabang.size(); i++) {
						if (i == 0) {
							strTempCabang = strTempCabang+listTempCabang.get(i).getNamaCabang();
						}else {
							strTempCabang = strTempCabang+", "+listTempCabang.get(i).getNamaCabang();
						}
					}
				}
				userProfileDto.setNamaRegional(strTempRegional);
				userProfileDto.setNamaCabang(strTempCabang);
				listUserProfile.add(userProfileDto);
			}
		}
		return listUserProfile;
	}
	
}
