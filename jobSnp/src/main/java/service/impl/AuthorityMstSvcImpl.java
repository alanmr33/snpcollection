package service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dao.AuthorityMstDao;
import dto.AuthorityMstDto;
import entity.AuthorityMst;
import service.AuthorityMstSvc;


@Service("authorityMstSvc")
@Transactional
public class AuthorityMstSvcImpl implements AuthorityMstSvc{
	
	@Autowired
	private AuthorityMstDao authorityMstDao;
	
	@Override
	public List<AuthorityMstDto> findAuthorityByAuthorityId(int authorityId) {
		List<Object[]> listauthority = authorityMstDao.findAuthorityByAuthorityId(authorityId);
		List<AuthorityMstDto> listAuthorityMstDto = new ArrayList<AuthorityMstDto>();
		for (Object[] obj : listauthority) {
			AuthorityMstDto authorityMstDto = new AuthorityMstDto();
			authorityMstDto.setAuthorityId(Integer.parseInt(obj[0].toString()));
			authorityMstDto.setAuthorityName(obj[1].toString());
			if(Integer.parseInt(obj[2].toString())==0){
			authorityMstDto.setAktif("Disable");
			}else{
				authorityMstDto.setAktif("Enable");	
			}
			listAuthorityMstDto.add(authorityMstDto);
		}
		return listAuthorityMstDto;
	}

	@Override
	public List<AuthorityMstDto> searchAuthority(String search) {
		List<Object[]> listauthority = authorityMstDao.searchAuthority(search);
		List<AuthorityMstDto> listAuthorityMstDto = new ArrayList<AuthorityMstDto>();
		for (Object[] obj : listauthority) {
			AuthorityMstDto authorityMstDto = new AuthorityMstDto();
			authorityMstDto.setAuthorityId(Integer.parseInt(obj[0].toString()));
			authorityMstDto.setAuthorityName(obj[1].toString());
			if(Integer.parseInt(obj[2].toString())==0){
			authorityMstDto.setAktif("Disable");
			}else{
				authorityMstDto.setAktif("Enable");	
			}
			listAuthorityMstDto.add(authorityMstDto);
		}
		return listAuthorityMstDto;
		}
	@Override
	public void saveAuthority(String authorityName,
			int aktifStatus, String createUser) {
		authorityMstDao.saveAuthority(authorityName, aktifStatus, createUser);
		
	}

	@Override
	public void updateAuthority(String authorityName, int aktifStatus,
			String modifyUser, int authorityID) {
		authorityMstDao.updateAuthority(authorityName, aktifStatus, modifyUser, authorityID);
	}

	@Override
	public void deleteAuthority(String modifyUser, int authorityID) {
		authorityMstDao.deleteAuthority(modifyUser, authorityID);
		
	}

	@Override
	public AuthorityMstDto findOneAuthority(int authorityId) {
		AuthorityMst authorityMst = authorityMstDao.findOneAuthority(authorityId);
		AuthorityMstDto authorityMstDto = new AuthorityMstDto();
		if(authorityMst!=null){
			authorityMstDto.setId(authorityMst.getAuthorityId());
		}
		return authorityMstDto;
	}

	@Override
	public List<AuthorityMstDto> searchAllAuthorityActive(String search) {
		List<Object[]> listauthority = authorityMstDao.searchAllAuthorityActive(search);
		List<AuthorityMstDto> listAuthorityMstDto = new ArrayList<AuthorityMstDto>();
		for (Object[] obj : listauthority) {
			AuthorityMstDto authorityMstDto = new AuthorityMstDto();
			authorityMstDto.setAuthorityId(Integer.parseInt(obj[0].toString()));
			authorityMstDto.setAuthorityName(obj[1].toString());
			authorityMstDto.setItemList(true);
			if(Integer.parseInt(obj[2].toString())==0){
			authorityMstDto.setAktif("Disable");
			}else{
				authorityMstDto.setAktif("Enable");	
			}
			listAuthorityMstDto.add(authorityMstDto);
		}
		return listAuthorityMstDto;
	}
		
}
