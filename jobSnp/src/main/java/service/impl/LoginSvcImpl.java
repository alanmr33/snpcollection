package service.impl;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Hex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import service.AuthorityListSvc;
import service.AuthorityMstSvc;
import service.MenuMstSvc;
import service.LoginSvc;
import service.UserProfileSvc;
import dao.AuthorityListDao;
import dao.AuthorityMstDao;
import dao.MenuMstDao;
import dao.UserProfileDao;
import dto.AuthorityListDto;
import dto.AuthorityMstDto;
import dto.MenuMstDto;
import dto.UserProfileDto;
import dto.MenuUrlDto;
import entity.UserMenuAccordion;
import entity.UserProfile;

@Service("loginSvc")
@Transactional(readOnly = true)
public class LoginSvcImpl implements LoginSvc {

	@Autowired
	private AuthorityListDao authorityListDao;

	@Autowired
	private AuthorityListSvc authorityListSvc;

	@Autowired
	private UserProfileDao userProfileDao;
	
	@Autowired
	private UserProfileSvc userProfileSvc;

	public Map<String, Object> findRoleMenu(String userId, String password)
			throws NoSuchAlgorithmException {
		Map<String, Object> map = new HashMap<>();
		List<UserMenuAccordion> userMenuAccordions = new ArrayList<>();
		UserProfileDto userProfileDto = new UserProfileDto();
		List<UserProfileDto> listUserProfileDto = new ArrayList<UserProfileDto>();
		if (userProfileSvc.findUserByUserIdAndPass(userId, password) != null) {
			listUserProfileDto = userProfileSvc.findUserByUserIdAndPass(userId, password);
		}
		if (listUserProfileDto.size() != 0) {

			for (int i = 0; i < listUserProfileDto.size(); i++) {
				userProfileDto.setUserId(listUserProfileDto.get(i).getUserId());
				userProfileDto.setKodeCabang(listUserProfileDto.get(i)
						.getKodeCabang());
				userProfileDto.setKodeRegion(listUserProfileDto.get(i)
						.getKodeRegion());
				userProfileDto.setAktifStatus(listUserProfileDto.get(i)
						.getAktifStatus());
				userProfileDto.setAuthorityId(listUserProfileDto.get(i)
						.getAuthorityId());
				userProfileDto.setNamaKaryawan(listUserProfileDto.get(i)
						.getNamaKaryawan());
				userProfileDto.setAuthorityName(listUserProfileDto.get(i)
						.getAuthorityName());
			}
			if (userProfileDto.getUserId() != null) {
				map.put("userProfile", userProfileDto);
				List<AuthorityListDto> listAuthoriityListDto = authorityListSvc.findAuthorityListByAuthorityId(
						userProfileDto.getAuthorityId());
				if (listAuthoriityListDto != null) {
					for (int j = 0; j < listAuthoriityListDto.size(); j++) {
						String menuUrl = listAuthoriityListDto.get(j).getUrl();
						UserMenuAccordion userMenuAccordion = new UserMenuAccordion();
						if (menuUrl.equals("masterUser")) {
							userMenuAccordion = new UserMenuAccordion(
									"MASTER USER", MenuUrlDto.MASTER_USER);
						} else if (menuUrl.equals("applicationRoles")) {
							userMenuAccordion = new UserMenuAccordion(
									"AKSES MENU", MenuUrlDto.APPLICATION_ROLES);
						} else if (menuUrl.equals("changePassword")) {
							userMenuAccordion = new UserMenuAccordion(
									"UBAH KATA SANDI", MenuUrlDto.CHANGE_PASSWORD);
						}else if (menuUrl.equals("masterRegion")) {
							userMenuAccordion = new UserMenuAccordion(
									"MASTER REGIONAL",
									MenuUrlDto.MASTER_REGIONAL);
						} else if (menuUrl.equals("masterCabang")) {
							userMenuAccordion = new UserMenuAccordion(
									"MASTER CABANG", MenuUrlDto.MASTER_CABANG);
						} else if (menuUrl.equals("masterMenu")) {
							userMenuAccordion = new UserMenuAccordion(
									"MASTER MENU", MenuUrlDto.MASTER_MENU);
						} else if (menuUrl.equals("masterAuthority")) {
							userMenuAccordion = new UserMenuAccordion(
									"MASTER AUTHORITY", MenuUrlDto.MASTER_AUTHORITY);
						} else if (menuUrl.equals("masterProvinsi")) {
							userMenuAccordion = new UserMenuAccordion(
									"MASTER PROVINSI", MenuUrlDto.MASTER_PROVINSI);
						} else if (menuUrl.equals("uploadHandling")) {
							userMenuAccordion = new UserMenuAccordion(
									"UNGGAH PENANGANAN", MenuUrlDto.UPLOAD_HANDLING);
						} else if (menuUrl.equals("notification")) {
							userMenuAccordion = new UserMenuAccordion(
									"PEMBERITAHUAN", MenuUrlDto.NOTIFICATION);
						} else if (menuUrl.equals("reassignment")) {
							userMenuAccordion = new UserMenuAccordion(
									"PENUGASAN ULANG", MenuUrlDto.REASSIGNMENT);
						} else if (menuUrl.equals("taskAdjustment")) {
							userMenuAccordion = new UserMenuAccordion(
									"KOREKSI", MenuUrlDto.TASK_ADJUSTMENT);
						} else if (menuUrl.equals("mobileTracking")) {
							userMenuAccordion = new UserMenuAccordion(
									"MOBILE TRACKING", MenuUrlDto.MOBILE_TRACKING);
						} else if (menuUrl.equals("reportKunjunganKolektor")) {
							userMenuAccordion = new UserMenuAccordion(
									"KUNJUNGAN KOLEKTOR", MenuUrlDto.REPORT_KUNJUNGAN_KOLEKTOR);
						} else if (menuUrl.equals("rekonKolektor")) {
							userMenuAccordion = new UserMenuAccordion(
									"REKON KOLEKTOR", MenuUrlDto.REKON_KOLEKTOR);
						} else if (menuUrl.equals("collectorFullData")) {
							userMenuAccordion = new UserMenuAccordion(
									"KOLLEKTOR FULL DATA", MenuUrlDto.COLLECTOR_FULL_DATA);
						} else if (menuUrl.equals("masterHandling")) {
							userMenuAccordion = new UserMenuAccordion(
									"MASTER HANDLING", MenuUrlDto.MASTER_HANDLING);
						} else if (menuUrl.equals("masterParameter")) {
							userMenuAccordion = new UserMenuAccordion(
									"MASTER PARAMETER", MenuUrlDto.MASTER_PARAMETER);
						} else if (menuUrl.equals("masterForms")) {
							userMenuAccordion = new UserMenuAccordion(
									"MASTER FORMS", MenuUrlDto.Master_FORMS);
						} else if (menuUrl.equals("masterEvents")) {
							userMenuAccordion = new UserMenuAccordion(
									"MASTER EVENTS", MenuUrlDto.MASTER_EVENTS);
						} else if (menuUrl.equals("formRows")) {
							userMenuAccordion = new UserMenuAccordion(
									"FORM ROWS", MenuUrlDto.FORM_ROWS);
						} 
						userMenuAccordions.add(userMenuAccordion);
					}
					map.put("listMenu", userMenuAccordions);
				} else {
					map.put("userProfile", userProfileDto);
					map.put("listMenu", userMenuAccordions);
					return map;
				}
			} else {
				map.put("userProfile", userProfileDto);
				map.put("listMenu", userMenuAccordions);
				return map;
			}
		}

		return map;

	}

}
