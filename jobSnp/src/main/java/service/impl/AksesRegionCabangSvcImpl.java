package service.impl;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Hex;
import org.apache.poi.hssf.record.DSFRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import service.AksesRegionCabangSvc;
import service.AuthorityMstSvc;
import service.CabangMstSvc;
import service.MenuMstSvc;
import service.LoginSvc;
import service.UserProfileSvc;
import dao.AksesRegionCabangDao;
import dao.AuthorityMstDao;
import dao.CabangMstDao;
import dao.MenuMstDao;
import dao.UserProfileDao;
import dto.AksesRegionCabangDto;
import dto.AuthorityMstDto;
import dto.CabangMstDto;
import dto.MenuMstDto;
import dto.UserProfileDto;
import dto.MenuUrlDto;
import entity.CabangMst;
import entity.UserMenuAccordion;
import entity.UserProfile;

@Service("aksesRegionCabangSvc")
@Transactional
public class AksesRegionCabangSvcImpl implements AksesRegionCabangSvc {
	
	@Autowired
	private AksesRegionCabangDao aksesRegionCabangDao;

	@Override
	public List<AksesRegionCabangDto> findRegionByUserId(String userId, String keySearch) {
		List<Object[]> listUserObj = aksesRegionCabangDao.findAksesRegionByUserId(userId, keySearch);
		List<AksesRegionCabangDto> listAksesRegionCabangDto = new ArrayList<AksesRegionCabangDto>();
		if (listUserObj.size() != 0) {
			for (Object[] obj : listUserObj) {
				AksesRegionCabangDto aksesRegionCabangDto = new AksesRegionCabangDto();
				aksesRegionCabangDto.setUserId(obj[0].toString());
				aksesRegionCabangDto.setKodeRegion(obj[1].toString());
				aksesRegionCabangDto.setNamaRegional(obj[2].toString());
				listAksesRegionCabangDto.add(aksesRegionCabangDto);
			}
		}
		return listAksesRegionCabangDto;
	}

	@Override
	public List<AksesRegionCabangDto> fintCabangByUserId(String userId, String keySearch) {
		List<Object[]> listUserObj = aksesRegionCabangDao.findAksesCabangByUserId(userId, keySearch);
		List<AksesRegionCabangDto> listAksesRegionCabangDto = new ArrayList<AksesRegionCabangDto>();
		if (listUserObj.size() != 0) {
			for (Object[] obj : listUserObj) {
				AksesRegionCabangDto aksesRegionCabangDto = new AksesRegionCabangDto();
				aksesRegionCabangDto.setUserId(obj[0].toString());
				aksesRegionCabangDto.setKodeCabang(obj[1].toString());
				aksesRegionCabangDto.setNamaCabang(obj[2].toString());
				listAksesRegionCabangDto.add(aksesRegionCabangDto);
			}
		}
		return listAksesRegionCabangDto;
	}

	@Override
	public void insertRegion(String userID, String kodeRegion) {
		aksesRegionCabangDao.insertRegion(userID, kodeRegion);
		
	}

	@Override
	public void insertCabang(String userID, String kodeCabang) {
		aksesRegionCabangDao.insertCabang(userID, kodeCabang);
		
	}

	@Override
	public void deleteRegion(String userID) {
		aksesRegionCabangDao.deleteRegionByUserId(userID);
		
	}

	@Override
	public void deleteCabang(String userID) {
		aksesRegionCabangDao.deleteCabangByUserId(userID);
		
	}

	@Override
	public List<AksesRegionCabangDto> fintCabangByUserIdAndKodeRegion(
			String userId, String kodeRegion, String keySearch) {
		List<Object[]> listUserObj = aksesRegionCabangDao.findAksesCabangByUserIdAndRegion(userId, kodeRegion, keySearch);
		List<AksesRegionCabangDto> listAksesRegionCabangDto = new ArrayList<AksesRegionCabangDto>();
		if (listUserObj.size() != 0) {
			for (Object[] obj : listUserObj) {
				AksesRegionCabangDto aksesRegionCabangDto = new AksesRegionCabangDto();
				aksesRegionCabangDto.setUserId(obj[0].toString());
				aksesRegionCabangDto.setKodeCabang(obj[1].toString());
				aksesRegionCabangDto.setNamaCabang(obj[2].toString());
				listAksesRegionCabangDto.add(aksesRegionCabangDto);
			}
		}
		return listAksesRegionCabangDto;
	}
	
	
}
