package service.impl;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.MasterHandlingDao;
import dto.MasterHandlingDto;
import entity.MasterHandling;
import service.MasterHandlingSvc;

@Service("masterHandlingSvc")
@Transactional
public class MasterHandlingSvcImpl implements MasterHandlingSvc {

	@Autowired
	private MasterHandlingDao masterHandlingDao;
	
	@Override
	public List<MasterHandlingDto> findAllHandling(String search, int batchHand) {
		List<Object[]> listHandling = masterHandlingDao.findAllHandling(search,batchHand);
		List<MasterHandlingDto> listHandlingDto = new ArrayList<MasterHandlingDto>();
		for (Object[] obj : listHandling) {
			MasterHandlingDto handlingDto = new MasterHandlingDto();
			handlingDto.setHandNo(Integer.parseInt(obj[0].toString()));
			handlingDto.setHandCode(obj[1].toString());
			handlingDto.setPromise(obj[2].toString());
			handlingDto.setDeskripsi(obj[3].toString());
			if(obj[4].toString().equals("0")||obj[4].toString().equals(null)){
				handlingDto.setAktif("Disabled");
			}else{
				handlingDto.setAktif("Enabled");
			}
			listHandlingDto.add(handlingDto);
		}
		return listHandlingDto;
	}
	
	@Override
	public List<MasterHandlingDto> findAllHandlingByBatchHandling(int batchHandling) {
		List<Object[]> listHandling = masterHandlingDao.findAllHandlingByBatchHandling(batchHandling);
		List<MasterHandlingDto> listHandlingDto = new ArrayList<MasterHandlingDto>();
		for (Object[] obj : listHandling) {
			MasterHandlingDto handlingDto = new MasterHandlingDto();
			handlingDto.setHandNo(Integer.parseInt(obj[0].toString()));
			handlingDto.setHandCode(obj[1].toString());
			handlingDto.setPromise(obj[2].toString());
			handlingDto.setDeskripsi(obj[3].toString());
			if(obj[4].toString().equals("0")||obj[4].toString().equals(null)){
				handlingDto.setAktif("Disabled");
			}else{
				handlingDto.setAktif("Enabled");
			}
			listHandlingDto.add(handlingDto);
		}
		return listHandlingDto;
	}

	@Override
	public MasterHandlingDto findOneHandling(int handNo) {
		MasterHandling handling= masterHandlingDao.findOneHandling(handNo);
		MasterHandlingDto handDto = new MasterHandlingDto();
		if(handling!=null){
			handDto.setHandNo(handling.getBatchHandlingNo());
		}
		return handDto;
	}

	@Override
	public void insertHandling(int batchHandling, String handCode, String promise,
			String deskripsi, String aktif, String createUser) {
		masterHandlingDao.insertHandling(batchHandling, handCode, promise, deskripsi, aktif, createUser);
		
	}

	@Override
	public void updateHandling(String handCode, String promise,
			String deskripsi, String aktif, String modifyUser, int handNo) {
		masterHandlingDao.updateHandling(handCode, promise, deskripsi, aktif, modifyUser, handNo);
		
	}

	@Override
	public void deleteHandling(String modifyUser, int handNo, String handCode) {
		masterHandlingDao.deleteHandling(modifyUser, handNo, handCode);
		
	}

	@Override
	public int maxBatchHandling() {
		int max = 0;
		max = masterHandlingDao.maxBatchHandling();
		return max;
	}

}
