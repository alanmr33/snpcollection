package service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.ManualAssignDao;
import dto.ManualAssignDto;
import service.ManualAssignSvc;

@Service("manualAssignSvc")
@Transactional
public class ManualAssignSvcImpl implements ManualAssignSvc{

	@Autowired
	private ManualAssignDao manualAssignDao;
	
	@Override
	public List<ManualAssignDto> findAllManualAssign(String search) {
		List<Object[]> listAssign = manualAssignDao.findAllManualAssign(search);
		List<ManualAssignDto> listAssignDto = new ArrayList<>();
		for(Object[] obj : listAssign){
			ManualAssignDto manualAssignDto = new ManualAssignDto();
			manualAssignDto.setInstalmentNo(Integer.parseInt(obj[0].toString()));
			manualAssignDto.setContractNo(obj[1].toString());
			manualAssignDto.setOrderId(Integer.parseInt(obj[2].toString()));
			manualAssignDto.setLpkNo(obj[3].toString());
			manualAssignDto.setCustomerName(obj[4].toString());
			manualAssignDto.setAddress(obj[5].toString());
			manualAssignDto.setPhoneNum(obj[6].toString());
			manualAssignDto.setNotes(obj[7].toString());
			manualAssignDto.setDueDate(obj[8].toString());
			listAssignDto.add(manualAssignDto);
		}
		return listAssignDto;
	}

	@Override
	public void insertAssign(int seqNo, String colCode,  String newColCode,
			Date periodFrom, Date periodTo, String notes, int orderId) {
		manualAssignDao.insertAssign(seqNo,colCode, newColCode, periodFrom, periodTo, notes, orderId);
		
	}

	@Override
	public void updateAssign(String newCol, String reassignBy,String colPrev,String notes,int orderId) {
		manualAssignDao.updateAssign(newCol, reassignBy, colPrev, notes, orderId);
	}

	@Override
	public List<ManualAssignDto> findManualAssign() {
		List<Object[]> listAssign = manualAssignDao.findManualAssign();
		List<ManualAssignDto> listAssignDto = new ArrayList<>();
		for(Object[] obj : listAssign){
			ManualAssignDto manualAssignDto = new ManualAssignDto();
			manualAssignDto.setInstalmentNo(Integer.parseInt(obj[0].toString()));
			manualAssignDto.setContractNo(obj[1].toString());
			manualAssignDto.setOrderId(Integer.parseInt(obj[2].toString()));
			manualAssignDto.setLpkNo(obj[3].toString());
			manualAssignDto.setCustomerName(obj[4].toString());
			manualAssignDto.setAddress(obj[5].toString());
			manualAssignDto.setPhoneNum(obj[6].toString());
			manualAssignDto.setNotes(obj[7].toString());
			manualAssignDto.setDueDate(obj[8].toString());
			listAssignDto.add(manualAssignDto);
		}
		return listAssignDto;
	}

	@Override
	public List<ManualAssignDto> findAssignRegionalCabang(String regionalCode,
			String cabangCode, String search) {
		List<Object[]> listAssign = manualAssignDao.findAssignRegionalCabang(regionalCode, cabangCode, search);
		List<ManualAssignDto> listAssignDto = new ArrayList<>();
		for(Object[] obj : listAssign){
			ManualAssignDto manualAssignDto = new ManualAssignDto();
			manualAssignDto.setInstalmentNo(Integer.parseInt(obj[0].toString()));
			manualAssignDto.setContractNo(obj[1].toString());
			manualAssignDto.setOrderId(Integer.parseInt(obj[2].toString()));
			manualAssignDto.setLpkNo(obj[3].toString());
			manualAssignDto.setCustomerName(obj[4].toString());
			manualAssignDto.setAddress(obj[5].toString());
			manualAssignDto.setPhoneNum(obj[6].toString());
			manualAssignDto.setNotes(obj[7].toString());
			manualAssignDto.setDueDate(obj[8].toString());
			manualAssignDto.setKodeKolektor(obj[9].toString());
			manualAssignDto.setNamaKaryawan(obj[10].toString());
			listAssignDto.add(manualAssignDto);
		}
		return listAssignDto;

	}

	@Override
	public List<ManualAssignDto> findAssignRegionalCabangSurveyor(
			String regionalCode, String cabangCode, String kodeKol,
			String search) {
		List<Object[]> listAssign = manualAssignDao.findAssignRegionalCabangSurveyor(regionalCode, cabangCode, kodeKol, search);
		List<ManualAssignDto> listAssignDto = new ArrayList<>();
		for(Object[] obj : listAssign){
			ManualAssignDto manualAssignDto = new ManualAssignDto();
			manualAssignDto.setInstalmentNo(Integer.parseInt(obj[0].toString()));
			manualAssignDto.setContractNo(obj[1].toString());
			manualAssignDto.setOrderId(Integer.parseInt(obj[2].toString()));
			manualAssignDto.setLpkNo(obj[3].toString());
			manualAssignDto.setCustomerName(obj[4].toString());
			manualAssignDto.setAddress(obj[5].toString());
			manualAssignDto.setPhoneNum(obj[6].toString());
			manualAssignDto.setNotes(obj[7].toString());
			manualAssignDto.setDueDate(obj[8].toString());
			listAssignDto.add(manualAssignDto);
		}
		return listAssignDto;
	}

	@Override
	public List<ManualAssignDto> findAssignCabang(String cabangCode,
			String search) {
		List<Object[]> listAssign = manualAssignDao.findAssignCabang(cabangCode, search);
		List<ManualAssignDto> listAssignDto = new ArrayList<>();
		for(Object[] obj : listAssign){
			ManualAssignDto manualAssignDto = new ManualAssignDto();
			manualAssignDto.setInstalmentNo(Integer.parseInt(obj[0].toString()));
			manualAssignDto.setContractNo(obj[1].toString());
			manualAssignDto.setOrderId(Integer.parseInt(obj[2].toString()));
			manualAssignDto.setLpkNo(obj[3].toString());
			manualAssignDto.setCustomerName(obj[4].toString());
			manualAssignDto.setAddress(obj[5].toString());
			manualAssignDto.setPhoneNum(obj[6].toString());
			manualAssignDto.setNotes(obj[7].toString());
			manualAssignDto.setDueDate(obj[8].toString());
			manualAssignDto.setKodeKolektor(obj[9].toString());
			manualAssignDto.setNamaKaryawan(obj[10].toString());
			listAssignDto.add(manualAssignDto);
		}
		return listAssignDto;
	}

	@Override
	public List<ManualAssignDto> findAssignCabangSurveyor(String cabangCode,
			String kodeKol, String search) {
		List<Object[]> listAssign = manualAssignDao.findAssignCabangSurveyor(cabangCode, kodeKol, search);
		List<ManualAssignDto> listAssignDto = new ArrayList<>();
		for(Object[] obj : listAssign){
			ManualAssignDto manualAssignDto = new ManualAssignDto();
			manualAssignDto.setInstalmentNo(Integer.parseInt(obj[0].toString()));
			manualAssignDto.setContractNo(obj[1].toString());
			manualAssignDto.setOrderId(Integer.parseInt(obj[2].toString()));
			manualAssignDto.setLpkNo(obj[3].toString());
			manualAssignDto.setCustomerName(obj[4].toString());
			manualAssignDto.setAddress(obj[5].toString());
			manualAssignDto.setPhoneNum(obj[6].toString());
			manualAssignDto.setNotes(obj[7].toString());
			manualAssignDto.setDueDate(obj[8].toString());
			manualAssignDto.setKodeKolektor(obj[9].toString());
			manualAssignDto.setNamaKaryawan(obj[10].toString());
			listAssignDto.add(manualAssignDto);
		}
		return listAssignDto;
	}

	@Override
	public int maxSeqnoBByUserId(String kodeKolektor) {
		int max = 0;
		max = manualAssignDao.maxSeqnoBByUserId(kodeKolektor);
		return max;
	}

}
