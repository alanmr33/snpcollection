package service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.CollectionDetailDao;
import dao.CollectionHeaderDao;
import dto.CollectionDetailDto;
import dto.CollectionHeaderDto;
import dto.ManualAssignDto;
import dto.MobileTrackingDto;
import dto.ReassignCollector;
import service.CollectionDetailSvc;
import service.CollectionHeaderSvc;

@Service("collectionDetailSvc")
@Transactional
public class CollectionDetailSvcImpl implements CollectionDetailSvc{
	
	@Autowired
	private CollectionDetailDao collectionDetailDao;

	@Override
	public void insertCollectionDetailInUploadManual(int orderId,
			String customerCode, String contractNo, int instalmentNo,
			String lpkNo, String customerName, String address, String village,
			String district, String rt, String rw, String city,
			String kodeProvinsi, String postCode, String dueDate,
			int installmentAmt, int depositAmt, int penaltyAmt,
			int collectionFee, int totalAmt, String receiptNo, int minPayment,
			int maxPayment, String repaymentDate, String notes,
			int promiseCount, int promiseAvailable, int promiseRange,
			String phoneArea, String phoneNumber, String mobilePrefix,
			String mobileNo, String namaBarang) {
		collectionDetailDao.insertCollectionDetailInUploadManual(orderId, customerCode, contractNo, instalmentNo, lpkNo, customerName, address, village, district, rt, rw, city, kodeProvinsi, postCode, dueDate, installmentAmt, depositAmt, penaltyAmt, collectionFee, totalAmt, receiptNo, minPayment, maxPayment, repaymentDate, notes, promiseCount, promiseAvailable, promiseRange, phoneArea, phoneNumber, mobilePrefix, mobileNo, namaBarang);
		
	}

	@Override
	public List<CollectionDetailDto> selectForTaskAdjustmentByKodeRegional(String kodeRegional, String keySearch) {
		List<Object[]> listObjek = collectionDetailDao.selectForTaskAdjustmentByKodeRegional(kodeRegional, keySearch);
		List<CollectionDetailDto> listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
		if (listObjek.size() != 0) {
			for (Object[] obj : listObjek) {
				CollectionDetailDto cd = new CollectionDetailDto();
				cd.setOrderId(Integer.valueOf(obj[0].toString()));
				cd.setCustomerName(obj[1].toString());
				cd.setAddress(obj[2].toString());
				cd.setNamaKaryawan(obj[3].toString());
				cd.setTagihan(obj[4].toString());
				cd.setNotes(obj[5].toString());
				cd.setContractNo(obj[6].toString());
				cd.setInstallmentNo(Integer.valueOf(obj[7].toString()));
				listCollectionDetailDto.add(cd);
			}
		}
		return listCollectionDetailDto;
	}

	@Override
	public List<CollectionDetailDto> selectReportKunjunganKodeCabang(Date dateAwal,
			Date dateAkhir, String kodeCabang, String keySearch) {
		List<Object[]> listObjek = collectionDetailDao.selectReportKunjunganKodeCabang(dateAwal, dateAkhir, kodeCabang, keySearch);
		List<CollectionDetailDto> listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
		if (listObjek.size() != 0) {
			for (Object[] obj : listObjek) {
				CollectionDetailDto cd = new CollectionDetailDto();
				cd.setCollectorCode(obj[0].toString());
				cd.setNamaKaryawan(obj[1].toString());
				cd.setNamaCabang(obj[2].toString());
				cd.setJumlahKunjungan(obj[3].toString());
				cd.setUserId(obj[4].toString());
				DateFormat df = new SimpleDateFormat("dd/MMM/yyyy");
				String temAwal = df.format(dateAwal);
				String temAkhir = df.format(dateAkhir);
				String intervalWakktu = temAwal+" s.d "+temAkhir;
				cd.setIntervalWaktu(intervalWakktu);
				listCollectionDetailDto.add(cd);
			}
		}
		return listCollectionDetailDto;
	}

	@Override
	public List<CollectionDetailDto> selectReportKunjunganKodeCabangKodeKolektor(Date dateAwal,
			Date dateAkhir, String kodeCabang, String kodeKolektor,
			String keySearch) {
		List<Object[]> listObjek = collectionDetailDao.selectReportKunjunganKodeCabangKodeKolektor(dateAwal, dateAkhir, kodeCabang,kodeKolektor, keySearch);
		List<CollectionDetailDto> listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
		if (listObjek.size() != 0) {
			for (Object[] obj : listObjek) {
				CollectionDetailDto cd = new CollectionDetailDto();
				cd.setCollectorCode(obj[0].toString());
				cd.setNamaKaryawan(obj[1].toString());
				cd.setNamaCabang(obj[2].toString());
				cd.setJumlahKunjungan(obj[3].toString());
				cd.setUserId(obj[4].toString());
				DateFormat df = new SimpleDateFormat("dd/MMM/yyyy");
				String temAwal = df.format(dateAwal);
				String temAkhir = df.format(dateAkhir);
				String intervalWakktu = temAwal+" s.d "+temAkhir;
				cd.setIntervalWaktu(intervalWakktu);
				listCollectionDetailDto.add(cd);
			}
		}
		return listCollectionDetailDto;
	}

	@Override
	public List<CollectionDetailDto> selectReportKunjunganDetailView(Date dateAwal,
			Date dateAkhir, String userId, String keySearch) {
		List<Object[]> listObjek = collectionDetailDao.selectDetailReportKunjungan(dateAwal, dateAkhir, userId, keySearch);
		List<CollectionDetailDto> listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
		if (listObjek.size() != 0) {
			for (Object[] obj : listObjek) {
				CollectionDetailDto cd = new CollectionDetailDto();
				cd.setCustomerName(obj[0].toString());
				cd.setTglBayar(obj[1].toString());
				cd.setTglJanjiBayar(obj[2].toString());
				cd.setNotes(obj[3].toString());
				cd.setJumlahBayar(obj[4].toString());
				cd.setTglKunjungan(obj[5].toString());
				cd.setStatus(obj[6].toString());
				cd.setContractNo(obj[7].toString());
				listCollectionDetailDto.add(cd);
			}
		}
		return listCollectionDetailDto;
	}

	

	@Override
	public List<CollectionDetailDto> selectRekonKolektorDetail(Date dateAwal,
			Date dateAkhir, String userId, String keySearch) {
		List<Object[]> listObjek = collectionDetailDao.selectDetailRekonKolektor(dateAwal, dateAkhir, userId, keySearch);
		List<CollectionDetailDto> listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
		if (listObjek.size() != 0) {
			for (Object[] obj : listObjek) {
				CollectionDetailDto cd = new CollectionDetailDto();
				cd.setContractNo(obj[0].toString());
				cd.setCustomerName(obj[1].toString());
				cd.setReceiptNo(obj[2].toString());
				cd.setAngsuran(obj[3].toString());
				cd.setJumlahKunjungan(obj[4].toString());
				cd.setKuitansiTertagih(obj[5].toString());
				cd.setPembayaran(obj[6].toString());
				listCollectionDetailDto.add(cd);
			}
		}
		return listCollectionDetailDto;
	}

	@Override
	public List<CollectionDetailDto> selectForTaskAdjustmentByKodeCabang(
			String koderCabang, String keySearch) {
		List<Object[]> listObjek = collectionDetailDao.selectForTaskAdjustmentByKodeCabang(koderCabang, keySearch);
		List<CollectionDetailDto> listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
		if (listObjek.size() != 0) {
			for (Object[] obj : listObjek) {
				CollectionDetailDto cd = new CollectionDetailDto();
				cd.setOrderId(Integer.valueOf(obj[0].toString()));
				cd.setCustomerName(obj[1].toString());
				cd.setAddress(obj[2].toString());
				cd.setNamaKaryawan(obj[3].toString());
				cd.setTagihan(obj[4].toString());
				cd.setNotes(obj[5].toString());
				cd.setContractNo(obj[6].toString());
				cd.setInstallmentNo(Integer.valueOf(obj[7].toString()));
				listCollectionDetailDto.add(cd);
			}
		}
		return listCollectionDetailDto;
	}

	@Override
	public List<CollectionDetailDto> selectForTaskAdjustmentByKodeCabangKodeKol(
			String koderCabang, String kodeKol, String keySearch) {
		List<Object[]> listObjek = collectionDetailDao.selectForTaskAdjustmentByKodeCabangKodeKol(koderCabang, kodeKol, keySearch);
		List<CollectionDetailDto> listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
		if (listObjek.size() != 0) {
			for (Object[] obj : listObjek) {
				CollectionDetailDto cd = new CollectionDetailDto();
				cd.setOrderId(Integer.valueOf(obj[0].toString()));
				cd.setCustomerName(obj[1].toString());
				cd.setAddress(obj[2].toString());
				cd.setNamaKaryawan(obj[3].toString());
				cd.setTagihan(obj[4].toString());
				cd.setNotes(obj[5].toString());
				cd.setContractNo(obj[6].toString());
				cd.setInstallmentNo(Integer.valueOf(obj[7].toString()));
				listCollectionDetailDto.add(cd);
			}
		}
		return listCollectionDetailDto;
	}

	@Override
	public List<CollectionDetailDto> selectForTaskAdjustmentByKodeRegionalKodeKol(
			String kodeRegional, String kodeKol, String keySearch) {
		List<Object[]> listObjek = collectionDetailDao.selectForTaskAdjustmentByKodeRegionalKodeKol(kodeRegional, kodeKol, keySearch);
		List<CollectionDetailDto> listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
		if (listObjek.size() != 0) {
			for (Object[] obj : listObjek) {
				CollectionDetailDto cd = new CollectionDetailDto();
				cd.setOrderId(Integer.valueOf(obj[0].toString()));
				cd.setCustomerName(obj[1].toString());
				cd.setAddress(obj[2].toString());
				cd.setNamaKaryawan(obj[3].toString());
				cd.setTagihan(obj[4].toString());
				cd.setNotes(obj[5].toString());
				cd.setContractNo(obj[6].toString());
				cd.setInstallmentNo(Integer.valueOf(obj[7].toString()));
				listCollectionDetailDto.add(cd);
			}
		}
		return listCollectionDetailDto;
	}

	@Override
	public List<CollectionDetailDto> selectForTaskAdjustmentByKodeRegionalKodeCabang(
			String koderRegional, String kodeCabang, String keySearch) {
		List<Object[]> listObjek = collectionDetailDao.selectForTaskAdjustmentByKodeRegionalKodeCabang(koderRegional, kodeCabang, keySearch);
		List<CollectionDetailDto> listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
		if (listObjek.size() != 0) {
			for (Object[] obj : listObjek) {
				CollectionDetailDto cd = new CollectionDetailDto();
				cd.setOrderId(Integer.valueOf(obj[0].toString()));
				cd.setCustomerName(obj[1].toString());
				cd.setAddress(obj[2].toString());
				cd.setNamaKaryawan(obj[3].toString());
				cd.setTagihan(obj[4].toString());
				cd.setNotes(obj[5].toString());
				cd.setContractNo(obj[6].toString());
				cd.setInstallmentNo(Integer.valueOf(obj[7].toString()));
				listCollectionDetailDto.add(cd);
			}
		}
		return listCollectionDetailDto;
	}

	@Override
	public List<CollectionDetailDto> selectReportKunjunganKodeRegionalKodeKolektor(
			Date dateAwal, Date dateAkhir, String kodeRegional,
			String kodeKolektor, String keySearch) {
		List<Object[]> listObjek = collectionDetailDao.selectReportKunjunganKodeRegionalKodeKolektor(dateAwal, dateAkhir, kodeRegional,kodeKolektor, keySearch);
		List<CollectionDetailDto> listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
		if (listObjek.size() != 0) {
			for (Object[] obj : listObjek) {
				CollectionDetailDto cd = new CollectionDetailDto();
				cd.setOrderId(Integer.valueOf(obj[0].toString()));
				cd.setCollectorCode(obj[1].toString());
				cd.setNamaKaryawan(obj[2].toString());
				cd.setNamaCabang(obj[3].toString());
				cd.setJumlahKunjungan(obj[4].toString());
				cd.setUserId(obj[5].toString());
				DateFormat df = new SimpleDateFormat("dd/MMM/yyyy");
				String temAwal = df.format(dateAwal);
				String temAkhir = df.format(dateAkhir);
				String intervalWakktu = temAwal+" s.d "+temAkhir;
				cd.setIntervalWaktu(intervalWakktu);
				listCollectionDetailDto.add(cd);
			}
		}
		return listCollectionDetailDto;
	}

	@Override
	public List<CollectionDetailDto> selectReportKunjunganKodeRegionalKodeCabang(
			Date dateAwal, Date dateAkhir, String kodeRegional,
			String kodeCabang, String keySearch) {
		List<Object[]> listObjek = collectionDetailDao.selectReportKunjunganKodeRegionalKodeCabang(dateAwal, dateAkhir, kodeRegional,kodeCabang, keySearch);
		List<CollectionDetailDto> listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
		if (listObjek.size() != 0) {
			for (Object[] obj : listObjek) {
				CollectionDetailDto cd = new CollectionDetailDto();
				cd.setCollectorCode(obj[0].toString());
				cd.setNamaKaryawan(obj[1].toString());
				cd.setNamaCabang(obj[2].toString());
				cd.setJumlahKunjungan(obj[3].toString());
				cd.setUserId(obj[4].toString());
				DateFormat df = new SimpleDateFormat("dd/MMM/yyyy");
				String temAwal = df.format(dateAwal);
				String temAkhir = df.format(dateAkhir);
				String intervalWakktu = temAwal+" s.d "+temAkhir;
				cd.setIntervalWaktu(intervalWakktu);
				listCollectionDetailDto.add(cd);
			}
		}
		return listCollectionDetailDto;
	}

	

	@Override
	public void updateCollectionHeadetToCMP(String status,int orderId) {
		collectionDetailDao.updateCollectionHeadetToCMP(status, orderId);
		
	}

	@Override
	public void updateCdToCloseInTaskAdjustment(String remark, int orderId) {
		collectionDetailDao.updateCdToCloseInTaskAdjustment(remark, orderId);
		
	}

	@Override
	public void updatePromiseCountPromisAvailable(int promiseCount,
			int promiseAvailable, int orderId, String contractNo) {
		collectionDetailDao.updatePromiseCountPromisAvailable(promiseCount, promiseAvailable, orderId, contractNo);
		
	}

	@Override
	public CollectionDetailDto selectCollectionDetailForInputPayment(
			int orderId, String contractNo, int instalmentNo) {
		List<Object[]> listObjek = collectionDetailDao.selectCollectionDetailForInputPayment(orderId, contractNo, instalmentNo);
		List<CollectionDetailDto> listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
		CollectionDetailDto cd = new CollectionDetailDto();
		if (listObjek.size() != 0) {
			for (Object[] obj : listObjek) {
				cd.setInstallmentNo(Integer.valueOf(obj[0].toString()));
				cd.setOrderId(Integer.valueOf(obj[1].toString()));
				cd.setCustomerCode(obj[2].toString());
				cd.setContractNo(obj[3].toString());
				cd.setCollectorCode(obj[4].toString());
				String temp = "0";
				String[] x = obj[5].toString().split(".");
				for (int j = 0; j < x.length; j++) {
					if (j == 0) {
						temp = x[j];
					}
				}
				cd.setTotalAmt(Integer.valueOf(temp));
				cd.setReceiptNo(obj[6].toString());
				cd.setLpkNo(obj[7].toString());
			}
		}
		return cd;
	}

	@Override
	public CollectionDetailDto selectCollectionDetailForInputHandling(
			int orderId, String contractNo, int instalmentNo) {
		List<Object[]> listObjek = collectionDetailDao.selectCollectionDetailForInputHandling(orderId, contractNo, instalmentNo);
		List<CollectionDetailDto> listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
		CollectionDetailDto cd = new CollectionDetailDto();
		if (listObjek.size() != 0) {
			for (Object[] obj : listObjek) {
				cd.setContractNo(obj[0].toString());
				cd.setInstallmentNo(Integer.valueOf(obj[1].toString()));
				cd.setOrderId(Integer.valueOf(obj[2].toString()));
				cd.setCustomerCode(obj[3].toString());
				cd.setLpkNo(obj[4].toString());
				cd.setNotes(obj[5].toString());
				cd.setPromiseCount(Integer.valueOf(obj[6].toString()));
				cd.setPromiseAvailable(Integer.valueOf(obj[7].toString()));
				cd.setPromiseRange(Integer.valueOf(obj[8].toString()));
			}
		}
		return cd;
	}
	
	@Override
	public CollectionDetailDto selectCollectionDetailForInputHandling2(
			int orderId, String contractNo, int instalmentNo) {
		List<Object[]> listObjek = collectionDetailDao.selectCollectionDetailForInputHandling2(orderId, contractNo, instalmentNo);
		List<CollectionDetailDto> listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
		CollectionDetailDto cd = new CollectionDetailDto();
		if (listObjek.size() != 0) {
			for (Object[] obj : listObjek) {
				cd.setContractNo(obj[0].toString());
				cd.setInstallmentNo(Integer.valueOf(obj[1].toString()));
				cd.setOrderId(Integer.valueOf(obj[2].toString()));
				cd.setCustomerCode(obj[3].toString());
				cd.setLpkNo(obj[4].toString());
				cd.setNotes(obj[5].toString());
				cd.setPromiseCount(Integer.valueOf(obj[6].toString()));
				cd.setPromiseAvailable(Integer.valueOf(obj[7].toString()));
				cd.setPromiseRange(Integer.valueOf(obj[8].toString()));
				String temp = "0";
				String[] x = obj[9].toString().split(".");
				for (int j = 0; j < x.length; j++) {
					if (j == 0) {
						temp = x[j];
					}
				}
				cd.setTotalAmt(Integer.valueOf(temp));
			}
		}
		return cd;
	}

	@Override
	public void updateStatusCollectionDetailToCMP(int orderId,
			String contractNo, int instalmentNo) {
		collectionDetailDao.updateStatusCollectionDetailToCMP(orderId, contractNo, instalmentNo);
		
	}

	@Override
	public void updateStatusCollectionDetailToCLS(int orderId,
			String contractNo, int instalmentNo, String notes) {
		collectionDetailDao.updateStatusCollectionDetailToCLS(orderId, contractNo, instalmentNo, notes);
		
	}

	@Override
	public int cekStatusCollectionDetailByOrderId(int orderId) {
		int tot = 0;
		tot = collectionDetailDao.cekStatusCollectionDetailByOrderId(orderId);
		return tot;
	}

	@Override
	public List<ManualAssignDto> selectResultPenugasanUlang(String keySearch) {
		List<Object[]> listObjek = collectionDetailDao.selectResultPenugasanUlang(keySearch);
		List<CollectionDetailDto> listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
		List<ManualAssignDto> listMa = new ArrayList<ManualAssignDto>();
		if (listObjek.size() != 0) {
			for (Object[] obj : listObjek) {
				CollectionDetailDto cd = new CollectionDetailDto();
				cd.setNamaRegional(obj[0].toString());
				cd.setNamaKaryawan(obj[1].toString());
				cd.setBucketCollector(Integer.valueOf(obj[2].toString()));
				cd.setBucketPayment(Integer.valueOf(obj[3].toString()));
				cd.setBucketHandling(Integer.valueOf(obj[4].toString()));
				cd.setBucketNotHandling(cd.getBucketCollector() - (cd.getBucketPayment()+cd.getBucketHandling()));
				cd.setPreviosCollector(obj[5].toString());
				cd.setOrderId(Integer.valueOf(obj[6].toString()));
				listCollectionDetailDto.add(cd);
			}
		}
		if (listCollectionDetailDto.size() != 0) {
			for (int i = 0; i < listCollectionDetailDto.size(); i++) {
				ManualAssignDto ma = new ManualAssignDto();
				ma.setNamaRegional(listCollectionDetailDto.get(i).getNamaRegional());
				ma.setNamaKaryawan(listCollectionDetailDto.get(i).getNamaKaryawan());
				ma.setBucketCollector(listCollectionDetailDto.get(i).getBucketCollector());
				ma.setBucketPayment(listCollectionDetailDto.get(i).getBucketPayment());
				ma.setBucketHandling(listCollectionDetailDto.get(i).getBucketHandling());
				ma.setBucketNotHandling(listCollectionDetailDto.get(i).getBucketNotHandling());
				ma.setPreviosCollector(listCollectionDetailDto.get(i).getPreviosCollector());
				ma.setOrderId(listCollectionDetailDto.get(i).getOrderId());
				listMa.add(ma);
			}
		}
		return listMa;
	}

	@Override
	public List<ManualAssignDto> selectResultPenugasanUlangKodeRegKodeCab(
			String kodeRegion, String kodeCabang, String keySearch) {
		List<Object[]> listObjek = collectionDetailDao.selectResultPenugasanUlangKodeRegKodeCab(kodeRegion, kodeCabang, keySearch);
		List<CollectionDetailDto> listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
		List<ManualAssignDto> listMa = new ArrayList<ManualAssignDto>();
		if (listObjek.size() != 0) {
			for (Object[] obj : listObjek) {
				CollectionDetailDto cd = new CollectionDetailDto();
				cd.setNamaRegional(obj[0].toString());
				cd.setNamaKaryawan(obj[1].toString());
				cd.setBucketCollector(Integer.valueOf(obj[2].toString()));
				cd.setPreviosCollector(obj[3].toString());
				cd.setOrderId(Integer.valueOf(obj[4].toString()));
				cd.setBucketPayment(collectionDetailDao.bucketPayment(cd.getOrderId()));
				cd.setBucketHandling(collectionDetailDao.bucketHandling(cd.getOrderId()));
				cd.setBucketNotHandling(cd.getBucketCollector() - (cd.getBucketPayment()+cd.getBucketHandling()));
				listCollectionDetailDto.add(cd);
			}
		}
		if (listCollectionDetailDto.size() != 0) {
			for (int i = 0; i < listCollectionDetailDto.size(); i++) {
				ManualAssignDto ma = new ManualAssignDto();
				ma.setNamaRegional(listCollectionDetailDto.get(i).getNamaRegional());
				ma.setNamaKaryawan(listCollectionDetailDto.get(i).getNamaKaryawan());
				ma.setBucketCollector(listCollectionDetailDto.get(i).getBucketCollector());
				ma.setBucketPayment(listCollectionDetailDto.get(i).getBucketPayment());
				ma.setBucketHandling(listCollectionDetailDto.get(i).getBucketHandling());
				ma.setBucketNotHandling(listCollectionDetailDto.get(i).getBucketNotHandling());
				ma.setPreviosCollector(listCollectionDetailDto.get(i).getPreviosCollector());
				ma.setOrderId(listCollectionDetailDto.get(i).getOrderId());
				listMa.add(ma);
			}
		}
		return listMa;
	}

	@Override
	public List<ManualAssignDto> selectResultPenugasanUlangKodeCab(
			String kodeCabang, String keySearch) {
		List<Object[]> listObjek = collectionDetailDao.selectResultPenugasanUlangKodeCab(kodeCabang, keySearch);
		List<CollectionDetailDto> listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
		List<ManualAssignDto> listMa = new ArrayList<ManualAssignDto>();
		if (listObjek.size() != 0) {
			for (Object[] obj : listObjek) {
				CollectionDetailDto cd = new CollectionDetailDto();
				cd.setNamaRegional(obj[0].toString());
				cd.setNamaKaryawan(obj[1].toString());
				cd.setBucketCollector(Integer.valueOf(obj[2].toString()));
				cd.setPreviosCollector(obj[3].toString());
				cd.setOrderId(Integer.valueOf(obj[4].toString()));
				cd.setBucketPayment(collectionDetailDao.bucketPayment(cd.getOrderId()));
				cd.setBucketHandling(collectionDetailDao.bucketHandling(cd.getOrderId()));
				cd.setBucketNotHandling(cd.getBucketCollector() - (cd.getBucketPayment()+cd.getBucketHandling()));
				listCollectionDetailDto.add(cd);
			}
		}
		if (listCollectionDetailDto.size() != 0) {
			for (int i = 0; i < listCollectionDetailDto.size(); i++) {
				ManualAssignDto ma = new ManualAssignDto();
				ma.setNamaRegional(listCollectionDetailDto.get(i).getNamaRegional());
				ma.setNamaKaryawan(listCollectionDetailDto.get(i).getNamaKaryawan());
				ma.setBucketCollector(listCollectionDetailDto.get(i).getBucketCollector());
				ma.setBucketPayment(listCollectionDetailDto.get(i).getBucketPayment());
				ma.setBucketHandling(listCollectionDetailDto.get(i).getBucketHandling());
				ma.setBucketNotHandling(listCollectionDetailDto.get(i).getBucketNotHandling());
				ma.setPreviosCollector(listCollectionDetailDto.get(i).getPreviosCollector());
				ma.setOrderId(listCollectionDetailDto.get(i).getOrderId());
				listMa.add(ma);
			}
		}
		return listMa;
	}

	@Override
	public List<ManualAssignDto> selectResultPenugasanUlangKodeCabKodeKol(
			String kodeCab, String kodeKol, String keySearch) {
		List<Object[]> listObjek = collectionDetailDao.selectResultPenugasanUlangKodeCabKodeKol(kodeCab, kodeKol, keySearch);
		List<CollectionDetailDto> listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
		List<ManualAssignDto> listMa = new ArrayList<ManualAssignDto>();
		if (listObjek.size() != 0) {
			for (Object[] obj : listObjek) {
				CollectionDetailDto cd = new CollectionDetailDto();
				cd.setNamaRegional(obj[0].toString());
				cd.setNamaKaryawan(obj[1].toString());
				cd.setBucketCollector(Integer.valueOf(obj[2].toString()));
				cd.setPreviosCollector(obj[3].toString());
				cd.setOrderId(Integer.valueOf(obj[4].toString()));
				cd.setBucketPayment(collectionDetailDao.bucketPayment(cd.getOrderId()));
				cd.setBucketHandling(collectionDetailDao.bucketHandling(cd.getOrderId()));
				cd.setBucketNotHandling(cd.getBucketCollector() - (cd.getBucketPayment()+cd.getBucketHandling()));
				listCollectionDetailDto.add(cd);
			}
		}
		if (listCollectionDetailDto.size() != 0) {
			for (int i = 0; i < listCollectionDetailDto.size(); i++) {
				ManualAssignDto ma = new ManualAssignDto();
				ma.setNamaRegional(listCollectionDetailDto.get(i).getNamaRegional());
				ma.setNamaKaryawan(listCollectionDetailDto.get(i).getNamaKaryawan());
				ma.setBucketCollector(listCollectionDetailDto.get(i).getBucketCollector());
				ma.setBucketPayment(listCollectionDetailDto.get(i).getBucketPayment());
				ma.setBucketHandling(listCollectionDetailDto.get(i).getBucketHandling());
				ma.setBucketNotHandling(listCollectionDetailDto.get(i).getBucketNotHandling());
				ma.setPreviosCollector(listCollectionDetailDto.get(i).getPreviosCollector());
				ma.setOrderId(listCollectionDetailDto.get(i).getOrderId());
				listMa.add(ma);
			}
		}
		return listMa;
	}

	@Override
	public List<ManualAssignDto> selectResultPenugasanUlangKodeRegKodeKol(
			String kodeReg, String kodeKol, String keySearch) {
		List<Object[]> listObjek = collectionDetailDao.selectResultPenugasanUlangKodeRegKodeKol(kodeReg, kodeKol, keySearch);
		List<CollectionDetailDto> listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
		List<ManualAssignDto> listMa = new ArrayList<ManualAssignDto>();
		if (listObjek.size() != 0) {
			for (Object[] obj : listObjek) {
				CollectionDetailDto cd = new CollectionDetailDto();
				cd.setNamaRegional(obj[0].toString());
				cd.setNamaKaryawan(obj[1].toString());
				cd.setBucketCollector(Integer.valueOf(obj[2].toString()));
				cd.setPreviosCollector(obj[3].toString());
				cd.setOrderId(Integer.valueOf(obj[4].toString()));
				cd.setBucketPayment(collectionDetailDao.bucketPayment(cd.getOrderId()));
				cd.setBucketHandling(collectionDetailDao.bucketHandling(cd.getOrderId()));
				cd.setBucketNotHandling(cd.getBucketCollector() - (cd.getBucketPayment()+cd.getBucketHandling()));
				listCollectionDetailDto.add(cd);
			}
		}
		if (listCollectionDetailDto.size() != 0) {
			for (int i = 0; i < listCollectionDetailDto.size(); i++) {
				ManualAssignDto ma = new ManualAssignDto();
				ma.setNamaRegional(listCollectionDetailDto.get(i).getNamaRegional());
				ma.setNamaKaryawan(listCollectionDetailDto.get(i).getNamaKaryawan());
				ma.setBucketCollector(listCollectionDetailDto.get(i).getBucketCollector());
				ma.setBucketPayment(listCollectionDetailDto.get(i).getBucketPayment());
				ma.setBucketHandling(listCollectionDetailDto.get(i).getBucketHandling());
				ma.setBucketNotHandling(listCollectionDetailDto.get(i).getBucketNotHandling());
				ma.setPreviosCollector(listCollectionDetailDto.get(i).getPreviosCollector());
				ma.setOrderId(listCollectionDetailDto.get(i).getOrderId());
				listMa.add(ma);
			}
		}
		return listMa;
	}
	
	@Override
	public List<ManualAssignDto> selectResultPenugasanUlangDetail(
			int orderId, String search) {
		List<Object[]> listObjek = collectionDetailDao.selectResultPenugasanUlangDetail(orderId, search);
		List<ManualAssignDto> listMa = new ArrayList<ManualAssignDto>();
		if (listObjek.size() != 0) {
			for (Object[] obj : listObjek) {
				ManualAssignDto ma = new ManualAssignDto();
				ma.setOrderId(Integer.valueOf(obj[0].toString()));
				ma.setContractNo(obj[1].toString());
				ma.setInstalmentNo(Integer.valueOf(obj[2].toString()));
				ma.setCustomerName(obj[3].toString());
				ma.setAddress(obj[4].toString());
				ma.setDueDate(obj[5].toString());
				ma.setKodeKolektor(obj[6].toString());
				listMa.add(ma);
			}
		}
		return listMa;
	}
	
	@Override
	public List<ManualAssignDto> selectCollectionHeaderReassignDate(
			int orderId) {
		List<Object[]> listObjek = collectionDetailDao.selectCollectionHeaderReassignDate(orderId);
		List<ManualAssignDto> listMa = new ArrayList<ManualAssignDto>();
		if (listObjek.size() != 0) {
			for (Object[] obj : listObjek) {
				ManualAssignDto ma = new ManualAssignDto();
				ma.setOrderId(Integer.valueOf(obj[0].toString()));
				ma.setReassignDate(obj[1].toString());
				
				listMa.add(ma);
			}
		}
		return listMa;
	}

	@Override
	public int bucketPayment(int orderId) {
		int a = 0;
		a = collectionDetailDao.bucketPayment(orderId);
		return a;
	}

	@Override
	public int bucketHandling(int orderId) {
		int a = 0;
		a = collectionDetailDao.bucketHandling(orderId);
		return a;
	}

	@Override
	public List<CollectionDetailDto> selectRekonKolektorKodeRegionalKodeCabang(
			Date dateAwal, Date dateAkhir, String kodeRegional,
			String kodeCabang, String keySearch) {
		List<Object[]> listObjek = collectionDetailDao.selectRekonKolektorKodeRegionalKodeCabang(dateAwal, dateAkhir, kodeRegional,kodeCabang, keySearch);
		List<CollectionDetailDto> listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
		if (listObjek.size() != 0) {
			for (Object[] obj : listObjek) {
				CollectionDetailDto cd = new CollectionDetailDto();
				cd.setCollectorCode(obj[0].toString());
				cd.setNamaKaryawan(obj[1].toString());
				cd.setNamaCabang(obj[2].toString());
				cd.setJumlahKunjungan(obj[3].toString());
				cd.setUserId(obj[4].toString());
				DateFormat df = new SimpleDateFormat("dd/MMM/yyyy");
				String temAwal = df.format(dateAwal);
				String temAkhir = df.format(dateAkhir);
				String intervalWakktu = temAwal+" s.d "+temAkhir;
				cd.setIntervalWaktu(intervalWakktu);
				listCollectionDetailDto.add(cd);
			}
		}
		return listCollectionDetailDto;
	}

	@Override
	public List<CollectionDetailDto> selectRekonKolektorKodeCabang(
			Date dateAwal, Date dateAkhir, String kodeCabang, String keySearch) {
		List<Object[]> listObjek = collectionDetailDao.selectRekonKolektorKodeCabang(dateAwal, dateAkhir, kodeCabang, keySearch);
		List<CollectionDetailDto> listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
		if (listObjek.size() != 0) {
			for (Object[] obj : listObjek) {
				CollectionDetailDto cd = new CollectionDetailDto();
				cd.setCollectorCode(obj[0].toString());
				cd.setNamaKaryawan(obj[1].toString());
				cd.setNamaCabang(obj[2].toString());
				cd.setJumlahKunjungan(obj[3].toString());
				cd.setUserId(obj[4].toString());
				DateFormat df = new SimpleDateFormat("dd/MMM/yyyy");
				String temAwal = df.format(dateAwal);
				String temAkhir = df.format(dateAkhir);
				String intervalWakktu = temAwal+" s.d "+temAkhir;
				cd.setIntervalWaktu(intervalWakktu);
				listCollectionDetailDto.add(cd);
			}
		}
		return listCollectionDetailDto;
	}

	@Override
	public List<CollectionDetailDto> selectRekonKolektorKodeCabangKodeKolektor(
			Date dateAwal, Date dateAkhir, String kodeCabang,
			String kodeKolektor, String keySearch) {
		List<Object[]> listObjek = collectionDetailDao.selectRekonKolektorKodeCabangKodeKolektor(dateAwal, dateAkhir, kodeCabang,kodeKolektor, keySearch);
		List<CollectionDetailDto> listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
		if (listObjek.size() != 0) {
			for (Object[] obj : listObjek) {
				CollectionDetailDto cd = new CollectionDetailDto();
				cd.setCollectorCode(obj[0].toString());
				cd.setNamaKaryawan(obj[1].toString());
				cd.setNamaCabang(obj[2].toString());
				cd.setJumlahKunjungan(obj[3].toString());
				cd.setUserId(obj[4].toString());
				DateFormat df = new SimpleDateFormat("dd/MMM/yyyy");
				String temAwal = df.format(dateAwal);
				String temAkhir = df.format(dateAkhir);
				String intervalWakktu = temAwal+" s.d "+temAkhir;
				cd.setIntervalWaktu(intervalWakktu);
				listCollectionDetailDto.add(cd);
			}
		}
		return listCollectionDetailDto;
	}

	@Override
	public List<CollectionDetailDto> selectRekonKolektorKodeRegionalKodeKolektor(
			Date dateAwal, Date dateAkhir, String kodeRegional,
			String kodeKolektor, String keySearch) {
		List<Object[]> listObjek = collectionDetailDao.selectRekonKolektorKodeRegionalKodeKolektor(dateAwal, dateAkhir, kodeRegional,kodeKolektor, keySearch);
		List<CollectionDetailDto> listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
		if (listObjek.size() != 0) {
			for (Object[] obj : listObjek) {
				CollectionDetailDto cd = new CollectionDetailDto();
				cd.setCollectorCode(obj[0].toString());
				cd.setNamaKaryawan(obj[1].toString());
				cd.setNamaCabang(obj[2].toString());
				cd.setJumlahKunjungan(obj[3].toString());
				cd.setUserId(obj[4].toString());
				DateFormat df = new SimpleDateFormat("dd/MMM/yyyy");
				String temAwal = df.format(dateAwal);
				String temAkhir = df.format(dateAkhir);
				String intervalWakktu = temAwal+" s.d "+temAkhir;
				cd.setIntervalWaktu(intervalWakktu);
				listCollectionDetailDto.add(cd);
			}
		}
		return listCollectionDetailDto;
	}

	@Override
	public List<ManualAssignDto> selectResultPenugasanUlangKodeRegional(
			String kodeRegional, String keySearch) {
		List<Object[]> listObjek = collectionDetailDao.selectResultPenugasanUlangKodeRegional(kodeRegional, keySearch);
		List<CollectionDetailDto> listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
		List<ManualAssignDto> listMa = new ArrayList<ManualAssignDto>();
		if (listObjek.size() != 0) {
			for (Object[] obj : listObjek) {
				CollectionDetailDto cd = new CollectionDetailDto();
				cd.setNamaRegional(obj[0].toString());
				cd.setNamaKaryawan(obj[1].toString());
				cd.setBucketCollector(Integer.valueOf(obj[2].toString()));
				cd.setPreviosCollector(obj[3].toString());
				cd.setOrderId(Integer.valueOf(obj[4].toString()));
				cd.setBucketPayment(collectionDetailDao.bucketPayment(cd.getOrderId()));
				cd.setBucketHandling(collectionDetailDao.bucketHandling(cd.getOrderId()));
				cd.setBucketNotHandling(cd.getBucketCollector() - (cd.getBucketPayment()+cd.getBucketHandling()));
				listCollectionDetailDto.add(cd);
			}
		}
		if (listCollectionDetailDto.size() != 0) {
			for (int i = 0; i < listCollectionDetailDto.size(); i++) {
				ManualAssignDto ma = new ManualAssignDto();
				ma.setNamaRegional(listCollectionDetailDto.get(i).getNamaRegional());
				ma.setNamaKaryawan(listCollectionDetailDto.get(i).getNamaKaryawan());
				ma.setBucketCollector(listCollectionDetailDto.get(i).getBucketCollector());
				ma.setBucketPayment(listCollectionDetailDto.get(i).getBucketPayment());
				ma.setBucketHandling(listCollectionDetailDto.get(i).getBucketHandling());
				ma.setBucketNotHandling(listCollectionDetailDto.get(i).getBucketNotHandling());
				ma.setPreviosCollector(listCollectionDetailDto.get(i).getPreviosCollector());
				ma.setOrderId(listCollectionDetailDto.get(i).getOrderId());
				listMa.add(ma);
			}
		}
		return listMa;
	}

	@Override
	public int jumOrderNotCmp(int orderId) {
		int max = 0;
		max = collectionDetailDao.jumOrderNotCmp(orderId);
		return max;
	}

	@Override
	public ReassignCollector selectReassignCollectorByCollectorCode(
			String kodeKolektor) {
		ReassignCollector rc = new ReassignCollector();
		List<Object[]> listObjek = collectionDetailDao.selectReassignCollectorByCollectorCode(kodeKolektor);
		if (listObjek.size() != 0) {
			for (Object[] obj : listObjek) {
				CollectionDetailDto cd = new CollectionDetailDto();
				rc.setCollectorCode(obj[0].toString());
				rc.setReassignCollector(obj[1].toString());
				rc.setStrPeriodFrom(obj[2].toString());
				rc.setStrPeriodTo(obj[3].toString());
			}
		}
		return rc;
	}

	
	
	

}
