package service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.ParamMstDao;
import dto.CabangMstDto;
import dto.ParamMstDto;
import entity.CabangMst;
import entity.ParamMst;
import service.ParamMstSvc;

@Service("paramMstSvc")
@Transactional
public class ParamMstSvvcImpl implements ParamMstSvc{
	
	@Autowired
	private ParamMstDao paramMstDao;

	@Override
	public List<ParamMstDto> findParamWithSearch(String keySearch) {
		List<Object[]> listObj = paramMstDao.findParamWithSearch(keySearch);
		List<ParamMstDto> listParamMstDto = new ArrayList<ParamMstDto>();
		if (listObj.size() != 0) {
			for (Object[] obj : listObj) {
				ParamMstDto paramMstDto = new ParamMstDto();
				paramMstDto.setParamId(obj[0].toString());
				paramMstDto.setCondition(obj[1].toString());
				paramMstDto.setLevelParam(obj[2].toString());
				paramMstDto.setParentId(obj[3].toString());
				listParamMstDto.add(paramMstDto);
			}
		}
		return listParamMstDto;
	}

	@Override
	public void deleteParam(String paramId) {
		paramMstDao.deleteParam(paramId);
		
	}

	@Override
	public void updateParam(String condition, String levelParam, String parentId,
			String paramId) {
		paramMstDao.updateParam(condition, levelParam, parentId, paramId);
		
	}

	@Override
	public void saveParam(String paramId, String condition, String levelParam,
			String parentId) {
		paramMstDao.saveParam(paramId, condition, levelParam, parentId);
		
	}

	@Override
	public ParamMstDto findOneParam(String paramId) {
		List<Object[]> listObj = paramMstDao.findOneParam(paramId);
		ParamMstDto paramMstDto = new ParamMstDto();
		if (listObj.size() != 0) {
			for (Object[] obj : listObj) {
				paramMstDto.setParamId(obj[0].toString());
				paramMstDto.setCondition(obj[1].toString());
				paramMstDto.setLevelParam(obj[2].toString());
				paramMstDto.setParentIdStr(obj[3].toString());
			}
		}
		return paramMstDto;
	}

}
