package service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.zkoss.zk.ui.Sessions;

import dao.HandlingDao;
import dao.MenuMstDao;
import dao.PaymentDao;
import dto.MenuMstDto;
import dto.UserProfileDto;
import entity.MenuMst;
import entity.MenuMstPK;
import service.HandlingSvc;
import service.MenuMstSvc;
import service.PaymentSvc;

@Service("handlingSvc")
@Transactional
public class HandlingSvcImpl implements HandlingSvc {

	@Autowired
	private HandlingDao handlingDao;

	@Override
	public void insertHandling(int BATCH_HANDLING, String CONTRACT_NO,
			int INSTALLMENT_NO, int ORDER_ID, String KODE_HANDLING,
			String KODE_KONSUMEN, String NO_LPK, String NOTES,
			String TANGGAL_HANDLING, String TANGGAL_JANJI, int PROMISE_COUNT,
			int PROMISE_AVAILABLE, int PROMISE_RANGE, String STATUS,
			String CREATE_USER) {
		handlingDao.insertHandling(BATCH_HANDLING, CONTRACT_NO, INSTALLMENT_NO, ORDER_ID, KODE_HANDLING, KODE_KONSUMEN, NO_LPK, NOTES, TANGGAL_HANDLING, TANGGAL_JANJI, PROMISE_COUNT, PROMISE_AVAILABLE, PROMISE_RANGE, STATUS, CREATE_USER);
		
	}
	
	
	
	@Override
	public void insertHandDepo(int BATCH_HANDLING, String CONTRACT_NO,
			int INSTALLMENT_NO, int ORDER_ID, String KODE_HANDLING,
			String KODE_KONSUMEN, String NO_LPK, String NOTES,
			String TANGGAL_HANDLING, String TANGGAL_JANJI, int PROMISE_COUNT,
			int PROMISE_AVAILABLE, int PROMISE_RANGE, String STATUS,
			String CREATE_USER, int deposit, String depositFrom) {
		handlingDao.insertHandlingDeposit(BATCH_HANDLING, CONTRACT_NO, INSTALLMENT_NO, ORDER_ID,KODE_HANDLING, KODE_KONSUMEN, NO_LPK, NOTES, TANGGAL_HANDLING, TANGGAL_JANJI, PROMISE_COUNT, PROMISE_AVAILABLE, PROMISE_RANGE, STATUS, CREATE_USER, deposit, depositFrom);
		
		
	}



	@Override
	public void insertHandDepoNoTglJanji(int BATCH_HANDLING,
			String CONTRACT_NO, int INSTALLMENT_NO, int ORDER_ID,
			String KODE_HANDLING, String KODE_KONSUMEN, String NO_LPK,
			String NOTES, String TANGGAL_HANDLING, 
			int PROMISE_COUNT, int PROMISE_AVAILABLE, int PROMISE_RANGE,
			String STATUS, String CREATE_USER, int deposit, String depositFrom) {
		handlingDao.insertHandDepoNoTglJanji(BATCH_HANDLING, CONTRACT_NO, INSTALLMENT_NO, ORDER_ID,KODE_HANDLING,KODE_KONSUMEN, NO_LPK, NOTES, TANGGAL_HANDLING, PROMISE_COUNT, PROMISE_AVAILABLE, PROMISE_RANGE, STATUS, CREATE_USER, deposit, depositFrom);
		
	}

	

	

	

		
}
