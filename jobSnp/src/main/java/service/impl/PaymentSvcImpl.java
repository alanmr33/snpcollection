package service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.zkoss.zk.ui.Sessions;

import dao.MenuMstDao;
import dao.PaymentDao;
import dto.MenuMstDto;
import dto.UserProfileDto;
import entity.MenuMst;
import entity.MenuMstPK;
import service.MenuMstSvc;
import service.PaymentSvc;

@Service("paymentSvc")
@Transactional
public class PaymentSvcImpl implements PaymentSvc {

	@Autowired
	private PaymentDao paymentDao;

	@Override
	public void insertPayment(int INSTALLMENT_NO, int ORDER_ID,
			String CUSTOMER_CODE, String CONTRACT_NO, String CODE_COLLECTOR,
			int TOTAL_BAYAR, String NO_KWITANSI, String NO_LPK,
			String TANGGAL_TRANSAKSI, String TANGGAL_BAYAR, String STATUS,
			String CREATE_USER) {
		paymentDao.insertPayment(INSTALLMENT_NO, ORDER_ID, CUSTOMER_CODE, CONTRACT_NO, CODE_COLLECTOR, TOTAL_BAYAR, NO_KWITANSI, NO_LPK, TANGGAL_TRANSAKSI, TANGGAL_BAYAR, STATUS, CREATE_USER);
		
	}

	

		
}
