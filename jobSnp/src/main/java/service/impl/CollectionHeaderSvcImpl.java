package service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.CollectionHeaderDao;
import dto.CollectionHeaderDto;
import service.CollectionHeaderSvc;

@Service("collectionHeaderSvc")
@Transactional
public class CollectionHeaderSvcImpl implements CollectionHeaderSvc{
	@Autowired
	private CollectionHeaderDao collectionHeaderDao;
	
	
	@Override
	public void insertCollectionHeaderInUploadManual(
			String orderId, String lpkNo, String lpkDate, String collectorCode,
			String status) {
		collectionHeaderDao.insertCollectionHeaderInUploadManual(orderId, lpkNo, lpkDate, collectorCode, "ASN");
	}


	@Override
	public void updateCollectionHeaderSatusToCmp(int orderId) {
		collectionHeaderDao.updateCollectionHeaderStatusToCmp(orderId);
		
	}


	@Override
	public void execSP_CHECK_TASK(int orderId) {
		collectionHeaderDao.execSP_CHECK_TASK(orderId);
	}
}
