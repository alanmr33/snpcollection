package service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import service.MobileTrackingSvc;
import dao.MobileTrackingDao;
import dto.MenuMstDto;
import dto.MobileTrackingDto;



@Service("mobileTrackingSvc")
@Transactional
public class MobileTrackingSvcImpl implements MobileTrackingSvc {
	
	@Autowired
	private MobileTrackingDao mobileTrackingDao;

	@Override
	public List<MobileTrackingDto> findMobileTrackingAllByCabang(String kodeCabang,
			Date dateAwal, Date dateAkhir, String keySearch) {
		List<Object[]> listMobileTracking = mobileTrackingDao.findMobileTrackingAllByCabang(kodeCabang, dateAwal, dateAkhir, keySearch);
		List<MobileTrackingDto> listMobileTrackingDto = new ArrayList<MobileTrackingDto>();
		if (listMobileTracking.size() != 0) {
			for (Object[] obj : listMobileTracking) {
				MobileTrackingDto mt = new MobileTrackingDto();
				mt.setUserId(obj[0].toString());
				mt.setNamaKaryawan(obj[1].toString());
				mt.setNamaRegion(obj[2].toString());
				mt.setNamaCabang(obj[3].toString());
				listMobileTrackingDto.add(mt);
			}
		}
		return listMobileTrackingDto;
	}

	@Override
	public List<MobileTrackingDto> findMobileTrackingByUserIdNotOrderId(String userId,
			Date dateAwal, Date dateAkhir) {
		List<Object[]> listMobileTracking = mobileTrackingDao.findMobileTrackingByUserIdNotOrderId(userId, dateAwal, dateAkhir);
		List<MobileTrackingDto> listMobileTrackingDto = new ArrayList<MobileTrackingDto>();
		if (listMobileTracking.size() != 0) {
			for (Object[] obj : listMobileTracking) {
				MobileTrackingDto mt = new MobileTrackingDto();
				mt.setUserId(obj[0].toString());
				mt.setNamaKaryawan(obj[1].toString());
				mt.setNamaRegion(obj[2].toString());
				mt.setNamaCabang(obj[3].toString());
				mt.setKetTrackingDate(obj[4].toString());
				mt.setLocation(obj[5].toString());
				mt.setOrderId(obj[6].toString());
				String latlong = mt.getLocation();
				String lat ="";
				String longi ="";
				float lat2=0;
				float longi2=0;
				String[] x = latlong.split(",");
				for (int j = 0; j < x.length; j++) {
					if (j == 0) {
						lat = x[j];
						lat2 = Float.parseFloat(lat);
					}
					if (j == 1) {
						longi = x[j];
						longi2 = Float.parseFloat(longi);
					}
				}
				mt.setLatitude(lat2);
				mt.setLongitude(longi2);
				String tempTooltip = mt.getNamaKaryawan()+" "+mt.getKetTrackingDate();
				mt.setKetTooltip(tempTooltip);
				listMobileTrackingDto.add(mt);
			}
		}
		return listMobileTrackingDto;
	}

	@Override
	public List<MobileTrackingDto> findMobileTrackingByUserIdWithOrderId(
			String userId, Date dateAwal, Date dateAkhir) {
		List<Object[]> listMobileTracking = mobileTrackingDao.findMobileTrackingByUserIdWithOrderId(userId, dateAwal, dateAkhir);
		List<MobileTrackingDto> listMobileTrackingDto = new ArrayList<MobileTrackingDto>();
		char huruf = 'A';
		if (listMobileTracking.size() != 0) {
			for (Object[] obj : listMobileTracking) {
				MobileTrackingDto mt = new MobileTrackingDto();
				mt.setUserId(obj[0].toString());
				mt.setNamaKaryawan(obj[1].toString());
				mt.setNamaRegion(obj[2].toString());
				mt.setNamaCabang(obj[3].toString());
				mt.setKetTrackingDate(obj[4].toString());
				mt.setLocation(obj[5].toString());
				mt.setOrderId(obj[6].toString());
				String latlong = mt.getLocation();
				String lat ="";
				String longi ="";
				float lat2=0;
				float longi2=0;
				String[] x = latlong.split(",");
				for (int j = 0; j < x.length; j++) {
					if (j == 0) {
						lat = x[j];
						lat2 = Float.parseFloat(lat);
					}
					if (j == 1) {
						longi = x[j];
						longi2 = Float.parseFloat(longi);
					}
				}
				mt.setHuruf(huruf);
				mt.setLatitude(lat2);
				mt.setLongitude(longi2);
				String tempTooltip = mt.getNamaKaryawan()+" "+mt.getKetTrackingDate();
				mt.setKetTooltip(tempTooltip);
				listMobileTrackingDto.add(mt);
			}
		}
		return listMobileTrackingDto;
	}

	@Override
	public List<MobileTrackingDto> findMobileTrackingByKodeCabangUserId(String kodeCabang,String userId,
			Date dateAwal, Date dateAkhir, String keySearch) {
		List<Object[]> listMobileTracking = mobileTrackingDao.findMobileTrackingByKodeCabangUserId(kodeCabang,userId, dateAwal, dateAkhir, keySearch);
		List<MobileTrackingDto> listMobileTrackingDto = new ArrayList<MobileTrackingDto>();
		if (listMobileTracking.size() != 0) {
			for (Object[] obj : listMobileTracking) {
				MobileTrackingDto mt = new MobileTrackingDto();
				mt.setUserId(obj[0].toString());
				mt.setNamaKaryawan(obj[1].toString());
				mt.setNamaRegion(obj[2].toString());
				mt.setNamaCabang(obj[3].toString());
				listMobileTrackingDto.add(mt);
			}
		}
		return listMobileTrackingDto;
	}

	@Override
	public List<MobileTrackingDto> findMobileTrackingByKodeRegionUserId(
			String kodeRegion, String userId, Date dateAwal, Date dateAkhir,
			String keySearch) {
		List<Object[]> listMobileTracking = mobileTrackingDao.findMobileTrackingByKodeRegionUserId(kodeRegion,userId, dateAwal, dateAkhir, keySearch);
		List<MobileTrackingDto> listMobileTrackingDto = new ArrayList<MobileTrackingDto>();
		if (listMobileTracking.size() != 0) {
			for (Object[] obj : listMobileTracking) {
				MobileTrackingDto mt = new MobileTrackingDto();
				mt.setUserId(obj[0].toString());
				mt.setNamaKaryawan(obj[1].toString());
				mt.setNamaRegion(obj[2].toString());
				mt.setNamaCabang(obj[3].toString());
				listMobileTrackingDto.add(mt);
			}
		}
		return listMobileTrackingDto;
	}

	@Override
	public List<MobileTrackingDto> findMobileTrackingByKodeRegionKodeCabang(
			String kodeRegion, String kodeCabang, Date dateAwal,
			Date dateAkhir, String keySearch) {
		List<Object[]> listMobileTracking = mobileTrackingDao.findMobileTrackingByKodeRegionKodeCang(kodeRegion,kodeCabang, dateAwal, dateAkhir, keySearch);
		List<MobileTrackingDto> listMobileTrackingDto = new ArrayList<MobileTrackingDto>();
		if (listMobileTracking.size() != 0) {
			for (Object[] obj : listMobileTracking) {
				MobileTrackingDto mt = new MobileTrackingDto();
				mt.setUserId(obj[0].toString());
				mt.setNamaKaryawan(obj[1].toString());
				mt.setNamaRegion(obj[2].toString());
				mt.setNamaCabang(obj[3].toString());
				listMobileTrackingDto.add(mt);
			}
		}
		return listMobileTrackingDto;
	}
}
