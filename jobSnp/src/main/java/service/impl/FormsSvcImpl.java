package service.impl;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Hex;
import org.apache.poi.hssf.record.DSFRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import service.AuthorityMstSvc;
import service.CabangMstSvc;
import service.FormsSvc;
import service.MenuMstSvc;
import service.LoginSvc;
import service.UserProfileSvc;
import dao.AuthorityMstDao;
import dao.CabangMstDao;
import dao.FormsDao;
import dao.MenuMstDao;
import dao.UserProfileDao;
import dto.AuthorityMstDto;
import dto.CabangMstDto;
import dto.FormsDto;
import dto.MenuMstDto;
import dto.UserProfileDto;
import dto.MenuUrlDto;
import entity.CabangMst;
import entity.UserMenuAccordion;
import entity.UserProfile;

@Service("formsSvc")
@Transactional
public class FormsSvcImpl implements FormsSvc {
	
	@Autowired
	private FormsDao formsDao;

	@Override
	public List<FormsDto> findFormsWithSearch(String keySearch) {
		List<FormsDto> listFormsDto = new ArrayList<FormsDto>();
		List<Object[]> listObject = new ArrayList<Object[]>();
		listObject = formsDao.findFormsWithSearch(keySearch);
		if (listObject.size() != 0) {
			for (Object[] obj : listObject) {
				FormsDto f = new FormsDto();
				f.setFormId(obj[0].toString());
				f.setFormName(obj[1].toString());
				f.setFormLabel(obj[2].toString());
				f.setFormOrder(obj[3].toString());
				listFormsDto.add(f);
			}
		}
		return listFormsDto;
	}

	@Override
	public void deleteForms(String formId) {
		formsDao.deleteForms(formId);
		
	}

	@Override
	public void updateForms(String formName, String formLabel,
			String formOrder, String formId) {
		formsDao.updateForms(formName, formLabel, formOrder, formId);
		
	}

	@Override
	public void saveForms(String formId, String formName, String formLabel,
			String formOrder) {
		formsDao.saveForms(formId, formName, formLabel, formOrder);
		
	}

	@Override
	public FormsDto findFormsById(String keySearch) {
		FormsDto f = new FormsDto();
		List<Object[]> listObject = new ArrayList<Object[]>();
		listObject = formsDao.findFormsWithSearch(keySearch);
		if (listObject.size() != 0) {
			for (Object[] obj : listObject) {
				f.setFormId(obj[0].toString());
				f.setFormName(obj[1].toString());
				f.setFormLabel(obj[2].toString());
				f.setFormOrder(obj[3].toString());
			}
		}
		return f;
	}

	@Override
	public void upDownOrder(int formOrder, String formId) {
		formsDao.upDownOrder(formOrder, formId);
	}
	
	
}
