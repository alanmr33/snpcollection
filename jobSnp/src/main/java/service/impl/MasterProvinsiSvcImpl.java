package service.impl;

import java.util.ArrayList;
import java.util.List;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Hex;
import org.apache.poi.hssf.record.DSFRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dao.MasterProvinsiDao;
import dto.CabangMstDto;
import dto.MasterProvinsiDto;
import dto.MasterRegionalDto;
import entity.CabangMst;
import entity.MasterProvinsi;
import service.MasterProvinsiSvc;

@Service("masterProvinsiSvc")
@Transactional
public class MasterProvinsiSvcImpl implements MasterProvinsiSvc {

	@Autowired
	private MasterProvinsiDao masterProvinsiDao;
	
	@Override
	public List<MasterProvinsiDto> findAllProvinsi() {
		List<Object[]> listProvinsi = masterProvinsiDao.findAllProvinsi();
		List<MasterProvinsiDto> listProvinsiDto = new ArrayList<>();
		for (Object[] obj : listProvinsi) {
			MasterProvinsiDto provinsiDto = new MasterProvinsiDto();
			provinsiDto.setKodeProvinsi(obj[0].toString());
			provinsiDto.setNamaProvinsi(obj[1].toString());
			if(Integer.parseInt(obj[2].toString())==0){
				provinsiDto.setAktifStatus("Disabled");
			}else{
				provinsiDto.setAktifStatus("Enabled");	
			}
			provinsiDto.setCreateUser(obj[3].toString());
			provinsiDto.setCreateDate(obj[4].toString());
			provinsiDto.setModifyUser(obj[5].toString());
			provinsiDto.setModifyDate(obj[6].toString());
			listProvinsiDto.add(provinsiDto);
		}
		return listProvinsiDto;
	}

	@Override
	public List<MasterProvinsiDto> searchAllProvinsi(String search) {
		List<Object[]> listProvinsi = masterProvinsiDao.searchAllProvinsi(search);
		List<MasterProvinsiDto> listProvinsiDto = new ArrayList<>();
		for (Object[] obj : listProvinsi) {
			MasterProvinsiDto provinsiDto = new MasterProvinsiDto();
			provinsiDto.setKodeProvinsi(obj[0].toString());
			provinsiDto.setNamaProvinsi(obj[1].toString());
			if(Integer.parseInt(obj[2].toString())==0){
				provinsiDto.setAktifStatus("Disabled");
			}else{
				provinsiDto.setAktifStatus("Enabled");	
			}
			provinsiDto.setCreateUser(obj[3].toString());
			provinsiDto.setCreateDate(obj[4].toString());
			provinsiDto.setModifyUser(obj[5].toString());
			provinsiDto.setModifyDate(obj[6].toString());
			listProvinsiDto.add(provinsiDto);
		}
		return listProvinsiDto;
	}

	@Override
	public void InsertProvinsi(String provinsiCode, String provinsiName,
			int aktifStatus, String createUser) {
		masterProvinsiDao.InsertProvinsi(provinsiCode, provinsiName, aktifStatus, createUser);
		
	}

	@Override
	public void UpdateProvinsi(String provinsiName,
			String modifyUser, int aktifStatus, String provinsiCode) {
		masterProvinsiDao.UpdateProvinsi(provinsiName, modifyUser, aktifStatus, provinsiCode);
	}

	@Override
	public void DeleteProvinsi(String modifyUser, String provinsiCode) {
		masterProvinsiDao.DeleteProvinsi(modifyUser, provinsiCode);
	}

	@Override
	public MasterProvinsiDto findOneProvinsi(String provinsiCode) {
		MasterProvinsi provinsiDto = masterProvinsiDao.findOneProvinsi(provinsiCode);
		MasterProvinsiDto prov = new MasterProvinsiDto();
		if(provinsiDto!=null){
			prov.setKodeProvinsi(provinsiDto.getKodeProvinsi());
		}
		return prov;
	}

}
