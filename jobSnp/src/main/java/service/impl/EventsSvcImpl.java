package service.impl;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Hex;
import org.apache.poi.hssf.record.DSFRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import service.AuthorityMstSvc;
import service.CabangMstSvc;
import service.EventsSvc;
import service.FormsSvc;
import service.MenuMstSvc;
import service.LoginSvc;
import service.UserProfileSvc;
import dao.AuthorityMstDao;
import dao.CabangMstDao;
import dao.EventsDao;
import dao.FormsDao;
import dao.MenuMstDao;
import dao.UserProfileDao;
import dto.AuthorityMstDto;
import dto.CabangMstDto;
import dto.EventsDto;
import dto.FormsDto;
import dto.MenuMstDto;
import dto.UserProfileDto;
import dto.MenuUrlDto;
import entity.CabangMst;
import entity.UserMenuAccordion;
import entity.UserProfile;

@Service("eventsSvc")
@Transactional
public class EventsSvcImpl implements EventsSvc {
	
	@Autowired
	private EventsDao eventsDao;

	@Override
	public List<EventsDto> findEventsWithSearch(String keySearch) {
		List<EventsDto> listEventsDto = new ArrayList<EventsDto>();
		List<Object[]> listObject = new ArrayList<Object[]>();
		listObject = eventsDao.findEventsWithSearch(keySearch);
		if (listObject.size() != 0) {
			for (Object[] obj : listObject) {
				EventsDto e = new EventsDto();
				e.setEventId(obj[0].toString());
				e.setEventType(obj[1].toString());
				e.setEventComponent(obj[2].toString());
				e.setEventComponentTarget(obj[3].toString());
				e.setEventData(obj[4].toString());
				e.setFormId(obj[5].toString());
				listEventsDto.add(e);
			}
		}
		return listEventsDto;
	}

	@Override
	public EventsDto findEventsById(String keySearch) {
		EventsDto e = new EventsDto();
		List<Object[]> listObject = new ArrayList<Object[]>();
		listObject = eventsDao.findEventsWithSearch(keySearch);
		if (listObject.size() != 0) {
			for (Object[] obj : listObject) {
				e.setEventId(obj[0].toString());
				e.setEventType(obj[1].toString());
				e.setEventComponent(obj[2].toString());
				e.setEventComponentTarget(obj[3].toString());
				e.setEventData(obj[4].toString());
				e.setFormId(obj[5].toString());
			}
		}
		return e;
	}

	@Override
	public void deleteEvents(String eventsId) {
		eventsDao.deleteEvents(eventsId);
		
	}

	@Override
	public void updateEvents(String eventType, String eventComponent,
			String eventComponenTarget, String eventData, String formId,
			String eventId) {
		eventsDao.updateEvents(eventType, eventComponent, eventComponenTarget, eventData, formId, eventId);
		
	}

	@Override
	public void saveEvents(String eventId, String eventType,
			String eventComponent, String eventComponenTarget,
			String eventData, String formId) {
		eventsDao.saveEvents(eventId, eventType, eventComponent, eventComponenTarget, eventData, formId);
		
	}

		
}
