package service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dao.MasterRegionalDao;
import dto.MasterRegionalDto;
import service.MasterRegionalSvc;

@Service("masterRegionalSvc")
@Transactional
public class MasterRegionalSvcImpl implements MasterRegionalSvc {

	@Autowired
	private MasterRegionalDao masterRegionalDao;

	@Override
	public List<MasterRegionalDto> findAllRegional() {
		List<Object[]> listRegional = masterRegionalDao.findAllRegional();
		List<MasterRegionalDto> listRegionalDto = new ArrayList<>();
		for (Object[] obj : listRegional) {
			MasterRegionalDto regionalDto = new MasterRegionalDto();
			regionalDto.setKodeRegion(obj[0].toString());
			regionalDto.setNamaRegion(obj[1].toString());
			regionalDto.setNotes(obj[2].toString());
			if(Integer.parseInt(obj[3].toString())==0){
				regionalDto.setAktifStatus("Disabled");
			}else{
				regionalDto.setAktifStatus("Enabled");	
			}
			regionalDto.setCreateuser(obj[4].toString());
			regionalDto.setCreateDate(obj[5].toString());
			regionalDto.setModifyUser(obj[6].toString());
			regionalDto.setModifyDate(obj[7].toString());
			listRegionalDto.add(regionalDto);
		}
		return listRegionalDto;
}

	@Override
	public void DeleteRegional(String modifyUser, String regionCode) {
		masterRegionalDao.DeleteRegional(modifyUser, regionCode);
	}

	@Override
	public void UpdateRegional(String regionName, String notes, String aktifStatus,
			String modifyUser, String regionCode) {
		masterRegionalDao.UpdateRegional(regionName, notes, aktifStatus, modifyUser, regionCode);
	}

	@Override
	public void InsertRegional(String regionalCode, String regionalName,
			String notes, int aktifStatus, String createUser) {
		masterRegionalDao.InsertRegional(regionalCode, regionalName, notes, aktifStatus, createUser);
	}

	@Override
	public List<MasterRegionalDto> searchAllRegional(String search) {
		List<Object[]> listRegional = masterRegionalDao.searchAllRegional(search);
		List<MasterRegionalDto> listRegionalDto = new ArrayList<>();
		for (Object[] obj : listRegional) {
			MasterRegionalDto regionalDto = new MasterRegionalDto();
			regionalDto.setKodeRegion(obj[0].toString());
			regionalDto.setNamaRegion(obj[1].toString());
			regionalDto.setNotes(obj[2].toString());
			if(Integer.parseInt(obj[3].toString())==0){
				regionalDto.setAktifStatus("Disabled");
			}else{
				regionalDto.setAktifStatus("Enabled");	
			}
			regionalDto.setCreateuser(obj[4].toString());
			regionalDto.setCreateDate(obj[5].toString());
			regionalDto.setModifyUser(obj[6].toString());
			regionalDto.setModifyDate(obj[7].toString());
			regionalDto.setItemList(true);
			listRegionalDto.add(regionalDto);
		}
		return listRegionalDto;
	}
	
	@Override
	public List<MasterRegionalDto> searchRegionalByRegionalId(String regionalId) {
		List<Object[]> listRegional = masterRegionalDao.searchRegionalByRegionalId(regionalId);
		List<MasterRegionalDto> listRegionalDto = new ArrayList<>();
		for (Object[] obj : listRegional) {
			MasterRegionalDto regionalDto = new MasterRegionalDto();
			regionalDto.setKodeRegion(obj[0].toString());
			regionalDto.setNamaRegion(obj[1].toString());
			regionalDto.setNotes(obj[2].toString());
			if(Integer.parseInt(obj[3].toString())==0){
				regionalDto.setAktifStatus("Disabled");
			}else{
				regionalDto.setAktifStatus("Enabled");	
			}
			regionalDto.setCreateuser(obj[4].toString());
			regionalDto.setCreateDate(obj[5].toString());
			regionalDto.setModifyUser(obj[6].toString());
			regionalDto.setModifyDate(obj[7].toString());
			listRegionalDto.add(regionalDto);
		}
		return listRegionalDto;
	}

	@Override
	public List<MasterRegionalDto> searchAllRegional2(String search) {
		List<Object[]> listRegional = masterRegionalDao.searchAllRegional2(search);
		List<MasterRegionalDto> listRegionalDto = new ArrayList<>();
		for (Object[] obj : listRegional) {
			MasterRegionalDto regionalDto = new MasterRegionalDto();
			regionalDto.setKodeRegion(obj[0].toString());
			regionalDto.setNamaRegion(obj[1].toString());
			regionalDto.setNotes(obj[2].toString());
			if(Integer.parseInt(obj[3].toString())==0){
				regionalDto.setAktifStatus("Disabled");
			}else{
				regionalDto.setAktifStatus("Enabled");	
			}
			regionalDto.setCreateuser(obj[4].toString());
			regionalDto.setCreateDate(obj[5].toString());
			regionalDto.setModifyUser(obj[6].toString());
			regionalDto.setModifyDate(obj[7].toString());
			listRegionalDto.add(regionalDto);
		}
		return listRegionalDto;
	}
}
