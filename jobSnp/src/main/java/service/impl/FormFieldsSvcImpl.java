package service.impl;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Hex;
import org.apache.poi.hssf.record.DSFRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import service.AuthorityMstSvc;
import service.CabangMstSvc;
import service.EventsSvc;
import service.FormFieldsSvc;
import service.FormsSvc;
import service.MenuMstSvc;
import service.LoginSvc;
import service.UserProfileSvc;
import dao.AuthorityMstDao;
import dao.CabangMstDao;
import dao.EventsDao;
import dao.FormFieldsDao;
import dao.FormsDao;
import dao.MenuMstDao;
import dao.UserProfileDao;
import dto.AuthorityMstDto;
import dto.CabangMstDto;
import dto.EventsDto;
import dto.FormFieldsDto;
import dto.FormRowsDto;
import dto.FormsDto;
import dto.MenuMstDto;
import dto.UserProfileDto;
import dto.MenuUrlDto;
import entity.CabangMst;
import entity.UserMenuAccordion;
import entity.UserProfile;

@Service("formFieldsSvc")
@Transactional
public class FormFieldsSvcImpl implements FormFieldsSvc {
	
	@Autowired
	private FormFieldsDao formFieldsDao;

	@Override
	public List<FormFieldsDto> findFormFieldsWithSearch(String keySearch) {
		List<FormFieldsDto> listFormFieldsDto = new ArrayList<FormFieldsDto>();
		List<Object[]> listObject = new ArrayList<Object[]>();
		listObject = formFieldsDao.findFormFieldsWithSearch(keySearch);
		if (listObject.size() != 0) {
			for (Object[] obj : listObject) {
				FormFieldsDto ff = new FormFieldsDto();
				ff.setFieldId(obj[0].toString());
				ff.setFieldName(obj[1].toString());
				ff.setFieldType(obj[2].toString());
				ff.setFieldVisibility(obj[3].toString());
				ff.setFieldLabel(obj[4].toString());
				ff.setFieldGlobalValue(obj[5].toString());
				ff.setFieldWeight(obj[6].toString());
				ff.setFieldStore(obj[7].toString());
				ff.setFieldExtra(obj[8].toString());
				ff.setRowId(obj[9].toString());
				listFormFieldsDto.add(ff);
			}
		}
		return listFormFieldsDto;
	}
	
	@Override
	public List<FormFieldsDto> findFormFieldsByRowId(String keySearch) {
		List<FormFieldsDto> listFormFieldsDto = new ArrayList<FormFieldsDto>();
		List<Object[]> listObject = new ArrayList<Object[]>();
		listObject = formFieldsDao.findFormFieldsByRowId(keySearch);
		if (listObject.size() != 0) {
			for (Object[] obj : listObject) {
				FormFieldsDto ff = new FormFieldsDto();
				ff.setFieldId(obj[0].toString());
				ff.setFieldName(obj[1].toString());
				ff.setFieldType(obj[2].toString());
				ff.setFieldVisibility(obj[3].toString());
				ff.setFieldLabel(obj[4].toString());
				ff.setFieldGlobalValue(obj[5].toString());
				ff.setFieldWeight(obj[6].toString());
				ff.setFieldStore(obj[7].toString());
				ff.setFieldExtra(obj[8].toString());
				ff.setRowId(obj[9].toString());
				listFormFieldsDto.add(ff);
			}
		}
		return listFormFieldsDto;
	}

	@Override
	public FormFieldsDto findFormFieldsById(String keySearch) {
		FormFieldsDto ff = new FormFieldsDto();
		List<Object[]> listObject = new ArrayList<Object[]>();
		listObject = formFieldsDao.findFormFieldsWithSearch(keySearch);
		if (listObject.size() != 0) {
			for (Object[] obj : listObject) {
				ff.setFieldId(obj[0].toString());
				ff.setFieldName(obj[1].toString());
				ff.setFieldType(obj[2].toString());
				ff.setFieldVisibility(obj[3].toString());
				ff.setFieldLabel(obj[4].toString());
				ff.setFieldGlobalValue(obj[5].toString());
				ff.setFieldWeight(obj[6].toString());
				ff.setFieldStore(obj[7].toString());
				ff.setFieldExtra(obj[8].toString());
				ff.setRowId(obj[9].toString());
			}
		}
		return ff;
	}

	@Override
	public void deleteFormFields(String field_id) {
		formFieldsDao.deleteFormFields(field_id);
		
	}

	@Override
	public void updateFormFields(String field_name, String field_type,
			String field_visibility, String field_label,
			String field_global_value, String field_weight, String field_store,
			String field_extra, String row_id, String field_id) {
		formFieldsDao.updateFormFields(field_name, field_type, field_visibility, field_label, field_global_value, field_weight, field_store, field_extra, row_id, field_id);
		
	}

	@Override
	public void saveFormFields(String field_id, String field_name,
			String field_type, String field_visibility, String field_label,
			String field_global_value, String field_weight, String field_store,
			String field_extra, String row_id) {
		formFieldsDao.saveFormFields(field_id, field_name, field_type, field_visibility, field_label, field_global_value, field_weight, field_store, field_extra, row_id);
		
	}

	

	

		
}
