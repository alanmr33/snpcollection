package service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dao.AuthorityListDao;
import dao.AuthorityMstDao;
import dto.AuthorityListDto;
import dto.AuthorityMstDto;
import service.AuthorityListSvc;
import service.AuthorityMstSvc;


@Service("authorityListSvc")
@Transactional
public class AuthorityListSvcImpl implements AuthorityListSvc{
	
	@Autowired
	private AuthorityListDao authorityListDao;

	@Override
	public List<AuthorityListDto> findAuthorityListByAuthorityId(
			String authorityId) {
		List<Object[]> listauthority = authorityListDao.findAuthorityListByAuthorityId(authorityId);
		List<AuthorityListDto> listAuthorityListDto = new ArrayList<AuthorityListDto>();
		for (Object[] obj : listauthority) {
			AuthorityListDto authorityListDto = new AuthorityListDto();
			authorityListDto.setAuthorityId(Integer.valueOf(obj[0].toString()));
			authorityListDto.setMenu(Integer.valueOf(obj[1].toString()));
			authorityListDto.setAuthorityName(obj[2].toString());
			authorityListDto.setMenuName(obj[3].toString());
			authorityListDto.setUrl(obj[4].toString());
			listAuthorityListDto.add(authorityListDto);
		}
		return listAuthorityListDto;
	}

	@Override
	public List<AuthorityListDto> findAllAuthorityBySearch(String keySearch) {
		List<Object[]> listauthority = authorityListDao.findAllAuthorityBySearch(keySearch);
		List<AuthorityListDto> listAuthorityListDto = new ArrayList<AuthorityListDto>();
		for (Object[] obj : listauthority) {
			AuthorityListDto authorityListDto = new AuthorityListDto();
			authorityListDto.setAuthorityId(Integer.valueOf(obj[0].toString()));
			authorityListDto.setAuthorityName(obj[1].toString());
			listAuthorityListDto.add(authorityListDto);
		}
		return listAuthorityListDto;
	}

	@Override
	public void saveAuthorityList(String authorityId, String menu) {
		authorityListDao.saveAuthority(authorityId, menu);
		
	}

	@Override
	public void delAuthorityList(String authorityId) {
		authorityListDao.delAuthorityList(authorityId);
		
	}
	
	
}
