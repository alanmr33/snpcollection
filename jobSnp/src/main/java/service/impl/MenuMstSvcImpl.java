package service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.zkoss.zk.ui.Sessions;

import dao.MenuMstDao;
import dto.MenuMstDto;
import dto.UserProfileDto;
import entity.MenuMst;
import entity.MenuMstPK;
import service.MenuMstSvc;

@Service("menuMstSvc")
@Transactional
public class MenuMstSvcImpl implements MenuMstSvc {

	@Autowired
	private MenuMstDao menuMstDao;

	@Override
	public List<MenuMstDto> findMenuAllBySearch(String keySearch) {
		List<Object[]> listMenuObj = menuMstDao.findMenuAllBySearch(keySearch);
		List<MenuMstDto> listMenuMstDto = new ArrayList<MenuMstDto>();
		if (listMenuObj.size() != 0) {
			for (Object[] obj : listMenuObj) {
				MenuMstDto menuMstDto = new MenuMstDto();
				menuMstDto.setMenu(Integer.valueOf(obj[0].toString()));
				menuMstDto.setMenuName(obj[1].toString());
				menuMstDto.setUrl(obj[2].toString());
				menuMstDto.setMenuSort(Integer.valueOf(obj[3].toString()));
				menuMstDto.setDisplayFlg(obj[4].toString());
				menuMstDto.setKetAktifStatus(obj[9].toString());
				listMenuMstDto.add(menuMstDto);
			}
		}
		return listMenuMstDto;
	}

	@Override
	public void DeleteMenu(String modifyUser, int menu) {
		menuMstDao.DeleteMenu(modifyUser, menu);
		
	}

	@Override
	public void aktifkanMenu(String modifyUser, int menu) {
		menuMstDao.aktifkanMenu(modifyUser, menu);
		
	}

	
}
