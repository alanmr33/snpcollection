package service;

import java.util.Date;
import java.util.List;

import dto.CollectionHeaderDto;
import dto.MobileTrackingDto;

public interface CollectionHeaderSvc {
	public void insertCollectionHeaderInUploadManual(String orderId, String lpkNo, String lpkDate, String collectorCode, String status);
	public void updateCollectionHeaderSatusToCmp(int orderId);
	public void execSP_CHECK_TASK(int orderId);
}
