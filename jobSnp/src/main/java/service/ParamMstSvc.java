package service;

import java.util.List;

import dto.CabangMstDto;
import dto.ParamMstDto;


public interface ParamMstSvc {
	public List<ParamMstDto> findParamWithSearch(String keySearch);
	public void deleteParam(String paramId);
	public void updateParam(String condition, String levelParam, String parentId, String paramId);
	public void saveParam(String paramId,String condition, String levelParam, String parentId);
	public ParamMstDto findOneParam(String paramId);
}
