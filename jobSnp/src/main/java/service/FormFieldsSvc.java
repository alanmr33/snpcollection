package service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import dto.AuthorityMstDto;
import dto.CabangMstDto;
import dto.EventsDto;
import dto.FormFieldsDto;
import dto.FormsDto;
import dto.UserProfileDto;
import entity.CabangMst;

public interface FormFieldsSvc {
	public List<FormFieldsDto> findFormFieldsWithSearch(String keySearch);
	public List<FormFieldsDto> findFormFieldsByRowId(String keySearch);
	public FormFieldsDto findFormFieldsById(String keySearch);
	public void deleteFormFields(String field_id);
	public void updateFormFields(String field_name,String field_type,String field_visibility,String field_label,String field_global_value, String field_weight, String field_store, String field_extra,String row_id, String field_id);
	public void saveFormFields(String field_id,String field_name,String field_type,String field_visibility,String field_label,String field_global_value, String field_weight, String field_store, String field_extra,String row_id);
}
