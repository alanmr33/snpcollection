package service;

import java.util.Date;
import java.util.List;

import dto.MobileTrackingDto;


public interface MobileTrackingSvc {
	public List<MobileTrackingDto> findMobileTrackingAllByCabang(String kodeCabang, Date dateAwal, Date dateAkhir, String keySearch);
	public List<MobileTrackingDto> findMobileTrackingByKodeCabangUserId(String kodeCabang,String userId, Date dateAwal, Date dateAkhir, String keySearch);
	public List<MobileTrackingDto> findMobileTrackingByKodeRegionUserId(String kodeRegion,String userId, Date dateAwal, Date dateAkhir, String keySearch);
	public List<MobileTrackingDto> findMobileTrackingByKodeRegionKodeCabang(String kodeRegion,String kodeCabang, Date dateAwal, Date dateAkhir, String keySearch);
	public List<MobileTrackingDto> findMobileTrackingByUserIdNotOrderId(String userId, Date dateAwal, Date dateAkhir);
	public List<MobileTrackingDto> findMobileTrackingByUserIdWithOrderId(String userId, Date dateAwal, Date dateAkhir);
}
