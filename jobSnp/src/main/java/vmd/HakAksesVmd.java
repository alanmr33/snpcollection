package vmd;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.record.DSFRecord;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

import dto.AuthorityListDto;
import dto.CabangMstDto;
import dto.MenuMstDto;
import dto.UserProfileDto;
import pagevmd.NavigationVmd;
import service.AuthorityListSvc;
import service.CabangMstSvc;
import service.MenuMstSvc;
import service.UserProfileSvc;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class HakAksesVmd extends NavigationVmd{

	@WireVariable
	private AuthorityListSvc authorityListSvc;
	
	
	
	private List<AuthorityListDto> listAuthorityListDto = new ArrayList<AuthorityListDto>();
	private List<AuthorityListDto> listAuthorityListDto2 = new ArrayList<AuthorityListDto>();
	private AuthorityListDto authorityListDto;
	private int page = 10;
	private String keySearch = "";
	private boolean visibleAddEdit = false;
	private boolean menMasterUser = false;
	private boolean menChangPass = false;
	private boolean menApplicationRoles = false;
	private boolean menMasterRegional = false;
	private boolean menMasterCabang = false;
	private boolean menMasterMenu = false;
	private boolean menMasterAuthority = false;
	private boolean menMasterProvinsi = false;
	private boolean menMasterHandling = false;
	private boolean menUploadHandling = false;
	private boolean menReassignment = false;
	private boolean menTaskAdjustment = false;
	private boolean menNotification = false;
	private boolean menMobileTracking = false;
	private boolean menReportKunjunganKolektor = false;
	private boolean menRekonKolektor = false;
	private boolean menCollectorFullData = false;
	private boolean menMasterParameter = false;
	private boolean menMasterForms = false;
	private boolean menMasterEvents = false;
	private boolean menFormRows = false;
	private UserProfileDto userProfileDtoSession = new UserProfileDto();
	
	
	
	public boolean isMenFormRows() {
		return menFormRows;
	}

	public void setMenFormRows(boolean menFormRows) {
		this.menFormRows = menFormRows;
	}

	public boolean isMenMasterEvents() {
		return menMasterEvents;
	}

	public void setMenMasterEvents(boolean menMasterEvents) {
		this.menMasterEvents = menMasterEvents;
	}

	public boolean isMenMasterForms() {
		return menMasterForms;
	}

	public void setMenMasterForms(boolean menMasterForms) {
		this.menMasterForms = menMasterForms;
	}

	public boolean isMenMasterParameter() {
		return menMasterParameter;
	}

	public void setMenMasterParameter(boolean menMasterParameter) {
		this.menMasterParameter = menMasterParameter;
	}

	public boolean isMenMasterHandling() {
		return menMasterHandling;
	}

	public void setMenMasterHandling(boolean menMasterHandling) {
		this.menMasterHandling = menMasterHandling;
	}

	public boolean isMenMobileTracking() {
		return menMobileTracking;
	}

	public void setMenMobileTracking(boolean menMobileTracking) {
		this.menMobileTracking = menMobileTracking;
	}

	public UserProfileDto getUserProfileDtoSession() {
		return userProfileDtoSession;
	}

	public void setUserProfileDtoSession(UserProfileDto userProfileDtoSession) {
		this.userProfileDtoSession = userProfileDtoSession;
	}

	public List<AuthorityListDto> getListAuthorityListDto2() {
		return listAuthorityListDto2;
	}

	public void setListAuthorityListDto2(
			List<AuthorityListDto> listAuthorityListDto2) {
		this.listAuthorityListDto2 = listAuthorityListDto2;
	}

	public boolean isMenMasterUser() {
		return menMasterUser;
	}

	public void setMenMasterUser(boolean menMasterUser) {
		this.menMasterUser = menMasterUser;
	}

	public boolean isMenChangPass() {
		return menChangPass;
	}

	public void setMenChangPass(boolean menChangPass) {
		this.menChangPass = menChangPass;
	}

	public boolean isMenMasterRegional() {
		return menMasterRegional;
	}

	public void setMenMasterRegional(boolean menMasterRegional) {
		this.menMasterRegional = menMasterRegional;
	}

	public boolean isMenMasterCabang() {
		return menMasterCabang;
	}

	public void setMenMasterCabang(boolean menMasterCabang) {
		this.menMasterCabang = menMasterCabang;
	}

	public boolean isMenMasterMenu() {
		return menMasterMenu;
	}

	public void setMenMasterMenu(boolean menMasterMenu) {
		this.menMasterMenu = menMasterMenu;
	}

	public boolean isMenMasterAuthority() {
		return menMasterAuthority;
	}

	public void setMenMasterAuthority(boolean menMasterAuthority) {
		this.menMasterAuthority = menMasterAuthority;
	}

	public boolean isVisibleAddEdit() {
		return visibleAddEdit;
	}

	public void setVisibleAddEdit(boolean visibleAddEdit) {
		this.visibleAddEdit = visibleAddEdit;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getKeySearch() {
		return keySearch;
	}

	public void setKeySearch(String keySearch) {
		this.keySearch = keySearch;
	}

	public List<AuthorityListDto> getListAuthorityListDto() {
		return listAuthorityListDto;
	}

	public void setListAuthorityListDto(List<AuthorityListDto> listAuthorityListDto) {
		this.listAuthorityListDto = listAuthorityListDto;
	}

	public AuthorityListDto getAuthorityListDto() {
		return authorityListDto;
	}

	public void setAuthorityListDto(AuthorityListDto authorityListDto) {
		this.authorityListDto = authorityListDto;
	}
	
	
	public boolean isMenReportKunjunganKolektor() {
		return menReportKunjunganKolektor;
	}

	public void setMenReportKunjunganKolektor(boolean menReportKunjunganKolektor) {
		this.menReportKunjunganKolektor = menReportKunjunganKolektor;
	}

	public boolean isMenApplicationRoles() {
		return menApplicationRoles;
	}

	public void setMenApplicationRoles(boolean menApplicationRoles) {
		this.menApplicationRoles = menApplicationRoles;
	}

	public boolean isMenMasterProvinsi() {
		return menMasterProvinsi;
	}

	public void setMenMasterProvinsi(boolean menMasterProvinsi) {
		this.menMasterProvinsi = menMasterProvinsi;
	}

	public boolean isMenUploadHandling() {
		return menUploadHandling;
	}

	public void setMenUploadHandling(boolean menUploadHandling) {
		this.menUploadHandling = menUploadHandling;
	}

	public boolean isMenReassignment() {
		return menReassignment;
	}

	public void setMenReassignment(boolean menReassignment) {
		this.menReassignment = menReassignment;
	}

	public boolean isMenTaskAdjustment() {
		return menTaskAdjustment;
	}

	public void setMenTaskAdjustment(boolean menTaskAdjustment) {
		this.menTaskAdjustment = menTaskAdjustment;
	}

	public boolean isMenNotification() {
		return menNotification;
	}

	public void setMenNotification(boolean menNotification) {
		this.menNotification = menNotification;
	}

	public boolean isMenRekonKolektor() {
		return menRekonKolektor;
	}

	public void setMenRekonKolektor(boolean menRekonKolektor) {
		this.menRekonKolektor = menRekonKolektor;
	}

	public boolean isMenCollectorFullData() {
		return menCollectorFullData;
	}

	public void setMenCollectorFullData(boolean menCollectorFullData) {
		this.menCollectorFullData = menCollectorFullData;
	}

	@Init
	public void load() {
		userProfileDtoSession = (UserProfileDto) Sessions.getCurrent().getAttribute("user");
		listAuthorityListDto = authorityListSvc.findAllAuthorityBySearch(keySearch);
		if (listAuthorityListDto.size() != 0) {
			List<String> listMenuAkses = new ArrayList<String>();
			List<AuthorityListDto> tempListAuthority = new ArrayList<AuthorityListDto>();
			for (int i = 0; i < listAuthorityListDto.size(); i++) {
				String  menuAkses= "";
				listAuthorityListDto2 = new ArrayList<AuthorityListDto>();
				listAuthorityListDto2 = authorityListSvc.findAuthorityListByAuthorityId(String.valueOf(listAuthorityListDto.get(i).getAuthorityId()));
				if (listAuthorityListDto2.size() != 0) {
					for (int j = 0; j < listAuthorityListDto2.size(); j++) {
						if (menuAkses.equals("")) {
							menuAkses = menuAkses +listAuthorityListDto2.get(j).getMenuName();
						}else {
							menuAkses = menuAkses +", "+listAuthorityListDto2.get(j).getMenuName();
						}
						
					}
				}
				listMenuAkses.add(menuAkses);
			}
			for (int i = 0; i < listAuthorityListDto.size(); i++) {
				AuthorityListDto authorityListDto = new AuthorityListDto();
				authorityListDto.setAuthorityId(listAuthorityListDto.get(i).getAuthorityId());
				authorityListDto.setAuthorityName(listAuthorityListDto.get(i).getAuthorityName());
				authorityListDto.setListMenu(listMenuAkses.get(i));
				tempListAuthority.add(authorityListDto);
			}
			listAuthorityListDto = tempListAuthority;
		}
	}
	
	@Command("page")
	@NotifyChange("page")
	public void page() {
		setPage(getPage());
	}
	
	@Command("update")
	@NotifyChange({"menFormRows","menMasterEvents","menMasterForms","menMasterParameter","menReportKunjunganKolektor","visibleAddEdit","authorityListDto", "menMasterUser","menChangPass", "menMasterRegional", "menMasterCabang", "menMasterMenu","menUploadHandling","menNotification",  
		"menMasterHandling","menMasterAuthority","menMobileTracking","menTaskAdjustment","menReassignment","menApplicationRoles","menManualAssignReport","menRekonKolektor","menCollectorFullData","menMasterProvinsi"})
	public void update() {
		List<AuthorityListDto> listAuthorityListDto = authorityListSvc.findAuthorityListByAuthorityId(String.valueOf(authorityListDto.getAuthorityId()));
		for (int i = 0; i < listAuthorityListDto.size(); i++) {
			if (listAuthorityListDto.get(i).getMenu() == 1) {
				setMenMasterUser(true);
			}
			if (listAuthorityListDto.get(i).getMenu() == 2) {
				setMenMasterRegional(true);
			}
			if (listAuthorityListDto.get(i).getMenu() == 3) {
				setMenMasterCabang(true);
			}
			if (listAuthorityListDto.get(i).getMenu() == 4) {
				setMenMasterMenu(true);
			}
			if (listAuthorityListDto.get(i).getMenu() == 5) {
				setMenMasterAuthority(true);
			}
			if (listAuthorityListDto.get(i).getMenu() == 1002) {
				setMenApplicationRoles(true);
			}
			if (listAuthorityListDto.get(i).getMenu() == 6) {
				setMenChangPass(true);
			}
			if (listAuthorityListDto.get(i).getMenu() == 1006) {
				setMenReassignment(true);
			}
			if (listAuthorityListDto.get(i).getMenu() == 1007) {
				setMenTaskAdjustment(true);
			}
			if (listAuthorityListDto.get(i).getMenu() == 1008) {
				setMenMobileTracking(true);
			}
			
			if (listAuthorityListDto.get(i).getMenu() == 1005) {
				setMenNotification(true);
			}
			if (listAuthorityListDto.get(i).getMenu() == 1004) {
				setMenUploadHandling(true);
			}
			if (listAuthorityListDto.get(i).getMenu() == 1009) {
				setMenReportKunjunganKolektor(true);
			}
			if (listAuthorityListDto.get(i).getMenu() == 1010) {
				setMenRekonKolektor(true);
			}
			if (listAuthorityListDto.get(i).getMenu() == 1011) {
				setMenCollectorFullData(true);
			}
			if (listAuthorityListDto.get(i).getMenu() == 1003) {
				setMenMasterProvinsi(true);
			}
			if (listAuthorityListDto.get(i).getMenu() == 1012) {
				setMenMasterHandling(true);
			}
			if (listAuthorityListDto.get(i).getMenu() == 1013) {
				setMenMasterParameter(true);
			}
			if (listAuthorityListDto.get(i).getMenu() == 2004) {
				setMenMasterForms(true);
			}
			if (listAuthorityListDto.get(i).getMenu() == 2005) {
				setMenMasterEvents(true);
			}
			if (listAuthorityListDto.get(i).getMenu() == 3004) {
				setMenFormRows(true);
			}
		}
		setVisibleAddEdit(true);
	}
	
	@Command("search")
	@NotifyChange({ "listAuthorityListDto","listAuthorityListDto2"})
	public void search() {
		listAuthorityListDto = authorityListSvc.findAllAuthorityBySearch(keySearch);
		if (listAuthorityListDto.size() != 0) {
			List<String> listMenuAkses = new ArrayList<String>();
			List<AuthorityListDto> tempListAuthority = new ArrayList<AuthorityListDto>();
			for (int i = 0; i < listAuthorityListDto.size(); i++) {
				String  menuAkses= "";
				listAuthorityListDto2 = new ArrayList<AuthorityListDto>();
				listAuthorityListDto2 = authorityListSvc.findAuthorityListByAuthorityId(String.valueOf(listAuthorityListDto.get(i).getAuthorityId()));
				if (listAuthorityListDto2.size() != 0) {
					for (int j = 0; j < listAuthorityListDto2.size(); j++) {
						if (menuAkses.equals("")) {
							menuAkses = menuAkses +listAuthorityListDto2.get(j).getMenuName();
						}else {
							menuAkses = menuAkses +", "+listAuthorityListDto2.get(j).getMenuName();
						}
						
					}
				}
				listMenuAkses.add(menuAkses);
			}
			for (int i = 0; i < listAuthorityListDto.size(); i++) {
				AuthorityListDto authorityListDto = new AuthorityListDto();
				authorityListDto.setAuthorityId(listAuthorityListDto.get(i).getAuthorityId());
				authorityListDto.setAuthorityName(listAuthorityListDto.get(i).getAuthorityName());
				authorityListDto.setListMenu(listMenuAkses.get(i));
				tempListAuthority.add(authorityListDto);
			}
			listAuthorityListDto = tempListAuthority;
		}
	}
	
	@Command("close")
	@NotifyChange({"menFormRows","menMasterEvents","menMasterForms","menMasterParameter","menReportKunjunganKolektor","visibleAddEdit","authorityListDto", "menMasterUser","menChangPass", "menMasterRegional", "menMasterCabang", "menMasterMenu","menUploadHandling","menNotification", 
		"menMasterHandling","menMasterAuthority","menMobileTracking","menTaskAdjustment","menReassignment","menApplicationRoles","menManualAssignReport","menRekonKolektor","menCollectorFullData","menMasterProvinsi"})
	public void close() {
		setMenFormRows(false);
		setMenMasterUser(false);
		setMenMasterRegional(false);
		setMenMasterCabang(false);
		setMenMasterMenu(false);
		setMenMasterAuthority(false);
		setMenApplicationRoles(false);
		setMenChangPass(false);
		setMenReassignment(false);
		setMenTaskAdjustment(false);
		setMenMobileTracking(false);
		setMenMasterProvinsi(false);
		setMenMasterHandling(false);
		setMenUploadHandling(false);
		setMenReportKunjunganKolektor(false);
		setMenRekonKolektor(false);
		setMenCollectorFullData(false);
		setMenNotification(false);
		setMenMasterParameter(false);
		setMenMasterForms(false);
		setMenMasterEvents(false);
		authorityListDto = new AuthorityListDto();;
		setVisibleAddEdit(false);
	}
	
	@Command("save")
	@NotifyChange({"menFormRows","menMasterEvents","menMasterForms","menMasterParameter","menReportKunjunganKolektor","listAuthorityListDto","visibleAddEdit","authorityListDto","authorityListDto", "menMasterUser","menChangPass", "menMasterRegional", "menMasterCabang", "menMasterMenu","menUploadHandling","menNotification", 
		"menMasterHandling","menMasterAuthority","menMobileTracking","menTaskAdjustment","menReassignment","menApplicationRoles","menManualAssignReport","menRekonKolektor","menCollectorFullData","menMasterProvinsi"})
	public void save() {
		authorityListSvc.delAuthorityList(String.valueOf(authorityListDto.getAuthorityId()));
		if (menMasterUser == true) {
			authorityListSvc.saveAuthorityList(String.valueOf(authorityListDto.getAuthorityId()), String.valueOf(1));
		}
		if (menApplicationRoles == true) {
			authorityListSvc.saveAuthorityList(String.valueOf(authorityListDto.getAuthorityId()), String.valueOf(1002));
		}
		if (menChangPass == true) {
			authorityListSvc.saveAuthorityList(String.valueOf(authorityListDto.getAuthorityId()), String.valueOf(6));
		}
		if (menMasterRegional == true) {
			authorityListSvc.saveAuthorityList(String.valueOf(authorityListDto.getAuthorityId()), String.valueOf(2));
		}
		if (menMasterCabang == true) {
			authorityListSvc.saveAuthorityList(String.valueOf(authorityListDto.getAuthorityId()), String.valueOf(3));
		}
		if (menMasterAuthority == true) {
			authorityListSvc.saveAuthorityList(String.valueOf(authorityListDto.getAuthorityId()), String.valueOf(5));
		}
		if (menMasterMenu == true) {
			authorityListSvc.saveAuthorityList(String.valueOf(authorityListDto.getAuthorityId()), String.valueOf(4));
		}
		if (menMobileTracking == true) {
			authorityListSvc.saveAuthorityList(String.valueOf(authorityListDto.getAuthorityId()), String.valueOf(1008));
		}
		if (menReassignment == true) {
			authorityListSvc.saveAuthorityList(String.valueOf(authorityListDto.getAuthorityId()), String.valueOf(1006));
		}
		if (menTaskAdjustment == true) {
			authorityListSvc.saveAuthorityList(String.valueOf(authorityListDto.getAuthorityId()), String.valueOf(1007));
		}
		if (menMasterProvinsi == true) {
			authorityListSvc.saveAuthorityList(String.valueOf(authorityListDto.getAuthorityId()), String.valueOf(1003));
		}
		if (menMasterHandling == true) {
			authorityListSvc.saveAuthorityList(String.valueOf(authorityListDto.getAuthorityId()), String.valueOf(1012));
		}
		if (menUploadHandling == true) {
			authorityListSvc.saveAuthorityList(String.valueOf(authorityListDto.getAuthorityId()), String.valueOf(1004));
		}
		if (menReportKunjunganKolektor == true) {
			authorityListSvc.saveAuthorityList(String.valueOf(authorityListDto.getAuthorityId()), String.valueOf(1009));
		}
		if (menCollectorFullData == true) {
			authorityListSvc.saveAuthorityList(String.valueOf(authorityListDto.getAuthorityId()), String.valueOf(1011));
		}
		if (menRekonKolektor == true) {
			authorityListSvc.saveAuthorityList(String.valueOf(authorityListDto.getAuthorityId()), String.valueOf(1010));
		}
		if (menNotification == true) {
			authorityListSvc.saveAuthorityList(String.valueOf(authorityListDto.getAuthorityId()), String.valueOf(1005));
		}
		if (menMasterParameter == true) {
			authorityListSvc.saveAuthorityList(String.valueOf(authorityListDto.getAuthorityId()), String.valueOf(1013));
		}
		if (menMasterForms == true) {
			authorityListSvc.saveAuthorityList(String.valueOf(authorityListDto.getAuthorityId()), String.valueOf(2004));
		}
		if (menMasterEvents == true) {
			authorityListSvc.saveAuthorityList(String.valueOf(authorityListDto.getAuthorityId()), String.valueOf(2005));
		}
		if (menFormRows == true) {
			authorityListSvc.saveAuthorityList(String.valueOf(authorityListDto.getAuthorityId()), String.valueOf(3004));
		}
		listAuthorityListDto = authorityListSvc.findAllAuthorityBySearch(keySearch);
		if (listAuthorityListDto.size() != 0) {
			List<String> listMenuAkses = new ArrayList<String>();
			List<AuthorityListDto> tempListAuthority = new ArrayList<AuthorityListDto>();
			for (int i = 0; i < listAuthorityListDto.size(); i++) {
				String  menuAkses= "";
				listAuthorityListDto2 = new ArrayList<AuthorityListDto>();
				listAuthorityListDto2 = authorityListSvc.findAuthorityListByAuthorityId(String.valueOf(listAuthorityListDto.get(i).getAuthorityId()));
				if (listAuthorityListDto2.size() != 0) {
					for (int j = 0; j < listAuthorityListDto2.size(); j++) {
						if (menuAkses.equals("")) {
							menuAkses = menuAkses +listAuthorityListDto2.get(j).getMenuName();
						}else {
							menuAkses = menuAkses +", "+listAuthorityListDto2.get(j).getMenuName();
						}
						
					}
				}
				listMenuAkses.add(menuAkses);
			}
			for (int i = 0; i < listAuthorityListDto.size(); i++) {
				AuthorityListDto authorityListDto = new AuthorityListDto();
				authorityListDto.setAuthorityId(listAuthorityListDto.get(i).getAuthorityId());
				authorityListDto.setAuthorityName(listAuthorityListDto.get(i).getAuthorityName());
				authorityListDto.setListMenu(listMenuAkses.get(i));
				tempListAuthority.add(authorityListDto);
			}
			listAuthorityListDto = tempListAuthority;
		}
		setMenMasterUser(false);
		setMenMasterRegional(false);
		setMenMasterCabang(false);
		setMenMasterMenu(false);
		setMenMasterAuthority(false);
		setMenApplicationRoles(false);
		setMenChangPass(false);
		setMenReassignment(false);
		setMenTaskAdjustment(false);
		setMenMobileTracking(false);
		setMenMasterProvinsi(false);
		setMenMasterHandling(false);
		setMenUploadHandling(false);
		setMenNotification(false);
		setMenReportKunjunganKolektor(false);
		setMenRekonKolektor(false);
		setMenCollectorFullData(false);
		setMenMasterParameter(false);
		setMenMasterForms(false);
		setMenMasterEvents(false);
		setMenFormRows(false);
		authorityListDto = new AuthorityListDto();
		setVisibleAddEdit(false);
		Messagebox.show("UPDATE AKSES SUCCESS!", "MESSAGE", null, null, null);
	}	
	
}
