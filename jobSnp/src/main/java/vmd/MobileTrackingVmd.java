package vmd;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Include;
import org.zkoss.zul.Messagebox;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import dto.AksesRegionCabangDto;
import dto.CabangMstDto;
import dto.MasterRegionalDto;
import dto.MobileTrackingDto;
import dto.SessionForMobileTrackingDto;
import dto.UserProfileDto;
import pagevmd.NavigationVmd;
import service.AksesRegionCabangSvc;
import service.AuthorityListSvc;
import service.CabangMstSvc;
import service.MasterRegionalSvc;
import service.MobileTrackingSvc;
import service.UserProfileSvc;
import service.impl.userProfileSvcImpl;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class MobileTrackingVmd extends NavigationVmd{
	
	@WireVariable
	private UserProfileSvc userProfileSvc;
	
	@WireVariable
	private MasterRegionalSvc masterRegionalSvc;
	
	@WireVariable
	private CabangMstSvc cabangMstSvc;
	
	@WireVariable
	private MobileTrackingSvc mobileTrackingSvc;
	
	@WireVariable
	private AksesRegionCabangSvc aksesRegionCabangSvc;
	
	private List<UserProfileDto> listUserKolektor = new ArrayList<UserProfileDto>();
	private List<UserProfileDto> listUserKolektorChoose = new ArrayList<UserProfileDto>();
	private List<MasterRegionalDto> listRegionalDto = new ArrayList<MasterRegionalDto>();
	private List<MasterRegionalDto> listRegionalDtoChoose = new ArrayList<MasterRegionalDto>();
	private List<MobileTrackingDto> listTracking = new ArrayList<MobileTrackingDto>();
	private List<CabangMstDto> listCabangMstDto = new ArrayList<CabangMstDto>();
	private List<CabangMstDto> listCabangMstDtoChoose = new ArrayList<CabangMstDto>();
	private List<MobileTrackingDto> listTrackingAll = new ArrayList<MobileTrackingDto>();
	private List<MobileTrackingDto> listTrackingWithOrderId = new ArrayList<MobileTrackingDto>();
	private UserProfileDto upSession = new UserProfileDto();
	private UserProfileDto userKolektor = new UserProfileDto();
	private MasterRegionalDto regionalDto = new MasterRegionalDto();
	private CabangMstDto cabangDto = new CabangMstDto();
	private MobileTrackingDto trackingByUser = new MobileTrackingDto();
	private MobileTrackingDto trackingDto = new MobileTrackingDto();
	private int page = 10;
	private int pageRegional = 10;
	private int pageCabang = 10;
	private int pageKolektor = 10;
	private int banyakListMap1 = 0;
	private int banyakListMap2 = 0;
	private Gson gsonMap1 = new GsonBuilder().create();
	private String jsonMap1;
	private Gson gsonMap2 = new GsonBuilder().create();
	private String jsonMap2;
	private boolean visibleRegional;
	private boolean visibleCabang;
	private boolean visibleKolektor;
	private String keySearch= "";
	private String keySearchregional = "";
	private String keySearchCabang = "";
	private String keySearchKolektor = "";
	private String stringArrayRegional = "";
	private String stringArrayCabang = "";
	private String stringArrayKolektor = "";
	private Date tglAwal;
	private Date tglAkhir;
	private List<MasterRegionalDto> listAksesRegional = new ArrayList<MasterRegionalDto>();
	private List<CabangMstDto> listAksesCabang = new ArrayList<CabangMstDto>();
	
	
	
	public List<MasterRegionalDto> getListAksesRegional() {
		return listAksesRegional;
	}
	public void setListAksesRegional(List<MasterRegionalDto> listAksesRegional) {
		this.listAksesRegional = listAksesRegional;
	}
	public List<CabangMstDto> getListAksesCabang() {
		return listAksesCabang;
	}
	public void setListAksesCabang(List<CabangMstDto> listAksesCabang) {
		this.listAksesCabang = listAksesCabang;
	}
	public UserProfileDto getUpSession() {
		return upSession;
	}
	public void setUpSession(UserProfileDto upSession) {
		this.upSession = upSession;
	}
	public String getKeySearch() {
		return keySearch;
	}
	public void setKeySearch(String keySearch) {
		this.keySearch = keySearch;
	}
	public Gson getGsonMap2() {
		return gsonMap2;
	}
	public void setGsonMap2(Gson gsonMap2) {
		this.gsonMap2 = gsonMap2;
	}
	public String getJsonMap2() {
		return jsonMap2;
	}
	public void setJsonMap2(String jsonMap2) {
		this.jsonMap2 = jsonMap2;
	}
	public MobileTrackingDto getTrackingDto() {
		return trackingDto;
	}
	public void setTrackingDto(MobileTrackingDto trackingDto) {
		this.trackingDto = trackingDto;
	}
	public Gson getGsonMap1() {
		return gsonMap1;
	}
	public void setGsonMap1(Gson gsonMap1) {
		this.gsonMap1 = gsonMap1;
	}
	public String getJsonMap1() {
		return jsonMap1;
	}
	public void setJsonMap1(String jsonMap1) {
		this.jsonMap1 = jsonMap1;
	}
	public int getBanyakListMap1() {
		return banyakListMap1;
	}
	public void setBanyakListMap1(int banyakListMap1) {
		this.banyakListMap1 = banyakListMap1;
	}
	public int getBanyakListMap2() {
		return banyakListMap2;
	}
	public void setBanyakListMap2(int banyakListMap2) {
		this.banyakListMap2 = banyakListMap2;
	}
	public List<MobileTrackingDto> getListTrackingAll() {
		return listTrackingAll;
	}
	public void setListTrackingAll(List<MobileTrackingDto> listTrackingAll) {
		this.listTrackingAll = listTrackingAll;
	}
	public List<MobileTrackingDto> getListTrackingWithOrderId() {
		return listTrackingWithOrderId;
	}
	public void setListTrackingWithOrderId(
			List<MobileTrackingDto> listTrackingWithOrderId) {
		this.listTrackingWithOrderId = listTrackingWithOrderId;
	}
	public List<MobileTrackingDto> getListTracking() {
		return listTracking;
	}
	public void setListTracking(List<MobileTrackingDto> listTracking) {
		this.listTracking = listTracking;
	}
	public int getPageRegional() {
		return pageRegional;
	}
	public void setPageRegional(int pageRegional) {
		this.pageRegional = pageRegional;
	}
	public int getPageCabang() {
		return pageCabang;
	}
	public void setPageCabang(int pageCabang) {
		this.pageCabang = pageCabang;
	}
	public int getPageKolektor() {
		return pageKolektor;
	}
	public void setPageKolektor(int pageKolektor) {
		this.pageKolektor = pageKolektor;
	}
	public Date getTglAwal() {
		return tglAwal;
	}
	public void setTglAwal(Date tglAwal) {
		this.tglAwal = tglAwal;
	}
	public Date getTglAkhir() {
		return tglAkhir;
	}
	public void setTglAkhir(Date tglAkhir) {
		this.tglAkhir = tglAkhir;
	}
	public List<UserProfileDto> getListUserKolektorChoose() {
		return listUserKolektorChoose;
	}
	public void setListUserKolektorChoose(
			List<UserProfileDto> listUserKolektorChoose) {
		this.listUserKolektorChoose = listUserKolektorChoose;
	}
	public List<MasterRegionalDto> getListRegionalDtoChoose() {
		return listRegionalDtoChoose;
	}
	public void setListRegionalDtoChoose(
			List<MasterRegionalDto> listRegionalDtoChoose) {
		this.listRegionalDtoChoose = listRegionalDtoChoose;
	}
	public List<CabangMstDto> getListCabangMstDtoChoose() {
		return listCabangMstDtoChoose;
	}
	public void setListCabangMstDtoChoose(List<CabangMstDto> listCabangMstDtoChoose) {
		this.listCabangMstDtoChoose = listCabangMstDtoChoose;
	}
	public String getStringArrayRegional() {
		return stringArrayRegional;
	}
	public void setStringArrayRegional(String stringArrayRegional) {
		this.stringArrayRegional = stringArrayRegional;
	}
	public String getStringArrayCabang() {
		return stringArrayCabang;
	}
	public void setStringArrayCabang(String stringArrayCabang) {
		this.stringArrayCabang = stringArrayCabang;
	}
	public String getStringArrayKolektor() {
		return stringArrayKolektor;
	}
	public void setStringArrayKolektor(String stringArrayKolektor) {
		this.stringArrayKolektor = stringArrayKolektor;
	}
	public String getKeySearchregional() {
		return keySearchregional;
	}
	public void setKeySearchregional(String keySearchregional) {
		this.keySearchregional = keySearchregional;
	}
	public String getKeySearchCabang() {
		return keySearchCabang;
	}
	public void setKeySearchCabang(String keySearchCabang) {
		this.keySearchCabang = keySearchCabang;
	}
	public String getKeySearchKolektor() {
		return keySearchKolektor;
	}
	public void setKeySearchKolektor(String keySearchKolektor) {
		this.keySearchKolektor = keySearchKolektor;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public MobileTrackingDto getTrackingByUser() {
		return trackingByUser;
	}
	public void setTrackingByUser(MobileTrackingDto trackingByUser) {
		this.trackingByUser = trackingByUser;
	}
	public List<UserProfileDto> getListUserKolektor() {
		return listUserKolektor;
	}
	public void setListUserKolektor(List<UserProfileDto> listUserKolektor) {
		this.listUserKolektor = listUserKolektor;
	}
	public List<MasterRegionalDto> getListRegionalDto() {
		return listRegionalDto;
	}
	public void setListRegionalDto(List<MasterRegionalDto> listRegionalDto) {
		this.listRegionalDto = listRegionalDto;
	}
	public List<CabangMstDto> getListCabangMstDto() {
		return listCabangMstDto;
	}
	public void setListCabangMstDto(List<CabangMstDto> listCabangMstDto) {
		this.listCabangMstDto = listCabangMstDto;
	}
	public UserProfileDto getUserKolektor() {
		return userKolektor;
	}
	public void setUserKolektor(UserProfileDto userKolektor) {
		this.userKolektor = userKolektor;
	}
	public MasterRegionalDto getRegionalDto() {
		return regionalDto;
	}
	public void setRegionalDto(MasterRegionalDto regionalDto) {
		this.regionalDto = regionalDto;
	}
	public CabangMstDto getCabangDto() {
		return cabangDto;
	}
	public void setCabangDto(CabangMstDto cabangDto) {
		this.cabangDto = cabangDto;
	}	
	public boolean isVisibleRegional() {
		return visibleRegional;
	}
	public void setVisibleRegional(boolean visibleRegional) {
		this.visibleRegional = visibleRegional;
	}
	public boolean isVisibleCabang() {
		return visibleCabang;
	}
	public void setVisibleCabang(boolean visibleCabang) {
		this.visibleCabang = visibleCabang;
	}
	public boolean isVisibleKolektor() {
		return visibleKolektor;
	}
	public void setVisibleKolektor(boolean visibleKolektor) {
		this.visibleKolektor = visibleKolektor;
	}
	
	@Init
	public void load() {
		tglAwal = new Date();
		tglAkhir = new Date();
		if (Sessions.getCurrent().getAttribute("sessionMobileTracking") != null) {
			SessionForMobileTrackingDto sessin = (SessionForMobileTrackingDto) Sessions.getCurrent().getAttribute("sessionMobileTracking");
			if (sessin.getListRegionalDtoChoose().size() != 0) {
				listRegionalDtoChoose = sessin.getListRegionalDtoChoose();
				stringArrayRegional = sessin.getStrArrayRegional();
			}
			if (sessin.getListCabangMstDtoChoose().size() != 0) {
				listCabangMstDtoChoose = sessin.getListCabangMstDtoChoose();
				stringArrayCabang = sessin.getStrArrayCabang();
			}
			if (sessin.getListUserKolektorChoose().size() != 0) {
				listUserKolektorChoose = sessin.getListUserKolektorChoose();
				stringArrayKolektor = sessin.getStrArrayKolektor();
			}
			if (sessin.getDateAkhir() != null) {
				tglAkhir = sessin.getDateAkhir();
				tglAwal = sessin.getDateAwal();
			}
			List<AksesRegionCabangDto> lr = aksesRegionCabangSvc.findRegionByUserId(upSession.getUserId(), "");
			List<AksesRegionCabangDto> lc = aksesRegionCabangSvc.fintCabangByUserId(upSession.getUserId(), "");
			if (lr.size() != 0) {
				for (int i = 0; i < lr.size(); i++) {
					MasterRegionalDto mr = new MasterRegionalDto();
					mr.setKodeRegion(lr.get(i).getKodeRegion());
					mr.setNamaRegion(lr.get(i).getNamaRegional());
					listAksesRegional.add(mr);
				}
			}
			if (lc.size() != 0) {
				for (int i = 0; i < lc.size(); i++) {
					CabangMstDto cb= new CabangMstDto();
					cb.setKodeCabang(lc.get(i).getKodeCabang());
					cb.setNamaCabang(lc.get(i).getNamaCabang());
					listAksesCabang.add(cb);
				}
			}
			tglAwal = sessin.getDateAwal();
			tglAkhir = sessin.getDateAkhir();
			search();
		}
		upSession = (UserProfileDto) Sessions.getCurrent().getAttribute("user");
		
	}
	
	@Command("showRegional")
	@NotifyChange({"visibleRegional","listRegionalDto","listRegionalDtoChoose","keySearchregional"})
	public void showRegional() {
		keySearchregional = "";
		listRegionalDto = new ArrayList<MasterRegionalDto>();
		List<AksesRegionCabangDto> listTempAkses = new ArrayList<AksesRegionCabangDto>();
		listTempAkses = aksesRegionCabangSvc.findRegionByUserId(upSession.getUserId(),keySearchregional);
		if (listTempAkses.size() != 0) {
			for (int i = 0; i < listTempAkses.size(); i++) {
				MasterRegionalDto mr = new MasterRegionalDto();
				mr.setKodeRegion(listTempAkses.get(i).getKodeRegion());
				mr.setNamaRegion(listTempAkses.get(i).getNamaRegional());
				mr.setItemList(true);
				listRegionalDto.add(mr);
			}
		}
		if (listRegionalDtoChoose.size() != 0 ) {
			List<MasterRegionalDto> listTemp = new ArrayList<MasterRegionalDto>();
			for (int i = 0; i < listRegionalDto.size(); i++) {
				boolean sama = false;
				for (int j = 0; j < listRegionalDtoChoose.size(); j++) {
					if (listRegionalDto.get(i).getKodeRegion().equals(listRegionalDtoChoose.get(j).getKodeRegion())) {
						sama = true;
					}
				}
				if (sama == true) {
					MasterRegionalDto temppDto = new MasterRegionalDto();
					temppDto.setKodeRegion(listRegionalDto.get(i).getKodeRegion());
					temppDto.setNamaRegion(listRegionalDto.get(i).getNamaRegion());
					temppDto.setChoose(true);
					temppDto.setItemList(true);
					listTemp.add(temppDto);
				}else {
					MasterRegionalDto temppDto = new MasterRegionalDto();
					temppDto.setKodeRegion(listRegionalDto.get(i).getKodeRegion());
					temppDto.setNamaRegion(listRegionalDto.get(i).getNamaRegion());
					temppDto.setChoose(false);
					temppDto.setItemList(true);
					listTemp.add(temppDto);
				}
			}
			listRegionalDto = listTemp;
		}
		MasterRegionalDto mre = new MasterRegionalDto();
		mre.setKodeRegion("");
		mre.setNamaRegion("");
		mre.setItemList(false);
		listRegionalDto.add(mre);
		setVisibleRegional(true);
	}
	
	@Command("showRegionalSearch")
	@NotifyChange({"visibleRegional","listRegionalDto","listRegionalDtoChoose"})
	public void showRegionalSearch() {
		listRegionalDto = new ArrayList<MasterRegionalDto>();
		List<AksesRegionCabangDto> listTempAkses = new ArrayList<AksesRegionCabangDto>();
		listTempAkses = aksesRegionCabangSvc.findRegionByUserId(upSession.getUserId(),keySearchregional);
		if (listTempAkses.size() != 0) {
			for (int i = 0; i < listTempAkses.size(); i++) {
				MasterRegionalDto mr = new MasterRegionalDto();
				mr.setKodeRegion(listTempAkses.get(i).getKodeRegion());
				mr.setNamaRegion(listTempAkses.get(i).getNamaRegional());
				mr.setItemList(true);
				listRegionalDto.add(mr);
			}
		}
		if (listRegionalDtoChoose.size() != 0 ) {
			List<MasterRegionalDto> listTemp = new ArrayList<MasterRegionalDto>();
			for (int i = 0; i < listRegionalDto.size(); i++) {
				boolean sama = false;
				for (int j = 0; j < listRegionalDtoChoose.size(); j++) {
					if (listRegionalDto.get(i).getKodeRegion().equals(listRegionalDtoChoose.get(j).getKodeRegion())) {
						sama = true;
					}
				}
				if (sama == true) {
					MasterRegionalDto temppDto = new MasterRegionalDto();
					temppDto.setKodeRegion(listRegionalDto.get(i).getKodeRegion());
					temppDto.setNamaRegion(listRegionalDto.get(i).getNamaRegion());
					temppDto.setChoose(true);
					temppDto.setItemList(true);
					listTemp.add(temppDto);
				}else {
					MasterRegionalDto temppDto = new MasterRegionalDto();
					temppDto.setKodeRegion(listRegionalDto.get(i).getKodeRegion());
					temppDto.setNamaRegion(listRegionalDto.get(i).getNamaRegion());
					temppDto.setChoose(false);
					temppDto.setItemList(true);
					listTemp.add(temppDto);
				}
			}
			listRegionalDto = listTemp;
		}
		MasterRegionalDto mre = new MasterRegionalDto();
		mre.setKodeRegion("");
		mre.setNamaRegion("");
		mre.setItemList(false);
		listRegionalDto.add(mre);
		setVisibleRegional(true);
	}
	
	@Command("chooseRegional")
	@NotifyChange({"listRegionalDtoChoose","visibleRegional","stringArrayKolektor","stringArrayCabang","stringArrayRegional","listCabangMstDtoChoose","listUserKolektorChoose"})
	public void chooseRegional() {
		if (listRegionalDtoChoose.size() != 0) {
			stringArrayRegional = "";
			for (int i = 0; i < listRegionalDtoChoose.size(); i++) {
				if (i == 0) {
					stringArrayRegional = stringArrayRegional+listRegionalDtoChoose.get(i).getNamaRegion();
				}else {
					stringArrayRegional = stringArrayRegional+", "+listRegionalDtoChoose.get(i).getNamaRegion();
				}
			}
		}else {
			listRegionalDtoChoose = new ArrayList<MasterRegionalDto>();
			stringArrayRegional = "";
		}
		stringArrayKolektor = "";
		stringArrayCabang = "";
		listCabangMstDtoChoose = new ArrayList<CabangMstDto>();
		listUserKolektorChoose = new ArrayList<UserProfileDto>();
		setVisibleRegional(false);
	}
	@Command("closeRegional")
	@NotifyChange("visibleRegional")
	public void closeRegional() {
		setVisibleRegional(false);
	}
	@Command("showCabang")
	@NotifyChange({"keySearchCabang","visibleCabang","listCabangMstDto","listCabangMstDto","listCabangMstDtoChoose"})
	public void showCabang() {
		keySearchCabang = "";
		if (listRegionalDtoChoose.size() == 0) {
			List<CabangMstDto> tempList = new ArrayList<CabangMstDto>();
			listCabangMstDto = new ArrayList<CabangMstDto>();
			if (listAksesRegional.size() != 0) {
				for (int i = 0; i < listAksesRegional.size(); i++) {
					List<CabangMstDto> listTemp = new ArrayList<CabangMstDto>();
					List<AksesRegionCabangDto> listTempAkses = new ArrayList<AksesRegionCabangDto>();
					listTempAkses = aksesRegionCabangSvc.fintCabangByUserIdAndKodeRegion(upSession.getUserId(), listAksesRegional.get(i).getKodeRegion(), keySearchCabang);
					if (listTempAkses.size() != 0) {
						for (int ki = 0; ki < listTempAkses.size(); ki++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTempAkses.get(ki).getKodeCabang());
							cb.setNamaCabang(listTempAkses.get(ki).getNamaCabang());
							listTemp.add(cb);
						}
					}
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTemp.get(j).getKodeCabang());
							cb.setNamaCabang(listTemp.get(j).getNamaCabang());
							cb.setItemList(true);
							listCabangMstDto.add(cb);
						}
					}
				}
			}
			if (listCabangMstDtoChoose.size() != 0) {
				for (int i = 0; i < listCabangMstDto.size(); i++) {	
						CabangMstDto cba = new CabangMstDto();
						boolean cheked = false;
						for (int j = 0; j < listCabangMstDtoChoose.size(); j++) {
							if (listCabangMstDto.get(i).getKodeCabang().equals(listCabangMstDtoChoose.get(j).getKodeCabang())) {
								cheked = true;
							}
						}
						if (cheked == true) {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(true);
						}else {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(false);
						}
						tempList.add(cba);
				}
				listCabangMstDto = new ArrayList<CabangMstDto>();
				listCabangMstDto = tempList;
			}
			if (listCabangMstDto.size() != 0) {
				CabangMstDto cb = new CabangMstDto();
				cb.setKodeCabang("");
				cb.setNamaCabang("");
				cb.setItemList(false);
				listCabangMstDto.add(cb);
				setVisibleCabang(true);
			}else {
				Messagebox.show("Tidak ada data cabang yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}else {
			List<CabangMstDto> tempList = new ArrayList<CabangMstDto>();
			listCabangMstDto = new ArrayList<CabangMstDto>();
			if (listRegionalDtoChoose.size() != 0) {
				for (int i = 0; i < listRegionalDtoChoose.size(); i++) {
					List<CabangMstDto> listTemp = new ArrayList<CabangMstDto>();
					List<AksesRegionCabangDto> listTempAkses = new ArrayList<AksesRegionCabangDto>();
					listTempAkses = aksesRegionCabangSvc.fintCabangByUserIdAndKodeRegion(upSession.getUserId(), listRegionalDtoChoose.get(i).getKodeRegion(), keySearchCabang);
					if (listTempAkses.size() != 0) {
						for (int ki = 0; ki < listTempAkses.size(); ki++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTempAkses.get(ki).getKodeCabang());
							cb.setNamaCabang(listTempAkses.get(ki).getNamaCabang());
							listTemp.add(cb);
						}
					}
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTemp.get(j).getKodeCabang());
							cb.setNamaCabang(listTemp.get(j).getNamaCabang());
							cb.setItemList(true);
							listCabangMstDto.add(cb);
						}
					}
				}
			}
			if (listCabangMstDtoChoose.size() != 0) {
				for (int i = 0; i < listCabangMstDto.size(); i++) {	
						CabangMstDto cba = new CabangMstDto();
						boolean cheked = false;
						for (int j = 0; j < listCabangMstDtoChoose.size(); j++) {
							if (listCabangMstDto.get(i).getKodeCabang().equals(listCabangMstDtoChoose.get(j).getKodeCabang())) {
								cheked = true;
							}
						}
						if (cheked == true) {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(true);
						}else {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(false);
						}
						tempList.add(cba);
				}
				listCabangMstDto = new ArrayList<CabangMstDto>();
				listCabangMstDto = tempList;
			}
			if (listCabangMstDto.size() != 0) {
				CabangMstDto cb = new CabangMstDto();
				cb.setKodeCabang("");
				cb.setNamaCabang("");
				cb.setItemList(false);
				listCabangMstDto.add(cb);
				setVisibleCabang(true);
			}else {
				Messagebox.show("Tidak ada data cabang yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}
	}
	
	@Command("showCabangSearch")
	@NotifyChange({"visibleCabang","listCabangMstDto","listCabangMstDto","listCabangMstDtoChoose"})
	public void showCabangSeaarch() {
		if (listRegionalDtoChoose.size() == 0) {
			List<CabangMstDto> tempList = new ArrayList<CabangMstDto>();
			listCabangMstDto = new ArrayList<CabangMstDto>();
			if (listAksesRegional.size() != 0) {
				for (int i = 0; i < listAksesRegional.size(); i++) {
					List<CabangMstDto> listTemp = new ArrayList<CabangMstDto>();
					List<AksesRegionCabangDto> listTempAkses = new ArrayList<AksesRegionCabangDto>();
					listTempAkses = aksesRegionCabangSvc.fintCabangByUserIdAndKodeRegion(upSession.getUserId(), listAksesRegional.get(i).getKodeRegion(), keySearchCabang);
					if (listTempAkses.size() != 0) {
						for (int ki = 0; ki < listTempAkses.size(); ki++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTempAkses.get(ki).getKodeCabang());
							cb.setNamaCabang(listTempAkses.get(ki).getNamaCabang());
							listTemp.add(cb);
						}
					}
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTemp.get(j).getKodeCabang());
							cb.setNamaCabang(listTemp.get(j).getNamaCabang());
							cb.setItemList(true);
							listCabangMstDto.add(cb);
						}
					}
				}
			}
			if (listCabangMstDtoChoose.size() != 0) {
				for (int i = 0; i < listCabangMstDto.size(); i++) {	
						CabangMstDto cba = new CabangMstDto();
						boolean cheked = false;
						for (int j = 0; j < listCabangMstDtoChoose.size(); j++) {
							if (listCabangMstDto.get(i).getKodeCabang().equals(listCabangMstDtoChoose.get(j).getKodeCabang())) {
								cheked = true;
							}
						}
						if (cheked == true) {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(true);
						}else {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(false);
						}
						tempList.add(cba);
				}
				listCabangMstDto = new ArrayList<CabangMstDto>();
				listCabangMstDto = tempList;
			}
			if (listCabangMstDto.size() != 0) {
				CabangMstDto cb = new CabangMstDto();
				cb.setKodeCabang("");
				cb.setNamaCabang("");
				cb.setItemList(false);
				listCabangMstDto.add(cb);
				setVisibleCabang(true);
			}else {
				Messagebox.show("Tidak ada data cabang yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}else {
			List<CabangMstDto> tempList = new ArrayList<CabangMstDto>();
			listCabangMstDto = new ArrayList<CabangMstDto>();
			if (listRegionalDtoChoose.size() != 0) {
				for (int i = 0; i < listRegionalDtoChoose.size(); i++) {
					List<CabangMstDto> listTemp = new ArrayList<CabangMstDto>();
					List<AksesRegionCabangDto> listTempAkses = new ArrayList<AksesRegionCabangDto>();
					listTempAkses = aksesRegionCabangSvc.fintCabangByUserIdAndKodeRegion(upSession.getUserId(), listRegionalDtoChoose.get(i).getKodeRegion(), keySearchCabang);
					if (listTempAkses.size() != 0) {
						for (int ki = 0; ki < listTempAkses.size(); ki++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTempAkses.get(ki).getKodeCabang());
							cb.setNamaCabang(listTempAkses.get(ki).getNamaCabang());
							listTemp.add(cb);
						}
					}
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTemp.get(j).getKodeCabang());
							cb.setNamaCabang(listTemp.get(j).getNamaCabang());
							cb.setItemList(true);
							listCabangMstDto.add(cb);
						}
					}
				}
			}
			if (listCabangMstDtoChoose.size() != 0) {
				for (int i = 0; i < listCabangMstDto.size(); i++) {	
						CabangMstDto cba = new CabangMstDto();
						boolean cheked = false;
						for (int j = 0; j < listCabangMstDtoChoose.size(); j++) {
							if (listCabangMstDto.get(i).getKodeCabang().equals(listCabangMstDtoChoose.get(j).getKodeCabang())) {
								cheked = true;
							}
						}
						if (cheked == true) {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(true);
						}else {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(false);
						}
						tempList.add(cba);
				}
				listCabangMstDto = new ArrayList<CabangMstDto>();
				listCabangMstDto = tempList;
			}
			if (listCabangMstDto.size() != 0) {
				CabangMstDto cb = new CabangMstDto();
				cb.setKodeCabang("");
				cb.setNamaCabang("");
				cb.setItemList(false);
				listCabangMstDto.add(cb);
				setVisibleCabang(true);
			}else {
				Messagebox.show("Tidak ada data cabang yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}
	}
	
	@Command("chooseCabang")
	@NotifyChange({"listCabangMstDtoChoose","visibleCabang","userKolektor","listUserKolektorChoose","stringArrayKolektor","stringArrayCabang"})
	public void chooseCabang() {
		if (listCabangMstDtoChoose.size() != 0) {
			stringArrayCabang = "";
			for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
				if (i == 0) {
					stringArrayCabang = stringArrayCabang+listCabangMstDtoChoose.get(i).getNamaCabang();
				}else {
					stringArrayCabang = stringArrayCabang+", "+listCabangMstDtoChoose.get(i).getNamaCabang();
				}
			}
		}else {
			stringArrayCabang ="";
			listCabangMstDtoChoose = new ArrayList<CabangMstDto>();
		}
		stringArrayKolektor = "";
		listUserKolektorChoose = new ArrayList<UserProfileDto>();
		setVisibleCabang(false);
		
	}
	
	@Command("closeCabang")
	@NotifyChange("visibleCabang")
	public void closeCabang() {
		setVisibleCabang(false);
	}
	
	@Command("showKolektor")
	@NotifyChange({"keySearchKolektor","visibleKolektor","listUserKolektor"})
	public void showKolektor() {
		keySearchKolektor = "";
		if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() == 0) { 
			listUserKolektor = new ArrayList<UserProfileDto>();
			for (int i = 0; i < listAksesCabang.size(); i++) {
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeCabangAndSearch(listAksesCabang.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserKolektor.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserKolektor.size(); k++) {
								if (listUserKolektor.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserKolektor.add(kol);
							}
						}else {
							listUserKolektor.add(kol);
						}
					}
				}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserKolektor.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserKolektor.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserKolektor = listTemp;
				
			}
			if (listUserKolektor.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserKolektor.add(up);
				setVisibleKolektor(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}else if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() != 0) {
			listUserKolektor = new ArrayList<UserProfileDto>();
			for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeCabangAndSearch(listCabangMstDtoChoose.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserKolektor.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserKolektor.size(); k++) {
								if (listUserKolektor.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserKolektor.add(kol);
							}
						}else {
							listUserKolektor.add(kol);
						}
					}
				}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserKolektor.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserKolektor.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserKolektor = listTemp;
				
			}
			if (listUserKolektor.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserKolektor.add(up);
				setVisibleKolektor(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
			
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() != 0) {
			listUserKolektor = new ArrayList<UserProfileDto>();
			for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeCabangAndSearch(listCabangMstDtoChoose.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserKolektor.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserKolektor.size(); k++) {
								if (listUserKolektor.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserKolektor.add(kol);
							}
						}else {
							listUserKolektor.add(kol);
						}
					}
				}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserKolektor.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserKolektor.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserKolektor = listTemp;
				
			}
			if (listUserKolektor.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserKolektor.add(up);
				setVisibleKolektor(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
			
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() == 0) {
			System.out.println("Masuk sini ke reg doang");
			listUserKolektor = new ArrayList<UserProfileDto>();
			for (int h = 0; h < listRegionalDtoChoose.size(); h++) {
			for (int i = 0; i < listAksesCabang.size(); i++) {
				System.out.println("masuk loop reg ke = "+h);
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeRegionalKodeCabangAndSearch(listRegionalDtoChoose.get(h).getKodeRegion(),listAksesCabang.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserKolektor.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserKolektor.size(); k++) {
								if (listUserKolektor.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserKolektor.add(kol);
							}
						}else {
							listUserKolektor.add(kol);
						}
					}
				}
			}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserKolektor.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserKolektor.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserKolektor = listTemp;
				
			}
			if (listUserKolektor.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserKolektor.add(up);
				setVisibleKolektor(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}
	}
	
	@Command("showKolektorSearch")
	@NotifyChange({"visibleKolektor","listUserKolektor"})
	public void showKolektorSearch() {
		if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() == 0) { 
			listUserKolektor = new ArrayList<UserProfileDto>();
			for (int i = 0; i < listAksesCabang.size(); i++) {
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeCabangAndSearch(listAksesCabang.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserKolektor.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserKolektor.size(); k++) {
								if (listUserKolektor.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserKolektor.add(kol);
							}
						}else {
							listUserKolektor.add(kol);
						}
					}
				}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserKolektor.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserKolektor.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserKolektor = listTemp;
				
			}
			if (listUserKolektor.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserKolektor.add(up);
				setVisibleKolektor(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}else if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() != 0) {
			listUserKolektor = new ArrayList<UserProfileDto>();
			for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeCabangAndSearch(listCabangMstDtoChoose.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserKolektor.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserKolektor.size(); k++) {
								if (listUserKolektor.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserKolektor.add(kol);
							}
						}else {
							listUserKolektor.add(kol);
						}
					}
				}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserKolektor.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserKolektor.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserKolektor = listTemp;
				
			}
			if (listUserKolektor.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserKolektor.add(up);
				setVisibleKolektor(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
			
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() != 0) {
			listUserKolektor = new ArrayList<UserProfileDto>();
			for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeCabangAndSearch(listCabangMstDtoChoose.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserKolektor.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserKolektor.size(); k++) {
								if (listUserKolektor.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserKolektor.add(kol);
							}
						}else {
							listUserKolektor.add(kol);
						}
					}
				}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserKolektor.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserKolektor.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserKolektor = listTemp;
				
			}
			if (listUserKolektor.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserKolektor.add(up);
				setVisibleKolektor(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
			
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() == 0) {
			System.out.println("Masuk sini ke reg doang");
			listUserKolektor = new ArrayList<UserProfileDto>();
			for (int h = 0; h < listRegionalDtoChoose.size(); h++) {
			for (int i = 0; i < listAksesCabang.size(); i++) {
				System.out.println("masuk loop reg ke = "+h);
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeRegionalKodeCabangAndSearch(listRegionalDtoChoose.get(h).getKodeRegion(),listAksesCabang.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserKolektor.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserKolektor.size(); k++) {
								if (listUserKolektor.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserKolektor.add(kol);
							}
						}else {
							listUserKolektor.add(kol);
						}
					}
				}
			}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserKolektor.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserKolektor.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserKolektor = listTemp;
				
			}
			if (listUserKolektor.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserKolektor.add(up);
				setVisibleKolektor(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}
	}
	
	@Command("chooseKolektor")
	@NotifyChange({"listUserKolektorChoose","visibleKolektor","stringArrayKolektor"})
	public void chooseKolektor() {
		if (listUserKolektorChoose.size() != 0) {
			stringArrayKolektor = "";
			for (int i = 0; i < listUserKolektorChoose.size(); i++) {
				if (i == 0  ) {
					stringArrayKolektor = stringArrayKolektor+listUserKolektorChoose.get(i).getNamaKaryawan();
				}else {
					stringArrayKolektor = stringArrayKolektor+", "+listUserKolektorChoose.get(i).getNamaKaryawan();
				}
			}
		}else {
			listUserKolektorChoose = new ArrayList<UserProfileDto>();
			stringArrayKolektor = "";
		}
		setVisibleKolektor(false);
	}
	
	@Command("closeKolektor")
	@NotifyChange("visibleKolektor")
	public void closeKolektor() {
		setVisibleKolektor(false);
	}
	
	@Command("searchKolektor")
	@NotifyChange("visibleKolektor")
	public void searchKolektor() {
		setVisibleKolektor(false);
	}
	
	@Command("searchRegional")
	@NotifyChange("visibleKolektor")
	public void searchRegional() {
		setVisibleKolektor(false);
	}
	
	@Command("searchCabang")
	@NotifyChange("visibleKolektor")
	public void searchCabang() {
		setVisibleKolektor(false);
	}
	
	@Command("page")
	@NotifyChange("page")
	public void page() {
		setPage(getPage());
	}
	@Command("pageRegional")
	@NotifyChange("pageRegional")
	public void pageRegional() {
		setPageRegional(getPageRegional());
	}
	@Command("pageCabang")
	@NotifyChange("pageCabang")
	public void pageCabang() {
		setPageCabang(getPageCabang());
	}
	@Command("pageKolektor")
	@NotifyChange("pageRegional")
	public void pageKolektor() {
		setPageKolektor(getPageKolektor());
	}
	
	@Command("search")
	@NotifyChange("listTracking")
	public void search() {
		if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() == 0 && listUserKolektorChoose.size() == 0) {
			listTracking = new ArrayList<MobileTrackingDto>();
			for (int h = 0; h < listRegionalDtoChoose.size(); h++) {
				for (int i = 0; i < listAksesCabang.size(); i++) {
					List<MobileTrackingDto> listTemp = new ArrayList<MobileTrackingDto>();
					listTemp = mobileTrackingSvc.findMobileTrackingByKodeRegionKodeCabang(listRegionalDtoChoose.get(h).getKodeRegion(),listAksesCabang.get(i).getKodeCabang(), tglAwal, tglAkhir, keySearch);
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							MobileTrackingDto mt = new MobileTrackingDto();
							mt.setUserId(listTemp.get(j).getUserId());
							mt.setNamaKaryawan(listTemp.get(j).getNamaKaryawan());
							mt.setNamaRegion(listTemp.get(j).getNamaRegion());
							mt.setNamaCabang(listTemp.get(j).getNamaCabang());
							mt.setKetTrackingDate(listTemp.get(j).getKetTrackingDate());
							mt.setLocation(listTemp.get(j).getLocation());
							mt.setOrderId(listTemp.get(j).getOrderId());
							listTracking.add(mt);
						}
					}
				}
			}
			SessionForMobileTrackingDto temSes = new SessionForMobileTrackingDto();
			if (listRegionalDtoChoose.size() != 0) {
				temSes.setListRegionalDtoChoose(listRegionalDtoChoose);
				temSes.setStrArrayRegional(stringArrayRegional);
			}
			if (listCabangMstDtoChoose.size() != 0) {
				temSes.setListCabangMstDtoChoose(listCabangMstDtoChoose);
				temSes.setStrArrayCabang(stringArrayCabang);
			}
			if (listUserKolektorChoose.size() != 0) {
				temSes.setListUserKolektorChoose(listUserKolektorChoose);
				temSes.setStrArrayKolektor(stringArrayKolektor);
			}
			temSes.setDateAwal(tglAwal);
			temSes.setDateAkhir(tglAkhir);
			Sessions.getCurrent().setAttribute("sessionMobileTracking",temSes);
		}else if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() != 0 && listUserKolektorChoose.size() == 0) {
			listTracking = new ArrayList<MobileTrackingDto>();
			for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
				List<MobileTrackingDto> listTemp = new ArrayList<MobileTrackingDto>();
				listTemp = mobileTrackingSvc.findMobileTrackingAllByCabang(listCabangMstDtoChoose.get(i).getKodeCabang(), tglAwal, tglAkhir, keySearch);
				if (listTemp.size() != 0) {
					for (int j = 0; j < listTemp.size(); j++) {
						MobileTrackingDto mt = new MobileTrackingDto();
						mt.setUserId(listTemp.get(j).getUserId());
						mt.setNamaKaryawan(listTemp.get(j).getNamaKaryawan());
						mt.setNamaRegion(listTemp.get(j).getNamaRegion());
						mt.setNamaCabang(listTemp.get(j).getNamaCabang());
						mt.setKetTrackingDate(listTemp.get(j).getKetTrackingDate());
						mt.setLocation(listTemp.get(j).getLocation());
						mt.setOrderId(listTemp.get(j).getOrderId());
						listTracking.add(mt);
					}
				}
			}
			SessionForMobileTrackingDto temSes = new SessionForMobileTrackingDto();
			if (listRegionalDtoChoose.size() != 0) {
				temSes.setListRegionalDtoChoose(listRegionalDtoChoose);
				temSes.setStrArrayRegional(stringArrayRegional);
			}
			if (listCabangMstDtoChoose.size() != 0) {
				temSes.setListCabangMstDtoChoose(listCabangMstDtoChoose);
				temSes.setStrArrayCabang(stringArrayCabang);
			}
			if (listUserKolektorChoose.size() != 0) {
				temSes.setListUserKolektorChoose(listUserKolektorChoose);
				temSes.setStrArrayKolektor(stringArrayKolektor);
			}
			temSes.setDateAwal(tglAwal);
			temSes.setDateAkhir(tglAkhir);
			Sessions.getCurrent().setAttribute("sessionMobileTracking",temSes);
		}else if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() == 0 && listUserKolektorChoose.size() != 0) {
			listTracking = new ArrayList<MobileTrackingDto>();
			for (int h = 0; h < listAksesCabang.size(); h++) {
				for (int i = 0; i < listUserKolektorChoose.size(); i++) {
					List<MobileTrackingDto> listTemp = new ArrayList<MobileTrackingDto>();
					listTemp = mobileTrackingSvc.findMobileTrackingByKodeRegionUserId(listAksesCabang.get(h).getKodeCabang(),listUserKolektorChoose.get(i).getUserId(), tglAwal, tglAkhir, keySearch);
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							MobileTrackingDto mt = new MobileTrackingDto();
							mt.setUserId(listTemp.get(j).getUserId());
							mt.setNamaKaryawan(listTemp.get(j).getNamaKaryawan());
							mt.setNamaRegion(listTemp.get(j).getNamaRegion());
							mt.setNamaCabang(listTemp.get(j).getNamaCabang());
							mt.setKetTrackingDate(listTemp.get(j).getKetTrackingDate());
							mt.setLocation(listTemp.get(j).getLocation());
							mt.setOrderId(listTemp.get(j).getOrderId());
							listTracking.add(mt);
						}
					}
				}
			}
			SessionForMobileTrackingDto temSes = new SessionForMobileTrackingDto();
			if (listRegionalDtoChoose.size() != 0) {
				temSes.setListRegionalDtoChoose(listRegionalDtoChoose);
				temSes.setStrArrayRegional(stringArrayRegional);
			}
			if (listCabangMstDtoChoose.size() != 0) {
				temSes.setListCabangMstDtoChoose(listCabangMstDtoChoose);
				temSes.setStrArrayCabang(stringArrayCabang);
			}
			if (listUserKolektorChoose.size() != 0) {
				temSes.setListUserKolektorChoose(listUserKolektorChoose);
				temSes.setStrArrayKolektor(stringArrayKolektor);
			}
			temSes.setDateAwal(tglAwal);
			temSes.setDateAkhir(tglAkhir);
			Sessions.getCurrent().setAttribute("sessionMobileTracking",temSes);
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() != 0 && listUserKolektorChoose.size() == 0) {
			listTracking = new ArrayList<MobileTrackingDto>();
			for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
				List<MobileTrackingDto> listTemp = new ArrayList<MobileTrackingDto>();
				listTemp = mobileTrackingSvc.findMobileTrackingAllByCabang(listCabangMstDtoChoose.get(i).getKodeCabang(), tglAwal, tglAkhir, keySearch);
				if (listTemp.size() != 0) {
					for (int j = 0; j < listTemp.size(); j++) {
						MobileTrackingDto mt = new MobileTrackingDto();
						mt.setUserId(listTemp.get(j).getUserId());
						mt.setNamaKaryawan(listTemp.get(j).getNamaKaryawan());
						mt.setNamaRegion(listTemp.get(j).getNamaRegion());
						mt.setNamaCabang(listTemp.get(j).getNamaCabang());
						mt.setKetTrackingDate(listTemp.get(j).getKetTrackingDate());
						mt.setLocation(listTemp.get(j).getLocation());
						mt.setOrderId(listTemp.get(j).getOrderId());
						listTracking.add(mt);
					}
				}
			}
			SessionForMobileTrackingDto temSes = new SessionForMobileTrackingDto();
			if (listRegionalDtoChoose.size() != 0) {
				temSes.setListRegionalDtoChoose(listRegionalDtoChoose);
				temSes.setStrArrayRegional(stringArrayRegional);
			}
			if (listCabangMstDtoChoose.size() != 0) {
				temSes.setListCabangMstDtoChoose(listCabangMstDtoChoose);
				temSes.setStrArrayCabang(stringArrayCabang);
			}
			if (listUserKolektorChoose.size() != 0) {
				temSes.setListUserKolektorChoose(listUserKolektorChoose);
				temSes.setStrArrayKolektor(stringArrayKolektor);
			}
			temSes.setDateAwal(tglAwal);
			temSes.setDateAkhir(tglAkhir);
			Sessions.getCurrent().setAttribute("sessionMobileTracking",temSes);
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() == 0 && listUserKolektorChoose.size() != 0) {
			listTracking = new ArrayList<MobileTrackingDto>();
			for (int h = 0; h < listRegionalDtoChoose.size(); h++) {
				for (int i = 0; i < listUserKolektorChoose.size(); i++) {
					List<MobileTrackingDto> listTemp = new ArrayList<MobileTrackingDto>();
					listTemp = mobileTrackingSvc.findMobileTrackingByKodeRegionUserId(listRegionalDtoChoose.get(h).getKodeRegion(),listUserKolektorChoose.get(i).getUserId(), tglAwal, tglAkhir, keySearch);
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							MobileTrackingDto mt = new MobileTrackingDto();
							mt.setUserId(listTemp.get(j).getUserId());
							mt.setNamaKaryawan(listTemp.get(j).getNamaKaryawan());
							mt.setNamaRegion(listTemp.get(j).getNamaRegion());
							mt.setNamaCabang(listTemp.get(j).getNamaCabang());
							mt.setKetTrackingDate(listTemp.get(j).getKetTrackingDate());
							mt.setLocation(listTemp.get(j).getLocation());
							mt.setOrderId(listTemp.get(j).getOrderId());
							listTracking.add(mt);
						}
					}
				}
			}
			SessionForMobileTrackingDto temSes = new SessionForMobileTrackingDto();
			if (listRegionalDtoChoose.size() != 0) {
				temSes.setListRegionalDtoChoose(listRegionalDtoChoose);
				temSes.setStrArrayRegional(stringArrayRegional);
			}
			if (listCabangMstDtoChoose.size() != 0) {
				temSes.setListCabangMstDtoChoose(listCabangMstDtoChoose);
				temSes.setStrArrayCabang(stringArrayCabang);
			}
			if (listUserKolektorChoose.size() != 0) {
				temSes.setListUserKolektorChoose(listUserKolektorChoose);
				temSes.setStrArrayKolektor(stringArrayKolektor);
			}
			temSes.setDateAwal(tglAwal);
			temSes.setDateAkhir(tglAkhir);
			Sessions.getCurrent().setAttribute("sessionMobileTracking",temSes);
		}else if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() != 0 && listUserKolektorChoose.size() != 0) {
			listTracking = new ArrayList<MobileTrackingDto>();
			for (int h = 0; h < listCabangMstDtoChoose.size(); h++) {
				for (int i = 0; i < listUserKolektorChoose.size(); i++) {
					List<MobileTrackingDto> listTemp = new ArrayList<MobileTrackingDto>();
					listTemp = mobileTrackingSvc.findMobileTrackingByKodeCabangUserId(listCabangMstDtoChoose.get(h).getKodeCabang(),listUserKolektorChoose.get(i).getUserId(), tglAwal, tglAkhir, keySearch);
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							MobileTrackingDto mt = new MobileTrackingDto();
							mt.setUserId(listTemp.get(j).getUserId());
							mt.setNamaKaryawan(listTemp.get(j).getNamaKaryawan());
							mt.setNamaRegion(listTemp.get(j).getNamaRegion());
							mt.setNamaCabang(listTemp.get(j).getNamaCabang());
							mt.setKetTrackingDate(listTemp.get(j).getKetTrackingDate());
							mt.setLocation(listTemp.get(j).getLocation());
							mt.setOrderId(listTemp.get(j).getOrderId());
							listTracking.add(mt);
						}
					}
				}
			}
			SessionForMobileTrackingDto temSes = new SessionForMobileTrackingDto();
			if (listRegionalDtoChoose.size() != 0) {
				temSes.setListRegionalDtoChoose(listRegionalDtoChoose);
				temSes.setStrArrayRegional(stringArrayRegional);
			}
			if (listCabangMstDtoChoose.size() != 0) {
				temSes.setListCabangMstDtoChoose(listCabangMstDtoChoose);
				temSes.setStrArrayCabang(stringArrayCabang);
			}
			if (listUserKolektorChoose.size() != 0) {
				temSes.setListUserKolektorChoose(listUserKolektorChoose);
				temSes.setStrArrayKolektor(stringArrayKolektor);
			}
			temSes.setDateAwal(tglAwal);
			temSes.setDateAkhir(tglAkhir);
			Sessions.getCurrent().setAttribute("sessionMobileTracking",temSes);
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() != 0 && listUserKolektorChoose.size() != 0) {
			listTracking = new ArrayList<MobileTrackingDto>();
			for (int h = 0; h < listCabangMstDtoChoose.size(); h++) {
				for (int i = 0; i < listUserKolektorChoose.size(); i++) {
					List<MobileTrackingDto> listTemp = new ArrayList<MobileTrackingDto>();
					listTemp = mobileTrackingSvc.findMobileTrackingByKodeCabangUserId(listCabangMstDtoChoose.get(h).getKodeCabang(),listUserKolektorChoose.get(i).getUserId(), tglAwal, tglAkhir, keySearch);
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							MobileTrackingDto mt = new MobileTrackingDto();
							mt.setUserId(listTemp.get(j).getUserId());
							mt.setNamaKaryawan(listTemp.get(j).getNamaKaryawan());
							mt.setNamaRegion(listTemp.get(j).getNamaRegion());
							mt.setNamaCabang(listTemp.get(j).getNamaCabang());
							mt.setKetTrackingDate(listTemp.get(j).getKetTrackingDate());
							mt.setLocation(listTemp.get(j).getLocation());
							mt.setOrderId(listTemp.get(j).getOrderId());
							listTracking.add(mt);
						}
					}
				}
			}
			SessionForMobileTrackingDto temSes = new SessionForMobileTrackingDto();
			if (listRegionalDtoChoose.size() != 0) {
				temSes.setListRegionalDtoChoose(listRegionalDtoChoose);
				temSes.setStrArrayRegional(stringArrayRegional);
			}
			
			if (listCabangMstDtoChoose.size() != 0) {
				temSes.setListCabangMstDtoChoose(listCabangMstDtoChoose);
				temSes.setStrArrayCabang(stringArrayCabang);
			}
			if (listUserKolektorChoose.size() != 0) {
				temSes.setListUserKolektorChoose(listUserKolektorChoose);
				temSes.setStrArrayKolektor(stringArrayKolektor);
			}
			temSes.setDateAwal(tglAwal);
			temSes.setDateAkhir(tglAkhir);
			Sessions.getCurrent().setAttribute("sessionMobileTracking",temSes);
		}else if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() == 0 && listUserKolektorChoose.size() == 0) {
			Messagebox.show("Isi terlebih dahulu parameter yang ada !", "MESSAGE", null, null, null);
		}
		
	}
	
	@Command("showMapNotOrderId")
	@NotifyChange({"listTrackingAll","banyakListMap1","jsonMap1","gsonMap1"})
	public void showMapNotOrderId() {
		listTrackingAll = new ArrayList<MobileTrackingDto>();
		banyakListMap1 = 0;
		listTrackingAll = mobileTrackingSvc.findMobileTrackingByUserIdNotOrderId(trackingDto.getUserId(), tglAwal, tglAkhir);
		if (listTrackingAll.size() != 0) {
			banyakListMap1 = listTrackingAll.size();
			jsonMap1 = gsonMap1.toJson(listTrackingAll);
			Sessions.getCurrent().setAttribute("jsonMap1", jsonMap1);
			Sessions.getCurrent().setAttribute("banyakListMap1", banyakListMap1);
			Sessions.getCurrent().setAttribute("tglAwal", tglAwal);
			Sessions.getCurrent().setAttribute("tglAkhir", tglAkhir);
			Sessions.getCurrent().setAttribute("trackingDto", trackingDto);
			
			Include inc5 = (Include) Executions.getCurrent().getDesktop()
					.getPage("index").getFellow("mainInclude");
			inc5.setSrc("/master/mobileTracking/map1.zul");
			
		}else {
			Messagebox.show("Tidak dapat menampilkan map, data kosong !","MESSAGE", null, null, null);
		}
	}
	
	
	@Command("showMapWithOrderId")
	@NotifyChange({"listTrackingAll","banyakListMap2","jsonMap2","gsonMap2"})
	public void showMapWithOrderId() {
		listTrackingAll = new ArrayList<MobileTrackingDto>();
		banyakListMap1 = 0;
		listTrackingAll = mobileTrackingSvc.findMobileTrackingByUserIdWithOrderId(trackingDto.getUserId(), tglAwal, tglAkhir);
		if (listTrackingAll.size() != 0) {
			banyakListMap2 = listTrackingAll.size();
			jsonMap2 = gsonMap2.toJson(listTrackingAll);
			Sessions.getCurrent().setAttribute("jsonMap2", jsonMap2);
			Sessions.getCurrent().setAttribute("banyakListMap2", banyakListMap2);
			Sessions.getCurrent().setAttribute("tglAwal", tglAwal);
			Sessions.getCurrent().setAttribute("tglAkhir", tglAkhir);
			Sessions.getCurrent().setAttribute("trackingDto", trackingDto);
			
			Include inc5 = (Include) Executions.getCurrent().getDesktop()
					.getPage("index").getFellow("mainInclude");
			inc5.setSrc("/master/mobileTracking/map2.zul");
			
		}else {
			Messagebox.show("Tidak dapat menampilkan map, data kosong !","MESSAGE", null, null, null);
		}
	}
	
	@Command("resultSearch")
	@NotifyChange("listTracking")
	public void resultSearch() {
		if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() == 0 && listUserKolektorChoose.size() == 0) {
			listTracking = new ArrayList<MobileTrackingDto>();
			for (int h = 0; h < listRegionalDtoChoose.size(); h++) {
				for (int i = 0; i < listAksesCabang.size(); i++) {
					List<MobileTrackingDto> listTemp = new ArrayList<MobileTrackingDto>();
					listTemp = mobileTrackingSvc.findMobileTrackingByKodeRegionKodeCabang(listRegionalDtoChoose.get(h).getKodeRegion(),listAksesCabang.get(i).getKodeCabang(), tglAwal, tglAkhir, keySearch);
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							MobileTrackingDto mt = new MobileTrackingDto();
							mt.setUserId(listTemp.get(j).getUserId());
							mt.setNamaKaryawan(listTemp.get(j).getNamaKaryawan());
							mt.setNamaRegion(listTemp.get(j).getNamaRegion());
							mt.setNamaCabang(listTemp.get(j).getNamaCabang());
							mt.setKetTrackingDate(listTemp.get(j).getKetTrackingDate());
							mt.setLocation(listTemp.get(j).getLocation());
							mt.setOrderId(listTemp.get(j).getOrderId());
							listTracking.add(mt);
						}
					}
				}
			}
		}else if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() != 0 && listUserKolektorChoose.size() == 0) {
			listTracking = new ArrayList<MobileTrackingDto>();
			for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
				List<MobileTrackingDto> listTemp = new ArrayList<MobileTrackingDto>();
				listTemp = mobileTrackingSvc.findMobileTrackingAllByCabang(listCabangMstDtoChoose.get(i).getKodeCabang(), tglAwal, tglAkhir, keySearch);
				if (listTemp.size() != 0) {
					for (int j = 0; j < listTemp.size(); j++) {
						MobileTrackingDto mt = new MobileTrackingDto();
						mt.setUserId(listTemp.get(j).getUserId());
						mt.setNamaKaryawan(listTemp.get(j).getNamaKaryawan());
						mt.setNamaRegion(listTemp.get(j).getNamaRegion());
						mt.setNamaCabang(listTemp.get(j).getNamaCabang());
						mt.setKetTrackingDate(listTemp.get(j).getKetTrackingDate());
						mt.setLocation(listTemp.get(j).getLocation());
						mt.setOrderId(listTemp.get(j).getOrderId());
						listTracking.add(mt);
					}
				}
			}
		}else if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() == 0 && listUserKolektorChoose.size() != 0) {
			listTracking = new ArrayList<MobileTrackingDto>();
			for (int h = 0; h < listAksesCabang.size(); h++) {
				for (int i = 0; i < listUserKolektorChoose.size(); i++) {
					List<MobileTrackingDto> listTemp = new ArrayList<MobileTrackingDto>();
					listTemp = mobileTrackingSvc.findMobileTrackingByKodeRegionUserId(listAksesCabang.get(h).getKodeCabang(),listUserKolektorChoose.get(i).getUserId(), tglAwal, tglAkhir, keySearch);
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							MobileTrackingDto mt = new MobileTrackingDto();
							mt.setUserId(listTemp.get(j).getUserId());
							mt.setNamaKaryawan(listTemp.get(j).getNamaKaryawan());
							mt.setNamaRegion(listTemp.get(j).getNamaRegion());
							mt.setNamaCabang(listTemp.get(j).getNamaCabang());
							mt.setKetTrackingDate(listTemp.get(j).getKetTrackingDate());
							mt.setLocation(listTemp.get(j).getLocation());
							mt.setOrderId(listTemp.get(j).getOrderId());
							listTracking.add(mt);
						}
					}
				}
			}
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() != 0 && listUserKolektorChoose.size() == 0) {
			listTracking = new ArrayList<MobileTrackingDto>();
			for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
				List<MobileTrackingDto> listTemp = new ArrayList<MobileTrackingDto>();
				listTemp = mobileTrackingSvc.findMobileTrackingAllByCabang(listCabangMstDtoChoose.get(i).getKodeCabang(), tglAwal, tglAkhir, keySearch);
				if (listTemp.size() != 0) {
					for (int j = 0; j < listTemp.size(); j++) {
						MobileTrackingDto mt = new MobileTrackingDto();
						mt.setUserId(listTemp.get(j).getUserId());
						mt.setNamaKaryawan(listTemp.get(j).getNamaKaryawan());
						mt.setNamaRegion(listTemp.get(j).getNamaRegion());
						mt.setNamaCabang(listTemp.get(j).getNamaCabang());
						mt.setKetTrackingDate(listTemp.get(j).getKetTrackingDate());
						mt.setLocation(listTemp.get(j).getLocation());
						mt.setOrderId(listTemp.get(j).getOrderId());
						listTracking.add(mt);
					}
				}
			}
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() == 0 && listUserKolektorChoose.size() != 0) {
			listTracking = new ArrayList<MobileTrackingDto>();
			for (int h = 0; h < listRegionalDtoChoose.size(); h++) {
				for (int i = 0; i < listUserKolektorChoose.size(); i++) {
					List<MobileTrackingDto> listTemp = new ArrayList<MobileTrackingDto>();
					listTemp = mobileTrackingSvc.findMobileTrackingByKodeRegionUserId(listRegionalDtoChoose.get(h).getKodeRegion(),listUserKolektorChoose.get(i).getUserId(), tglAwal, tglAkhir, keySearch);
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							MobileTrackingDto mt = new MobileTrackingDto();
							mt.setUserId(listTemp.get(j).getUserId());
							mt.setNamaKaryawan(listTemp.get(j).getNamaKaryawan());
							mt.setNamaRegion(listTemp.get(j).getNamaRegion());
							mt.setNamaCabang(listTemp.get(j).getNamaCabang());
							mt.setKetTrackingDate(listTemp.get(j).getKetTrackingDate());
							mt.setLocation(listTemp.get(j).getLocation());
							mt.setOrderId(listTemp.get(j).getOrderId());
							listTracking.add(mt);
						}
					}
				}
			}
		}else if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() != 0 && listUserKolektorChoose.size() != 0) {
			listTracking = new ArrayList<MobileTrackingDto>();
			for (int h = 0; h < listCabangMstDtoChoose.size(); h++) {
				for (int i = 0; i < listUserKolektorChoose.size(); i++) {
					List<MobileTrackingDto> listTemp = new ArrayList<MobileTrackingDto>();
					listTemp = mobileTrackingSvc.findMobileTrackingByKodeCabangUserId(listCabangMstDtoChoose.get(h).getKodeCabang(),listUserKolektorChoose.get(i).getUserId(), tglAwal, tglAkhir, keySearch);
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							MobileTrackingDto mt = new MobileTrackingDto();
							mt.setUserId(listTemp.get(j).getUserId());
							mt.setNamaKaryawan(listTemp.get(j).getNamaKaryawan());
							mt.setNamaRegion(listTemp.get(j).getNamaRegion());
							mt.setNamaCabang(listTemp.get(j).getNamaCabang());
							mt.setKetTrackingDate(listTemp.get(j).getKetTrackingDate());
							mt.setLocation(listTemp.get(j).getLocation());
							mt.setOrderId(listTemp.get(j).getOrderId());
							listTracking.add(mt);
						}
					}
				}
			}
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() != 0 && listUserKolektorChoose.size() != 0) {
			listTracking = new ArrayList<MobileTrackingDto>();
			for (int h = 0; h < listCabangMstDtoChoose.size(); h++) {
				for (int i = 0; i < listUserKolektorChoose.size(); i++) {
					List<MobileTrackingDto> listTemp = new ArrayList<MobileTrackingDto>();
					listTemp = mobileTrackingSvc.findMobileTrackingByKodeCabangUserId(listCabangMstDtoChoose.get(h).getKodeCabang(),listUserKolektorChoose.get(i).getUserId(), tglAwal, tglAkhir, keySearch);
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							MobileTrackingDto mt = new MobileTrackingDto();
							mt.setUserId(listTemp.get(j).getUserId());
							mt.setNamaKaryawan(listTemp.get(j).getNamaKaryawan());
							mt.setNamaRegion(listTemp.get(j).getNamaRegion());
							mt.setNamaCabang(listTemp.get(j).getNamaCabang());
							mt.setKetTrackingDate(listTemp.get(j).getKetTrackingDate());
							mt.setLocation(listTemp.get(j).getLocation());
							mt.setOrderId(listTemp.get(j).getOrderId());
							listTracking.add(mt);
						}
					}
				}
			}
		}
	}
	
}
