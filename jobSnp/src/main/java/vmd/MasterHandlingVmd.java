package vmd;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;

import dto.MasterHandlingDto;
import dto.MasterProvinsiDto;
import dto.UserProfileDto;
import service.MasterHandlingSvc;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class MasterHandlingVmd {
	
	@WireVariable
	private MasterHandlingSvc masterHandlingSvc;
	
	private boolean addedit = false;
	private boolean id = false;
	private String search = "";
	private int page = 10;
	
	
	private List<MasterHandlingDto> listHandling = new ArrayList<MasterHandlingDto>();
	private MasterHandlingDto handlingDto = new MasterHandlingDto();
	public boolean isAddedit() {
		return addedit;
	}
	public void setAddedit(boolean addedit) {
		this.addedit = addedit;
	}
	public boolean isId() {
		return id;
	}
	public void setId(boolean id) {
		this.id = id;
	}
	public String getSearch() {
		return search;
	}
	public void setSearch(String search) {
		this.search = search;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public List<MasterHandlingDto> getListHandling() {
		return listHandling;
	}
	public void setListHandling(List<MasterHandlingDto> listHandling) {
		this.listHandling = listHandling;
	}
	public MasterHandlingDto getHandlingDto() {
		return handlingDto;
	}
	public void setHandlingDto(MasterHandlingDto handlingDto) {
		this.handlingDto = handlingDto;
	}
	

	@Init
	public void load(){
		Sessions.getCurrent().removeAttribute("sessionMobileTracking");
		int maxBatch = 0;
		maxBatch = masterHandlingSvc.maxBatchHandling();
		System.out.println("ini max batch nya = "+maxBatch);
		listHandling = masterHandlingSvc.findAllHandling("",maxBatch);
	}
	
	@Command("page")
	@NotifyChange("page")
	public void page() {
		setPage(getPage());
	}
	
	@Command("search")
	@NotifyChange({ "listHandling" })
	public void search() {
		int maxBatch = 0;
		maxBatch = masterHandlingSvc.maxBatchHandling();
		listHandling = masterHandlingSvc.findAllHandling(search,maxBatch);
	}
	
	@Command("add")
	@NotifyChange({"addedit","handlingDto","id"})
	public void add() {
		handlingDto = new MasterHandlingDto();
		setAddedit(true);
		setId(false);
	}
	
	@Command("close")
	@NotifyChange({ "addedit","handlingDto","listHandling"})
	public void close() {
		handlingDto = new MasterHandlingDto();
		load();
		setAddedit(false);
	}
	
	@Command("edit")
	@NotifyChange({"addedit","handlingDto","id"})
	public void edit() {
		if(handlingDto==null){
			Messagebox.show("Choose your data first !","Message",null,null,null);
		} else{
			setId(true);
			setAddedit(true);
		}
	}
	
	@Command("save")
	@NotifyChange({ "addedit","id","handlingDto","listHandling" })
	public void save() throws NoSuchAlgorithmException{
		System.out.println("hand no "+handlingDto.getHandNo());
		MasterHandlingDto hand2 = new MasterHandlingDto();
		hand2 = masterHandlingSvc.findOneHandling(handlingDto.getHandNo());
		if (hand2.getHandNo() == 0) {
			setId(false);
			if (handlingDto.getHandCode() == null ) {
				Messagebox.show("Kode Handling Tidak Boleh Kosong","Message",null,null,null);
			}  else{
				handlingDto.setHandNo(handlingDto.getHandNo());
				UserProfileDto userProfileDto = (UserProfileDto) Sessions.getCurrent().getAttribute("user");
				handlingDto.setCreateUser(userProfileDto.getNamaKaryawan());
				if(handlingDto.getAktif().equals("Enable")||handlingDto.getAktif().equals("1")){
					handlingDto.setAktif("1");
				}else{
					handlingDto.setAktif("0");
				}
				if(handlingDto.getPromise().equals("YES")||handlingDto.getPromise().equals("1")){
					handlingDto.setPromise("1");
				}else{
					handlingDto.setPromise("0");
				}
				int maxBatch = 0;
				maxBatch = masterHandlingSvc.maxBatchHandling();
				List<MasterHandlingDto> lh = new ArrayList<MasterHandlingDto>();
				lh= masterHandlingSvc.findAllHandlingByBatchHandling(maxBatch);
				
				maxBatch = maxBatch + 1;
				for (int i = 0; i < lh.size(); i++) {
					masterHandlingSvc.insertHandling(maxBatch,lh.get(i).getHandCode(), lh.get(i).getPromise(), lh.get(i).getDeskripsi(), 
							"1", userProfileDto.getUserId());
				}
				masterHandlingSvc.insertHandling(maxBatch,handlingDto.getHandCode(), handlingDto.getPromise(), handlingDto.getDeskripsi(), 
						handlingDto.getAktif(), handlingDto.getCreateUser());
				load();
				Messagebox.show("Data Handling Berhasil Dimasukan","Message",null,null,null);
				setAddedit(false);
			}
		} 
		else {
		UserProfileDto userProfileDto = (UserProfileDto) Sessions.getCurrent().getAttribute("user");
		handlingDto.setModifyUser(userProfileDto.getNamaKaryawan());
		if(handlingDto.getAktif().equals("Enable")||handlingDto.getAktif().equals("1")){
			handlingDto.setAktif("1");
		}else{
			handlingDto.setAktif("0");
		}
		if(handlingDto.getPromise().equals("YES")||handlingDto.getPromise().equals("1")){
			handlingDto.setPromise("1");
		}else{
			handlingDto.setPromise("0");
		}
		if (handlingDto.getHandCode() == null ) {
			Messagebox.show("Kode Handling Tidak Boleh Kosong","Message",null,null,null);
		}else{
			setId(true);
			int maxBatch = 0;
			maxBatch = masterHandlingSvc.maxBatchHandling();
			List<MasterHandlingDto> lh = new ArrayList<MasterHandlingDto>();
			lh= masterHandlingSvc.findAllHandlingByBatchHandling(maxBatch);
			
			maxBatch = maxBatch + 1;
			for (int i = 0; i < lh.size(); i++) {
				if (lh.get(i).getHandCode().equalsIgnoreCase(handlingDto.getHandCode())) {
					masterHandlingSvc.insertHandling(maxBatch,handlingDto.getHandCode(), handlingDto.getPromise(), handlingDto.getDeskripsi(), 
							handlingDto.getAktif(), handlingDto.getCreateUser());
				}else {
					masterHandlingSvc.insertHandling(maxBatch,lh.get(i).getHandCode(), lh.get(i).getPromise(), lh.get(i).getDeskripsi(), 
							"1", userProfileDto.getUserId());
				}
				
			}
			/*masterHandlingSvc.updateHandling(handlingDto.getHandCode(), handlingDto.getPromise(), handlingDto.getDeskripsi(), handlingDto.getAktif(),
					handlingDto.getModifyUser(), handlingDto.getHandNo());*/
			load();
			setAddedit(false);
			Messagebox.show("Data Handling Berhasil Diubah","Message",null,null,null);
		}}
		int maxBatch = 0;
		maxBatch = masterHandlingSvc.maxBatchHandling();
		listHandling = masterHandlingSvc.findAllHandling("",maxBatch);
		handlingDto = new MasterHandlingDto();
		
	}
	
	@Command("delete")
	@NotifyChange({"handlingDto", "listHandling" })
	public void delete() {
		if (handlingDto.getHandNo()== 0) {
			Messagebox.show("Choose your data first !","Message",null,null,null);
		} else {
			Messagebox.show("Are you sure to delete ?", "perhatian",
					new Button[] { Button.YES, Button.NO },
					Messagebox.QUESTION, Button.NO,
					new EventListener<Messagebox.ClickEvent>() {
						@Override
						public void onEvent(ClickEvent event) throws Exception {
							if (Messagebox.ON_YES.equals(event.getName())) {
								/*MasterRegionalDto dsfMstUser2Dto = (DsfMstUser2Dto) Sessions.getCurrent().getAttribute("user");
								dsfMstRegionalDto.setModifiedBy(dsfMstUser2Dto.getUserID());*/
								UserProfileDto userProfileDto = (UserProfileDto) Sessions.getCurrent().getAttribute("user");
								handlingDto.setModifyUser(userProfileDto.getNamaKaryawan());
								masterHandlingSvc.deleteHandling(handlingDto.getModifyUser(), handlingDto.getHandNo(), handlingDto.getHandCode());
								BindUtils.postNotifyChange(null, null,
										MasterHandlingVmd.this,
										"listHandling");
								int maxBatch = 0;
								maxBatch = masterHandlingSvc.maxBatchHandling();
								listHandling = masterHandlingSvc.findAllHandling("",maxBatch);
								Messagebox.show("Data Handling Berhasil Dihapus","Message",null,null,null);
								handlingDto = new MasterHandlingDto();
							}
						}
					});
		}
	}

	
}
