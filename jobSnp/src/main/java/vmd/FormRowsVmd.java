package vmd;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.record.DSFRecord;
import org.hibernate.cache.spi.access.RegionAccessStrategy;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;

import dto.CabangMstDto;
import dto.EventsDto;
import dto.FormFieldsDto;
import dto.FormRowsDto;
import dto.FormsDto;
import dto.MasterRegionalDto;
import dto.ParamMstDto;
import dto.UserProfileDto;
import pagevmd.NavigationVmd;
import service.CabangMstSvc;
import service.EventsSvc;
import service.FormFieldsSvc;
import service.FormRowsSvc;
import service.FormsSvc;
import service.MasterRegionalSvc;
import service.ParamMstSvc;
import service.UserProfileSvc;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class FormRowsVmd extends NavigationVmd{

	@WireVariable
	private FormRowsSvc formRowsSvc;
	
	@WireVariable
	private FormFieldsSvc formFieldsSvc;
	
	@WireVariable
	private ParamMstSvc paramMstSvc;
	
	private List<FormRowsDto> listFormRowsDto = new ArrayList<FormRowsDto>();
	private List<ParamMstDto> listParamMstDto = new ArrayList<ParamMstDto>();
	private List<FormFieldsDto> listFormFieldsDto = new ArrayList<FormFieldsDto>();
	private ParamMstDto paramMstDto;
	private FormRowsDto formRowsDto;
	private FormFieldsDto formFieldsDto;
	private int page = 10;
	private String keySearch = "";
	private String keySearchParam = "";
	private boolean visibleAddEdit = false;
	private boolean visibleAddFields = false;
	private boolean visibleDetailFields = false;
	private boolean visibleParam = false;
	private boolean fieldRowId = false;
	private boolean fieldId = false;

	
	
	public String getKeySearchParam() {
		return keySearchParam;
	}

	public void setKeySearchParam(String keySearchParam) {
		this.keySearchParam = keySearchParam;
	}

	public List<ParamMstDto> getListParamMstDto() {
		return listParamMstDto;
	}

	public void setListParamMstDto(List<ParamMstDto> listParamMstDto) {
		this.listParamMstDto = listParamMstDto;
	}

	public ParamMstDto getParamMstDto() {
		return paramMstDto;
	}

	public void setParamMstDto(ParamMstDto paramMstDto) {
		this.paramMstDto = paramMstDto;
	}

	public boolean isVisibleParam() {
		return visibleParam;
	}

	public void setVisibleParam(boolean visibleParam) {
		this.visibleParam = visibleParam;
	}

	public boolean isFieldId() {
		return fieldId;
	}

	public void setFieldId(boolean fieldId) {
		this.fieldId = fieldId;
	}

	public boolean isVisibleDetailFields() {
		return visibleDetailFields;
	}

	public void setVisibleDetailFields(boolean visibleDetailFields) {
		this.visibleDetailFields = visibleDetailFields;
	}

	public boolean isVisibleAddFields() {
		return visibleAddFields;
	}

	public void setVisibleAddFields(boolean visibleAddFields) {
		this.visibleAddFields = visibleAddFields;
	}

	public List<FormRowsDto> getListFormRowsDto() {
		return listFormRowsDto;
	}

	public void setListFormRowsDto(List<FormRowsDto> listFormRowsDto) {
		this.listFormRowsDto = listFormRowsDto;
	}

	public List<FormFieldsDto> getListFormFieldsDto() {
		return listFormFieldsDto;
	}

	public void setListFormFieldsDto(List<FormFieldsDto> listFormFieldsDto) {
		this.listFormFieldsDto = listFormFieldsDto;
	}

	public FormRowsDto getFormRowsDto() {
		return formRowsDto;
	}

	public void setFormRowsDto(FormRowsDto formRowsDto) {
		this.formRowsDto = formRowsDto;
	}

	public FormFieldsDto getFormFieldsDto() {
		return formFieldsDto;
	}

	public void setFormFieldsDto(FormFieldsDto formFieldsDto) {
		this.formFieldsDto = formFieldsDto;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getKeySearch() {
		return keySearch;
	}

	public void setKeySearch(String keySearch) {
		this.keySearch = keySearch;
	}

	public boolean isVisibleAddEdit() {
		return visibleAddEdit;
	}

	public void setVisibleAddEdit(boolean visibleAddEdit) {
		this.visibleAddEdit = visibleAddEdit;
	}

	public boolean isFieldRowId() {
		return fieldRowId;
	}

	public void setFieldRowId(boolean fieldRowId) {
		this.fieldRowId = fieldRowId;
	}

	@Init
	public void load() {
		keySearch = "";
		Sessions.getCurrent().removeAttribute("sessionMobileTracking");
		listFormRowsDto = formRowsSvc.findFormRowsWithSearch(keySearch);
	}
	
	@Command("page")
	@NotifyChange("page")
	public void page() {
		setPage(getPage());
	}
	
	@Command("add")
	@NotifyChange({"formRowsDto", "fieldRowId", "visibleAddEdit"})
	public void add() {
		formRowsDto = new FormRowsDto();
		setFieldRowId(false);
		setVisibleAddEdit(true);
	}
	
	@Command("search")
	@NotifyChange({ "listFormRowsDto" })
	public void search() {
		listFormRowsDto = formRowsSvc.findFormRowsWithSearch(keySearch);
	}
	
	@Command("close")
	@NotifyChange({ "visibleAddEdit","formRowsDto" })
	public void close() {
		formRowsDto = new FormRowsDto();
		setVisibleAddEdit(false);
	}
	
	@Command("save")
	@NotifyChange({"formRowsDto", "listFormRowsDto", "fieldRowId", "visibleAddEdit"})
	public void save() throws NoSuchAlgorithmException{
		FormRowsDto fr = new FormRowsDto();
		fr = formRowsSvc.findFormRowsById(formRowsDto.getRowId());
		if (fr.getRowId() == null) {
			if (formRowsDto.getRowId() == null ) {
				Messagebox.show("Row Id harus terisi", "MESSAGE", null, null, null);
			} else if (formRowsDto.getRowOrder() == null ) {
				Messagebox.show("Row order harus terisi", "MESSAGE", null, null, null);
			} else if (formRowsDto.getRowVisibility() == null ) {
				Messagebox.show("Row visibility harus terisi", "MESSAGE", null, null, null);
			} else if (formRowsDto.getFormId() == null ) {
				Messagebox.show("Form id harus terisi", "MESSAGE", null, null, null);
			} else {
				formRowsSvc.saveFormRows(formRowsDto.getRowId(), formRowsDto.getRowOrder(), formRowsDto.getRowVisibility(), formRowsDto.getFormId());
				load();
				Messagebox.show("Data row berhasil disimpan", "MESSAGE", null, null, null);
				formRowsDto = new FormRowsDto();
				setVisibleAddEdit(false);
			}
		} else {
			if (formRowsDto.getRowId() == null ) {
				Messagebox.show("Row Id harus terisi", "MESSAGE", null, null, null);
			} else if (formRowsDto.getRowOrder() == null ) {
				Messagebox.show("Row order harus terisi", "MESSAGE", null, null, null);
			} else if (formRowsDto.getRowVisibility() == null ) {
				Messagebox.show("Row visibility harus terisi", "MESSAGE", null, null, null);
			} else if (formRowsDto.getFormId() == null ) {
				Messagebox.show("Form id harus terisi", "MESSAGE", null, null, null);
			} else {
				formRowsSvc.updateFormRows(formRowsDto.getRowOrder(), formRowsDto.getRowVisibility(), formRowsDto.getFormId(), formRowsDto.getRowId());
				load();
				Messagebox.show("Data row berhasil disimpan", "MESSAGE", null, null, null);
				formRowsDto = new FormRowsDto();
				setVisibleAddEdit(false);
			}
		}
	}
	
	
	@Command("edit")
	@NotifyChange({"formRowsDto", "fieldRowId", "visibleAddEdit"})
	public void edit() {
		if(formRowsDto == null){
			Messagebox.show("Choose your data first !","Message",null,null,null);
		} else{
			setFieldRowId(true);
			setVisibleAddEdit(true);
		}
	}
	
	
	@Command("delete")
	@NotifyChange({  "formsDto", "listFormsDto" })
	public void delete() {
		if (formRowsDto.getRowId() == null) {
			Messagebox.show("Choose your data first !","Message",null,null,null);
		} else {
			Messagebox.show("Are you sure to delete ?", "perhatian",
					new Button[] { Button.YES, Button.NO },
					Messagebox.QUESTION, Button.NO,
					new EventListener<Messagebox.ClickEvent>() {
						@Override
						public void onEvent(ClickEvent event) throws Exception {
							if (Messagebox.ON_YES.equals(event.getName())) {
								formRowsSvc.deleteFormRows(formRowsDto.getRowId());
								BindUtils.postNotifyChange(null, null,
										FormRowsVmd.this,
										"listFormRowsDto");
								BindUtils.postNotifyChange(null, null,
										FormRowsVmd.this,
										"formRowsDto");
								listFormRowsDto = formRowsSvc.findFormRowsWithSearch("");
								Messagebox.show("Data berhasil di hapus", "MESSAGE", null, null, null);
								formRowsDto = new FormRowsDto();
							}
						}
					});
		}
	}
	
	@Command("upOrder")
	@NotifyChange({"formRowsDto", "listFormRowsDto"})
	public void upOrder() {
		if (formRowsDto == null) {
			Messagebox.show("Choose your data first !","Message",null,null,null);
		}else {
			int order = Integer.valueOf(formRowsDto.getRowOrder());
			order = order + 1;
			formRowsSvc.upDownFormRows(order, formRowsDto.getRowId());
			listFormRowsDto = formRowsSvc.findFormRowsWithSearch(keySearch);
		}
	}
	
	@Command("downOrder")
	@NotifyChange({"formRowsDto", "listFormRowsDto"})
	public void downOrder() {
		if (formRowsDto == null) {
			Messagebox.show("Choose your data first !","Message",null,null,null);
		}else {
			int order = Integer.valueOf(formRowsDto.getRowOrder());
			order = order - 1;
			formRowsSvc.upDownFormRows(order, formRowsDto.getRowId());
			listFormRowsDto = formRowsSvc.findFormRowsWithSearch(keySearch);
		}
	}
	
	@Command("addFields")
	@NotifyChange({"formFieldsDto", "visibleAddFields","fieldId"})
	public void addFields() {
		setFieldId(false);
		formFieldsDto = new FormFieldsDto();
		formFieldsDto.setRowId(formRowsDto.getRowId());
		setVisibleAddFields(true);
	}
	@Command("updateFields")
	@NotifyChange({"formFieldsDto", "visibleAddFields","fieldId"})
	public void updateFields() {
		setFieldId(true);
		setVisibleAddFields(true);
	}
	
	@Command("closeFields")
	@NotifyChange({"formFieldsDto", "visibleAddFields"})
	public void closeFields() {
		formFieldsDto = new FormFieldsDto();
		setVisibleAddFields(false);
	}
	
	@Command("saveFields")
	@NotifyChange({"formFieldsDto", "visibleAddFields"})
	public void saveFields() {
		FormFieldsDto fd = new FormFieldsDto();
		fd = formFieldsSvc.findFormFieldsById(formFieldsDto.getFieldId());
		if (fd.getFieldId() == null) {
			if (formFieldsDto.getFieldId() == null ) {
				Messagebox.show("Field ID harus terisi", "MESSAGE", null, null, null);
			} else if (formFieldsDto.getFieldName() == null ) {
				Messagebox.show("Field name harus terisi", "MESSAGE", null, null, null);
			} else if (formFieldsDto.getFieldType() == null ) {
				Messagebox.show("Field Type harus terisi", "MESSAGE", null, null, null);
			} else if (formFieldsDto.getFieldVisibility() == null ) {
				Messagebox.show("VISIBILITY harus terisi", "MESSAGE", null, null, null);
			} else if (formFieldsDto.getFieldLabel() == null ) {
				Messagebox.show("Label harus terisi", "MESSAGE", null, null, null);
			} else if (formFieldsDto.getFieldGlobalValue() == null ) {
				Messagebox.show("Global value harus terisi", "MESSAGE", null, null, null);
			} else if (formFieldsDto.getFieldWeight() == null ) {
				Messagebox.show("Weight harus terisi", "MESSAGE", null, null, null);
			} else if (formFieldsDto.getFieldStore() == null ) {
				Messagebox.show("Store harus terisi", "MESSAGE", null, null, null);
			} else if (formFieldsDto.getFieldExtra() == null ) {
				Messagebox.show("Extra harus terisi", "MESSAGE", null, null, null);
			} else if (formFieldsDto.getRowId() == null ) {
				Messagebox.show("Row id harus terisi", "MESSAGE", null, null, null);
			} else {
				formFieldsSvc.saveFormFields(formFieldsDto.getFieldId(), formFieldsDto.getFieldName(), formFieldsDto.getFieldType(), formFieldsDto.getFieldVisibility(), formFieldsDto.getFieldLabel(), formFieldsDto.getFieldGlobalValue(), formFieldsDto.getFieldWeight(), formFieldsDto.getFieldStore(), formFieldsDto.getFieldExtra(), formFieldsDto.getRowId());
				load();
				Messagebox.show("Data field berhasil disimpan", "MESSAGE", null, null, null);
				formRowsDto = new FormRowsDto();
				setVisibleAddFields(false);
			}
		} else {
			if (formFieldsDto.getFieldId() == null ) {
				Messagebox.show("Field ID harus terisi", "MESSAGE", null, null, null);
			} else if (formFieldsDto.getFieldName() == null ) {
				Messagebox.show("Field name harus terisi", "MESSAGE", null, null, null);
			} else if (formFieldsDto.getFieldType() == null ) {
				Messagebox.show("Field Type harus terisi", "MESSAGE", null, null, null);
			} else if (formFieldsDto.getFieldVisibility() == null ) {
				Messagebox.show("VISIBILITY harus terisi", "MESSAGE", null, null, null);
			} else if (formFieldsDto.getFieldLabel() == null ) {
				Messagebox.show("Label harus terisi", "MESSAGE", null, null, null);
			} else if (formFieldsDto.getFieldGlobalValue() == null ) {
				Messagebox.show("Global value harus terisi", "MESSAGE", null, null, null);
			} else if (formFieldsDto.getFieldWeight() == null ) {
				Messagebox.show("Weight harus terisi", "MESSAGE", null, null, null);
			} else if (formFieldsDto.getFieldStore() == null ) {
				Messagebox.show("Store harus terisi", "MESSAGE", null, null, null);
			} else if (formFieldsDto.getFieldExtra() == null ) {
				Messagebox.show("Extra harus terisi", "MESSAGE", null, null, null);
			} else if (formFieldsDto.getRowId() == null ) {
				Messagebox.show("Row id harus terisi", "MESSAGE", null, null, null);
			} else {
				formFieldsSvc.updateFormFields(formFieldsDto.getFieldName(), formFieldsDto.getFieldType(), formFieldsDto.getFieldVisibility(), formFieldsDto.getFieldLabel(), formFieldsDto.getFieldGlobalValue(), formFieldsDto.getFieldWeight(), formFieldsDto.getFieldStore(), formFieldsDto.getFieldExtra(), formFieldsDto.getRowId(), formFieldsDto.getFieldId());
				load();
				Messagebox.show("Data field berhasil diubah", "MESSAGE", null, null, null);
				formRowsDto = new FormRowsDto();
				setVisibleAddFields(false);
			}
		}
	}
	
	
	@Command("showDetailFields")
	@NotifyChange({"visibleDetailFields","listFormFieldsDto"})
	public void showDetailFields() {
		listFormFieldsDto = formFieldsSvc.findFormFieldsByRowId(formRowsDto.getRowId());
		setVisibleDetailFields(true);
	}
	
	@Command("closeDetailFields")
	@NotifyChange({"visibleDetailFields","listFormFieldsDto"})
	public void closeDetailFields() {
		listFormFieldsDto = new ArrayList<FormFieldsDto>();
		setVisibleDetailFields(false);
	}
	
	
	@Command("showParameter")
	@NotifyChange({"listParamMstDto","formFieldsDto","visibleParam","keySearchParam"})
	public void showParameter() {
		keySearchParam = "";
		listParamMstDto = paramMstSvc.findParamWithSearch(keySearchParam);
		setVisibleParam(true);
	}
	
	@Command("searchParameter")
	@NotifyChange({"formFieldsDto","visibleParam","keySearchParam"})
	public void searchParameter() {
		listParamMstDto = paramMstSvc.findParamWithSearch(keySearchParam);
	}
	
	@Command("chooseParam")
	@NotifyChange({"formFieldsDto","visibleParam"})
	public void chooseParam() {
		formFieldsDto.setFieldGlobalValue(paramMstDto.getParamId());
		setVisibleParam(false);
	}
	
	@Command("closeParameter")
	@NotifyChange({"formFieldsDto","visibleParam", "paramMstDto","listParamMstDto"})
	public void closeParameter() {
		paramMstDto = new ParamMstDto();
		listParamMstDto = new ArrayList<ParamMstDto>();
		setVisibleParam(false);
	}
	

}
