package vmd;

import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import dto.AuthorityListDto;
import dto.AuthorityMstDto;
import dto.MenuMstDto;
import dto.UserProfileDto;
import pagevmd.NavigationVmd;
import service.AuthorityListSvc;
import service.AuthorityMstSvc;
import service.MenuMstSvc;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class sidebarvmd extends NavigationVmd {
	
	private boolean menu1 = true;
	private boolean menu2 = true;
	private boolean menu3 = true;
	private boolean menu4 = true;
	private boolean menu5 = true;

	private boolean MASTER_USER = false;
	private boolean APPLICATION_ROLES = false;
	private boolean CHANGE_PASSWORD = false;
	
	private boolean MASTER_MENU = false;
	private boolean MASTER_REGIONAL = false;
	private boolean MASTER_CABANG = false;
	private boolean MASTER_AUTHORITY = false;
	private boolean MASTER_PROVINSI = false;
	private boolean MASTER_HANDLING = false;
	private boolean MASTER_PARAMETER = false;
	
	private boolean UPLOAD_HANDLING = false;
	private boolean REASSIGNMENT = false;
	private boolean TASK_ADJUSTMENT = false;
	private boolean NOTIFICATION = false;

	private boolean MOBILE_TRACKING = false;
	
	private boolean REPORT_KUNJUNGAN_KOLEKTOR = false;
	private boolean REKON_KOLEKTOR = false;
	private boolean COLLECTOR_FULL_DATA = false;
	
	private UserProfileDto userDto = new UserProfileDto();
	private String roleID;

	

	@WireVariable
	private AuthorityListSvc authorityListSvc;

	
	public boolean isMASTER_PARAMETER() {
		return MASTER_PARAMETER;
	}

	public void setMASTER_PARAMETER(boolean mASTER_PARAMETER) {
		MASTER_PARAMETER = mASTER_PARAMETER;
	}

	public boolean isREPORT_KUNJUNGAN_KOLEKTOR() {
		return REPORT_KUNJUNGAN_KOLEKTOR;
	}

	public void setREPORT_KUNJUNGAN_KOLEKTOR(boolean rEPORT_KUNJUNGAN_KOLEKTOR) {
		REPORT_KUNJUNGAN_KOLEKTOR = rEPORT_KUNJUNGAN_KOLEKTOR;
	}

	public boolean isMASTER_HANDLING() {
		return MASTER_HANDLING;
	}

	public void setMASTER_HANDLING(boolean mASTER_HANDLING) {
		MASTER_HANDLING = mASTER_HANDLING;
	}

	public boolean isMenu3() {
		return menu3;
	}

	public void setMenu3(boolean menu3) {
		this.menu3 = menu3;
	}

	public boolean isMenu4() {
		return menu4;
	}

	public void setMenu4(boolean menu4) {
		this.menu4 = menu4;
	}

	

	public boolean isAPPLICATION_ROLES() {
		return APPLICATION_ROLES;
	}

	public void setAPPLICATION_ROLES(boolean aPPLICATION_ROLES) {
		APPLICATION_ROLES = aPPLICATION_ROLES;
	}

	public boolean isMASTER_PROVINSI() {
		return MASTER_PROVINSI;
	}

	public void setMASTER_PROVINSI(boolean mASTER_PROVINSI) {
		MASTER_PROVINSI = mASTER_PROVINSI;
	}

	public boolean isUPLOAD_HANDLING() {
		return UPLOAD_HANDLING;
	}

	public void setUPLOAD_HANDLING(boolean uPLOAD_HANDLING) {
		UPLOAD_HANDLING = uPLOAD_HANDLING;
	}

	public boolean isREASSIGNMENT() {
		return REASSIGNMENT;
	}

	public void setREASSIGNMENT(boolean rEASSIGNMENT) {
		REASSIGNMENT = rEASSIGNMENT;
	}

	public boolean isTASK_ADJUSTMENT() {
		return TASK_ADJUSTMENT;
	}

	public void setTASK_ADJUSTMENT(boolean tASK_ADJUSTMENT) {
		TASK_ADJUSTMENT = tASK_ADJUSTMENT;
	}

	public boolean isNOTIFICATION() {
		return NOTIFICATION;
	}

	public void setNOTIFICATION(boolean nOTIFICATION) {
		NOTIFICATION = nOTIFICATION;
	}
	
	public boolean isREKON_KOLEKTOR() {
		return REKON_KOLEKTOR;
	}

	public void setREKON_KOLEKTOR(boolean rEKON_KOLEKTOR) {
		REKON_KOLEKTOR = rEKON_KOLEKTOR;
	}

	public boolean isCOLLECTOR_FULL_DATA() {
		return COLLECTOR_FULL_DATA;
	}

	public void setCOLLECTOR_FULL_DATA(boolean cOLLECTOR_FULL_DATA) {
		COLLECTOR_FULL_DATA = cOLLECTOR_FULL_DATA;
	}

	public boolean isMOBILE_TRACKING() {
		return MOBILE_TRACKING;
	}

	public void setMOBILE_TRACKING(boolean mOBILE_TRACKING) {
		MOBILE_TRACKING = mOBILE_TRACKING;
	}

	public UserProfileDto getUserDto() {
		return userDto;
	}

	public void setUserDto(UserProfileDto userDto) {
		this.userDto = userDto;
	}

	public String getRoleID() {
		return roleID;
	}

	public void setRoleID(String roleID) {
		this.roleID = roleID;
	}

	public boolean isMenu1() {
		return menu1;
	}

	public void setMenu1(boolean menu1) {
		this.menu1 = menu1;
	}

	public boolean isMenu2() {
		return menu2;
	}

	public void setMenu2(boolean menu2) {
		this.menu2 = menu2;
	}

	public boolean isMASTER_USER() {
		return MASTER_USER;
	}

	public void setMASTER_USER(boolean mASTER_USER) {
		MASTER_USER = mASTER_USER;
	}

	public boolean isMASTER_MENU() {
		return MASTER_MENU;
	}

	public void setMASTER_MENU(boolean mASTER_MENU) {
		MASTER_MENU = mASTER_MENU;
	}

	public boolean isMASTER_REGIONAL() {
		return MASTER_REGIONAL;
	}

	public void setMASTER_REGIONAL(boolean mASTER_REGIONAL) {
		MASTER_REGIONAL = mASTER_REGIONAL;
	}

	public boolean isMASTER_AUTHORITY() {
		return MASTER_AUTHORITY;
	}

	public void setMASTER_AUTHORITY(boolean mASTER_AUTHORITY) {
		MASTER_AUTHORITY = mASTER_AUTHORITY;
	}

	public boolean isMASTER_CABANG() {
		return MASTER_CABANG;
	}

	public void setMASTER_CABANG(boolean mASTER_CABANG) {
		MASTER_CABANG = mASTER_CABANG;
	}

	public boolean isCHANGE_PASSWORD() {
		return CHANGE_PASSWORD;
	}

	public void setCHANGE_PASSWORD(boolean cHANGE_PASSWORD) {
		CHANGE_PASSWORD = cHANGE_PASSWORD;
	}
	

	public boolean isMenu5() {
		return menu5;
	}

	public void setMenu5(boolean menu5) {
		this.menu5 = menu5;
	}

	@Init
	public void load() {
		if (Sessions.getCurrent().getAttribute("user") != null) {
			userDto = (UserProfileDto) Sessions.getCurrent().getAttribute(
					"user");
			List<AuthorityListDto> listDsfMstAuthorityListMenu = authorityListSvc.findAuthorityListByAuthorityId(userDto.getAuthorityId());

			for (int i = 0; i < listDsfMstAuthorityListMenu.size(); i++) {
				if (listDsfMstAuthorityListMenu.get(i).getMenuName().equals("masterUser")) {
					setMASTER_USER(true);
				}
				if (listDsfMstAuthorityListMenu.get(i).getMenuName().equals("changePassword")) {
					setCHANGE_PASSWORD(true);
				}
				if (listDsfMstAuthorityListMenu.get(i).getMenuName().equals("applicationRoles")) {
					setAPPLICATION_ROLES(true);
				}
				
				
				if (listDsfMstAuthorityListMenu.get(i).getMenuName().equals("masterRegion")) {
					setMASTER_REGIONAL(true);
				}
				if (listDsfMstAuthorityListMenu.get(i).getMenuName().equals("masterCabang")) {
					setMASTER_CABANG(true);
				}
				if (listDsfMstAuthorityListMenu.get(i).getMenuName().equals("masterMenu")) {
					setMASTER_CABANG(true);
				}
				if (listDsfMstAuthorityListMenu.get(i).getMenuName().equals("masterAuthority")) {
					setMASTER_AUTHORITY(true);
				}
				if (listDsfMstAuthorityListMenu.get(i).getMenuName().equals("masterProvinsi")) {
					setMASTER_PROVINSI(true);
				}
				if (listDsfMstAuthorityListMenu.get(i).getMenuName().equals("masterHandling")) {
					setMASTER_HANDLING(true);
				}
				if (listDsfMstAuthorityListMenu.get(i).getMenuName().equals("masterParameter")) {
					setMASTER_PARAMETER(true);
				}
				
				if (listDsfMstAuthorityListMenu.get(i).getMenuName().equals("uploadHandling")) {
					setUPLOAD_HANDLING(true);
				}
				if (listDsfMstAuthorityListMenu.get(i).getMenuName().equals("reassignment")) {
					setREASSIGNMENT(true);
				}
				if (listDsfMstAuthorityListMenu.get(i).getMenuName().equals("taskAdjustment")) {
					setTASK_ADJUSTMENT(true);
				}
				if (listDsfMstAuthorityListMenu.get(i).getMenuName().equals("notification")) {
					setNOTIFICATION(true);
				}
				
				if (listDsfMstAuthorityListMenu.get(i).getMenuName().equals("mobileTracking")) {
					setMOBILE_TRACKING(true);
				}
				
				if (listDsfMstAuthorityListMenu.get(i).getMenuName().equals("reportKunjunganKolektor")) {
					setREPORT_KUNJUNGAN_KOLEKTOR(true);
				}
				if (listDsfMstAuthorityListMenu.get(i).getMenuName().equals("rekonKolektor")) {
					setREKON_KOLEKTOR(true);
				}
				if (listDsfMstAuthorityListMenu.get(i).getMenuName().equals("collectorFullData")) {
					setCOLLECTOR_FULL_DATA(true);
				}
			}

			if (MASTER_USER == false && APPLICATION_ROLES == false && CHANGE_PASSWORD == false) {
				setMenu1(false);
			}
			if (MASTER_CABANG == false && MASTER_REGIONAL == false && MASTER_MENU == false && MASTER_AUTHORITY == false && MASTER_PROVINSI == false && MASTER_HANDLING == false && MASTER_PARAMETER == false) {
				setMenu2(false);
			}
			if (UPLOAD_HANDLING == false && REASSIGNMENT == false && TASK_ADJUSTMENT == false && NOTIFICATION == false ) {
				setMenu3(false);
			}
			if (MOBILE_TRACKING == false) {
				setMenu4(false);
			}
			if (REPORT_KUNJUNGAN_KOLEKTOR == false && REKON_KOLEKTOR == false && COLLECTOR_FULL_DATA == false) {
				setMenu5(false);
			}
		}
	}
}
