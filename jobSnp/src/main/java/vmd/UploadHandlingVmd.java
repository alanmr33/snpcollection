package vmd;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.DriverManager;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.activation.MimetypesFileTypeMap;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.zkoss.bind.BindContext;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.io.Files;
import org.zkoss.util.media.Media;
import org.zkoss.zel.impl.lang.ELSupport;
import org.zkoss.zhtml.Filedownload;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;

import ch.qos.logback.core.joran.conditional.ElseAction;
import dto.CollectionDetailDto;
import dto.CollectionHeaderDto;
import pagevmd.NavigationVmd;
import service.CollectionDetailSvc;
import service.CollectionHeaderSvc;


@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class UploadHandlingVmd extends NavigationVmd{
	
	@WireVariable
	private CollectionHeaderSvc collectionHeaderSvc;
	
	@WireVariable
	private CollectionDetailSvc collectionDetailSvc;
	
	private String fileNameUpload;
	private Media media;
	private File fileUpload;
	private String filePath = "";
	private String namaFileAcak = "";
	private List<CollectionHeaderDto> listCollectionHeaderDto = new ArrayList<CollectionHeaderDto>();
	private List<CollectionDetailDto> listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
	private List<CollectionDetailDto> listCollectionDetailDtoCheked = new ArrayList<CollectionDetailDto>();
	private List<CollectionDetailDto> listCollectionDetailDtoFalse = new ArrayList<CollectionDetailDto>();
	private CollectionHeaderDto collHeaderDto = new CollectionHeaderDto();
	private CollectionDetailDto collDetailDto = new CollectionDetailDto();
	private boolean stsShowListTrue = false;
	private boolean stsShowListFalse = false;
	private boolean stsShowListImport = false;
	private int page = 10;
	
	
	
	public boolean isStsShowListImport() {
		return stsShowListImport;
	}
	public void setStsShowListImport(boolean stsShowListImport) {
		this.stsShowListImport = stsShowListImport;
	}
	public List<CollectionDetailDto> getListCollectionDetailDtoCheked() {
		return listCollectionDetailDtoCheked;
	}
	public void setListCollectionDetailDtoCheked(
			List<CollectionDetailDto> listCollectionDetailDtoCheked) {
		this.listCollectionDetailDtoCheked = listCollectionDetailDtoCheked;
	}
	public File getFileUpload() {
		return fileUpload;
	}
	public void setFileUpload(File fileUpload) {
		this.fileUpload = fileUpload;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public boolean isStsShowListTrue() {
		return stsShowListTrue;
	}
	public void setStsShowListTrue(boolean stsShowListTrue) {
		this.stsShowListTrue = stsShowListTrue;
	}
	public boolean isStsShowListFalse() {
		return stsShowListFalse;
	}
	public void setStsShowListFalse(boolean stsShowListFalse) {
		this.stsShowListFalse = stsShowListFalse;
	}
	public List<CollectionDetailDto> getListCollectionDetailDtoFalse() {
		return listCollectionDetailDtoFalse;
	}
	public void setListCollectionDetailDtoFalse(
			List<CollectionDetailDto> listCollectionDetailDtoFalse) {
		this.listCollectionDetailDtoFalse = listCollectionDetailDtoFalse;
	}
	public List<CollectionHeaderDto> getListCollectionHeaderDto() {
		return listCollectionHeaderDto;
	}
	public void setListCollectionHeaderDto(
			List<CollectionHeaderDto> listCollectionHeaderDto) {
		this.listCollectionHeaderDto = listCollectionHeaderDto;
	}
	public List<CollectionDetailDto> getListCollectionDetailDto() {
		return listCollectionDetailDto;
	}
	public void setListCollectionDetailDto(
			List<CollectionDetailDto> listCollectionDetailDto) {
		this.listCollectionDetailDto = listCollectionDetailDto;
	}
	public CollectionHeaderDto getCollHeaderDto() {
		return collHeaderDto;
	}
	public void setCollHeaderDto(CollectionHeaderDto collHeaderDto) {
		this.collHeaderDto = collHeaderDto;
	}
	public CollectionDetailDto getCollDetailDto() {
		return collDetailDto;
	}
	public void setCollDetailDto(CollectionDetailDto collDetailDto) {
		this.collDetailDto = collDetailDto;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getNamaFileAcak() {
		return namaFileAcak;
	}
	public void setNamaFileAcak(String namaFileAcak) {
		this.namaFileAcak = namaFileAcak;
	}
	public String getFileNameUpload() {
		return fileNameUpload;
	}
	public void setFileNameUpload(String fileNameUpload) {
		this.fileNameUpload = fileNameUpload;
	}
	public Media getMedia() {
		return media;
	}
	public void setMedia(Media media) {
		this.media = media;
	}
	
	@Command("downloadTemplate")
	public void downloadTemplate() {
		FileInputStream inputStream;
		try {
			File dosfile = new File("C:/mobile_coll/webapps/file/template.xlsx");
			//File dosfile = new File("C:/EdwinBootcamp/jobSnp/src/main/webapp/file/template.xlsx");
			if (dosfile.exists()) {
				inputStream = new FileInputStream(dosfile);
				Filedownload.save(inputStream,
						new MimetypesFileTypeMap().getContentType(dosfile),
						dosfile.getName());
				Messagebox.show("Download template.xlsx berhasil !","MESSAGE",null,null,null);
			} else
				Messagebox.show("Gagal download template ","MESSAGE",null,null,null);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	@Command("page")
	@NotifyChange("page")
	public void page() {
		setPage(getPage());
	}
	
	@Command("closeListfalse")
	@NotifyChange("stsShowListFalse")
	public void closeListfalse() {
		setStsShowListFalse(false);
	}
	
	@Command("closeListImport")
	@NotifyChange("stsShowListImport")
	public void closeListImport() {
		setStsShowListImport(false);
	}
	
	@Command("showListfalse")
	@NotifyChange("stsShowListFalse")
	public void showListfalse() {
		setStsShowListFalse(true);
	}
	
	@Command("nextToImport")
	@NotifyChange("stsShowListImport")
	public void nextToImport() {
		if (listCollectionDetailDtoCheked.size() == 0) {
			Messagebox.show("Pilih terlebih dahulu data yang ingin diimport", "MESSAGE", null, null, null, null);
		}else {
			setStsShowListImport(true);
		}
	}
	
	@Command("runUpload")
	@NotifyChange({ "fileNameUpload", "nameFileAcak", "lisDsfMStOrdersDto","orderDto","filePath","fileUpload"})
	public void doUpload(@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx) throws IOException {
		UploadEvent upEvent = null;
		Object objUploadEvent = ctx.getTriggerEvent();
		if (objUploadEvent != null && (objUploadEvent instanceof UploadEvent)) {
			upEvent = (UploadEvent) objUploadEvent;
		}
		media = upEvent.getMedia();
	    if (media.getFormat().equalsIgnoreCase("xlx") || media.getFormat().equalsIgnoreCase("xlsx") || media.getFormat().equalsIgnoreCase("xls")) {
			fileNameUpload = media.getName();
		} else {
			Messagebox.show("Upload data hanya file excel !","MESSAGE",null,null,null);
		}
	}
	
	@Command("showData")
	@NotifyChange({"listCollectionDetailDtoCheked","stsShowListFalse","stsShowListTrue","listCollectionDetailDto", "listCollectionDetailDtoFalse","fileNameUpload"})
	public void showData() throws IOException {
		listCollectionDetailDtoCheked = new ArrayList<CollectionDetailDto>();
		listCollectionDetailDtoFalse = new ArrayList<CollectionDetailDto>();
		listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
		List<CollectionDetailDto> temListCollectionDetailDtoTrue = new ArrayList<CollectionDetailDto>();
		List<CollectionDetailDto> temListCollectionDetailDtoFalse = new ArrayList<CollectionDetailDto>();
		boolean stsData = true;
		if (fileNameUpload != null || fileNameUpload == "") {
			Workbook workbook = new XSSFWorkbook(media.getStreamData());
			Sheet firstSheet = workbook.getSheetAt(0);
			Row roww;
			String orderId;
			String customerCode;
			String contractNo;
			int installmentNo;
			String lpkNo;
			String customerName;
			String address;
			String village;
			String district;
			String rt;
			String rw;
			String city;
			String kodeProvinsi;
			String postCode;
			Date dueDate;
			String ketDueDate;
			int installmentAmt;
			int depositAmt;
			int penaltyAmt;
			int collectionFee;
			int totalAmt;
			String receiptNo;
			int minPayment;
			int maxPayment;
			Date repaymentDate;
			String ketRepaymentDate;
			String notes;
			int promiseCount;
			int promiseAvailable;
			int promiseRange;
			String phoneArea;
			String phoneNumber;
			String mobilePrefix;
			String mobileNo;
			String namaBarang;
			Date lpkDate;
			String ketLpkDate;
			String collectorCode;
			String status;
			for (int i = 1; i <= firstSheet.getLastRowNum(); i++) {
				orderId = "NULL";
				customerCode = "NULL";
				contractNo = "NULL";
				installmentNo = 0;
				lpkNo = "NULL";
				customerName = "NULL";
				address = "NULL";
				village  = "NULL";
				district  = "NULL";
				rt = "NULL";
				rw = "NULL";
				city = "NULL";
				kodeProvinsi = "NULL";
				postCode = "NULL";
				ketDueDate = "NULL";
				installmentAmt = 0;
				depositAmt = 0;
				penaltyAmt = 0;
				collectionFee = 0;
				totalAmt = 0;
				receiptNo = "NULL";
				minPayment = 0;
				maxPayment = 0;
				ketRepaymentDate = "NULL";
				notes = "";
				promiseCount = 0;
				promiseAvailable = 0;
				promiseRange = 0;
				phoneArea = "NULL";
				phoneNumber = "NULL";
				mobilePrefix = "NULL";
				mobileNo = "NULL";
				namaBarang = "NULL";
				ketLpkDate = "NULL";
				collectorCode = "NULL";
				status = "NULL";
				roww = firstSheet.getRow(i);
					try {
						orderId = roww.getCell(0).toString();
					} catch (Exception e) {
						orderId = "NULL";
					}
					try {
						customerCode = roww.getCell(1).toString();
					} catch (Exception e) {
						customerCode = "NULL";
					}
					try {
						contractNo = roww.getCell(2).toString();
					} catch (Exception e) {
						contractNo = "NULL";
					}
					try {
						installmentNo = Integer.valueOf(roww.getCell(3).toString());
					} catch (Exception e) {
						installmentNo = 0;
					}
					try {
						lpkNo = roww.getCell(4).toString();
					} catch (Exception e) {
						lpkNo = "NULL";
					}
					try {
						collectorCode = roww.getCell(6).toString();
					} catch (Exception e) {
						collectorCode = "NULL";
					}
					try {
						customerName = roww.getCell(8).toString();
					} catch (Exception e) {
						customerName = "NULL";
					}
					try {
						address = roww.getCell(9).toString();
					} catch (Exception e) {
						address = "NULL";
					}
					try {
						village = roww.getCell(10).toString();
					} catch (Exception e) {
						village = "NULL";
					}
					try {
						district = roww.getCell(11).toString();
					} catch (Exception e) {
						district = "NULL";
					}
					try {
						rt = roww.getCell(12).toString();
					} catch (Exception e) {
						rt = "NULL";
					}
					try {
						rw = roww.getCell(13).toString();
					} catch (Exception e) {
						rw = "NULL";
					}
					try {
						city = roww.getCell(14).toString();
					} catch (Exception e) {
						city = "NULL";
					}
					try {
						kodeProvinsi = roww.getCell(15).toString();
					} catch (Exception e) {
						kodeProvinsi = "NULL";
					}
					try {
						postCode = roww.getCell(16).toString();
					} catch (Exception e) {
						postCode = "NULL";
					}
					try {
						ketDueDate = roww.getCell(17).toString();
					} catch (Exception e) {
						ketDueDate = "NULL";
					}
					try {
						installmentAmt = Integer.valueOf(roww.getCell(18).toString());
					} catch (Exception e) {
						installmentAmt = 0;
					}
					try {
						depositAmt = Integer.valueOf(roww.getCell(19).toString());
					} catch (Exception e) {
						depositAmt = 0;
					}
					try {
						penaltyAmt = Integer.valueOf(roww.getCell(20).toString());
					} catch (Exception e) {
						penaltyAmt = 0;
					}
					try {
						collectionFee = Integer.valueOf(roww.getCell(21).toString());
					} catch (Exception e) {
						collectionFee = 0;
					}
					try {
						totalAmt = Integer.valueOf(roww.getCell(22).toString());
					} catch (Exception e) {
						totalAmt = 0;
					}
					try {
						receiptNo = roww.getCell(23).toString();
					} catch (Exception e) {
						receiptNo = "NULL";
					}
					try {
						minPayment = Integer.valueOf(roww.getCell(24).toString());
					} catch (Exception e) {
						minPayment = 0;
					}
					try {
						maxPayment = Integer.valueOf(roww.getCell(25).toString());
					} catch (Exception e) {
						maxPayment = 0;
					}
					try {
						ketRepaymentDate = roww.getCell(26).toString();
					} catch (Exception e) {
						ketRepaymentDate = "NULL";
					}
					try {
						notes = roww.getCell(27).toString();
					} catch (Exception e) {
						notes = "NULL";
					}
					try {
						promiseCount = Integer.valueOf(roww.getCell(28).toString());
					} catch (Exception e) {
						promiseCount = 0;
					}
					try {
						promiseAvailable = Integer.valueOf(roww.getCell(29).toString());
					} catch (Exception e) {
						promiseAvailable = 0;
					}
					try {
						promiseRange = Integer.valueOf(roww.getCell(30).toString());
					} catch (Exception e) {
						promiseRange = 0;
					}
					try {
						phoneArea = roww.getCell(31).toString();
					} catch (Exception e) {
						phoneArea = "NULL";
					}
					try {
						phoneNumber = roww.getCell(32).toString();
					} catch (Exception e) {
						phoneNumber = "NULL";
					}
					try {
						mobilePrefix = roww.getCell(33).toString();
					} catch (Exception e) {
						mobilePrefix = "NULL";
					}
					try {
						mobileNo = roww.getCell(34).toString();
					} catch (Exception e) {
						mobileNo = "NULL";
					}
					try {
						namaBarang = roww.getCell(35).toString();
					} catch (Exception e) {
						namaBarang = "NULL";
					}
					try {
						ketLpkDate = roww.getCell(5).toString();
					} catch (Exception e) {
						ketLpkDate = "NULL";
					}
					try {
						status = roww.getCell(7).toString();
					} catch (Exception e) {
						status = "NULL";
					}
					
					CollectionDetailDto cd = new CollectionDetailDto();
					cd.setOrderId(Integer.valueOf(orderId));
					cd.setCustomerCode(customerCode);
					cd.setContractNo(contractNo);
					cd.setInstallmentNo(installmentNo);
					cd.setLpkNo(lpkNo);
					cd.setKetLpkDate(ketLpkDate);
					cd.setCollectorCode(collectorCode);
					cd.setStatus(status);
					cd.setCustomerName(customerName);
					cd.setAddress(address);
					cd.setVillage(village);
					cd.setDistrict(district);
					cd.setRt(rt);
					cd.setRw(rw);
					cd.setCity(city);
					cd.setKodeProvinsi(kodeProvinsi);
					cd.setPostCode(postCode);
					cd.setKetDueDate(ketDueDate);
					cd.setInstallmentAmt(installmentAmt);
					cd.setDepositAmt(depositAmt);
					cd.setPenaltyAmt(penaltyAmt);
					cd.setCollectionFee(collectionFee);
					cd.setTotalAmt(totalAmt);
					cd.setReceiptNo(receiptNo);
					cd.setMinPayment(minPayment);
					cd.setMaxPayment(maxPayment);
					cd.setKetRepaymentDate(ketRepaymentDate);
					cd.setNotes(notes);
					cd.setPromiseCount(promiseCount);
					cd.setPromiseAvailable(promiseAvailable);
					cd.setPromiseRange(promiseRange);
					cd.setPhoneArea(phoneArea);
					cd.setPhoneNumber(phoneNumber);
					cd.setMobilePrefix(mobilePrefix);
					cd.setMobileNo(mobileNo);
					cd.setNamaBarang(namaBarang);
					listCollectionDetailDto.add(cd);
			}
			
			if (listCollectionDetailDto.size() != 0) {
				String ketValOrderId;
				String ketValcustomerCode;
				String ketValcontractNo;
				String ketValinstallmentNo;
				String ketVallpkNo;
				String ketValcustomerName;
				String ketValaddress;
				String ketValvillage;
				String ketValdistrict;
				String ketValrt;
				String ketValrw;
				String ketValcity;
				String ketValkodeProvinsi;
				String ketValpostCode;
				String ketValketDueDate;
				String ketValinstallmentAmt;
				String ketValdepositAmt;
				String ketValpenaltyAmt;
				String ketValcollectionFee;
				String ketValtotalAmt;
				String ketValreceiptNo;
				String ketValminPayment;
				String ketValmaxPayment;
				String ketValketRepaymentDate;
				String ketValnotes;
				String ketValpromiseCount;
				String ketValpromiseAvailable;
				String ketValpromiseRange;
				String ketValphoneArea;
				String ketValphoneNumber;
				String ketValmobilePrefix;
				String ketValmobileNo;
				String ketValnamaBarang;
				String ketValketLpkDate;
				String ketValcollectorCode;
				String ketValstatus;
				
				boolean trueOrderId;
				boolean trueCustomerCode;
				boolean trueContractNo;
				boolean trueInstallmentNo;
				boolean trueLpkNo;
				boolean trueCustomerName;
				boolean trueAddress;
				boolean trueVillage;
				boolean trueDistrict;
				boolean trueRt;
				boolean trueRw;
				boolean trueCity;
				boolean trueKodeProvinsi;
				boolean truePostCode;
				boolean trueKetDueDate;
				boolean trueInstallmentAmt;
				boolean trueDepositAmt;
				boolean truePenaltyAmt;
				boolean trueCollectionFee;
				boolean trueTotalAmt;
				boolean trueReceiptNo;
				boolean trueMinPayment;
				boolean trueMaxPayment;
				boolean trueKetRepaymentDate;
				boolean trueNotes;
				boolean truePromiseCount;
				boolean truePromiseAvailable;
				boolean truePromiseRange;
				boolean truePhoneArea;
				boolean truePhoneNumber;
				boolean trueMobilePrefix;
				boolean trueMobileNo;
				boolean trueNamaBarang;
				boolean trueKetLpkDate;
				boolean trueCollectorCode;
				boolean trueStatus;
				for (int i = 0; i < listCollectionDetailDto.size(); i++) {
					ketValOrderId = "";
					ketValcustomerCode = "";
					ketValcontractNo = "";
					ketValinstallmentNo = "";
					ketVallpkNo = "";
					ketValcustomerName = "";
					ketValaddress = "";
					ketValvillage = "";
					ketValdistrict = "";
					ketValrt = "";
					ketValrw = "";
					ketValcity = "";
					ketValkodeProvinsi = "";
					ketValpostCode = "";
					ketValketDueDate = "";
					ketValinstallmentAmt = "";
					ketValdepositAmt = "";
					ketValpenaltyAmt = "";
					ketValcollectionFee = "";
					ketValtotalAmt = "";
					ketValreceiptNo = "";
					ketValminPayment = "";
					ketValmaxPayment = "";
					ketValketRepaymentDate = "";
					ketValnotes = "";
					ketValpromiseCount = "";
					ketValpromiseAvailable = "";
					ketValpromiseRange = "";
					ketValphoneArea = "";
					ketValphoneNumber = "";
					ketValmobilePrefix = "";
					ketValmobileNo = "";
					ketValnamaBarang = "";
					ketValketLpkDate = "";
					ketValcollectorCode = "";
					ketValstatus = "";
					
					trueOrderId = false;
					trueCustomerCode = false;
					trueContractNo = false;
					trueInstallmentNo = false;
					trueLpkNo = false;
					trueCustomerName = false;
					trueAddress = false;
					trueVillage = false;
					trueDistrict = false;
					trueRt = false;
					trueRw = false;
					trueCity = false;
					trueKodeProvinsi = false;
					truePostCode = false;
					trueKetDueDate = false;
					trueInstallmentAmt = false;
					trueDepositAmt = false;
					truePenaltyAmt = false;
					trueCollectionFee = false;
					trueTotalAmt = false;
					trueReceiptNo = false;
					trueMinPayment = false;
					trueMaxPayment = false;
					trueKetRepaymentDate = false;
					trueNotes = false;
					truePromiseCount = false;
					truePromiseAvailable = false;
					truePromiseRange = false;
					truePhoneArea = false;
					truePhoneNumber = false;
					trueMobilePrefix = false;
					trueMobileNo = false;
					trueNamaBarang = false;
					trueKetLpkDate = false;
					trueCollectorCode = false;
					trueStatus = false;
					//================Validasi kolom null
					if (listCollectionDetailDto.get(i).getOrderId() != 0) {
						trueOrderId = true;
					}else {
						ketValOrderId = "Order ID kosong";
					}
					if (listCollectionDetailDto.get(i).getCustomerCode() != "NULL") {
						if (listCollectionDetailDto.get(i).getCustomerCode().length() <= 20) {
							trueCustomerCode = true;
						}else {
							ketValcustomerCode = "Customer code lebih dari 20 karakter";
						}
					}else {
						ketValcustomerCode = "Customer code kosong";
					}
					if (listCollectionDetailDto.get(i).getContractNo() != "NULL" ) {
						if (listCollectionDetailDto.get(i).getContractNo().length() <= 15) {
							trueContractNo = true;
						} else {
							ketValcontractNo = "Contract no lebih dari 15 karakter";
						}
					}else {
						ketValcontractNo = "Contract no kosong";
					}
					if (listCollectionDetailDto.get(i).getInstallmentNo() != 0) {
						trueInstallmentNo = true;
					}else {
						ketValinstallmentNo = "Installment no kosong";
					}
					if (listCollectionDetailDto.get(i).getLpkNo() != "NULL") {
						if (listCollectionDetailDto.get(i).getLpkNo().length() <= 15) {
							trueLpkNo = true;
						} else {
							ketVallpkNo = "LPK no lebih dari 15 karakter";
						}
					}else {
						ketVallpkNo = "LPK no kosong";
					}
					if (listCollectionDetailDto.get(i).getKetLpkDate() != "NULL") {
						trueKetLpkDate = true;
					}else {
						ketValketLpkDate = "LPK date kosong";
					}
					if (listCollectionDetailDto.get(i).getCollectorCode() != "NULL") {
						if (listCollectionDetailDto.get(i).getCollectorCode().length() <= 12) {
							trueCollectorCode = true;
						} else {
							ketValcollectorCode = "Collector code lebih dari 12 karakter";
						}
					}else {
						ketValcollectorCode = "Collector code kosong";
					}
					if (listCollectionDetailDto.get(i).getStatus() != "NULL") {
						if (listCollectionDetailDto.get(i).getStatus().length() <= 3) {
							trueStatus = true;
						}else {
							ketValstatus = "Status lebih dari 3 karakter";
						}
					}else {
						ketValstatus = "Status kosong";
					}
					if (listCollectionDetailDto.get(i).getCustomerName() != "NULL") {
						if (listCollectionDetailDto.get(i).getCustomerName().length() <= 100) {
							trueCustomerName = true;
						} else {
							ketValcustomerName = "Customer name lebih dari 100 karakter";
						}
					}else {
						ketValcustomerName = "Customer name kosong";
					}
					if (listCollectionDetailDto.get(i).getAddress() != "NULL") {
						if (listCollectionDetailDto.get(i).getAddress().length() <= 100) {
							trueAddress = true;
						} else {
							ketValaddress = "Address lebih dari 100 krakter";
						}
					}else {
						ketValaddress = "Address kosong";
					}
					if (listCollectionDetailDto.get(i).getVillage() != "NULL") {
						if (listCollectionDetailDto.get(i).getVillage().length() <= 100) {
							trueVillage = true;
						} else {
							ketValvillage = "Village lebih dari 100 karakter";
						}
					}else {
						ketValvillage = "Village kosong";
					}
					if (listCollectionDetailDto.get(i).getDistrict() != "NULL") {
						if (listCollectionDetailDto.get(i).getDistrict().length() <= 100) {
							trueDistrict = true;
						} else {
							ketValdistrict = "District lebih dari 100 karakter";
						}
					}else {
						ketValdistrict = "District kosong";
					}
					if (listCollectionDetailDto.get(i).getRt() != "NULL") {
						if (listCollectionDetailDto.get(i).getRt().length() <= 3) {
							trueRt = true;
						}else{
							ketValrt = "RT lebih dari 3 karakter";
						}
					}else {
						ketValrt = "RT kosong";
					}
					if (listCollectionDetailDto.get(i).getRw() != "NULL") {
						if (listCollectionDetailDto.get(i).getRw().length() <= 3) {
							trueRw = true;
						} else {
							ketValrw = "RW lebih dari 3 karakter";
						}
						
					}else {
						ketValrw = "RW kosong";
					}
					if (listCollectionDetailDto.get(i).getCity() != "NULL") {
						if (listCollectionDetailDto.get(i).getCity().length() <= 100) {
							trueCity = true;
						} else {
							ketValcity = "City lebih dari 100 karakter";
						}
					}else {
						ketValcity = "City kosong";
					}
					if (listCollectionDetailDto.get(i).getKodeProvinsi() != "NULL") {
						if (listCollectionDetailDto.get(i).getKodeProvinsi() .length() <= 5) {
							trueKodeProvinsi = true;
						} else {
							ketValkodeProvinsi = "Kode lebih dari 5 karakter";
						}
					}else {
						ketValkodeProvinsi = "Kode provinsi kosong";
					}
					if (listCollectionDetailDto.get(i).getPostCode() != "NULL") {
						if (listCollectionDetailDto.get(i).getPostCode().length() <= 10) {
							truePostCode = true;
						} else {
							ketValpostCode = "Post code lebih dari 10 karakter";
						}
					}else {
						ketValpostCode = "Post code kosong";
					}
					if (listCollectionDetailDto.get(i).getKetDueDate() != "NULL") {
						trueKetDueDate = true;
					}else {
						ketValketDueDate = "Due date kosong";
					}
					if (listCollectionDetailDto.get(i).getInstallmentAmt() != 0) {
						trueInstallmentAmt = true;
					}else {
						ketValinstallmentAmt = "Installment amt kosong";
					}
					if (listCollectionDetailDto.get(i).getDepositAmt() != 0) {
						trueDepositAmt = true;
					}else {
						ketValdepositAmt = "Deposit amt kosong";
					}
					if (listCollectionDetailDto.get(i).getPenaltyAmt() != 0) {
						truePenaltyAmt = true;
					}else {
						ketValpenaltyAmt = "Penalty amt kosong";
					}
					if (listCollectionDetailDto.get(i).getCollectionFee() != 0) {
						trueCollectionFee = true;
					}else {
						ketValcollectionFee = "Collection amt kosong";
					}
					if (listCollectionDetailDto.get(i).getTotalAmt() != 0) {
						trueTotalAmt = true;
					}else {
						ketValtotalAmt = "Total amt kosong";
					}
					if (listCollectionDetailDto.get(i).getReceiptNo() != "NULL") {
						if (listCollectionDetailDto.get(i).getReceiptNo().length() <= 15) {
							trueReceiptNo = true;
						} else {
							ketValreceiptNo = "Receipt lebih dari 15 karakter";
						}
					}else {
						ketValreceiptNo = "Receipt no kosong";
					}
					if (listCollectionDetailDto.get(i).getMinPayment() != 0) {
						trueMinPayment = true;
					}else {
						ketValminPayment = "Min Payment kosong";
					}
					if (listCollectionDetailDto.get(i).getMaxPayment() != 0) {
						trueMaxPayment = true;
					}else {
						ketValmaxPayment = "Max Payment kosong";
					}
					if (listCollectionDetailDto.get(i).getKetRepaymentDate() != "NULL") {
						trueKetRepaymentDate = true;
					}else {
						ketValketRepaymentDate = "Repayment date kosong";
					}
					if (listCollectionDetailDto.get(i).getNotes() != "NULL") {
						trueNotes = true;
					}else {
						ketValnotes = "Notes kosong";
					}
					if (listCollectionDetailDto.get(i).getPromiseCount() != 0) {
						truePromiseCount = true;
					}else {
						ketValpromiseCount = "Promise count kosong";
					}
					if (listCollectionDetailDto.get(i).getPromiseAvailable() != 0) {
						truePromiseAvailable = true;
					}else {
						ketValpromiseAvailable = "Promise available kosong";
					}
					if (listCollectionDetailDto.get(i).getPromiseRange() != 0) {
						truePromiseRange = true;
					}else {
						ketValpromiseRange = "Promise range kosong";
					}
					if (listCollectionDetailDto.get(i).getPhoneArea() != "NULL") {
						if (listCollectionDetailDto.get(i).getPhoneArea().length() <= 4) {
							truePhoneArea = true;
						} else {
							ketValphoneArea = "Phone area lebih dari 4 karakter";
						}
					}else {
						ketValphoneArea = "Phone area kosong";
					}
					if (listCollectionDetailDto.get(i).getPhoneNumber() != "NULL") {
						if (listCollectionDetailDto.get(i).getPhoneNumber().length() <= 12) {
							truePhoneNumber = true;
						} else {
							ketValphoneNumber = "Phone number lebih dari 12 karakter";
						}
					}else {
						ketValphoneNumber = "Phone number kosong";
					}
					if (listCollectionDetailDto.get(i).getMobilePrefix() != "NULL") {
						if (listCollectionDetailDto.get(i).getMobilePrefix().length() <= 4) {
							trueMobilePrefix = true;
						} else {
							ketValmobilePrefix = "Mobile prefix lebih dari 4 karakter";
						}
					}else {
						ketValmobilePrefix = "Mobile prefix kosong";
					}
					if (listCollectionDetailDto.get(i).getMobileNo() != "NULL") {
						if (listCollectionDetailDto.get(i).getMobileNo().length() <= 12) {
							trueMobileNo = true;
						} else {
							ketValmobileNo = "Mobile NO lebih dari 12 karakter";
						}
					}else {
						ketValmobileNo = "Mobile NO kosong";
					}
					if (listCollectionDetailDto.get(i).getNamaBarang() != "NULL") {
						trueNamaBarang = true;
					}else {
						ketValnamaBarang = "Nama barang kosong";
					}
					//================================
					
					//==================== Validasi inputan valid apa tidak
					if (trueOrderId == true && 
							trueCustomerCode == true && 
							trueContractNo == true && 
							trueInstallmentNo == true &&
							trueLpkNo == true &&
							trueCustomerName == true &&
							trueAddress == true &&
							trueVillage == true &&
							trueDistrict == true &&
							trueRt == true &&
							trueRw == true &&
							trueCity == true &&
							trueKodeProvinsi == true &&
							truePostCode == true &&
							trueKetDueDate == true &&
							trueInstallmentAmt == true &&
							trueDepositAmt == true &&
							truePenaltyAmt == true &&
							trueCollectionFee == true &&
							trueTotalAmt == true &&
							trueReceiptNo == true &&
							trueMinPayment == true &&
							trueMaxPayment == true &&
							trueKetRepaymentDate == true &&
							trueNotes == true &&
							truePromiseCount == true &&
							truePromiseAvailable == true &&
							truePromiseRange == true &&
							truePhoneArea == true &&
							truePhoneNumber == true &&
							trueMobilePrefix == true &&
							trueMobileNo == true &&
							trueNamaBarang == true &&
							trueKetLpkDate == true &&
							trueCollectorCode == true &&
							trueStatus == true	) {
						CollectionDetailDto cdd = new CollectionDetailDto();
						cdd.setOrderId(listCollectionDetailDto.get(i).getOrderId());
						cdd.setCustomerCode(listCollectionDetailDto.get(i).getCustomerCode());
						cdd.setContractNo(listCollectionDetailDto.get(i).getContractNo());
						cdd.setInstallmentNo(listCollectionDetailDto.get(i).getInstallmentNo());
						cdd.setLpkNo(listCollectionDetailDto.get(i).getLpkNo());
						cdd.setKetLpkDate(listCollectionDetailDto.get(i).getKetLpkDate());
						cdd.setCollectorCode(listCollectionDetailDto.get(i).getCollectorCode());
						cdd.setStatus(listCollectionDetailDto.get(i).getStatus());
						cdd.setCustomerName(listCollectionDetailDto.get(i).getCustomerName());
						cdd.setAddress(listCollectionDetailDto.get(i).getAddress());
						cdd.setVillage(listCollectionDetailDto.get(i).getVillage());
						cdd.setDistrict(listCollectionDetailDto.get(i).getDistrict());
						cdd.setRt(listCollectionDetailDto.get(i).getRt());
						cdd.setRw(listCollectionDetailDto.get(i).getRw());
						cdd.setCity(listCollectionDetailDto.get(i).getCity());
						cdd.setKodeProvinsi(listCollectionDetailDto.get(i).getKodeProvinsi());
						cdd.setPostCode(listCollectionDetailDto.get(i).getPostCode());
						cdd.setKetDueDate(listCollectionDetailDto.get(i).getKetDueDate());
						cdd.setInstallmentAmt(listCollectionDetailDto.get(i).getInstallmentAmt());
						cdd.setDepositAmt(listCollectionDetailDto.get(i).getDepositAmt());
						cdd.setPenaltyAmt(listCollectionDetailDto.get(i).getPenaltyAmt());
						cdd.setCollectionFee(listCollectionDetailDto.get(i).getCollectionFee());
						cdd.setTotalAmt(listCollectionDetailDto.get(i).getTotalAmt());
						cdd.setReceiptNo(listCollectionDetailDto.get(i).getReceiptNo());
						cdd.setMinPayment(listCollectionDetailDto.get(i).getMinPayment());
						cdd.setMaxPayment(listCollectionDetailDto.get(i).getMaxPayment());
						cdd.setKetRepaymentDate(listCollectionDetailDto.get(i).getKetRepaymentDate());
						cdd.setNotes(listCollectionDetailDto.get(i).getNotes());
						cdd.setPromiseCount(listCollectionDetailDto.get(i).getPromiseCount());
						cdd.setPromiseAvailable(listCollectionDetailDto.get(i).getPromiseAvailable());
						cdd.setPromiseRange(listCollectionDetailDto.get(i).getPromiseRange());
						cdd.setPhoneArea(listCollectionDetailDto.get(i).getPhoneArea());
						cdd.setPhoneNumber(listCollectionDetailDto.get(i).getPhoneNumber());
						cdd.setPhoneArea(listCollectionDetailDto.get(i).getPhoneArea());
						cdd.setMobilePrefix(listCollectionDetailDto.get(i).getMobilePrefix());
						cdd.setMobileNo(listCollectionDetailDto.get(i).getMobileNo());
						cdd.setNamaBarang(listCollectionDetailDto.get(i).getNamaBarang());
						temListCollectionDetailDtoTrue.add(cdd);
					} else {
						CollectionDetailDto cdd2 = new CollectionDetailDto();
						cdd2.setOrderId(listCollectionDetailDto.get(i).getOrderId());
						cdd2.setCustomerCode(listCollectionDetailDto.get(i).getCustomerCode());
						cdd2.setContractNo(listCollectionDetailDto.get(i).getContractNo());
						cdd2.setInstallmentNo(listCollectionDetailDto.get(i).getInstallmentNo());
						cdd2.setLpkNo(listCollectionDetailDto.get(i).getLpkNo());
						cdd2.setKetLpkDate(listCollectionDetailDto.get(i).getKetLpkDate());
						cdd2.setCollectorCode(listCollectionDetailDto.get(i).getCollectorCode());
						cdd2.setStatus(listCollectionDetailDto.get(i).getStatus());
						cdd2.setCustomerName(listCollectionDetailDto.get(i).getCustomerName());
						cdd2.setAddress(listCollectionDetailDto.get(i).getAddress());
						cdd2.setVillage(listCollectionDetailDto.get(i).getVillage());
						cdd2.setDistrict(listCollectionDetailDto.get(i).getDistrict());
						cdd2.setRt(listCollectionDetailDto.get(i).getRt());
						cdd2.setRw(listCollectionDetailDto.get(i).getRw());
						cdd2.setCity(listCollectionDetailDto.get(i).getCity());
						cdd2.setKodeProvinsi(listCollectionDetailDto.get(i).getKodeProvinsi());
						cdd2.setPostCode(listCollectionDetailDto.get(i).getPostCode());
						cdd2.setKetDueDate(listCollectionDetailDto.get(i).getKetDueDate());
						cdd2.setInstallmentAmt(listCollectionDetailDto.get(i).getInstallmentAmt());
						cdd2.setDepositAmt(listCollectionDetailDto.get(i).getDepositAmt());
						cdd2.setPenaltyAmt(listCollectionDetailDto.get(i).getPenaltyAmt());
						cdd2.setCollectionFee(listCollectionDetailDto.get(i).getCollectionFee());
						cdd2.setTotalAmt(listCollectionDetailDto.get(i).getTotalAmt());
						cdd2.setReceiptNo(listCollectionDetailDto.get(i).getReceiptNo());
						cdd2.setMinPayment(listCollectionDetailDto.get(i).getMinPayment());
						cdd2.setMaxPayment(listCollectionDetailDto.get(i).getMaxPayment());
						cdd2.setKetRepaymentDate(listCollectionDetailDto.get(i).getKetRepaymentDate());
						cdd2.setNotes(listCollectionDetailDto.get(i).getNotes());
						cdd2.setPromiseCount(listCollectionDetailDto.get(i).getPromiseCount());
						cdd2.setPromiseAvailable(listCollectionDetailDto.get(i).getPromiseAvailable());
						cdd2.setPromiseRange(listCollectionDetailDto.get(i).getPromiseRange());
						cdd2.setPhoneArea(listCollectionDetailDto.get(i).getPhoneArea());
						cdd2.setPhoneNumber(listCollectionDetailDto.get(i).getPhoneNumber());
						cdd2.setMobilePrefix(listCollectionDetailDto.get(i).getMobilePrefix());
						cdd2.setMobileNo(listCollectionDetailDto.get(i).getMobileNo());
						cdd2.setNamaBarang(listCollectionDetailDto.get(i).getNamaBarang());
						String temKetValsidasiFalse = "";
						if (trueOrderId == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValOrderId+", ";
						}
						if (trueCustomerCode == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValcustomerCode+", ";
						}
						if (trueContractNo == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValcontractNo+", ";
						}
						if (trueInstallmentNo == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValinstallmentNo+", ";
						}
						if (trueLpkNo == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketVallpkNo+", ";
						}
						if (trueCustomerName == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValcustomerName+", ";
						}
						if (trueAddress == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValaddress+", ";
						}
						if (trueVillage == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValvillage+", ";
						}
						if (trueDistrict == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValdistrict+", ";
						}
						if (trueRt == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValrt+", ";
						}
						if (trueRw == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValrw+", ";
						}
						if (trueCity == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValcity+", ";
						}
						if (trueKodeProvinsi == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValkodeProvinsi+", ";
						}
						if (truePostCode == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValpostCode+", ";
						}
						if (trueKetDueDate == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValketDueDate+", ";
						}
						if (trueInstallmentAmt == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValinstallmentAmt+", ";
						}
						if (trueDepositAmt == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValdepositAmt+", ";
						}
						if (truePenaltyAmt == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValpenaltyAmt+", ";
						}
						if (trueCollectionFee == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValcollectionFee+", ";
						}
						if (trueTotalAmt == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValtotalAmt+", ";
						}
						if (trueReceiptNo == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValreceiptNo+", ";
						}
						if (trueMinPayment == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValminPayment+", ";
						}
						if (trueMaxPayment == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValmaxPayment+", ";
						}
						if (trueKetRepaymentDate == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValketRepaymentDate+", ";
						}
						if (trueNotes == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValnotes+", ";
						}
						if (truePromiseCount == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValpromiseCount+", ";
						}
						if (truePromiseAvailable == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValpromiseAvailable+", ";
						}
						if (truePromiseRange == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValpromiseRange+", ";
						}
						if (truePhoneArea == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValphoneArea+", ";
						}
						if (truePhoneNumber == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValphoneNumber+", ";
						}
						if (trueMobilePrefix == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValmobilePrefix+", ";
						}
						if (trueMobileNo == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValmobileNo+", ";
						}
						if (trueNamaBarang == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValnamaBarang+", ";
						}
						if (trueKetLpkDate == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValketLpkDate+", ";
						}
						if (trueCollectorCode == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValcollectorCode+", ";
						}
						if (trueStatus == false) {
							temKetValsidasiFalse = temKetValsidasiFalse+ketValstatus+", ";
						}
								 						
						cdd2.setKetValidasi(temKetValsidasiFalse);
						temListCollectionDetailDtoFalse.add(cdd2);
					}
					
				}
				listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
				if (temListCollectionDetailDtoTrue.size() != 0) {
					listCollectionDetailDto = temListCollectionDetailDtoTrue;
					//setStsShowListTrue(true);
				}
				if (temListCollectionDetailDtoFalse.size() != 0) {
					listCollectionDetailDtoFalse = new ArrayList<CollectionDetailDto>();
					listCollectionDetailDtoFalse = temListCollectionDetailDtoFalse;
					setStsShowListFalse(true);
				}
				
				//=============================
			} else {
				//setStsShowListTrue(false);
				setStsShowListFalse(false);
				listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
				Messagebox.show("Data excel are empty !","MESSAGE",null,null,null);
				fileNameUpload = "";
				media = null;
			}
			
		} else {
			Messagebox.show("Upload file excel terlebih dahulu !", "MESSAGE", null, null, null);
		}
	}
	
	@Command("importToDb")
	@NotifyChange("listCollectionDetailDtoCheked")
	public void importToDb() {
		Messagebox.show("Apakah anda yakin ?", "Message",
				new Button[] { Button.YES, Button.NO }, Messagebox.QUESTION,
				Button.NO, new EventListener<Messagebox.ClickEvent>() {
					@Override
					@NotifyChange({"listCollectionDetailDtoCheked","stsShowListImport","listCollectionDetailDto","listCollectionDetailDtoFalse"})
					public void onEvent(ClickEvent event) throws Exception {
						if (Messagebox.ON_YES.equals(event.getName())) {
							List<CollectionHeaderDto> listCollHeader = new ArrayList<CollectionHeaderDto>();
							for (int i = 0; i < listCollectionDetailDtoCheked.size(); i++) {
								if (listCollHeader.size() == 0) {
									CollectionHeaderDto collHeader = new CollectionHeaderDto();
									collHeader.setOrderId(listCollectionDetailDtoCheked.get(i).getOrderId());
									collHeader.setLpkNo(listCollectionDetailDtoCheked.get(i).getLpkNo());
									collHeader.setKetLpkDate(listCollectionDetailDtoCheked.get(i).getKetLpkDate());
									collHeader.setCollectorCode(listCollectionDetailDtoCheked.get(i).getCollectorCode());
									collHeader.setStatus(listCollectionDetailDtoCheked.get(i).getStatus());
									listCollHeader.add(collHeader);
								} else {
									boolean sama = false;
									for (int j = 0; j < listCollHeader.size(); j++) {
										if (listCollectionDetailDtoCheked.get(i).getOrderId() == listCollHeader.get(j).getOrderId()) {
											sama = true;
										}
									}
									if (sama == false) {
										CollectionHeaderDto collHeader = new CollectionHeaderDto();
										collHeader.setOrderId(listCollectionDetailDtoCheked.get(i).getOrderId());
										collHeader.setLpkNo(listCollectionDetailDtoCheked.get(i).getLpkNo());
										collHeader.setKetLpkDate(listCollectionDetailDtoCheked.get(i).getKetLpkDate());
										collHeader.setCollectorCode(listCollectionDetailDtoCheked.get(i).getCollectorCode());
										collHeader.setStatus(listCollectionDetailDtoCheked.get(i).getStatus());
										listCollHeader.add(collHeader);
									}
								}
							}
							boolean sama = false;
							for (int i = 0; i < listCollectionDetailDtoCheked.size(); i++) {
								CollectionDetailDto colDet = new CollectionDetailDto();
								colDet.setCustomerCode(listCollectionDetailDtoCheked.get(i).getCollectorCode());
								colDet.setInstallmentNo(listCollectionDetailDtoCheked.get(i).getInstallmentNo());
								colDet.setContractNo(listCollectionDetailDtoCheked.get(i).getContractNo());
								for (int j = 0; j < listCollectionDetailDtoCheked.size(); j++) {
									if (colDet.getCustomerCode().equals(listCollectionDetailDtoCheked.get(j).getCustomerCode()) &&
											colDet.getInstallmentNo() == listCollectionDetailDtoCheked.get(j).getInstallmentNo() &&
											colDet.getContractNo().equals(listCollectionDetailDtoCheked.get(j).getContractNo())) {
										sama = true;
									}
								}
								
							}
							if (sama != true) {
								for (int i = 0; i < listCollHeader.size(); i++) {
									collectionHeaderSvc
											.insertCollectionHeaderInUploadManual(
													String.valueOf(listCollHeader
															.get(i)
															.getOrderId()),
													listCollHeader.get(i)
															.getLpkNo(),
													listCollHeader.get(i)
															.getKetLpkDate(),
													listCollHeader.get(i)
															.getCollectorCode(),
													listCollHeader.get(i)
															.getStatus());
								}
								for (int i = 0; i < listCollectionDetailDtoCheked
										.size(); i++) {
									collectionDetailSvc.insertCollectionDetailInUploadManual(
													listCollectionDetailDtoCheked
															.get(i)
															.getOrderId(),
													listCollectionDetailDtoCheked
															.get(i)
															.getCustomerCode(),
													listCollectionDetailDtoCheked
															.get(i)
															.getContractNo(),
													listCollectionDetailDtoCheked
															.get(i)
															.getInstallmentNo(),
													listCollectionDetailDtoCheked
															.get(i).getLpkNo(),
													listCollectionDetailDtoCheked
															.get(i)
															.getCustomerName(),
													listCollectionDetailDtoCheked
															.get(i)
															.getAddress(),
													listCollectionDetailDtoCheked
															.get(i)
															.getVillage(),
													listCollectionDetailDtoCheked
															.get(i)
															.getDistrict(),
													listCollectionDetailDtoCheked
															.get(i).getRt(),
													listCollectionDetailDtoCheked
															.get(i).getRw(),
													listCollectionDetailDtoCheked
															.get(i).getCity(),
													listCollectionDetailDtoCheked
															.get(i)
															.getKodeProvinsi(),
													listCollectionDetailDtoCheked
															.get(i)
															.getPostCode(),
													listCollectionDetailDtoCheked
															.get(i)
															.getKetDueDate(),
													listCollectionDetailDtoCheked
															.get(i)
															.getInstallmentAmt(),
													listCollectionDetailDtoCheked
															.get(i)
															.getDepositAmt(),
													listCollectionDetailDtoCheked
															.get(i)
															.getPenaltyAmt(),
													listCollectionDetailDtoCheked
															.get(i)
															.getCollectionFee(),
													listCollectionDetailDtoCheked
															.get(i)
															.getTotalAmt(),
													listCollectionDetailDtoCheked
															.get(i)
															.getReceiptNo(),
													listCollectionDetailDtoCheked
															.get(i)
															.getMinPayment(),
													listCollectionDetailDtoCheked
															.get(i)
															.getMaxPayment(),
													listCollectionDetailDtoCheked
															.get(i)
															.getKetRepaymentDate(),
													listCollectionDetailDtoCheked
															.get(i).getNotes(),
													listCollectionDetailDtoCheked
															.get(i)
															.getPromiseCount(),
													listCollectionDetailDtoCheked
															.get(i)
															.getPromiseAvailable(),
													listCollectionDetailDtoCheked
															.get(i)
															.getPromiseRange(),
													listCollectionDetailDtoCheked
															.get(i)
															.getPhoneArea(),
													listCollectionDetailDtoCheked
															.get(i)
															.getPhoneNumber(),
													listCollectionDetailDtoCheked
															.get(i)
															.getMobilePrefix(),
													listCollectionDetailDtoCheked
															.get(i)
															.getMobileNo(),
													listCollectionDetailDtoCheked
															.get(i)
															.getNamaBarang());
								}
							} else {
								Messagebox
										.show("Tidak dapat melakukan impor,ada data yang duplikat antara customer code, contract no dan instalment no !",
												"MESSAGE", null, null, null);
							}
							listCollectionDetailDtoFalse = new ArrayList<CollectionDetailDto>();
							listCollectionDetailDtoCheked = new ArrayList<CollectionDetailDto>();
							listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
							fileNameUpload = "";
							setStsShowListImport(false);
							setStsShowListFalse(false);
							BindUtils.postNotifyChange(null, null,
									UploadHandlingVmd.this,
									"fileNameUpload");
							BindUtils.postNotifyChange(null, null,
									UploadHandlingVmd.this,
									"listCollectionDetailDtoFalse");
							BindUtils.postNotifyChange(null, null,
									UploadHandlingVmd.this,
									"listCollectionDetailDtoCheked");
							BindUtils.postNotifyChange(null, null,
									UploadHandlingVmd.this,
									"listCollectionDetailDto");
							BindUtils.postNotifyChange(null, null,
									UploadHandlingVmd.this,
									"stsShowListImport");
							BindUtils.postNotifyChange(null, null,
									UploadHandlingVmd.this,
									"stsShowListFalse");
							Messagebox.show("Import data berhasil !", "MESSAGE", null, null, null);
					
						}
					}
				});
	}

}
