package vmd;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mobile.Notification;

import org.modelmapper.internal.asm.tree.JumpInsnNode;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Li;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;

import com.mysql.fabric.xmlrpc.base.Array;

import pagevmd.NavigationVmd;
import service.AksesRegionCabangSvc;
import service.CabangMstSvc;
import service.CollectionDetailSvc;
import service.CollectionHeaderSvc;
import service.ManualAssignSvc;
import service.MasterRegionalSvc;
import service.UserProfileSvc;
import dto.AksesRegionCabangDto;
import dto.CabangMstDto;
import dto.CollectionDetailDto;
import dto.ManualAssignDto;
import dto.MasterRegionalDto;
import dto.MobileTrackingDto;
import dto.ReassignCollector;
import dto.UserProfileDto;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class SnpManualAassignVmd extends NavigationVmd{
	
	@WireVariable
	private MasterRegionalSvc masterRegionalSvc;
	
	@WireVariable
	private CabangMstSvc cabangMstSvc;
	
	@WireVariable
	private UserProfileSvc userProfileSvc;
	
	@WireVariable
	private ManualAssignSvc manualAssignSvc;
	
	@WireVariable
	private CollectionDetailSvc collectionDetailSvc;
	
	@WireVariable
	private CollectionHeaderSvc collectionHeaderSvc;
	
	@WireVariable
	private AksesRegionCabangSvc aksesRegionCabangSvc;
	
	private boolean regional = false;
	private boolean branch = false;
	private boolean colectorOld = false;
	private boolean colectorNew = false;
	private boolean popUpPesan = false;
	private boolean visibleDetail = false;
	private Date dateAwal=null;
	private Date dateNow=null;
	private Date dateAkhir=null;
	private int page=10;
	private int pageRegional = 10;
	private int pageCabang = 10;
	private int pageKolektor = 10;
	private int pageDetail = 10;
	private String search="";
	private String keySearchregional = "";
	private String keySearchCabang = "";
	private String keySearchKolektor = "";
	private String keySearchAssignTo = "";
	private String stringArrayRegional = "";
	private String stringArrayCabang = "";
	private String stringArrayKolektor = "";
	private String notesValue = "";
	
	private List<ManualAssignDto> checkAssign = new ArrayList<ManualAssignDto>();
	
	private List<ManualAssignDto> listManualAssign = new ArrayList<ManualAssignDto>();
	private List<ManualAssignDto> listManualAssignDetail = new ArrayList<ManualAssignDto>();
	private ManualAssignDto manualAssignDto = new ManualAssignDto();
	private List<MasterRegionalDto> listMasterRegionalDto = new ArrayList<MasterRegionalDto>();
	private MasterRegionalDto masterRegionalDto = new MasterRegionalDto();
	private UserProfileDto userProfileDtoSession;
	private List<CabangMstDto> listCabangMstDto = new ArrayList<CabangMstDto>();
	private CabangMstDto cabangMstDto = new CabangMstDto();
	private List<UserProfileDto> listUserProfileDto = new ArrayList<UserProfileDto>();
	private UserProfileDto userProfileDto = new UserProfileDto();
	private List<UserProfileDto> listUserAssign = new ArrayList<UserProfileDto>();
	private UserProfileDto userAssignDto = new UserProfileDto();
	private UserProfileDto upSession = new UserProfileDto();
	private List<MasterRegionalDto> listAksesRegional = new ArrayList<MasterRegionalDto>();
	private List<CabangMstDto> listAksesCabang = new ArrayList<CabangMstDto>();
	private ManualAssignDto manualAssignDtoSelected = new ManualAssignDto();
	private String keySearchDetail = "";
	
	
	
	public Date getDateNow() {
		return dateNow;
	}
	public void setDateNow(Date dateNow) {
		this.dateNow = dateNow;
	}
	public String getNotesValue() {
		return notesValue;
	}
	public void setNotesValue(String notesValue) {
		this.notesValue = notesValue;
	}
	public String getKeySearchDetail() {
		return keySearchDetail;
	}
	public void setKeySearchDetail(String keySearchDetail) {
		this.keySearchDetail = keySearchDetail;
	}
	public int getPageDetail() {
		return pageDetail;
	}
	public void setPageDetail(int pageDetail) {
		this.pageDetail = pageDetail;
	}
	public boolean isVisibleDetail() {
		return visibleDetail;
	}
	public void setVisibleDetail(boolean visibleDetail) {
		this.visibleDetail = visibleDetail;
	}
	public List<ManualAssignDto> getListManualAssignDetail() {
		return listManualAssignDetail;
	}
	public void setListManualAssignDetail(
			List<ManualAssignDto> listManualAssignDetail) {
		this.listManualAssignDetail = listManualAssignDetail;
	}
	public ManualAssignDto getManualAssignDtoSelected() {
		return manualAssignDtoSelected;
	}
	public void setManualAssignDtoSelected(ManualAssignDto manualAssignDtoSelected) {
		this.manualAssignDtoSelected = manualAssignDtoSelected;
	}
	public String getKeySearchAssignTo() {
		return keySearchAssignTo;
	}
	public void setKeySearchAssignTo(String keySearchAssignTo) {
		this.keySearchAssignTo = keySearchAssignTo;
	}
	public List<MasterRegionalDto> getListAksesRegional() {
		return listAksesRegional;
	}
	public void setListAksesRegional(List<MasterRegionalDto> listAksesRegional) {
		this.listAksesRegional = listAksesRegional;
	}
	public List<CabangMstDto> getListAksesCabang() {
		return listAksesCabang;
	}
	public void setListAksesCabang(List<CabangMstDto> listAksesCabang) {
		this.listAksesCabang = listAksesCabang;
	}
	public UserProfileDto getUpSession() {
		return upSession;
	}
	public void setUpSession(UserProfileDto upSession) {
		this.upSession = upSession;
	}
	public int getPageRegional() {
		return pageRegional;
	}
	public void setPageRegional(int pageRegional) {
		this.pageRegional = pageRegional;
	}
	public int getPageCabang() {
		return pageCabang;
	}
	public void setPageCabang(int pageCabang) {
		this.pageCabang = pageCabang;
	}
	public int getPageKolektor() {
		return pageKolektor;
	}
	public void setPageKolektor(int pageKolektor) {
		this.pageKolektor = pageKolektor;
	}

	private List<UserProfileDto> listUserAssignChoose = new ArrayList<UserProfileDto>();
	private List<UserProfileDto> listUserKolektorChoose = new ArrayList<UserProfileDto>();
	private List<MasterRegionalDto> listRegionalDtoChoose = new ArrayList<MasterRegionalDto>();
	private List<CabangMstDto> listCabangMstDtoChoose = new ArrayList<CabangMstDto>();
	
	public List<UserProfileDto> getListUserAssignChoose() {
		return listUserAssignChoose;
	}
	public void setListUserAssignChoose(List<UserProfileDto> listUserAssignChoose) {
		this.listUserAssignChoose = listUserAssignChoose;
	}
	public List<UserProfileDto> getListUserKolektorChoose() {
		return listUserKolektorChoose;
	}
	public void setListUserKolektorChoose(
			List<UserProfileDto> listUserKolektorChoose) {
		this.listUserKolektorChoose = listUserKolektorChoose;
	}
	public List<MasterRegionalDto> getListRegionalDtoChoose() {
		return listRegionalDtoChoose;
	}
	public void setListRegionalDtoChoose(
			List<MasterRegionalDto> listRegionalDtoChoose) {
		this.listRegionalDtoChoose = listRegionalDtoChoose;
	}
	public List<CabangMstDto> getListCabangMstDtoChoose() {
		return listCabangMstDtoChoose;
	}
	public void setListCabangMstDtoChoose(List<CabangMstDto> listCabangMstDtoChoose) {
		this.listCabangMstDtoChoose = listCabangMstDtoChoose;
	}
	public String getKeySearchregional() {
		return keySearchregional;
	}
	public void setKeySearchregional(String keySearchregional) {
		this.keySearchregional = keySearchregional;
	}
	public String getKeySearchCabang() {
		return keySearchCabang;
	}
	public void setKeySearchCabang(String keySearchCabang) {
		this.keySearchCabang = keySearchCabang;
	}
	public String getKeySearchKolektor() {
		return keySearchKolektor;
	}
	public void setKeySearchKolektor(String keySearchKolektor) {
		this.keySearchKolektor = keySearchKolektor;
	}
	public String getStringArrayRegional() {
		return stringArrayRegional;
	}
	public void setStringArrayRegional(String stringArrayRegional) {
		this.stringArrayRegional = stringArrayRegional;
	}
	public String getStringArrayCabang() {
		return stringArrayCabang;
	}
	public void setStringArrayCabang(String stringArrayCabang) {
		this.stringArrayCabang = stringArrayCabang;
	}
	public String getStringArrayKolektor() {
		return stringArrayKolektor;
	}
	public void setStringArrayKolektor(String stringArrayKolektor) {
		this.stringArrayKolektor = stringArrayKolektor;
	}
	public boolean isPopUpPesan() {
		return popUpPesan;
	}
	public void setPopUpPesan(boolean popUpPesan) {
		this.popUpPesan = popUpPesan;
	}
	public List<ManualAssignDto> getCheckAssign() {
		return checkAssign;
	}
	public void setCheckAssign(List<ManualAssignDto> checkAssign) {
		this.checkAssign = checkAssign;
	}
	public String getSearch() {
		return search;
	}
	public void setSearch(String search) {
		this.search = search;
	}
	public List<ManualAssignDto> getListManualAssign() {
		return listManualAssign;
	}
	public void setListManualAssign(List<ManualAssignDto> listManualAssign) {
		this.listManualAssign = listManualAssign;
	}
	public ManualAssignDto getManualAssignDto() {
		return manualAssignDto;
	}
	public void setManualAssignDto(ManualAssignDto manualAssignDto) {
		this.manualAssignDto = manualAssignDto;
	}
	public List<UserProfileDto> getListUserAssign() {
		return listUserAssign;
	}
	public void setListUserAssign(List<UserProfileDto> listUserAssign) {
		this.listUserAssign = listUserAssign;
	}
	public UserProfileDto getUserAssignDto() {
		return userAssignDto;
	}
	public void setUserAssignDto(UserProfileDto userAssignDto) {
		this.userAssignDto = userAssignDto;
	}
	public List<UserProfileDto> getListUserProfileDto() {
		return listUserProfileDto;
	}
	public void setListUserProfileDto(List<UserProfileDto> listUserProfileDto) {
		this.listUserProfileDto = listUserProfileDto;
	}
	public List<CabangMstDto> getListCabangMstDto() {
		return listCabangMstDto;
	}
	public void setListCabangMstDto(List<CabangMstDto> listCabangMstDto) {
		this.listCabangMstDto = listCabangMstDto;
	}
	public CabangMstDto getCabangMstDto() {
		return cabangMstDto;
	}
	public void setCabangMstDto(CabangMstDto cabangMstDto) {
		this.cabangMstDto = cabangMstDto;
	}
	public UserProfileDto getUserProfileDto() {
		return userProfileDto;
	}
	public void setUserProfileDto(UserProfileDto userProfileDto) {
		this.userProfileDto = userProfileDto;
	}
	public UserProfileDto getUserProfileDtoSession() {
		return userProfileDtoSession;
	}
	public void setUserProfileDtoSession(UserProfileDto userProfileDtoSession) {
		this.userProfileDtoSession = userProfileDtoSession;
	}
	public List<MasterRegionalDto> getListMasterRegionalDto() {
		return listMasterRegionalDto;
	}
	public void setListMasterRegionalDto(List<MasterRegionalDto> listMasterRegionalDto) {
		this.listMasterRegionalDto = listMasterRegionalDto;
	}
	public MasterRegionalDto getMasterRegionalDto() {
		return masterRegionalDto;
	}
	public void setMasterRegionalDto(MasterRegionalDto masterRegionalDto) {
		this.masterRegionalDto = masterRegionalDto;
	}
	public boolean isRegional() {
		return regional;
	}
	public void setRegional(boolean regional) {
		this.regional = regional;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public Date getDateAwal() {
		return dateAwal;
	}
	public void setDateAwal(Date dateAwal) {
		this.dateAwal = dateAwal;
	}
	public Date getDateAkhir() {
		return dateAkhir;
	}
	public void setDateAkhir(Date dateAkhir) {
		this.dateAkhir = dateAkhir;
	}
	
	public boolean isBranch() {
		return branch;
	}
	public void setBranch(boolean branch) {
		this.branch = branch;
	}
	public boolean isColectorOld() {
		return colectorOld;
	}
	public void setColectorOld(boolean colectorOld) {
		this.colectorOld = colectorOld;
	}
	public boolean isColectorNew() {
		return colectorNew;
	}
	public void setColectorNew(boolean colectorNew) {
		this.colectorNew = colectorNew;
	}
	
	@Init
	public void load(){
		dateNow = new Date();
 		dateAwal= new Date();
		dateAkhir= new Date();
		userProfileDtoSession  = (UserProfileDto) Sessions.getCurrent().getAttribute("user");
		List<AksesRegionCabangDto> lr = aksesRegionCabangSvc.findRegionByUserId(userProfileDtoSession.getUserId(), "");
		List<AksesRegionCabangDto> lc = aksesRegionCabangSvc.fintCabangByUserId(userProfileDtoSession.getUserId(), "");
		if (lr.size() != 0) {
			for (int i = 0; i < lr.size(); i++) {
				MasterRegionalDto mr = new MasterRegionalDto();
				mr.setKodeRegion(lr.get(i).getKodeRegion());
				mr.setNamaRegion(lr.get(i).getNamaRegional());
				listAksesRegional.add(mr);
			}
		}
		if (lc.size() != 0) {
			for (int i = 0; i < lc.size(); i++) {
				CabangMstDto cb= new CabangMstDto();
				cb.setKodeCabang(lc.get(i).getKodeCabang());
				cb.setNamaCabang(lc.get(i).getNamaCabang());
				listAksesCabang.add(cb);
			}
		}
		//========
		for(int i=0; i<listAksesRegional.size();i++){
				List<ManualAssignDto> manual = new ArrayList<ManualAssignDto>();
				manual = collectionDetailSvc.selectResultPenugasanUlangKodeRegional(listAksesRegional.get(i).getKodeRegion(),"");
				for (int k = 0; k < manual.size(); k++) {
					collectionHeaderSvc.execSP_CHECK_TASK(manual.get(k).getOrderId());
				}
		}
		//========
	}
	
	@Command("page")
	@NotifyChange("page")
	public void page() {
		setPage(getPage());
	}
	
	@Command("showRegional")
	@NotifyChange({ "regional","listMasterRegionalDto","keySearchregional","listRegionalDtoChoose","masterRegionalDto"})
	public void showRegional() {
		listMasterRegionalDto = new ArrayList<MasterRegionalDto>();
		List<AksesRegionCabangDto> listTempAkses = new ArrayList<AksesRegionCabangDto>();
		listTempAkses = aksesRegionCabangSvc.findRegionByUserId(userProfileDtoSession.getUserId(),keySearchregional);
		if (listTempAkses.size() != 0) {
			for (int i = 0; i < listTempAkses.size(); i++) {
				MasterRegionalDto mr = new MasterRegionalDto();
				mr.setKodeRegion(listTempAkses.get(i).getKodeRegion());
				mr.setNamaRegion(listTempAkses.get(i).getNamaRegional());
				mr.setItemList(true);
				listMasterRegionalDto.add(mr);
			}
		}
		if (listRegionalDtoChoose.size() != 0 ) {
			List<MasterRegionalDto> listTemp = new ArrayList<MasterRegionalDto>();
			for (int i = 0; i < listMasterRegionalDto.size(); i++) {
				boolean sama = false;
				for (int j = 0; j < listRegionalDtoChoose.size(); j++) {
					if (listMasterRegionalDto.get(i).getKodeRegion().equals(listRegionalDtoChoose.get(j).getKodeRegion())) {
						sama = true;
					}
				}
				if (sama == true) {
					MasterRegionalDto temppDto = new MasterRegionalDto();
					temppDto.setKodeRegion(listMasterRegionalDto.get(i).getKodeRegion());
					temppDto.setNamaRegion(listMasterRegionalDto.get(i).getNamaRegion());
					temppDto.setChoose(true);
					temppDto.setItemList(true);
					listTemp.add(temppDto);
				}else {
					MasterRegionalDto temppDto = new MasterRegionalDto();
					temppDto.setKodeRegion(listMasterRegionalDto.get(i).getKodeRegion());
					temppDto.setNamaRegion(listMasterRegionalDto.get(i).getNamaRegion());
					temppDto.setChoose(false);
					temppDto.setItemList(true);
					listTemp.add(temppDto);
				}
			}
			listMasterRegionalDto = listTemp;
		}
		MasterRegionalDto mre = new MasterRegionalDto();
		mre.setKodeRegion("");
		mre.setNamaRegion("");
		mre.setItemList(false);
		listMasterRegionalDto.add(mre);
		setRegional(true);
		System.out.println("datanya : "+listMasterRegionalDto.size());
	}
	
	@Command("showSearchRegional")
	@NotifyChange({ "regional","listMasterRegionalDto","keySearchregional","listRegionalDtoChoose","masterRegionalDto"})
	public void showSearchRegional() {
		listMasterRegionalDto = new ArrayList<MasterRegionalDto>();
		List<AksesRegionCabangDto> listTempAkses = new ArrayList<AksesRegionCabangDto>();
		listTempAkses = aksesRegionCabangSvc.findRegionByUserId(userProfileDtoSession.getUserId(),keySearchregional);
		if (listTempAkses.size() != 0) {
			for (int i = 0; i < listTempAkses.size(); i++) {
				MasterRegionalDto mr = new MasterRegionalDto();
				mr.setKodeRegion(listTempAkses.get(i).getKodeRegion());
				mr.setNamaRegion(listTempAkses.get(i).getNamaRegional());
				mr.setItemList(true);
				listMasterRegionalDto.add(mr);
			}
		}
		if (listRegionalDtoChoose.size() != 0 ) {
			List<MasterRegionalDto> listTemp = new ArrayList<MasterRegionalDto>();
			for (int i = 0; i < listMasterRegionalDto.size(); i++) {
				boolean sama = false;
				for (int j = 0; j < listRegionalDtoChoose.size(); j++) {
					if (listMasterRegionalDto.get(i).getKodeRegion().equals(listRegionalDtoChoose.get(j).getKodeRegion())) {
						sama = true;
					}
				}
				if (sama == true) {
					MasterRegionalDto temppDto = new MasterRegionalDto();
					temppDto.setKodeRegion(listMasterRegionalDto.get(i).getKodeRegion());
					temppDto.setNamaRegion(listMasterRegionalDto.get(i).getNamaRegion());
					temppDto.setChoose(true);
					temppDto.setItemList(true);
					listTemp.add(temppDto);
				}else {
					MasterRegionalDto temppDto = new MasterRegionalDto();
					temppDto.setKodeRegion(listMasterRegionalDto.get(i).getKodeRegion());
					temppDto.setNamaRegion(listMasterRegionalDto.get(i).getNamaRegion());
					temppDto.setChoose(false);
					temppDto.setItemList(true);
					listTemp.add(temppDto);
				}
			}
			listMasterRegionalDto = listTemp;
		}
		MasterRegionalDto mre = new MasterRegionalDto();
		mre.setKodeRegion("");
		mre.setNamaRegion("");
		mre.setItemList(false);
		listMasterRegionalDto.add(mre);
		}
	
	@Command("closeRegional")
	@NotifyChange({ "regional","listMasterRegionalDto","masterRegionalDto"})
	public void closeRegional() {
		masterRegionalDto = new MasterRegionalDto();
		setRegional(false);
	}
	
	@Command("chooseRegional")
	@NotifyChange({"listManualAssign","stringArrayRegional","listRegionalDtoChoose","regional","listMasterRegionalDto","listUserKolektorChoose","listCabangMstDtoChoose",
		"stringArrayCabang","stringArrayKolektor","listRegionalDtoChoose","stringArrayRegional","masterRegionalDto"})
	public void chooseRegional() {
		if (listRegionalDtoChoose.size() != 0) {
			stringArrayRegional = "";
			for (int i = 0; i < listRegionalDtoChoose.size(); i++) {
				if (i == 0) {
					stringArrayRegional = stringArrayRegional+listRegionalDtoChoose.get(i).getNamaRegion();
				}else {
					stringArrayRegional = stringArrayRegional+", "+listRegionalDtoChoose.get(i).getNamaRegion();
				}
			}
			searchListManualAssign();
		}else {
			stringArrayRegional = "";
			stringArrayKolektor = "";
			stringArrayCabang = "";
			listRegionalDtoChoose = new ArrayList<MasterRegionalDto>();
			listCabangMstDtoChoose = new ArrayList<CabangMstDto>();
			listUserKolektorChoose = new ArrayList<UserProfileDto>();
			searchListManualAssign();
		}
		stringArrayKolektor = "";
		stringArrayCabang = "";
		listCabangMstDtoChoose = new ArrayList<CabangMstDto>();
		listUserKolektorChoose = new ArrayList<UserProfileDto>();
		setRegional(false);
	}
	
	@Command("showBranch")
	@NotifyChange({ "branch","listCabangMstDto","listCabangMstDtoChoose"})
	public void showBranch() {
		if (listRegionalDtoChoose.size() == 0) {
			List<CabangMstDto> tempList = new ArrayList<CabangMstDto>();
			listCabangMstDto = new ArrayList<CabangMstDto>();
			if (listAksesRegional.size() != 0) {
				for (int i = 0; i < listAksesRegional.size(); i++) {
					List<CabangMstDto> listTemp = new ArrayList<CabangMstDto>();
					List<AksesRegionCabangDto> listTempAkses = new ArrayList<AksesRegionCabangDto>();
					listTempAkses = aksesRegionCabangSvc.fintCabangByUserIdAndKodeRegion(userProfileDtoSession.getUserId(), listAksesRegional.get(i).getKodeRegion(), keySearchCabang);
					if (listTempAkses.size() != 0) {
						for (int ki = 0; ki < listTempAkses.size(); ki++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTempAkses.get(ki).getKodeCabang());
							cb.setNamaCabang(listTempAkses.get(ki).getNamaCabang());
							listTemp.add(cb);
						}
					}
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTemp.get(j).getKodeCabang());
							cb.setNamaCabang(listTemp.get(j).getNamaCabang());
							cb.setItemList(true);
							listCabangMstDto.add(cb);
						}
					}
				}
			}
			if (listCabangMstDtoChoose.size() != 0) {
				for (int i = 0; i < listCabangMstDto.size(); i++) {	
						CabangMstDto cba = new CabangMstDto();
						boolean cheked = false;
						for (int j = 0; j < listCabangMstDtoChoose.size(); j++) {
							if (listCabangMstDto.get(i).getKodeCabang().equals(listCabangMstDtoChoose.get(j).getKodeCabang())) {
								cheked = true;
							}
						}
						if (cheked == true) {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(true);
						}else {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(false);
						}
						tempList.add(cba);
				}
				listCabangMstDto = new ArrayList<CabangMstDto>();
				listCabangMstDto = tempList;
			}
			if (listCabangMstDto.size() != 0) {
				CabangMstDto cb = new CabangMstDto();
				cb.setKodeCabang("");
				cb.setNamaCabang("");
				cb.setItemList(false);
				listCabangMstDto.add(cb);
				setBranch(true);
			}else {
				Messagebox.show("Tidak ada data cabang yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}else {
			List<CabangMstDto> tempList = new ArrayList<CabangMstDto>();
			listCabangMstDto = new ArrayList<CabangMstDto>();
			if (listRegionalDtoChoose.size() != 0) {
				for (int i = 0; i < listRegionalDtoChoose.size(); i++) {
					List<CabangMstDto> listTemp = new ArrayList<CabangMstDto>();
					List<AksesRegionCabangDto> listTempAkses = new ArrayList<AksesRegionCabangDto>();
					listTempAkses = aksesRegionCabangSvc.fintCabangByUserIdAndKodeRegion(userProfileDtoSession.getUserId(), listRegionalDtoChoose.get(i).getKodeRegion(), keySearchCabang);
					if (listTempAkses.size() != 0) {
						for (int ki = 0; ki < listTempAkses.size(); ki++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTempAkses.get(ki).getKodeCabang());
							cb.setNamaCabang(listTempAkses.get(ki).getNamaCabang());
							listTemp.add(cb);
						}
					}
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTemp.get(j).getKodeCabang());
							cb.setNamaCabang(listTemp.get(j).getNamaCabang());
							cb.setItemList(true);
							listCabangMstDto.add(cb);
						}
					}
				}
			}
			if (listCabangMstDtoChoose.size() != 0) {
				for (int i = 0; i < listCabangMstDto.size(); i++) {	
						CabangMstDto cba = new CabangMstDto();
						boolean cheked = false;
						for (int j = 0; j < listCabangMstDtoChoose.size(); j++) {
							if (listCabangMstDto.get(i).getKodeCabang().equals(listCabangMstDtoChoose.get(j).getKodeCabang())) {
								cheked = true;
							}
						}
						if (cheked == true) {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(true);
						}else {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(false);
						}
						tempList.add(cba);
				}
				listCabangMstDto = new ArrayList<CabangMstDto>();
				listCabangMstDto = tempList;
			}
			if (listCabangMstDto.size() != 0) {
				CabangMstDto cb = new CabangMstDto();
				cb.setKodeCabang("");
				cb.setNamaCabang("");
				cb.setItemList(false);
				listCabangMstDto.add(cb);
				setBranch(true);
			}else {
				Messagebox.show("Tidak ada data cabang yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}
	}
	
	@Command("showSearchBranch")
	@NotifyChange({ "branch","listCabangMstDto","listCabangMstDtoChoose"})
	public void showSearchBranch() {
		if (listRegionalDtoChoose.size() == 0) {
			List<CabangMstDto> tempList = new ArrayList<CabangMstDto>();
			listCabangMstDto = new ArrayList<CabangMstDto>();
			if (listAksesRegional.size() != 0) {
				for (int i = 0; i < listAksesRegional.size(); i++) {
					List<CabangMstDto> listTemp = new ArrayList<CabangMstDto>();
					List<AksesRegionCabangDto> listTempAkses = new ArrayList<AksesRegionCabangDto>();
					listTempAkses = aksesRegionCabangSvc.fintCabangByUserIdAndKodeRegion(userProfileDtoSession.getUserId(), listAksesRegional.get(i).getKodeRegion(), keySearchCabang);
					if (listTempAkses.size() != 0) {
						for (int ki = 0; ki < listTempAkses.size(); ki++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTempAkses.get(ki).getKodeCabang());
							cb.setNamaCabang(listTempAkses.get(ki).getNamaCabang());
							listTemp.add(cb);
						}
					}
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTemp.get(j).getKodeCabang());
							cb.setNamaCabang(listTemp.get(j).getNamaCabang());
							cb.setItemList(true);
							listCabangMstDto.add(cb);
						}
					}
				}
			}
			if (listCabangMstDtoChoose.size() != 0) {
				for (int i = 0; i < listCabangMstDto.size(); i++) {	
						CabangMstDto cba = new CabangMstDto();
						boolean cheked = false;
						for (int j = 0; j < listCabangMstDtoChoose.size(); j++) {
							if (listCabangMstDto.get(i).getKodeCabang().equals(listCabangMstDtoChoose.get(j).getKodeCabang())) {
								cheked = true;
							}
						}
						if (cheked == true) {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(true);
						}else {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(false);
						}
						tempList.add(cba);
				}
				listCabangMstDto = new ArrayList<CabangMstDto>();
				listCabangMstDto = tempList;
			}
			if (listCabangMstDto.size() != 0) {
				CabangMstDto cb = new CabangMstDto();
				cb.setKodeCabang("");
				cb.setNamaCabang("");
				cb.setItemList(false);
				listCabangMstDto.add(cb);
				setBranch(true);
			}else {
				Messagebox.show("Tidak ada data cabang yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}else {
			List<CabangMstDto> tempList = new ArrayList<CabangMstDto>();
			listCabangMstDto = new ArrayList<CabangMstDto>();
			if (listRegionalDtoChoose.size() != 0) {
				for (int i = 0; i < listRegionalDtoChoose.size(); i++) {
					List<CabangMstDto> listTemp = cabangMstSvc.findCabangAllActiveByRegionalSearch(listRegionalDtoChoose.get(i).getKodeRegion(), keySearchCabang);
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTemp.get(j).getKodeCabang());
							cb.setNamaCabang(listTemp.get(j).getNamaCabang());
							cb.setItemList(true);
							listCabangMstDto.add(cb);
						}
					}
				}
			}
			if (listCabangMstDtoChoose.size() != 0) {
				for (int i = 0; i < listCabangMstDto.size(); i++) {	
						CabangMstDto cba = new CabangMstDto();
						boolean cheked = false;
						for (int j = 0; j < listCabangMstDtoChoose.size(); j++) {
							if (listCabangMstDto.get(i).getKodeCabang().equals(listCabangMstDtoChoose.get(j).getKodeCabang())) {
								cheked = true;
							}
						}
						if (cheked == true) {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(true);
						}else {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(false);
						}
						tempList.add(cba);
				}
				listCabangMstDto = new ArrayList<CabangMstDto>();
				listCabangMstDto = tempList;
			}
			if (listCabangMstDto.size() != 0) {
				CabangMstDto cb = new CabangMstDto();
				cb.setKodeCabang("");
				cb.setNamaCabang("");
				cb.setItemList(false);
				listCabangMstDto.add(cb);
			}else {
				Messagebox.show("Tidak ada data cabang yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}
	}
	
	@Command("closeBranch")
	@NotifyChange({ "branch","cabangMstDto"})
	public void closeBranch() {
		cabangMstDto = new CabangMstDto();
		setBranch(false);
	}
	
	@Command("showDetail")
	@NotifyChange({"visibleDetail","listManualAssignDetail","keySearchDetail","userAssignDto"})
	public void showDetail() {
		userAssignDto = new UserProfileDto();
		keySearchDetail = "";
		if (manualAssignDtoSelected != null) {
			listManualAssignDetail = collectionDetailSvc.selectResultPenugasanUlangDetail(manualAssignDtoSelected.getOrderId(),keySearchDetail);
			setVisibleDetail(true);
		} else {
			Messagebox.show("Pilih data terlebih dahulu","MESSAGE", null, null, null);
		}
	}
	
	@Command("showDetailSearch")
	@NotifyChange({"visibleDetail","listManualAssignDetail","keySearchDetail"})
	public void showDetailSearch() {
		if (manualAssignDtoSelected != null) {
			listManualAssignDetail = collectionDetailSvc.selectResultPenugasanUlangDetail(manualAssignDtoSelected.getOrderId(),keySearchDetail);
			setVisibleDetail(true);
		} else {
			Messagebox.show("Pilih data terlebih dahulu","MESSAGE", null, null, null);
		}
		
	}
	
	@Command("closeDetail")
	@NotifyChange({"visibleDetail","listManualAssignDetail"})
	public void closeDetail() {
		setVisibleDetail(false);
		listManualAssignDetail = new ArrayList<ManualAssignDto>();
	}
	
	@Command("chooseBranch")
	@NotifyChange({"listCabangMstDtoChoose","stringArrayCabang","listUserKolektorChoose","stringArrayKolektor","branch","listCabangMstDto","listManualAssign","listCabangMstDtoChoose"
		,"stringArrayCabang","cabangMstDto","stringArrayKolektor","listManualAssign","listUserKolektorChoose"})
	public void chooseBranch() {
		if (listCabangMstDtoChoose.size() != 0) {
			stringArrayCabang = "";
			for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
				if (i == 0) {
					stringArrayCabang = stringArrayCabang+listCabangMstDtoChoose.get(i).getNamaCabang();
				}else {
					stringArrayCabang = stringArrayCabang+", "+listCabangMstDtoChoose.get(i).getNamaCabang();
				}
			}
			searchListManualAssign();
		}else {
			listCabangMstDtoChoose = new ArrayList<CabangMstDto>();
			stringArrayCabang = "";
			listUserKolektorChoose = new ArrayList<UserProfileDto>();
			stringArrayKolektor = "";
			searchListManualAssign();
		}
		stringArrayKolektor = "";
		listUserKolektorChoose = new ArrayList<UserProfileDto>();
		setBranch(false);
	}
	
	@Command("showCollector")
	@NotifyChange({"keySearchKolektor","colectorOld","listUserProfileDto","listCabangMstDtoChoose","listUserKolektorChoose","userProfileDto"})
	public void showCollector() {
		keySearchKolektor = "";
		if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() == 0) { 
			listUserProfileDto = new ArrayList<UserProfileDto>();
			for (int i = 0; i < listAksesCabang.size(); i++) {
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeCabangAndSearch(listAksesCabang.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserProfileDto.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserProfileDto.size(); k++) {
								if (listUserProfileDto.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserProfileDto.add(kol);
							}
						}else {
							listUserProfileDto.add(kol);
						}
					}
				}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserProfileDto.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserProfileDto.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserProfileDto.get(i).getUserId());
						kole.setNamaKaryawan(listUserProfileDto.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserProfileDto.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserProfileDto.get(i).getUserId());
						kole.setNamaKaryawan(listUserProfileDto.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserProfileDto.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserProfileDto = listTemp;
				
			}
			if (listUserProfileDto.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserProfileDto.add(up);
				setColectorOld(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}else if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() != 0) {
			listUserProfileDto = new ArrayList<UserProfileDto>();
			for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeCabangAndSearch(listCabangMstDtoChoose.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserProfileDto.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserProfileDto.size(); k++) {
								if (listUserProfileDto.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserProfileDto.add(kol);
							}
						}else {
							listUserProfileDto.add(kol);
						}
					}
				}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserProfileDto.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserProfileDto.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserProfileDto.get(i).getUserId());
						kole.setNamaKaryawan(listUserProfileDto.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserProfileDto.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserProfileDto.get(i).getUserId());
						kole.setNamaKaryawan(listUserProfileDto.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserProfileDto.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserProfileDto = listTemp;
				
			}
			if (listUserProfileDto.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserProfileDto.add(up);
				setColectorOld(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
			
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() != 0) {
			listUserProfileDto = new ArrayList<UserProfileDto>();
			for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeCabangAndSearch(listCabangMstDtoChoose.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserProfileDto.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserProfileDto.size(); k++) {
								if (listUserProfileDto.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserProfileDto.add(kol);
							}
						}else {
							listUserProfileDto.add(kol);
						}
					}
				}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserProfileDto.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserProfileDto.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserProfileDto.get(i).getUserId());
						kole.setNamaKaryawan(listUserProfileDto.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserProfileDto.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserProfileDto.get(i).getUserId());
						kole.setNamaKaryawan(listUserProfileDto.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserProfileDto.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserProfileDto = listTemp;
				
			}
			if (listUserProfileDto.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserProfileDto.add(up);
				setColectorOld(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
			
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() == 0) {
			System.out.println("Masuk sini ke reg doang");
			listUserProfileDto = new ArrayList<UserProfileDto>();
			for (int h = 0; h < listRegionalDtoChoose.size(); h++) {
			for (int i = 0; i < listAksesCabang.size(); i++) {
				System.out.println("masuk loop reg ke = "+h);
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeRegionalKodeCabangAndSearch(listRegionalDtoChoose.get(h).getKodeRegion(),listAksesCabang.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserProfileDto.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserProfileDto.size(); k++) {
								if (listUserProfileDto.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserProfileDto.add(kol);
							}
						}else {
							listUserProfileDto.add(kol);
						}
					}
				}
			}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserProfileDto.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserProfileDto.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserProfileDto.get(i).getUserId());
						kole.setNamaKaryawan(listUserProfileDto.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserProfileDto.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserProfileDto.get(i).getUserId());
						kole.setNamaKaryawan(listUserProfileDto.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserProfileDto.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserProfileDto = listTemp;
				
			}
			if (listUserProfileDto.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserProfileDto.add(up);
				setColectorOld(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}
	}
	
	@Command("showSearchCollector")
	@NotifyChange({ "colectorOld","listUserProfileDto","listCabangMstDtoChoose","listUserKolektorChoose","userProfileDto","keySearchKolektor"})
	public void showSearchCollector() {
		if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() == 0) { 
			listUserProfileDto = new ArrayList<UserProfileDto>();
			for (int i = 0; i < listAksesCabang.size(); i++) {
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeCabangAndSearch(listAksesCabang.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserProfileDto.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserProfileDto.size(); k++) {
								if (listUserProfileDto.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserProfileDto.add(kol);
							}
						}else {
							listUserProfileDto.add(kol);
						}
					}
				}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserProfileDto.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserProfileDto.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserProfileDto.get(i).getUserId());
						kole.setNamaKaryawan(listUserProfileDto.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserProfileDto.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserProfileDto.get(i).getUserId());
						kole.setNamaKaryawan(listUserProfileDto.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserProfileDto.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserProfileDto = listTemp;
				
			}
			if (listUserProfileDto.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserProfileDto.add(up);
				setColectorOld(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}else if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() != 0) {
			listUserProfileDto = new ArrayList<UserProfileDto>();
			for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeCabangAndSearch(listCabangMstDtoChoose.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserProfileDto.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserProfileDto.size(); k++) {
								if (listUserProfileDto.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserProfileDto.add(kol);
							}
						}else {
							listUserProfileDto.add(kol);
						}
					}
				}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserProfileDto.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserProfileDto.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserProfileDto.get(i).getUserId());
						kole.setNamaKaryawan(listUserProfileDto.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserProfileDto.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserProfileDto.get(i).getUserId());
						kole.setNamaKaryawan(listUserProfileDto.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserProfileDto.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserProfileDto = listTemp;
				
			}
			if (listUserProfileDto.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserProfileDto.add(up);
				setColectorOld(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
			
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() != 0) {
			listUserProfileDto = new ArrayList<UserProfileDto>();
			for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeCabangAndSearch(listCabangMstDtoChoose.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserProfileDto.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserProfileDto.size(); k++) {
								if (listUserProfileDto.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserProfileDto.add(kol);
							}
						}else {
							listUserProfileDto.add(kol);
						}
					}
				}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserProfileDto.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserProfileDto.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserProfileDto.get(i).getUserId());
						kole.setNamaKaryawan(listUserProfileDto.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserProfileDto.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserProfileDto.get(i).getUserId());
						kole.setNamaKaryawan(listUserProfileDto.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserProfileDto.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserProfileDto = listTemp;
				
			}
			if (listUserProfileDto.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserProfileDto.add(up);
				setColectorOld(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
			
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() == 0) {
			System.out.println("Masuk sini ke reg doang");
			listUserProfileDto = new ArrayList<UserProfileDto>();
			for (int h = 0; h < listRegionalDtoChoose.size(); h++) {
			for (int i = 0; i < listAksesCabang.size(); i++) {
				System.out.println("masuk loop reg ke = "+h);
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeRegionalKodeCabangAndSearch(listRegionalDtoChoose.get(h).getKodeRegion(),listAksesCabang.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserProfileDto.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserProfileDto.size(); k++) {
								if (listUserProfileDto.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserProfileDto.add(kol);
							}
						}else {
							listUserProfileDto.add(kol);
						}
					}
				}
			}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserProfileDto.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserProfileDto.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserProfileDto.get(i).getUserId());
						kole.setNamaKaryawan(listUserProfileDto.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserProfileDto.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserProfileDto.get(i).getUserId());
						kole.setNamaKaryawan(listUserProfileDto.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserProfileDto.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserProfileDto = listTemp;
				
			}
			if (listUserProfileDto.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserProfileDto.add(up);
				setColectorOld(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}
	}
	
	@Command("closeCollector")
	@NotifyChange({ "colectorOld","listUserProfileDto","userProfileDto"})
	public void closeCollector() {
		userProfileDto=new UserProfileDto();
		setColectorOld(false);
	}
	
	@Command("chooseCollector")
	@NotifyChange({"stringArrayKolektor","listUserKolektorChoose","colectorOld","listManualAssign","listUserProfileDto","stringArrayKolektor","userProfileDto"})
	public void chooseCollector() {
		if (listUserKolektorChoose.size() != 0) {
			stringArrayKolektor = "";
			for (int i = 0; i < listUserKolektorChoose.size(); i++) {
				if (i == 0  ) {
					stringArrayKolektor = stringArrayKolektor+listUserKolektorChoose.get(i).getNamaKaryawan();
				}else {
					stringArrayKolektor = stringArrayKolektor+", "+listUserKolektorChoose.get(i).getNamaKaryawan();
				}
			}
			searchListManualAssign();
		}else {
			stringArrayKolektor ="";
			listUserKolektorChoose = new ArrayList<UserProfileDto>();
			searchListManualAssign();
		}
		setColectorOld(false);
	}
	
	@Command("showAssignTo")
	@NotifyChange({"colectorNew","listUserAssign","userAssignDto","keySearchAssignTo"})
	public void showAssignTo() {
		keySearchAssignTo = "";
		listUserAssign=new ArrayList<UserProfileDto>();
		for (int i = 0; i < listAksesCabang.size(); i++) {
			List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeCabangAndSearch(listAksesCabang.get(i).getKodeCabang(), keySearchAssignTo);
			if (listTempKol.size() != 0) {
				for (int j = 0; j < listTempKol.size(); j++) {
					UserProfileDto kol = new UserProfileDto();
					kol.setUserId(listTempKol.get(j).getUserId());
					kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
					kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
					if (listUserAssign.size() != 0) {
						boolean sama = false;
						for (int k = 0; k < listUserAssign.size(); k++) {
							if (listUserAssign.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
								sama = true;
							}
						}
						if (sama == false) {
							listUserAssign.add(kol);
						}
					}else {
						listUserAssign.add(kol);
					}
				}
			}
		}
		if (listUserKolektorChoose.size() != 0 ) {
			List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
			for (int i = 0; i < listUserAssign.size(); i++) {
				boolean sama = false;
				for (int j = 0; j < listUserKolektorChoose.size(); j++) {
					if (listUserAssign.get(i).getUserId().equalsIgnoreCase(listUserKolektorChoose.get(j).getUserId())) {
						sama = true;
					}
				}
				if (sama == true) {
					System.out.println("sama jangan di tampilin");
				}else {
					UserProfileDto kole = new UserProfileDto();
					kole.setUserId(listUserAssign.get(i).getUserId());
					kole.setNamaKaryawan(listUserAssign.get(i).getNamaKaryawan());
					kole.setKodeKolektor(listUserAssign.get(i).getKodeKolektor());
					listTemp.add(kole);
				}
			}
			listUserAssign = listTemp;
		}
		setColectorNew(true);
	}
	@Command("showSearchAssignTo")
	@NotifyChange({"colectorNew","listUserAssign","userAssignDto"})
	public void showSearchAssignTo() {
		listUserAssign=new ArrayList<UserProfileDto>();
		for (int i = 0; i < listAksesCabang.size(); i++) {
			List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeCabangAndSearch(listAksesCabang.get(i).getKodeCabang(), keySearchAssignTo);
			if (listTempKol.size() != 0) {
				for (int j = 0; j < listTempKol.size(); j++) {
					UserProfileDto kol = new UserProfileDto();
					kol.setUserId(listTempKol.get(j).getUserId());
					kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
					kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
					if (listUserAssign.size() != 0) {
						boolean sama = false;
						for (int k = 0; k < listUserAssign.size(); k++) {
							if (listUserAssign.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
								sama = true;
							}
						}
						if (sama == false) {
							listUserAssign.add(kol);
						}
					}else {
						listUserAssign.add(kol);
					}
				}
			}
		}
		if (listUserKolektorChoose.size() != 0 ) {
			List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
			for (int i = 0; i < listUserAssign.size(); i++) {
				boolean sama = false;
				for (int j = 0; j < listUserKolektorChoose.size(); j++) {
					if (listUserAssign.get(i).getUserId().equalsIgnoreCase(listUserKolektorChoose.get(j).getUserId())) {
						sama = true;
					}
				}
				if (sama == true) {
					System.out.println("sama jangan di tampilin");
				}else {
					UserProfileDto kole = new UserProfileDto();
					kole.setUserId(listUserAssign.get(i).getUserId());
					kole.setNamaKaryawan(listUserAssign.get(i).getNamaKaryawan());
					kole.setKodeKolektor(listUserAssign.get(i).getKodeKolektor());
					listTemp.add(kole);
				}
			}
			listUserAssign = listTemp;
		}
		setColectorNew(true);
	}
	
	@Command("closeAssignTo")
	@NotifyChange({"colectorNew","listUserProfileDto","userAssignDto"})
	public void closeAssignTo() {
		setColectorNew(false);
	}
	
	@Command("chooseAssignTo")
	@NotifyChange({"colectorNew","listUserProfileDto","userProfileDto"})
	public void chooseAssignTo() {
		setColectorNew(false);
	}
	
	@Command("popUpAssign")
	@NotifyChange({"popUpPesan","manualAssignDto","notesValue"})
	public void popUpPesan(){
		if (userAssignDto.getNamaKaryawan() != null) {
			ReassignCollector rc = new ReassignCollector();
			rc = collectionDetailSvc.selectReassignCollectorByCollectorCode(userAssignDto.getKodeKolektor());
			int lolos = 0;
			if (rc.getCollectorCode() != null) {		
				Date dateNow = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				String strDateNow = sdf.format(dateNow);
				int yDateReaInt = 0;
				int mDateReaInt = 0;
				int dDateReaInt = 0;
				int yDateNowInt = 0;
				int mDateNowInt = 0;
				int dDateNowInt = 0;
				String[] x = rc.getStrPeriodTo().split("-");
				for (int j = 0; j < x.length; j++) {
					if (j == 0) {
						yDateReaInt = Integer.valueOf(x[j]);
					}
					if (j == 1) {
						mDateReaInt = Integer.valueOf(x[j]);
					}
					if (j == 2) {
						dDateReaInt = Integer.valueOf(x[j]);
					}
				}
				String[] y = strDateNow.split("-");
				for (int j = 0; j < y.length; j++) {
					if (j == 0) {
						yDateNowInt = Integer.valueOf(y[j]);
					}
					if (j == 1) {
						mDateNowInt = Integer.valueOf(y[j]);
					}
					if (j == 2) {
						dDateNowInt = Integer.valueOf(y[j]);
					}
				}
				if ( yDateNowInt < yDateReaInt ) {
					System.out.println("masuk ke 1");
					lolos = 0;
				}else if (mDateNowInt < mDateReaInt) {
					System.out.println("masuk ke 2");
					lolos = 0;
				}else if (dDateNowInt < dDateReaInt) {
					if (mDateNowInt > mDateReaInt) {
						System.out.println("masuk ke 3");
						lolos = 1;
					}else {
						System.out.println("masuk ke 4");
						lolos = 0;
					}
				}else {
					System.out.println("masuk ke 5");
					lolos = 1;
				}
			}else {
				lolos = 1;
			}
			
			if (lolos ==1) {
				DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
				String strDateAwal = df.format(dateAwal);
				String strDateAkhir = df.format(dateAkhir);
				String yDateAwalStr = "";
				String mDateAwalStr = "";
				String dDateAwalStr = "";
				String yDateAkhirStr = "";
				String mDateAkhirStr = "";
				String dDateAkhirStr = "";
				int yDateAwalInt = 0;
				int mDateAwalInt = 0;
				int dDateAwalInt = 0;
				int yDateAkhirInt = 0;
				int mDateAkhirInt = 0;
				int dDateAkhirInt = 0;
				String[] x = strDateAwal.split("/");
				for (int j = 0; j < x.length; j++) {
					if (j == 0) {
						yDateAwalStr = x[j];
					}
					if (j == 1) {
						mDateAwalStr = x[j];
					}
					if (j == 2) {
						dDateAwalStr = x[j];
					}
				}
				String[] y = strDateAkhir.split("/");
				for (int j = 0; j < y.length; j++) {
					if (j == 0) {
						yDateAkhirStr = y[j];
					}
					if (j == 1) {
						mDateAkhirStr = y[j];
					}
					if (j == 2) {
						dDateAkhirStr = y[j];
					}
				}
				yDateAwalInt = Integer.valueOf(yDateAwalStr);
				mDateAwalInt = Integer.valueOf(mDateAwalStr);
				dDateAwalInt = Integer.valueOf(dDateAwalStr);
				yDateAkhirInt = Integer.valueOf(yDateAkhirStr);
				mDateAkhirInt = Integer.valueOf(mDateAkhirStr);
				dDateAkhirInt = Integer.valueOf(dDateAkhirStr);
				Date dateNow = new Date();
				if (listManualAssignDetail.size() != 0) {
					if (df.format(dateNow).equalsIgnoreCase(df.format(dateAwal))) {
						if (yDateAkhirInt < yDateAwalInt ) {
							System.out.println("MASUK 1 = "+dateAkhir.getYear()+" | "+dateAwal.getYear());
							Messagebox.show("Sampai tanggal harus lebih besar sama dengan tanggal awal", "MESSAGE", null, null, null);
						} else {
							if (mDateAkhirInt < mDateAwalInt) {
								System.out.println("MASUK 2 = "+dateAkhir.getMonth()+" | "+dateAwal.getMonth());
								Messagebox.show("Sampai tanggal harus lebih besar sama dengan tanggal awal", "MESSAGE", null, null, null);
							}else {
								if (mDateAkhirInt == mDateAwalInt) {
									if (dDateAkhirInt <= dDateAwalInt) {
										System.out.println("MASUK 3 = "+dateAkhir.getDay()+" | "+dateAwal.getDay());
										Messagebox.show("Sampai tanggal harus lebih besar sama dengan tanggal awal", "MESSAGE", null, null, null);
									}else {
										notesValue = "";
										setPopUpPesan(true);
									}
								}else {
									notesValue = "";
									setPopUpPesan(true);
								}
							}
						}
					}else {
						Messagebox.show("Tanggal awal haruslah hari ini", "MESSAGE", null, null, null);
					}
				}else {
					manualAssignDto = new ManualAssignDto();
					Messagebox.show("Tidak ada data yang akan di proses", "MESSAGE", null, null, null);
			
				}
			}else {
				Messagebox.show("Penugasan ulang tidak dapat dilkukan dikarenakan data tersebut masih dalam proses penugasan ulang sebelumnya dan hanya bisa di tugaskan ulang kepada kolektor yang di tugaskan sebelumnya","MESSAGE", null, null, null);
			}
		}else {
			Messagebox.show("Kepada siapa penugasan ulang ini di tuju, Pilih terlebih dahulu kolektor.", "MESSAGE", null, null, null);
		}
	}
	
	@Command("cancelPopUpAssign")
	@NotifyChange("popUpPesan")
	public void cancelPopUpPesan(){
		setPopUpPesan(false);
	}
	
	@Command("assign")
	@NotifyChange({"listManualAssignDetail","visibleDetail","manualAssignDto","listManualAssign","popUpPesan","listRegionalDtoChoose","listCabangMstDtoChoose","listUserKolektorChoose","userAssignDto"})
	public void assign(){
			Messagebox.show("Apakah anda yakin ?", "MESSAGE",
					new Button[] { Button.YES, Button.NO },
					Messagebox.QUESTION, Button.NO,
					new EventListener<Messagebox.ClickEvent>() {
				@Override
				@NotifyChange({"visibleDetail","listManualAssign","listManualAssignDetail","checkAssign","listRegionalDtoChoose","listCabangMstDtoChoose","listUserKolektorChoose","userAssignDto"})
				public void onEvent(ClickEvent event) throws Exception {
					if (Messagebox.ON_YES.equals(event.getName())) {
						ManualAssignDto manAss = new ManualAssignDto();
						int seqNo = 0;
						for(int i=0; i < listManualAssignDetail.size(); i++){
							manAss.setKodeKolektor(listManualAssignDetail.get(i).getKodeKolektor());
							manAss.setOrderId(listManualAssignDetail.get(i).getOrderId()); 
						}
						if (manAss != null) {
							if (manualAssignSvc.maxSeqnoBByUserId(manAss.getKodeKolektor()) != 0) {
								seqNo = manualAssignSvc.maxSeqnoBByUserId(manAss.getKodeKolektor());
							}
							seqNo = seqNo +1;
							manualAssignSvc.insertAssign(seqNo,manAss.getKodeKolektor(), userAssignDto.getKodeKolektor(),
									dateAwal, dateAkhir,notesValue, manAss.getOrderId());
							manualAssignSvc.updateAssign(userAssignDto.getKodeKolektor() ,upSession.getUserId(), manAss.getKodeKolektor(), notesValue, manAss.getOrderId());
						}
						Notification.sendNotif(upSession.getUserId(), "command", "get_data");
						Messagebox.show("Manual Assign Success","Message",null,null,null);
						listManualAssign = new ArrayList<ManualAssignDto>();			
						setPopUpPesan(false);
						BindUtils.postNotifyChange(null, null, SnpManualAassignVmd.this, "listManualAssign");
						BindUtils.postNotifyChange(null, null, SnpManualAassignVmd.this, "listRegionalDtoChoose");
						BindUtils.postNotifyChange(null, null, SnpManualAassignVmd.this, "listCabangMstDtoChoose");
						BindUtils.postNotifyChange(null, null, SnpManualAassignVmd.this, "listUserKolektorChoose");
						BindUtils.postNotifyChange(null, null, SnpManualAassignVmd.this, "userAssignDto");
						BindUtils.postNotifyChange(null, null, SnpManualAassignVmd.this, "stringArrayCabang");
						BindUtils.postNotifyChange(null, null, SnpManualAassignVmd.this, "stringArrayRegional");
						BindUtils.postNotifyChange(null, null, SnpManualAassignVmd.this, "stringArrayKolektor");
						BindUtils.postNotifyChange(null, null, SnpManualAassignVmd.this, "listManualAssignDetail");
						BindUtils.postNotifyChange(null, null, SnpManualAassignVmd.this, "visibleDetail");
						setVisibleDetail(false);
						listManualAssignDetail = new ArrayList<ManualAssignDto>();
						listRegionalDtoChoose = new ArrayList<MasterRegionalDto>();
						listCabangMstDtoChoose = new ArrayList<CabangMstDto>();
						listUserKolektorChoose = new ArrayList<UserProfileDto>();
						userAssignDto = new UserProfileDto();
						stringArrayCabang = "";
						stringArrayRegional = "";
						stringArrayKolektor =  "";
					}	
				}
			});
			setPopUpPesan(false);
	}
	
	@Command("pageRegional")
	@NotifyChange("pageRegional")
	public void pageRegional() {
		setPageRegional(getPageRegional());
	}
	
	@Command("pageDetail")
	@NotifyChange("pageDetail")
	public void pageDetail() {
		setPageDetail(getPageDetail());
	}
	
	@Command("pageCabang")
	@NotifyChange("pageCabang")
	public void pageCabang() {
		setPageCabang(getPageCabang());
	}
	@Command("pageKolektor")
	@NotifyChange("pageRegional")
	public void pageKolektor() {
		setPageKolektor(getPageKolektor());
	}
	
	@Command("searchListManualAssign")
	@NotifyChange({"listManualAssign"})
	public void searchListManualAssign() {
		if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() != 0 && listUserKolektorChoose.size() != 0) {
			listManualAssign=new ArrayList<ManualAssignDto>();
			for(int j=0;j<listCabangMstDtoChoose.size();j++){
				for(int k=0;k<listUserKolektorChoose.size();k++){
					System.out.println("Masuk sini nih ke semua di isi");
					List<ManualAssignDto> manual = new ArrayList<ManualAssignDto>();
					manual = collectionDetailSvc.selectResultPenugasanUlangKodeCabKodeKol(listCabangMstDtoChoose.get(j).getKodeCabang(), listUserKolektorChoose.get(k).getKodeKolektor(), search);
					if(manual.size()!=0){
						for(int l=0; l<manual.size(); l++){
							ManualAssignDto manualAssign = new ManualAssignDto();
							manualAssign.setNamaRegional(manual.get(l).getNamaRegional());
							manualAssign.setNamaKaryawan(manual.get(l).getNamaKaryawan());
							manualAssign.setBucketCollector(manual.get(l).getBucketCollector());
							manualAssign.setBucketPayment(manual.get(l).getBucketPayment());
							manualAssign.setBucketHandling(manual.get(l).getBucketHandling());
							manualAssign.setBucketNotHandling(manual.get(l).getBucketNotHandling());
							manualAssign.setPreviosCollector(manual.get(l).getPreviosCollector());
							manualAssign.setOrderId(manual.get(l).getOrderId());
							listManualAssign.add(manualAssign);
						}
					}
					
				}
			}
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() == 0 && listUserKolektorChoose.size() == 0) {
			listManualAssign=new ArrayList<ManualAssignDto>();
			for(int i=0; i<listRegionalDtoChoose.size();i++){
				for (int j = 0; j < listAksesCabang.size(); j++) {
					List<ManualAssignDto> manual = new ArrayList<ManualAssignDto>();
					manual = collectionDetailSvc.selectResultPenugasanUlangKodeRegKodeCab(listRegionalDtoChoose.get(i).getKodeRegion(), listAksesCabang.get(j).getKodeCabang(), search);
					if(manual.size()!=0){
						for(int l=0; l<manual.size(); l++){
							ManualAssignDto manualAssign = new ManualAssignDto();
							manualAssign.setNamaRegional(manual.get(l).getNamaRegional());
							manualAssign.setNamaKaryawan(manual.get(l).getNamaKaryawan());
							manualAssign.setBucketCollector(manual.get(l).getBucketCollector());
							manualAssign.setBucketPayment(manual.get(l).getBucketPayment());
							manualAssign.setBucketHandling(manual.get(l).getBucketHandling());
							manualAssign.setBucketNotHandling(manual.get(l).getBucketNotHandling());
							manualAssign.setPreviosCollector(manual.get(l).getPreviosCollector());
							manualAssign.setOrderId(manual.get(l).getOrderId());
							listManualAssign.add(manualAssign);
						}
					}	
				}
			}
		}else if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() != 0 && listUserKolektorChoose.size() == 0) {
			listManualAssign=new ArrayList<ManualAssignDto>();
			for (int j = 0; j < listCabangMstDtoChoose.size(); j++) {
				List<ManualAssignDto> manual = new ArrayList<ManualAssignDto>();
				manual = collectionDetailSvc.selectResultPenugasanUlangKodeCab(listCabangMstDtoChoose.get(j).getKodeCabang(), search);
				if (manual.size() != 0) {
					for (int l = 0; l < manual.size(); l++) {
						ManualAssignDto manualAssign = new ManualAssignDto();
						manualAssign.setNamaRegional(manual.get(l).getNamaRegional());
						manualAssign.setNamaKaryawan(manual.get(l).getNamaKaryawan());
						manualAssign.setBucketCollector(manual.get(l).getBucketCollector());
						manualAssign.setBucketPayment(manual.get(l).getBucketPayment());
						manualAssign.setBucketHandling(manual.get(l).getBucketHandling());
						manualAssign.setBucketNotHandling(manual.get(l).getBucketNotHandling());
						manualAssign.setPreviosCollector(manual.get(l).getPreviosCollector());
						manualAssign.setOrderId(manual.get(l).getOrderId());
						listManualAssign.add(manualAssign);
					}
				}
			}
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() != 0 && listUserKolektorChoose.size() == 0) {
			listManualAssign=new ArrayList<ManualAssignDto>();
			for (int j = 0; j < listCabangMstDtoChoose.size(); j++) {
				List<ManualAssignDto> manual = new ArrayList<ManualAssignDto>();
				manual = collectionDetailSvc.selectResultPenugasanUlangKodeCab(listCabangMstDtoChoose.get(j).getKodeCabang(), search);
				if (manual.size() != 0) {
					for (int l = 0; l < manual.size(); l++) {
						ManualAssignDto manualAssign = new ManualAssignDto();
						manualAssign.setNamaRegional(manual.get(l).getNamaRegional());
						manualAssign.setNamaKaryawan(manual.get(l).getNamaKaryawan());
						manualAssign.setBucketCollector(manual.get(l).getBucketCollector());
						manualAssign.setBucketPayment(manual.get(l).getBucketPayment());
						manualAssign.setBucketHandling(manual.get(l).getBucketHandling());
						manualAssign.setBucketNotHandling(manual.get(l).getBucketNotHandling());
						manualAssign.setPreviosCollector(manual.get(l).getPreviosCollector());
						manualAssign.setOrderId(manual.get(l).getOrderId());
						listManualAssign.add(manualAssign);
					}
				}
			}
			
		}else if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() == 0 && listUserKolektorChoose.size() != 0) {
			listManualAssign=new ArrayList<ManualAssignDto>();
			for(int j=0;j<listAksesCabang.size();j++){
				for(int k=0;k<listUserKolektorChoose.size();k++){
					List<ManualAssignDto> manual = new ArrayList<ManualAssignDto>();
					manual = collectionDetailSvc.selectResultPenugasanUlangKodeCabKodeKol(listAksesCabang.get(j).getKodeCabang(), listUserKolektorChoose.get(k).getKodeKolektor(), search);
					if(manual.size()!=0){
						for(int l=0; l<manual.size(); l++){
							ManualAssignDto manualAssign = new ManualAssignDto();
							manualAssign.setNamaRegional(manual.get(l).getNamaRegional());
							manualAssign.setNamaKaryawan(manual.get(l).getNamaKaryawan());
							manualAssign.setBucketCollector(manual.get(l).getBucketCollector());
							manualAssign.setBucketPayment(manual.get(l).getBucketPayment());
							manualAssign.setBucketHandling(manual.get(l).getBucketHandling());
							manualAssign.setBucketNotHandling(manual.get(l).getBucketNotHandling());
							manualAssign.setPreviosCollector(manual.get(l).getPreviosCollector());
							manualAssign.setOrderId(manual.get(l).getOrderId());
							listManualAssign.add(manualAssign);
						}
					}		
				}
			}
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() == 0 && listUserKolektorChoose.size() != 0) {
			listManualAssign=new ArrayList<ManualAssignDto>();
			for(int j=0;j<listRegionalDtoChoose.size();j++){
				for(int k=0;k<listUserKolektorChoose.size();k++){
					List<ManualAssignDto> manual = new ArrayList<ManualAssignDto>();
					manual = collectionDetailSvc.selectResultPenugasanUlangKodeRegKodeKol(listRegionalDtoChoose.get(j).getKodeRegion(), listCabangMstDtoChoose.get(k).getKodeCabang(), search);
					if(manual.size()!=0){
						for(int l=0; l<manual.size(); l++){
							ManualAssignDto manualAssign = new ManualAssignDto();
							manualAssign.setNamaRegional(manual.get(l).getNamaRegional());
							manualAssign.setNamaKaryawan(manual.get(l).getNamaKaryawan());
							manualAssign.setBucketCollector(manual.get(l).getBucketCollector());
							manualAssign.setBucketPayment(manual.get(l).getBucketPayment());
							manualAssign.setBucketHandling(manual.get(l).getBucketHandling());
							manualAssign.setBucketNotHandling(manual.get(l).getBucketNotHandling());
							manualAssign.setPreviosCollector(manual.get(l).getPreviosCollector());
							manualAssign.setOrderId(manual.get(l).getOrderId());
							listManualAssign.add(manualAssign);
						}
					}		
				}
			}
		}else if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() != 0 && listUserKolektorChoose.size() != 0) {
			listManualAssign=new ArrayList<ManualAssignDto>();
			for(int j=0;j<listCabangMstDtoChoose.size();j++){
				for(int k=0;k<listUserKolektorChoose.size();k++){
					List<ManualAssignDto> manual = new ArrayList<ManualAssignDto>();
					manual = collectionDetailSvc.selectResultPenugasanUlangKodeCabKodeKol(listCabangMstDtoChoose.get(j).getKodeCabang(), listUserKolektorChoose.get(k).getKodeKolektor(), search);
					if(manual.size()!=0){
						for(int l=0; l<manual.size(); l++){
							ManualAssignDto manualAssign = new ManualAssignDto();
							manualAssign.setNamaRegional(manual.get(l).getNamaRegional());
							manualAssign.setNamaKaryawan(manual.get(l).getNamaKaryawan());
							manualAssign.setBucketCollector(manual.get(l).getBucketCollector());
							manualAssign.setBucketPayment(manual.get(l).getBucketPayment());
							manualAssign.setBucketHandling(manual.get(l).getBucketHandling());
							manualAssign.setBucketNotHandling(manual.get(l).getBucketNotHandling());
							manualAssign.setPreviosCollector(manual.get(l).getPreviosCollector());
							manualAssign.setOrderId(manual.get(l).getOrderId());
							listManualAssign.add(manualAssign);
						}
					}		
				}
			}
		}else if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() == 0 && listUserKolektorChoose.size() == 0) {
			listManualAssign = new ArrayList<ManualAssignDto>();
		}
	}
		
}
