package vmd;

import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;

import org.apache.commons.codec.digest.DigestUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

import com.mysql.fabric.xmlrpc.base.Data;

import dto.UserProfileDto;
import entity.UserMenuAccordion;
import service.LoginSvc;
import service.UserProfileSvc;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class LoginVmd {

	@WireVariable
	LoginSvc loginSvc;
	
	@WireVariable
	UserProfileSvc userProfileSvc;


	private String username;
	private String password;

	private UserProfileDto userProfileDto = new UserProfileDto();

	@Command("login")
	@NotifyChange({ "username", "password" })
	public void login() throws NoSuchAlgorithmException {
		if (username != null && password != null) {
				String md5Pass = DigestUtils.md5Hex(password);
				Map<String, Object> map = loginSvc.findRoleMenu(username.toUpperCase(), md5Pass);
				userProfileDto = (UserProfileDto) map.get("userProfile");
				if (userProfileDto != null) {
					List<UserProfileDto> lp = userProfileSvc.findUserByUserId(userProfileDto.getUserId());
					UserProfileDto up = new UserProfileDto();
					if (lp.size() != 0) {
						for (int i = 0; i < lp.size(); i++) {
							up.setUserId(lp.get(i).getUserId());
							up.setPassword(lp.get(i).getPassword());
						}
					}
					String passDefault = "columbia123";
					passDefault = DigestUtils.md5Hex(passDefault);
					if (lp.size() != 0 && up.getPassword().equalsIgnoreCase(passDefault)) {
						List<UserMenuAccordion> a = (List<UserMenuAccordion>) map.get("listMenu");
						userProfileDto.setLisAccordions(a);
						Sessions.getCurrent().setAttribute("user",userProfileDto);
						UserProfileDto dsfMstUserDtoSession = (UserProfileDto) Sessions.getCurrent().getAttribute("user");
						Executions.sendRedirect("/master/changePassword/_index.zul");
					} else if (userProfileDto.getUserId() != null) {
						List<UserMenuAccordion> a = (List<UserMenuAccordion>) map.get("listMenu");
						userProfileDto.setLisAccordions(a);
						Sessions.getCurrent().setAttribute("user",userProfileDto);
						UserProfileDto dsfMstUserDtoSession = (UserProfileDto) Sessions.getCurrent().getAttribute("user");
						Executions.sendRedirect("/index.zul");
						
					} else {
						Messagebox.show("Invalid login ", "Message", null,
								null, null);
						setUsername(null);
						setPassword(null);
					}
				} else {
					Messagebox.show("Invalid login ", "Message", null, null,
							null);
					setUsername(null);
					setPassword(null);
				}
		}

		else {
			Messagebox.show("Please fill in all colomn", "Message", null, null,
					null);
		}
		// TODO: handle exception
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
