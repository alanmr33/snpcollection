package vmd;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.SystemException;

import org.apache.commons.codec.digest.DigestUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;

import dto.UserProfileDto;
import pagevmd.NavigationVmd;
import service.UserProfileSvc;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class ChangePasswordVmd extends NavigationVmd{
	
	@WireVariable
	private UserProfileSvc userProfileSvc;
	
	private String passOld;
	private String passNew = null;
	private String passConf= null;
	private String passNewMd5 = null;
	private UserProfileDto userProfileDtoSession = new UserProfileDto();
	private List<UserProfileDto> listUserProDto = new ArrayList<UserProfileDto>();
	
	
	
	public String getPassNewMd5() {
		return passNewMd5;
	}
	public void setPassNewMd5(String passNewMd5) {
		this.passNewMd5 = passNewMd5;
	}
	public UserProfileDto getUserProfileDtoSession() {
		return userProfileDtoSession;
	}
	public void setUserProfileDtoSession(UserProfileDto userProfileDtoSession) {
		this.userProfileDtoSession = userProfileDtoSession;
	}
	public List<UserProfileDto> getListUserProDto() {
		return listUserProDto;
	}
	public void setListUserProDto(List<UserProfileDto> listUserProDto) {
		this.listUserProDto = listUserProDto;
	}
	public String getPassOld() {
		return passOld;
	}
	public void setPassOld(String passOld) {
		this.passOld = passOld;
	}
	public String getPassNew() {
		return passNew;
	}
	public void setPassNew(String passNew) {
		this.passNew = passNew;
	}
	public String getPassConf() {
		return passConf;
	}
	public void setPassConf(String passConf) {
		this.passConf = passConf;
	}
	
	@Init
	public void load() {
		Sessions.getCurrent().removeAttribute("sessionMobileTracking");
		userProfileDtoSession  = (UserProfileDto) Sessions.getCurrent().getAttribute("user");
	}
	
	@Command("changePass")
	@NotifyChange({ "listUserProDto", "passOld", "passNew","passConf","passNewMd5"})
	public void changePass() {
		listUserProDto = new ArrayList<UserProfileDto>();
		String passOldConf="";
		String passOldMd5 = "";
		listUserProDto = userProfileSvc.findUserByUserId(userProfileDtoSession.getUserId());
		passOldMd5 = DigestUtils.md5Hex(passOld);
		if (listUserProDto.size() != 0) {
			if (passOld != null) {
				for (int i = 0; i < listUserProDto.size(); i++) {
					passOldConf = listUserProDto.get(i).getPassword();
				}
				if (passOldMd5.equals(passOldConf)) {
					if (passNew != null && passConf != null) {
						if (passNew.equals(passConf)) {
							passNewMd5 = DigestUtils.md5Hex(passNew);
							Messagebox.show("Are you sure want to change password - LOGOUT AUTOMATICALLY", "Message", new Button[]{Button.YES, Button.NO}, 
									Messagebox.QUESTION, Button.NO, new EventListener<Messagebox.ClickEvent>() {
										@Override
										public void onEvent(ClickEvent event) throws Exception {
											if (Messagebox.ON_YES.equals(event.getName())) {
												System.out.println("ni pass enkripnya= "+passNewMd5);
												userProfileSvc.changePassworUserProfile(passNewMd5, userProfileDtoSession.getUserId(), userProfileDtoSession.getUserId());
												Sessions.getCurrent().removeAttribute("user");
												Executions.getCurrent().sendRedirect("/login.zul");
												
											}
										}
									});
						}else {
							Messagebox.show("New Password does not match!", "MESSAGE", null, null, null);
						}
					}else {
						Messagebox.show("New Password does not match!", "MESSAGE", null, null, null);
					}
				}else {
					Messagebox.show("Old Password is wrong!", "MESSAGE", null, null, null);
				}
			}else {
				Messagebox.show("Old Password is wrong!", "MESSAGE", null, null, null);
			}
		}else {
			Messagebox.show("Old Password is wrong!", "MESSAGE", null, null, null);
		}
	}
}
