package vmd;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.NewCookie;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.poi.hssf.record.DSFRecord;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

import com.mysql.fabric.xmlrpc.base.Array;

import dto.AksesRegionCabangDto;
import dto.AuthorityMstDto;
import dto.CabangMstDto;
import dto.MasterRegionalDto;
import dto.UserProfileDto;
import pagevmd.NavigationVmd;
import service.AksesRegionCabangSvc;
import service.AuthorityMstSvc;
import service.CabangMstSvc;
import service.MasterRegionalSvc;
import service.UserProfileSvc;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class MasterUserVmd extends NavigationVmd{

	@WireVariable
	private UserProfileSvc userProfileSvc;
	
	@WireVariable
	private MasterRegionalSvc masterRegionalSvc;
	
	@WireVariable
	private CabangMstSvc cabangMstSvc;
	
	@WireVariable
	private AuthorityMstSvc authorityMstSvc;
	
	@WireVariable
	private AksesRegionCabangSvc aksesRegionCabangSvc;
	
	private List<UserProfileDto> listUserProfileDto = new ArrayList<UserProfileDto>();
	private List<CabangMstDto> listCabangMstDto = new ArrayList<CabangMstDto>();
	private List<CabangMstDto> listCabangMstDtoChoose = new ArrayList<CabangMstDto>();
	private List<MasterRegionalDto> listMasterRegionalDto = new ArrayList<MasterRegionalDto>();
	private List<MasterRegionalDto> listMasterRegionalDtoChoose = new ArrayList<MasterRegionalDto>();
	private List<AuthorityMstDto> listAuthorityMstDto = new ArrayList<AuthorityMstDto>();
	private CabangMstDto cabangMstDto = new CabangMstDto();
	private MasterRegionalDto masterRegionalDto = new MasterRegionalDto();
	private AuthorityMstDto authorityMstDto = new AuthorityMstDto();
	private UserProfileDto userProfileDto;
	private UserProfileDto userProfileDtoSession;
	private int page = 10;
	private int pageRegional = 10;
	private int pageCabang = 10;
	private int pageAuthority = 10;
	private String keySearch = "";
	private boolean visibleAddEdit = false;
	private boolean fieldUserId = false;
	private boolean visibleRegional = false;
	private boolean visibleCabang = false;
	private boolean visibleAuthority = false;
	private String keySearchRegional = "";
	private String keySearchCabang = "";
	private String keySearchAuthority = "";
	private String keySearchKaryawan = "";
	
	
	public int getPageRegional() {
		return pageRegional;
	}
	public void setPageRegional(int pageRegional) {
		this.pageRegional = pageRegional;
	}
	public int getPageCabang() {
		return pageCabang;
	}
	public void setPageCabang(int pageCabang) {
		this.pageCabang = pageCabang;
	}
	public int getPageAuthority() {
		return pageAuthority;
	}
	public void setPageAuthority(int pageAuthority) {
		this.pageAuthority = pageAuthority;
	}
	public List<CabangMstDto> getListCabangMstDtoChoose() {
		return listCabangMstDtoChoose;
	}
	public void setListCabangMstDtoChoose(List<CabangMstDto> listCabangMstDtoChoose) {
		this.listCabangMstDtoChoose = listCabangMstDtoChoose;
	}
	public List<MasterRegionalDto> getListMasterRegionalDtoChoose() {
		return listMasterRegionalDtoChoose;
	}
	public void setListMasterRegionalDtoChoose(
			List<MasterRegionalDto> listMasterRegionalDtoChoose) {
		this.listMasterRegionalDtoChoose = listMasterRegionalDtoChoose;
	}
	public UserProfileDto getUserProfileDtoSession() {
		return userProfileDtoSession;
	}
	public void setUserProfileDtoSession(UserProfileDto userProfileDtoSession) {
		this.userProfileDtoSession = userProfileDtoSession;
	}
	public boolean isVisibleCabang() {
		return visibleCabang;
	}
	public void setVisibleCabang(boolean visibleCabang) {
		this.visibleCabang = visibleCabang;
	}
	public boolean isVisibleAuthority() {
		return visibleAuthority;
	}
	public void setVisibleAuthority(boolean visibleAuthority) {
		this.visibleAuthority = visibleAuthority;
	}
	public List<CabangMstDto> getListCabangMstDto() {
		return listCabangMstDto;
	}
	public void setListCabangMstDto(List<CabangMstDto> listCabangMstDto) {
		this.listCabangMstDto = listCabangMstDto;
	}
	public List<MasterRegionalDto> getListMasterRegionalDto() {
		return listMasterRegionalDto;
	}
	public void setListMasterRegionalDto(
			List<MasterRegionalDto> listMasterRegionalDto) {
		this.listMasterRegionalDto = listMasterRegionalDto;
	}
	public List<AuthorityMstDto> getListAuthorityMstDto() {
		return listAuthorityMstDto;
	}
	public void setListAuthorityMstDto(List<AuthorityMstDto> listAuthorityMstDto) {
		this.listAuthorityMstDto = listAuthorityMstDto;
	}
	public CabangMstDto getCabangMstDto() {
		return cabangMstDto;
	}
	public void setCabangMstDto(CabangMstDto cabangMstDto) {
		this.cabangMstDto = cabangMstDto;
	}
	public MasterRegionalDto getMasterRegionalDto() {
		return masterRegionalDto;
	}
	public void setMasterRegionalDto(MasterRegionalDto masterRegionalDto) {
		this.masterRegionalDto = masterRegionalDto;
	}
	public AuthorityMstDto getAuthorityMstDto() {
		return authorityMstDto;
	}
	public void setAuthorityMstDto(AuthorityMstDto authorityMstDto) {
		this.authorityMstDto = authorityMstDto;
	}
	public String getKeySearchRegional() {
		return keySearchRegional;
	}
	public void setKeySearchRegional(String keySearchRegional) {
		this.keySearchRegional = keySearchRegional;
	}
	public String getKeySearchCabang() {
		return keySearchCabang;
	}
	public void setKeySearchCabang(String keySearchCabang) {
		this.keySearchCabang = keySearchCabang;
	}
	public String getKeySearchAuthority() {
		return keySearchAuthority;
	}
	public void setKeySearchAuthority(String keySearchAuthority) {
		this.keySearchAuthority = keySearchAuthority;
	}
	public String getKeySearchKaryawan() {
		return keySearchKaryawan;
	}
	public void setKeySearchKaryawan(String keySearchKaryawan) {
		this.keySearchKaryawan = keySearchKaryawan;
	}
	public boolean isVisibleAddEdit() {
		return visibleAddEdit;
	}
	public void setVisibleAddEdit(boolean visibleAddEdit) {
		this.visibleAddEdit = visibleAddEdit;
	}
	public boolean isFieldUserId() {
		return fieldUserId;
	}
	public void setFieldUserId(boolean fieldUserId) {
		this.fieldUserId = fieldUserId;
	}
	public boolean isVisibleRegional() {
		return visibleRegional;
	}
	public void setVisibleRegional(boolean visibleRegional) {
		this.visibleRegional = visibleRegional;
	}
	public List<UserProfileDto> getListUserProfileDto() {
		return listUserProfileDto;
	}
	public void setListUserProfileDto(List<UserProfileDto> listUserProfileDto) {
		this.listUserProfileDto = listUserProfileDto;
	}
	public UserProfileDto getUserProfileDto() {
		return userProfileDto;
	}
	public void setUserProfileDto(UserProfileDto userProfileDto) {
		this.userProfileDto = userProfileDto;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public String getKeySearch() {
		return keySearch;
	}
	public void setKeySearch(String keySearch) {
		this.keySearch = keySearch;
	}
	
	@Init
	public void load() {
		Sessions.getCurrent().removeAttribute("sessionMobileTracking");
		userProfileDtoSession  = (UserProfileDto) Sessions.getCurrent().getAttribute("user");
		listUserProfileDto = userProfileSvc.findUserAllBySearch(keySearch);
	}
	
	@Command("page")
	@NotifyChange("page")
	public void page() {
		setPage(getPage());
	}
	
	@Command("search")
	@NotifyChange({ "listUserProfileDto" })
	public void search() {
		listUserProfileDto = userProfileSvc.findUserAllBySearch(keySearch);
	}
	
	@Command("add")
	@NotifyChange({"authorityMstDto","visibleAddEdit","userProfileDto","fieldUserId","listCabangMstDtoChoose","listMasterRegionalDtoChoose"})
	public void add() {
		setFieldUserId(false);
		authorityMstDto = new AuthorityMstDto();
		listMasterRegionalDtoChoose = new ArrayList<MasterRegionalDto>();
		listCabangMstDtoChoose = new ArrayList<CabangMstDto>();
		userProfileDto = new UserProfileDto();
		setVisibleAddEdit(true);
	}
	
	@Command("edit")
	@NotifyChange({"keySearchAuthority","keySearchRegional", "keySearchCabang","visibleAddEdit","userProfileDto","fieldUserId","listMasterRegionalDtoChoose","listMasterRegionalDto","listCabangMstDto","listCabangMstDtoChoose"})
	public void edit() {
		keySearchRegional = "";
		setFieldUserId(true);
		keySearchCabang = "";
		keySearchAuthority = "";
		if (aksesRegionCabangSvc.findRegionByUserId(userProfileDto.getUserId(), keySearchRegional) != null) {
			List<AksesRegionCabangDto> listTempRegion = aksesRegionCabangSvc.findRegionByUserId(userProfileDto.getUserId(), keySearchRegional);
			if (listTempRegion.size() != 0) {
				for (int i = 0; i < listTempRegion.size(); i++) {
					MasterRegionalDto mr = new MasterRegionalDto();
					mr.setKodeRegion(listTempRegion.get(i).getKodeRegion());
					mr.setNamaRegion(listTempRegion.get(i).getNamaRegional());
					listMasterRegionalDtoChoose.add(mr);
				}
			}
		}
		if (aksesRegionCabangSvc.fintCabangByUserId(userProfileDto.getUserId(), keySearchCabang) != null) {
			List<AksesRegionCabangDto> listTempCabang = aksesRegionCabangSvc.fintCabangByUserId(userProfileDto.getUserId(), keySearchCabang);
			if (listTempCabang.size() != 0) {
				for (int i = 0; i < listTempCabang.size(); i++) {
					CabangMstDto cb = new CabangMstDto();
					cb.setKodeCabang(listTempCabang.get(i).getKodeCabang());
					cb.setNamaCabang(listTempCabang.get(i).getNamaCabang());
					listCabangMstDtoChoose.add(cb);
				}
			}
		}
		String temStrKodRegional = "";
		String temStrKodCabang = "";
		String temStrRegional = "";
		String temStrCabang = "";
		if (listMasterRegionalDtoChoose.size() != 0) {
			for (int i = 0; i < listMasterRegionalDtoChoose.size(); i++) {
				if (i == 0) {
					temStrRegional = temStrRegional+listMasterRegionalDtoChoose.get(i).getNamaRegion();
				}else {
					temStrRegional = temStrRegional+", "+listMasterRegionalDtoChoose.get(i).getNamaRegion();
				}
				if (i == 0) {
					temStrKodRegional = temStrKodRegional+listMasterRegionalDtoChoose.get(i).getKodeRegion();
				}else {
					temStrKodRegional = temStrKodRegional+", "+listMasterRegionalDtoChoose.get(i).getKodeRegion();
				}
			}
		}
		if (listCabangMstDtoChoose.size() != 0) {
			for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
				if (i == 0) {
					temStrCabang = temStrCabang+listCabangMstDtoChoose.get(i).getNamaCabang();
				}else {
					temStrCabang = temStrCabang+", "+listCabangMstDtoChoose.get(i).getNamaCabang();
				}
				if (i == 0) {
					temStrKodCabang = temStrKodCabang+listCabangMstDtoChoose.get(i).getKodeCabang();
				}else {
					temStrKodCabang = temStrKodCabang+", "+listCabangMstDtoChoose.get(i).getKodeCabang();
				}
			}
		}
		userProfileDto.setKodeRegion(temStrKodRegional);
		userProfileDto.setNamaRegional(temStrRegional);
		userProfileDto.setKodeCabang(temStrKodCabang);
		userProfileDto.setNamaCabang(temStrCabang);
		setVisibleAddEdit(true);
	}
	
	
	
	@Command("close")
	@NotifyChange({"listUserProfileDto","authorityMstDto","visibleAddEdit","userProfileDto","listCabangMstDtoChoose","listMasterRegionalDtoChoose"})
	public void close() {
		userProfileDto = new UserProfileDto();
		listCabangMstDtoChoose = new ArrayList<CabangMstDto>();
		listMasterRegionalDtoChoose = new ArrayList<MasterRegionalDto>();
		authorityMstDto = new AuthorityMstDto();
		load();
		setVisibleAddEdit(false);
	}
	
	@Command("save")
	@NotifyChange({"authorityMstDto","visibleAddEdit","userProfileDto","listUserProfileDto","listCabangMstDtoChoose","listMasterRegionalDtoChoose"})
	public void save() {
		boolean lanjut = true;
		if (userProfileDto.getUserId() == null) {
			lanjut = false;
		}
		if (userProfileDto.getKodeCabang() == null) {
			lanjut = false;
		}
		if (userProfileDto.getKodeRegion() == null) {
			lanjut = false;
		}
		if (userProfileDto.getKodeKaryawan() == null) {
			lanjut = false;
		}
		if (userProfileDto.getNamaKaryawan() == null) {
			lanjut = false;
		}
		if (userProfileDto.getAuthorityId() == null) {
			lanjut = false;
		}
		if (userProfileDto.getImeiNumber() == null) {
			lanjut = false;
		}
		if (userProfileDto.getPhoneNumber() == null) {
			lanjut = false;
		}
		if (userProfileDto.getKeteranganStatus() == null) {
			lanjut = false;
		}
		if (userProfileDto.getKodeKolektor() == null) {
			lanjut = false;
		}
		if (listMasterRegionalDtoChoose.size() == 0) {
			lanjut = false;
		}
		if (listCabangMstDtoChoose.size() == 0) {
			lanjut = false;
		}
		if (lanjut == true) {
			List<UserProfileDto> listCekUserProfilee = userProfileSvc
					.findUserByUserId(userProfileDto.getUserId());
			if (listCekUserProfilee.size() == 0) {
				if (userProfileDto.getKeteranganStatus().equals("ENABLED")) {
					userProfileDto.setAktifStatus(1);
				} else {
					userProfileDto.setAktifStatus(0);
				}
				/*String genPass = RandomStringUtils.random(64, false, true);
				genPass = RandomStringUtils.random(8, 0, 62, true, true,
						"abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ"
								.toCharArray());*/
				String md5Pass = "columbia123";
				md5Pass = DigestUtils.md5Hex(md5Pass);
				userProfileSvc.saveUserProfile(userProfileDto.getUserId().toUpperCase(),
						userProfileDto.getKodeKaryawan(),
						userProfileDto.getNamaKaryawan(),
						userProfileDto.getAuthorityId(),
						userProfileDto.getImeiNumber(),
						userProfileDto.getPhoneNumber(),
						String.valueOf(userProfileDto.getAktifStatus()),
						userProfileDtoSession.getUserId(), 
						md5Pass,
						userProfileDto.getKodeKolektor());
				if (listMasterRegionalDtoChoose.size() != 0) {
					aksesRegionCabangSvc.deleteRegion(userProfileDto.getUserId());
					for (int i = 0; i < listMasterRegionalDtoChoose.size(); i++) {
						aksesRegionCabangSvc.insertRegion(userProfileDto.getUserId(), listMasterRegionalDtoChoose.get(i).getKodeRegion());
					}
				}
				if (listCabangMstDtoChoose.size() != 0) {
					aksesRegionCabangSvc.deleteCabang(userProfileDto.getUserId());
					for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
						aksesRegionCabangSvc.insertCabang(userProfileDto.getUserId(), listCabangMstDtoChoose.get(i).getKodeCabang());
					}
				}
				userProfileDto = new UserProfileDto();
				setVisibleAddEdit(false);
				listCabangMstDtoChoose = new ArrayList<CabangMstDto>();
				listMasterRegionalDtoChoose = new ArrayList<MasterRegionalDto>();
				listUserProfileDto = userProfileSvc.findUserAllBySearch(keySearch);
				Messagebox.show("DATA USER BARU BERHASIL DITAMBAHKAN","MESSAGE",null, null, null);
			} else {
				if (userProfileDto.getKeteranganStatus().equals("ENABLED")) {
					userProfileDto.setAktifStatus(1);
				} else {
					userProfileDto.setAktifStatus(0);
				}
				userProfileSvc.updateUserProfile(
						userProfileDto.getKodeKaryawan(),
						userProfileDto.getNamaKaryawan(),
						userProfileDto.getAuthorityId(),
						userProfileDto.getImeiNumber(),
						userProfileDto.getPhoneNumber(),
						String.valueOf(userProfileDto.getAktifStatus()),
						userProfileDtoSession.getUserId(),
						userProfileDto.getUserId(),
						userProfileDto.getKodeKolektor());
				if (listMasterRegionalDtoChoose.size() != 0) {
					aksesRegionCabangSvc.deleteRegion(userProfileDto.getUserId());
					for (int i = 0; i < listMasterRegionalDtoChoose.size(); i++) {
						aksesRegionCabangSvc.insertRegion(userProfileDto.getUserId(), listMasterRegionalDtoChoose.get(i).getKodeRegion());
					}
				}
				if (listCabangMstDtoChoose.size() != 0) {
					aksesRegionCabangSvc.deleteCabang(userProfileDto.getUserId());
					for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
						aksesRegionCabangSvc.insertCabang(userProfileDto.getUserId(), listCabangMstDtoChoose.get(i).getKodeCabang());
					}
				}
				userProfileDto = new UserProfileDto();
				setVisibleAddEdit(false);
				listCabangMstDtoChoose = new ArrayList<CabangMstDto>();
				listMasterRegionalDtoChoose = new ArrayList<MasterRegionalDto>();
				authorityMstDto = new AuthorityMstDto();
				listUserProfileDto = userProfileSvc.findUserAllBySearch(keySearch);
				Messagebox.show("DATA USER BERHASIL DIUBAH", "MESSAGE", null, null,null);
			}
		} else {
			Messagebox.show("SEMUA FIELD HARUS DI ISI ", "MESSAGE", null, null,
					null);
		}
		
	}
	
	@Command("disableUser")
	@NotifyChange({ "visibleAddEdit","userProfileDto","listUserProfileDto" })
	public void disableUser() {
		userProfileSvc.disableUserProfile("0", userProfileDtoSession.getUserId(), userProfileDto.getUserId());
		userProfileDto = new UserProfileDto();;
		setVisibleAddEdit(false);
		listUserProfileDto = userProfileSvc.findUserAllBySearch(keySearch);
		Messagebox.show("DISABLE USER SUCCESS", "MESSAGE", null, null, null);
	}
	
	@Command("resetPass")
	@NotifyChange({ "visibleAddEdit","userProfileDto","listUserProfileDto" })
	public void resetPass() {
		String genPass = RandomStringUtils.random(64, false, true);
		String genpasMd5 = "";
		genPass = RandomStringUtils.random(8, 0, 62, true, true, "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray());
		genpasMd5 = DigestUtils.md5Hex(genPass);
		userProfileSvc.resetPassword(genpasMd5, userProfileDtoSession.getUserId(), userProfileDto.getUserId());
		listUserProfileDto = userProfileSvc.findUserAllBySearch(keySearch);
		Messagebox.show("RESET PASSWORD USER, NEW PASSWORD = "+genPass, "MESSAGE", null, null, null);
	}
	
	@Command("showRegional")
	@NotifyChange({"keySearchRegional","visibleRegional","listMasterRegionalDto","masterRegionalDto","listMasterRegionalDto","listMasterRegionalDtoChoose"})
	public void showRegional() {
		keySearchRegional = "";
		listMasterRegionalDto = new ArrayList<MasterRegionalDto>();
		List<MasterRegionalDto> listTempAksesRegional = new ArrayList<MasterRegionalDto>();
		listTempAksesRegional = masterRegionalSvc.searchAllRegional(keySearchRegional);
		if (listTempAksesRegional.size() != 0) {
			for (int i = 0; i < listTempAksesRegional.size(); i++) {
				MasterRegionalDto mr = new MasterRegionalDto();
				mr.setKodeRegion(listTempAksesRegional.get(i).getKodeRegion());
				mr.setNamaRegion(listTempAksesRegional.get(i).getNamaRegion());
				mr.setItemList(true);
				listMasterRegionalDto.add(mr);
			}
		}
		
		/*if (listMasterRegionalDtoChoose.size() == 0) {
			MasterRegionalDto mr = new MasterRegionalDto();
			mr.setKodeRegion("dummy");
			mr.setNamaRegion("");
			listMasterRegionalDtoChoose.add(mr);
		}*/
		if (listMasterRegionalDtoChoose.size() != 0 ) {
			List<MasterRegionalDto> listTemp = new ArrayList<MasterRegionalDto>();
			for (int i = 0; i < listMasterRegionalDto.size(); i++) {
				boolean sama = false;
				for (int j = 0; j < listMasterRegionalDtoChoose.size(); j++) {
					if (listMasterRegionalDto.get(i).getKodeRegion().equals(listMasterRegionalDtoChoose.get(j).getKodeRegion())) {
						sama = true;
					}
				}
				if (sama == true) {
					MasterRegionalDto temppDto = new MasterRegionalDto();
					temppDto.setKodeRegion(listMasterRegionalDto.get(i).getKodeRegion());
					temppDto.setNamaRegion(listMasterRegionalDto.get(i).getNamaRegion());
					temppDto.setChoose(true);
					temppDto.setItemList(true);
					listTemp.add(temppDto);
				}else {
					MasterRegionalDto temppDto = new MasterRegionalDto();
					temppDto.setKodeRegion(listMasterRegionalDto.get(i).getKodeRegion());
					temppDto.setNamaRegion(listMasterRegionalDto.get(i).getNamaRegion());
					temppDto.setChoose(false);
					temppDto.setItemList(true);
					listTemp.add(temppDto);
				}
			}
			listMasterRegionalDto = listTemp;
		}
		MasterRegionalDto mre = new MasterRegionalDto();
		mre.setKodeRegion("");
		mre.setNamaRegion("");
		mre.setItemList(false);
		listMasterRegionalDto.add(mre);
		setVisibleRegional(true);
	}
	
	@Command("showSearchRegional")
	@NotifyChange({"keySearchRegional","visibleRegional","listMasterRegionalDto","masterRegionalDto","listMasterRegionalDto","listMasterRegionalDtoChoose"})
	public void showSearchRegional() {
		listMasterRegionalDto = new ArrayList<MasterRegionalDto>();
		List<MasterRegionalDto> listTempAksesRegional = new ArrayList<MasterRegionalDto>();
		listTempAksesRegional = masterRegionalSvc.searchAllRegional(keySearchRegional);
		if (listTempAksesRegional.size() != 0) {
			for (int i = 0; i < listTempAksesRegional.size(); i++) {
				MasterRegionalDto mr = new MasterRegionalDto();
				mr.setKodeRegion(listTempAksesRegional.get(i).getKodeRegion());
				mr.setNamaRegion(listTempAksesRegional.get(i).getNamaRegion());
				mr.setItemList(true);
				listMasterRegionalDto.add(mr);
			}
		}
		/*if (listMasterRegionalDtoChoose.size() == 0) {
			MasterRegionalDto mr = new MasterRegionalDto();
			mr.setKodeRegion("dummy");
			mr.setNamaRegion("");
			listMasterRegionalDtoChoose.add(mr);
		}*/
		if (listMasterRegionalDtoChoose.size() != 0 ) {
			List<MasterRegionalDto> listTemp = new ArrayList<MasterRegionalDto>();
			for (int i = 0; i < listMasterRegionalDto.size(); i++) {
				boolean sama = false;
				for (int j = 0; j < listMasterRegionalDtoChoose.size(); j++) {
					if (listMasterRegionalDto.get(i).getKodeRegion().equals(listMasterRegionalDtoChoose.get(j).getKodeRegion())) {
						sama = true;
					}
				}
				if (sama == true) {
					MasterRegionalDto temppDto = new MasterRegionalDto();
					temppDto.setKodeRegion(listMasterRegionalDto.get(i).getKodeRegion());
					temppDto.setNamaRegion(listMasterRegionalDto.get(i).getNamaRegion());
					temppDto.setChoose(true);
					temppDto.setItemList(true);
					listTemp.add(temppDto);
				}else {
					MasterRegionalDto temppDto = new MasterRegionalDto();
					temppDto.setKodeRegion(listMasterRegionalDto.get(i).getKodeRegion());
					temppDto.setNamaRegion(listMasterRegionalDto.get(i).getNamaRegion());
					temppDto.setChoose(false);
					temppDto.setItemList(true);
					listTemp.add(temppDto);
				}
			}
			listMasterRegionalDto = listTemp;
		}
		MasterRegionalDto mre = new MasterRegionalDto();
		mre.setKodeRegion("");
		mre.setNamaRegion("");
		mre.setItemList(false);
		listMasterRegionalDto.add(mre);
	}
	
	@Command("closeRegional")
	@NotifyChange({"listMasterRegionalDtoChoose","listMasterRegionalDto","visibleRegional","listMasterRegionalDto","masterRegionalDto"})
	public void closeRegional() {
		listMasterRegionalDtoChoose = new ArrayList<MasterRegionalDto>();
		listMasterRegionalDto = new ArrayList<MasterRegionalDto>();
		masterRegionalDto = new MasterRegionalDto();
		setVisibleRegional(false);
	}
	
	@Command("chooseRegional")
	@NotifyChange({ "userProfileDto","visibleRegional","listMasterRegionalDto","masterRegionalDto","userProfileDto","cabangMstDto"})
	public void chooseRegional() {
		String temStrKodRegional = "";
		String temStrRegional = "";
		if (listMasterRegionalDtoChoose.size() != 0) {
			for (int i = 0; i < listMasterRegionalDtoChoose.size(); i++) {
				if (i == 0) {
					temStrRegional = temStrRegional+listMasterRegionalDtoChoose.get(i).getNamaRegion();
				}else {
					temStrRegional = temStrRegional+", "+listMasterRegionalDtoChoose.get(i).getNamaRegion();
				}
				if (i == 0) {
					temStrKodRegional = temStrKodRegional+listMasterRegionalDtoChoose.get(i).getKodeRegion();
				}else {
					temStrKodRegional = temStrKodRegional+", "+listMasterRegionalDtoChoose.get(i).getKodeRegion();
				}
			}
		}
		userProfileDto.setKodeRegion(temStrKodRegional);
		userProfileDto.setNamaRegional(temStrRegional);
		setVisibleRegional(false);
	}
	
	@Command("showCabang")
	@NotifyChange({"keySearchCabang","visibleCabang","listCabangMstDto","cabangMstDto"})
	public void showCabang() {
		keySearchCabang = "";
		if (listMasterRegionalDtoChoose.size() == 0) {
			Messagebox.show("Pilih regional terlebih dahulu !", "MESSAGE", null, null, null);
		}else {
			List<CabangMstDto> tempList = new ArrayList<CabangMstDto>();
			listCabangMstDto = new ArrayList<CabangMstDto>();
			if (listMasterRegionalDtoChoose.size() != 0) {
				for (int i = 0; i < listMasterRegionalDtoChoose.size(); i++) {
					List<CabangMstDto> listTemp = new ArrayList<CabangMstDto>();
					List<CabangMstDto> listTempAkses = new ArrayList<CabangMstDto>();
					listTempAkses = cabangMstSvc.findCabangAllActiveByRegionalSearch(listMasterRegionalDtoChoose.get(i).getKodeRegion(), keySearchCabang);
					if (listTempAkses.size() != 0) {
						for (int ki = 0; ki < listTempAkses.size(); ki++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTempAkses.get(ki).getKodeCabang());
							cb.setNamaCabang(listTempAkses.get(ki).getNamaCabang());
							listTemp.add(cb);
						}
					}
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTemp.get(j).getKodeCabang());
							cb.setNamaCabang(listTemp.get(j).getNamaCabang());
							cb.setItemList(true);
							listCabangMstDto.add(cb);
						}
					}
				}
			}
			if (listCabangMstDtoChoose.size() != 0) {
				for (int i = 0; i < listCabangMstDto.size(); i++) {	
						CabangMstDto cba = new CabangMstDto();
						boolean cheked = false;
						for (int j = 0; j < listCabangMstDtoChoose.size(); j++) {
							if (listCabangMstDto.get(i).getKodeCabang().equals(listCabangMstDtoChoose.get(j).getKodeCabang())) {
								cheked = true;
							}
						}
						if (cheked == true) {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(true);
						}else {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(false);
						}
						tempList.add(cba);
				}
				listCabangMstDto = new ArrayList<CabangMstDto>();
				listCabangMstDto = tempList;
			}
			if (listCabangMstDto.size() != 0) {
				CabangMstDto cb = new CabangMstDto();
				cb.setKodeCabang("");
				cb.setNamaCabang("");
				cb.setItemList(false);
				listCabangMstDto.add(cb);
				setVisibleCabang(true);
			}else {
				Messagebox.show("Tidak ada data cabang yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}
		
	}
	
	@Command("showSearchCabang")
	@NotifyChange({"keySearchCabang","visibleCabang","listCabangMstDto","cabangMstDto"})
	public void showSearchCabang() {
		/*if (listCabangMstDtoChoose.size() == 0) {
			CabangMstDto cb = new CabangMstDto();
			cb.setKodeCabang("dummy");
			cb.setNamaCabang("");
			listCabangMstDtoChoose.add(cb);
		}*/
		if (listMasterRegionalDtoChoose.size() == 0) {
			Messagebox.show("Pilih regional terlebih dahulu !", "MESSAGE", null, null, null);
		}else {
			List<CabangMstDto> tempList = new ArrayList<CabangMstDto>();
			listCabangMstDto = new ArrayList<CabangMstDto>();
			if (listMasterRegionalDtoChoose.size() != 0) {
				for (int i = 0; i < listMasterRegionalDtoChoose.size(); i++) {
					List<CabangMstDto> listTemp = new ArrayList<CabangMstDto>();
					List<CabangMstDto> listTempAkses = new ArrayList<CabangMstDto>();
					listTempAkses = cabangMstSvc.findCabangAllActiveByRegionalSearch(listMasterRegionalDtoChoose.get(i).getKodeRegion(), keySearchCabang);
					if (listTempAkses.size() != 0) {
						for (int ki = 0; ki < listTempAkses.size(); ki++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTempAkses.get(ki).getKodeCabang());
							cb.setNamaCabang(listTempAkses.get(ki).getNamaCabang());
							listTemp.add(cb);
						}
					}
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTemp.get(j).getKodeCabang());
							cb.setNamaCabang(listTemp.get(j).getNamaCabang());
							cb.setItemList(true);
							listCabangMstDto.add(cb);
						}
					}
				}
			}
			if (listCabangMstDtoChoose.size() != 0) {
				for (int i = 0; i < listCabangMstDto.size(); i++) {	
						CabangMstDto cba = new CabangMstDto();
						boolean cheked = false;
						for (int j = 0; j < listCabangMstDtoChoose.size(); j++) {
							if (listCabangMstDto.get(i).getKodeCabang().equals(listCabangMstDtoChoose.get(j).getKodeCabang())) {
								cheked = true;
							}
						}
						if (cheked == true) {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(true);
						}else {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(false);
						}
						tempList.add(cba);
				}
				listCabangMstDto = new ArrayList<CabangMstDto>();
				listCabangMstDto = tempList;
			}
			CabangMstDto cb = new CabangMstDto();
			cb.setKodeCabang("");
			cb.setNamaCabang("");
			cb.setItemList(false);
			listCabangMstDto.add(cb);
		}
		
	}
	
	@Command("closeCabang")
	@NotifyChange({"listCabangMstDtoChoose","listCabangMstDto","visibleCabang","listCabangMstDto","cabangMstDto"})
	public void closeCabang() {
		listCabangMstDtoChoose = new ArrayList<CabangMstDto>();
		listCabangMstDto = new ArrayList<CabangMstDto>();
		cabangMstDto = new CabangMstDto();
		setVisibleCabang(false);
	}
	
	@Command("chooseCabang")
	@NotifyChange({ "userProfileDto","visibleCabang","listCabangMstDto","cabangMstDto"})
	public void chooseCabang() {
		String temStrKodCabang = "";
		String temStrCabang = "";
		if (listCabangMstDtoChoose.size() != 0) {
			for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
				if (i == 0) {
					temStrCabang = temStrCabang+listCabangMstDtoChoose.get(i).getNamaCabang();
				}else {
					temStrCabang = temStrCabang+", "+listCabangMstDtoChoose.get(i).getNamaCabang();
				}
				if (i == 0) {
					temStrKodCabang = temStrKodCabang+listCabangMstDtoChoose.get(i).getKodeCabang();
				}else {
					temStrKodCabang = temStrKodCabang+", "+listCabangMstDtoChoose.get(i).getKodeCabang();
				}
			}
		}
		userProfileDto.setKodeCabang(temStrKodCabang);
		userProfileDto.setNamaCabang(temStrCabang);
		setVisibleCabang(false);
	}
	
	@Command("showAuthority")
	@NotifyChange({"keySearchAuthority","visibleAuthority","listAuthorityMstDto","authorityMstDto"})
	public void showAuthority() {
		keySearchAuthority = "";
		listAuthorityMstDto = authorityMstSvc.searchAllAuthorityActive(keySearchAuthority);
		if (userProfileDto.getAuthorityId() != null) {
			List<AuthorityMstDto> lauTemp = new ArrayList<AuthorityMstDto>();
			for (int i = 0; i < listAuthorityMstDto.size(); i++) {
				boolean sama = false;
				if (userProfileDto.getAuthorityName().equals(listAuthorityMstDto.get(i).getAuthorityName())) {
					sama = true;
				}
				if (sama = false) {
					AuthorityMstDto au = new AuthorityMstDto();
					au.setAuthorityId(listAuthorityMstDto.get(i).getAuthorityId());
					au.setAuthorityName(listAuthorityMstDto.get(i).getAuthorityName());
					au.setItemList(true);
					au.setChoose(true);
					lauTemp.add(au);
				}else {
					AuthorityMstDto au = new AuthorityMstDto();
					au.setAuthorityId(listAuthorityMstDto.get(i).getAuthorityId());
					au.setAuthorityName(listAuthorityMstDto.get(i).getAuthorityName());
					au.setItemList(true);
					au.setChoose(false);
					lauTemp.add(au);
				}
			}
			listAuthorityMstDto = new ArrayList<AuthorityMstDto>();
			listAuthorityMstDto = lauTemp;
			AuthorityMstDto au = new AuthorityMstDto();
			au.setAuthorityId(0);
			au.setAuthorityName("");
			au.setItemList(false);
			listAuthorityMstDto.add(au);
		}
		setVisibleAuthority(true);
	}
	@Command("showSearchAuthority")
	@NotifyChange({ "visibleAuthority","listAuthorityMstDto","authorityMstDto"})
	public void showSearchAuthority() {
		listAuthorityMstDto = authorityMstSvc.searchAllAuthorityActive(keySearchAuthority);
		if (userProfileDto.getAuthorityId() != null) {
			authorityMstDto.setAuthorityId(Integer.valueOf(userProfileDto.getAuthorityId()));
			authorityMstDto.setAuthorityName(userProfileDto.getAuthorityName());
		}
		if (authorityMstDto.getAuthorityId() != 0) {
			List<AuthorityMstDto> lauTemp = new ArrayList<AuthorityMstDto>();
			for (int i = 0; i < listAuthorityMstDto.size(); i++) {
				boolean sama = false;
				if (listAuthorityMstDto.get(i).getAuthorityId() == authorityMstDto.getAuthorityId()) {
					sama = true;
				}
				if (sama = false) {
					AuthorityMstDto au = new AuthorityMstDto();
					au.setAuthorityId(listAuthorityMstDto.get(i).getAuthorityId());
					au.setAuthorityName(listAuthorityMstDto.get(i).getAuthorityName());
					au.setChoose(true);
					au.setItemList(true);
					lauTemp.add(au);
				}else {
					AuthorityMstDto au = new AuthorityMstDto();
					au.setAuthorityId(listAuthorityMstDto.get(i).getAuthorityId());
					au.setAuthorityName(listAuthorityMstDto.get(i).getAuthorityName());
					au.setChoose(false);
					au.setItemList(true);
					lauTemp.add(au);
				}
			}
			listAuthorityMstDto = new ArrayList<AuthorityMstDto>();
			listAuthorityMstDto = lauTemp;
			AuthorityMstDto au = new AuthorityMstDto();
			au.setAuthorityId(0);
			au.setAuthorityName("");
			au.setItemList(false);
			listAuthorityMstDto.add(au);
		}
	}
	
	@Command("closeAuthority")
	@NotifyChange({ "visibleAuthority","listAuthorityMstDto","authorityMstDto"})
	public void closeAuthority() {
		authorityMstDto = new AuthorityMstDto();
		setVisibleAuthority(false);
	}
	
	@Command("chooseAuthority")
	@NotifyChange({ "userProfileDto","visibleAuthority","listAuthorityMstDto","authorityMstDto"})
	public void chooseAuthority() {
		if (authorityMstDto.getAuthorityId() != 0) {
			userProfileDto.setAuthorityId(String.valueOf(authorityMstDto.getAuthorityId()));
			userProfileDto.setAuthorityName(authorityMstDto.getAuthorityName());
			setVisibleAuthority(false);
		}else {
			Messagebox.show("Pilih authority terlebih dahulu", "MESSAGE", null, null, null);
		}
		
	}
	@Command("pageRegional")
	@NotifyChange("pageRegional")
	public void pageRegional() {
		setPageRegional(getPageRegional());
	}
	@Command("pageCabang")
	@NotifyChange("pageCabang")
	public void pageCabang() {
		setPageCabang(getPageCabang());
	}
	@Command("pageAuthority")
	@NotifyChange("pageAuthority")
	public void pageAuthority() {
		setPageAuthority(getPageAuthority());
	}
}