package vmd;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.record.DSFRecord;
import org.hibernate.cache.spi.access.RegionAccessStrategy;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;

import dto.CabangMstDto;
import dto.FormsDto;
import dto.MasterRegionalDto;
import dto.UserProfileDto;
import pagevmd.NavigationVmd;
import service.CabangMstSvc;
import service.FormsSvc;
import service.MasterRegionalSvc;
import service.UserProfileSvc;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class FormsVmd extends NavigationVmd{

	@WireVariable
	private FormsSvc formsSvc;
	
	
	
	private List<FormsDto> listFormsDto = new ArrayList<FormsDto>();
	private FormsDto formsDto;
	private int page = 10;
	private String keySearch = "";
	private boolean visibleAddEdit = false;
	private boolean fieldFormId = false;
	
	

	public List<FormsDto> getListFormsDto() {
		return listFormsDto;
	}

	public void setListFormsDto(List<FormsDto> listFormsDto) {
		this.listFormsDto = listFormsDto;
	}

	public FormsDto getFormsDto() {
		return formsDto;
	}

	public void setFormsDto(FormsDto formsDto) {
		this.formsDto = formsDto;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getKeySearch() {
		return keySearch;
	}

	public void setKeySearch(String keySearch) {
		this.keySearch = keySearch;
	}

	public boolean isVisibleAddEdit() {
		return visibleAddEdit;
	}

	public void setVisibleAddEdit(boolean visibleAddEdit) {
		this.visibleAddEdit = visibleAddEdit;
	}

	public boolean isFieldFormId() {
		return fieldFormId;
	}

	public void setFieldFormId(boolean fieldFormId) {
		this.fieldFormId = fieldFormId;
	}

	@Init
	public void load() {
		keySearch = "";
		Sessions.getCurrent().removeAttribute("sessionMobileTracking");
		listFormsDto = formsSvc.findFormsWithSearch(keySearch);
	}
	
	@Command("page")
	@NotifyChange("page")
	public void page() {
		setPage(getPage());
	}
	
	@Command("add")
	@NotifyChange({"visibleAddEdit","formsDto","fieldFormId"})
	public void add() {
		formsDto = new FormsDto();
		setFieldFormId(false);
		setVisibleAddEdit(true);
	}
	
	@Command("search")
	@NotifyChange({ "listFormsDto" })
	public void search() {
		listFormsDto = formsSvc.findFormsWithSearch(keySearch);
	}
	
	@Command("close")
	@NotifyChange({ "visibleAddEdit","formsDto" })
	public void close() {
		formsDto = new FormsDto();
		setVisibleAddEdit(false);
	}
	
	@Command("save")
	@NotifyChange({ "formsDto", "listFormsDto", "fieldFormId", "visibleAddEdit"})
	public void save() throws NoSuchAlgorithmException{
		FormsDto f = new FormsDto();
		f = formsSvc.findFormsById(formsDto.getFormId());
		if (f.getFormId() == null) {
			if (formsDto.getFormId() == null ) {
				Messagebox.show("Form ID harus terisi", "MESSAGE", null, null, null);
			} else if (formsDto.getFormName() == null ) {
				Messagebox.show("Form Name harus terisi", "MESSAGE", null, null, null);
			} else if (formsDto.getFormLabel() == null ) {
				Messagebox.show("Form label harus terisi", "MESSAGE", null, null, null);
			} else if (formsDto.getFormOrder() == null ) {
				Messagebox.show("Form order harus terisi", "MESSAGE", null, null, null);
			} else
				formsSvc.saveForms(formsDto.getFormId(), formsDto.getFormName(), formsDto.getFormLabel(), formsDto.getFormOrder());
				load();
				Messagebox.show("Data form berhasil disimpan", "MESSAGE", null, null, null);
				formsDto = new FormsDto();
				setVisibleAddEdit(false);
		} else {
			if (formsDto.getFormId() == null ) {
				Messagebox.show("Form ID harus terisi", "MESSAGE", null, null, null);
			} else if (formsDto.getFormName() == null ) {
				Messagebox.show("Form Name harus terisi", "MESSAGE", null, null, null);
			} else if (formsDto.getFormLabel() == null ) {
				Messagebox.show("Form label harus terisi", "MESSAGE", null, null, null);
			} else if (formsDto.getFormOrder() == null ) {
				Messagebox.show("Form order harus terisi", "MESSAGE", null, null, null);
			} else {
				formsSvc.updateForms(formsDto.getFormName(), formsDto.getFormLabel(), formsDto.getFormOrder(), formsDto.getFormId());
				load();
				Messagebox.show("Data form berhasil disimpan", "MESSAGE", null, null, null);
				formsDto = new FormsDto();
				setVisibleAddEdit(false);
			}
		}
	}
	
	
	@Command("edit")
	@NotifyChange({"fieldFormId", "visibleAddEdit"})
	public void edit() {
		if(formsDto == null){
			Messagebox.show("Choose your data first !","Message",null,null,null);
		} else{
			setFieldFormId(true);
			setVisibleAddEdit(true);
		}
	}
	
	
	@Command("delete")
	@NotifyChange({  "formsDto", "listFormsDto" })
	public void delete() {
		if (formsDto.getFormId() == null) {
			Messagebox.show("Choose your data first !","Message",null,null,null);
		} else {
			Messagebox.show("Are you sure to delete ?", "perhatian",
					new Button[] { Button.YES, Button.NO },
					Messagebox.QUESTION, Button.NO,
					new EventListener<Messagebox.ClickEvent>() {
						@Override
						public void onEvent(ClickEvent event) throws Exception {
							if (Messagebox.ON_YES.equals(event.getName())) {
								formsSvc.deleteForms(formsDto.getFormId());
								BindUtils.postNotifyChange(null, null,
										FormsVmd.this,
										"listFormsDto");
								BindUtils.postNotifyChange(null, null,
										FormsVmd.this,
										"formsDto");
								listFormsDto = formsSvc.findFormsWithSearch("");
								Messagebox.show("Data berhasil di hapus", "MESSAGE", null, null, null);
								formsDto = new FormsDto();
							}
						}
					});
		}
	}
	
	@Command("upOrder")
	@NotifyChange({"formsDto", "listFormsDto"})
	public void upOrder() {
		if (formsDto == null) {
			Messagebox.show("Choose your data first !","Message",null,null,null);
		}else {
			int order = Integer.valueOf(formsDto.getFormOrder());
			order = order + 1;
			formsSvc.upDownOrder(order, formsDto.getFormId());
			listFormsDto = formsSvc.findFormsWithSearch("");
		}
	}
	
	@Command("downOrder")
	@NotifyChange({"formsDto", "listFormsDto"})
	public void downOrder() {
		if (formsDto == null) {
			Messagebox.show("Choose your data first !","Message",null,null,null);
		}else {
			int order = Integer.valueOf(formsDto.getFormOrder());
			order = order - 1;
			formsSvc.upDownOrder(order, formsDto.getFormId());
			listFormsDto = formsSvc.findFormsWithSearch("");
		}
	}

}
