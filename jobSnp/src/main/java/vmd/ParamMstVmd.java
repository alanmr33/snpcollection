package vmd;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.record.DSFRecord;
import org.hibernate.cache.spi.access.RegionAccessStrategy;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;

import dto.CabangMstDto;
import dto.MasterRegionalDto;
import dto.ParamMstDto;
import dto.UserProfileDto;
import pagevmd.NavigationVmd;
import service.CabangMstSvc;
import service.MasterRegionalSvc;
import service.ParamMstSvc;
import service.UserProfileSvc;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class ParamMstVmd extends NavigationVmd{

	@WireVariable
	private ParamMstSvc paramMstSvc;
	
	
	private List<ParamMstDto> listParam = new ArrayList<ParamMstDto>();
	private List<ParamMstDto> listParamPil = new ArrayList<ParamMstDto>();
	private ParamMstDto paramMstDto;
	private ParamMstDto paramMstDtoPil;
	private int page = 10;
	private String keySearch = "";
	private boolean visibleAddEdit = false;
	private boolean fieldParamId = false;
	private boolean visiblePilParam = false;
	private String keySearchPilParam = "";
	
	
	
	public String getKeySearchPilParam() {
		return keySearchPilParam;
	}

	public void setKeySearchPilParam(String keySearchPilParam) {
		this.keySearchPilParam = keySearchPilParam;
	}

	public boolean isVisiblePilParam() {
		return visiblePilParam;
	}

	public void setVisiblePilParam(boolean visiblePilParam) {
		this.visiblePilParam = visiblePilParam;
	}

	public List<ParamMstDto> getListParamPil() {
		return listParamPil;
	}

	public void setListParamPil(List<ParamMstDto> listParamPil) {
		this.listParamPil = listParamPil;
	}

	public ParamMstDto getParamMstDtoPil() {
		return paramMstDtoPil;
	}

	public void setParamMstDtoPil(ParamMstDto paramMstDtoPil) {
		this.paramMstDtoPil = paramMstDtoPil;
	}

	public List<ParamMstDto> getListParam() {
		return listParam;
	}

	public void setListParam(List<ParamMstDto> listParam) {
		this.listParam = listParam;
	}

	public ParamMstDto getParamMstDto() {
		return paramMstDto;
	}

	public void setParamMstDto(ParamMstDto paramMstDto) {
		this.paramMstDto = paramMstDto;
	}

	public boolean isFieldParamId() {
		return fieldParamId;
	}

	public void setFieldParamId(boolean fieldParamId) {
		this.fieldParamId = fieldParamId;
	}

	public boolean isVisibleAddEdit() {
		return visibleAddEdit;
	}

	public void setVisibleAddEdit(boolean visibleAddEdit) {
		this.visibleAddEdit = visibleAddEdit;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getKeySearch() {
		return keySearch;
	}

	public void setKeySearch(String keySearch) {
		this.keySearch = keySearch;
	}
	

	@Init
	public void load() {
		keySearch = "";
		Sessions.getCurrent().removeAttribute("sessionMobileTracking");
		listParam = paramMstSvc.findParamWithSearch(keySearch);
	}
	
	@Command("page")
	@NotifyChange("page")
	public void page() {
		setPage(getPage());
	}
	
	@Command("add")
	@NotifyChange({"visibleAddEdit","cabangMstDto","fieldParamId"})
	public void add() {
		paramMstDto = new ParamMstDto();
		setFieldParamId(false);
		setVisibleAddEdit(true);
	}
	
	@Command("search")
	@NotifyChange({ "listParam" })
	public void search() {
		listParam = paramMstSvc.findParamWithSearch(keySearch);
	}
	
	@Command("close")
	@NotifyChange({ "visibleAddEdit","listParam" })
	public void close() {
		paramMstDto = new ParamMstDto();
		setVisibleAddEdit(false);
	}
	
	@Command("save")
	@NotifyChange({ "visibleAddEdit","fieldParamId","paramMstDto","listParam" })
	public void save() throws NoSuchAlgorithmException{
		ParamMstDto par = new ParamMstDto();
		par = paramMstSvc.findOneParam(paramMstDto.getParamId());
		if (par.getParamId() == null) {
			setFieldParamId(false);
			if (paramMstDto.getParamId() == null ) {
				Messagebox.show("Param ID harus terisi", "MESSAGE", null, null, null);
			} else if (paramMstDto.getCondition() == null ) {
				Messagebox.show("Condition param harus terisi", "MESSAGE", null, null, null);
			} else if (paramMstDto.getLevelParam() == null ) {
				Messagebox.show("Level param harus terisi", "MESSAGE", null, null, null);
			} else if (paramMstDto.getParentId() == null ) {
				Messagebox.show("Parent ID harus terisi", "MESSAGE", null, null, null);
			} else{
				paramMstSvc.saveParam(paramMstDto.getParamId(), paramMstDto.getCondition(), paramMstDto.getLevelParam(), paramMstDto.getParentId());
				load();
				Messagebox.show("Data parameter berhasil disimpan", "MESSAGE", null, null, null);
				setVisibleAddEdit(false);
			}
		} else {
			if (paramMstDto.getParamId() == null ) {
				Messagebox.show("Param ID harus terisi", "MESSAGE", null, null, null);
			} else if (paramMstDto.getCondition() == null ) {
				Messagebox.show("Condition param harus terisi", "MESSAGE", null, null, null);
			} else if (paramMstDto.getLevelParam() == null ) {
				Messagebox.show("Level param harus terisi", "MESSAGE", null, null, null);
			} else if (paramMstDto.getParentId() == null ) {
				Messagebox.show("Parent ID harus terisi", "MESSAGE", null, null, null);
			} else{
					setFieldParamId(true);
					paramMstSvc.updateParam(paramMstDto.getCondition(), paramMstDto.getLevelParam(), paramMstDto.getParentId(),paramMstDto.getParamId());
					load();
					setVisibleAddEdit(false);
					Messagebox.show("Data parameter berhasil diubah", "MESSAGE", null, null, null);
				}
			}
			listParam = paramMstSvc.findParamWithSearch("");
			paramMstDto = new ParamMstDto();
	}
	
	@Command("edit")
	@NotifyChange({"fieldParamId","visibleAddEdit","masterRegionalDto"})
	public void edit() {
		if(paramMstDto==null){
			Messagebox.show("Choose your data first !","Message",null,null,null);
		} else{
			setFieldParamId(true);
			setVisibleAddEdit(true);
		}
	}
	@Command("delete")
	@NotifyChange({  "paramMstDto", "listParam" })
	public void delete() {
		if (paramMstDto.getParamId()== null) {
			Messagebox.show("Choose your data first !","Message",null,null,null);
		} else {
			Messagebox.show("Are you sure to delete ?", "perhatian",
					new Button[] { Button.YES, Button.NO },
					Messagebox.QUESTION, Button.NO,
					new EventListener<Messagebox.ClickEvent>() {
						@Override
						public void onEvent(ClickEvent event) throws Exception {
							if (Messagebox.ON_YES.equals(event.getName())) {
								paramMstSvc.deleteParam(paramMstDto.getParamId());
								BindUtils.postNotifyChange(null, null,
										ParamMstVmd.this,
										"listParam");
								BindUtils.postNotifyChange(null, null,
										ParamMstVmd.this,
										"paramMstDto");
								listParam = paramMstSvc.findParamWithSearch("");
								Messagebox.show("Data parameter berhasil di hapus", "MESSAGE", null, null, null);
								paramMstDto = new ParamMstDto();
							}
						}
					});
		}
	}
	
	@Command("pilParam")
	@NotifyChange({"listParamPil", "paramMstDtoPil","visiblePilParam"})
	public void pilParam() {
		paramMstDtoPil = new ParamMstDto();
		listParamPil = paramMstSvc.findParamWithSearch("");
		/*List<ParamMstDto> lp = new ArrayList<ParamMstDto>();
		if (paramMstDto.getParamId() != null) {
			for (int i = 0; i < listParamPil.size(); i++) {
				ParamMstDto pm = new ParamMstDto();
				if (listParamPil.get(i).getParamId().equalsIgnoreCase(paramMstDto.getParamId())) {
					pm.setParamId(listParamPil.get(i).getParamId());
					pm.setCondition(listParamPil.get(i).getCondition());
					pm.setLevelParam(listParamPil.get(i).getLevelParam());
					pm.setParentId(listParamPil.get(i).getParentId());
					pm.setChoose(true);
				}else {
					pm.setParamId(listParamPil.get(i).getParamId());
					pm.setCondition(listParamPil.get(i).getCondition());
					pm.setLevelParam(listParamPil.get(i).getLevelParam());
					pm.setParentId(listParamPil.get(i).getParentId());
					pm.setChoose(false);
				}
				lp.add(pm);
			}
			listParamPil = lp;
		}*/
		
		setVisiblePilParam(true);
	}
	
	@Command("searchInPilParam")
	@NotifyChange({"listParamPil", "paramMstDtoPil","visiblePilParam"})
	public void searchInPilParam() {
		listParamPil = paramMstSvc.findParamWithSearch(keySearchPilParam);
	}
	@Command("choosePilParam")
	@NotifyChange({"paramMstDto","listParamPil", "paramMstDtoPil","visiblePilParam"})
	public void choosePilParam() {
		if (paramMstDtoPil != null) {
			paramMstDto.setParentId(paramMstDtoPil.getParamId());
			setVisiblePilParam(false);
		}else {
			Messagebox.show("Pilih data terlebih dahulu", "MESSAGE", null, null, null);
		}
		
	}
	
	@Command("closePilParam")
	@NotifyChange({"listParamPil", "paramMstDtoPil","visiblePilParam"})
	public void closePilParam() {
		paramMstDtoPil = new ParamMstDto();
		setVisiblePilParam(false);
	}

}
