package vmd;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mobile.Notification;

import org.apache.poi.ss.usermodel.DateUtil;
import org.exolab.castor.types.DateTime;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

import dao.HandlingDao;
import dao.PaymentDao;
import dto.AksesRegionCabangDto;
import dto.CabangMstDto;
import dto.CollectionDetailDto;
import dto.MasterHandlingDto;
import dto.MasterRegionalDto;
import dto.UserProfileDto;
import pagevmd.NavigationVmd;
import service.AksesRegionCabangSvc;
import service.CabangMstSvc;
import service.CollectionDetailSvc;
import service.HandlingSvc;
import service.MasterHandlingSvc;
import service.MasterRegionalSvc;
import service.PaymentSvc;
import service.UserProfileSvc;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class TaskAdjustmentVmd extends NavigationVmd{
	@WireVariable
	private UserProfileSvc userProfileSvc;
	
	@WireVariable
	private MasterRegionalSvc masterRegionalSvc;
	
	@WireVariable
	private CabangMstSvc cabangMstSvc;
	
	@WireVariable
	private CollectionDetailSvc collectionDetailSvc;
	
	@WireVariable
	private AksesRegionCabangSvc aksesRegionCabangSvc;
	
	@WireVariable
	private PaymentSvc paymentSvc;
	
	@WireVariable
	private HandlingSvc handlingSvc;
	
	@WireVariable
	private MasterHandlingSvc masterHandlingSvc;
	
	private List<CollectionDetailDto> listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
	private CollectionDetailDto collectionDetailDto = new CollectionDetailDto();
	private List<UserProfileDto> listUserKolektor = new ArrayList<UserProfileDto>();
	private List<UserProfileDto> listUserKolektorChoose = new ArrayList<UserProfileDto>();
	private List<MasterRegionalDto> listRegionalDto = new ArrayList<MasterRegionalDto>();
	private List<MasterRegionalDto> listRegionalDtoChoose = new ArrayList<MasterRegionalDto>();
	private List<CabangMstDto> listCabangMstDto = new ArrayList<CabangMstDto>();
	private List<CabangMstDto> listCabangMstDtoChoose = new ArrayList<CabangMstDto>();
	private int pageRegional = 10;
	private int pageCabang = 10;
	private int pageKolektor = 10;
	private int pageMasterHandling = 10;
	private int page = 10;
	private String keySearch = "";
	private String keySearchregional = "";
	private String keySearchCabang = "";
	private String keySearchKolektor = "";
	private String stringArrayRegional = "";
	private String stringArrayCabang = "";
	private String stringArrayKolektor = "";
	private boolean visibleRegional;
	private boolean visibleCabang =false;
	private boolean visibleKolektor= false;
	private boolean visibleActionAdjustment = false;
	private boolean visibleCloseTask = false;
	private UserProfileDto upSession = new UserProfileDto();
	private String stsActionAdjustment = "";
	private String remarkActionAdjustment;
	private List<MasterRegionalDto> listAksesRegional = new ArrayList<MasterRegionalDto>();
	private List<CabangMstDto> listAksesCabang = new ArrayList<CabangMstDto>();
	private Date tglJanjiBayar;
	private boolean disTglByr = false;
	private boolean disPilHandling = false;
	private List<MasterHandlingDto> listMasterHandlingDto = new ArrayList<MasterHandlingDto>();
	private MasterHandlingDto masterHandlingDto = new MasterHandlingDto();
	private String keySearchMasterHandling = "";
	private boolean visibleMasterHandling = false;
	private int deposit = 0;
	
	
	
	
	public int getDeposit() {
		return deposit;
	}
	public void setDeposit(int deposit) {
		this.deposit = deposit;
	}
	public boolean isDisPilHandling() {
		return disPilHandling;
	}
	public void setDisPilHandling(boolean disPilHandling) {
		this.disPilHandling = disPilHandling;
	}
	public int getPageMasterHandling() {
		return pageMasterHandling;
	}
	public void setPageMasterHandling(int pageMasterHandling) {
		this.pageMasterHandling = pageMasterHandling;
	}
	public MasterHandlingDto getMasterHandlingDto() {
		return masterHandlingDto;
	}
	public void setMasterHandlingDto(MasterHandlingDto masterHandlingDto) {
		this.masterHandlingDto = masterHandlingDto;
	}
	public boolean isVisibleMasterHandling() {
		return visibleMasterHandling;
	}
	public void setVisibleMasterHandling(boolean visibleMasterHandling) {
		this.visibleMasterHandling = visibleMasterHandling;
	}
	public String getKeySearchMasterHandling() {
		return keySearchMasterHandling;
	}
	public void setKeySearchMasterHandling(String keySearchMasterHandling) {
		this.keySearchMasterHandling = keySearchMasterHandling;
	}
	public MasterHandlingSvc getMasterHandlingSvc() {
		return masterHandlingSvc;
	}
	public void setMasterHandlingSvc(MasterHandlingSvc masterHandlingSvc) {
		this.masterHandlingSvc = masterHandlingSvc;
	}
	public List<MasterHandlingDto> getListMasterHandlingDto() {
		return listMasterHandlingDto;
	}
	public void setListMasterHandlingDto(
			List<MasterHandlingDto> listMasterHandlingDto) {
		this.listMasterHandlingDto = listMasterHandlingDto;
	}
	public boolean isDisTglByr() {
		return disTglByr;
	}
	public void setDisTglByr(boolean disTglByr) {
		this.disTglByr = disTglByr;
	}
	public Date getTglJanjiBayar() {
		return tglJanjiBayar;
	}
	public void setTglJanjiBayar(Date tglJanjiBayar) {
		this.tglJanjiBayar = tglJanjiBayar;
	}
	public CollectionDetailDto getCollectionDetailDto() {
		return collectionDetailDto;
	}
	public void setCollectionDetailDto(CollectionDetailDto collectionDetailDto) {
		this.collectionDetailDto = collectionDetailDto;
	}
	public List<MasterRegionalDto> getListAksesRegional() {
		return listAksesRegional;
	}
	public void setListAksesRegional(List<MasterRegionalDto> listAksesRegional) {
		this.listAksesRegional = listAksesRegional;
	}
	public List<CabangMstDto> getListAksesCabang() {
		return listAksesCabang;
	}
	public void setListAksesCabang(List<CabangMstDto> listAksesCabang) {
		this.listAksesCabang = listAksesCabang;
	}
	public String getKeySearch() {
		return keySearch;
	}
	public void setKeySearch(String keySearch) {
		this.keySearch = keySearch;
	}
	public boolean isVisibleCloseTask() {
		return visibleCloseTask;
	}
	public void setVisibleCloseTask(boolean visibleCloseTask) {
		this.visibleCloseTask = visibleCloseTask;
	}
	public String getRemarkActionAdjustment() {
		return remarkActionAdjustment;
	}
	public void setRemarkActionAdjustment(String remarkActionAdjustment) {
		this.remarkActionAdjustment = remarkActionAdjustment;
	}
	public boolean isVisibleActionAdjustment() {
		return visibleActionAdjustment;
	}
	public void setVisibleActionAdjustment(boolean visibleActionAdjustment) {
		this.visibleActionAdjustment = visibleActionAdjustment;
	}
	public String getStsActionAdjustment() {
		return stsActionAdjustment;
	}
	public void setStsActionAdjustment(String stsActionAdjustment) {
		this.stsActionAdjustment = stsActionAdjustment;
	}
	public UserProfileDto getUpSession() {
		return upSession;
	}
	public void setUpSession(UserProfileDto upSession) {
		this.upSession = upSession;
	}
	public List<CollectionDetailDto> getListCollectionDetailDto() {
		return listCollectionDetailDto;
	}
	public void setListCollectionDetailDto(
			List<CollectionDetailDto> listCollectionDetailDto) {
		this.listCollectionDetailDto = listCollectionDetailDto;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public boolean isVisibleRegional() {
		return visibleRegional;
	}
	public void setVisibleRegional(boolean visibleRegional) {
		this.visibleRegional = visibleRegional;
	}
	public boolean isVisibleCabang() {
		return visibleCabang;
	}
	public void setVisibleCabang(boolean visibleCabang) {
		this.visibleCabang = visibleCabang;
	}
	public boolean isVisibleKolektor() {
		return visibleKolektor;
	}
	public void setVisibleKolektor(boolean visibleKolektor) {
		this.visibleKolektor = visibleKolektor;
	}
	public UserProfileSvc getUserProfileSvc() {
		return userProfileSvc;
	}
	public void setUserProfileSvc(UserProfileSvc userProfileSvc) {
		this.userProfileSvc = userProfileSvc;
	}
	public MasterRegionalSvc getMasterRegionalSvc() {
		return masterRegionalSvc;
	}
	public void setMasterRegionalSvc(MasterRegionalSvc masterRegionalSvc) {
		this.masterRegionalSvc = masterRegionalSvc;
	}
	public List<UserProfileDto> getListUserKolektor() {
		return listUserKolektor;
	}
	public void setListUserKolektor(List<UserProfileDto> listUserKolektor) {
		this.listUserKolektor = listUserKolektor;
	}
	public List<UserProfileDto> getListUserKolektorChoose() {
		return listUserKolektorChoose;
	}
	public void setListUserKolektorChoose(
			List<UserProfileDto> listUserKolektorChoose) {
		this.listUserKolektorChoose = listUserKolektorChoose;
	}
	public List<MasterRegionalDto> getListRegionalDto() {
		return listRegionalDto;
	}
	public void setListRegionalDto(List<MasterRegionalDto> listRegionalDto) {
		this.listRegionalDto = listRegionalDto;
	}
	public List<MasterRegionalDto> getListRegionalDtoChoose() {
		return listRegionalDtoChoose;
	}
	public void setListRegionalDtoChoose(
			List<MasterRegionalDto> listRegionalDtoChoose) {
		this.listRegionalDtoChoose = listRegionalDtoChoose;
	}
	public List<CabangMstDto> getListCabangMstDto() {
		return listCabangMstDto;
	}
	public void setListCabangMstDto(List<CabangMstDto> listCabangMstDto) {
		this.listCabangMstDto = listCabangMstDto;
	}
	public List<CabangMstDto> getListCabangMstDtoChoose() {
		return listCabangMstDtoChoose;
	}
	public void setListCabangMstDtoChoose(List<CabangMstDto> listCabangMstDtoChoose) {
		this.listCabangMstDtoChoose = listCabangMstDtoChoose;
	}
	public int getPageRegional() {
		return pageRegional;
	}
	public void setPageRegional(int pageRegional) {
		this.pageRegional = pageRegional;
	}
	public int getPageCabang() {
		return pageCabang;
	}
	public void setPageCabang(int pageCabang) {
		this.pageCabang = pageCabang;
	}
	public int getPageKolektor() {
		return pageKolektor;
	}
	public void setPageKolektor(int pageKolektor) {
		this.pageKolektor = pageKolektor;
	}
	public String getKeySearchregional() {
		return keySearchregional;
	}
	public void setKeySearchregional(String keySearchregional) {
		this.keySearchregional = keySearchregional;
	}
	public String getKeySearchCabang() {
		return keySearchCabang;
	}
	public void setKeySearchCabang(String keySearchCabang) {
		this.keySearchCabang = keySearchCabang;
	}
	public String getKeySearchKolektor() {
		return keySearchKolektor;
	}
	public void setKeySearchKolektor(String keySearchKolektor) {
		this.keySearchKolektor = keySearchKolektor;
	}
	public String getStringArrayRegional() {
		return stringArrayRegional;
	}
	public void setStringArrayRegional(String stringArrayRegional) {
		this.stringArrayRegional = stringArrayRegional;
	}
	public String getStringArrayCabang() {
		return stringArrayCabang;
	}
	public void setStringArrayCabang(String stringArrayCabang) {
		this.stringArrayCabang = stringArrayCabang;
	}
	public String getStringArrayKolektor() {
		return stringArrayKolektor;
	}
	public void setStringArrayKolektor(String stringArrayKolektor) {
		this.stringArrayKolektor = stringArrayKolektor;
	}
	
	@Init
	public void load() {
		tglJanjiBayar = new Date();
		upSession = (UserProfileDto) Sessions.getCurrent().getAttribute("user");
		List<AksesRegionCabangDto> lr = aksesRegionCabangSvc.findRegionByUserId(upSession.getUserId(), "");
		List<AksesRegionCabangDto> lc = aksesRegionCabangSvc.fintCabangByUserId(upSession.getUserId(), "");
		if (lr.size() != 0) {
			for (int i = 0; i < lr.size(); i++) {
				MasterRegionalDto mr = new MasterRegionalDto();
				mr.setKodeRegion(lr.get(i).getKodeRegion());
				mr.setNamaRegion(lr.get(i).getNamaRegional());
				listAksesRegional.add(mr);
			}
		}
		if (lc.size() != 0) {
			for (int i = 0; i < lc.size(); i++) {
				CabangMstDto cb= new CabangMstDto();
				cb.setKodeCabang(lc.get(i).getKodeCabang());
				cb.setNamaCabang(lc.get(i).getNamaCabang());
				listAksesCabang.add(cb);
			}
		}
	}
	
	@Command("showRegional")
	@NotifyChange({"keySearchregional","visibleRegional","listRegionalDto","listRegionalDtoChoose"})
	public void showRegional() {
		keySearchregional = "";
		listRegionalDto = new ArrayList<MasterRegionalDto>();
		List<AksesRegionCabangDto> listTempAkses = new ArrayList<AksesRegionCabangDto>();
		listTempAkses = aksesRegionCabangSvc.findRegionByUserId(upSession.getUserId(),keySearchregional);
		if (listTempAkses.size() != 0) {
			for (int i = 0; i < listTempAkses.size(); i++) {
				MasterRegionalDto mr = new MasterRegionalDto();
				mr.setKodeRegion(listTempAkses.get(i).getKodeRegion());
				mr.setNamaRegion(listTempAkses.get(i).getNamaRegional());
				mr.setItemList(true);
				listRegionalDto.add(mr);
			}
		}
		if (listRegionalDtoChoose.size() != 0 ) {
			List<MasterRegionalDto> listTemp = new ArrayList<MasterRegionalDto>();
			for (int i = 0; i < listRegionalDto.size(); i++) {
				boolean sama = false;
				for (int j = 0; j < listRegionalDtoChoose.size(); j++) {
					if (listRegionalDto.get(i).getKodeRegion().equals(listRegionalDtoChoose.get(j).getKodeRegion())) {
						sama = true;
					}
				}
				if (sama == true) {
					MasterRegionalDto temppDto = new MasterRegionalDto();
					temppDto.setKodeRegion(listRegionalDto.get(i).getKodeRegion());
					temppDto.setNamaRegion(listRegionalDto.get(i).getNamaRegion());
					temppDto.setChoose(true);
					temppDto.setItemList(true);
					listTemp.add(temppDto);
				}else {
					MasterRegionalDto temppDto = new MasterRegionalDto();
					temppDto.setKodeRegion(listRegionalDto.get(i).getKodeRegion());
					temppDto.setNamaRegion(listRegionalDto.get(i).getNamaRegion());
					temppDto.setChoose(false);
					temppDto.setItemList(true);
					listTemp.add(temppDto);
				}
			}
			listRegionalDto = listTemp;
		}
		MasterRegionalDto mre = new MasterRegionalDto();
		mre.setKodeRegion("");
		mre.setNamaRegion("");
		mre.setItemList(false);
		listRegionalDto.add(mre);
		setVisibleRegional(true);
	}
	
	@Command("showRegionalSearch")
	@NotifyChange({"visibleRegional","listRegionalDto","listRegionalDtoChoose"})
	public void showRegionalSearch() {
		listRegionalDto = new ArrayList<MasterRegionalDto>();
		List<AksesRegionCabangDto> listTempAkses = new ArrayList<AksesRegionCabangDto>();
		listTempAkses = aksesRegionCabangSvc.findRegionByUserId(upSession.getUserId(),keySearchregional);
		if (listTempAkses.size() != 0) {
			for (int i = 0; i < listTempAkses.size(); i++) {
				MasterRegionalDto mr = new MasterRegionalDto();
				mr.setKodeRegion(listTempAkses.get(i).getKodeRegion());
				mr.setNamaRegion(listTempAkses.get(i).getNamaRegional());
				mr.setItemList(true);
				listRegionalDto.add(mr);
			}
		}
		if (listRegionalDtoChoose.size() != 0 ) {
			List<MasterRegionalDto> listTemp = new ArrayList<MasterRegionalDto>();
			for (int i = 0; i < listRegionalDto.size(); i++) {
				boolean sama = false;
				for (int j = 0; j < listRegionalDtoChoose.size(); j++) {
					if (listRegionalDto.get(i).getKodeRegion().equals(listRegionalDtoChoose.get(j).getKodeRegion())) {
						sama = true;
					}
				}
				if (sama == true) {
					MasterRegionalDto temppDto = new MasterRegionalDto();
					temppDto.setKodeRegion(listRegionalDto.get(i).getKodeRegion());
					temppDto.setNamaRegion(listRegionalDto.get(i).getNamaRegion());
					temppDto.setChoose(true);
					temppDto.setItemList(true);
					listTemp.add(temppDto);
				}else {
					MasterRegionalDto temppDto = new MasterRegionalDto();
					temppDto.setKodeRegion(listRegionalDto.get(i).getKodeRegion());
					temppDto.setNamaRegion(listRegionalDto.get(i).getNamaRegion());
					temppDto.setChoose(false);
					temppDto.setItemList(true);
					listTemp.add(temppDto);
				}
			}
			listRegionalDto = listTemp;
		}
		MasterRegionalDto mre = new MasterRegionalDto();
		mre.setKodeRegion("");
		mre.setNamaRegion("");
		mre.setItemList(false);
		listRegionalDto.add(mre);
		setVisibleRegional(true);
	}
	
	@Command("chooseRegional")
	@NotifyChange({"listRegionalDtoChoose","stringArrayRegional","visibleRegional","stringArrayKolektor","stringArrayCabang","stringArrayRegional","listCabangMstDtoChoose","listUserKolektorChoose"})
	public void chooseRegional() {
		if (listRegionalDtoChoose.size() != 0) {
			stringArrayRegional = "";
			for (int i = 0; i < listRegionalDtoChoose.size(); i++) {
				if (i == 0) {
					stringArrayRegional = stringArrayRegional+listRegionalDtoChoose.get(i).getNamaRegion();
				}else {
					stringArrayRegional = stringArrayRegional+", "+listRegionalDtoChoose.get(i).getNamaRegion();
				}
			}
		}else {
			listRegionalDtoChoose = new ArrayList<MasterRegionalDto>();
			stringArrayRegional = "";
		}
		stringArrayKolektor = "";
		stringArrayCabang = "";
		listCabangMstDtoChoose = new ArrayList<CabangMstDto>();
		listUserKolektorChoose = new ArrayList<UserProfileDto>();
		setVisibleRegional(false);
	}
	
	@Command("search")
	@NotifyChange({"visibleRegional","listCollectionDetailDto"})
	public void search() {
		listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
		if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() == 0 && listUserKolektorChoose.size() == 0) {
			System.out.println("Masuk ke reg doang");
			listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
			for (int i = 0; i < listRegionalDtoChoose.size(); i++) {
				for (int h = 0; h < listAksesCabang.size(); h++) {
					System.out.println("loop ke "+i);
					List<CollectionDetailDto> listTemp = new ArrayList<CollectionDetailDto>();
					listTemp = collectionDetailSvc.selectForTaskAdjustmentByKodeRegionalKodeCabang(listRegionalDtoChoose.get(i).getKodeRegion(),listAksesCabang.get(h).getKodeCabang(),keySearch);
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							CollectionDetailDto cd = new CollectionDetailDto();
							cd.setOrderId(listTemp.get(j).getOrderId());
							cd.setCustomerName(listTemp.get(j).getCustomerName());
							cd.setAddress(listTemp.get(j).getAddress());
							cd.setNamaKaryawan(listTemp.get(j).getNamaKaryawan());
							cd.setTagihan(listTemp.get(j).getTagihan());
							cd.setNotes(listTemp.get(j).getNotes());
							cd.setContractNo(listTemp.get(j).getContractNo());
							cd.setInstallmentNo(listTemp.get(j).getInstallmentNo());
							listCollectionDetailDto.add(cd);
						}
					}
				}
			}
		}else if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() != 0 && listUserKolektorChoose.size() == 0) {
			listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
			for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
				List<CollectionDetailDto> listTemp = new ArrayList<CollectionDetailDto>();
				listTemp = collectionDetailSvc.selectForTaskAdjustmentByKodeCabang(listCabangMstDtoChoose.get(i).getKodeCabang(), keySearch);
				if (listTemp.size() != 0) {
					for (int j = 0; j < listTemp.size(); j++) {
						CollectionDetailDto cd = new CollectionDetailDto();
						cd.setOrderId(listTemp.get(j).getOrderId());
						cd.setCustomerName(listTemp.get(j).getCustomerName());
						cd.setAddress(listTemp.get(j).getAddress());
						cd.setNamaKaryawan(listTemp.get(j).getNamaKaryawan());
						cd.setTagihan(listTemp.get(j).getTagihan());
						cd.setNotes(listTemp.get(j).getNotes());
						cd.setContractNo(listTemp.get(j).getContractNo());
						cd.setInstallmentNo(listTemp.get(j).getInstallmentNo());
						listCollectionDetailDto.add(cd);
					}
				}
			}
		}else if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() == 0 && listUserKolektorChoose.size() != 0) {
			listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
			for (int i = 0; i < listAksesCabang.size(); i++) {
				for (int h = 0; h < listUserKolektorChoose.size(); h++) {
					List<CollectionDetailDto> listTemp = new ArrayList<CollectionDetailDto>();
					listTemp = collectionDetailSvc.selectForTaskAdjustmentByKodeCabangKodeKol(listAksesCabang.get(i).getKodeCabang(), listUserKolektorChoose.get(h).getKodeKolektor(), keySearch);
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							CollectionDetailDto cd = new CollectionDetailDto();
							cd.setOrderId(listTemp.get(j).getOrderId());
							cd.setCustomerName(listTemp.get(j).getCustomerName());
							cd.setAddress(listTemp.get(j).getAddress());
							cd.setNamaKaryawan(listTemp.get(j).getNamaKaryawan());
							cd.setTagihan(listTemp.get(j).getTagihan());
							cd.setNotes(listTemp.get(j).getNotes());
							cd.setContractNo(listTemp.get(j).getContractNo());
							cd.setInstallmentNo(listTemp.get(j).getInstallmentNo());
							listCollectionDetailDto.add(cd);
						}
					}
				}
			}
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() != 0 && listUserKolektorChoose.size() == 0) {
			listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
			for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
				List<CollectionDetailDto> listTemp = new ArrayList<CollectionDetailDto>();
				listTemp = collectionDetailSvc.selectForTaskAdjustmentByKodeCabang(listCabangMstDtoChoose.get(i).getKodeCabang(), keySearch);
				if (listTemp.size() != 0) {
					for (int j = 0; j < listTemp.size(); j++) {
						CollectionDetailDto cd = new CollectionDetailDto();
						cd.setOrderId(listTemp.get(j).getOrderId());
						cd.setCustomerName(listTemp.get(j).getCustomerName());
						cd.setAddress(listTemp.get(j).getAddress());
						cd.setNamaKaryawan(listTemp.get(j).getNamaKaryawan());
						cd.setTagihan(listTemp.get(j).getTagihan());
						cd.setNotes(listTemp.get(j).getNotes());
						cd.setContractNo(listTemp.get(j).getContractNo());
						cd.setInstallmentNo(listTemp.get(j).getInstallmentNo());
						listCollectionDetailDto.add(cd);
					}
				}
			}
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() == 0 && listUserKolektorChoose.size() != 0) {
			listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
			for (int i = 0; i < listRegionalDtoChoose.size(); i++) {
				for (int h = 0; h < listUserKolektorChoose.size(); h++) {
					List<CollectionDetailDto> listTemp = new ArrayList<CollectionDetailDto>();
					listTemp = collectionDetailSvc.selectForTaskAdjustmentByKodeRegionalKodeKol(listRegionalDtoChoose.get(i).getKodeRegion(), listUserKolektorChoose.get(h).getKodeKolektor(), keySearch);
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							CollectionDetailDto cd = new CollectionDetailDto();
							cd.setOrderId(listTemp.get(j).getOrderId());
							cd.setCustomerName(listTemp.get(j).getCustomerName());
							cd.setAddress(listTemp.get(j).getAddress());
							cd.setNamaKaryawan(listTemp.get(j).getNamaKaryawan());
							cd.setTagihan(listTemp.get(j).getTagihan());
							cd.setNotes(listTemp.get(j).getNotes());
							cd.setContractNo(listTemp.get(j).getContractNo());
							cd.setInstallmentNo(listTemp.get(j).getInstallmentNo());
							listCollectionDetailDto.add(cd);
						}
					}
				}
			}
		}else if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() != 0 && listUserKolektorChoose.size() != 0) {
			listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
			for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
				for (int h = 0; h < listUserKolektorChoose.size(); h++) {
					List<CollectionDetailDto> listTemp = new ArrayList<CollectionDetailDto>();
					listTemp = collectionDetailSvc.selectForTaskAdjustmentByKodeCabangKodeKol(listCabangMstDtoChoose.get(i).getKodeCabang(), listUserKolektorChoose.get(h).getKodeKolektor(), keySearch);
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							CollectionDetailDto cd = new CollectionDetailDto();
							cd.setOrderId(listTemp.get(j).getOrderId());
							cd.setCustomerName(listTemp.get(j).getCustomerName());
							cd.setAddress(listTemp.get(j).getAddress());
							cd.setNamaKaryawan(listTemp.get(j).getNamaKaryawan());
							cd.setTagihan(listTemp.get(j).getTagihan());
							cd.setNotes(listTemp.get(j).getNotes());
							cd.setContractNo(listTemp.get(j).getContractNo());
							cd.setInstallmentNo(listTemp.get(j).getInstallmentNo());
							listCollectionDetailDto.add(cd);
						}
					}
				}
			}
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() != 0 && listUserKolektorChoose.size() != 0) {
			listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
			for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
				for (int h = 0; h < listUserKolektorChoose.size(); h++) {
					List<CollectionDetailDto> listTemp = new ArrayList<CollectionDetailDto>();
					listTemp = collectionDetailSvc.selectForTaskAdjustmentByKodeCabangKodeKol(listCabangMstDtoChoose.get(i).getKodeCabang(), listUserKolektorChoose.get(h).getKodeKolektor(), keySearch);
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							CollectionDetailDto cd = new CollectionDetailDto();
							cd.setOrderId(listTemp.get(j).getOrderId());
							cd.setCustomerName(listTemp.get(j).getCustomerName());
							cd.setAddress(listTemp.get(j).getAddress());
							cd.setNamaKaryawan(listTemp.get(j).getNamaKaryawan());
							cd.setTagihan(listTemp.get(j).getTagihan());
							cd.setNotes(listTemp.get(j).getNotes());
							cd.setContractNo(listTemp.get(j).getContractNo());
							cd.setInstallmentNo(listTemp.get(j).getInstallmentNo());
							listCollectionDetailDto.add(cd);
						}
					}
				}
			}
		}
	}
	
	@Command("closeRegional")
	@NotifyChange("visibleRegional")
	public void closeRegional() {
		setVisibleRegional(false);
	}
	
	@Command("showCabang")
	@NotifyChange({"keySearchCabang","visibleCabang","listCabangMstDto","listCabangMstDto","listCabangMstDtoChoose"})
	public void showCabang() {
		keySearchCabang = "";
		if (listRegionalDtoChoose.size() == 0) {
			List<CabangMstDto> tempList = new ArrayList<CabangMstDto>();
			listCabangMstDto = new ArrayList<CabangMstDto>();
			if (listAksesRegional.size() != 0) {
				for (int i = 0; i < listAksesRegional.size(); i++) {
					List<CabangMstDto> listTemp = new ArrayList<CabangMstDto>();
					List<AksesRegionCabangDto> listTempAkses = new ArrayList<AksesRegionCabangDto>();
					listTempAkses = aksesRegionCabangSvc.fintCabangByUserIdAndKodeRegion(upSession.getUserId(), listAksesRegional.get(i).getKodeRegion(), keySearchCabang);
					if (listTempAkses.size() != 0) {
						for (int ki = 0; ki < listTempAkses.size(); ki++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTempAkses.get(ki).getKodeCabang());
							cb.setNamaCabang(listTempAkses.get(ki).getNamaCabang());
							listTemp.add(cb);
						}
					}
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTemp.get(j).getKodeCabang());
							cb.setNamaCabang(listTemp.get(j).getNamaCabang());
							cb.setItemList(true);
							listCabangMstDto.add(cb);
						}
					}
				}
			}
			if (listCabangMstDtoChoose.size() != 0) {
				for (int i = 0; i < listCabangMstDto.size(); i++) {	
						CabangMstDto cba = new CabangMstDto();
						boolean cheked = false;
						for (int j = 0; j < listCabangMstDtoChoose.size(); j++) {
							if (listCabangMstDto.get(i).getKodeCabang().equals(listCabangMstDtoChoose.get(j).getKodeCabang())) {
								cheked = true;
							}
						}
						if (cheked == true) {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(true);
						}else {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(false);
						}
						tempList.add(cba);
				}
				listCabangMstDto = new ArrayList<CabangMstDto>();
				listCabangMstDto = tempList;
			}
			if (listCabangMstDto.size() != 0) {
				CabangMstDto cb = new CabangMstDto();
				cb.setKodeCabang("");
				cb.setNamaCabang("");
				cb.setItemList(false);
				listCabangMstDto.add(cb);
				setVisibleCabang(true);
			}else {
				Messagebox.show("Tidak ada data cabang yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}else {
			List<CabangMstDto> tempList = new ArrayList<CabangMstDto>();
			listCabangMstDto = new ArrayList<CabangMstDto>();
			if (listRegionalDtoChoose.size() != 0) {
				for (int i = 0; i < listRegionalDtoChoose.size(); i++) {
					List<CabangMstDto> listTemp = new ArrayList<CabangMstDto>();
					List<AksesRegionCabangDto> listTempAkses = new ArrayList<AksesRegionCabangDto>();
					listTempAkses = aksesRegionCabangSvc.fintCabangByUserIdAndKodeRegion(upSession.getUserId(), listRegionalDtoChoose.get(i).getKodeRegion(), keySearchCabang);
					if (listTempAkses.size() != 0) {
						for (int ki = 0; ki < listTempAkses.size(); ki++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTempAkses.get(ki).getKodeCabang());
							cb.setNamaCabang(listTempAkses.get(ki).getNamaCabang());
							listTemp.add(cb);
						}
					}
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTemp.get(j).getKodeCabang());
							cb.setNamaCabang(listTemp.get(j).getNamaCabang());
							cb.setItemList(true);
							listCabangMstDto.add(cb);
						}
					}
				}
			}
			if (listCabangMstDtoChoose.size() != 0) {
				for (int i = 0; i < listCabangMstDto.size(); i++) {	
						CabangMstDto cba = new CabangMstDto();
						boolean cheked = false;
						for (int j = 0; j < listCabangMstDtoChoose.size(); j++) {
							if (listCabangMstDto.get(i).getKodeCabang().equals(listCabangMstDtoChoose.get(j).getKodeCabang())) {
								cheked = true;
							}
						}
						if (cheked == true) {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(true);
						}else {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(false);
						}
						tempList.add(cba);
				}
				listCabangMstDto = new ArrayList<CabangMstDto>();
				listCabangMstDto = tempList;
			}
			if (listCabangMstDto.size() != 0) {
				CabangMstDto cb = new CabangMstDto();
				cb.setKodeCabang("");
				cb.setNamaCabang("");
				cb.setItemList(false);
				listCabangMstDto.add(cb);
				setVisibleCabang(true);
			}else {
				Messagebox.show("Tidak ada data cabang yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}
	}
	
	@Command("showCabangSearch")
	@NotifyChange({"visibleCabang","listCabangMstDto","listCabangMstDto","listCabangMstDtoChoose"})
	public void showCabangSeaarch() {
		if (listRegionalDtoChoose.size() == 0) {
			List<CabangMstDto> tempList = new ArrayList<CabangMstDto>();
			listCabangMstDto = new ArrayList<CabangMstDto>();
			if (listAksesRegional.size() != 0) {
				for (int i = 0; i < listAksesRegional.size(); i++) {
					List<CabangMstDto> listTemp = new ArrayList<CabangMstDto>();
					List<AksesRegionCabangDto> listTempAkses = new ArrayList<AksesRegionCabangDto>();
					listTempAkses = aksesRegionCabangSvc.fintCabangByUserIdAndKodeRegion(upSession.getUserId(), listAksesRegional.get(i).getKodeRegion(), keySearchCabang);
					if (listTempAkses.size() != 0) {
						for (int ki = 0; ki < listTempAkses.size(); ki++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTempAkses.get(ki).getKodeCabang());
							cb.setNamaCabang(listTempAkses.get(ki).getNamaCabang());
							listTemp.add(cb);
						}
					}
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTemp.get(j).getKodeCabang());
							cb.setNamaCabang(listTemp.get(j).getNamaCabang());
							cb.setItemList(true);
							listCabangMstDto.add(cb);
						}
					}
				}
			}
			if (listCabangMstDtoChoose.size() != 0) {
				for (int i = 0; i < listCabangMstDto.size(); i++) {	
						CabangMstDto cba = new CabangMstDto();
						boolean cheked = false;
						for (int j = 0; j < listCabangMstDtoChoose.size(); j++) {
							if (listCabangMstDto.get(i).getKodeCabang().equals(listCabangMstDtoChoose.get(j).getKodeCabang())) {
								cheked = true;
							}
						}
						if (cheked == true) {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(true);
						}else {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(false);
						}
						tempList.add(cba);
				}
				listCabangMstDto = new ArrayList<CabangMstDto>();
				listCabangMstDto = tempList;
			}
			if (listCabangMstDto.size() != 0) {
				CabangMstDto cb = new CabangMstDto();
				cb.setKodeCabang("");
				cb.setNamaCabang("");
				cb.setItemList(false);
				listCabangMstDto.add(cb);
				setVisibleCabang(true);
			}else {
				Messagebox.show("Tidak ada data cabang yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}else {
			List<CabangMstDto> tempList = new ArrayList<CabangMstDto>();
			listCabangMstDto = new ArrayList<CabangMstDto>();
			if (listRegionalDtoChoose.size() != 0) {
				for (int i = 0; i < listRegionalDtoChoose.size(); i++) {
					List<CabangMstDto> listTemp = new ArrayList<CabangMstDto>();
					List<AksesRegionCabangDto> listTempAkses = new ArrayList<AksesRegionCabangDto>();
					listTempAkses = aksesRegionCabangSvc.fintCabangByUserIdAndKodeRegion(upSession.getUserId(), listRegionalDtoChoose.get(i).getKodeRegion(), keySearchCabang);
					if (listTempAkses.size() != 0) {
						for (int ki = 0; ki < listTempAkses.size(); ki++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTempAkses.get(ki).getKodeCabang());
							cb.setNamaCabang(listTempAkses.get(ki).getNamaCabang());
							listTemp.add(cb);
						}
					}
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTemp.get(j).getKodeCabang());
							cb.setNamaCabang(listTemp.get(j).getNamaCabang());
							cb.setItemList(true);
							listCabangMstDto.add(cb);
						}
					}
				}
			}
			if (listCabangMstDtoChoose.size() != 0) {
				for (int i = 0; i < listCabangMstDto.size(); i++) {	
						CabangMstDto cba = new CabangMstDto();
						boolean cheked = false;
						for (int j = 0; j < listCabangMstDtoChoose.size(); j++) {
							if (listCabangMstDto.get(i).getKodeCabang().equals(listCabangMstDtoChoose.get(j).getKodeCabang())) {
								cheked = true;
							}
						}
						if (cheked == true) {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(true);
						}else {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(false);
						}
						tempList.add(cba);
				}
				listCabangMstDto = new ArrayList<CabangMstDto>();
				listCabangMstDto = tempList;
			}
			if (listCabangMstDto.size() != 0) {
				CabangMstDto cb = new CabangMstDto();
				cb.setKodeCabang("");
				cb.setNamaCabang("");
				cb.setItemList(false);
				listCabangMstDto.add(cb);
				setVisibleCabang(true);
			}else {
				Messagebox.show("Tidak ada data cabang yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}
	}
	
	@Command("chooseCabang")
	@NotifyChange({"listCabangMstDtoChoose","stringArrayCabang","visibleCabang","userKolektor","listUserKolektorChoose","stringArrayKolektor","stringArrayCabang"})
	public void chooseCabang() {
		if (listCabangMstDtoChoose.size() != 0) {
			stringArrayCabang = "";
			for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
				if (i == 0) {
					stringArrayCabang = stringArrayCabang+listCabangMstDtoChoose.get(i).getNamaCabang();
				}else {
					stringArrayCabang = stringArrayCabang+", "+listCabangMstDtoChoose.get(i).getNamaCabang();
				}
			}
		}else {
			stringArrayCabang ="";
			listCabangMstDtoChoose = new ArrayList<CabangMstDto>();
		}
		stringArrayKolektor = "";
		listUserKolektorChoose = new ArrayList<UserProfileDto>();
		setVisibleCabang(false);
		
	}
	
	@Command("closeCabang")
	@NotifyChange("visibleCabang")
	public void closeCabang() {
		setVisibleCabang(false);
	}
	
	@Command("closeKolektor")
	@NotifyChange("visibleKolektor")
	public void closeKolektor() {
		setVisibleKolektor(false);
	}
	
	@Command("showKolektor")
	@NotifyChange({"visibleKolektor","listUserKolektor","keySearchKolektor"})
	public void showKolektor() {
		keySearchKolektor = "";
		if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() == 0) { 
			listUserKolektor = new ArrayList<UserProfileDto>();
			for (int i = 0; i < listAksesCabang.size(); i++) {
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeCabangAndSearch(listAksesCabang.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserKolektor.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserKolektor.size(); k++) {
								if (listUserKolektor.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserKolektor.add(kol);
							}
						}else {
							listUserKolektor.add(kol);
						}
					}
				}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserKolektor.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserKolektor.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserKolektor = listTemp;
				
			}
			if (listUserKolektor.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserKolektor.add(up);
				setVisibleKolektor(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}else if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() != 0) {
			listUserKolektor = new ArrayList<UserProfileDto>();
			for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeCabangAndSearch(listCabangMstDtoChoose.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserKolektor.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserKolektor.size(); k++) {
								if (listUserKolektor.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserKolektor.add(kol);
							}
						}else {
							listUserKolektor.add(kol);
						}
					}
				}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserKolektor.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserKolektor.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserKolektor = listTemp;
				
			}
			if (listUserKolektor.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserKolektor.add(up);
				setVisibleKolektor(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
			
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() != 0) {
			listUserKolektor = new ArrayList<UserProfileDto>();
			for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeCabangAndSearch(listCabangMstDtoChoose.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserKolektor.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserKolektor.size(); k++) {
								if (listUserKolektor.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserKolektor.add(kol);
							}
						}else {
							listUserKolektor.add(kol);
						}
					}
				}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserKolektor.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserKolektor.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserKolektor = listTemp;
				
			}
			if (listUserKolektor.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserKolektor.add(up);
				setVisibleKolektor(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
			
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() == 0) {
			System.out.println("Masuk sini ke reg doang");
			listUserKolektor = new ArrayList<UserProfileDto>();
			for (int h = 0; h < listRegionalDtoChoose.size(); h++) {
			for (int i = 0; i < listAksesCabang.size(); i++) {
				System.out.println("masuk loop reg ke = "+h);
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeRegionalKodeCabangAndSearch(listRegionalDtoChoose.get(h).getKodeRegion(),listAksesCabang.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserKolektor.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserKolektor.size(); k++) {
								if (listUserKolektor.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserKolektor.add(kol);
							}
						}else {
							listUserKolektor.add(kol);
						}
					}
				}
			}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserKolektor.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserKolektor.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserKolektor = listTemp;
				
			}
			if (listUserKolektor.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserKolektor.add(up);
				setVisibleKolektor(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}
	}
	
	@Command("showKolektorSearch")
	@NotifyChange({"visibleKolektor","listUserKolektor"})
	public void showKolektorSearch() {
		if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() == 0) { 
			listUserKolektor = new ArrayList<UserProfileDto>();
			for (int i = 0; i < listAksesCabang.size(); i++) {
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeCabangAndSearch(listAksesCabang.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserKolektor.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserKolektor.size(); k++) {
								if (listUserKolektor.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserKolektor.add(kol);
							}
						}else {
							listUserKolektor.add(kol);
						}
					}
				}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserKolektor.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserKolektor.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserKolektor = listTemp;
				
			}
			if (listUserKolektor.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserKolektor.add(up);
				setVisibleKolektor(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}else if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() != 0) {
			listUserKolektor = new ArrayList<UserProfileDto>();
			for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeCabangAndSearch(listCabangMstDtoChoose.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserKolektor.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserKolektor.size(); k++) {
								if (listUserKolektor.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserKolektor.add(kol);
							}
						}else {
							listUserKolektor.add(kol);
						}
					}
				}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserKolektor.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserKolektor.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserKolektor = listTemp;
				
			}
			if (listUserKolektor.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserKolektor.add(up);
				setVisibleKolektor(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
			
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() != 0) {
			listUserKolektor = new ArrayList<UserProfileDto>();
			for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeCabangAndSearch(listCabangMstDtoChoose.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserKolektor.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserKolektor.size(); k++) {
								if (listUserKolektor.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserKolektor.add(kol);
							}
						}else {
							listUserKolektor.add(kol);
						}
					}
				}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserKolektor.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserKolektor.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserKolektor = listTemp;
				
			}
			if (listUserKolektor.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserKolektor.add(up);
				setVisibleKolektor(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
			
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() == 0) {
			System.out.println("Masuk sini ke reg doang");
			listUserKolektor = new ArrayList<UserProfileDto>();
			for (int h = 0; h < listRegionalDtoChoose.size(); h++) {
			for (int i = 0; i < listAksesCabang.size(); i++) {
				System.out.println("masuk loop reg ke = "+h);
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeRegionalKodeCabangAndSearch(listRegionalDtoChoose.get(h).getKodeRegion(),listAksesCabang.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserKolektor.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserKolektor.size(); k++) {
								if (listUserKolektor.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserKolektor.add(kol);
							}
						}else {
							listUserKolektor.add(kol);
						}
					}
				}
			}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserKolektor.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserKolektor.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserKolektor = listTemp;
				
			}
			if (listUserKolektor.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserKolektor.add(up);
				setVisibleKolektor(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}
	}
	
	@Command("chooseKolektor")
	@NotifyChange({"visibleKolektor","stringArrayKolektor","listUserKolektorChoose","stringArrayKolektor"})
	public void chooseKolektor() {
		if (listUserKolektorChoose.size() != 0) {
			stringArrayKolektor = "";
			for (int i = 0; i < listUserKolektorChoose.size(); i++) {
				if (i == 0  ) {
					stringArrayKolektor = stringArrayKolektor+listUserKolektorChoose.get(i).getNamaKaryawan();
				}else {
					stringArrayKolektor = stringArrayKolektor+", "+listUserKolektorChoose.get(i).getNamaKaryawan();
				}
			}
		}else {
			listUserKolektorChoose = new ArrayList<UserProfileDto>();
			stringArrayKolektor = "";
		}
		setVisibleKolektor(false);
	}
	
	
	@Command("page")
	@NotifyChange("page")
	public void page() {
		setPage(getPage());
	}
	
	@Command("actionAdjustment")
	@NotifyChange({"visibleActionAdjustment", "stsActionAdjustment","disPilHandling","disTglByr"})
	public void actionAdjustment() {
		setVisibleActionAdjustment(true);
		stsActionAdjustment = "BAYAR";
		setDisTglByr(true);
		setDisPilHandling(true);
				
	}
	
	@Command("okActionAdjustment")
	@NotifyChange({"visibleActionAdjustment", "stsActionAdjustment","listCollectionDetailDto"})
	public void okActionAdjustment() {
		CollectionDetailDto cd = new CollectionDetailDto();
		Date dateNow = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		System.out.println("data col = "+collectionDetailDto.getOrderId()+", "+collectionDetailDto.getContractNo()+", "+collectionDetailDto.getInstallmentNo());
		if (stsActionAdjustment.equalsIgnoreCase("BAYAR")) {
			int tootStsAsn = 0;
			cd = collectionDetailSvc.selectCollectionDetailForInputPayment(collectionDetailDto.getOrderId(), collectionDetailDto.getContractNo(), collectionDetailDto.getInstallmentNo());
			paymentSvc.insertPayment(cd.getInstallmentNo(), cd.getOrderId(), cd.getCustomerCode(), cd.getContractNo(), cd.getCollectorCode(), cd.getTotalAmt(), cd.getReceiptNo(), cd.getLpkNo(), df.format(dateNow), df.format(dateNow), "CMP", upSession.getUserId());
			collectionDetailSvc.updateStatusCollectionDetailToCMP(collectionDetailDto.getOrderId(), collectionDetailDto.getContractNo(), collectionDetailDto.getInstallmentNo());
			tootStsAsn = collectionDetailSvc.cekStatusCollectionDetailByOrderId(collectionDetailDto.getOrderId());
			if (tootStsAsn == 0) {
				collectionDetailSvc.updateCollectionHeadetToCMP("CMP", collectionDetailDto.getOrderId());
			}
			Notification.sendNotif(collectionDetailDto.getUserId(), "command", "get_data");
			search();
			Messagebox.show("Proses berhasil dilakukan", "MESSAGE", null, null, null);
			setVisibleActionAdjustment(false);
		}else {
			int batchHand = 0;
			batchHand = masterHandlingSvc.maxBatchHandling();
			cd = collectionDetailSvc.selectCollectionDetailForInputHandling(collectionDetailDto.getOrderId(), collectionDetailDto.getContractNo(), collectionDetailDto.getInstallmentNo());
			int promiseCount = 0;
			int promiseAvailable = 0;
			int promiseRange = 0;
			if (cd.getPromiseCount() != 0) {
				promiseCount = cd.getPromiseCount();
			}
			if (cd.getPromiseAvailable() != 0) {
				promiseAvailable = cd.getPromiseAvailable();
			}
			if (cd.getPromiseRange() != 0) {
				promiseRange = cd.getPromiseRange();
			}
			if (promiseAvailable != 0) {
				int tootStsAsn = 0;
				if (masterHandlingDto.getPromise() == "1") {
					handlingSvc.insertHandDepo(batchHand, cd.getContractNo(), cd.getInstallmentNo(), cd.getOrderId(), masterHandlingDto.getHandCode(), cd.getCustomerCode(), cd.getLpkNo(), cd.getNotes(), df.format(dateNow), df.format(dateNow), promiseCount, promiseAvailable, promiseRange, "CMP", upSession.getUserId(), deposit, upSession.getUserId());
					collectionDetailSvc.updateStatusCollectionDetailToCMP(collectionDetailDto.getOrderId(), collectionDetailDto.getContractNo(), collectionDetailDto.getInstallmentNo());
					tootStsAsn = collectionDetailSvc.cekStatusCollectionDetailByOrderId(collectionDetailDto.getOrderId());
					if (tootStsAsn == 0) {
						collectionDetailSvc.updateCollectionHeadetToCMP("CMP", collectionDetailDto.getOrderId());
					}
					Notification.sendNotif(collectionDetailDto.getUserId(), "command", "get_data");
					search();
					Messagebox.show("Proses berhasil dilakukan", "MESSAGE", null, null, null);
					setVisibleActionAdjustment(false);
				}else {
					System.out.println("ni data mau di insert = "+batchHand+", "+cd.getContractNo()+", "+cd.getInstallmentNo()+", "+cd.getOrderId()+", "+masterHandlingDto.getHandCode()+", "+cd.getCustomerCode()+", "+cd.getLpkNo()+", "+cd.getNotes()+", "+df.format(dateNow)+", "+"NULL"+", "+promiseCount+", "+promiseAvailable+", "+promiseRange+", "+"CMP"+", "+upSession.getUserId()+", "+deposit+", "+upSession.getUserId());
					handlingSvc.insertHandDepoNoTglJanji(batchHand, cd.getContractNo(), cd.getInstallmentNo(), cd.getOrderId(), masterHandlingDto.getHandCode(), cd.getCustomerCode(), cd.getLpkNo(), cd.getNotes(), df.format(dateNow), promiseCount, promiseAvailable, promiseRange, "CMP", upSession.getUserId(), deposit, upSession.getUserId());
					collectionDetailSvc.updateStatusCollectionDetailToCMP(collectionDetailDto.getOrderId(), collectionDetailDto.getContractNo(), collectionDetailDto.getInstallmentNo());
					tootStsAsn = collectionDetailSvc.cekStatusCollectionDetailByOrderId(collectionDetailDto.getOrderId());
					if (tootStsAsn == 0) {
						collectionDetailSvc.updateCollectionHeadetToCMP("CMP", collectionDetailDto.getOrderId());
					}
					Notification.sendNotif(collectionDetailDto.getUserId(), "command", "get_data");
					search();
					Messagebox.show("Proses berhasil dilakukan", "MESSAGE", null, null, null);
					setVisibleActionAdjustment(false);
				}
			}else {
				Messagebox.show("Available promise = 0 ", "MESSAGE", null, null, null);
				setVisibleActionAdjustment(false);
			}
			
		}			
	}
	
	@Command("closeActionAdjustment")
	@NotifyChange({"visibleActionAdjustment", "stsActionAdjustment"})
	public void closeActionAdjustment() {
		setVisibleActionAdjustment(false);
				
	}
	
	@Command("showCloseTask")
	@NotifyChange({"visibleCloseTask","remarkActionAdjustment"})
	public void showCloseTask() {
		setVisibleCloseTask(true);
		remarkActionAdjustment = "";		
	}
	
	@Command("okShowCloseTask")
	@NotifyChange({"visibleCloseTask","remarkActionAdjustment","collectionDetailDto","listCollectionDetailDto"})
	public void okShowCloseTask() {		
		if (collectionDetailDto != null && remarkActionAdjustment != null && remarkActionAdjustment != "") {
			collectionDetailSvc.updateStatusCollectionDetailToCLS(collectionDetailDto.getOrderId(), collectionDetailDto.getContractNo(), collectionDetailDto.getInstallmentNo(), remarkActionAdjustment);
			Notification.sendNotif(collectionDetailDto.getUserId(), "command", "get_data");
			collectionDetailDto = new CollectionDetailDto();
			setVisibleCloseTask(false);
			Messagebox.show("Close task berhasil!", "MESSAGE", null, null, null);
		}else {
			Messagebox.show("REMARK TIDAK BOLEH KOSONG !", "MESSAGE", null, null, null);
		}
		search();
	}
	
	@Command("closeShowCloseTask")
	@NotifyChange({"visibleCloseTask","remarkActionAdjustment"})
	public void closeShowCloseTask() {
		setVisibleCloseTask(false);
		remarkActionAdjustment = "";
	}
	
	@Command("pageCabang")
	@NotifyChange("pageCabang")
	public void pageCabang() {
		setPageCabang(getPageCabang());
	}
	@Command("pageKolektor")
	@NotifyChange("pageRegional")
	public void pageKolektor() {
		setPageKolektor(getPageKolektor());
	}
	
	@Command("pageMasterHandling")
	@NotifyChange("pageMasterHandling")
	public void pageMasterHandling() {
		setPageMasterHandling(getPageMasterHandling());
	}
	
	@Command("showTidakBayar")
	@NotifyChange({"disPilHandling","disTglByr"})
	public void showTidakBayar() {
		if (stsActionAdjustment.equalsIgnoreCase("TIDAK BAYAR")) {
			setDisPilHandling(false);
		}else {
			setDisPilHandling(true);
		}
	}
	
	@Command("showMasterHandling")
	@NotifyChange({"visibleMasterHandling", "masterHandlingDto", "keySearchMasterHandling","listMasterHandlingDto"})
	public void showMasterHandling() {
		keySearchMasterHandling = "";
		listMasterHandlingDto = new ArrayList<MasterHandlingDto>();
		int maxBatch = 0;
		maxBatch = masterHandlingSvc.maxBatchHandling();
		listMasterHandlingDto = masterHandlingSvc.findAllHandling(keySearchMasterHandling,maxBatch);
		masterHandlingDto = new MasterHandlingDto();
		setVisibleMasterHandling(true);
	}
	
	@Command("showMasterHandlingSearch")
	@NotifyChange({"visibleMasterHandling", "masterHandlingDto", "keySearchMasterHandling","listMasterHandlingDto"})
	public void showMasterHandlingSearch() {
		keySearchMasterHandling = "";
		listMasterHandlingDto = new ArrayList<MasterHandlingDto>();
		int maxBatch = 0;
		maxBatch = masterHandlingSvc.maxBatchHandling();
		listMasterHandlingDto = masterHandlingSvc.findAllHandling(keySearchMasterHandling,maxBatch);
		masterHandlingDto = new MasterHandlingDto();
		setVisibleMasterHandling(true);
	}
	
	@Command("chooseMasterHandling")
	@NotifyChange({"visibleMasterHandling","disTglByr"})
	public void chooseMasterHandling() {
		if (masterHandlingDto.getHandCode() != null) {
			setVisibleMasterHandling(false);
			if (masterHandlingDto.getPromise().equalsIgnoreCase("1")) {
				setDisTglByr(false);
			}else {
				setDisTglByr(true);
			}
		}else {
			Messagebox.show("Pilih data terlebih dahulu", "MESSAGE", null, null, null);
		}
	}
	
	@Command("closeMasterHandling")
	@NotifyChange("visibleMasterHandling")
	public void closeMasterHandling() {
		setVisibleMasterHandling(false);
	}
	
}
