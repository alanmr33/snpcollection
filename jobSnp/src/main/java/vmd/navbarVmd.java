package vmd;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zul.Include;
import org.zkoss.zul.Messagebox;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class navbarVmd {
	
	@Init
	public void load(){
		Include inc = (Include)Executions.getCurrent().getDesktop().getPage("index").getFellow("mainInclude");
		inc.setSrc("latihanEdwin//master/userManagement/UserManagement.zul");
	}
	/*
	@Command("userManagement")
	public void userManagement() {
		Include inc = (Include)Executions.getCurrent().getDesktop().getPage("index").getFellow("mainInclude");
		inc.setSrc("latihanEdwin//master/userManagement/UserManagement.zul");
	}
	@Command("karyawan")
	public void karyawan() {
		Include inc = (Include)Executions.getCurrent().getDesktop().getPage("index").getFellow("mainInclude");
		inc.setSrc("/master/karyawan/karyawan.zul");
	}*/
}
