package vmd;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.sf.jasperreports.engine.JRException;

import org.apache.commons.io.IOUtils;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Include;
import org.zkoss.zul.Messagebox;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import dao.CollectionDetailDao;
import dto.AksesRegionCabangDto;
import dto.CabangMstDto;
import dto.CollectionDetailDto;
import dto.MasterRegionalDto;
import dto.MobileTrackingDto;
import dto.UserProfileDto;
import pagevmd.NavigationVmd;
import service.AksesRegionCabangSvc;
import service.AuthorityListSvc;
import service.CabangMstSvc;
import service.CollectionDetailSvc;
import service.MasterRegionalSvc;
import service.MobileTrackingSvc;
import service.UserProfileSvc;
import service.impl.userProfileSvcImpl;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class ReportKunjunganKolektorVmd extends NavigationVmd{
	
	@WireVariable
	private UserProfileSvc userProfileSvc;
	
	@WireVariable
	private MasterRegionalSvc masterRegionalSvc;
	
	@WireVariable
	private CabangMstSvc cabangMstSvc;
	
	@WireVariable
	private CollectionDetailSvc collectionDetailSvc;
	
	
	@WireVariable
	private AksesRegionCabangSvc aksesRegionCabangSvc;
	
	private List<UserProfileDto> listUserKolektor = new ArrayList<UserProfileDto>();
	private List<UserProfileDto> listUserKolektorChoose = new ArrayList<UserProfileDto>();
	private List<MasterRegionalDto> listRegionalDto = new ArrayList<MasterRegionalDto>();
	private List<MasterRegionalDto> listRegionalDtoChoose = new ArrayList<MasterRegionalDto>();
	private List<CabangMstDto> listCabangMstDto = new ArrayList<CabangMstDto>();
	private List<CabangMstDto> listCabangMstDtoChoose = new ArrayList<CabangMstDto>();
	private List<CollectionDetailDto> listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
	private List<CollectionDetailDto> listCollectionDetailViewDto = new ArrayList<CollectionDetailDto>();
	private CollectionDetailDto collectionDetailDto = new CollectionDetailDto();
	private UserProfileDto upSession = new UserProfileDto();
	private UserProfileDto userKolektor = new UserProfileDto();
	private MasterRegionalDto regionalDto = new MasterRegionalDto();
	private CabangMstDto cabangDto = new CabangMstDto();
	private int page = 10;
	private int pageDetail = 10;
	private int pageRegional = 10;
	private int pageCabang = 10;
	private int pageKolektor = 10;
	private int banyakListMap1 = 0;
	private int banyakListMap2 = 0;
	private boolean visibleRegional = false;
	private boolean visibleCabang = false;
	private boolean visibleKolektor = false;
	private boolean visibleDetail = false;
	private String keySearch= "";
	private String keySearchDetail= "";
	private String keySearchregional = "";
	private String keySearchCabang = "";
	private String keySearchKolektor = "";
	private String stringArrayRegional = "";
	private String stringArrayCabang = "";
	private String stringArrayKolektor = "";
	private Date tglAwal;
	private Date tglAkhir;
	private String export="excel";
	private List<MasterRegionalDto> listAksesRegional = new ArrayList<MasterRegionalDto>();
	private List<CabangMstDto> listAksesCabang = new ArrayList<CabangMstDto>();
	
	
	public List<MasterRegionalDto> getListAksesRegional() {
		return listAksesRegional;
	}
	public void setListAksesRegional(List<MasterRegionalDto> listAksesRegional) {
		this.listAksesRegional = listAksesRegional;
	}
	public List<CabangMstDto> getListAksesCabang() {
		return listAksesCabang;
	}
	public void setListAksesCabang(List<CabangMstDto> listAksesCabang) {
		this.listAksesCabang = listAksesCabang;
	}
	public String getExport() {
		return export;
	}
	public void setExport(String export) {
		this.export = export;
	}
	public String getKeySearchDetail() {
		return keySearchDetail;
	}
	public void setKeySearchDetail(String keySearchDetail) {
		this.keySearchDetail = keySearchDetail;
	}
	public List<CollectionDetailDto> getListCollectionDetailViewDto() {
		return listCollectionDetailViewDto;
	}
	public void setListCollectionDetailViewDto(
			List<CollectionDetailDto> listCollectionDetailViewDto) {
		this.listCollectionDetailViewDto = listCollectionDetailViewDto;
	}
	public boolean isVisibleDetail() {
		return visibleDetail;
	}
	public void setVisibleDetail(boolean visibleDetail) {
		this.visibleDetail = visibleDetail;
	}
	public int getPageDetail() {
		return pageDetail;
	}
	public void setPageDetail(int pageDetail) {
		this.pageDetail = pageDetail;
	}
	public List<CollectionDetailDto> getListCollectionDetailDto() {
		return listCollectionDetailDto;
	}
	public void setListCollectionDetailDto(
			List<CollectionDetailDto> listCollectionDetailDto) {
		this.listCollectionDetailDto = listCollectionDetailDto;
	}
	public CollectionDetailDto getCollectionDetailDto() {
		return collectionDetailDto;
	}
	public void setCollectionDetailDto(CollectionDetailDto collectionDetailDto) {
		this.collectionDetailDto = collectionDetailDto;
	}
	public UserProfileDto getUpSession() {
		return upSession;
	}
	public void setUpSession(UserProfileDto upSession) {
		this.upSession = upSession;
	}
	public String getKeySearch() {
		return keySearch;
	}
	public void setKeySearch(String keySearch) {
		this.keySearch = keySearch;
	}
	
	public int getBanyakListMap1() {
		return banyakListMap1;
	}
	public void setBanyakListMap1(int banyakListMap1) {
		this.banyakListMap1 = banyakListMap1;
	}
	public int getBanyakListMap2() {
		return banyakListMap2;
	}
	public void setBanyakListMap2(int banyakListMap2) {
		this.banyakListMap2 = banyakListMap2;
	}
	public int getPageRegional() {
		return pageRegional;
	}
	public void setPageRegional(int pageRegional) {
		this.pageRegional = pageRegional;
	}
	public int getPageCabang() {
		return pageCabang;
	}
	public void setPageCabang(int pageCabang) {
		this.pageCabang = pageCabang;
	}
	public int getPageKolektor() {
		return pageKolektor;
	}
	public void setPageKolektor(int pageKolektor) {
		this.pageKolektor = pageKolektor;
	}
	public Date getTglAwal() {
		return tglAwal;
	}
	public void setTglAwal(Date tglAwal) {
		this.tglAwal = tglAwal;
	}
	public Date getTglAkhir() {
		return tglAkhir;
	}
	public void setTglAkhir(Date tglAkhir) {
		this.tglAkhir = tglAkhir;
	}
	public List<UserProfileDto> getListUserKolektorChoose() {
		return listUserKolektorChoose;
	}
	public void setListUserKolektorChoose(
			List<UserProfileDto> listUserKolektorChoose) {
		this.listUserKolektorChoose = listUserKolektorChoose;
	}
	public List<MasterRegionalDto> getListRegionalDtoChoose() {
		return listRegionalDtoChoose;
	}
	public void setListRegionalDtoChoose(
			List<MasterRegionalDto> listRegionalDtoChoose) {
		this.listRegionalDtoChoose = listRegionalDtoChoose;
	}
	public List<CabangMstDto> getListCabangMstDtoChoose() {
		return listCabangMstDtoChoose;
	}
	public void setListCabangMstDtoChoose(List<CabangMstDto> listCabangMstDtoChoose) {
		this.listCabangMstDtoChoose = listCabangMstDtoChoose;
	}
	public String getStringArrayRegional() {
		return stringArrayRegional;
	}
	public void setStringArrayRegional(String stringArrayRegional) {
		this.stringArrayRegional = stringArrayRegional;
	}
	public String getStringArrayCabang() {
		return stringArrayCabang;
	}
	public void setStringArrayCabang(String stringArrayCabang) {
		this.stringArrayCabang = stringArrayCabang;
	}
	public String getStringArrayKolektor() {
		return stringArrayKolektor;
	}
	public void setStringArrayKolektor(String stringArrayKolektor) {
		this.stringArrayKolektor = stringArrayKolektor;
	}
	public String getKeySearchregional() {
		return keySearchregional;
	}
	public void setKeySearchregional(String keySearchregional) {
		this.keySearchregional = keySearchregional;
	}
	public String getKeySearchCabang() {
		return keySearchCabang;
	}
	public void setKeySearchCabang(String keySearchCabang) {
		this.keySearchCabang = keySearchCabang;
	}
	public String getKeySearchKolektor() {
		return keySearchKolektor;
	}
	public void setKeySearchKolektor(String keySearchKolektor) {
		this.keySearchKolektor = keySearchKolektor;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public List<UserProfileDto> getListUserKolektor() {
		return listUserKolektor;
	}
	public void setListUserKolektor(List<UserProfileDto> listUserKolektor) {
		this.listUserKolektor = listUserKolektor;
	}
	public List<MasterRegionalDto> getListRegionalDto() {
		return listRegionalDto;
	}
	public void setListRegionalDto(List<MasterRegionalDto> listRegionalDto) {
		this.listRegionalDto = listRegionalDto;
	}
	public List<CabangMstDto> getListCabangMstDto() {
		return listCabangMstDto;
	}
	public void setListCabangMstDto(List<CabangMstDto> listCabangMstDto) {
		this.listCabangMstDto = listCabangMstDto;
	}
	public UserProfileDto getUserKolektor() {
		return userKolektor;
	}
	public void setUserKolektor(UserProfileDto userKolektor) {
		this.userKolektor = userKolektor;
	}
	public MasterRegionalDto getRegionalDto() {
		return regionalDto;
	}
	public void setRegionalDto(MasterRegionalDto regionalDto) {
		this.regionalDto = regionalDto;
	}
	public CabangMstDto getCabangDto() {
		return cabangDto;
	}
	public void setCabangDto(CabangMstDto cabangDto) {
		this.cabangDto = cabangDto;
	}	
	public boolean isVisibleRegional() {
		return visibleRegional;
	}
	public void setVisibleRegional(boolean visibleRegional) {
		this.visibleRegional = visibleRegional;
	}
	public boolean isVisibleCabang() {
		return visibleCabang;
	}
	public void setVisibleCabang(boolean visibleCabang) {
		this.visibleCabang = visibleCabang;
	}
	public boolean isVisibleKolektor() {
		return visibleKolektor;
	}
	public void setVisibleKolektor(boolean visibleKolektor) {
		this.visibleKolektor = visibleKolektor;
	}
	
	@Init
	public void load() {
		Sessions.getCurrent().removeAttribute("sessionMobileTracking");
		upSession = (UserProfileDto) Sessions.getCurrent().getAttribute("user");
		List<AksesRegionCabangDto> lr = aksesRegionCabangSvc.findRegionByUserId(upSession.getUserId(), "");
		List<AksesRegionCabangDto> lc = aksesRegionCabangSvc.fintCabangByUserId(upSession.getUserId(), "");
		if (lr.size() != 0) {
			for (int i = 0; i < lr.size(); i++) {
				MasterRegionalDto mr = new MasterRegionalDto();
				mr.setKodeRegion(lr.get(i).getKodeRegion());
				mr.setNamaRegion(lr.get(i).getNamaRegional());
				listAksesRegional.add(mr);
			}
		}
		if (lc.size() != 0) {
			for (int i = 0; i < lc.size(); i++) {
				CabangMstDto cb= new CabangMstDto();
				cb.setKodeCabang(lc.get(i).getKodeCabang());
				cb.setNamaCabang(lc.get(i).getNamaCabang());
				listAksesCabang.add(cb);
			}
		}
		tglAwal = new Date();
		tglAkhir = new Date();
	}
	
	@Command("showRegional")
	@NotifyChange({"keySearchregional","visibleRegional","listRegionalDto","listRegionalDtoChoose"})
	public void showRegional() {
		keySearchregional = "";
		listRegionalDto = new ArrayList<MasterRegionalDto>();
		List<AksesRegionCabangDto> listTempAkses = new ArrayList<AksesRegionCabangDto>();
		listTempAkses = aksesRegionCabangSvc.findRegionByUserId(upSession.getUserId(),keySearchregional);
		if (listTempAkses.size() != 0) {
			for (int i = 0; i < listTempAkses.size(); i++) {
				MasterRegionalDto mr = new MasterRegionalDto();
				mr.setKodeRegion(listTempAkses.get(i).getKodeRegion());
				mr.setNamaRegion(listTempAkses.get(i).getNamaRegional());
				mr.setItemList(true);
				listRegionalDto.add(mr);
			}
		}
		if (listRegionalDtoChoose.size() != 0 ) {
			List<MasterRegionalDto> listTemp = new ArrayList<MasterRegionalDto>();
			for (int i = 0; i < listRegionalDto.size(); i++) {
				boolean sama = false;
				for (int j = 0; j < listRegionalDtoChoose.size(); j++) {
					if (listRegionalDto.get(i).getKodeRegion().equals(listRegionalDtoChoose.get(j).getKodeRegion())) {
						sama = true;
					}
				}
				if (sama == true) {
					MasterRegionalDto temppDto = new MasterRegionalDto();
					temppDto.setKodeRegion(listRegionalDto.get(i).getKodeRegion());
					temppDto.setNamaRegion(listRegionalDto.get(i).getNamaRegion());
					temppDto.setChoose(true);
					temppDto.setItemList(true);
					listTemp.add(temppDto);
				}else {
					MasterRegionalDto temppDto = new MasterRegionalDto();
					temppDto.setKodeRegion(listRegionalDto.get(i).getKodeRegion());
					temppDto.setNamaRegion(listRegionalDto.get(i).getNamaRegion());
					temppDto.setChoose(false);
					temppDto.setItemList(true);
					listTemp.add(temppDto);
				}
			}
			listRegionalDto = listTemp;
		}
		MasterRegionalDto mre = new MasterRegionalDto();
		mre.setKodeRegion("");
		mre.setNamaRegion("");
		mre.setItemList(false);
		listRegionalDto.add(mre);
		setVisibleRegional(true);
	}
	
	@Command("showRegionalSearch")
	@NotifyChange({"visibleRegional","listRegionalDto","listRegionalDtoChoose"})
	public void showRegionalSearch() {
		listRegionalDto = new ArrayList<MasterRegionalDto>();
		List<AksesRegionCabangDto> listTempAkses = new ArrayList<AksesRegionCabangDto>();
		listTempAkses = aksesRegionCabangSvc.findRegionByUserId(upSession.getUserId(),keySearchregional);
		if (listTempAkses.size() != 0) {
			for (int i = 0; i < listTempAkses.size(); i++) {
				MasterRegionalDto mr = new MasterRegionalDto();
				mr.setKodeRegion(listTempAkses.get(i).getKodeRegion());
				mr.setNamaRegion(listTempAkses.get(i).getNamaRegional());
				mr.setItemList(true);
				listRegionalDto.add(mr);
			}
		}
		if (listRegionalDtoChoose.size() != 0 ) {
			List<MasterRegionalDto> listTemp = new ArrayList<MasterRegionalDto>();
			for (int i = 0; i < listRegionalDto.size(); i++) {
				boolean sama = false;
				for (int j = 0; j < listRegionalDtoChoose.size(); j++) {
					if (listRegionalDto.get(i).getKodeRegion().equals(listRegionalDtoChoose.get(j).getKodeRegion())) {
						sama = true;
					}
				}
				if (sama == true) {
					MasterRegionalDto temppDto = new MasterRegionalDto();
					temppDto.setKodeRegion(listRegionalDto.get(i).getKodeRegion());
					temppDto.setNamaRegion(listRegionalDto.get(i).getNamaRegion());
					temppDto.setChoose(true);
					temppDto.setItemList(true);
					listTemp.add(temppDto);
				}else {
					MasterRegionalDto temppDto = new MasterRegionalDto();
					temppDto.setKodeRegion(listRegionalDto.get(i).getKodeRegion());
					temppDto.setNamaRegion(listRegionalDto.get(i).getNamaRegion());
					temppDto.setChoose(false);
					temppDto.setItemList(true);
					listTemp.add(temppDto);
				}
			}
			listRegionalDto = listTemp;
		}
		MasterRegionalDto mre = new MasterRegionalDto();
		mre.setKodeRegion("");
		mre.setNamaRegion("");
		mre.setItemList(false);
		listRegionalDto.add(mre);
		setVisibleRegional(true);
	}
	
	@Command("chooseRegional")
	@NotifyChange({"listRegionalDtoChoose","visibleRegional","stringArrayKolektor","stringArrayCabang","stringArrayRegional","listCabangMstDtoChoose","listUserKolektorChoose"})
	public void chooseRegional() {
		if (listRegionalDtoChoose.size() != 0) {
			stringArrayRegional = "";
			for (int i = 0; i < listRegionalDtoChoose.size(); i++) {
				if (i == 0) {
					stringArrayRegional = stringArrayRegional+listRegionalDtoChoose.get(i).getNamaRegion();
				}else {
					stringArrayRegional = stringArrayRegional+", "+listRegionalDtoChoose.get(i).getNamaRegion();
				}
			}
		}else {
			listRegionalDtoChoose = new ArrayList<MasterRegionalDto>();
			stringArrayRegional = "";
		}
		stringArrayKolektor = "";
		stringArrayCabang = "";
		listCabangMstDtoChoose = new ArrayList<CabangMstDto>();
		listUserKolektorChoose = new ArrayList<UserProfileDto>();
		setVisibleRegional(false);
	}
	
	@Command("closeRegional")
	@NotifyChange("visibleRegional")
	public void closeRegional() {
		setVisibleRegional(false);
	}
	
	@Command("showDetail")
	@NotifyChange({"visibleDetail", "listCollectionDetailViewDto"})
	public void showDetail() {
		listCollectionDetailViewDto = new ArrayList<CollectionDetailDto>();
		listCollectionDetailViewDto = collectionDetailSvc.selectReportKunjunganDetailView(tglAwal, tglAkhir, collectionDetailDto.getUserId(), keySearchDetail);
		setVisibleDetail(true);
	}

	@Command("resultSearchDetail")
	@NotifyChange({"visibleDetail", "listCollectionDetailViewDto"})
	public void resultSearchDetail() {
		listCollectionDetailViewDto = new ArrayList<CollectionDetailDto>();
		listCollectionDetailViewDto = collectionDetailSvc.selectReportKunjunganDetailView(tglAwal, tglAkhir, collectionDetailDto.getUserId(), keySearchDetail);
	}
	
	@Command("closeDetail")
	@NotifyChange("visibleDetail")
	public void closeDetail() {
		setVisibleDetail(false);
	}
	
	@Command("showCabang")
	@NotifyChange({"keySearchCabang","visibleCabang","listCabangMstDto","listCabangMstDto","listCabangMstDtoChoose"})
	public void showCabang() {
		keySearchCabang = "";
		if (listRegionalDtoChoose.size() == 0) {
			List<CabangMstDto> tempList = new ArrayList<CabangMstDto>();
			listCabangMstDto = new ArrayList<CabangMstDto>();
			if (listAksesRegional.size() != 0) {
				for (int i = 0; i < listAksesRegional.size(); i++) {
					List<CabangMstDto> listTemp = new ArrayList<CabangMstDto>();
					List<AksesRegionCabangDto> listTempAkses = new ArrayList<AksesRegionCabangDto>();
					listTempAkses = aksesRegionCabangSvc.fintCabangByUserIdAndKodeRegion(upSession.getUserId(), listAksesRegional.get(i).getKodeRegion(), keySearchCabang);
					if (listTempAkses.size() != 0) {
						for (int ki = 0; ki < listTempAkses.size(); ki++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTempAkses.get(ki).getKodeCabang());
							cb.setNamaCabang(listTempAkses.get(ki).getNamaCabang());
							listTemp.add(cb);
						}
					}
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTemp.get(j).getKodeCabang());
							cb.setNamaCabang(listTemp.get(j).getNamaCabang());
							cb.setItemList(true);
							listCabangMstDto.add(cb);
						}
					}
				}
			}
			if (listCabangMstDtoChoose.size() != 0) {
				for (int i = 0; i < listCabangMstDto.size(); i++) {	
						CabangMstDto cba = new CabangMstDto();
						boolean cheked = false;
						for (int j = 0; j < listCabangMstDtoChoose.size(); j++) {
							if (listCabangMstDto.get(i).getKodeCabang().equals(listCabangMstDtoChoose.get(j).getKodeCabang())) {
								cheked = true;
							}
						}
						if (cheked == true) {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(true);
						}else {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(false);
						}
						tempList.add(cba);
				}
				listCabangMstDto = new ArrayList<CabangMstDto>();
				listCabangMstDto = tempList;
			}
			if (listCabangMstDto.size() != 0) {
				CabangMstDto cb = new CabangMstDto();
				cb.setKodeCabang("");
				cb.setNamaCabang("");
				cb.setItemList(false);
				listCabangMstDto.add(cb);
				setVisibleCabang(true);
			}else {
				Messagebox.show("Tidak ada data cabang yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}else {
			List<CabangMstDto> tempList = new ArrayList<CabangMstDto>();
			listCabangMstDto = new ArrayList<CabangMstDto>();
			if (listRegionalDtoChoose.size() != 0) {
				for (int i = 0; i < listRegionalDtoChoose.size(); i++) {
					List<CabangMstDto> listTemp = new ArrayList<CabangMstDto>();
					List<AksesRegionCabangDto> listTempAkses = new ArrayList<AksesRegionCabangDto>();
					listTempAkses = aksesRegionCabangSvc.fintCabangByUserIdAndKodeRegion(upSession.getUserId(), listRegionalDtoChoose.get(i).getKodeRegion(), keySearchCabang);
					if (listTempAkses.size() != 0) {
						for (int ki = 0; ki < listTempAkses.size(); ki++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTempAkses.get(ki).getKodeCabang());
							cb.setNamaCabang(listTempAkses.get(ki).getNamaCabang());
							listTemp.add(cb);
						}
					}
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTemp.get(j).getKodeCabang());
							cb.setNamaCabang(listTemp.get(j).getNamaCabang());
							cb.setItemList(true);
							listCabangMstDto.add(cb);
						}
					}
				}
			}
			if (listCabangMstDtoChoose.size() != 0) {
				for (int i = 0; i < listCabangMstDto.size(); i++) {	
						CabangMstDto cba = new CabangMstDto();
						boolean cheked = false;
						for (int j = 0; j < listCabangMstDtoChoose.size(); j++) {
							if (listCabangMstDto.get(i).getKodeCabang().equals(listCabangMstDtoChoose.get(j).getKodeCabang())) {
								cheked = true;
							}
						}
						if (cheked == true) {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(true);
						}else {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(false);
						}
						tempList.add(cba);
				}
				listCabangMstDto = new ArrayList<CabangMstDto>();
				listCabangMstDto = tempList;
			}
			if (listCabangMstDto.size() != 0) {
				CabangMstDto cb = new CabangMstDto();
				cb.setKodeCabang("");
				cb.setNamaCabang("");
				cb.setItemList(false);
				listCabangMstDto.add(cb);
				setVisibleCabang(true);
			}else {
				Messagebox.show("Tidak ada data cabang yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}
	}
	
	@Command("showCabangSearch")
	@NotifyChange({"visibleCabang","listCabangMstDto","listCabangMstDto","listCabangMstDtoChoose"})
	public void showCabangSeaarch() {
		if (listRegionalDtoChoose.size() == 0) {
			List<CabangMstDto> tempList = new ArrayList<CabangMstDto>();
			listCabangMstDto = new ArrayList<CabangMstDto>();
			if (listAksesRegional.size() != 0) {
				for (int i = 0; i < listAksesRegional.size(); i++) {
					List<CabangMstDto> listTemp = new ArrayList<CabangMstDto>();
					List<AksesRegionCabangDto> listTempAkses = new ArrayList<AksesRegionCabangDto>();
					listTempAkses = aksesRegionCabangSvc.fintCabangByUserIdAndKodeRegion(upSession.getUserId(), listAksesRegional.get(i).getKodeRegion(), keySearchCabang);
					if (listTempAkses.size() != 0) {
						for (int ki = 0; ki < listTempAkses.size(); ki++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTempAkses.get(ki).getKodeCabang());
							cb.setNamaCabang(listTempAkses.get(ki).getNamaCabang());
							listTemp.add(cb);
						}
					}
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTemp.get(j).getKodeCabang());
							cb.setNamaCabang(listTemp.get(j).getNamaCabang());
							cb.setItemList(true);
							listCabangMstDto.add(cb);
						}
					}
				}
			}
			if (listCabangMstDtoChoose.size() != 0) {
				for (int i = 0; i < listCabangMstDto.size(); i++) {	
						CabangMstDto cba = new CabangMstDto();
						boolean cheked = false;
						for (int j = 0; j < listCabangMstDtoChoose.size(); j++) {
							if (listCabangMstDto.get(i).getKodeCabang().equals(listCabangMstDtoChoose.get(j).getKodeCabang())) {
								cheked = true;
							}
						}
						if (cheked == true) {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(true);
						}else {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(false);
						}
						tempList.add(cba);
				}
				listCabangMstDto = new ArrayList<CabangMstDto>();
				listCabangMstDto = tempList;
			}
			if (listCabangMstDto.size() != 0) {
				CabangMstDto cb = new CabangMstDto();
				cb.setKodeCabang("");
				cb.setNamaCabang("");
				cb.setItemList(false);
				listCabangMstDto.add(cb);
				setVisibleCabang(true);
			}else {
				Messagebox.show("Tidak ada data cabang yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}else {
			List<CabangMstDto> tempList = new ArrayList<CabangMstDto>();
			listCabangMstDto = new ArrayList<CabangMstDto>();
			if (listRegionalDtoChoose.size() != 0) {
				for (int i = 0; i < listRegionalDtoChoose.size(); i++) {
					List<CabangMstDto> listTemp = new ArrayList<CabangMstDto>();
					List<AksesRegionCabangDto> listTempAkses = new ArrayList<AksesRegionCabangDto>();
					listTempAkses = aksesRegionCabangSvc.fintCabangByUserIdAndKodeRegion(upSession.getUserId(), listRegionalDtoChoose.get(i).getKodeRegion(), keySearchCabang);
					if (listTempAkses.size() != 0) {
						for (int ki = 0; ki < listTempAkses.size(); ki++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTempAkses.get(ki).getKodeCabang());
							cb.setNamaCabang(listTempAkses.get(ki).getNamaCabang());
							listTemp.add(cb);
						}
					}
					if (listTemp.size() != 0) {
						for (int j = 0; j < listTemp.size(); j++) {
							CabangMstDto cb = new CabangMstDto();
							cb.setKodeCabang(listTemp.get(j).getKodeCabang());
							cb.setNamaCabang(listTemp.get(j).getNamaCabang());
							cb.setItemList(true);
							listCabangMstDto.add(cb);
						}
					}
				}
			}
			if (listCabangMstDtoChoose.size() != 0) {
				for (int i = 0; i < listCabangMstDto.size(); i++) {	
						CabangMstDto cba = new CabangMstDto();
						boolean cheked = false;
						for (int j = 0; j < listCabangMstDtoChoose.size(); j++) {
							if (listCabangMstDto.get(i).getKodeCabang().equals(listCabangMstDtoChoose.get(j).getKodeCabang())) {
								cheked = true;
							}
						}
						if (cheked == true) {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(true);
						}else {
							cba.setKodeCabang(listCabangMstDto.get(i).getKodeCabang());
							cba.setNamaCabang(listCabangMstDto.get(i).getNamaCabang());
							cba.setItemList(true);
							cba.setChoose(false);
						}
						tempList.add(cba);
				}
				listCabangMstDto = new ArrayList<CabangMstDto>();
				listCabangMstDto = tempList;
			}
			if (listCabangMstDto.size() != 0) {
				CabangMstDto cb = new CabangMstDto();
				cb.setKodeCabang("");
				cb.setNamaCabang("");
				cb.setItemList(false);
				listCabangMstDto.add(cb);
				setVisibleCabang(true);
			}else {
				Messagebox.show("Tidak ada data cabang yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}
	}
	
	
	@Command("chooseCabang")
	@NotifyChange({"listCabangMstDtoChoose","visibleCabang","userKolektor","listUserKolektorChoose","stringArrayKolektor","stringArrayCabang"})
	public void chooseCabang() {
		if (listCabangMstDtoChoose.size() != 0) {
			stringArrayCabang = "";
			for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
				if (i == 0) {
					stringArrayCabang = stringArrayCabang+listCabangMstDtoChoose.get(i).getNamaCabang();
				}else {
					stringArrayCabang = stringArrayCabang+", "+listCabangMstDtoChoose.get(i).getNamaCabang();
				}
			}
		}else {
			stringArrayCabang ="";
			listCabangMstDtoChoose = new ArrayList<CabangMstDto>();
		}
		stringArrayKolektor = "";
		listUserKolektorChoose = new ArrayList<UserProfileDto>();
		setVisibleCabang(false);
		
	}
	
	@Command("closeCabang")
	@NotifyChange("visibleCabang")
	public void closeCabang() {
		setVisibleCabang(false);
	}
	
	@Command("showKolektor")
	@NotifyChange({"keySearchKolektor","visibleKolektor","listUserKolektor"})
	public void showKolektor() {
		keySearchKolektor = "";
		if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() == 0) { 
			listUserKolektor = new ArrayList<UserProfileDto>();
			for (int i = 0; i < listAksesCabang.size(); i++) {
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeCabangAndSearch(listAksesCabang.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserKolektor.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserKolektor.size(); k++) {
								if (listUserKolektor.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserKolektor.add(kol);
							}
						}else {
							listUserKolektor.add(kol);
						}
					}
				}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserKolektor.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserKolektor.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserKolektor = listTemp;
				
			}
			if (listUserKolektor.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserKolektor.add(up);
				setVisibleKolektor(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}else if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() != 0) {
			listUserKolektor = new ArrayList<UserProfileDto>();
			for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeCabangAndSearch(listCabangMstDtoChoose.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserKolektor.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserKolektor.size(); k++) {
								if (listUserKolektor.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserKolektor.add(kol);
							}
						}else {
							listUserKolektor.add(kol);
						}
					}
				}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserKolektor.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserKolektor.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserKolektor = listTemp;
				
			}
			if (listUserKolektor.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserKolektor.add(up);
				setVisibleKolektor(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
			
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() != 0) {
			listUserKolektor = new ArrayList<UserProfileDto>();
			for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeCabangAndSearch(listCabangMstDtoChoose.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserKolektor.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserKolektor.size(); k++) {
								if (listUserKolektor.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserKolektor.add(kol);
							}
						}else {
							listUserKolektor.add(kol);
						}
					}
				}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserKolektor.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserKolektor.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserKolektor = listTemp;
				
			}
			if (listUserKolektor.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserKolektor.add(up);
				setVisibleKolektor(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
			
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() == 0) {
			System.out.println("Masuk sini ke reg doang");
			listUserKolektor = new ArrayList<UserProfileDto>();
			for (int h = 0; h < listRegionalDtoChoose.size(); h++) {
			for (int i = 0; i < listAksesCabang.size(); i++) {
				System.out.println("masuk loop reg ke = "+h);
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeRegionalKodeCabangAndSearch(listRegionalDtoChoose.get(h).getKodeRegion(),listAksesCabang.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserKolektor.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserKolektor.size(); k++) {
								if (listUserKolektor.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserKolektor.add(kol);
							}
						}else {
							listUserKolektor.add(kol);
						}
					}
				}
			}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserKolektor.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserKolektor.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserKolektor = listTemp;
				
			}
			if (listUserKolektor.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserKolektor.add(up);
				setVisibleKolektor(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}
	}
	
	@Command("showKolektorSearch")
	@NotifyChange({"visibleKolektor","listUserKolektor"})
	public void showKolektorSearch() {
		if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() == 0) { 
			listUserKolektor = new ArrayList<UserProfileDto>();
			for (int i = 0; i < listAksesCabang.size(); i++) {
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeCabangAndSearch(listAksesCabang.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserKolektor.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserKolektor.size(); k++) {
								if (listUserKolektor.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserKolektor.add(kol);
							}
						}else {
							listUserKolektor.add(kol);
						}
					}
				}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserKolektor.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserKolektor.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserKolektor = listTemp;
				
			}
			if (listUserKolektor.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserKolektor.add(up);
				setVisibleKolektor(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}else if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() != 0) {
			listUserKolektor = new ArrayList<UserProfileDto>();
			for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeCabangAndSearch(listCabangMstDtoChoose.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserKolektor.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserKolektor.size(); k++) {
								if (listUserKolektor.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserKolektor.add(kol);
							}
						}else {
							listUserKolektor.add(kol);
						}
					}
				}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserKolektor.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserKolektor.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserKolektor = listTemp;
				
			}
			if (listUserKolektor.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserKolektor.add(up);
				setVisibleKolektor(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
			
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() != 0) {
			listUserKolektor = new ArrayList<UserProfileDto>();
			for (int i = 0; i < listCabangMstDtoChoose.size(); i++) {
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeCabangAndSearch(listCabangMstDtoChoose.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserKolektor.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserKolektor.size(); k++) {
								if (listUserKolektor.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserKolektor.add(kol);
							}
						}else {
							listUserKolektor.add(kol);
						}
					}
				}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserKolektor.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserKolektor.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserKolektor = listTemp;
				
			}
			if (listUserKolektor.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserKolektor.add(up);
				setVisibleKolektor(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
			
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() == 0) {
			System.out.println("Masuk sini ke reg doang");
			listUserKolektor = new ArrayList<UserProfileDto>();
			for (int h = 0; h < listRegionalDtoChoose.size(); h++) {
			for (int i = 0; i < listAksesCabang.size(); i++) {
				System.out.println("masuk loop reg ke = "+h);
				List<UserProfileDto> listTempKol = userProfileSvc.findUserAllByKodeRegionalKodeCabangAndSearch(listRegionalDtoChoose.get(h).getKodeRegion(),listAksesCabang.get(i).getKodeCabang(), keySearchKolektor);
				if (listTempKol.size() != 0) {
					for (int j = 0; j < listTempKol.size(); j++) {
						UserProfileDto kol = new UserProfileDto();
						kol.setUserId(listTempKol.get(j).getUserId());
						kol.setNamaKaryawan(listTempKol.get(j).getNamaKaryawan());
						kol.setKodeKolektor(listTempKol.get(j).getKodeKolektor());
						kol.setItemList(true);
						if (listUserKolektor.size() != 0) {
							boolean sama = false;
							for (int k = 0; k < listUserKolektor.size(); k++) {
								if (listUserKolektor.get(k).getUserId().equalsIgnoreCase(listTempKol.get(j).getUserId())) {
									sama = true;
								}
							}
							if (sama == false) {
								listUserKolektor.add(kol);
							}
						}else {
							listUserKolektor.add(kol);
						}
					}
				}
			}
			}
			if (listUserKolektorChoose.size() != 0 ) {
				List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
				for (int i = 0; i < listUserKolektor.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listUserKolektorChoose.size(); j++) {
						if (listUserKolektor.get(i).getUserId().equals(listUserKolektorChoose.get(j).getUserId())) {
							sama = true;
						}
					}
					if (sama == true) {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(true);
						kole.setItemList(true);
						listTemp.add(kole);
					}else {
						UserProfileDto kole = new UserProfileDto();
						kole.setUserId(listUserKolektor.get(i).getUserId());
						kole.setNamaKaryawan(listUserKolektor.get(i).getNamaKaryawan());
						kole.setKodeKolektor(listUserKolektor.get(i).getKodeKolektor());
						kole.setChoose(false);
						kole.setItemList(true);
						listTemp.add(kole);
					}
				}
				listUserKolektor = listTemp;
				
			}
			if (listUserKolektor.size() != 0) {
				UserProfileDto up = new UserProfileDto();
				up.setUserId("");
				up.setNamaKaryawan("");
				up.setKodeKolektor("");
				up.setItemList(false);
				listUserKolektor.add(up);
				setVisibleKolektor(true);
			}else {
				Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
			}
		}
	}
	
	@Command("chooseKolektor")
	@NotifyChange({"listUserKolektorChoose","visibleKolektor","stringArrayKolektor"})
	public void chooseKolektor() {
		if (listUserKolektorChoose.size() != 0) {
			stringArrayKolektor = "";
			for (int i = 0; i < listUserKolektorChoose.size(); i++) {
				if (i == 0  ) {
					stringArrayKolektor = stringArrayKolektor+listUserKolektorChoose.get(i).getNamaKaryawan();
				}else {
					stringArrayKolektor = stringArrayKolektor+", "+listUserKolektorChoose.get(i).getNamaKaryawan();
				}
			}
		}else {
			listUserKolektorChoose = new ArrayList<UserProfileDto>();
			stringArrayKolektor = "";
		}
		setVisibleKolektor(false);
	}
	
	@Command("closeKolektor")
	@NotifyChange("visibleKolektor")
	public void closeKolektor() {
		setVisibleKolektor(false);
	}
	
	@Command("searchKolektor")
	@NotifyChange("visibleKolektor")
	public void searchKolektor() {
		setVisibleKolektor(false);
	}
	
	@Command("searchRegional")
	@NotifyChange("visibleKolektor")
	public void searchRegional() {
		setVisibleKolektor(false);
	}
	
	@Command("searchCabang")
	@NotifyChange("visibleKolektor")
	public void searchCabang() {
		setVisibleKolektor(false);
	}
	
	@Command("page")
	@NotifyChange("page")
	public void page() {
		setPage(getPage());
	}
	@Command("pageRegional")
	@NotifyChange("pageRegional")
	public void pageRegional() {
		setPageRegional(getPageRegional());
	}
	
	@Command("pageDetail")
	@NotifyChange("pageDetail")
	public void pageDetail() {
		setPageDetail(getPageDetail());
	}
	
	@Command("pageCabang")
	@NotifyChange("pageCabang")
	public void pageCabang() {
		setPageCabang(getPageCabang());
	}
	@Command("pageKolektor")
	@NotifyChange("pageRegional")
	public void pageKolektor() {
		setPageKolektor(getPageKolektor());
	}
	
	@Command("search")
	@NotifyChange({"listCollectionDetailDto","keySearch"})
	public void search() {
		keySearch = "";
		listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
		if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() == 0 && listUserKolektorChoose.size() == 0) {
			for (int j = 0; j < listRegionalDtoChoose.size(); j++) {
				for (int k = 0; k < listAksesCabang.size(); k++) {
					if (collectionDetailSvc.selectReportKunjunganKodeRegionalKodeCabang(tglAwal, tglAkhir, listRegionalDtoChoose.get(j).getKodeRegion(), listAksesCabang.get(k).getKodeCabang(),keySearch) != null) {
						List<CollectionDetailDto> listTemp = new ArrayList<CollectionDetailDto>();
						listTemp = collectionDetailSvc.selectReportKunjunganKodeRegionalKodeCabang(tglAwal, tglAkhir, listRegionalDtoChoose.get(j).getKodeRegion(), listAksesCabang.get(k).getKodeCabang(), keySearch);
						for (int i = 0; i < listTemp.size(); i++) {
							if (listCollectionDetailDto.size() != 0) {
								boolean sama = false;
								for (int g = 0; g < listCollectionDetailDto.size(); g++) {
									if (listCollectionDetailDto.get(g).getCollectorCode().equalsIgnoreCase(listTemp.get(i).getCollectorCode())) {
										sama = true;
									}
								}
								if (sama == false) {
									CollectionDetailDto cd = new CollectionDetailDto();
									cd.setCollectorCode(listTemp.get(i).getCollectorCode());
									cd.setNamaKaryawan(listTemp.get(i).getNamaKaryawan());
									cd.setNamaCabang(listTemp.get(i).getNamaCabang());
									cd.setJumlahKunjungan(listTemp.get(i).getJumlahKunjungan());
									cd.setIntervalWaktu(listTemp.get(i).getIntervalWaktu());
									cd.setUserId(listTemp.get(i).getUserId());
									listCollectionDetailDto.add(cd);
								}
							}else {
								CollectionDetailDto cd = new CollectionDetailDto();
								cd.setCollectorCode(listTemp.get(i).getCollectorCode());
								cd.setNamaKaryawan(listTemp.get(i).getNamaKaryawan());
								cd.setNamaCabang(listTemp.get(i).getNamaCabang());
								cd.setJumlahKunjungan(listTemp.get(i).getJumlahKunjungan());
								cd.setIntervalWaktu(listTemp.get(i).getIntervalWaktu());
								cd.setUserId(listTemp.get(i).getUserId());
								listCollectionDetailDto.add(cd);
							}
						}
					}
				}
			}
		}else if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() != 0 && listUserKolektorChoose.size() == 0) {
			for (int j = 0; j < listCabangMstDtoChoose.size(); j++) {
				if (collectionDetailSvc.selectReportKunjunganKodeCabang(tglAwal, tglAkhir, listCabangMstDtoChoose.get(j).getKodeCabang(),keySearch) != null) {
					List<CollectionDetailDto> listTemp = new ArrayList<CollectionDetailDto>();
					listTemp = collectionDetailSvc.selectReportKunjunganKodeCabang(tglAwal, tglAkhir, listCabangMstDtoChoose.get(j).getKodeCabang(),keySearch);
					for (int i = 0; i < listTemp.size(); i++) {
						if (listCollectionDetailDto.size() != 0) {
							boolean sama = false;
							for (int g = 0; g < listCollectionDetailDto.size(); g++) {
								if (listCollectionDetailDto.get(g).getCollectorCode().equalsIgnoreCase(listTemp.get(i).getCollectorCode())) {
									sama = true;
								}
							}
							if (sama == false) {
								CollectionDetailDto cd = new CollectionDetailDto();
								cd.setCollectorCode(listTemp.get(i).getCollectorCode());
								cd.setNamaKaryawan(listTemp.get(i).getNamaKaryawan());
								cd.setNamaCabang(listTemp.get(i).getNamaCabang());
								cd.setJumlahKunjungan(listTemp.get(i).getJumlahKunjungan());
								cd.setIntervalWaktu(listTemp.get(i).getIntervalWaktu());
								cd.setUserId(listTemp.get(i).getUserId());
								listCollectionDetailDto.add(cd);
							}
						}else {
							CollectionDetailDto cd = new CollectionDetailDto();
							cd.setCollectorCode(listTemp.get(i).getCollectorCode());
							cd.setNamaKaryawan(listTemp.get(i).getNamaKaryawan());
							cd.setNamaCabang(listTemp.get(i).getNamaCabang());
							cd.setJumlahKunjungan(listTemp.get(i).getJumlahKunjungan());
							cd.setIntervalWaktu(listTemp.get(i).getIntervalWaktu());
							cd.setUserId(listTemp.get(i).getUserId());
							listCollectionDetailDto.add(cd);
						}
					}
				}
			}
		}else if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() == 0 && listUserKolektorChoose.size() != 0) {
			for (int j = 0; j < listAksesCabang.size(); j++) {
				for (int k = 0; k < listUserKolektorChoose.size(); k++) {
					if (collectionDetailSvc.selectReportKunjunganKodeCabangKodeKolektor(tglAwal, tglAkhir, listAksesCabang.get(j).getKodeCabang(), listUserKolektorChoose.get(k).getUserId(),keySearch) != null) {
						List<CollectionDetailDto> listTemp = new ArrayList<CollectionDetailDto>();
						listTemp = collectionDetailSvc.selectReportKunjunganKodeCabangKodeKolektor(tglAwal, tglAkhir, listCabangMstDtoChoose.get(j).getKodeCabang(),listUserKolektorChoose.get(k).getUserId(),keySearch);
						for (int i = 0; i < listTemp.size(); i++) {
							if (listCollectionDetailDto.size() != 0) {
								boolean sama = false;
								for (int g = 0; g < listCollectionDetailDto.size(); g++) {
									if (listCollectionDetailDto.get(g).getCollectorCode().equalsIgnoreCase(listTemp.get(i).getCollectorCode())) {
										sama = true;
									}
								}
								if (sama == false) {
									CollectionDetailDto cd = new CollectionDetailDto();
									cd.setCollectorCode(listTemp.get(i).getCollectorCode());
									cd.setNamaKaryawan(listTemp.get(i).getNamaKaryawan());
									cd.setNamaCabang(listTemp.get(i).getNamaCabang());
									cd.setJumlahKunjungan(listTemp.get(i).getJumlahKunjungan());
									cd.setIntervalWaktu(listTemp.get(i).getIntervalWaktu());
									cd.setUserId(listTemp.get(i).getUserId());
									listCollectionDetailDto.add(cd);
								}
							}else {
								CollectionDetailDto cd = new CollectionDetailDto();
								cd.setCollectorCode(listTemp.get(i).getCollectorCode());
								cd.setNamaKaryawan(listTemp.get(i).getNamaKaryawan());
								cd.setNamaCabang(listTemp.get(i).getNamaCabang());
								cd.setJumlahKunjungan(listTemp.get(i).getJumlahKunjungan());
								cd.setIntervalWaktu(listTemp.get(i).getIntervalWaktu());
								cd.setUserId(listTemp.get(i).getUserId());
								listCollectionDetailDto.add(cd);
							}
						}
					}
				}
			}
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() != 0 && listUserKolektorChoose.size() == 0) {
			for (int j = 0; j < listCabangMstDtoChoose.size(); j++) {
				if (collectionDetailSvc.selectReportKunjunganKodeCabang(tglAwal, tglAkhir, listCabangMstDtoChoose.get(j).getKodeCabang(),keySearch) != null) {
					List<CollectionDetailDto> listTemp = new ArrayList<CollectionDetailDto>();
					listTemp = collectionDetailSvc.selectReportKunjunganKodeCabang(tglAwal, tglAkhir, listCabangMstDtoChoose.get(j).getKodeCabang(),keySearch);
					for (int i = 0; i < listTemp.size(); i++) {
						if (listCollectionDetailDto.size() != 0) {
							boolean sama = false;
							for (int g = 0; g < listCollectionDetailDto.size(); g++) {
								if (listCollectionDetailDto.get(g).getCollectorCode().equalsIgnoreCase(listTemp.get(i).getCollectorCode())) {
									sama = true;
								}
							}
							if (sama == false) {
								CollectionDetailDto cd = new CollectionDetailDto();
								cd.setCollectorCode(listTemp.get(i).getCollectorCode());
								cd.setNamaKaryawan(listTemp.get(i).getNamaKaryawan());
								cd.setNamaCabang(listTemp.get(i).getNamaCabang());
								cd.setJumlahKunjungan(listTemp.get(i).getJumlahKunjungan());
								cd.setIntervalWaktu(listTemp.get(i).getIntervalWaktu());
								cd.setUserId(listTemp.get(i).getUserId());
								listCollectionDetailDto.add(cd);
							}
						}else {
							CollectionDetailDto cd = new CollectionDetailDto();
							cd.setCollectorCode(listTemp.get(i).getCollectorCode());
							cd.setNamaKaryawan(listTemp.get(i).getNamaKaryawan());
							cd.setNamaCabang(listTemp.get(i).getNamaCabang());
							cd.setJumlahKunjungan(listTemp.get(i).getJumlahKunjungan());
							cd.setIntervalWaktu(listTemp.get(i).getIntervalWaktu());
							cd.setUserId(listTemp.get(i).getUserId());
							listCollectionDetailDto.add(cd);
						}
					}
				}
			}
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() == 0 && listUserKolektorChoose.size() != 0) {
			for (int j = 0; j < listRegionalDtoChoose.size(); j++) {
				for (int k = 0; k < listUserKolektorChoose.size(); k++) {
					if (collectionDetailSvc.selectReportKunjunganKodeRegionalKodeKolektor(tglAwal, tglAkhir, listRegionalDtoChoose.get(j).getKodeRegion(), listUserKolektorChoose.get(k).getKodeKolektor(),keySearch) != null) {
						List<CollectionDetailDto> listTemp = new ArrayList<CollectionDetailDto>();
						listTemp = collectionDetailSvc.selectReportKunjunganKodeRegionalKodeKolektor(tglAwal, tglAkhir, listRegionalDtoChoose.get(j).getKodeRegion(), listUserKolektorChoose.get(k).getKodeKolektor(),keySearch);
						for (int i = 0; i < listTemp.size(); i++) {
							if (listCollectionDetailDto.size() != 0) {
								boolean sama = false;
								for (int g = 0; g < listCollectionDetailDto.size(); g++) {
									if (listCollectionDetailDto.get(g).getCollectorCode().equalsIgnoreCase(listTemp.get(i).getCollectorCode())) {
										sama = true;
									}
								}
								if (sama == false) {
									CollectionDetailDto cd = new CollectionDetailDto();
									cd.setCollectorCode(listTemp.get(i).getCollectorCode());
									cd.setNamaKaryawan(listTemp.get(i).getNamaKaryawan());
									cd.setNamaCabang(listTemp.get(i).getNamaCabang());
									cd.setJumlahKunjungan(listTemp.get(i).getJumlahKunjungan());
									cd.setIntervalWaktu(listTemp.get(i).getIntervalWaktu());
									cd.setUserId(listTemp.get(i).getUserId());
									listCollectionDetailDto.add(cd);
								}
							}else {
								CollectionDetailDto cd = new CollectionDetailDto();
								cd.setCollectorCode(listTemp.get(i).getCollectorCode());
								cd.setNamaKaryawan(listTemp.get(i).getNamaKaryawan());
								cd.setNamaCabang(listTemp.get(i).getNamaCabang());
								cd.setJumlahKunjungan(listTemp.get(i).getJumlahKunjungan());
								cd.setIntervalWaktu(listTemp.get(i).getIntervalWaktu());
								cd.setUserId(listTemp.get(i).getUserId());
								listCollectionDetailDto.add(cd);
							}
						}
					}
				}
			}
		}else if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() != 0 && listUserKolektorChoose.size() != 0) {
			for (int j = 0; j < listCabangMstDtoChoose.size(); j++) {
				for (int k = 0; k < listUserKolektorChoose.size(); k++) {
					if (collectionDetailSvc.selectReportKunjunganKodeCabangKodeKolektor(tglAwal, tglAkhir, listCabangMstDtoChoose.get(j).getKodeCabang(), listUserKolektorChoose.get(k).getUserId(),keySearch) != null) {
						List<CollectionDetailDto> listTemp = new ArrayList<CollectionDetailDto>();
						listTemp = collectionDetailSvc.selectReportKunjunganKodeCabangKodeKolektor(tglAwal, tglAkhir, listCabangMstDtoChoose.get(j).getKodeCabang(), listUserKolektorChoose.get(k).getUserId(),keySearch);
						for (int i = 0; i < listTemp.size(); i++) {
							if (listCollectionDetailDto.size() != 0) {
								boolean sama = false;
								for (int g = 0; g < listCollectionDetailDto.size(); g++) {
									if (listCollectionDetailDto.get(g).getCollectorCode().equalsIgnoreCase(listTemp.get(i).getCollectorCode())) {
										sama = true;
									}
								}
								if (sama == false) {
									CollectionDetailDto cd = new CollectionDetailDto();
									cd.setCollectorCode(listTemp.get(i).getCollectorCode());
									cd.setNamaKaryawan(listTemp.get(i).getNamaKaryawan());
									cd.setNamaCabang(listTemp.get(i).getNamaCabang());
									cd.setJumlahKunjungan(listTemp.get(i).getJumlahKunjungan());
									cd.setIntervalWaktu(listTemp.get(i).getIntervalWaktu());
									cd.setUserId(listTemp.get(i).getUserId());
									listCollectionDetailDto.add(cd);
								}
							}else {
								CollectionDetailDto cd = new CollectionDetailDto();
								cd.setCollectorCode(listTemp.get(i).getCollectorCode());
								cd.setNamaKaryawan(listTemp.get(i).getNamaKaryawan());
								cd.setNamaCabang(listTemp.get(i).getNamaCabang());
								cd.setJumlahKunjungan(listTemp.get(i).getJumlahKunjungan());
								cd.setIntervalWaktu(listTemp.get(i).getIntervalWaktu());
								cd.setUserId(listTemp.get(i).getUserId());
								listCollectionDetailDto.add(cd);
							}
						}
					}
				}
			}
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() != 0 && listUserKolektorChoose.size() != 0) {
			for (int j = 0; j < listCabangMstDtoChoose.size(); j++) {
				for (int k = 0; k < listUserKolektorChoose.size(); k++) {
					if (collectionDetailSvc.selectReportKunjunganKodeCabangKodeKolektor(tglAwal, tglAkhir, listCabangMstDtoChoose.get(j).getKodeCabang(), listUserKolektorChoose.get(k).getUserId(),keySearch) != null) {
						List<CollectionDetailDto> listTemp = new ArrayList<CollectionDetailDto>();
						listTemp = collectionDetailSvc.selectReportKunjunganKodeCabangKodeKolektor(tglAwal, tglAkhir, listCabangMstDtoChoose.get(j).getKodeCabang(), listUserKolektorChoose.get(k).getUserId(),keySearch);
						for (int i = 0; i < listTemp.size(); i++) {
							if (listCollectionDetailDto.size() != 0) {
								boolean sama = false;
								for (int g = 0; g < listCollectionDetailDto.size(); g++) {
									if (listCollectionDetailDto.get(g).getCollectorCode().equalsIgnoreCase(listTemp.get(i).getCollectorCode())) {
										sama = true;
									}
								}
								if (sama == false) {
									CollectionDetailDto cd = new CollectionDetailDto();
									cd.setCollectorCode(listTemp.get(i).getCollectorCode());
									cd.setNamaKaryawan(listTemp.get(i).getNamaKaryawan());
									cd.setNamaCabang(listTemp.get(i).getNamaCabang());
									cd.setJumlahKunjungan(listTemp.get(i).getJumlahKunjungan());
									cd.setIntervalWaktu(listTemp.get(i).getIntervalWaktu());
									cd.setUserId(listTemp.get(i).getUserId());
									listCollectionDetailDto.add(cd);
								}
							}else {
								CollectionDetailDto cd = new CollectionDetailDto();
								cd.setCollectorCode(listTemp.get(i).getCollectorCode());
								cd.setNamaKaryawan(listTemp.get(i).getNamaKaryawan());
								cd.setNamaCabang(listTemp.get(i).getNamaCabang());
								cd.setJumlahKunjungan(listTemp.get(i).getJumlahKunjungan());
								cd.setIntervalWaktu(listTemp.get(i).getIntervalWaktu());
								cd.setUserId(listTemp.get(i).getUserId());
								listCollectionDetailDto.add(cd);
							}
						}
					}
				}
			}
		}else if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() == 0 && listUserKolektorChoose.size() == 0) {
			Messagebox.show("Isi terlebih dahulu parameter yang ada !", "MESSAGE", null, null, null);
		}
	}
	
	
	@Command("resultSearch")
	@NotifyChange({"listCollectionDetailDto"})
	public void resultSearch() {
		listCollectionDetailDto = new ArrayList<CollectionDetailDto>();
		if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() == 0 && listUserKolektorChoose.size() == 0) {
			for (int j = 0; j < listRegionalDtoChoose.size(); j++) {
				for (int k = 0; k < listAksesCabang.size(); k++) {
					if (collectionDetailSvc.selectReportKunjunganKodeRegionalKodeCabang(tglAwal, tglAkhir, listRegionalDtoChoose.get(j).getKodeRegion(), listAksesCabang.get(k).getKodeCabang(),keySearch) != null) {
						List<CollectionDetailDto> listTemp = new ArrayList<CollectionDetailDto>();
						listTemp = collectionDetailSvc.selectReportKunjunganKodeRegionalKodeCabang(tglAwal, tglAkhir, listRegionalDtoChoose.get(j).getKodeRegion(), listAksesCabang.get(k).getKodeCabang(), keySearch);
						for (int i = 0; i < listTemp.size(); i++) {
							if (listCollectionDetailDto.size() != 0) {
								boolean sama = false;
								for (int g = 0; g < listCollectionDetailDto.size(); g++) {
									if (listCollectionDetailDto.get(g).getCollectorCode().equalsIgnoreCase(listTemp.get(i).getCollectorCode())) {
										sama = true;
									}
								}
								if (sama == false) {
									CollectionDetailDto cd = new CollectionDetailDto();
									cd.setCollectorCode(listTemp.get(i).getCollectorCode());
									cd.setNamaKaryawan(listTemp.get(i).getNamaKaryawan());
									cd.setNamaCabang(listTemp.get(i).getNamaCabang());
									cd.setJumlahKunjungan(listTemp.get(i).getJumlahKunjungan());
									cd.setIntervalWaktu(listTemp.get(i).getIntervalWaktu());
									cd.setUserId(listTemp.get(i).getUserId());
									listCollectionDetailDto.add(cd);
								}
							}else {
								CollectionDetailDto cd = new CollectionDetailDto();
								cd.setCollectorCode(listTemp.get(i).getCollectorCode());
								cd.setNamaKaryawan(listTemp.get(i).getNamaKaryawan());
								cd.setNamaCabang(listTemp.get(i).getNamaCabang());
								cd.setJumlahKunjungan(listTemp.get(i).getJumlahKunjungan());
								cd.setIntervalWaktu(listTemp.get(i).getIntervalWaktu());
								cd.setUserId(listTemp.get(i).getUserId());
								listCollectionDetailDto.add(cd);
							}
						}
					}
				}
			}
		}else if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() != 0 && listUserKolektorChoose.size() == 0) {
			for (int j = 0; j < listCabangMstDtoChoose.size(); j++) {
				if (collectionDetailSvc.selectReportKunjunganKodeCabang(tglAwal, tglAkhir, listCabangMstDtoChoose.get(j).getKodeCabang(),keySearch) != null) {
					List<CollectionDetailDto> listTemp = new ArrayList<CollectionDetailDto>();
					listTemp = collectionDetailSvc.selectReportKunjunganKodeCabang(tglAwal, tglAkhir, listCabangMstDtoChoose.get(j).getKodeCabang(),keySearch);
					for (int i = 0; i < listTemp.size(); i++) {
						if (listCollectionDetailDto.size() != 0) {
							boolean sama = false;
							for (int g = 0; g < listCollectionDetailDto.size(); g++) {
								if (listCollectionDetailDto.get(g).getCollectorCode().equalsIgnoreCase(listTemp.get(i).getCollectorCode())) {
									sama = true;
								}
							}
							if (sama == false) {
								CollectionDetailDto cd = new CollectionDetailDto();
								cd.setCollectorCode(listTemp.get(i).getCollectorCode());
								cd.setNamaKaryawan(listTemp.get(i).getNamaKaryawan());
								cd.setNamaCabang(listTemp.get(i).getNamaCabang());
								cd.setJumlahKunjungan(listTemp.get(i).getJumlahKunjungan());
								cd.setIntervalWaktu(listTemp.get(i).getIntervalWaktu());
								cd.setUserId(listTemp.get(i).getUserId());
								listCollectionDetailDto.add(cd);
							}
						}else {
							CollectionDetailDto cd = new CollectionDetailDto();
							cd.setCollectorCode(listTemp.get(i).getCollectorCode());
							cd.setNamaKaryawan(listTemp.get(i).getNamaKaryawan());
							cd.setNamaCabang(listTemp.get(i).getNamaCabang());
							cd.setJumlahKunjungan(listTemp.get(i).getJumlahKunjungan());
							cd.setIntervalWaktu(listTemp.get(i).getIntervalWaktu());
							cd.setUserId(listTemp.get(i).getUserId());
							listCollectionDetailDto.add(cd);
						}
					}
				}
			}
		}else if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() == 0 && listUserKolektorChoose.size() != 0) {
			for (int j = 0; j < listAksesCabang.size(); j++) {
				for (int k = 0; k < listUserKolektorChoose.size(); k++) {
					if (collectionDetailSvc.selectReportKunjunganKodeCabangKodeKolektor(tglAwal, tglAkhir, listAksesCabang.get(j).getKodeCabang(), listUserKolektorChoose.get(k).getUserId(),keySearch) != null) {
						List<CollectionDetailDto> listTemp = new ArrayList<CollectionDetailDto>();
						listTemp = collectionDetailSvc.selectReportKunjunganKodeCabangKodeKolektor(tglAwal, tglAkhir, listCabangMstDtoChoose.get(j).getKodeCabang(),listUserKolektorChoose.get(k).getUserId(),keySearch);
						for (int i = 0; i < listTemp.size(); i++) {
							if (listCollectionDetailDto.size() != 0) {
								boolean sama = false;
								for (int g = 0; g < listCollectionDetailDto.size(); g++) {
									if (listCollectionDetailDto.get(g).getCollectorCode().equalsIgnoreCase(listTemp.get(i).getCollectorCode())) {
										sama = true;
									}
								}
								if (sama == false) {
									CollectionDetailDto cd = new CollectionDetailDto();
									cd.setCollectorCode(listTemp.get(i).getCollectorCode());
									cd.setNamaKaryawan(listTemp.get(i).getNamaKaryawan());
									cd.setNamaCabang(listTemp.get(i).getNamaCabang());
									cd.setJumlahKunjungan(listTemp.get(i).getJumlahKunjungan());
									cd.setIntervalWaktu(listTemp.get(i).getIntervalWaktu());
									cd.setUserId(listTemp.get(i).getUserId());
									listCollectionDetailDto.add(cd);
								}
							}else {
								CollectionDetailDto cd = new CollectionDetailDto();
								cd.setCollectorCode(listTemp.get(i).getCollectorCode());
								cd.setNamaKaryawan(listTemp.get(i).getNamaKaryawan());
								cd.setNamaCabang(listTemp.get(i).getNamaCabang());
								cd.setJumlahKunjungan(listTemp.get(i).getJumlahKunjungan());
								cd.setIntervalWaktu(listTemp.get(i).getIntervalWaktu());
								cd.setUserId(listTemp.get(i).getUserId());
								listCollectionDetailDto.add(cd);
							}
						}
					}
				}
			}
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() != 0 && listUserKolektorChoose.size() == 0) {
			for (int j = 0; j < listCabangMstDtoChoose.size(); j++) {
				if (collectionDetailSvc.selectReportKunjunganKodeCabang(tglAwal, tglAkhir, listCabangMstDtoChoose.get(j).getKodeCabang(),keySearch) != null) {
					List<CollectionDetailDto> listTemp = new ArrayList<CollectionDetailDto>();
					listTemp = collectionDetailSvc.selectReportKunjunganKodeCabang(tglAwal, tglAkhir, listCabangMstDtoChoose.get(j).getKodeCabang(),keySearch);
					for (int i = 0; i < listTemp.size(); i++) {
						if (listCollectionDetailDto.size() != 0) {
							boolean sama = false;
							for (int g = 0; g < listCollectionDetailDto.size(); g++) {
								if (listCollectionDetailDto.get(g).getCollectorCode().equalsIgnoreCase(listTemp.get(i).getCollectorCode())) {
									sama = true;
								}
							}
							if (sama == false) {
								CollectionDetailDto cd = new CollectionDetailDto();
								cd.setCollectorCode(listTemp.get(i).getCollectorCode());
								cd.setNamaKaryawan(listTemp.get(i).getNamaKaryawan());
								cd.setNamaCabang(listTemp.get(i).getNamaCabang());
								cd.setJumlahKunjungan(listTemp.get(i).getJumlahKunjungan());
								cd.setIntervalWaktu(listTemp.get(i).getIntervalWaktu());
								cd.setUserId(listTemp.get(i).getUserId());
								listCollectionDetailDto.add(cd);
							}
						}else {
							CollectionDetailDto cd = new CollectionDetailDto();
							cd.setCollectorCode(listTemp.get(i).getCollectorCode());
							cd.setNamaKaryawan(listTemp.get(i).getNamaKaryawan());
							cd.setNamaCabang(listTemp.get(i).getNamaCabang());
							cd.setJumlahKunjungan(listTemp.get(i).getJumlahKunjungan());
							cd.setIntervalWaktu(listTemp.get(i).getIntervalWaktu());
							cd.setUserId(listTemp.get(i).getUserId());
							listCollectionDetailDto.add(cd);
						}
					}
				}
			}
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() == 0 && listUserKolektorChoose.size() != 0) {
			for (int j = 0; j < listRegionalDtoChoose.size(); j++) {
				for (int k = 0; k < listUserKolektorChoose.size(); k++) {
					if (collectionDetailSvc.selectReportKunjunganKodeRegionalKodeKolektor(tglAwal, tglAkhir, listRegionalDtoChoose.get(j).getKodeRegion(), listUserKolektorChoose.get(k).getKodeKolektor(),keySearch) != null) {
						List<CollectionDetailDto> listTemp = new ArrayList<CollectionDetailDto>();
						listTemp = collectionDetailSvc.selectReportKunjunganKodeRegionalKodeKolektor(tglAwal, tglAkhir, listRegionalDtoChoose.get(j).getKodeRegion(), listUserKolektorChoose.get(k).getKodeKolektor(),keySearch);
						for (int i = 0; i < listTemp.size(); i++) {
							if (listCollectionDetailDto.size() != 0) {
								boolean sama = false;
								for (int g = 0; g < listCollectionDetailDto.size(); g++) {
									if (listCollectionDetailDto.get(g).getCollectorCode().equalsIgnoreCase(listTemp.get(i).getCollectorCode())) {
										sama = true;
									}
								}
								if (sama == false) {
									CollectionDetailDto cd = new CollectionDetailDto();
									cd.setCollectorCode(listTemp.get(i).getCollectorCode());
									cd.setNamaKaryawan(listTemp.get(i).getNamaKaryawan());
									cd.setNamaCabang(listTemp.get(i).getNamaCabang());
									cd.setJumlahKunjungan(listTemp.get(i).getJumlahKunjungan());
									cd.setIntervalWaktu(listTemp.get(i).getIntervalWaktu());
									cd.setUserId(listTemp.get(i).getUserId());
									listCollectionDetailDto.add(cd);
								}
							}else {
								CollectionDetailDto cd = new CollectionDetailDto();
								cd.setCollectorCode(listTemp.get(i).getCollectorCode());
								cd.setNamaKaryawan(listTemp.get(i).getNamaKaryawan());
								cd.setNamaCabang(listTemp.get(i).getNamaCabang());
								cd.setJumlahKunjungan(listTemp.get(i).getJumlahKunjungan());
								cd.setIntervalWaktu(listTemp.get(i).getIntervalWaktu());
								cd.setUserId(listTemp.get(i).getUserId());
								listCollectionDetailDto.add(cd);
							}
						}
					}
				}
			}
		}else if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() != 0 && listUserKolektorChoose.size() != 0) {
			for (int j = 0; j < listCabangMstDtoChoose.size(); j++) {
				for (int k = 0; k < listUserKolektorChoose.size(); k++) {
					if (collectionDetailSvc.selectReportKunjunganKodeCabangKodeKolektor(tglAwal, tglAkhir, listCabangMstDtoChoose.get(j).getKodeCabang(), listUserKolektorChoose.get(k).getUserId(),keySearch) != null) {
						List<CollectionDetailDto> listTemp = new ArrayList<CollectionDetailDto>();
						listTemp = collectionDetailSvc.selectReportKunjunganKodeCabangKodeKolektor(tglAwal, tglAkhir, listCabangMstDtoChoose.get(j).getKodeCabang(), listUserKolektorChoose.get(k).getUserId(),keySearch);
						for (int i = 0; i < listTemp.size(); i++) {
							if (listCollectionDetailDto.size() != 0) {
								boolean sama = false;
								for (int g = 0; g < listCollectionDetailDto.size(); g++) {
									if (listCollectionDetailDto.get(g).getCollectorCode().equalsIgnoreCase(listTemp.get(i).getCollectorCode())) {
										sama = true;
									}
								}
								if (sama == false) {
									CollectionDetailDto cd = new CollectionDetailDto();
									cd.setCollectorCode(listTemp.get(i).getCollectorCode());
									cd.setNamaKaryawan(listTemp.get(i).getNamaKaryawan());
									cd.setNamaCabang(listTemp.get(i).getNamaCabang());
									cd.setJumlahKunjungan(listTemp.get(i).getJumlahKunjungan());
									cd.setIntervalWaktu(listTemp.get(i).getIntervalWaktu());
									cd.setUserId(listTemp.get(i).getUserId());
									listCollectionDetailDto.add(cd);
								}
							}else {
								CollectionDetailDto cd = new CollectionDetailDto();
								cd.setCollectorCode(listTemp.get(i).getCollectorCode());
								cd.setNamaKaryawan(listTemp.get(i).getNamaKaryawan());
								cd.setNamaCabang(listTemp.get(i).getNamaCabang());
								cd.setJumlahKunjungan(listTemp.get(i).getJumlahKunjungan());
								cd.setIntervalWaktu(listTemp.get(i).getIntervalWaktu());
								cd.setUserId(listTemp.get(i).getUserId());
								listCollectionDetailDto.add(cd);
							}
						}
					}
				}
			}
		}else if (listRegionalDtoChoose.size() != 0 && listCabangMstDtoChoose.size() != 0 && listUserKolektorChoose.size() != 0) {
			for (int j = 0; j < listCabangMstDtoChoose.size(); j++) {
				for (int k = 0; k < listUserKolektorChoose.size(); k++) {
					if (collectionDetailSvc.selectReportKunjunganKodeCabangKodeKolektor(tglAwal, tglAkhir, listCabangMstDtoChoose.get(j).getKodeCabang(), listUserKolektorChoose.get(k).getUserId(),keySearch) != null) {
						List<CollectionDetailDto> listTemp = new ArrayList<CollectionDetailDto>();
						listTemp = collectionDetailSvc.selectReportKunjunganKodeCabangKodeKolektor(tglAwal, tglAkhir, listCabangMstDtoChoose.get(j).getKodeCabang(), listUserKolektorChoose.get(k).getUserId(),keySearch);
						for (int i = 0; i < listTemp.size(); i++) {
							if (listCollectionDetailDto.size() != 0) {
								boolean sama = false;
								for (int g = 0; g < listCollectionDetailDto.size(); g++) {
									if (listCollectionDetailDto.get(g).getCollectorCode().equalsIgnoreCase(listTemp.get(i).getCollectorCode())) {
										sama = true;
									}
								}
								if (sama == false) {
									CollectionDetailDto cd = new CollectionDetailDto();
									cd.setCollectorCode(listTemp.get(i).getCollectorCode());
									cd.setNamaKaryawan(listTemp.get(i).getNamaKaryawan());
									cd.setNamaCabang(listTemp.get(i).getNamaCabang());
									cd.setJumlahKunjungan(listTemp.get(i).getJumlahKunjungan());
									cd.setIntervalWaktu(listTemp.get(i).getIntervalWaktu());
									cd.setUserId(listTemp.get(i).getUserId());
									listCollectionDetailDto.add(cd);
								}
							}else {
								CollectionDetailDto cd = new CollectionDetailDto();
								cd.setCollectorCode(listTemp.get(i).getCollectorCode());
								cd.setNamaKaryawan(listTemp.get(i).getNamaKaryawan());
								cd.setNamaCabang(listTemp.get(i).getNamaCabang());
								cd.setJumlahKunjungan(listTemp.get(i).getJumlahKunjungan());
								cd.setIntervalWaktu(listTemp.get(i).getIntervalWaktu());
								cd.setUserId(listTemp.get(i).getUserId());
								listCollectionDetailDto.add(cd);
							}
						}
					}
				}
			}
		}else if (listRegionalDtoChoose.size() == 0 && listCabangMstDtoChoose.size() == 0 && listUserKolektorChoose.size() == 0) {
			Messagebox.show("Isi terlebih dahulu parameter yang ada !", "MESSAGE", null, null, null);
		}
	}
	
	@Command("downloadReport")
	@NotifyChange({"dataObjek","reportConfigRP","listOrder"})
	public void downloadReport() throws JRException, IOException{
		if (export.equalsIgnoreCase("pdf")) {
			
		}else {
			try {
				HSSFWorkbook workbook = new HSSFWorkbook();
				HSSFSheet sheet = workbook
						.createSheet("KUNJUNGAN KOLEKTOR DETAILS");

				sheet.autoSizeColumn(1);
				sheet.addMergedRegion(new CellRangeAddress(1, 2, 1, 13));
				HSSFRow judul = sheet.createRow((short) 1);
				judul.createCell(1)
						.setCellValue("KUNJUNGAN KOLEKTOR DETAILS");
				
				HSSFRow rowhead = sheet.createRow((short) 5);
				rowhead.createCell(1).setCellValue("TANGGAL TAGIH");
				rowhead.createCell(2).setCellValue(": " +collectionDetailDto.getIntervalWaktu());

				HSSFRow rowhead2 = sheet.createRow((short) 6);
				rowhead2.createCell(1).setCellValue("NAMA KOLEKTOR");
				rowhead2.createCell(2).setCellValue(": " +collectionDetailDto.getNamaKaryawan());

				HSSFRow rowhead3 = sheet.createRow((short) 7);
				rowhead3.createCell(1).setCellValue("JUMLAH KUNJUNGAN");
				rowhead3.createCell(2).setCellValue(": " +collectionDetailDto.getJumlahKunjungan());

				HSSFRow rowdata = sheet.createRow((short) 10);
				rowdata.createCell(1).setCellValue("NO");
				rowdata.createCell(2).setCellValue("NAMA KONSUMEN");
				rowdata.createCell(3).setCellValue("TGL BAYAR");
				rowdata.createCell(4).setCellValue("TGL JANJI BAYAR");
				rowdata.createCell(5).setCellValue("ALASAN");
				rowdata.createCell(6).setCellValue("JUMLAH BAYAR");
				rowdata.createCell(7).setCellValue("TGL KUNJUNGAN");
				rowdata.createCell(8).setCellValue("STATUS");

				for (int i = 0; i < listCollectionDetailViewDto.size(); i++) {
					HSSFRow rowisi = sheet.createRow((short) 11 + i);
					rowisi.createCell(1).setCellValue(i+1);
					rowisi.createCell(2).setCellValue(
							listCollectionDetailViewDto.get(i).getCustomerName());
					rowisi.createCell(3).setCellValue(
							listCollectionDetailViewDto.get(i).getTglBayar());
					rowisi.createCell(4).setCellValue(
							listCollectionDetailViewDto.get(i).getTglJanjiBayar());
					rowisi.createCell(5).setCellValue(
							listCollectionDetailViewDto.get(i).getNotes());
					rowisi.createCell(6).setCellValue(
							listCollectionDetailViewDto.get(i).getJumlahBayar());
					rowisi.createCell(7).setCellValue(
							listCollectionDetailViewDto.get(i).getTglKunjungan());
					rowisi.createCell(8).setCellValue(
							listCollectionDetailViewDto.get(i).getStatus());
				}
				/*for(int columnIndex = 0; columnIndex <=25; columnIndex++) {
	                sheet.autoSizeColumn(columnIndex);
	           }*/
				File fileTempToDownload = File.createTempFile("kunjunganKolektorDetails", ".xls");
				FileOutputStream tempOs = new FileOutputStream(fileTempToDownload);
				workbook.write(tempOs);
				FileInputStream tempIs = new FileInputStream(fileTempToDownload);
				String tempFileName = "Kunjungan_"+collectionDetailDto.getNamaKaryawan();
				Filedownload.save(IOUtils.toByteArray(tempIs),
						null, tempFileName + ".xls");
				tempIs.close();
				tempOs.close();
				Messagebox.show(
						"Download File XLSX Kunjungan Kolektor Details Berhasil",
						"Message", null, null, null);
			} catch (Exception ex) {
				
			}
		}
	}

	
}
