package vmd;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.record.DSFRecord;
import org.hibernate.cache.spi.access.RegionAccessStrategy;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;

import dto.CabangMstDto;
import dto.MasterRegionalDto;
import dto.UserProfileDto;
import pagevmd.NavigationVmd;
import service.CabangMstSvc;
import service.MasterRegionalSvc;
import service.UserProfileSvc;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class MasterCabangVmd extends NavigationVmd{

	@WireVariable
	private CabangMstSvc cabangMstSvc;
	
	@WireVariable
	private MasterRegionalSvc masterRegionalSvc;
	
	private List<MasterRegionalDto> listRegional = new ArrayList<MasterRegionalDto>();
	private List<CabangMstDto> listCabangMstDto = new ArrayList<CabangMstDto>();
	private MasterRegionalDto masterRegionalDto;
	private CabangMstDto cabangMstDto;
	private int page = 10;
	private String keySearch = "";
	private String keySearchRegional = "";
	private boolean visibleAddEdit = false;
	private boolean fieldKodeCabang = false;
	private boolean visibleRegional = false;
	private boolean visibleKodeCabang = false;
	
	public boolean isVisibleKodeCabang() {
		return visibleKodeCabang;
	}

	public void setVisibleKodeCabang(boolean visibleKodeCabang) {
		this.visibleKodeCabang = visibleKodeCabang;
	}

	public String getKeySearchRegional() {
		return keySearchRegional;
	}

	public void setKeySearchRegional(String keySearchRegional) {
		this.keySearchRegional = keySearchRegional;
	}

	public List<MasterRegionalDto> getListRegional() {
		return listRegional;
	}

	public void setListRegional(List<MasterRegionalDto> listRegional) {
		this.listRegional = listRegional;
	}

	public MasterRegionalDto getMasterRegionalDto() {
		return masterRegionalDto;
	}

	public void setMasterRegionalDto(MasterRegionalDto masterRegionalDto) {
		this.masterRegionalDto = masterRegionalDto;
	}

	public boolean isVisibleRegional() {
		return visibleRegional;
	}

	public void setVisibleRegional(boolean visibleRegional) {
		this.visibleRegional = visibleRegional;
	}

	public boolean isVisibleAddEdit() {
		return visibleAddEdit;
	}

	public void setVisibleAddEdit(boolean visibleAddEdit) {
		this.visibleAddEdit = visibleAddEdit;
	}

	public List<CabangMstDto> getListCabangMstDto() {
		return listCabangMstDto;
	}

	public void setListCabangMstDto(List<CabangMstDto> listCabangMstDto) {
		this.listCabangMstDto = listCabangMstDto;
	}

	public CabangMstDto getCabangMstDto() {
		return cabangMstDto;
	}

	public void setCabangMstDto(CabangMstDto cabangMstDto) {
		this.cabangMstDto = cabangMstDto;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getKeySearch() {
		return keySearch;
	}

	public void setKeySearch(String keySearch) {
		this.keySearch = keySearch;
	}
	
	public boolean isFieldKodeCabang() {
		return fieldKodeCabang;
	}

	public void setFieldKodeCabang(boolean fieldKodeCabang) {
		this.fieldKodeCabang = fieldKodeCabang;
	}

	@Init
	public void load() {
		Sessions.getCurrent().removeAttribute("sessionMobileTracking");
		listCabangMstDto = cabangMstSvc.findCabangAllBySearch(keySearch);
	}
	
	@Command("page")
	@NotifyChange("page")
	public void page() {
		setPage(getPage());
	}
	
	@Command("add")
	@NotifyChange({"visibleAddEdit","cabangMstDto","visibleKodeCabang"})
	public void add() {
		cabangMstDto = new CabangMstDto();
		setVisibleKodeCabang(false);
		setVisibleAddEdit(true);
	}
	
	@Command("search")
	@NotifyChange({ "listCabangMstDto" })
	public void search() {
		listCabangMstDto = cabangMstSvc.findCabangAllBySearch(keySearch);
	}
	
	@Command("close")
	@NotifyChange({ "visibleAddEdit","cabangMstDto","listCabangMstDto"})
	public void close() {
		cabangMstDto = new CabangMstDto();
		load();
		setVisibleAddEdit(false);
	}
	
	@Command("save")
	@NotifyChange({ "visibleAddEdit","visibleKodeCabang","cabangMstDto","listCabangMstDto" })
	public void save() throws NoSuchAlgorithmException{
		CabangMstDto cabang2 = new CabangMstDto();
		cabang2 = cabangMstSvc.findOneCabang(cabangMstDto.getKodeCabang());
		if (cabang2.getKodeCabang() == null) {
			setVisibleKodeCabang(false);
			if (cabangMstDto.getKodeCabang() == null ) {
				Messagebox.show("kode cabang harus terisi", "MESSAGE", null, null, null);
			} else if (cabangMstDto.getNamaCabang() == null ) {
				Messagebox.show("Nama cabang harus terisi", "MESSAGE", null, null, null);
			} else if (masterRegionalDto.getKodeRegion() == null ) {
				Messagebox.show("Regional harus terisi", "MESSAGE", null, null, null);
			} else if (cabangMstDto.getAlamat() == null ) {
				Messagebox.show("Alamat harus terisi", "MESSAGE", null, null, null);
			} else{
				cabangMstDto.setKodeRegional(masterRegionalDto.getKodeRegion());
				System.out.println("ket aktif : "+cabangMstDto.getKeteranganAktif());
				UserProfileDto userProfileDto = (UserProfileDto) Sessions.getCurrent().getAttribute("user");
				cabangMstDto.setModifyUser(userProfileDto.getNamaKaryawan());
				if(cabangMstDto.getKeteranganAktif().equals("Enabled")||cabangMstDto.getKeteranganAktif().equals("1")){
					cabangMstDto.setAktifStatus(1);
				}else{
					cabangMstDto.setAktifStatus(0);
				}
				cabangMstSvc.saveCabang(cabangMstDto.getKodeCabang(), cabangMstDto.getKodeRegional(), 
						cabangMstDto.getNamaCabang(), cabangMstDto.getAlamat(), 
						cabangMstDto.getAktifStatus(), cabangMstDto.getModifyUser());
				load();
				Messagebox.show("Data cabang berhasil disimpan", "MESSAGE", null, null, null);
				setVisibleAddEdit(false);
			}
		} else {
			if (cabangMstDto.getKodeCabang() == null || cabangMstDto.getKodeCabang().equalsIgnoreCase("") ) {
				Messagebox.show("Kode cabang harus terisi", "MESSAGE", null, null, null);			
			} else if (cabangMstDto.getNamaCabang() == null || cabangMstDto.getNamaCabang().equalsIgnoreCase("")) {
				Messagebox.show("Nama cabang harus terisi", "MESSAGE", null, null, null);				
			} else if (cabangMstDto.getKodeRegional() == null || cabangMstDto.getKodeRegional().equalsIgnoreCase("")) {
				Messagebox.show("Regional harus terisi", "MESSAGE", null, null, null);
			} else if (cabangMstDto.getAlamat() == null || cabangMstDto.getAlamat().equalsIgnoreCase("")) {
				Messagebox.show("Alamat harus terisi", "MESSAGE", null, null, null);
			} else {
				UserProfileDto userProfileDto = (UserProfileDto) Sessions
						.getCurrent().getAttribute("user");
				cabangMstDto.setModifyUser(userProfileDto.getNamaKaryawan());
				if (cabangMstDto.getKeteranganAktif().equals("Enabled")
						|| cabangMstDto.getKeteranganAktif().equals("1")) {
					cabangMstDto.setAktifStatus(1);
				} else {
					cabangMstDto.setAktifStatus(0);
				}
				if (cabangMstDto.getKodeRegional() == null) {
					Messagebox.show("Regional harus terisi", "MESSAGE", null, null, null);
				} else {
					System.out.println("DATA MASUK UPDATE");
					setVisibleKodeCabang(true);
					cabangMstSvc.updateCabang(cabangMstDto.getKodeRegional(),
							cabangMstDto.getNamaCabang(),
							cabangMstDto.getAlamat(),
							cabangMstDto.getAktifStatus(),
							cabangMstDto.getModifyUser(),
							cabangMstDto.getKodeCabang());
					load();
					setVisibleAddEdit(false);
					Messagebox.show("Data cabang berhasil diubah", "MESSAGE", null, null, null);
				}
			}
			listCabangMstDto = cabangMstSvc.findCabangAllBySearch("");
			cabangMstDto = new CabangMstDto();
		}
		
	}
	
	@Command("showRegional")
	@NotifyChange({ "visibleRegional","listRegional"})
	public void showRegional() {
		listRegional = masterRegionalSvc.searchAllRegional(keySearchRegional);
		setVisibleRegional(true);
	}
	
	@Command("chooseRegional")
	@NotifyChange({ "visibleRegional", "cabangMstDto"})
	public void chooseRegional() {
		cabangMstDto.setKodeRegional(masterRegionalDto.getKodeRegion());
		cabangMstDto.setNamaRegion(masterRegionalDto.getNamaRegion());
		setVisibleRegional(false);
	}
	
	@Command("closeRegional")
	@NotifyChange({ "visibleRegional" })
	public void closeRegional() {
		setVisibleRegional(false);
	}
	
	@Command("edit")
	@NotifyChange({"visibleKodeCabang","visibleAddEdit","masterRegionalDto"})
	public void edit() {
		if(cabangMstDto==null){
			Messagebox.show("Choose your data first !","Message",null,null,null);
		} else{
			setVisibleKodeCabang(true);
			setVisibleAddEdit(true);
		}
	}
	@Command("delete")
	@NotifyChange({  "cabangMstDto", "listCabangMstDto" })
	public void delete() {
		if (cabangMstDto.getKodeCabang()== null) {
			Messagebox.show("Choose your data first !","Message",null,null,null);
		} else {
			Messagebox.show("Are you sure to delete ?", "perhatian",
					new Button[] { Button.YES, Button.NO },
					Messagebox.QUESTION, Button.NO,
					new EventListener<Messagebox.ClickEvent>() {
						@Override
						public void onEvent(ClickEvent event) throws Exception {
							if (Messagebox.ON_YES.equals(event.getName())) {
								/*MasterRegionalDto dsfMstUser2Dto = (DsfMstUser2Dto) Sessions.getCurrent().getAttribute("user");
								dsfMstRegionalDto.setModifiedBy(dsfMstUser2Dto.getUserID());*/
								UserProfileDto userProfileDto = (UserProfileDto) Sessions.getCurrent().getAttribute("user");
								cabangMstDto.setModifyUser(userProfileDto.getNamaKaryawan());
								if(cabangMstDto.getKeteranganAktif().equals("1")){
									cabangMstDto.setAktifStatus(1);
								}else{
									cabangMstDto.setAktifStatus(0);
								}
								cabangMstSvc.deleteCabang(cabangMstDto.getModifyUser(), cabangMstDto.getKodeCabang());
								BindUtils.postNotifyChange(null, null,
										MasterCabangVmd.this,
										"listCabangMstDto");
								BindUtils.postNotifyChange(null, null,
										MasterCabangVmd.this,
										"cabangMstDto");
								listCabangMstDto = cabangMstSvc.findCabangAllBySearch("");
								Messagebox.show("Data berhasil di disable", "MESSAGE", null, null, null);
								cabangMstDto= new CabangMstDto();
							}
						}
					});
		}
	}

}
