package vmd;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import mobile.Notification;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

import dto.AksesRegionCabangDto;
import dto.CabangMstDto;
import dto.MasterRegionalDto;
import dto.UserProfileDto;
import pagevmd.NavigationVmd;
import service.AksesRegionCabangSvc;
import service.CabangMstSvc;
import service.MasterRegionalSvc;
import service.UserProfileSvc;


@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class NotificationVmd  extends NavigationVmd{

	@WireVariable
	private UserProfileSvc userProfileSvc;
	
	@WireVariable
	private MasterRegionalSvc masterRegionalSvc;
	
	@WireVariable
	private CabangMstSvc cabangMstSvc;
	
	@WireVariable
	private AksesRegionCabangSvc aksesRegionCabangSvc;
	
	
	
	private List<UserProfileDto> listTarget = new ArrayList<UserProfileDto>();
	private List<UserProfileDto> listUserProfileDto = new ArrayList<UserProfileDto>();
	private List<UserProfileDto> listuserProfileDtoChoose = new ArrayList<UserProfileDto>();
	private List<MasterRegionalDto> listRegionalDto = new ArrayList<MasterRegionalDto>();
	private List<MasterRegionalDto> listRegionalDtoChoose = new ArrayList<MasterRegionalDto>();
	private List<CabangMstDto> listCabangDto = new ArrayList<CabangMstDto>();
	private List<CabangMstDto> listCabangDtoChoose = new ArrayList<CabangMstDto>();
	private String stringArrayUserChoose = "";
	private String stringArrayRegionalChoose = "";
	private String stringArrayCabangChoose = "";
	private String stringArrayTarget = "";
	private String title;
	private String body;
	private String targetOption = "REGIONAL";
	private boolean stsShowRegional = false;
	private boolean stsShowCabang = false;
	private boolean stsShowUser = false;
	private String keySearchRegional = "";
	private String keySearchCabang = "";
	private String keySearchUser = "";
	private int pageRegional = 10;
	private int pageCabang = 10;
	private int pageUser = 10;
	
	
	
	public List<UserProfileDto> getListTarget() {
		return listTarget;
	}
	public void setListTarget(List<UserProfileDto> listTarget) {
		this.listTarget = listTarget;
	}
	public int getPageRegional() {
		return pageRegional;
	}
	public void setPageRegional(int pageRegional) {
		this.pageRegional = pageRegional;
	}
	public int getPageCabang() {
		return pageCabang;
	}
	public void setPageCabang(int pageCabang) {
		this.pageCabang = pageCabang;
	}
	public int getPageUser() {
		return pageUser;
	}
	public void setPageUser(int pageUser) {
		this.pageUser = pageUser;
	}
	public String getStringArrayRegionalChoose() {
		return stringArrayRegionalChoose;
	}
	public void setStringArrayRegionalChoose(String stringArrayRegionalChoose) {
		this.stringArrayRegionalChoose = stringArrayRegionalChoose;
	}
	public String getStringArrayCabangChoose() {
		return stringArrayCabangChoose;
	}
	public void setStringArrayCabangChoose(String stringArrayCabangChoose) {
		this.stringArrayCabangChoose = stringArrayCabangChoose;
	}
	public String getStringArrayTarget() {
		return stringArrayTarget;
	}
	public void setStringArrayTarget(String stringArrayTarget) {
		this.stringArrayTarget = stringArrayTarget;
	}
	public UserProfileSvc getUserProfileSvc() {
		return userProfileSvc;
	}
	public void setUserProfileSvc(UserProfileSvc userProfileSvc) {
		this.userProfileSvc = userProfileSvc;
	}
	public MasterRegionalSvc getMasterRegionalSvc() {
		return masterRegionalSvc;
	}
	public void setMasterRegionalSvc(MasterRegionalSvc masterRegionalSvc) {
		this.masterRegionalSvc = masterRegionalSvc;
	}
	public List<MasterRegionalDto> getListRegionalDto() {
		return listRegionalDto;
	}
	public void setListRegionalDto(List<MasterRegionalDto> listRegionalDto) {
		this.listRegionalDto = listRegionalDto;
	}
	public List<MasterRegionalDto> getListRegionalDtoChoose() {
		return listRegionalDtoChoose;
	}
	public void setListRegionalDtoChoose(
			List<MasterRegionalDto> listRegionalDtoChoose) {
		this.listRegionalDtoChoose = listRegionalDtoChoose;
	}
	public List<CabangMstDto> getListCabangDto() {
		return listCabangDto;
	}
	public void setListCabangDto(List<CabangMstDto> listCabangDto) {
		this.listCabangDto = listCabangDto;
	}
	public List<CabangMstDto> getListCabangDtoChoose() {
		return listCabangDtoChoose;
	}
	public void setListCabangDtoChoose(List<CabangMstDto> listCabangDtoChoose) {
		this.listCabangDtoChoose = listCabangDtoChoose;
	}
	public String getKeySearchRegional() {
		return keySearchRegional;
	}
	public void setKeySearchRegional(String keySearchRegional) {
		this.keySearchRegional = keySearchRegional;
	}
	public String getKeySearchCabang() {
		return keySearchCabang;
	}
	public void setKeySearchCabang(String keySearchCabang) {
		this.keySearchCabang = keySearchCabang;
	}
	public String getKeySearchUser() {
		return keySearchUser;
	}
	public void setKeySearchUser(String keySearchUser) {
		this.keySearchUser = keySearchUser;
	}
	public boolean isStsShowRegional() {
		return stsShowRegional;
	}
	public void setStsShowRegional(boolean stsShowRegional) {
		this.stsShowRegional = stsShowRegional;
	}
	public boolean isStsShowCabang() {
		return stsShowCabang;
	}
	public void setStsShowCabang(boolean stsShowCabang) {
		this.stsShowCabang = stsShowCabang;
	}
	public boolean isStsShowUser() {
		return stsShowUser;
	}
	public void setStsShowUser(boolean stsShowUser) {
		this.stsShowUser = stsShowUser;
	}
	public String getTargetOption() {
		return targetOption;
	}
	public void setTargetOption(String targetOption) {
		this.targetOption = targetOption;
	}
	public String getStringArrayUserChoose() {
		return stringArrayUserChoose;
	}
	public void setStringArrayUserChoose(String stringArrayUserChoose) {
		this.stringArrayUserChoose = stringArrayUserChoose;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public List<UserProfileDto> getListUserProfileDto() {
		return listUserProfileDto;
	}
	public void setListUserProfileDto(List<UserProfileDto> listUserProfileDto) {
		this.listUserProfileDto = listUserProfileDto;
	}
	public List<UserProfileDto> getListuserProfileDtoChoose() {
		return listuserProfileDtoChoose;
	}
	public void setListuserProfileDtoChoose(
			List<UserProfileDto> listuserProfileDtoChoose) {
		this.listuserProfileDtoChoose = listuserProfileDtoChoose;
	}
	
	@Command("showTarget")
	@NotifyChange({"listRegionalDto", "listUserProfileDto","listCabangDto","title","stringArrayUserChoose","body", "listuserProfileDtoChoose","stsShowRegional","stsShowCabang","stsShowUser"})
	public void showTarget() {
		if (targetOption.equalsIgnoreCase("REGIONAL")) {
			listRegionalDto = masterRegionalSvc.searchAllRegional(keySearchRegional);
			if (listRegionalDtoChoose.size() == 0) {
				MasterRegionalDto mr = new MasterRegionalDto();
				mr.setKodeRegion("dummy");
				mr.setNamaRegion("");
				listRegionalDtoChoose.add(mr);
			}
			if (listRegionalDtoChoose.size() != 0 ) {
				List<MasterRegionalDto> listTemp = new ArrayList<MasterRegionalDto>();
				for (int i = 0; i < listRegionalDto.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listRegionalDtoChoose.size(); j++) {
						if (listRegionalDto.get(i).getKodeRegion().equals(listRegionalDtoChoose.get(j).getKodeRegion())) {
							sama = true;
						}
					}
					if (sama == true) {
						MasterRegionalDto temppDto = new MasterRegionalDto();
						temppDto.setKodeRegion(listRegionalDto.get(i).getKodeRegion());
						temppDto.setNamaRegion(listRegionalDto.get(i).getNamaRegion());
						temppDto.setChoose(true);
						temppDto.setItemList(true);
						listTemp.add(temppDto);
					}else {
						MasterRegionalDto temppDto = new MasterRegionalDto();
						temppDto.setKodeRegion(listRegionalDto.get(i).getKodeRegion());
						temppDto.setNamaRegion(listRegionalDto.get(i).getNamaRegion());
						temppDto.setChoose(false);
						temppDto.setItemList(true);
						listTemp.add(temppDto);
					}
				}
				listRegionalDto = listTemp;
				MasterRegionalDto mre = new MasterRegionalDto();
				mre.setKodeRegion("");
				mre.setNamaRegion("");
				mre.setItemList(false);
				listRegionalDto.add(mre);
			}
			setStsShowRegional(true);
		}else if (targetOption.equalsIgnoreCase("CABANG")) {
			if (listCabangDtoChoose.size() == 0) {
				CabangMstDto cb = new CabangMstDto();
				cb.setKodeCabang("dummy");
				cb.setNamaCabang("");
				listCabangDtoChoose.add(cb);
			}
				List<CabangMstDto> tempList = new ArrayList<CabangMstDto>();
				listCabangDto = new ArrayList<CabangMstDto>();
				listCabangDto = cabangMstSvc.findCabangAllActiveBySearch(keySearchCabang);
				if (listCabangDtoChoose.size() != 0) {
					for (int i = 0; i < listCabangDto.size(); i++) {	
							CabangMstDto cba = new CabangMstDto();
							boolean cheked = false;
							for (int j = 0; j < listCabangDtoChoose.size(); j++) {
								if (listCabangDto.get(i).getKodeCabang().equals(listCabangDtoChoose.get(j).getKodeCabang())) {
									cheked = true;
								}
							}
							if (cheked == true) {
								cba.setKodeCabang(listCabangDto.get(i).getKodeCabang());
								cba.setNamaCabang(listCabangDto.get(i).getNamaCabang());
								cba.setItemList(true);
								cba.setChoose(true);
							}else {
								cba.setKodeCabang(listCabangDto.get(i).getKodeCabang());
								cba.setNamaCabang(listCabangDto.get(i).getNamaCabang());
								cba.setItemList(true);
								cba.setChoose(false);
							}
							tempList.add(cba);
					}
					listCabangDto = new ArrayList<CabangMstDto>();
					listCabangDto = tempList;
					CabangMstDto cb = new CabangMstDto();
					cb.setKodeCabang("");
					cb.setNamaCabang("");
					cb.setItemList(false);
					listCabangDto.add(cb);
				}
				if (listCabangDto.size() != 0) {
					setStsShowCabang(true);
				}else {
					Messagebox.show("Tidak ada data cabang yang dapat ditampilkan !", "MESSAGE", null, null, null);
				}
		}else if (targetOption.equalsIgnoreCase("USER")) {
			listUserProfileDto = new ArrayList<UserProfileDto>();
				if (listuserProfileDtoChoose.size() == 0) {
					UserProfileDto up = new UserProfileDto();
					up.setUserId("dummy");
					up.setNamaKaryawan("");
					listuserProfileDtoChoose.add(up);
				}
				listUserProfileDto = userProfileSvc.findUserAllBySearch(keySearchUser);
				
				if (listuserProfileDtoChoose.size() != 0 ) {
					List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
					for (int i = 0; i < listUserProfileDto.size(); i++) {
						boolean sama = false;
						for (int j = 0; j < listuserProfileDtoChoose.size(); j++) {
							if (listUserProfileDto.get(i).getUserId().equals(listuserProfileDtoChoose.get(j).getUserId())) {
								sama = true;
							}
						}
						if (sama == true) {
							UserProfileDto use = new UserProfileDto();
							use.setUserId(listUserProfileDto.get(i).getUserId());
							use.setNamaKaryawan(listUserProfileDto.get(i).getNamaKaryawan());
							use.setChoose(true);
							use.setItemList(true);
							listTemp.add(use);
						}else {
							UserProfileDto use = new UserProfileDto();
							use.setUserId(listUserProfileDto.get(i).getUserId());
							use.setNamaKaryawan(listUserProfileDto.get(i).getNamaKaryawan());
							use.setChoose(false);
							use.setItemList(true);
							listTemp.add(use);
						}
					}
					listUserProfileDto = listTemp;
					UserProfileDto up = new UserProfileDto();
					up.setUserId("");
					up.setNamaKaryawan("");
					up.setItemList(false);
					listUserProfileDto.add(up);
				}
				if (listUserProfileDto.size() != 0) {
					setStsShowUser(true);
				}else {
					Messagebox.show("Tidak ada data kolektor yang dapat ditampilkan !", "MESSAGE", null, null, null);
				}
		}else {
			Messagebox.show("Option tidak di ketahui", "MESSAGE", null, null, null);
		}
		
	}
	
	@Command("searchTarget")
	@NotifyChange({"listRegionalDto", "listUserProfileDto","listCabangDto","title","stringArrayUserChoose","body", "listuserProfileDtoChoose","stsShowRegional","stsShowCabang","stsShowUser"})
	public void searchTarget() {
		if (targetOption.equalsIgnoreCase("REGIONAL")) {
			listRegionalDto = masterRegionalSvc.searchAllRegional(keySearchRegional);
			if (listRegionalDtoChoose.size() == 0) {
				MasterRegionalDto mr = new MasterRegionalDto();
				mr.setKodeRegion("dummy");
				mr.setNamaRegion("");
				listRegionalDtoChoose.add(mr);
			}
			if (listRegionalDtoChoose.size() != 0 ) {
				List<MasterRegionalDto> listTemp = new ArrayList<MasterRegionalDto>();
				for (int i = 0; i < listRegionalDto.size(); i++) {
					boolean sama = false;
					for (int j = 0; j < listRegionalDtoChoose.size(); j++) {
						if (listRegionalDto.get(i).getKodeRegion().equals(listRegionalDtoChoose.get(j).getKodeRegion())) {
							sama = true;
						}
					}
					if (sama == true) {
						MasterRegionalDto temppDto = new MasterRegionalDto();
						temppDto.setKodeRegion(listRegionalDto.get(i).getKodeRegion());
						temppDto.setNamaRegion(listRegionalDto.get(i).getNamaRegion());
						temppDto.setChoose(true);
						temppDto.setItemList(true);
						listTemp.add(temppDto);
					}else {
						MasterRegionalDto temppDto = new MasterRegionalDto();
						temppDto.setKodeRegion(listRegionalDto.get(i).getKodeRegion());
						temppDto.setNamaRegion(listRegionalDto.get(i).getNamaRegion());
						temppDto.setChoose(false);
						temppDto.setItemList(true);
						listTemp.add(temppDto);
					}
				}
				listRegionalDto = listTemp;
				MasterRegionalDto mre = new MasterRegionalDto();
				mre.setKodeRegion("");
				mre.setNamaRegion("");
				mre.setItemList(false);
				listRegionalDto.add(mre);
			}
		}else if (targetOption.equalsIgnoreCase("CABANG")) {
			if (listCabangDtoChoose.size() == 0) {
				CabangMstDto cb = new CabangMstDto();
				cb.setKodeCabang("dummy");
				cb.setNamaCabang("");
				listCabangDtoChoose.add(cb);
			}
				List<CabangMstDto> tempList = new ArrayList<CabangMstDto>();
				listCabangDto = new ArrayList<CabangMstDto>();
				listCabangDto = cabangMstSvc.findCabangAllActiveBySearch(keySearchCabang);
				if (listCabangDtoChoose.size() != 0) {
					for (int i = 0; i < listCabangDto.size(); i++) {	
							CabangMstDto cba = new CabangMstDto();
							boolean cheked = false;
							for (int j = 0; j < listCabangDtoChoose.size(); j++) {
								if (listCabangDto.get(i).getKodeCabang().equals(listCabangDtoChoose.get(j).getKodeCabang())) {
									cheked = true;
								}
							}
							if (cheked == true) {
								cba.setKodeCabang(listCabangDto.get(i).getKodeCabang());
								cba.setNamaCabang(listCabangDto.get(i).getNamaCabang());
								cba.setItemList(true);
								cba.setChoose(true);
							}else {
								cba.setKodeCabang(listCabangDto.get(i).getKodeCabang());
								cba.setNamaCabang(listCabangDto.get(i).getNamaCabang());
								cba.setItemList(true);
								cba.setChoose(false);
							}
							tempList.add(cba);
					}
					listCabangDto = new ArrayList<CabangMstDto>();
					listCabangDto = tempList;
					CabangMstDto cb = new CabangMstDto();
					cb.setKodeCabang("");
					cb.setNamaCabang("");
					cb.setItemList(false);
					listCabangDto.add(cb);
				}
		}else if (targetOption.equalsIgnoreCase("USER")) {
			listUserProfileDto = new ArrayList<UserProfileDto>();
				if (listuserProfileDtoChoose.size() == 0) {
					UserProfileDto up = new UserProfileDto();
					up.setUserId("dummy");
					up.setNamaKaryawan("");
					listuserProfileDtoChoose.add(up);
				}
				listUserProfileDto = userProfileSvc.findUserAllBySearch(keySearchUser);
				
				if (listuserProfileDtoChoose.size() != 0 ) {
					List<UserProfileDto> listTemp = new ArrayList<UserProfileDto>();
					for (int i = 0; i < listUserProfileDto.size(); i++) {
						boolean sama = false;
						for (int j = 0; j < listuserProfileDtoChoose.size(); j++) {
							if (listUserProfileDto.get(i).getUserId().equals(listuserProfileDtoChoose.get(j).getUserId())) {
								sama = true;
							}
						}
						if (sama == true) {
							UserProfileDto use = new UserProfileDto();
							use.setUserId(listUserProfileDto.get(i).getUserId());
							use.setNamaKaryawan(listUserProfileDto.get(i).getNamaKaryawan());
							use.setChoose(true);
							use.setItemList(true);
							listTemp.add(use);
						}else {
							UserProfileDto use = new UserProfileDto();
							use.setUserId(listUserProfileDto.get(i).getUserId());
							use.setNamaKaryawan(listUserProfileDto.get(i).getNamaKaryawan());
							use.setChoose(false);
							use.setItemList(true);
							listTemp.add(use);
						}
					}
					listUserProfileDto = listTemp;
					UserProfileDto up = new UserProfileDto();
					up.setUserId("");
					up.setNamaKaryawan("");
					up.setItemList(false);
					listUserProfileDto.add(up);
				}
		}else {
			Messagebox.show("Option tidak di ketahui", "MESSAGE", null, null, null);
		}
		
	}
	
	
	@Command("chooseTarget")
	@NotifyChange({"listTarget","stsShowCabang", "stsShowRegional", "stsShowUser","stringArrayRegionalChoose", "stringArrayCabangChoose", "stringArrayUserChoose", "stringArrayTarget"})
	public void chooseTarget() {
		listTarget = new ArrayList<UserProfileDto>();
		UserProfileDto upSession = (UserProfileDto) Sessions.getCurrent().getAttribute("user");
		List<AksesRegionCabangDto> lr = aksesRegionCabangSvc.findRegionByUserId(upSession.getUserId(), "");
		List<AksesRegionCabangDto> lc = aksesRegionCabangSvc.fintCabangByUserId(upSession.getUserId(), "");
		if (targetOption.equalsIgnoreCase("REGIONAL")) {
			if (listRegionalDtoChoose.size() != 0) {
				stringArrayRegionalChoose = "";
				for (int i = 0; i < listRegionalDtoChoose.size(); i++) {
					if (i == 0) {
						stringArrayRegionalChoose = stringArrayRegionalChoose+listRegionalDtoChoose.get(i).getNamaRegion();
					}else {
						stringArrayRegionalChoose = stringArrayRegionalChoose+", "+listRegionalDtoChoose.get(i).getNamaRegion();
					}
				}
				
				for (int i = 0; i < listRegionalDtoChoose.size(); i++) {
					for (int j = 0; j < lc.size(); j++) {
						List<UserProfileDto> lpTemp = userProfileSvc.findUserAllByKodeRegionalKodeCabangAndSearch(listRegionalDtoChoose.get(i).getKodeRegion(), lc.get(j).getKodeCabang(), "");
						if (lpTemp.size() != 0) {
							for (int k = 0; k < lpTemp.size(); k++) {
								UserProfileDto up = new UserProfileDto();
								up.setUserId(lpTemp.get(k).getUserId());
								listTarget.add(up);	
							}
						}
						
					}
				}
			}
			stringArrayTarget = stringArrayRegionalChoose;
			setStsShowRegional(false);
			
		}else if (targetOption.equalsIgnoreCase("CABANG")) {
			if (listCabangDtoChoose.size() != 0) {
				stringArrayCabangChoose = "";
				for (int i = 0; i < listCabangDtoChoose.size(); i++) {
					if (i == 0) {
						stringArrayCabangChoose = stringArrayCabangChoose+listCabangDtoChoose.get(i).getNamaCabang();
					}else {
						stringArrayCabangChoose = stringArrayCabangChoose+", "+listCabangDtoChoose.get(i).getNamaCabang();
					}
				}
				for (int i = 0; i < listCabangDtoChoose.size(); i++) {
					List<UserProfileDto> lpTemp = userProfileSvc.findUserAllByKodeCabangAndSearch(listCabangDtoChoose.get(i).getKodeCabang(), "");
					if (lpTemp.size() != 0) {
						for (int k = 0; k < lpTemp.size(); k++) {
							UserProfileDto up = new UserProfileDto();
							up.setUserId(lpTemp.get(k).getUserId());
							listTarget.add(up);	
						}
					}
				}
			}
			stringArrayTarget = stringArrayCabangChoose;
			setStsShowCabang(false);
		}else if (targetOption.equalsIgnoreCase("USER")) {
			if (listuserProfileDtoChoose.size() != 0) {
				stringArrayUserChoose = "";
				for (int i = 0; i < listuserProfileDtoChoose.size(); i++) {
					if (i == 0  ) {
						stringArrayUserChoose = stringArrayUserChoose+listuserProfileDtoChoose.get(i).getNamaKaryawan();
					}else {
						stringArrayUserChoose = stringArrayUserChoose+", "+listuserProfileDtoChoose.get(i).getNamaKaryawan();
					}
				}
				for (int i = 0; i < listuserProfileDtoChoose.size(); i++) {
					UserProfileDto up = new UserProfileDto();
					up.setUserId(listuserProfileDtoChoose.get(i).getUserId());
					listTarget.add(up);
				}
			}
			stringArrayTarget = stringArrayUserChoose;
			setStsShowUser(false);
		}else {
			Messagebox.show("Option tidak di ketahui", "MESSAGE", null, null, null);
		}
	}
	
	@Command("send")
	@NotifyChange({"listCabangDtoChoose","listuserProfileDtoChoose","listRegionalDtoChoose","title","stringArrayUserChoose","body", "listuserProfileDtoChoose","listTarget","stringArrayTarget"})
	public void send() {
		if (listTarget.size() != 0) {
			for (int i = 0; i < listTarget.size(); i++) {
				System.out.println("Ni USER id yg di kirim = "+listTarget.get(i).getUserId());
				String target = "";
				target = listTarget.get(i).getUserId();
				Notification.sendNotif(target, title, body);
			}
			Messagebox.show("Notification telah terkirim ! ", "MESSAGE", null, null, null);
			title = "";
			body = "";
			stringArrayTarget = "";
			listTarget = new ArrayList<UserProfileDto>();
			listCabangDtoChoose = new ArrayList<CabangMstDto>();
			listuserProfileDtoChoose = new ArrayList<UserProfileDto>();
			listRegionalDtoChoose = new ArrayList<MasterRegionalDto>();
		}else {
			Messagebox.show("Pilih target terlebih dahulu !", "MESSAGE", null, null, null);
		}
	}
	
	@Command("closeRegional")
	@NotifyChange({"stsShowRegional"})
	public void closeRegional() {
		setStsShowRegional(false);
	}
	
	@Command("closeCabang")
	@NotifyChange({"stsShowCabang"})
	public void closeCabang() {
		setStsShowCabang(false);
	}
	
	@Command("closeUser")
	@NotifyChange({"stsShowUser"})
	public void closeUser() {
		setStsShowUser(false);
	}
	
	@Command("pageRegional")
	@NotifyChange("pageRegional")
	public void pageRegional() {
		setPageRegional(getPageRegional());
	}
	@Command("pageCabang")
	@NotifyChange("pageCabang")
	public void pageCabang() {
		setPageCabang(getPageCabang());
	}
	@Command("pageUser")
	@NotifyChange("pageUser")
	public void pageUser() {
		setPageUser(getPageUser());
	}
	@Command("targetOption")
	@NotifyChange("targetOption")
	public void targetOption() {
		targetOption = targetOption;
	}
	
	
}
