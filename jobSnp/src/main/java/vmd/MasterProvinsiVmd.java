package vmd;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;

import dto.CabangMstDto;
import dto.MasterProvinsiDto;
import dto.UserProfileDto;
import pagevmd.NavigationVmd;
import service.MasterProvinsiSvc;
import service.MasterRegionalSvc;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class MasterProvinsiVmd extends NavigationVmd {
	@WireVariable
	private MasterProvinsiSvc masterProvinsiSvc;
	
	private MasterProvinsiDto masterProvinsiDto= new MasterProvinsiDto();
	private List<MasterProvinsiDto> listProvinsiDto = new ArrayList<MasterProvinsiDto>();
	
	private String search="";
	private boolean addEditProvinsi=false;
	private int pages = 10;
	private boolean visibleKodeProvinsi=false;
	
	public MasterProvinsiDto getMasterProvinsiDto() {
		return masterProvinsiDto;
	}
	public void setMasterProvinsiDto(MasterProvinsiDto masterProvinsiDto) {
		this.masterProvinsiDto = masterProvinsiDto;
	}
	public List<MasterProvinsiDto> getListProvinsiDto() {
		return listProvinsiDto;
	}
	public void setListProvinsiDto(List<MasterProvinsiDto> listProvinsiDto) {
		this.listProvinsiDto = listProvinsiDto;
	}
	public String getSearch() {
		return search;
	}
	public void setSearch(String search) {
		this.search = search;
	}
	public boolean isAddEditProvinsi() {
		return addEditProvinsi;
	}
	public void setAddEditProvinsi(boolean addEditProvinsi) {
		this.addEditProvinsi = addEditProvinsi;
	}
	public int getPages() {
		return pages;
	}
	public void setPages(int pages) {
		this.pages = pages;
	}
	public boolean isVisibleKodeProvinsi() {
		return visibleKodeProvinsi;
	}
	public void setVisibleKodeProvinsi(boolean visibleKodeProvinsi) {
		this.visibleKodeProvinsi = visibleKodeProvinsi;
	}
	
	@Init
	public void load(){
		Sessions.getCurrent().removeAttribute("sessionMobileTracking");
		listProvinsiDto = masterProvinsiSvc.findAllProvinsi();
	}
	
	@Command("page")
	@NotifyChange("page")
	public void page() {
		setPages(getPages());
	}
	
	@Command("add")
	@NotifyChange({"addEditProvinsi","masterProvinsiDto","visibleKodeProvinsi"})
	public void add() {
		masterProvinsiDto = new MasterProvinsiDto();
		setVisibleKodeProvinsi(false);
		setAddEditProvinsi(true);
	}
	
	@Command("search")
	@NotifyChange({ "listProvinsiDto" })
	public void search() {
		listProvinsiDto = masterProvinsiSvc.searchAllProvinsi(search);
	}
	
	@Command("close")
	@NotifyChange({ "addEditProvinsi","masterProvinsiDto","listProvinsiDto"})
	public void close() {
		masterProvinsiDto = new MasterProvinsiDto();
		load();
		setAddEditProvinsi(false);
	}
	
	@Command("edit")
	@NotifyChange({"addEditProvinsi","masterProvinsiDto","visibleKodeProvinsi"})
	public void edit() {
		if(masterProvinsiDto==null){
			Messagebox.show("Choose your data first !","Message",null,null,null);
		} else{
			setVisibleKodeProvinsi(true);
			setAddEditProvinsi(true);
		}
	}
	
	@Command("save")
	@NotifyChange({ "visibleAddEdit","listProvinsiDto","addEditProvinsi","visibleKodeCabang","cabangMstDto","listCabangMstDto" })
	public void save() throws NoSuchAlgorithmException{
		MasterProvinsiDto prov = new MasterProvinsiDto();
		prov = masterProvinsiSvc.findOneProvinsi(masterProvinsiDto.getKodeProvinsi());
		if (prov.getKodeProvinsi() == null || prov.getKodeProvinsi().equalsIgnoreCase("")) {
			setVisibleKodeProvinsi(false);
			if (masterProvinsiDto.getKodeProvinsi() == null || masterProvinsiDto.getKodeProvinsi().equalsIgnoreCase("") ) {
				Messagebox.show("Kode provinsi Tidak Boleh Kosong","Message",null,null,null);
			} else if (masterProvinsiDto.getNamaProvinsi() == null || masterProvinsiDto.getNamaProvinsi().equalsIgnoreCase("") ) {
				Messagebox.show("Nama provinsi Tidak Boleh Kosong","Message",null,null,null);
			} else if (masterProvinsiDto.getAktifStatus() == null || masterProvinsiDto.getAktifStatus().equalsIgnoreCase("")) {
				Messagebox.show("Status provinsi Tidak Boleh Kosong","Message",null,null,null);
			}  else{
				masterProvinsiDto.setKodeProvinsi(masterProvinsiDto.getKodeProvinsi());
				System.out.println("ket aktif : "+masterProvinsiDto.getAktifStatus());
				UserProfileDto userProfileDto = (UserProfileDto) Sessions.getCurrent().getAttribute("user");
				masterProvinsiDto.setCreateUser(userProfileDto.getNamaKaryawan());
				int intStatus = 1;
				System.out.println("status : "+masterProvinsiDto.getAktifStatus());
				if(masterProvinsiDto.getAktifStatus().equalsIgnoreCase("Enable")||masterProvinsiDto.getAktifStatus().equals("1")){
					intStatus = 1;
				}else{
					intStatus = 0;
				}
				masterProvinsiSvc.InsertProvinsi(masterProvinsiDto.getKodeProvinsi(), masterProvinsiDto.getNamaProvinsi(), intStatus, masterProvinsiDto.getCreateUser());
				masterProvinsiDto = new MasterProvinsiDto();
				load();
				Messagebox.show("Data berhasil dimasukan","Message",null,null,null);
				setAddEditProvinsi(false);
			}
		} else {
			if (masterProvinsiDto.getKodeProvinsi() == null || masterProvinsiDto.getKodeProvinsi().equalsIgnoreCase("") ) {
				Messagebox.show("Kode provinsi Tidak Boleh Kosong","Message",null,null,null);
			} else if (masterProvinsiDto.getNamaProvinsi() == null || masterProvinsiDto.getNamaProvinsi().equalsIgnoreCase("") ) {
				Messagebox.show("Nama provinsi Tidak Boleh Kosong","Message",null,null,null);
			} else if (masterProvinsiDto.getAktifStatus() == null || masterProvinsiDto.getAktifStatus().equalsIgnoreCase("")) {
				Messagebox.show("Status provinsi Tidak Boleh Kosong","Message",null,null,null);
			} else {
				UserProfileDto userProfileDto = (UserProfileDto) Sessions.getCurrent().getAttribute("user");
				masterProvinsiDto.setModifyUser(userProfileDto.getNamaKaryawan());
				int intStatus = 1;
				System.out.println("status : "+masterProvinsiDto.getAktifStatus());
				if(masterProvinsiDto.getAktifStatus().equalsIgnoreCase("Enable")||
						masterProvinsiDto.getAktifStatus().equals("1")||masterProvinsiDto.getAktifStatus().equals(1)){
					intStatus = 1;
				}else{
					intStatus = 0;
				}
				setVisibleKodeProvinsi(true);
				masterProvinsiSvc.UpdateProvinsi(masterProvinsiDto.getNamaProvinsi(), masterProvinsiDto.getModifyUser(), intStatus, masterProvinsiDto.getKodeProvinsi());
				masterProvinsiDto = new MasterProvinsiDto();
				load();
				setAddEditProvinsi(false);
				Messagebox.show("Data berhasil diubah","Message",null,null,null);
			}
		}
		listProvinsiDto = masterProvinsiSvc.findAllProvinsi();
		
		
	}
	@Command("delete")
	@NotifyChange({  "cabangMstDto","listProvinsiDto", "listCabangMstDto" })
	public void delete() {
		if (masterProvinsiDto.getKodeProvinsi()== null) {
			Messagebox.show("Choose your data first !","Message",null,null,null);
		} else {
			Messagebox.show("Are you sure to delete ?", "perhatian",
					new Button[] { Button.YES, Button.NO },
					Messagebox.QUESTION, Button.NO,
					new EventListener<Messagebox.ClickEvent>() {
						@Override
						public void onEvent(ClickEvent event) throws Exception {
							if (Messagebox.ON_YES.equals(event.getName())) {
								/*MasterRegionalDto dsfMstUser2Dto = (DsfMstUser2Dto) Sessions.getCurrent().getAttribute("user");
								dsfMstRegionalDto.setModifiedBy(dsfMstUser2Dto.getUserID());*/
								UserProfileDto userProfileDto = (UserProfileDto) Sessions.getCurrent().getAttribute("user");
								masterProvinsiDto.setModifyUser(userProfileDto.getNamaKaryawan());
								masterProvinsiSvc.DeleteProvinsi(masterProvinsiDto.getModifyUser(), masterProvinsiDto.getKodeProvinsi());
								BindUtils.postNotifyChange(null, null,
										MasterProvinsiVmd.this,
										"listProvinsiDto");
								listProvinsiDto = masterProvinsiSvc.findAllProvinsi();
								Messagebox.show("Data berhasil di-nonAktifkan","Message",null,null,null);
								masterProvinsiDto = new MasterProvinsiDto();
							}
						}
					});
		}
	}

}
