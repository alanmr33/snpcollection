package vmd;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.record.DSFRecord;
import org.hibernate.cache.spi.access.RegionAccessStrategy;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;

import dto.CabangMstDto;
import dto.EventsDto;
import dto.FormsDto;
import dto.MasterRegionalDto;
import dto.UserProfileDto;
import pagevmd.NavigationVmd;
import service.CabangMstSvc;
import service.EventsSvc;
import service.FormsSvc;
import service.MasterRegionalSvc;
import service.UserProfileSvc;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class EventsVmd extends NavigationVmd{

	@WireVariable
	private EventsSvc eventsSvc;	
	
	private List<EventsDto> listEventDto = new ArrayList<EventsDto>();
	private EventsDto eventsDto;
	private int page = 10;
	private String keySearch = "";
	private boolean visibleAddEdit = false;
	private boolean fieldEventId = false;

	public List<EventsDto> getListEventDto() {
		return listEventDto;
	}

	public void setListEventDto(List<EventsDto> listEventDto) {
		this.listEventDto = listEventDto;
	}

	public EventsDto getEventsDto() {
		return eventsDto;
	}

	public void setEventsDto(EventsDto eventsDto) {
		this.eventsDto = eventsDto;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getKeySearch() {
		return keySearch;
	}

	public void setKeySearch(String keySearch) {
		this.keySearch = keySearch;
	}

	public boolean isVisibleAddEdit() {
		return visibleAddEdit;
	}

	public void setVisibleAddEdit(boolean visibleAddEdit) {
		this.visibleAddEdit = visibleAddEdit;
	}

	public boolean isFieldEventId() {
		return fieldEventId;
	}

	public void setFieldEventId(boolean fieldEventId) {
		this.fieldEventId = fieldEventId;
	}

	@Init
	public void load() {
		keySearch = "";
		Sessions.getCurrent().removeAttribute("sessionMobileTracking");
		listEventDto = eventsSvc.findEventsWithSearch(keySearch);
	}
	
	@Command("page")
	@NotifyChange("page")
	public void page() {
		setPage(getPage());
	}
	
	@Command("add")
	@NotifyChange({"visibleAddEdit","fieldEventId", "eventsDto"})
	public void add() {
		eventsDto = new EventsDto();
		setFieldEventId(false);
		setVisibleAddEdit(true);
	}
	
	@Command("search")
	@NotifyChange({ "listEventDto" })
	public void search() {
		listEventDto = eventsSvc.findEventsWithSearch(keySearch);
	}
	
	@Command("close")
	@NotifyChange({ "visibleAddEdit","eventsDto" })
	public void close() {
		eventsDto = new EventsDto();
		setVisibleAddEdit(false);
	}
	
	@Command("save")
	@NotifyChange({"eventsDto", "listEventDto", "fieldEventId", "visibleAddEdit"})
	public void save() throws NoSuchAlgorithmException{
		EventsDto e = new EventsDto();
		e = eventsSvc.findEventsById(eventsDto.getEventId());
		if (e.getEventId() == null) {
			if (eventsDto.getEventId() == null ) {
				Messagebox.show("Event ID harus terisi", "MESSAGE", null, null, null);
			} else if (eventsDto.getEventType() == null ) {
				Messagebox.show("EVENT type harus terisi", "MESSAGE", null, null, null);
			} else if (eventsDto.getEventComponent() == null ) {
				Messagebox.show("Event component harus terisi", "MESSAGE", null, null, null);
			} else if (eventsDto.getEventComponentTarget() == null ) {
				Messagebox.show("Event component target harus terisi", "MESSAGE", null, null, null);
			} else if (eventsDto.getEventData() == null ) {
				Messagebox.show("Event data harus terisi", "MESSAGE", null, null, null);
			} else if (eventsDto.getFormId() == null ) {
				Messagebox.show("Form id harus terisi", "MESSAGE", null, null, null);
			} else
				eventsSvc.saveEvents(eventsDto.getEventId(), eventsDto.getEventType(), eventsDto.getEventComponent(), eventsDto.getEventComponentTarget(), eventsDto.getEventData(), eventsDto.getFormId());
				load();
				Messagebox.show("Data event berhasil disimpan", "MESSAGE", null, null, null);
				eventsDto = new EventsDto();
				setVisibleAddEdit(false);
		} else {
			if (eventsDto.getEventId() == null ) {
				Messagebox.show("Event ID harus terisi", "MESSAGE", null, null, null);
			} else if (eventsDto.getEventType() == null ) {
				Messagebox.show("EVENT type harus terisi", "MESSAGE", null, null, null);
			} else if (eventsDto.getEventComponent() == null ) {
				Messagebox.show("Event component harus terisi", "MESSAGE", null, null, null);
			} else if (eventsDto.getEventComponentTarget() == null ) {
				Messagebox.show("Event component target harus terisi", "MESSAGE", null, null, null);
			} else if (eventsDto.getEventData() == null ) {
				Messagebox.show("Event data harus terisi", "MESSAGE", null, null, null);
			} else if (eventsDto.getFormId() == null ) {
				Messagebox.show("Form id harus terisi", "MESSAGE", null, null, null);
			} else {
				eventsSvc.updateEvents(eventsDto.getEventType(), eventsDto.getEventComponent(), eventsDto.getEventComponentTarget(), eventsDto.getEventData(), eventsDto.getFormId(),eventsDto.getEventId());
				load();
				Messagebox.show("Data event berhasil disimpan", "MESSAGE", null, null, null);
				eventsDto = new EventsDto();
				setVisibleAddEdit(false);
			}
		}
	}
	
	
	@Command("edit")
	@NotifyChange({"fieldEventId", "visibleAddEdit"})
	public void edit() {
		if(eventsDto == null){
			Messagebox.show("Choose your data first !","Message",null,null,null);
		} else{
			setFieldEventId(true);
			setVisibleAddEdit(true);
		}
	}
	
	
	@Command("delete")
	@NotifyChange({  "formsDto", "listFormsDto" })
	public void delete() {
		if (eventsDto.getEventId() == null) {
			Messagebox.show("Choose your data first !","Message",null,null,null);
		} else {
			Messagebox.show("Are you sure to delete ?", "perhatian",
					new Button[] { Button.YES, Button.NO },
					Messagebox.QUESTION, Button.NO,
					new EventListener<Messagebox.ClickEvent>() {
						@Override
						public void onEvent(ClickEvent event) throws Exception {
							if (Messagebox.ON_YES.equals(event.getName())) {
								eventsSvc.deleteEvents(eventsDto.getEventId());
								BindUtils.postNotifyChange(null, null,
										EventsVmd.this,
										"listEventDto");
								BindUtils.postNotifyChange(null, null,
										EventsVmd.this,
										"eventsDto");
								listEventDto = eventsSvc.findEventsWithSearch("");
								Messagebox.show("Data berhasil di hapus", "MESSAGE", null, null, null);
								eventsDto = new EventsDto();
							}
						}
					});
		}
	}

}
