package vmd;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import service.UserProfileSvc;

import dto.UserProfileDto;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class banerVmd {
	
	
	@WireVariable
	private UserProfileSvc dsfMstUser2Svc;
	
	private Date hariIni;
	private String tglini;
	private String username;
	private String role;
	private String position ;
	private String user ;
	private String name;


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getTglini() {
		return tglini;
	}

	public void setTglini(String tglini) {
		this.tglini = tglini;
	}

	public Date getHariIni() {
		return hariIni;
	}

	public void setHariIni(Date hariIni) {
		this.hariIni = hariIni;
	}
	

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	@Init
	public void load() {
		hariIni = new Date();
		SimpleDateFormat obDateFormat = new SimpleDateFormat("EEEE, dd MMM yyyy HH:MM");
		tglini = " "+obDateFormat.format(hariIni.getTime())+" ";
		if (Sessions.getCurrent().getAttribute("user") != null) {
			UserProfileDto user1 = (UserProfileDto) Sessions.getCurrent().getAttribute("user");	
			name = user1.getNamaKaryawan();
			position = "'"+user1.getAuthorityName()+"'";
			
		}
	}
	
	@Command("logout")
	public void logout() {
		Sessions.getCurrent().removeAttribute("user");
		Executions.getCurrent().sendRedirect("/login.zul");
	}



}
