package vmd;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.record.DSFRecord;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;

import dto.AuthorityMstDto;
import dto.CabangMstDto;
import dto.MenuMstDto;
import dto.UserProfileDto;
import pagevmd.NavigationVmd;
import service.CabangMstSvc;
import service.MenuMstSvc;
import service.UserProfileSvc;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class MasterMenuVmd extends NavigationVmd{

	@WireVariable
	private MenuMstSvc menuMstSvc;
	List<MenuMstDto> listMenuMstDto = new ArrayList<MenuMstDto>();
	private MenuMstDto menuMstDto;
	private int page = 10;
	private String keySearch = "";
	private boolean visibleAddEdit = false;
	
	
	public MenuMstSvc getMenuMstSvc() {
		return menuMstSvc;
	}

	public void setMenuMstSvc(MenuMstSvc menuMstSvc) {
		this.menuMstSvc = menuMstSvc;
	}

	public List<MenuMstDto> getListMenuMstDto() {
		return listMenuMstDto;
	}

	public void setListMenuMstDto(List<MenuMstDto> listMenuMstDto) {
		this.listMenuMstDto = listMenuMstDto;
	}

	public MenuMstDto getMenuMstDto() {
		return menuMstDto;
	}

	public void setMenuMstDto(MenuMstDto menuMstDto) {
		this.menuMstDto = menuMstDto;
	}

	public boolean isVisibleAddEdit() {
		return visibleAddEdit;
	}

	public void setVisibleAddEdit(boolean visibleAddEdit) {
		this.visibleAddEdit = visibleAddEdit;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getKeySearch() {
		return keySearch;
	}

	public void setKeySearch(String keySearch) {
		this.keySearch = keySearch;
	}
	
	
	@Init
	public void load() {
		Sessions.getCurrent().removeAttribute("sessionMobileTracking");
		listMenuMstDto = menuMstSvc.findMenuAllBySearch(keySearch);
	}
	
	@Command("page")
	@NotifyChange("page")
	public void page() {
		setPage(getPage());
	}
	
	@Command("add")
	@NotifyChange({"visibleAddEdit","menuMstDto"})
	public void add() {
		menuMstDto = new MenuMstDto();
		setVisibleAddEdit(true);
	}
	
	@Command("search")
	@NotifyChange({ "listMenuMstDto" })
	public void search() {
		listMenuMstDto = menuMstSvc.findMenuAllBySearch(keySearch);
	}
	
	@Command("close")
	@NotifyChange({ "visibleAddEdit","menuMstDto","listMenuMstDto"})
	public void close() {
		menuMstDto = new MenuMstDto();
		load();
		setVisibleAddEdit(false);
	}
	
	@Command("save")
	@NotifyChange({ "visibleAddEdit","menuMstDto" })
	public void save() {
		
		menuMstDto = new MenuMstDto();;
		setVisibleAddEdit(false);
	}
	
	@Command("delete")
	@NotifyChange({  "cabangMstDto", "listCabangMstDto" })
	public void delete() {
		if (menuMstDto.getMenu() == 0) {
			Messagebox.show("Pilih data terlebih dahulu !","Message",null,null,null);
		} else {
			Messagebox.show("Apakah anda yakin akan menonaktifkan data ini ?", "perhatian",
					new Button[] { Button.YES, Button.NO },
					Messagebox.QUESTION, Button.NO,
					new EventListener<Messagebox.ClickEvent>() {
						@Override
						public void onEvent(ClickEvent event) throws Exception {
							if (Messagebox.ON_YES.equals(event.getName())) {
								UserProfileDto userProfileDto = (UserProfileDto) Sessions.getCurrent().getAttribute("user");
								menuMstDto.setModifyUser(userProfileDto.getNamaKaryawan());
								Date date = new Date();
								menuMstSvc.DeleteMenu(menuMstDto.getModifyUser(), menuMstDto.getMenu());
								BindUtils.postNotifyChange(null, null,
										MasterMenuVmd.this, "listMenuMstDto");
								listMenuMstDto= menuMstSvc.findMenuAllBySearch("");
								Messagebox.show("Menu berhasil di disable", "MESSAGE", null, null, null);								
								menuMstDto= new MenuMstDto();
							}
						}
					});
		}
	}
	
	@Command("aktifkan")
	@NotifyChange({  "cabangMstDto", "listCabangMstDto" })
	public void aktifkan() {
		if (menuMstDto.getMenu() == 0) {
			Messagebox.show("Pilih data terlebih dahulu !","Message",null,null,null);
		} else {
			Messagebox.show("Apakah anda yakin akan mengaktifkan data ini ?", "perhatian",
					new Button[] { Button.YES, Button.NO },
					Messagebox.QUESTION, Button.NO,
					new EventListener<Messagebox.ClickEvent>() {
						@Override
						public void onEvent(ClickEvent event) throws Exception {
							if (Messagebox.ON_YES.equals(event.getName())) {
								UserProfileDto userProfileDto = (UserProfileDto) Sessions.getCurrent().getAttribute("user");
								menuMstDto.setModifyUser(userProfileDto.getNamaKaryawan());
								Date date = new Date();
								menuMstSvc.aktifkanMenu(menuMstDto.getModifyUser(), menuMstDto.getMenu());
								BindUtils.postNotifyChange(null, null,
										MasterMenuVmd.this, "listMenuMstDto");
								listMenuMstDto= menuMstSvc.findMenuAllBySearch("");
								Messagebox.show("Menu berhasil diaktifkan", "MESSAGE", null, null, null);								
								menuMstDto= new MenuMstDto();
							}
						}
					});
		}
	}
	
}
