package vmd;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.record.DSFRecord;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;

import dto.AuthorityMstDto;
import dto.CabangMstDto;
import dto.UserProfileDto;
import pagevmd.NavigationVmd;
import service.AuthorityMstSvc;
import service.CabangMstSvc;
import service.UserProfileSvc;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class MasterAuthoritysVmd extends NavigationVmd{

	@WireVariable
	private CabangMstSvc cabangMstSvc;
	
	@WireVariable
	private AuthorityMstSvc authorityMstSvc;
	
	private List<AuthorityMstDto> listAuthorityMstDto = new ArrayList<AuthorityMstDto>();
	private AuthorityMstDto authorityMstDto;
	private int page = 10;
	private String keySearch = "";
	private boolean visibleAddEdit = false;
	private boolean fieldKodeCabang = false;
	private boolean visibleAuth = false;
	
	
	

	public boolean isVisibleAuth() {
		return visibleAuth;
	}

	public void setVisibleAuth(boolean visibleAuth) {
		this.visibleAuth = visibleAuth;
	}

	public boolean isVisibleAddEdit() {
		return visibleAddEdit;
	}

	public void setVisibleAddEdit(boolean visibleAddEdit) {
		this.visibleAddEdit = visibleAddEdit;
	}

	public List<AuthorityMstDto> getListAuthorityMstDto() {
		return listAuthorityMstDto;
	}

	public void setListAuthorityMstDto(List<AuthorityMstDto> listAuthorityMstDto) {
		this.listAuthorityMstDto = listAuthorityMstDto;
	}

	public AuthorityMstDto getAuthorityMstDto() {
		return authorityMstDto;
	}

	public void setAuthorityMstDto(AuthorityMstDto authorityMstDto) {
		this.authorityMstDto = authorityMstDto;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getKeySearch() {
		return keySearch;
	}

	public void setKeySearch(String keySearch) {
		this.keySearch = keySearch;
	}
	
	public boolean isFieldKodeCabang() {
		return fieldKodeCabang;
	}

	public void setFieldKodeCabang(boolean fieldKodeCabang) {
		this.fieldKodeCabang = fieldKodeCabang;
	}

	@Init
	public void load() {
		Sessions.getCurrent().removeAttribute("sessionMobileTracking");
		listAuthorityMstDto = authorityMstSvc.searchAuthority("");
	}
	
	@Command("page")
	@NotifyChange("page")
	public void page() {
		setPage(getPage());
	}
	
	@Command("add")
	@NotifyChange({"visibleAddEdit","authorityMstDto","fieldKodeCabang"})
	public void add() {
		authorityMstDto = new AuthorityMstDto();
		setFieldKodeCabang(false);
		setVisibleAddEdit(true);
	}
	
	@Command("search")
	@NotifyChange({ "listAuthorityMstDto","keySearch" })
	public void search() {
		listAuthorityMstDto = authorityMstSvc.searchAuthority(keySearch);
	}
	
	@Command("close")
	@NotifyChange({ "visibleAddEdit","cabangMstDto","listAuthorityMstDto"})
	public void close() {
		authorityMstDto = new AuthorityMstDto();
		load();
		setVisibleAddEdit(false);
	}
	
	@Command("save")
	@NotifyChange({ "visibleAddEdit","authorityMstDto","listAuthorityMstDto" })
	public void save() {
		AuthorityMstDto auth2 = new AuthorityMstDto();
		auth2 = authorityMstSvc.findOneAuthority(authorityMstDto.getAuthorityId());
		System.out.println("authority id : "+auth2.getAuthorityId());
		if (authorityMstDto.getAuthorityId() == 0) {
			if (authorityMstDto.getAuthorityName() == null || authorityMstDto.getAuthorityName().equalsIgnoreCase("")) {
				Messagebox.show("Nama authority harus terisi", "MESSAGE", null, null, null);
			}else if (authorityMstDto.getAktif() == null || authorityMstDto.getAktif().equalsIgnoreCase("")) {
				Messagebox.show("Status aktif harus terisi", "MESSAGE", null, null, null);
			} else{
				UserProfileDto userProfileDto = (UserProfileDto) Sessions.getCurrent().getAttribute("user");
				authorityMstDto.setModifyUser(userProfileDto.getNamaKaryawan());
				int intAktif = 1;
				if(authorityMstDto.getAktif().equalsIgnoreCase("1") || authorityMstDto.getAktif().equalsIgnoreCase("Enabled")){
					intAktif = 1;
				}else{
					intAktif = 0;
				}
				authorityMstSvc.saveAuthority(authorityMstDto.getAuthorityName(), intAktif, authorityMstDto.getModifyUser());
				load();
				Messagebox.show("Data cabang berhasil disimpan", "MESSAGE", null, null, null);
				setVisibleAddEdit(false);
			}
		} else {
			if (authorityMstDto.getAuthorityName() == null || authorityMstDto.getAuthorityName().equalsIgnoreCase("")) {
				Messagebox.show("Nama authority harus terisi", "MESSAGE", null, null, null);
			}else if (authorityMstDto.getAktif() == null || authorityMstDto.getAktif().equalsIgnoreCase("")) {
				Messagebox.show("Status aktif harus terisi", "MESSAGE", null, null, null);
			} else{
				UserProfileDto userProfileDto = (UserProfileDto) Sessions.getCurrent().getAttribute("user");
				authorityMstDto.setModifyUser(userProfileDto.getNamaKaryawan());
				int intAktif = 1;
				if(authorityMstDto.getAktif().equalsIgnoreCase("1") || authorityMstDto.getAktif().equalsIgnoreCase("Enabled")){
					intAktif = 1;
				}else{
					intAktif = 0;
				}
				System.out.println("masuk edit");
				authorityMstSvc.updateAuthority(authorityMstDto.getAuthorityName(), intAktif, authorityMstDto.getModifyUser(), authorityMstDto.getAuthorityId());
				load();
				setVisibleAddEdit(false);
				Messagebox.show("Data authority berhasil diubah", "MESSAGE", null, null, null);
			}
		}
		listAuthorityMstDto = authorityMstSvc.searchAuthority("");
		authorityMstDto = new AuthorityMstDto();
		
	
	}
	
	@Command("edit")
	@NotifyChange({"visibleKodeCabang","visibleAddEdit","visibleAuth","masterRegionalDto"})
	public void edit() {
		if(authorityMstDto==null){
			Messagebox.show("Choose your data first !","Message",null,null,null);
		} else{
			setVisibleAuth(true);
			setVisibleAddEdit(true);
		}
	}
	
	@Command("delete")
	@NotifyChange({  "cabangMstDto", "listCabangMstDto","authorityMstDto"})
	public void delete() {
		if (authorityMstDto.getAuthorityId()== 0) {
			Messagebox.show("Choose your data first !","Message",null,null,null);
		} else {
			UserProfileDto userProfileDto = (UserProfileDto) Sessions.getCurrent().getAttribute("user");
			authorityMstDto.setModifyUser(userProfileDto.getNamaKaryawan());
			Messagebox.show("Are you sure to delete ?", "perhatian",
					new Button[] { Button.YES, Button.NO },
					Messagebox.QUESTION, Button.NO,
					new EventListener<Messagebox.ClickEvent>() {
						@Override
						public void onEvent(ClickEvent event) throws Exception {
							if (Messagebox.ON_YES.equals(event.getName())) {
								authorityMstSvc.deleteAuthority(authorityMstDto.getModifyUser(), authorityMstDto.getAuthorityId());
								BindUtils.postNotifyChange(null, null,
										MasterAuthoritysVmd.this,
										"listAuthorityMstDto");
								BindUtils.postNotifyChange(null, null,
										MasterAuthoritysVmd.this,
										"authorityMstDto");
								listAuthorityMstDto = authorityMstSvc.searchAuthority("");
								Messagebox.show("Data berhasil di disable !","Message",null,null,null);
								authorityMstDto= new AuthorityMstDto();
							}
						}
					});
		}
	}

}
