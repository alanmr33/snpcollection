package vmd;

import java.util.Date;
import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Include;

import service.MobileTrackingSvc;
import service.UserProfileSvc;
import dto.MobileTrackingDto;
import dto.UserProfileDto;


public class MobileTrackingMap2 {
	private String jsonMap2;
	private int banyakListMap2;
	private String strArrayRegional = "";
	private String strArrayCabang = "";
	private String strArrayKolektor = "";
	private Date tglAwal;
	private Date tglAkhir;
	private MobileTrackingDto mt = new MobileTrackingDto();
	private MobileTrackingDto trackingDto = new MobileTrackingDto();
	
	@WireVariable
	private UserProfileSvc userProfileSvc;
	
	
	
	public String getJsonMap2() {
		return jsonMap2;
	}
	public void setJsonMap2(String jsonMap2) {
		this.jsonMap2 = jsonMap2;
	}
	public int getBanyakListMap2() {
		return banyakListMap2;
	}
	public void setBanyakListMap2(int banyakListMap2) {
		this.banyakListMap2 = banyakListMap2;
	}
	public String getStrArrayRegional() {
		return strArrayRegional;
	}
	public void setStrArrayRegional(String strArrayRegional) {
		this.strArrayRegional = strArrayRegional;
	}
	public String getStrArrayCabang() {
		return strArrayCabang;
	}
	public void setStrArrayCabang(String strArrayCabang) {
		this.strArrayCabang = strArrayCabang;
	}
	public String getStrArrayKolektor() {
		return strArrayKolektor;
	}
	public void setStrArrayKolektor(String strArrayKolektor) {
		this.strArrayKolektor = strArrayKolektor;
	}
	public Date getTglAwal() {
		return tglAwal;
	}
	public void setTglAwal(Date tglAwal) {
		this.tglAwal = tglAwal;
	}
	public Date getTglAkhir() {
		return tglAkhir;
	}
	public void setTglAkhir(Date tglAkhir) {
		this.tglAkhir = tglAkhir;
	}
	public MobileTrackingDto getMt() {
		return mt;
	}
	public void setMt(MobileTrackingDto mt) {
		this.mt = mt;
	}
	
	
	public MobileTrackingDto getTrackingDto() {
		return trackingDto;
	}
	public void setTrackingDto(MobileTrackingDto trackingDto) {
		this.trackingDto = trackingDto;
	}
	@Init
	public void load(){
		mt = (MobileTrackingDto) Sessions.getCurrent().getAttribute("trackingDto");
		strArrayKolektor = mt.getNamaKaryawan();
		strArrayRegional = mt.getNamaRegion();
		strArrayCabang = mt.getNamaCabang();
		jsonMap2 = (String) Sessions.getCurrent().getAttribute("jsonMap2");
		banyakListMap2 = (int) Sessions.getCurrent().getAttribute("banyakListMap2");
		tglAwal = (Date) Sessions.getCurrent().getAttribute("tglAwal");
		tglAkhir = (Date) Sessions.getCurrent().getAttribute("tglAkhir");
	}
	
	@Command("closeMap")
	public void closeMap(){
		Include inc = (Include) Executions.getCurrent().getDesktop()
				.getPage("index").getFellow("mainInclude");
		inc.setSrc("/master/mobileTracking/refresh.zul");
		Include inc2 = (Include) Executions.getCurrent().getDesktop()
				.getPage("index").getFellow("mainInclude");
		inc2.setSrc("/master/mobileTracking/_index.zul");
	}

}
