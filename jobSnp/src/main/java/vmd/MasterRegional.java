package vmd;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;

import dto.MasterRegionalDto;
import dto.UserProfileDto;
import pagevmd.NavigationVmd;
import service.MasterRegionalSvc;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class MasterRegional extends NavigationVmd{
	
	@WireVariable
	private MasterRegionalSvc masterRegionalSvc;
	
	private MasterRegionalDto masterRegionalDto;
	private List<MasterRegionalDto> listRegionalDto = new ArrayList<MasterRegionalDto>();
	
	private String search="";
	private boolean addEditRegional=false;
	private int pages = 10;
	private boolean visibleKodeCabang=false;
	private UserProfileDto userProfileSession;
	
	
	
	public UserProfileDto getUserProfileSession() {
		return userProfileSession;
	}
	public void setUserProfileSession(UserProfileDto userProfileSession) {
		this.userProfileSession = userProfileSession;
	}
	public boolean isVisibleKodeCabang() {
		return visibleKodeCabang;
	}
	public void setVisibleKodeCabang(boolean visibleKodeCabang) {
		this.visibleKodeCabang = visibleKodeCabang;
	}
	public String getSearch() {
		return search;
	}
	public void setSearch(String search) {
		this.search = search;
	}
	public boolean isAddEditRegional() {
		return addEditRegional;
	}
	public void setAddEditRegional(boolean addEditRegional) {
		this.addEditRegional = addEditRegional;
	}
	public int getPages() {
		return pages;
	}
	public void setPages(int pages) {
		this.pages = pages;
	}
	public MasterRegionalDto getMasterRegionalDto() {
		return masterRegionalDto;
	}
	public void setMasterRegionalDto(MasterRegionalDto masterRegionalDto) {
		this.masterRegionalDto = masterRegionalDto;
	}
	public List<MasterRegionalDto> getListRegionalDto() {
		return listRegionalDto;
	}
	public void setListRegionalDto(List<MasterRegionalDto> listRegionalDto) {
		this.listRegionalDto = listRegionalDto;
	}
	
	
	
	@Init
	public void load(){
		Sessions.getCurrent().removeAttribute("sessionMobileTracking");
		listRegionalDto = masterRegionalSvc.searchAllRegional2("");
		userProfileSession = (UserProfileDto) Sessions.getCurrent().getAttribute("user");
	}
	
	@Command("page")
	@NotifyChange("page")
	public void page() {
		setPages(getPages());
	}
	
	@Command("add")
	@NotifyChange({"addEditRegional","masterRegionalDto", "visibleKodeCabang"})
	public void add() {
		setVisibleKodeCabang(false);
		masterRegionalDto = new MasterRegionalDto();
		setAddEditRegional(true);
	}
	
	@Command("search")
	@NotifyChange({ "listRegionalDto","search" })
	public void search() {
		listRegionalDto = masterRegionalSvc.searchAllRegional2(search);
	}
	
	@Command("close")
	@NotifyChange({ "addEditRegional","masterRegionalDto","listRegionalDto" })
	public void close() {
		masterRegionalDto = new MasterRegionalDto();
		load();
		setAddEditRegional(false);
	}
	
	@Command("save")
	@NotifyChange({ "addEditRegional","masterRegionalDto","listRegionalDto" })
	public void save() {
		List<MasterRegionalDto> listCekregional = masterRegionalSvc.searchRegionalByRegionalId(masterRegionalDto.getKodeRegion());
	    if (listCekregional.size() == 0) {
			if (masterRegionalDto.getKodeRegion() == null || masterRegionalDto.getNamaRegion()== null || masterRegionalDto.getNotes() == null || masterRegionalDto.getAktifStatus() == null) {
				Messagebox.show("Semua field harus diisi !", "MESSAGE", null, null, null);
			} else {
				int intAktifAtat = 1;
				if (masterRegionalDto.getAktifStatus().equalsIgnoreCase("Active")) {
					intAktifAtat = 1;
				}else {
					intAktifAtat = 0;
				}
				masterRegionalSvc.InsertRegional(masterRegionalDto.getKodeRegion(), 
						masterRegionalDto.getNamaRegion(), 
						masterRegionalDto.getNotes(), 
						intAktifAtat, userProfileSession.getUserId());
				load();
				Messagebox.show("SAVE BERHASIL !", "MESSAGE", null, null, null);
				setAddEditRegional(false);
			}
		} else {
			if (masterRegionalDto.getKodeRegion() == null || masterRegionalDto.getNamaRegion()== null || masterRegionalDto.getNotes() == null || masterRegionalDto.getAktifStatus() == null) {
				Messagebox.show("Semua field harus diisi !", "MESSAGE", null, null, null);
			} else {
				String intAktifAtat = "1";
				if (masterRegionalDto.getAktifStatus().equalsIgnoreCase("Active")) {
					intAktifAtat = "1";
				}else {
					intAktifAtat = "0";
				}
				masterRegionalSvc.UpdateRegional(masterRegionalDto.getNamaRegion(), masterRegionalDto.getNotes(), intAktifAtat , userProfileSession.getUserId(), masterRegionalDto.getKodeRegion());
				load();
				setAddEditRegional(false);
				Messagebox.show("UPDATE BERHASIL !", "MESSAGE", null, null, null);
			}
		}
		listRegionalDto = masterRegionalSvc.findAllRegional();
	}
	
	@Command("edit")
	@NotifyChange({"addEditRegional","visibleKodeCabang","masterRegionalDto"})
	public void edit() {
		if(masterRegionalDto==null){
			Messagebox.show("Choose your data first !","Message",null,null,null);
		} else{
			System.out.println("kode region "+masterRegionalDto.getKodeRegion());
			setVisibleKodeCabang(true);
			setAddEditRegional(true);
		}
	}
	@Command("delete")
	@NotifyChange({ "addEditRegional", "masterRegionalDto", "listRegionalDto" })
	public void delete() {
		if (masterRegionalDto.getKodeRegion()== null) {
			Messagebox.show("Choose your data first !","Message",null,null,null);
		} else {
			Messagebox.show("Are you sure to delete ?", "perhatian",
					new Button[] { Button.YES, Button.NO },
					Messagebox.QUESTION, Button.NO,
					new EventListener<Messagebox.ClickEvent>() {
						@Override
						public void onEvent(ClickEvent event) throws Exception {
							if (Messagebox.ON_YES.equals(event.getName())) {
								Date date = new Date();
								masterRegionalSvc.DeleteRegional(userProfileSession.getUserId(), masterRegionalDto.getKodeRegion());
								BindUtils.postNotifyChange(null, null,
										MasterRegional.this,
										"listDsfMstRegionalDto");
								BindUtils.postNotifyChange(null, null,
										MasterRegional.this,
										"listRegionalDto");
								listRegionalDto = masterRegionalSvc.searchAllRegional2("");
								listRegionalDto = masterRegionalSvc.findAllRegional();
								Clients.showNotification(
										"Data berhasil di non active",
										Clients.NOTIFICATION_TYPE_INFO, null,
										null, 1500);
								masterRegionalDto= new MasterRegionalDto();
							}
						}
					});
		}
	}
}
