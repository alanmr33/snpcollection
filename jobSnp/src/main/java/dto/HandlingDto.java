package dto;

import java.util.Date;

public class HandlingDto {
	private int batchHandling;
	private String countracNo;
	private int installmentNo;
	private int orderId;
	private String kodeHandling;
	private String kodeKonsumen;
	private String noLpk;
	private String notes;
	private Date tanggalHandling;
	private String ketTanggalHandling;
	private Date tanggalJanji;
	private String ketTanggalJanji;
	private int promiseCount;
	private int promiseAvailable;
	private int promiseRange;
	private String status;
	private String createUser;
	private Date createDate;
	private String ketCreateDate;
	private String modifyUser;
	private Date modifyDate;
	private String ketModifyDate;
	public int getBatchHandling() {
		return batchHandling;
	}
	public void setBatchHandling(int batchHandling) {
		this.batchHandling = batchHandling;
	}
	public String getCountracNo() {
		return countracNo;
	}
	public void setCountracNo(String countracNo) {
		this.countracNo = countracNo;
	}
	public int getInstallmentNo() {
		return installmentNo;
	}
	public void setInstallmentNo(int installmentNo) {
		this.installmentNo = installmentNo;
	}
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public String getKodeHandling() {
		return kodeHandling;
	}
	public void setKodeHandling(String kodeHandling) {
		this.kodeHandling = kodeHandling;
	}
	public String getKodeKonsumen() {
		return kodeKonsumen;
	}
	public void setKodeKonsumen(String kodeKonsumen) {
		this.kodeKonsumen = kodeKonsumen;
	}
	public String getNoLpk() {
		return noLpk;
	}
	public void setNoLpk(String noLpk) {
		this.noLpk = noLpk;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public Date getTanggalHandling() {
		return tanggalHandling;
	}
	public void setTanggalHandling(Date tanggalHandling) {
		this.tanggalHandling = tanggalHandling;
	}
	public String getKetTanggalHandling() {
		return ketTanggalHandling;
	}
	public void setKetTanggalHandling(String ketTanggalHandling) {
		this.ketTanggalHandling = ketTanggalHandling;
	}
	public Date getTanggalJanji() {
		return tanggalJanji;
	}
	public void setTanggalJanji(Date tanggalJanji) {
		this.tanggalJanji = tanggalJanji;
	}
	public String getKetTanggalJanji() {
		return ketTanggalJanji;
	}
	public void setKetTanggalJanji(String ketTanggalJanji) {
		this.ketTanggalJanji = ketTanggalJanji;
	}
	public int getPromiseCount() {
		return promiseCount;
	}
	public void setPromiseCount(int promiseCount) {
		this.promiseCount = promiseCount;
	}
	public int getPromiseAvailable() {
		return promiseAvailable;
	}
	public void setPromiseAvailable(int promiseAvailable) {
		this.promiseAvailable = promiseAvailable;
	}
	public int getPromiseRange() {
		return promiseRange;
	}
	public void setPromiseRange(int promiseRange) {
		this.promiseRange = promiseRange;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getKetCreateDate() {
		return ketCreateDate;
	}
	public void setKetCreateDate(String ketCreateDate) {
		this.ketCreateDate = ketCreateDate;
	}
	public String getModifyUser() {
		return modifyUser;
	}
	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}
	public Date getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	public String getKetModifyDate() {
		return ketModifyDate;
	}
	public void setKetModifyDate(String ketModifyDate) {
		this.ketModifyDate = ketModifyDate;
	}
	
	
}
