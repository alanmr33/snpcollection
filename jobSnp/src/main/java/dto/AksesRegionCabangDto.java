package dto;

public class AksesRegionCabangDto {
	private int id;
	private String  userId;
	private String kodeRegion;
	private String kodeCabang;
	private String namaKaryawan;
	private String namaRegional;
	private String namaCabang;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getKodeRegion() {
		return kodeRegion;
	}
	public void setKodeRegion(String kodeRegion) {
		this.kodeRegion = kodeRegion;
	}
	public String getKodeCabang() {
		return kodeCabang;
	}
	public void setKodeCabang(String kodeCabang) {
		this.kodeCabang = kodeCabang;
	}
	public String getNamaKaryawan() {
		return namaKaryawan;
	}
	public void setNamaKaryawan(String namaKaryawan) {
		this.namaKaryawan = namaKaryawan;
	}
	public String getNamaRegional() {
		return namaRegional;
	}
	public void setNamaRegional(String namaRegional) {
		this.namaRegional = namaRegional;
	}
	public String getNamaCabang() {
		return namaCabang;
	}
	public void setNamaCabang(String namaCabang) {
		this.namaCabang = namaCabang;
	}
	
	

}
