package dto;

import java.util.Date;

public class MobileTrackingDto {
	private String userId;
	private String namaKaryawan;
	private String namaRegion;
	private String namaCabang;
	private Date trackingDate;
	private String location;
	private String orderId;
	private String ketTrackingDate;
	private float latitude;
	private float longitude;
	private char huruf;
	private String ketTooltip;
	
	
	public String getKetTooltip() {
		return ketTooltip;
	}
	public void setKetTooltip(String ketTooltip) {
		this.ketTooltip = ketTooltip;
	}
	public char getHuruf() {
		return huruf;
	}
	public void setHuruf(char huruf) {
		this.huruf = huruf;
	}
	public float getLatitude() {
		return latitude;
	}
	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}
	public float getLongitude() {
		return longitude;
	}
	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}
	public String getKetTrackingDate() {
		return ketTrackingDate;
	}
	public void setKetTrackingDate(String ketTrackingDate) {
		this.ketTrackingDate = ketTrackingDate;
	}
	public String getNamaKaryawan() {
		return namaKaryawan;
	}
	public void setNamaKaryawan(String namaKaryawan) {
		this.namaKaryawan = namaKaryawan;
	}
	public String getNamaRegion() {
		return namaRegion;
	}
	public void setNamaRegion(String namaRegion) {
		this.namaRegion = namaRegion;
	}
	public String getNamaCabang() {
		return namaCabang;
	}
	public void setNamaCabang(String namaCabang) {
		this.namaCabang = namaCabang;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Date getTrackingDate() {
		return trackingDate;
	}
	public void setTrackingDate(Date trackingDate) {
		this.trackingDate = trackingDate;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
}
