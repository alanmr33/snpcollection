package dto;

import java.util.Date;

public class FormFieldsDto {
	private String fieldId;
	private String fieldName;
	private String fieldType;
	private String fieldVisibility;
	private String fieldLabel;
	private String fieldGlobalValue;
	private String fieldGlobalValueKet;
	private String fieldWeight;
	private String fieldStore;
	private String fieldExtra;
	private String rowId;
	
	
	public String getFieldGlobalValueKet() {
		return fieldGlobalValueKet;
	}
	public void setFieldGlobalValueKet(String fieldGlobalValueKet) {
		this.fieldGlobalValueKet = fieldGlobalValueKet;
	}
	public String getFieldId() {
		return fieldId;
	}
	public void setFieldId(String fieldId) {
		this.fieldId = fieldId;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getFieldType() {
		return fieldType;
	}
	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}
	public String getFieldVisibility() {
		return fieldVisibility;
	}
	public void setFieldVisibility(String fieldVisibility) {
		this.fieldVisibility = fieldVisibility;
	}
	public String getFieldLabel() {
		return fieldLabel;
	}
	public void setFieldLabel(String fieldLabel) {
		this.fieldLabel = fieldLabel;
	}
	public String getFieldGlobalValue() {
		return fieldGlobalValue;
	}
	public void setFieldGlobalValue(String fieldGlobalValue) {
		this.fieldGlobalValue = fieldGlobalValue;
	}
	public String getFieldWeight() {
		return fieldWeight;
	}
	public void setFieldWeight(String fieldWeight) {
		this.fieldWeight = fieldWeight;
	}
	public String getFieldStore() {
		return fieldStore;
	}
	public void setFieldStore(String fieldStore) {
		this.fieldStore = fieldStore;
	}
	public String getFieldExtra() {
		return fieldExtra;
	}
	public void setFieldExtra(String fieldExtra) {
		this.fieldExtra = fieldExtra;
	}
	public String getRowId() {
		return rowId;
	}
	public void setRowId(String rowId) {
		this.rowId = rowId;
	}
	
	
}
