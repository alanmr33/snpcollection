package dto;

import java.util.Date;

public class FormRowsDto {
	private String rowId;
	private String rowOrder;
	private String rowVisibility;
	private String formId;
	private String item;
	
	
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getRowId() {
		return rowId;
	}
	public void setRowId(String rowId) {
		this.rowId = rowId;
	}
	public String getRowOrder() {
		return rowOrder;
	}
	public void setRowOrder(String rowOrder) {
		this.rowOrder = rowOrder;
	}
	public String getRowVisibility() {
		return rowVisibility;
	}
	public void setRowVisibility(String rowVisibility) {
		this.rowVisibility = rowVisibility;
	}
	public String getFormId() {
		return formId;
	}
	public void setFormId(String formId) {
		this.formId = formId;
	}
	
	
}
