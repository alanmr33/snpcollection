package dto;

import java.util.Date;

public class MenuMstDto {
	private int menu;
	private String menuName;
	private String url;
	private int menuSort;
	private String displayFlg;
	private String createUser;
	private Date createDate;
	private String modifyUser;
	private Date modifyDate;
	private int aktifStatus;
	private String ketAktifStatus;
	
	
	public int getAktifStatus() {
		return aktifStatus;
	}
	public void setAktifStatus(int aktifStatus) {
		this.aktifStatus = aktifStatus;
	}
	public String getKetAktifStatus() {
		return ketAktifStatus;
	}
	public void setKetAktifStatus(String ketAktifStatus) {
		this.ketAktifStatus = ketAktifStatus;
	}
	public int getMenu() {
		return menu;
	}
	public void setMenu(int menu) {
		this.menu = menu;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getMenuSort() {
		return menuSort;
	}
	public void setMenuSort(int menuSort) {
		this.menuSort = menuSort;
	}
	public String getDisplayFlg() {
		return displayFlg;
	}
	public void setDisplayFlg(String displayFlg) {
		this.displayFlg = displayFlg;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getModifyUser() {
		return modifyUser;
	}
	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}
	public Date getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	
	
}
