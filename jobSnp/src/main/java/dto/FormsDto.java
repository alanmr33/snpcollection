package dto;

import java.util.Date;

public class FormsDto {
	private String formId;
	private String formName;
	private String formLabel;
	private String formOrder;
	public String getFormId() {
		return formId;
	}
	public void setFormId(String formId) {
		this.formId = formId;
	}
	public String getFormName() {
		return formName;
	}
	public void setFormName(String formName) {
		this.formName = formName;
	}
	public String getFormLabel() {
		return formLabel;
	}
	public void setFormLabel(String formLabel) {
		this.formLabel = formLabel;
	}
	public String getFormOrder() {
		return formOrder;
	}
	public void setFormOrder(String formOrder) {
		this.formOrder = formOrder;
	}
	
	
}
