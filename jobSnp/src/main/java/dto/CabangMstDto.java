package dto;

import java.util.Date;

public class CabangMstDto {
	private String kodeCabang;
	private String kodeRegional;
	private String namaRegion;
	private String namaCabang;
	private String alamat;
	private int aktifStatus;
	private String keteranganAktif;
	private String createUser;
	private Date createDate;
	private String modifyUser;
	private Date modifyDate;
	private boolean choose;
	private boolean itemList;
	
	
	public boolean isItemList() {
		return itemList;
	}
	public void setItemList(boolean itemList) {
		this.itemList = itemList;
	}
	public boolean isChoose() {
		return choose;
	}
	public void setChoose(boolean choose) {
		this.choose = choose;
	}
	public String getKeteranganAktif() {
		return keteranganAktif;
	}
	public void setKeteranganAktif(String keteranganAktif) {
		this.keteranganAktif = keteranganAktif;
	}
	public String getNamaRegion() {
		return namaRegion;
	}
	public void setNamaRegion(String namaRegion) {
		this.namaRegion = namaRegion;
	}
	public String getKodeCabang() {
		return kodeCabang;
	}
	public void setKodeCabang(String kodeCabang) {
		this.kodeCabang = kodeCabang;
	}
	public String getKodeRegional() {
		return kodeRegional;
	}
	public void setKodeRegional(String kodeRegional) {
		this.kodeRegional = kodeRegional;
	}
	public String getNamaCabang() {
		return namaCabang;
	}
	public void setNamaCabang(String namaCabang) {
		this.namaCabang = namaCabang;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public int getAktifStatus() {
		return aktifStatus;
	}
	public void setAktifStatus(int aktifStatus) {
		this.aktifStatus = aktifStatus;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getModifyUser() {
		return modifyUser;
	}
	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}
	public Date getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	
	
}
