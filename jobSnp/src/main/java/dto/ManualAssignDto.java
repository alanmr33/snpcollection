package dto;

public class ManualAssignDto {

	private int orderId;
	private String customerCode;
	private String kodeKolektor;
	private String namaKaryawan;
	private String contractNo;
	private String lpkNo;
	private String customerName;
	private String address;
	private String phoneNum;
	private String notes;
	private int instalmentNo;
	private String dueDate;
	private String reassignBy;
	private String namaRegional;
	private int bucketCollector;
	private int bucketPayment;
	private int bucketHandling;
	private int bucketNotHandling;
	private String previosCollector;
	private String reassignDate;
	
	
	
	public String getReassignDate() {
		return reassignDate;
	}
	public void setReassignDate(String reassignDate) {
		this.reassignDate = reassignDate;
	}
	public String getNamaRegional() {
		return namaRegional;
	}
	public void setNamaRegional(String namaRegional) {
		this.namaRegional = namaRegional;
	}
	public int getBucketCollector() {
		return bucketCollector;
	}
	public void setBucketCollector(int bucketCollector) {
		this.bucketCollector = bucketCollector;
	}
	public int getBucketPayment() {
		return bucketPayment;
	}
	public void setBucketPayment(int bucketPayment) {
		this.bucketPayment = bucketPayment;
	}
	public int getBucketHandling() {
		return bucketHandling;
	}
	public void setBucketHandling(int bucketHandling) {
		this.bucketHandling = bucketHandling;
	}
	public int getBucketNotHandling() {
		return bucketNotHandling;
	}
	public void setBucketNotHandling(int bucketNotHandling) {
		this.bucketNotHandling = bucketNotHandling;
	}
	public String getPreviosCollector() {
		return previosCollector;
	}
	public void setPreviosCollector(String previosCollector) {
		this.previosCollector = previosCollector;
	}
	public String getNamaKaryawan() {
		return namaKaryawan;
	}
	public void setNamaKaryawan(String namaKaryawan) {
		this.namaKaryawan = namaKaryawan;
	}
	public String getKodeKolektor() {
		return kodeKolektor;
	}
	public void setKodeKolektor(String kodeKolektor) {
		this.kodeKolektor = kodeKolektor;
	}
	public String getReassignBy() {
		return reassignBy;
	}
	public void setReassignBy(String reassignBy) {
		this.reassignBy = reassignBy;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public int getInstalmentNo() {
		return instalmentNo;
	}
	public void setInstalmentNo(int instalmentNo) {
		this.instalmentNo = instalmentNo;
	}
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getContractNo() {
		return contractNo;
	}
	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}
	public String getLpkNo() {
		return lpkNo;
	}
	public void setLpkNo(String lpkNo) {
		this.lpkNo = lpkNo;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	
}
