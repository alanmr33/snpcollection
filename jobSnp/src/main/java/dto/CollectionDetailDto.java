package dto;

import java.util.Date;

import org.jasypt.salt.StringFixedSaltGenerator;

public class CollectionDetailDto {
	private int orderId;
	private String customerCode;
	private String contractNo;
	private int installmentNo;
	private String lpkNo;
	private String customerName;
	private String address;
	private String village;
	private String district;
	private String rt;
	private String rw;
	private String city;
	private String kodeProvinsi;
	private String postCode;
	private Date dueDate;
	private String ketDueDate;
	private int installmentAmt;
	private int depositAmt;
	private int penaltyAmt;
	private int collectionFee;
	private int totalAmt;
	private String receiptNo;
	private int minPayment;
	private int maxPayment;
	private Date repaymentDate;
	private String ketRepaymentDate;
	private String notes;
	private int promiseCount;
	private int promiseAvailable;
	private int promiseRange;
	private String phoneArea;
	private String phoneNumber;
	private String mobilePrefix;
	private String mobileNo;
	private String namaBarang;
	private String poto;
	private Date lpkDate;
	private String ketLpkDate;
	private String collectorCode;
	private String status;
	private String ketValidasi;
	private String namaKaryawan;
	private String tagihan;
	private String idKolektor;
	private String namaCabang;
	private String intervalWaktu;
	private String jumlahKunjungan;
	private int no;
	private String tglBayar;
	private String tglJanjiBayar;
	private String jumlahBayar;
	private String tglKunjungan;
	private String userId;
	private String angsuran;
	private String kuitansiTertagih;
	private String pembayaran;
	private String totalPembayaran;
	private String namaRegional;
	private int bucketCollector;
	private int bucketPayment;
	private int bucketHandling;
	private int bucketNotHandling;
	private String previosCollector;
	
	
	
	public String getNamaRegional() {
		return namaRegional;
	}
	public void setNamaRegional(String namaRegional) {
		this.namaRegional = namaRegional;
	}
	public int getBucketCollector() {
		return bucketCollector;
	}
	public void setBucketCollector(int bucketCollector) {
		this.bucketCollector = bucketCollector;
	}
	public int getBucketPayment() {
		return bucketPayment;
	}
	public void setBucketPayment(int bucketPayment) {
		this.bucketPayment = bucketPayment;
	}
	public int getBucketHandling() {
		return bucketHandling;
	}
	public void setBucketHandling(int bucketHandling) {
		this.bucketHandling = bucketHandling;
	}
	public int getBucketNotHandling() {
		return bucketNotHandling;
	}
	public void setBucketNotHandling(int bucketNotHandling) {
		this.bucketNotHandling = bucketNotHandling;
	}
	public String getPreviosCollector() {
		return previosCollector;
	}
	public void setPreviosCollector(String previosCollector) {
		this.previosCollector = previosCollector;
	}
	public String getTotalPembayaran() {
		return totalPembayaran;
	}
	public void setTotalPembayaran(String totalPembayaran) {
		this.totalPembayaran = totalPembayaran;
	}
	public String getPembayaran() {
		return pembayaran;
	}
	public void setPembayaran(String pembayaran) {
		this.pembayaran = pembayaran;
	}
	public String getKuitansiTertagih() {
		return kuitansiTertagih;
	}
	public void setKuitansiTertagih(String kuitansiTertagih) {
		this.kuitansiTertagih = kuitansiTertagih;
	}
	public String getAngsuran() {
		return angsuran;
	}
	public void setAngsuran(String angsuran) {
		this.angsuran = angsuran;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getTglKunjungan() {
		return tglKunjungan;
	}
	public void setTglKunjungan(String tglKunjungan) {
		this.tglKunjungan = tglKunjungan;
	}
	public String getJumlahBayar() {
		return jumlahBayar;
	}
	public void setJumlahBayar(String jumlahBayar) {
		this.jumlahBayar = jumlahBayar;
	}
	public String getTglJanjiBayar() {
		return tglJanjiBayar;
	}
	public void setTglJanjiBayar(String tglJanjiBayar) {
		this.tglJanjiBayar = tglJanjiBayar;
	}
	public String getTglBayar() {
		return tglBayar;
	}
	public void setTglBayar(String tglBayar) {
		this.tglBayar = tglBayar;
	}
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public String getJumlahKunjungan() {
		return jumlahKunjungan;
	}
	public void setJumlahKunjungan(String jumlahKunjungan) {
		this.jumlahKunjungan = jumlahKunjungan;
	}
	public String getIntervalWaktu() {
		return intervalWaktu;
	}
	public void setIntervalWaktu(String intervalWaktu) {
		this.intervalWaktu = intervalWaktu;
	}
	public String getNamaCabang() {
		return namaCabang;
	}
	public void setNamaCabang(String namaCabang) {
		this.namaCabang = namaCabang;
	}
	public String getIdKolektor() {
		return idKolektor;
	}
	public void setIdKolektor(String idKolektor) {
		this.idKolektor = idKolektor;
	}
	public String getTagihan() {
		return tagihan;
	}
	public void setTagihan(String tagihan) {
		this.tagihan = tagihan;
	}
	public String getNamaKaryawan() {
		return namaKaryawan;
	}
	public void setNamaKaryawan(String namaKaryawan) {
		this.namaKaryawan = namaKaryawan;
	}
	public String getKetValidasi() {
		return ketValidasi;
	}
	public void setKetValidasi(String ketValidasi) {
		this.ketValidasi = ketValidasi;
	}
	public String getKetDueDate() {
		return ketDueDate;
	}
	public void setKetDueDate(String ketDueDate) {
		this.ketDueDate = ketDueDate;
	}
	public String getKetRepaymentDate() {
		return ketRepaymentDate;
	}
	public void setKetRepaymentDate(String ketRepaymentDate) {
		this.ketRepaymentDate = ketRepaymentDate;
	}
	public Date getLpkDate() {
		return lpkDate;
	}
	public void setLpkDate(Date lpkDate) {
		this.lpkDate = lpkDate;
	}
	public String getKetLpkDate() {
		return ketLpkDate;
	}
	public void setKetLpkDate(String ketLpkDate) {
		this.ketLpkDate = ketLpkDate;
	}
	public String getCollectorCode() {
		return collectorCode;
	}
	public void setCollectorCode(String collectorCode) {
		this.collectorCode = collectorCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getContractNo() {
		return contractNo;
	}
	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}
	public int getInstallmentNo() {
		return installmentNo;
	}
	public void setInstallmentNo(int installmentNo) {
		this.installmentNo = installmentNo;
	}
	public String getLpkNo() {
		return lpkNo;
	}
	public void setLpkNo(String lpkNo) {
		this.lpkNo = lpkNo;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getRt() {
		return rt;
	}
	public void setRt(String rt) {
		this.rt = rt;
	}
	public String getRw() {
		return rw;
	}
	public void setRw(String rw) {
		this.rw = rw;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getKodeProvinsi() {
		return kodeProvinsi;
	}
	public void setKodeProvinsi(String kodeProvinsi) {
		this.kodeProvinsi = kodeProvinsi;
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public int getInstallmentAmt() {
		return installmentAmt;
	}
	public void setInstallmentAmt(int installmentAmt) {
		this.installmentAmt = installmentAmt;
	}
	public int getDepositAmt() {
		return depositAmt;
	}
	public void setDepositAmt(int depositAmt) {
		this.depositAmt = depositAmt;
	}
	public int getPenaltyAmt() {
		return penaltyAmt;
	}
	public void setPenaltyAmt(int penaltyAmt) {
		this.penaltyAmt = penaltyAmt;
	}
	public int getCollectionFee() {
		return collectionFee;
	}
	public void setCollectionFee(int collectionFee) {
		this.collectionFee = collectionFee;
	}
	public int getTotalAmt() {
		return totalAmt;
	}
	public void setTotalAmt(int totalAmt) {
		this.totalAmt = totalAmt;
	}
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	public int getMinPayment() {
		return minPayment;
	}
	public void setMinPayment(int minPayment) {
		this.minPayment = minPayment;
	}
	public int getMaxPayment() {
		return maxPayment;
	}
	public void setMaxPayment(int maxPayment) {
		this.maxPayment = maxPayment;
	}
	public Date getRepaymentDate() {
		return repaymentDate;
	}
	public void setRepaymentDate(Date repaymentDate) {
		this.repaymentDate = repaymentDate;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public int getPromiseCount() {
		return promiseCount;
	}
	public void setPromiseCount(int promiseCount) {
		this.promiseCount = promiseCount;
	}
	public int getPromiseAvailable() {
		return promiseAvailable;
	}
	public void setPromiseAvailable(int promiseAvailable) {
		this.promiseAvailable = promiseAvailable;
	}
	public int getPromiseRange() {
		return promiseRange;
	}
	public void setPromiseRange(int promiseRange) {
		this.promiseRange = promiseRange;
	}
	public String getPhoneArea() {
		return phoneArea;
	}
	public void setPhoneArea(String phoneArea) {
		this.phoneArea = phoneArea;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getMobilePrefix() {
		return mobilePrefix;
	}
	public void setMobilePrefix(String mobilePrefix) {
		this.mobilePrefix = mobilePrefix;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getNamaBarang() {
		return namaBarang;
	}
	public void setNamaBarang(String namaBarang) {
		this.namaBarang = namaBarang;
	}
	public String getPoto() {
		return poto;
	}
	public void setPoto(String poto) {
		this.poto = poto;
	}
	
}
