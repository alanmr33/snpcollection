package dto;

public class MasterHandlingDto {
	
	private int handNo;
	private String handCode;
	private String promise;
	private String deskripsi;
	private String aktif;
	private String createUser;
	private String modifyUser;
	
	
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getModifyUser() {
		return modifyUser;
	}
	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}
	public int getHandNo() {
		return handNo;
	}
	public void setHandNo(int handNo) {
		this.handNo = handNo;
	}
	public String getHandCode() {
		return handCode;
	}
	public void setHandCode(String handCode) {
		this.handCode = handCode;
	}
	public String getPromise() {
		return promise;
	}
	public void setPromise(String promise) {
		this.promise = promise;
	}
	public String getDeskripsi() {
		return deskripsi;
	}
	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}
	public String getAktif() {
		return aktif;
	}
	public void setAktif(String aktif) {
		this.aktif = aktif;
	}

	
}
