package dto;

import java.util.Date;

public class ReassignCollector {
	private String collectorCode;
	private int seqNo;
	private String reassignCollector;
	private Date periodFrom;
	private Date periodTo;
	private String strPeriodFrom;
	private String strPeriodTo;
	private String notes;
	private int orderId;
	
	
	
	public String getStrPeriodFrom() {
		return strPeriodFrom;
	}
	public void setStrPeriodFrom(String strPeriodFrom) {
		this.strPeriodFrom = strPeriodFrom;
	}
	public String getStrPeriodTo() {
		return strPeriodTo;
	}
	public void setStrPeriodTo(String strPeriodTo) {
		this.strPeriodTo = strPeriodTo;
	}
	public String getCollectorCode() {
		return collectorCode;
	}
	public void setCollectorCode(String collectorCode) {
		this.collectorCode = collectorCode;
	}
	public int getSeqNo() {
		return seqNo;
	}
	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}
	public String getReassignCollector() {
		return reassignCollector;
	}
	public void setReassignCollector(String reassignCollector) {
		this.reassignCollector = reassignCollector;
	}
	public Date getPeriodFrom() {
		return periodFrom;
	}
	public void setPeriodFrom(Date periodFrom) {
		this.periodFrom = periodFrom;
	}
	public Date getPeriodTo() {
		return periodTo;
	}
	public void setPeriodTo(Date periodTo) {
		this.periodTo = periodTo;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	
	
	
}
