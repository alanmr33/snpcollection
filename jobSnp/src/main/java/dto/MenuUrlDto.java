package dto;

public class MenuUrlDto {
	
	public final static String MASTER_USER = "/master/masterUser/_index.zul";
	public final static String APPLICATION_ROLES = "/master/applicationRoles/_index.zul";
	public final static String CHANGE_PASSWORD = "/master/changePassword/_index.zul";
	
	public final static String MASTER_REGIONAL = "/master/masterRegion/_index.zul";
	public final static String MASTER_CABANG = "/master/masterCabang/_index.zul";
	public final static String MASTER_MENU = "/master/masterMenu/_index.zul";
	public final static String MASTER_AUTHORITY = "/master/masterAuthority/_index.zul";
	public final static String MASTER_PROVINSI = "/master/masterProvinsi/_index.zul";
	public final static String MASTER_HANDLING = "/master/masterHandling/_index.zul";
	
	public final static String UPLOAD_HANDLING = "/master/uploadHandling/_index.zul";
	public final static String REASSIGNMENT = "/master/reassignment/_index.zul";
	public final static String TASK_ADJUSTMENT = "/master/taskAdjustment/_index.zul";
	public final static String NOTIFICATION = "/master/notification/_index.zul";
	
	public final static String MOBILE_TRACKING = "/master/mobileTracking/_index.zul";
	
	public final static String REPORT_KUNJUNGAN_KOLEKTOR = "/master/reportKunjunganKolektor/_index.zul";
	public final static String REKON_KOLEKTOR = "/master/rekonKolektor/_index.zul";
	public final static String COLLECTOR_FULL_DATA = "/master/collectorFullData/_index.zul";
	
	public final static String MASTER_PARAMETER = "/master/masterParam/_index.zul";
	public final static String Master_FORMS = "/master/forms/_index.zul";
	public final static String MASTER_EVENTS = "/master/events/_index.zul";
	public final static String FORM_ROWS = "/master/formRows/_index.zul";
	

}
