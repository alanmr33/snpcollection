package dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import entity.UserMenuAccordion;

public class UserProfileDto {
	private String userId;
	private String kodeCabang;
	private String kodeRegion;
	private int aktifStatus;
	private String password;
	private String authorityId;
	private String authorityName;
	private String kodeKolektor;
	private String kodeKaryawan;
	private String namaKaryawan;
	private String imeiNumber;
	private String phoneNumber;
	private Date tanggalMulai;
	private Date tanggalAkhir;
	private String createUser;
	private Date createDate;
	private String modifyUser;
	private Date modifyDate;
	private String namaRegional;
	private String namaCabang;
	private String keteranganStatus;
	private boolean choose;
	private boolean itemList;
	
	
	private List<UserMenuAccordion> lisAccordions = new ArrayList<>();
	private List<String> listUserMenu = new ArrayList<>();
	
	
	public boolean isItemList() {
		return itemList;
	}
	public void setItemList(boolean itemList) {
		this.itemList = itemList;
	}
	public boolean isChoose() {
		return choose;
	}
	public void setChoose(boolean choose) {
		this.choose = choose;
	}
	public String getAuthorityName() {
		return authorityName;
	}
	public void setAuthorityName(String authorityName) {
		this.authorityName = authorityName;
	}
	public List<String> getListUserMenu() {
		return listUserMenu;
	}
	public void setListUserMenu(List<String> listUserMenu) {
		this.listUserMenu = listUserMenu;
	}
	public List<UserMenuAccordion> getLisAccordions() {
		return lisAccordions;
	}
	public void setLisAccordions(List<UserMenuAccordion> lisAccordions) {
		this.lisAccordions = lisAccordions;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getKodeCabang() {
		return kodeCabang;
	}
	public void setKodeCabang(String kodeCabang) {
		this.kodeCabang = kodeCabang;
	}
	public String getKodeRegion() {
		return kodeRegion;
	}
	public void setKodeRegion(String kodeRegion) {
		this.kodeRegion = kodeRegion;
	}
	public int getAktifStatus() {
		return aktifStatus;
	}
	public void setAktifStatus(int aktifStatus) {
		this.aktifStatus = aktifStatus;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAuthorityId() {
		return authorityId;
	}
	public void setAuthorityId(String authorityId) {
		this.authorityId = authorityId;
	}
	public String getKodeKolektor() {
		return kodeKolektor;
	}
	public void setKodeKolektor(String kodeKolektor) {
		this.kodeKolektor = kodeKolektor;
	}
	public String getKodeKaryawan() {
		return kodeKaryawan;
	}
	public void setKodeKaryawan(String kodeKaryawan) {
		this.kodeKaryawan = kodeKaryawan;
	}
	public String getNamaKaryawan() {
		return namaKaryawan;
	}
	public void setNamaKaryawan(String namaKaryawan) {
		this.namaKaryawan = namaKaryawan;
	}
	public String getImeiNumber() {
		return imeiNumber;
	}
	public void setImeiNumber(String imeiNumber) {
		this.imeiNumber = imeiNumber;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public Date getTanggalMulai() {
		return tanggalMulai;
	}
	public void setTanggalMulai(Date tanggalMulai) {
		this.tanggalMulai = tanggalMulai;
	}
	public Date getTanggalAkhir() {
		return tanggalAkhir;
	}
	public void setTanggalAkhir(Date tanggalAkhir) {
		this.tanggalAkhir = tanggalAkhir;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getModifyUser() {
		return modifyUser;
	}
	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}
	public Date getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	public String getNamaRegional() {
		return namaRegional;
	}
	public void setNamaRegional(String namaRegional) {
		this.namaRegional = namaRegional;
	}
	public String getNamaCabang() {
		return namaCabang;
	}
	public void setNamaCabang(String namaCabang) {
		this.namaCabang = namaCabang;
	}
	public String getKeteranganStatus() {
		return keteranganStatus;
	}
	public void setKeteranganStatus(String keteranganStatus) {
		this.keteranganStatus = keteranganStatus;
	}
	
	
	
}
