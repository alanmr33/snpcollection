package dto;

import java.util.Date;

public class PaymentDto {
	private int instalmentNo;
	private int orederId;
	private String customerCode;
	private String contractNo;
	private String codeCollector;
	private int totalBayar;
	private int noKwitansi;
	private String noLpk;
	private Date tanggalTransaksi;
	private String ketTanggalTransaksi;
	private Date tanggalBayar;
	private String ketTanggalBayar;
	private String status;
	private String createUser;
	private Date createDate;
	private String ketCreateDate;
	private String modifyUser;
	private Date modifyDate;
	private String ketModifyDate;
	
	
	public String getKetTanggalTransaksi() {
		return ketTanggalTransaksi;
	}
	public void setKetTanggalTransaksi(String ketTanggalTransaksi) {
		this.ketTanggalTransaksi = ketTanggalTransaksi;
	}
	public String getKetTanggalBayar() {
		return ketTanggalBayar;
	}
	public void setKetTanggalBayar(String ketTanggalBayar) {
		this.ketTanggalBayar = ketTanggalBayar;
	}
	public String getKetCreateDate() {
		return ketCreateDate;
	}
	public void setKetCreateDate(String ketCreateDate) {
		this.ketCreateDate = ketCreateDate;
	}
	public String getKetModifyDate() {
		return ketModifyDate;
	}
	public void setKetModifyDate(String ketModifyDate) {
		this.ketModifyDate = ketModifyDate;
	}
	public int getInstalmentNo() {
		return instalmentNo;
	}
	public void setInstalmentNo(int instalmentNo) {
		this.instalmentNo = instalmentNo;
	}
	public int getOrederId() {
		return orederId;
	}
	public void setOrederId(int orederId) {
		this.orederId = orederId;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getContractNo() {
		return contractNo;
	}
	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}
	public String getCodeCollector() {
		return codeCollector;
	}
	public void setCodeCollector(String codeCollector) {
		this.codeCollector = codeCollector;
	}
	public int getTotalBayar() {
		return totalBayar;
	}
	public void setTotalBayar(int totalBayar) {
		this.totalBayar = totalBayar;
	}
	public int getNoKwitansi() {
		return noKwitansi;
	}
	public void setNoKwitansi(int noKwitansi) {
		this.noKwitansi = noKwitansi;
	}
	public String getNoLpk() {
		return noLpk;
	}
	public void setNoLpk(String noLpk) {
		this.noLpk = noLpk;
	}
	public Date getTanggalTransaksi() {
		return tanggalTransaksi;
	}
	public void setTanggalTransaksi(Date tanggalTransaksi) {
		this.tanggalTransaksi = tanggalTransaksi;
	}
	public Date getTanggalBayar() {
		return tanggalBayar;
	}
	public void setTanggalBayar(Date tanggalBayar) {
		this.tanggalBayar = tanggalBayar;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getModifyUser() {
		return modifyUser;
	}
	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}
	public Date getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	
	
	
	
}
