package dto;

import java.util.Date;

public class MasterRegionalDto {

	private String kodeRegion;
	private String namaRegion;
	private String notes;
	private String aktifStatus;
	private String createuser;
	private String createDate;
	private String modifyUser;
	private String modifyDate;
	private boolean choose;
	private boolean itemList;
	
	
	
	public boolean isItemList() {
		return itemList;
	}
	public void setItemList(boolean itemList) {
		this.itemList = itemList;
	}
	public boolean isChoose() {
		return choose;
	}
	public void setChoose(boolean choose) {
		this.choose = choose;
	}
	public String getAktifStatus() {
		return aktifStatus;
	}
	public void setAktifStatus(String aktifStatus) {
		this.aktifStatus = aktifStatus;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(String modifyDate) {
		this.modifyDate = modifyDate;
	}
	public String getKodeRegion() {
		return kodeRegion;
	}
	public void setKodeRegion(String kodeRegion) {
		this.kodeRegion = kodeRegion;
	}
	public String getNamaRegion() {
		return namaRegion;
	}
	public void setNamaRegion(String namaRegion) {
		this.namaRegion = namaRegion;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getCreateuser() {
		return createuser;
	}
	public void setCreateuser(String createuser) {
		this.createuser = createuser;
	}
	public String getModifyUser() {
		return modifyUser;
	}
	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}
}
