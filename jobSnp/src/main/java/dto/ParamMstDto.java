package dto;

public class ParamMstDto {
	
	private String paramId;
	private String condition;
	private String levelParam;
	private String parentId;
	private String parentIdStr;
	private boolean choose;
	private boolean itemList;
	
	
	
	public boolean isItemList() {
		return itemList;
	}
	public void setItemList(boolean itemList) {
		this.itemList = itemList;
	}
	public boolean isChoose() {
		return choose;
	}
	public void setChoose(boolean choose) {
		this.choose = choose;
	}
	public String getParentIdStr() {
		return parentIdStr;
	}
	public void setParentIdStr(String parentIdStr) {
		this.parentIdStr = parentIdStr;
	}
	public String getParamId() {
		return paramId;
	}
	public void setParamId(String paramId) {
		this.paramId = paramId;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public String getLevelParam() {
		return levelParam;
	}
	public void setLevelParam(String levelParam) {
		this.levelParam = levelParam;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	
	
	
}
