package dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SessionForMobileTrackingDto {
	private List<MasterRegionalDto> listRegionalDtoChoose = new ArrayList<MasterRegionalDto>();
	private List<CabangMstDto> listCabangMstDtoChoose = new ArrayList<CabangMstDto>();
	private List<UserProfileDto> listUserKolektorChoose = new ArrayList<UserProfileDto>();
	private String strArrayRegional;
	private String strArrayCabang;
	private String strArrayKolektor;
	private Date dateAwal;
	private Date dateAkhir;
	
	
	
	public Date getDateAwal() {
		return dateAwal;
	}
	public void setDateAwal(Date dateAwal) {
		this.dateAwal = dateAwal;
	}
	
	public Date getDateAkhir() {
		return dateAkhir;
	}
	public void setDateAkhir(Date dateAkhir) {
		this.dateAkhir = dateAkhir;
	}
	public String getStrArrayRegional() {
		return strArrayRegional;
	}
	public void setStrArrayRegional(String strArrayRegional) {
		this.strArrayRegional = strArrayRegional;
	}
	public String getStrArrayCabang() {
		return strArrayCabang;
	}
	public void setStrArrayCabang(String strArrayCabang) {
		this.strArrayCabang = strArrayCabang;
	}
	public String getStrArrayKolektor() {
		return strArrayKolektor;
	}
	public void setStrArrayKolektor(String strArrayKolektor) {
		this.strArrayKolektor = strArrayKolektor;
	}
	public List<MasterRegionalDto> getListRegionalDtoChoose() {
		return listRegionalDtoChoose;
	}
	public void setListRegionalDtoChoose(
			List<MasterRegionalDto> listRegionalDtoChoose) {
		this.listRegionalDtoChoose = listRegionalDtoChoose;
	}
	public List<CabangMstDto> getListCabangMstDtoChoose() {
		return listCabangMstDtoChoose;
	}
	public void setListCabangMstDtoChoose(List<CabangMstDto> listCabangMstDtoChoose) {
		this.listCabangMstDtoChoose = listCabangMstDtoChoose;
	}
	public List<UserProfileDto> getListUserKolektorChoose() {
		return listUserKolektorChoose;
	}
	public void setListUserKolektorChoose(
			List<UserProfileDto> listUserKolektorChoose) {
		this.listUserKolektorChoose = listUserKolektorChoose;
	}
	
	
}
