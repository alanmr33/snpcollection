package dto;

import java.util.Date;

public class EventsDto {
	private String eventId;
	private String eventType;
	private String eventComponent;
	private String eventComponentTarget;
	private String eventData;
	private String formId;
	public String getEventId() {
		return eventId;
	}
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public String getEventComponent() {
		return eventComponent;
	}
	public void setEventComponent(String eventComponent) {
		this.eventComponent = eventComponent;
	}
	public String getEventComponentTarget() {
		return eventComponentTarget;
	}
	public void setEventComponentTarget(String eventComponentTarget) {
		this.eventComponentTarget = eventComponentTarget;
	}
	public String getEventData() {
		return eventData;
	}
	public void setEventData(String eventData) {
		this.eventData = eventData;
	}
	public String getFormId() {
		return formId;
	}
	public void setFormId(String formId) {
		this.formId = formId;
	}
	
	
}
