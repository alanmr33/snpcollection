package dto;

import java.util.Date;

public class CollectionHeaderDto {
	private int orderId;
	private String lpkNo;
	private Date lpkDate;
	private String ketLpkDate;
	private String collectorCode;
	private String status;
	private String reassign;
	private Date reassignDate;
	private String ketReassignDate;
	private String reassignBy;
	private String previosCollector;
	private String reassignNotes;
	
	
	
	public String getKetLpkDate() {
		return ketLpkDate;
	}
	public void setKetLpkDate(String ketLpkDate) {
		this.ketLpkDate = ketLpkDate;
	}
	public String getKetReassignDate() {
		return ketReassignDate;
	}
	public void setKetReassignDate(String ketReassignDate) {
		this.ketReassignDate = ketReassignDate;
	}
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public String getLpkNo() {
		return lpkNo;
	}
	public void setLpkNo(String lpkNo) {
		this.lpkNo = lpkNo;
	}
	public Date getLpkDate() {
		return lpkDate;
	}
	public void setLpkDate(Date lpkDate) {
		this.lpkDate = lpkDate;
	}
	public String getCollectorCode() {
		return collectorCode;
	}
	public void setCollectorCode(String collectorCode) {
		this.collectorCode = collectorCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getReassign() {
		return reassign;
	}
	public void setReassign(String reassign) {
		this.reassign = reassign;
	}
	public Date getReassignDate() {
		return reassignDate;
	}
	public void setReassignDate(Date reassignDate) {
		this.reassignDate = reassignDate;
	}
	public String getReassignBy() {
		return reassignBy;
	}
	public void setReassignBy(String reassignBy) {
		this.reassignBy = reassignBy;
	}
	public String getPreviosCollector() {
		return previosCollector;
	}
	public void setPreviosCollector(String previosCollector) {
		this.previosCollector = previosCollector;
	}
	public String getReassignNotes() {
		return reassignNotes;
	}
	public void setReassignNotes(String reassignNotes) {
		this.reassignNotes = reassignNotes;
	}
	
	
	
}
