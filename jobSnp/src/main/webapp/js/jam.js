/**
 * Created by Indocyber on 24/02/2018.
 */
setInterval(function () {
    var date=new Date();
    var day=(date.getDate()<10)? "0"+date.getDate() : date.getDate();
    var month=(date.getMonth()<10)? "0"+date.getMonth() : date.getMonth();
    var year=date.getFullYear();
    var hour=(date.getHours()<10)? "0"+date.getHours() : date.getHours();
    var minute=(date.getMinutes()<10)? "0"+date.getMinutes() : date.getMinutes();
    var second=(date.getSeconds()<10)? "0"+date.getSeconds() : date.getSeconds();
    var formated=day+"/"+month+"/"+year+" "+hour+":"+minute+":"+second;
    if (document.getElementsByClassName("simple-timer")!=null) {
        $(".simple-timer").html(formated);
    }else{
        console.log("no element found");
    }
},1000);